var
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    /*obfuscate = require('gulp-obfuscate');*/
    /*csso = require('gulp-csso'),*/
    cssbeautify = require('gulp-cssbeautify'),
    sass = require('gulp-sass'),
    less = require('gulp-less');

gulp.task('mainJsModules', function () {
    gulp.src([
        'assets/js/modules/*.js',
        'assets/js/src/elements/validator_modal_help.js',
        'assets/js/src/elements/select_dropdown.js',
        'assets/js/src/elements/function_ajaxSend.js',
        'assets/js/src/elements/function_isImage.js',
        'assets/js/src/elements/function_fileSizeMore.js',
        'assets/js/src/elements/function_showError.js'
    ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('assets/js/full'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('mainJsApi', function () {
    gulp.src('assets/js/src/api.js')
        //.pipe(concat('api.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('mainJsSlick', function () {
    gulp.src('assets/js/src/slick.js')
        .pipe(concat('slick.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('mainJsScrollbox', function () {
    gulp.src('assets/js/src/scrollbox.js')
        .pipe(concat('scrollbox.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('devJsModules', function(){
    gulp.src([
        'assets/js/ejs.js',
        'assets/js/src/elements/select_dropdown.js',
        'assets/js/src/elements/validator_modal_help.js',
        'assets/js/src/elements/validator_dev-help__form.js',
        'assets/js/src/elements/function_ajaxSend.js',
        'assets/js/src/elements/function_isImage.js',
        'assets/js/src/elements/function_fileSizeMore.js',
        'assets/js/dev/common.js',
        'assets/js/dev/add.js',
        'assets/js/modules/inProgress.js',
        'assets/js/modules/modal.js',
        'assets/js/modules/request.js'
    ])
        .pipe(concat('dev.js'))
        .pipe(gulp.dest('assets/js/full'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('jsGuest', function(){
    gulp.src([
        'assets/js/ejs.js',
        'assets/js/src/elements/validator_modal_help.js',
        'assets/js/src/elements/validator_dev-help__form.js',
        'assets/js/src/elements/select_dropdown.js',
        'assets/js/src/elements/function_ajaxSend.js',
        'assets/js/dev/guest.js',
        'assets/js/main/guest.js'
    ])
        .pipe(concat('guest.js'))
        .pipe(gulp.dest('assets/js/full'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'));
});

gulp.task('devCss', function () {
    gulp.src([
        'assets/css/dev-src/reset.css',
        'assets/css/dev-src/fonts.css',
        'assets/css/dev-src/style.css',
        'assets/css/dev-src/responsive.css'
    ])
        .pipe(concat('dev.css'))
        .pipe(cssbeautify())
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('jsDPR', function () {
    gulp.src('assets/js/src/dpr.js')
        //.pipe(concat('api.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('modal', function() {
    return gulp.src('assets/css/modal.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('watch', function(){
    // Developers
    gulp.watch([
        'assets/css/dev-src/*.css'
    ], ['devCss']);

    gulp.watch([
        'assets/js/src/elements/select_dropdown.js',
        'assets/js/src/elements/validator_modal_help.js',
        'assets/js/src/elements/validator_dev-help__form.js',
        'assets/js/src/elements/function_ajaxSend.js',
        'assets/js/dev/common.js',
        'assets/js/dev/add.js',
        'assets/js/modules/inProgress.js',
        'assets/js/modules/modal.js',
        'assets/js/modules/request.js'
    ], ['devJsModules']);

    gulp.watch([
        'assets/js/ejs.js',
        'assets/js/src/elements/validator_modal_help.js',
        'assets/js/src/elements/validator_dev-help__form.js',
        'assets/js/src/elements/select_dropdown.js',
        'assets/js/src/elements/function_ajaxSend.js',
        'assets/js/dev/guest.js',
        'assets/js/main/guest.js'
    ], ['jsGuest']);

    // Main
    gulp.watch(['assets/css/modal.scss'], ['modal']);

    gulp.watch([
        'assets/js/modules/*.js',
        'assets/js/src/elements/validator_modal_help.js',
        'assets/js/src/elements/select_dropdown.js',
        'assets/js/src/elements/function_ajaxSend.js'
    ], ['mainJsModules']);
    gulp.watch('assets/js/src/api.js', ['mainJsApi']);
    gulp.watch('assets/js/src/slick.js', ['mainJsSlick']);
    gulp.watch('assets/js/src/scrollbox.js', ['mainJsScrollbox']);

    // Both
    gulp.watch('assets/js/src/dpr.js', ['jsDPR']);
});

gulp.task('less', function() {
    gulp.src('sources/less/style.less')
        .pipe(less())
        .pipe(concat('_style.css'))
        .pipe(gulp.dest('sources/css/'));
});

gulp.task('imgAreaSelect', function() {
    gulp.src('assets/js/src/jquery.imgareaselect.js')
        .pipe(uglify())
        .pipe(concat('jquery.imgareaselect.min.js'))
        .pipe(gulp.dest('assets/js/'));
});

/*
gulp.task('cssbeautify', function () {
    gulp.src('assets/css/main1.css')
        .pipe(cssbeautify())
        .pipe(gulp.dest('min'));
});*/
