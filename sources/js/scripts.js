$(document).ready(function () {

	var onWindowResizeLoad = function () {
		var _f = $('.dev-footer'),
			_m = $('.minHeight');

		_m.css('min-height', 'auto');

		var contentHeight = _f.offset().top + _f.outerHeight(),
			windowHeight  = $(window).height();

		if (windowHeight > contentHeight)
			_m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
	}

	$(window).on('resize load', onWindowResizeLoad);

	$('.sel').each(function () {
		var self = $(this);

		self.find('.sel_hidden').val(self.find('.sel_list li:first').attr('data-value'));
	});

	$('.sel_in').on('click', function () {
		var self = $(this),
			select = self.closest('.sel'),
			option_list = select.find('.sel_list');

		if (option_list.is(':visible')) {
			option_list.slideUp(200);
			select.removeClass('is-opened');
			self.find('.sel_arrow').removeClass('is-active');
		} else {
			if ($('.sel .sel_list:visible').length) {
				$('.sel .sel_list:visible').hide();
				$('.sel .sel_arrow').removeClass('is-active');
			}

			option_list.slideDown(200);
			select.addClass('is-opened');
			self.find('.arrow').addClass('is-active');
		}
	});

	$('.sel_list li').on('click', function () {
		var self = $(this),
			title = self.closest('.sel').find('.sel_in .sel_title'),
			option = self.html();

		title.html(option);
		self.closest('.sel').find('input[type=hidden]').val(self.attr('data-value'));
		self.closest('.sel_list').find('li').removeClass('is-active');
		self.addClass('is-active');
		self.closest('.sel_list').slideUp(200);
		self.closest('.sel').removeClass('is-opened');
		self.closest('.sel').find('.sel_arrow').removeClass('is-active');
	});

	$(document).on('click', function (e) {
		if ($('.sel .sel_list:visible').length && !$(e.target).closest('.sel').length) {
			$('.sel').removeClass('is-opened');
			$('.sel .sel_list').slideUp(200);
			$('.sel .sel_arrow').removeClass('is-active');
		}
	});

	$(document).keyup(function (e) {
		if (e.keyCode == 27) {
			$('.sel').removeClass('is-opened');
			$('.sel .sel_list').slideUp(200);
		}
	});

	$('.dev-charts__tabs-list-item').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).addClass('active').parent().find('.active').not(this).removeClass('active');

		var id = $(this).data('tab');

		$(this).closest('.dev-charts__tabs').find('.dev-charts__tabs-box.shown').removeClass('shown').parent().find('*[data-tab=' + id + ']').addClass('shown');

		return false;
	});

	var _b = $('.dev-dialog__addAdmin-box');

	_b.find('form').on('submit', function () {
		if (_b.find('input[type=text]').val() != '') {

			// SEARCH CODE HERE. ADD RESULT TO .dev-dialog__addAdmin-result

			var _r = _b.find('.dev-dialog__addAdmin-result');

			_r.show();

			_r.unbind();
			_r.on('click', function () {
				
				_b.find('input[type=text]').val('');
				_b.hide();

				_d = _b.parent().find('.dev-dialog__addAdmin-done');

				_d.find('.dev-dialog__addAdmin-done-item-name').html(_r.find('.dev-roles__list-item-name').html());
				_d.show();

				_d.find('.dev-dialog__addAdmin-done-item-delete').unbind();
				_d.find('.dev-dialog__addAdmin-done-item-delete').on('click', function () {
					_d.hide();
					_r.hide();

					_b.show();

					_b.find('input[type=text]').focus();
				});
			});
		} else {
			_b.find('input[type=text]').focus();
		}

		return false;
	});

	/*var dlgtrigger = document.querySelectorAll('[data-modal]');

	for ( i = 0; i < dlgtrigger.length; i++) {
		 somedialog = document.getElementById(dlgtrigger[i].getAttribute('data-modal'));
		 dlg = new DialogFx(somedialog);

		$(dlgtrigger[i]).on('click', function (e) {
			e.preventDefault ();
			e.stopPropagation ();

			dlg.toggle();

			$('body, html').animate({
				scrollTop: $('.modal--open .modal').offset().top - 100
			}, 500);

			return false;
		});
	}*/

	$('[data-modal]').each(function () {
		var id = $(this).data('modal');
		var dlg = new DialogFx(document.getElementById(id));

		$(this).on('click', function (e) {
			e.preventDefault ();
			e.stopPropagation ();

			dlg.toggle();

			return false;
		});
	});

	$('.ui.dropdown').dropdown();

	$('.dev-news__item-tools-delete').on({
		click: function (e) {
			e.preventDefault();
			e.stopPropagation();

			$(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);

			return false;
		}
	});

});