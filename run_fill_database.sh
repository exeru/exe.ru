#!/bin/bash

offset=$1

if [ $offset -gt 0 ]
then
    for i in 0 1 2 3 4 5 6 7 8 9
    do
	let "c = 100000 * $i + $offset"
	let "d = $i + 1"
	echo "php index.php console task_2477 $c 80000 100 > mlog_${offset}_${d}.log 2>&1 &"
	`php index.php console task_2477 $c 80000 100 > mlog_${offset}_${d}.log 2>&1 &`
    done
else
    echo "Usage: ./run_fill_database.sh offset"
fi
exit