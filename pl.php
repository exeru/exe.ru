<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27.04.17
 * Time: 17:39
 */
$uPixel = $nuPixel = 0;
$a = file('1/20170428.click.log');
$by_ip = [];
foreach($a as $aIndex=>$aString) {
    $aString = trim($aString, "\n,)('");
    list($aTI, $agent) = explode(', \'', $aString);
    list($time, $ip) = explode(', ', $aTI);
    $time = date('H:i:s', $time);
    $ip = long2ip($ip);

    if (!isset($by_ip[$ip])) {
        $by_ip[$ip] =  [
            'click' => [],
            'pixel' => shell_exec('cat 1/20170428.pixel.log | grep ' . $ip),
            'access' => shell_exec('cat 1/20170428.access.log | grep ' . $ip)
        ];

        if (isset($by_ip[$ip]['pixel'])) {
            $pixel = explode("\n", $by_ip[$ip]['pixel']);
            $by_ip[$ip]['pixel'] = [];
            $uPixel++;
            foreach($pixel as $pIndex=>$pString) {
                if (strlen($pString) == 0) {
                    continue;
                }
                $nuPixel++;
                $pString = (array) json_decode($pString);
                $by_ip[$ip]['pixel'][date('H:i:s', $pString['time'])] = $pString['uid'];
            }
        }

        if (isset($by_ip[$ip]['access'])) {
            $access = explode("\n", $by_ip[$ip]['access']);
            $by_ip[$ip]['access'] = [];
            foreach($access as $acIndex => $acString) {
                if (strlen($acString) == 0) {
                    continue;
                }
                $acString = explode(': ', $acString);
                list(,$acTime,) = explode(' ', $acString[0]);
                $by_ip[$ip]['access'][$acTime] = substr($acString[1], strpos($acString[1] , ']') + 2);
            }
        }
    }

    $by_ip[$ip]['click'][$time] = $agent;

}
echo 'Количество кликов: ' . count($a) . "\n";
echo 'Уникальных ip: ' . count($by_ip) . "\n";
echo 'Пиксель запрашивался(уникальные ip): ' . $uPixel . "\n";
echo 'Пиксель запрашивался раз: ' . $nuPixel . "\n";

print_r($by_ip);