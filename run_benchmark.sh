#!/bin/bash

offset=$1

if [ $offset -gt 0 ]
then
    echo "php index.php console benchmark_prepare_requests ${offset}"
    `php index.php console benchmark_prepare_requests ${offset}`
    for i in 0 1 2 3 4 5 6 7 8 9
    do
	let "c = offset / 10"
	echo "php index.php console benchmark_start $c >> benchmark_${c}.log 2>&1 &"
	`php index.php console benchmark_start $c >> benchmark_${offset}.log 2>&1 &`
    done
else
    echo "Usage: ./run_benchmark.sh count"
fi
exit