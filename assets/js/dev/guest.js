/**
 * Created by ruslan on 05.04.16.
 * source: /assets/js/dev/guest.js
 * Разработчики, общий для не-авторизованного пользователя
 */
$(document).ready(function() {
    $('[data-modal]').on('click', function() {
        var id = this.getAttribute('data-modal'),
            modal = $('#' + id);

        if (id == 'help' && pageUrl != '/blocked') {
            history.pushState({}, '', '/help');
            modal.find('form')[0].reset();
            modal.find('label.error').hide();
            $('#help .sel_title').html('Выберите тему обращения');
        }

        modal.removeClass('modal--close');
        modal.addClass('modal--open');

        hideScroll();

        modal.find('.overlay, [data-modal-close]').off('click').on('click', function(){
            var parent = $(this).parents('.modal-wrap'),
                path = /^\/(news|dialogs|friends|settings|help)(?:\/*)$/.exec(window.location.pathname);

            if (parent[0].id == 'help' && path != null) {
                history.pushState({}, '', '/');
            }

            parent
                .addClass('modal--close')
                .removeClass('modal--open');

            autoScroll();

            return false;
        });
        return false;
    });

    (function(){
        var handlerPopState = function(e) {
            // Восстанавливаем состояние окна с закрытыми модальными окнами
            autoScroll();

            var path = /^\/(help)(?:\/*)$/.exec(window.location.pathname);
            if (path != null) {
                var modalId = path[1] == 'help' ? path[1] : path[1] + '-modal';
                $('[data-modal="'+ modalId +'"]')[0].click();
            }
        };

        $(window).on('popstate', handlerPopState);

        handlerPopState(null);
    })();
});

function hideScroll() {
    var body = $('body'),
        overflowY = - body[0].clientWidth;

    body.css({'overflow-y':'hidden'});
    overflowY += body[0].clientWidth;

    if (overflowY > 0) {
        body.css({'margin-right': overflowY + 'px'});
    }
}

function autoScroll() {
    $('body').css({'overflow-y':'auto', 'margin-right':0});
}
