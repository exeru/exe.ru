/**
 * Created by ruslan on 03.02.16.
 */
$(document).ready(function() {
    $('#c1').on('change', function() {
        $('.dev-add-game form [name="agree_dub"]').trigger('click');
    });

    $('form#statusControl button').on('click', function(e) {
        if (e.currentTarget.value != 'reject' && e.currentTarget.value != 'accept') {
            return false;
        }

        var form = $('form#statusControl');

        if (e.currentTarget.value == 'reject') {
            var comment = $('[name="comment"]', form).val().trim();
            if (comment.length < 3) {
                $('#reject-comment-error').show();
                $('[name="comment"]', form).focus();
                $('[name="comment"]', form).one('change keypress', function() {
                    $('#reject-comment-error').hide();
                    $('[name="comment"]', form).unbind();
                });
                return false;
            }
        }

        $('[name="action"]').val(e.currentTarget.value);

        ajaxSend(form);
    });
    // Дополнительные методы валидации
    $.validator.addMethod(
        "minLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length < param) {
                return false;
            } else {
                return true;
            }
        }, "Не меньше 2-х символов");
    $(".dev-add-game form, .dev-game__info form:first").validate({
        rules: {
            'title': {
                required: true,
                minLen: 2
            },
            'description': {
                required: true,
                minLen: 2
            },
            'action_dub': {
                required: true
            },
            'agree_dub': {
                required: true
            }
        },
        messages: {
            'title': {
                required: "Добавьте название игры",
                minLen: "Добавьте название игры"
            },
            'description': {
                required: "Добавьте краткое описание игры",
                minLen: "Добавьте краткое описание игры"
            },
            'action_dub': {
                required: "Необходимо выбрать жанр игры"
            },
            'agree_dub': {
                required: "Необходимо подтвердить согласие с правилами"
            }
        },
        submitHandler: function(form) {
            ajaxSend(form);
        }
    });

    $(".dev-game__settings_p1").validate({
        rules: {
            'api_key': {
                minlength: 16,
                required: true
            }
        },
        messages: {
            'api_key': {
                minlength: "Длина ключа не менее 16 символов",
                required: "Необходимо придумать секретный ключ"
            }
        },
        submitHandler: function(form) {
            ajaxSend(form);
        }
    });

    $(".dev-game__settings_p2").validate({
        rules: {
            'height': {
                min: 250,
                max: 4000,
                required: true
            },
            'http': {
                url: true,
                required: true
            },
            'https': {
                url: true,
                required: true
            },
            'callback_url': {
                url: true,
                required: true
            }
        },
        messages: {
            'height': {
                min: "Минимальная высота 250px",
                max: "Максимальная высота 4000px",
                required: "Укажите высоту iframe"
            },
            'http': {
                url: "Укажите правильный http адрес фрейма с приложением",
                required: "Укажите http адрес фрейма"
            },
            'https': {
                url: "Укажите правильный https адрес фрейма с приложением",
                required: "Укажите https адрес фрейма"
            },
            'callback_url': {
                url: "Укажите правильный адрес для обратных запросов",
                required: "Укажите http адрес обратных для обратных вызовов"
            }
        },
        submitHandler: function(form) {
            ajaxSend(form);
        }
    });

    $("#addNews form").validate({
        rules: {
            'content': {
                required: true
            }
        },
        messages: {
            'content': {
                required: "Текст новости - обязательное поле."
            }
        }
    });
});
