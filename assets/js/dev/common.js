function stampToDate(stamp, format) {
    var date = new Date();
    date.setTime(stamp * 1000);

    out = {
        d : date.getDate(),
        m : date.getMonth()+ 1,
        y : date.getYear() - 100,
        Y : date.getFullYear(),
        H : date.getHours(),
        i : date.getMinutes()
    };

    for(var k in out)
        format = format.replace('%' + k, out[k] < 10 ? "0" + out[k] : out[k]);

    return format;
}

$(document).ready(function () {
    // При получении ответа 403 - редирект на корень
    $.ajaxSetup({
        statusCode: {
            403: function() {
                window.location.href = '/';
            }
        }
    });

	var onWindowResizeLoad = function () {
		var _f = $('.dev-footer'),
			_m = $('.minHeight');

		_m.css('min-height', 'auto');

		var contentHeight = _f.offset().top + _f.outerHeight(),
			windowHeight  = $(window).height();

		if (windowHeight > contentHeight)
			_m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
	};

    onWindowResizeLoad();

	$(window).on('resize load', onWindowResizeLoad);


    $("#dev-help__form").on('submit', function(){
        if ($('input.error', this).length > 0) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        ajaxSend(this, function(r) {
            var version = (typeof ExeRu !== 'undefined' && typeof ExeRu.version !== 'undefined') ? '?' + ExeRu.version : '',
                html = new EJS({url:'/assets/js/dev/views/help_ok.ejs' + version}).render(r);

            $('#dev-help__form').replaceWith(html);
        });

        return false;
    });

    // Форма помощи у авторизованного пользователя
    $('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        ajaxSend($('#help form'), function(r) {
            if (typeof r.result !== 'undefined' && r.result) {
                alert('Ваш вопрос отправлен.');
                $("#help form input").attr('readonly', false);
                $('#help [data-modal-close]').click();
            }
        });

        return false;
    });

	$('.dev-charts__tabs-list-item').on('click', function(e) {
        var parent = $(this).parent(),
            id = $(this).data('tab');

		$(this).addClass('active');
        parent.find('a.active').not(this).removeClass('active');

        parent.find('span').removeClass('active');
        parent.find('span[data-tab="' + id + '"]').addClass('active');

        $(this).closest('.dev-charts__tabs').find('.dev-charts__tabs-box.shown').removeClass('shown').parent().find('*[data-tab=' + id + ']').addClass('shown');

		return false;
	});

	$('#f1, #f2, #f3, #f4, #f5, #f6, #f7').on('change', function(e) {
        if (!isImage(this.files[0])) {
            alert('Необходимо выбрать изображение');
            return false;
        }

        if (fileSizeMore(this.files[0], 10485760)) {
            alert('Слишкой большой файл, не более 10 мб.');
            return false;
        }

        ExeRu.inProgress.on();

        var form = $(this).closest('form')[0],
		    formData = new FormData(form),
            xhr = new XMLHttpRequest();

        xhr.open('POST', form.action);

        xhr.onload = function(e) {
            var r = JSON.parse(e.target.responseText);
            if (typeof r.location !== 'undefined') {
                location.href = r.location;
            } else if (typeof r.response.error !== 'undefined') {
                $(':file', form).val('');
                alert(r.response.error);
            } else {
                $('img', form).attr('src', imageHost + r.response.filename);
            }
            console.debug(r);
            ExeRu.inProgress.off();
        };

        xhr.upload.onprogress = function (e) {
            console.debug(parseInt(e.loaded / e.total * 100));
        };

        xhr.send(formData);
	});

    ExeRu.modal.init(document);

    (function(){
        var handlerPopState = function(e) {
            // Восстанавливаем состояние окна с закрытыми модальными окнами
            $("body").css({'overflow-y':'auto', 'padding-right': '0'});
            $('iframe').css('visibility', 'visible');
            $('.modal--open').each(function(){
                $(ExeRu.modal.dialogs[this.id].el).trigger('closeDialog');
                ExeRu.modal.dialogs[this.id].isOpen = false;
                $('[data-modal="' + this.id + '"]').removeClass('active');
                $(this).removeClass('modal--open');
            });

            var path = /^\/(news|dialogs|friends|settings|help)(?:\/*)$/.exec(window.location.pathname);
            if (path != null) {
                var modalId = path[1] == 'help' ? path[1] : path[1] + '-modal';

                ExeRu.modal.dialogs[modalId].el.setAttribute('no-push-history', true);
                ExeRu.modal.dialogs[modalId].toggle();
                // Включаем первую табу
                $(ExeRu.modal.dialogs[modalId].el).find('.modal-tabs__list a:first').click();
                // А для фильтров - первый чекбох
                $(ExeRu.modal.dialogs[modalId].el).find('.checkbox-list input:first').click();
            } else {
                var id = /^\/id(\d+)(?:\/*)$/.exec(window.location.pathname);
                if (id != null) {
                    $('#forOpenByHash').attr({'data-uid':id[1], 'no-push-history':true})[0].click();
                }
            }
        };

        $(window).on('popstate', handlerPopState);

        handlerPopState(null);
    })();

    $('a.btn-main', document).on('click', ExeRu.call);

    $('#dev_news_add_file').on('change', ExeRu.dev.attachFile);

    $('.dev-news__uploaded-delete').on('click', ExeRu.dev.detachFile);


    $('.dev-news__item-tools-delete').on('click', function() {
            $(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);
            return false;
    });
    $('.dev-news__item-tools-delete-cancel').on('click', function() {
        $(this).parents('.dev-news__item-deleteBox').hide();
        return false;
    });

    $('.dev-billing .dev-billing__history-search input').on('keyup keydown focus blur', function(e) {
        if (e.type == 'keydown' && e.which == 13) {
            $('.dev-billing .dev-billing__history-search-do').trigger('click');
            return false;
        }

        var text = $.trim(this.value);
        if (text.length >= 5) {
            $('.dev-billing .dev-billing__history-search-error').hide();
        }

        if (text.length > 0) {
            $('.dev-billing .dev-billing__history-search-reset').show();
        } else {
            $('.dev-billing .dev-billing__history-search-reset').hide();
            $('.dev-billing .dev-billing__history-not-found').hide();
            $('.dev-billing .dev-billing__history-table').show();
            $('.dev-billing .dev-billing__history-table tbody tr').show();
        }
    });

    $('.dev-billing .dev-billing__history-search-reset').on('click', function() {
        $('.dev-billing .dev-billing__history-search input')
            .val('')
            .trigger('blur');
        return false;
    });

    $('.dev-billing .dev-billing__history-search-do').on('click', function() {
        var input = $('.dev-billing .dev-billing__history-search input'),
            text = input.val().trim();

        if (text.length < 5) {
            $('.dev-billing .dev-billing__history-search-error').show();
            input.val(text);
            input.focus();
            return false;
        }

        var table = $('.dev-billing .dev-billing__history-table'),
            result = table.find('tbody tr[data-uid="' + text + '"]'),
            noResult = $('.dev-billing .dev-billing__history-not-found');

        if (result.length == 0) {
            table.hide();
            noResult.show();
        } else {
            noResult.hide();
            table.show();
            table.find('tbody tr').hide();
            result.show();
        }

        return false;
    });

    $('.dev-news .dev-billing__history-search input').on('keyup keydown focus blur', function(e) {
        if (e.type == 'keydown' && e.which == 13) {
            $('.dev-billing__history-search-do').trigger('click');
            return false;
        }

        var text = $.trim(this.value);
        if (text.length >= 5) {
            $('.dev-billing__history-search-error').hide();
        }

        if (text.length > 0) {
            $('.dev-billing__history-search-reset').show();
        } else {
            $('.dev-billing__history-search-reset').hide();
            $('.dev-news__search-box').hide();
            $('.dev-news__history-box').show();
        }
    });

    $('.dev-news .dev-billing__history-search-reset').on('click', function() {
        $('.dev-billing__history-search input')
            .val('')
            .trigger('blur');
        $('.dev-news__search-box').hide();
        $('.dev-news__history-box').show();
        $('.dev-billing__history-search-error').hide();
    });

    $('.dev-news .dev-billing__history-search-do').on('click', ExeRu.dev.feedsSearch);

    $('.ui.dropdown').dropdown();

    $('.dev-roles input:checkbox').on('change', ExeRu.dev.changeAccess);


    jQuery.datetimepicker.setLocale('ru');
    jQuery('#datetimepicker').datetimepicker({
        format: 'd.m.Y H:i',
        lang:'ru',
        onChangeDateTime:function(dp,$input){
            if (dp) {
                $('#addNews form [name="time"]').val(Math.floor(dp.getTime()/1000));
            } else {
                $('#addNews form [name="time"]').val('');
            }
        }
    });


});

var ExeRu = {
	version: 75,
	call: function(e) {
		var module = this.getAttribute('data-module');
		var method = this.getAttribute('data-method');
		if (module !== null && method !== null) {
			ExeRu[module][method](this);
		}
		return false;
	}
};

ExeRu.dev = {
    urlFeedDelete: '/deleteFeed',
    urlFeedEdit: '/getFeed',
    urlFeedsGetNext: '/devAjax/feedsGetNext',
    urlFeedsSearch: '/devAjax/feedsSearch',
    urlRolesChangeAccess: '/roles/changeAccess',

    preview: function(e) {
        var form = $(e).closest('form');
        if (form.valid()) {
            $('[data-modal="preview"]')[0].click();
        }
        return false;
    },
    prepare: function() {
        var form = $('#addNews form'),
            data = {};
        data['game_title'] = form.find('[name="game_title"]').val();
        data['game_gid'] = form.find('[name="game_gid"]').val();
        data['game_image_150x150'] = form.find('[name="game_image_150x150"]').val();
        data['game_applink'] = form.find('[name="game_applink"]').val();

        data['title'] = form.find('[name="title"]').val();
        data['content'] = form.find('[name="content"]').val();
        data['params'] = encodeURIComponent(form.find('[name="params"]').val());
        data['image_url'] = document.getElementById('uploaded_file').getAttribute('src');
        data['timestamp'] = form.find('[name="time"]').val();

        if (data['timestamp'] == '') {
            var date = new Date();
        } else {
            var date = new Date(parseInt(data['timestamp']) * 1000);
        }

        out = {
            d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
            m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
            Y : date.getFullYear().toString()
        };
        data['date'] = out.d + '.' + out.m + '.' + out.Y[2] + out.Y[3];

        var html = new EJS({url:'/assets/js/dev/views/newsPreview.ejs?' + ExeRu.version}).render(data);
        $('#preview .dev-dialog__preview').html(html);

        return false;
    },
    attachFile: function() {
        var fileType = this.files[0].type,
            overhead = this.files[0].size - 10485760;

        if (fileType != 'image/gif' && fileType != 'image/jpg' && fileType != 'image/jpeg' && fileType != 'image/png') {
            alert('Необходимо выбрать изображение');
            return false;
        }

        if (overhead > 0) {
            alert('Слишкой большой файл, не более 10 мб.');
            return false;
        }

        var reader = new FileReader();
        reader.onload = function (e) {
            var img = document.getElementById("uploaded_file");
            img.setAttribute("src", e.target.result);

            $('#addNews form [name="file_state"]').val('');

            $('#addNews .dev-news__file').hide();
            $('#addNews .dev-news__uploaded').show();
         };

         reader.readAsDataURL(this.files[0]);
    },
    detachFile: function() {
        $('#addNews .dev-news__uploaded').hide();
        $('#addNews .dev-news__file').show();

        var img = document.getElementById("uploaded_file");
        img.setAttribute("src", '');

        $('#addNews form [name="file_state"]').val('detach');

        return false;
    },
    publish: function() {
        var form = $('#addNews form');
        if (form.valid()) {
            ExeRu.inProgress.on();
            form.submit();
        }
        return false;
    },
    create: function() {
        $('#datetimepicker').parent().show();
        var form = $('#addNews form');
        form.find('[name="fid"]').val('');
        form.find('[name="time"]').val('');
        form.find('[name="title"]').val('');
        form.find('[name="content"]').val('');
        form.find('[name="params"]').val('');
        $('#dev_news_add_file').val('');
        $('#uploaded_file').attr('src', '');

        $('#addNews .dev-dialog__header-title').html('Создание новости');
        $('#addNews .dev-news__uploaded').hide();
        $('#addNews .dev-news__file').show();
        $('#addNews .dev-dialog__footer-main, #preview .dev-dialog__footer-main').html('Опубликовать');

        $('[data-modal="addNews"]')[0].click();
        return false;
    },
    edit: function(eventElement) {
        var params = {
            fid: eventElement.getAttribute('data-fid'),
            gid: eventElement.getAttribute('data-gid'),
            appointed: eventElement.hasAttribute('data-type') && eventElement.getAttribute('data-type') == 'appointed'
        };

        if (params.appointed) {
            $('#datetimepicker').parent().show();
        } else {
            $('#datetimepicker').parent().hide();
        }

        ExeRu.inProgress.on();
        ExeRu.request.get(ExeRu.dev.urlFeedEdit, params, function(r) {
            var feed = r.response.result,
                form = $('#addNews form');

            form.find('[name="fid"]').val(feed.fid);
            form.find('[name="time"]').val(feed.time);
            form.find('[name="title"]').val(feed.title === null ? '' : feed.title);
            form.find('[name="content"]').val(feed.content);
            form.find('[name="params"]').val(decodeURIComponent(feed.params === null ? '' : feed.params));

            if (typeof feed.appointed !== 'undefined') {
                form.find('#datetimepicker').val(feed.appointed);
            }

            $('#addNews .dev-dialog__header-title').html('Редактирование новости');
            $('#addNews .dev-dialog__footer-main, #preview .dev-dialog__footer-main').html('Сохранить');

            $('#dev_news_add_file').val('');

            if (feed.image_url === null) {
                $('#uploaded_file').attr('src', '');
                $('#addNews .dev-news__uploaded').hide();
                $('#addNews .dev-news__file').show();
            } else {
                $('#uploaded_file').attr('src', imageHost + feed.image_url);
                $('#addNews .dev-news__file').hide();
                $('#addNews .dev-news__uploaded').show();
            }

            $('[data-modal="addNews"]')[0].click();

            ExeRu.inProgress.off();
        });


        return false;
    },
    delete: function(eventElement) {
        var params = {
            fid: eventElement.getAttribute('data-fid'),
            gid: eventElement.getAttribute('data-gid'),
            appointed: eventElement.hasAttribute('data-type') && eventElement.getAttribute('data-type') == 'appointed'
        };

        ExeRu.inProgress.on();
        ExeRu.request.post(ExeRu.dev.urlFeedDelete, params, function(r) {
            if (typeof(r.response.result.fid) !== 'undefined') {
                if (typeof(r.response.result.appointed) !== 'undefined' && r.response.result.appointed) {
                    var news = $('.dev-news__item[data-fid="' + r.response.result.fid + '"][data-type="appointed"]');
                    if (news.parent().find('div[data-type="appointed"]').length == 1) {
                        news.parents('.dev-news').find('.dev-game__head.appointed').remove();
                    }
                    news.remove();
                } else {
                    $('[data-fid="' + r.response.result.fid + '"].dev-news__item').remove();
                    // Меняем смещение кнопке показать ещё
                    var offset = parseInt($('.dev-billing__history-more [data-offset]').attr('data-offset'));
                    if (offset > 0) {
                        $('.dev-billing__history-more [data-offset]').attr('data-offset', (offset - 1));
                    }
                }
            }
            ExeRu.inProgress.off();
        });

        return false;
    },
    copy: function(eventElement) {
        ExeRu.inProgress.on();
        var form = $(eventElement).closest('form'),
            params = {
                fileIconType: form.find('[name="fileIconType"]').val(),
                action: 'copy'
            };

        ExeRu.request.post(form[0].action, params, function(r) {
            if (typeof r.response.filename !== 'undefined') {
                $('img', form).attr('src', imageHost + r.response.filename);
            }
            ExeRu.inProgress.off();
        });

        return false;
    },
    feedsGetNext: function(eventElement) {
        var params = {
            gid: eventElement.getAttribute('data-gid'),
            offset: eventElement.getAttribute('data-offset')
        };

        ExeRu.request.get(ExeRu.dev.urlFeedsGetNext, params, function(r) {
            var limit = parseInt(eventElement.getAttribute('data-limit'));
            var offset = parseInt(eventElement.getAttribute('data-offset')) + parseInt(r.length);
            eventElement.setAttribute('data-offset', offset);

            if (r.length > 0) {
                var form = $('#addNews form'),
                    data = {};
                data['game_title'] = form.find('[name="game_title"]').val();
                data['game_gid'] = form.find('[name="game_gid"]').val();
                data['game_image_150x150'] = form.find('[name="game_image_150x150"]').val();
                data['game_applink'] = form.find('[name="game_applink"]').val();

                for(var i in r) {
                    data['fid'] = r[i].fid;
                    data['title'] = r[i].title;
                    data['content'] = r[i].content;
                    data['params'] = encodeURIComponent(r[i].params);
                    data['image_url'] = r[i].image_url;
                    data['timestamp'] = r[i].time;

                    var date = new Date(parseInt(data['timestamp']) * 1000);
                    out = {
                        d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
                        m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
                        Y : date.getFullYear().toString()
                    };
                    data['date'] = out.d + '.' + out.m + '.' + out.Y[2] + out.Y[3];

                    var html = $(new EJS({url:'/assets/js/dev/views/onenews.ejs?' + ExeRu.version}).render(data));

                    $('a.btn-main', html).on('click', ExeRu.call);

                    $('.dev-news__item-tools-delete', html).on('click', function() {
                        $(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);
                        return false;
                    });
                    $('.dev-news__item-tools-delete-cancel', html).on('click', function() {
                        $(this).parents('.dev-news__item-deleteBox').hide();
                        return false;
                    });

                    html.appendTo('.dev-news__history-box');
                }
            }

            if (r.length < limit) {
                $('.dev-billing__history-more').hide();
            }
        });
        return false;
    },
    feedsSearch:  function(eventElement) {
        var input = $('.dev-billing__history-search input'),
            text = input.val().trim();

        if (text.length < 5) {
            $('.dev-billing__history-search-error').show();
            input.val(text);
            input.focus();
            return false;
        }

        ExeRu.inProgress.on();

        var params = {
            gid: eventElement.target.getAttribute('data-gid'),
            search: text
        };

        ExeRu.request.get(ExeRu.dev.urlFeedsSearch, params, function(r) {
            if (r.length < 1) {
                var html = $('<div class="not-found">По данному запросу ничего не найдено.</div>');
                $('.dev-news__search-box').html(html);
            } else {
                $('.dev-news__search-box').html('');

                var form = $('#addNews form'),
                    data = {};

                data['game_title'] = form.find('[name="game_title"]').val();
                data['game_gid'] = form.find('[name="game_gid"]').val();
                data['game_image_150x150'] = form.find('[name="game_image_150x150"]').val();
                data['game_applink'] = form.find('[name="game_applink"]').val();

                for(var i in r) {
                    data['fid'] = r[i].fid;
                    data['title'] = r[i].title;
                    data['content'] = r[i].content;
                    data['params'] = encodeURIComponent(r[i].params);
                    data['image_url'] = r[i].image_url;
                    data['timestamp'] = r[i].time;

                    var date = new Date(parseInt(data['timestamp']) * 1000);
                    out = {
                        d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
                        m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
                        Y : date.getFullYear().toString()
                    };
                    data['date'] = out.d + '.' + out.m + '.' + out.Y[2] + out.Y[3];

                    var html = $(new EJS({url:'/assets/js/dev/views/onenews.ejs?' + ExeRu.version}).render(data));

                    $('a.btn-main', html).on('click', ExeRu.call);

                    $('.dev-news__item-tools-delete', html).on('click', function() {
                        $(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);
                        return false;
                    });
                    $('.dev-news__item-tools-delete-cancel', html).on('click', function() {
                        $(this).parents('.dev-news__item-deleteBox').hide();
                        return false;
                    });

                    html.appendTo('.dev-news__search-box');
                }
            }

            $('.dev-news__history-box').hide();
            $('.dev-news__search-box').show();

            ExeRu.inProgress.off();
        });
    },

    prepareAddAdmin: function() {
        $('#addAdmin .dev-dialog__header-title').html('Добавить администратора');
        $('#addAdmin form').attr('action', '/roles/addAdmin');
        ExeRu.dev.resetAddAdmin();

        $('[data-modal="addAdmin"]')[0].click();
    },
    prepareAddTester: function() {
        $('#addAdmin .dev-dialog__header-title').html('Добавить тестировщика');
        $('#addAdmin form').attr('action', '/roles/addTester');
        ExeRu.dev.resetAddAdmin();

        $('[data-modal="addAdmin"]')[0].click();
    },
    resetAddAdmin: function() {
        var root = $('#addAdmin'),
            search = root.find('.dev-billing__history-search button'),
            submit = root.find('.dev-add-game__btns button'),
            input  = root.find('input:text'),
            box    = root.find('.dev-dialog__addAdmin-box'),
            rBox   = root.find('.dev-dialog__addAdmin-result'),
            dBox   = root.find('.dev-dialog__addAdmin-done');

        input.val('').focus();
        dBox.attr('data-uid', '');

        rBox.hide();
        dBox.hide();
        box.show();

        input.off('keyup focus').on('keyup focus', function(){
            rBox.hide();
        });

        // Кнопка поиска
        search.off('click').on('click', function() {
            var text = input.val().trim();
            if (text.length < 3 || (parseInt(text) != text)) {
                input.focus();
                return false;
            }

            ExeRu.inProgress.on();

            // Ищем пользователя
            ExeRu.request.get('/ajax/getInfoAboutUser/', {uid:text}, function(r) {
                if (r == false) {
                    alert('Пользователь не найден');
                    input.focus();
                } else {
                    var html = '<img src="' + imageHost + r.photo[70] + '" alt="" /><div class="dev-roles__list-item-name">' + r.name + ' ' + r.last_name + '</div>';
                    rBox.html(html);

                    rBox.off('click').on('click', function() {
                        box.hide();

                        dBox.attr('data-uid', text);

                        dBox.find('.dev-dialog__addAdmin-done-item-name').html(rBox.find('.dev-roles__list-item-name').html());
                        dBox.show();

                        dBox.find('.dev-dialog__addAdmin-done-item-delete').off('click').on('click', function () {
                            dBox.attr('data-uid', '');

                            dBox.hide();
                            rBox.hide();

                            box.show();
                            input.val('').focus();
                        });
                    });

                    rBox.show();
                }
                ExeRu.inProgress.off();
            });

            return false;
        });

        // Кнопка добавить
        submit.off('click').on('click', function() {
            var uid = dBox.attr('data-uid'),
                gid = dBox.attr('data-gid'),
                action = root.find('form').attr('action');
            if (uid == '' || gid == '' || action == '') {
                input.focus();
                return false;
            }

            ExeRu.inProgress.on();
            ExeRu.request.post(action, {uid:uid, gid:gid}, function(r) {
                if (typeof(r.response.errors) !== 'undefined') {
                    for(var i in r.response.errors) {
                        alert(r.response.errors[i]);
                    }
                    input.val('').focus();
                    dBox.attr('data-uid', '');

                    rBox.hide();
                    dBox.hide();
                    box.show();
                    ExeRu.inProgress.off();
                } else {
                    if (typeof(r.response.location) !== 'undefined') {
                        window.location.href = r.response.location;
                    }
                }
            });

            return false;
        });

    },
    prepareDeleteAdmin: function(eventElement) {
        var uid = $(eventElement).parents('.dev-roles__list-item').attr('data-uid');
        $('#deleteTester').attr({'data-uid':uid, 'data-action':'/roles/deleteAdmin'});
        $('#deleteTester .dev-dialog__header-title').html('Удалить администратора');

        ExeRu.dev.prepareDeleteButtons();

        $('[data-modal="deleteTester"]')[0].click();
    },
    prepareDeleteTester: function(eventElement) {
        var uid = $(eventElement).parents('.dev-roles__list-item').attr('data-uid');
        $('#deleteTester').attr({'data-uid':uid, 'data-action':'/roles/deleteTester'});
        $('#deleteTester .dev-dialog__header-title').html('Удалить тестировщика');

        ExeRu.dev.prepareDeleteButtons();

        $('[data-modal="deleteTester"]')[0].click();
    },
    prepareDeleteButtons: function() {
        var root = $('#deleteTester'),
            uid = root.attr('data-uid'),
            gid = root.attr('data-gid'),
            action = root.attr('data-action'),
            cancel = root.find('.dev-dialog__footer-additional'),
            submit = root.find('.dev-dialog__footer-main');

        cancel.off('click').on('click', function(){
            root.find('[data-modal-close]').click();
            return false;
        });

        submit.off('click').on('click', function() {
            if (uid == '' || gid == '') {
                return false;
            }
            ExeRu.inProgress.on();
            ExeRu.request.post(action, {uid: uid, gid: gid}, function(r) {
                if (typeof(r.response.errors) !== 'undefined') {
                    for(var i in r.response.errors) {
                        alert(r.response.errors[i]);
                    }
                } else {
                    if (typeof(r.response.location) !== 'undefined') {
                        window.location.href = r.response.location;
                    }
                }
            });

            return false;
        });
    },
    changeAccess: function(eventElement) {
        var checkbox = $(eventElement.target),
            uid = checkbox.parents('.dev-roles__list-item').attr('data-uid'),
            gid = checkbox.parents('.dev-roles__list-item').attr('data-gid'),
            checked = eventElement.target.checked;

        ExeRu.inProgress.on();
        ExeRu.request.post(ExeRu.dev.urlRolesChangeAccess, {uid:uid, gid:gid, checked:checked}, function(r){
            if (typeof(r.response.errors) !== 'undefined') {
                for(var i in r.response.errors) {
                    alert(r.response.errors[i]);
                }
            } else {
                if (typeof(r.response.location) !== 'undefined') {
                    window.location.href = r.response.location;
                }
            }
        });
    },
    helpReset: function() {
        $('#help form')[0].reset();
        $('#help .sel_title').html('Выберите тему обращения');
    },
    cashout: function(eventElement) {
        var root = $('#withdraw');
        $('.sum-info', root).attr('data-max-sum', eventElement.getAttribute('data-sum'));
        $('.sum-info', root).html(eventElement.getAttribute('data-sum-text'));
        $('[name="gid"]').val(eventElement.getAttribute('data-gid'));

        return false;
    },
    cashoutDo: function(eventElement) {
        var root = $('#withdraw'),
            maxSum = $('.sum-info', root).attr('data-max-sum'),
            sum = $('[name="cashout_sum"]', root).val().trim();

        if (sum == 0 || (maxSum - sum) < 0) {
            return false;
        }

        ajaxSend($('form', root)[0]);

        return false;
    },
    showTransaction: function(eventElement) {
        var root = $('#transaction');

        $('.transaction-id', root).html(eventElement.getAttribute('data-transaction-id'));
        $('.user-name', root).html('<a href="' + eventElement.getAttribute('data-user-href') + '" target="_blank">' + eventElement.getAttribute('data-user-name') + '</a>');
        $('.contract-number', root).html(eventElement.getAttribute('data-contract-number'));
        $('.withdraw-sum', root).html(eventElement.getAttribute('data-withdraw-sum'));
        $('.transaction-comment', root).html(eventElement.getAttribute('data-comment'));
        $('.transaction-date', root).html(eventElement.getAttribute('data-transaction-date'));
    }
};
