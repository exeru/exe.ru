/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/select_dropdown.js
 * Выпадающий список
 */
$(document).ready(function(){
    $('.sel_in').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            option_list = select.find('.sel_list');

        if (option_list.is(':visible')) {
            option_list.slideUp(200);
            select.removeClass('is-opened');
            self.find('.sel_arrow').removeClass('is-active');
        } else {
            if ($('.sel .sel_list:visible').length) {
                $('.sel .sel_list:visible').hide();
                $('.sel .sel_arrow').removeClass('is-active');
            }

            option_list.slideDown(200);
            select.addClass('is-opened');
            self.find('.arrow').addClass('is-active');
        }
    });

    $('.sel_list li').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            title = select.find('.sel_in .sel_title'),
            option = self.html(),
            depends = $(select.data('depends'));

        title.html(option);
        self.closest('.sel').find('input[type=hidden]').val(self.attr('data-value'));
        self.closest('.sel_list').find('li').removeClass('is-active');
        self.addClass('is-active');
        self.closest('.sel_list').slideUp(200);
        self.closest('.sel').removeClass('is-opened');
        self.closest('.sel').find('.sel_arrow').removeClass('is-active');

        depends.val(self.attr('data-value'));
        depends.trigger('keyup');
    });

    $(document).on('click', function (e) {
        if ($('.sel .sel_list:visible').length && !$(e.target).closest('.sel').length) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
            $('.sel .sel_arrow').removeClass('is-active');
        }
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
        }
    });

    $('.sel_list li.is-active').click();
});