/**
 * Created by ruslan on 27.05.16.
 * source: /assets/js/src/elements/validator_dev-help__form.js
 * Dependencies: EJS, jQuery.Validate, AjaxSend
 *
 * Валидатор формы отправки сообщений в документации и в моих играх
 */
$(document).ready(function(){
    $('#dev-help__form').validate({
        rules:{
            'subject': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'name': {
                required: true
            },
            'question': {
                required: true
            }
        },
        messages:{
            'subject': {
                required: "Выберите тему вопроса"
            },
            'email': {
                required: "Введите адрес электронной почты",
                email: "Введите правильный адрес электронной почты"
            },
            'name': {
                required: "Введите ваше имя"
            },
            'question': {
                required: "Задайте ваш вопрос"
            }
        }
    });
});