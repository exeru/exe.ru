/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/function_ajaxSend.js
 * Отправка формы ajax-запросом. Чаще всего используется в валидаторах форм.
 */
var ajaxSend = function(form, callback) {
    if (typeof ExeRu !== 'undefined' && typeof ExeRu.inProgress !=='undefined') {
        var inProgress = ExeRu.inProgress;
    } else {
        var inProgress = {
            on: function(){ $('#inProgressShadow').show(); },
            off: function(){ $('#inProgressShadow').hide(); }
        }
    }

    inProgress.on();
    $.post($(form).attr('action'), $(form).serialize(), function(r) {
        if (typeof r.errors !== 'undefined') {
            for(var i in r.errors) {
                alert(r.errors[i]);
            }
            inProgress.off();
        }  else if (typeof callback == 'function') {
            inProgress.off();
            callback(r);
        } else if (typeof r.location !== 'undefined' && r.location != location.pathname) {
            location.href = r.location;
        } else if (typeof r.location !== 'undefined' && r.location == location.pathname) {
            inProgress.off();
        }
    });
};