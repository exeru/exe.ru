/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_isImage.js
 * Проверяет, что тип файл является типом изображений.
 */
if (typeof isImage == 'undefined') {
    var isImage = function(file) {
        return file.type == 'image/gif' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/png';
    };
}