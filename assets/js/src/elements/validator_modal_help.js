/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/validator_modal_help.js
 * Валидатор формы отправки сообщений (помощь)
 */
$(document).ready(function(){
    $('#help form').validate({
        rules:{
            'subject': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'name': {
                required: true
            },
            'question': {
                required: true
            }
        },
        messages:{
            'subject': {
                required: "Выберите тему вопроса"
            },
            'email': {
                required: "Введите адрес электронной почты",
                email: "Введите правильный адрес электронной почты"
            },
            'name': {
                required: "Введите ваше имя"
            },
            'question': {
                required: "Задайте ваш вопрос"
            }
        }/*,
        submitHandler: function(form) {
            ajaxSend(form, function(r) {
                if (typeof r.result !== 'undefined' && r.result) {
                    alert('Ваш вопрос отправлен.');
                    $('#help [data-modal-close]').click();
                }
            });
        }*/
    });
});