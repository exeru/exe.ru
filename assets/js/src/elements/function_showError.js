/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_showError.js
 * Обвертка для отображения ошибок.
 */
if (typeof showError == 'undefined') {
    var showError = function(text) {
        alert(text);
        if (typeof ExeRu != 'undefined' && typeof ExeRu.inProgress != 'undefined' && ExeRu.inProgress.count == 1) {
            ExeRu.inProgress.off();
        }
        return false;
    };
}