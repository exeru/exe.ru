/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_fileSizeMore.jss
 * Проверяет, является ли файл файлом большего размера, чем требуется.
 */
if (typeof fileSizeMore == 'undefined') {
    var fileSizeMore = function (file, size) {
        return file.size - size > 0;
    }
}