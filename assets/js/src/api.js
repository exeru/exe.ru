/**
 * Created by ruslan on 21.09.15.
 */
function ExeRuApi(opt) {
    var self = {
        gameSID: null,
        options: opt,
        exe_user: null,
        app_id: null,
        users: {
            get: function (requestParams) {
                var params = self.getParams(['user_ids', 'fields'], requestParams);
                params.method = 'getUserInfo';
                top.postMessage({type: 'apiRequest', params: params, callback: 'users.get'}, '*');
            },
            isAppUser: function (requestParams) {
                var params = self.getParams(['user_id'], requestParams);
                params.method = 'isAppUser';
                top.postMessage({type: 'apiRequest', params: params, callback: 'users.isAppUser'}, '*');
            }
        },
        friends: {
            get: function (requestParams) {
                var params = self.getParams(['user_id', 'order', 'count', 'offset', 'fields'], requestParams);
                params.method = 'getFriends';
                top.postMessage({type: 'apiRequest', params: params, callback: 'friends.get'}, '*');
            },
            getAppUsers: function (requestParams) {
                var params = self.getParams([], requestParams);
                params.method = 'getAppUsers';
                top.postMessage({type: 'apiRequest', params: params, callback: 'friends.getAppUsers'}, '*');
            }
        },
        showOrderBox: function(requestParams) {
            top.postMessage({type: 'showOrderBox', data: requestParams}, '*');
        },
        showPublishBox: function(requestParams) {
            top.postMessage({type: 'showPublishBox', data: requestParams}, '*');
        },
        showRequestBox: function (requestParams) { // Вызов всплывайки в родительском окне
            top.postMessage({type: 'showRequestBox', data: requestParams}, '*');
        },
        showInviteFriends: function (requestParams) {  // Вызов всплывайки в родительском окне
            top.postMessage({type: 'showInviteFriends', data: requestParams}, '*');
        },
        setHeight: function (requestParams) { // Устанавливает высоту iframe
            top.postMessage({type: 'setHeight', data: requestParams}, '*');
        },
        setGood: function() {
            top.postMessage({type: 'setGood', data: {app_id: self.app_id}}, '*');
        },
        setSuperGood: function() {
            top.postMessage({type: 'setSuperGood', data: {app_id: self.app_id}}, '*');
        },
        getParams: function (fields, requestParams) {
            var params = {
                api_id: 1,
                format: 'json',
                sid: self.gameSID,
                rnd: Math.random()
            };
            if (fields.length < 1 || typeof(requestParams) === 'undefined') {
                return params;
            }
            fields.forEach(function (prop) {
                if (typeof(requestParams[prop]) !== 'undefined') {
                    params[prop] = requestParams[prop];
                }
            });
            return params;
        },
        listener: function (event) {
            switch (event.data.type) {
                case 'showRequestBox':
                {
                    window[self.options['showRequestBox']]('showRequestBox', event.data.data);
                    break;
                }
                case 'showInviteFriends':
                {
                    window[self.options['showInviteFriends']]('showInviteFriends', event.data.data);
                    break;
                }
                case 'showOrderBox':
                {
                    window[self.options['showOrderBox']]('showOrderBox', event.data.data);
                    break;
                }
                case 'showPublishBox':
                {
                    window[self.options['showPublishBox']]('showPublishBox', event.data.data);
                    break;
                }
                case 'apiResponse':
                {
                    if (typeof(window[self.options[event.data.callback]]) == 'function') {
                        window[self.options[event.data.callback]](event.data.callback, event.data.data);
                    }
                }
            }
        },
        init: function() {
            var params = self.parseQueryString(window.location.search);

            if (typeof(params['game_sid'] == 'undefined' || params['game_sid'].length !== 64)) {
                self.gameSID = params['game_sid'];
            }

            ['exe_user', 'app_id'].forEach(function(field){
                if (typeof(params[field]) !== 'undefined') {
                    var value = parseInt(params[field]);
                    if (value == params[field] && value > 0) {
                        self[field] = value;
                    }
                }
            });

            if (window.addEventListener) {
                window.addEventListener("message", self.listener);
            } else { // IE8
                window.attachEvent("onmessage", self.listener);
            }
            setTimeout(self.setGood, 60000);
            setTimeout(self.setSuperGood, 600000);
        },
        parseQueryString: function (strQuery) {
            var i,
                tmp = [],
                tmp2 = [],
                objRes = {};
            if (strQuery != '') {
                tmp = (strQuery.substr(1)).split('&');
                for (i = 0; i < tmp.length; i += 1) {
                    tmp2 = tmp[i].split('=');
                    if (tmp2[0]) {
                        objRes[tmp2[0]] = tmp2[1];
                    }
                }
            }
            return objRes;
        }
    };

    self.init();

    return self;
}