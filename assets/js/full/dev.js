(function(){


    var rsplit = function(string, regex) {
            var result = regex.exec(string),retArr = new Array(), first_idx, last_idx, first_bit;
            while (result != null)
            {
                first_idx = result.index; last_idx = regex.lastIndex;
                if ((first_idx) != 0)
                {
                    first_bit = string.substring(0,first_idx);
                    retArr.push(string.substring(0,first_idx));
                    string = string.slice(first_idx);
                }
                retArr.push(result[0]);
                string = string.slice(result[0].length);
                result = regex.exec(string);
            }
            if (! string == '')
            {
                retArr.push(string);
            }
            return retArr;
        },
        chop =  function(string){
            return string.substr(0, string.length - 1);
        },
        extend = function(d, s){
            for(var n in s){
                if(s.hasOwnProperty(n))  d[n] = s[n]
            }
        }


    EJS = function( options ){
        options = typeof options == "string" ? {view: options} : options
        this.set_options(options);
        if(options.precompiled){
            this.template = {};
            this.template.process = options.precompiled;
            EJS.update(this.name, this);
            return;
        }
        if(options.element)
        {
            if(typeof options.element == 'string'){
                var name = options.element
                options.element = document.getElementById(  options.element )
                if(options.element == null) throw name+'does not exist!'
            }
            if(options.element.value){
                this.text = options.element.value
            }else{
                this.text = options.element.innerHTML
            }
            this.name = options.element.id
            this.type = '['
        }else if(options.url){
            options.url = EJS.endExt(options.url, this.extMatch);
            this.name = this.name ? this.name : options.url;
            var url = options.url
            //options.view = options.absolute_url || options.view || options.;
            var template = EJS.get(this.name /*url*/, this.cache);
            if (template) return template;
            if (template == EJS.INVALID_PATH) return null;
            try{
                this.text = EJS.request( url+(this.cache ? '' : '?'+Math.random() ));
            }catch(e){}

            if(this.text == null){
                throw( {type: 'EJS', message: 'There is no template at '+url}  );
            }
            //this.name = url;
        }
        var template = new EJS.Compiler(this.text, this.type);

        template.compile(options, this.name);


        EJS.update(this.name, this);
        this.template = template;
    };
    /* @Prototype*/
    EJS.prototype = {
        /**
         * Renders an object with extra view helpers attached to the view.
         * @param {Object} object data to be rendered
         * @param {Object} extra_helpers an object with additonal view helpers
         * @return {String} returns the result of the string
         */
        render : function(object, extra_helpers){
            object = object || {};
            this._extra_helpers = extra_helpers;
            var v = new EJS.Helpers(object, extra_helpers || {});
            return this.template.process.call(object, object,v);
        },
        update : function(element, options){
            if(typeof element == 'string'){
                element = document.getElementById(element)
            }
            if(options == null){
                _template = this;
                return function(object){
                    EJS.prototype.update.call(_template, element, object)
                }
            }
            if(typeof options == 'string'){
                params = {}
                params.url = options
                _template = this;
                params.onComplete = function(request){
                    var object = eval( request.responseText )
                    EJS.prototype.update.call(_template, element, object)
                }
                EJS.ajax_request(params)
            }else
            {
                element.innerHTML = this.render(options)
            }
        },
        out : function(){
            return this.template.out;
        },
        /**
         * Sets options on this view to be rendered with.
         * @param {Object} options
         */
        set_options : function(options){
            this.type = options.type || EJS.type;
            this.cache = options.cache != null ? options.cache : EJS.cache;
            this.text = options.text || null;
            this.name =  options.name || null;
            this.ext = options.ext || EJS.ext;
            this.extMatch = new RegExp(this.ext.replace(/\./, '\.'));
        }
    };
    EJS.endExt = function(path, match){
        if(!path) return null;
        match.lastIndex = 0
        return path+ (match.test(path) ? '' : this.ext )
    }




    /* @Static*/
    EJS.Scanner = function(source, left, right) {

        extend(this,
            {left_delimiter: 	left +'%',
                right_delimiter: 	'%'+right,
                double_left: 		left+'%%',
                double_right:  	'%%'+right,
                left_equal: 		left+'%=',
                left_comment: 	left+'%#'})

        this.SplitRegexp = left=='[' ? /(\[%%)|(%%\])|(\[%=)|(\[%#)|(\[%)|(%\]\n)|(%\])|(\n)/ : new RegExp('('+this.double_left+')|(%%'+this.double_right+')|('+this.left_equal+')|('+this.left_comment+')|('+this.left_delimiter+')|('+this.right_delimiter+'\n)|('+this.right_delimiter+')|(\n)') ;

        this.source = source;
        this.stag = null;
        this.lines = 0;
    };

    EJS.Scanner.to_text = function(input){
        if(input == null || input === undefined)
            return '';
        if(input instanceof Date)
            return input.toDateString();
        if(input.toString)
            return input.toString();
        return '';
    };

    EJS.Scanner.prototype = {
        scan: function(block) {
            scanline = this.scanline;
            regex = this.SplitRegexp;
            if (! this.source == '')
            {
                var source_split = rsplit(this.source, /\n/);
                for(var i=0; i<source_split.length; i++) {
                    var item = source_split[i];
                    this.scanline(item, regex, block);
                }
            }
        },
        scanline: function(line, regex, block) {
            this.lines++;
            var line_split = rsplit(line, regex);
            for(var i=0; i<line_split.length; i++) {
                var token = line_split[i];
                if (token != null) {
                    try{
                        block(token, this);
                    }catch(e){
                        throw {type: 'EJS.Scanner', line: this.lines};
                    }
                }
            }
        }
    };


    EJS.Buffer = function(pre_cmd, post_cmd) {
        this.line = new Array();
        this.script = "";
        this.pre_cmd = pre_cmd;
        this.post_cmd = post_cmd;
        for (var i=0; i<this.pre_cmd.length; i++)
        {
            this.push(pre_cmd[i]);
        }
    };
    EJS.Buffer.prototype = {

        push: function(cmd) {
            this.line.push(cmd);
        },

        cr: function() {
            this.script = this.script + this.line.join('; ');
            this.line = new Array();
            this.script = this.script + "\n";
        },

        close: function() {
            if (this.line.length > 0)
            {
                for (var i=0; i<this.post_cmd.length; i++){
                    this.push(pre_cmd[i]);
                }
                this.script = this.script + this.line.join('; ');
                line = null;
            }
        }

    };


    EJS.Compiler = function(source, left) {
        this.pre_cmd = ['var ___ViewO = [];'];
        this.post_cmd = new Array();
        this.source = ' ';
        if (source != null)
        {
            if (typeof source == 'string')
            {
                source = source.replace(/\r\n/g, "\n");
                source = source.replace(/\r/g,   "\n");
                this.source = source;
            }else if (source.innerHTML){
                this.source = source.innerHTML;
            }
            if (typeof this.source != 'string'){
                this.source = "";
            }
        }
        left = left || '<';
        var right = '>';
        switch(left) {
            case '[':
                right = ']';
                break;
            case '<':
                break;
            default:
                throw left+' is not a supported deliminator';
                break;
        }
        this.scanner = new EJS.Scanner(this.source, left, right);
        this.out = '';
    };
    EJS.Compiler.prototype = {
        compile: function(options, name) {
            options = options || {};
            this.out = '';
            var put_cmd = "___ViewO.push(";
            var insert_cmd = put_cmd;
            var buff = new EJS.Buffer(this.pre_cmd, this.post_cmd);
            var content = '';
            var clean = function(content)
            {
                content = content.replace(/\\/g, '\\\\');
                content = content.replace(/\n/g, '\\n');
                content = content.replace(/"/g,  '\\"');
                return content;
            };
            this.scanner.scan(function(token, scanner) {
                if (scanner.stag == null)
                {
                    switch(token) {
                        case '\n':
                            content = content + "\n";
                            buff.push(put_cmd + '"' + clean(content) + '");');
                            buff.cr();
                            content = '';
                            break;
                        case scanner.left_delimiter:
                        case scanner.left_equal:
                        case scanner.left_comment:
                            scanner.stag = token;
                            if (content.length > 0)
                            {
                                buff.push(put_cmd + '"' + clean(content) + '")');
                            }
                            content = '';
                            break;
                        case scanner.double_left:
                            content = content + scanner.left_delimiter;
                            break;
                        default:
                            content = content + token;
                            break;
                    }
                }
                else {
                    switch(token) {
                        case scanner.right_delimiter:
                            switch(scanner.stag) {
                                case scanner.left_delimiter:
                                    if (content[content.length - 1] == '\n')
                                    {
                                        content = chop(content);
                                        buff.push(content);
                                        buff.cr();
                                    }
                                    else {
                                        buff.push(content);
                                    }
                                    break;
                                case scanner.left_equal:
                                    buff.push(insert_cmd + "(EJS.Scanner.to_text(" + content + ")))");
                                    break;
                            }
                            scanner.stag = null;
                            content = '';
                            break;
                        case scanner.double_right:
                            content = content + scanner.right_delimiter;
                            break;
                        default:
                            content = content + token;
                            break;
                    }
                }
            });
            if (content.length > 0)
            {
                // Chould be content.dump in Ruby
                buff.push(put_cmd + '"' + clean(content) + '")');
            }
            buff.close();
            this.out = buff.script + ";";
            var to_be_evaled = '/*'+name+'*/this.process = function(_CONTEXT,_VIEW) { try { with(_VIEW) { with (_CONTEXT) {'+this.out+" return ___ViewO.join('');}}}catch(e){e.lineNumber=null;throw e;}};";

            try{
                eval(to_be_evaled);
            }catch(e){
                if(typeof JSLINT != 'undefined'){
                    JSLINT(this.out);
                    for(var i = 0; i < JSLINT.errors.length; i++){
                        var error = JSLINT.errors[i];
                        if(error.reason != "Unnecessary semicolon."){
                            error.line++;
                            var e = new Error();
                            e.lineNumber = error.line;
                            e.message = error.reason;
                            if(options.view)
                                e.fileName = options.view;
                            throw e;
                        }
                    }
                }else{
                    throw e;
                }
            }
        }
    };


//type, cache, folder
    /**
     * Sets default options for all views
     * @param {Object} options Set view with the following options
     * <table class="options">
     <tbody><tr><th>Option</th><th>Default</th><th>Description</th></tr>
     <tr>
     <td>type</td>
     <td>'<'</td>
     <td>type of magic tags.  Options are '&lt;' or '['
     </td>
     </tr>
     <tr>
     <td>cache</td>
     <td>true in production mode, false in other modes</td>
     <td>true to cache template.
     </td>
     </tr>
     </tbody></table>
     *
     */
    EJS.config = function(options){
        EJS.cache = options.cache != null ? options.cache : EJS.cache;
        EJS.type = options.type != null ? options.type : EJS.type;
        EJS.ext = options.ext != null ? options.ext : EJS.ext;

        var templates_directory = EJS.templates_directory || {}; //nice and private container
        EJS.templates_directory = templates_directory;
        EJS.get = function(path, cache){
            if(cache == false) return null;
            if(templates_directory[path]) return templates_directory[path];
            return null;
        };

        EJS.update = function(path, template) {
            if(path == null) return;
            templates_directory[path] = template ;
        };

        EJS.INVALID_PATH =  -1;
    };
    EJS.config( {cache: true, type: '<', ext: '.ejs' } );



    /**
     * @constructor
     * By adding functions to EJS.Helpers.prototype, those functions will be available in the
     * views.
     * @init Creates a view helper.  This function is called internally.  You should never call it.
     * @param {Object} data The data passed to the view.  Helpers have access to it through this._data
     */
    EJS.Helpers = function(data, extras){
        this._data = data;
        this._extras = extras;
        extend(this, extras );
    };
    /* @prototype*/
    EJS.Helpers.prototype = {
        /**
         * Renders a new view.  If data is passed in, uses that to render the view.
         * @param {Object} options standard options passed to a new view.
         * @param {optional:Object} data
         * @return {String}
         */
        view: function(options, data, helpers){
            if(!helpers) helpers = this._extras
            if(!data) data = this._data;
            return new EJS(options).render(data, helpers);
        },
        /**
         * For a given value, tries to create a human representation.
         * @param {Object} input the value being converted.
         * @param {Object} null_text what text should be present if input == null or undefined, defaults to ''
         * @return {String}
         */
        to_text: function(input, null_text) {
            if(input == null || input === undefined) return null_text || '';
            if(input instanceof Date) return input.toDateString();
            if(input.toString) return input.toString().replace(/\n/g, '<br />').replace(/''/g, "'");
            return '';
        }
    };
    EJS.newRequest = function(){
        var factories = [function() { return new ActiveXObject("Msxml2.XMLHTTP"); },function() { return new XMLHttpRequest(); },function() { return new ActiveXObject("Microsoft.XMLHTTP"); }];
        for(var i = 0; i < factories.length; i++) {
            try {
                var request = factories[i]();
                if (request != null)  return request;
            }
            catch(e) { continue;}
        }
    }

    EJS.request = function(path){
        var request = new EJS.newRequest()
        request.open("GET", path, false);

        try{request.send(null);}
        catch(e){return null;}

        if ( request.status == 404 || request.status == 2 ||(request.status == 0 && request.responseText == '') ) return null;

        return request.responseText
    }
    EJS.ajax_request = function(params){
        params.method = ( params.method ? params.method : 'GET')

        var request = new EJS.newRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                if(request.status == 200){
                    params.onComplete(request)
                }else
                {
                    params.onComplete(request)
                }
            }
        }
        request.open(params.method, params.url)
        request.send(null)
    }


})();
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/select_dropdown.js
 * Выпадающий список
 */
$(document).ready(function(){
    $('.sel_in').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            option_list = select.find('.sel_list');

        if (option_list.is(':visible')) {
            option_list.slideUp(200);
            select.removeClass('is-opened');
            self.find('.sel_arrow').removeClass('is-active');
        } else {
            if ($('.sel .sel_list:visible').length) {
                $('.sel .sel_list:visible').hide();
                $('.sel .sel_arrow').removeClass('is-active');
            }

            option_list.slideDown(200);
            select.addClass('is-opened');
            self.find('.arrow').addClass('is-active');
        }
    });

    $('.sel_list li').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            title = select.find('.sel_in .sel_title'),
            option = self.html(),
            depends = $(select.data('depends'));

        title.html(option);
        self.closest('.sel').find('input[type=hidden]').val(self.attr('data-value'));
        self.closest('.sel_list').find('li').removeClass('is-active');
        self.addClass('is-active');
        self.closest('.sel_list').slideUp(200);
        self.closest('.sel').removeClass('is-opened');
        self.closest('.sel').find('.sel_arrow').removeClass('is-active');

        depends.val(self.attr('data-value'));
        depends.trigger('keyup');
    });

    $(document).on('click', function (e) {
        if ($('.sel .sel_list:visible').length && !$(e.target).closest('.sel').length) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
            $('.sel .sel_arrow').removeClass('is-active');
        }
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
        }
    });

    $('.sel_list li.is-active').click();
});
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/validator_modal_help.js
 * Валидатор формы отправки сообщений (помощь)
 */
$(document).ready(function(){
    $('#help form').validate({
        rules:{
            'subject': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'name': {
                required: true
            },
            'question': {
                required: true
            }
        },
        messages:{
            'subject': {
                required: "Выберите тему вопроса"
            },
            'email': {
                required: "Введите адрес электронной почты",
                email: "Введите правильный адрес электронной почты"
            },
            'name': {
                required: "Введите ваше имя"
            },
            'question': {
                required: "Задайте ваш вопрос"
            }
        }/*,
        submitHandler: function(form) {
            ajaxSend(form, function(r) {
                if (typeof r.result !== 'undefined' && r.result) {
                    alert('Ваш вопрос отправлен.');
                    $('#help [data-modal-close]').click();
                }
            });
        }*/
    });
});
/**
 * Created by ruslan on 27.05.16.
 * source: /assets/js/src/elements/validator_dev-help__form.js
 * Dependencies: EJS, jQuery.Validate, AjaxSend
 *
 * Валидатор формы отправки сообщений в документации и в моих играх
 */
$(document).ready(function(){
    $('#dev-help__form').validate({
        rules:{
            'subject': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'name': {
                required: true
            },
            'question': {
                required: true
            }
        },
        messages:{
            'subject': {
                required: "Выберите тему вопроса"
            },
            'email': {
                required: "Введите адрес электронной почты",
                email: "Введите правильный адрес электронной почты"
            },
            'name': {
                required: "Введите ваше имя"
            },
            'question': {
                required: "Задайте ваш вопрос"
            }
        }
    });
});
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/function_ajaxSend.js
 * Отправка формы ajax-запросом. Чаще всего используется в валидаторах форм.
 */
var ajaxSend = function(form, callback) {
    if (typeof ExeRu !== 'undefined' && typeof ExeRu.inProgress !=='undefined') {
        var inProgress = ExeRu.inProgress;
    } else {
        var inProgress = {
            on: function(){ $('#inProgressShadow').show(); },
            off: function(){ $('#inProgressShadow').hide(); }
        }
    }

    inProgress.on();
    $.post($(form).attr('action'), $(form).serialize(), function(r) {
        if (typeof r.errors !== 'undefined') {
            for(var i in r.errors) {
                alert(r.errors[i]);
            }
            inProgress.off();
        }  else if (typeof callback == 'function') {
            inProgress.off();
            callback(r);
        } else if (typeof r.location !== 'undefined' && r.location != location.pathname) {
            location.href = r.location;
        } else if (typeof r.location !== 'undefined' && r.location == location.pathname) {
            inProgress.off();
        }
    });
};
/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_isImage.js
 * Проверяет, что тип файл является типом изображений.
 */
if (typeof isImage == 'undefined') {
    var isImage = function(file) {
        return file.type == 'image/gif' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/png';
    };
}
/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_fileSizeMore.jss
 * Проверяет, является ли файл файлом большего размера, чем требуется.
 */
if (typeof fileSizeMore == 'undefined') {
    var fileSizeMore = function (file, size) {
        return file.size - size > 0;
    }
}
function stampToDate(stamp, format) {
    var date = new Date();
    date.setTime(stamp * 1000);

    out = {
        d : date.getDate(),
        m : date.getMonth()+ 1,
        y : date.getYear() - 100,
        Y : date.getFullYear(),
        H : date.getHours(),
        i : date.getMinutes()
    };

    for(var k in out)
        format = format.replace('%' + k, out[k] < 10 ? "0" + out[k] : out[k]);

    return format;
}

$(document).ready(function () {
    // При получении ответа 403 - редирект на корень
    $.ajaxSetup({
        statusCode: {
            403: function() {
                window.location.href = '/';
            }
        }
    });

	var onWindowResizeLoad = function () {
		var _f = $('.dev-footer'),
			_m = $('.minHeight');

		_m.css('min-height', 'auto');

		var contentHeight = _f.offset().top + _f.outerHeight(),
			windowHeight  = $(window).height();

		if (windowHeight > contentHeight)
			_m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
	};

    onWindowResizeLoad();

	$(window).on('resize load', onWindowResizeLoad);


    $("#dev-help__form").on('submit', function(){
        if ($('input.error', this).length > 0) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        ajaxSend(this, function(r) {
            var version = (typeof ExeRu !== 'undefined' && typeof ExeRu.version !== 'undefined') ? '?' + ExeRu.version : '',
                html = new EJS({url:'/assets/js/dev/views/help_ok.ejs' + version}).render(r);

            $('#dev-help__form').replaceWith(html);
        });

        return false;
    });

    // Форма помощи у авторизованного пользователя
    $('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        ajaxSend($('#help form'), function(r) {
            if (typeof r.result !== 'undefined' && r.result) {
                alert('Ваш вопрос отправлен.');
                $("#help form input").attr('readonly', false);
                $('#help [data-modal-close]').click();
            }
        });

        return false;
    });

	$('.dev-charts__tabs-list-item').on('click', function(e) {
        var parent = $(this).parent(),
            id = $(this).data('tab');

		$(this).addClass('active');
        parent.find('a.active').not(this).removeClass('active');

        parent.find('span').removeClass('active');
        parent.find('span[data-tab="' + id + '"]').addClass('active');

        $(this).closest('.dev-charts__tabs').find('.dev-charts__tabs-box.shown').removeClass('shown').parent().find('*[data-tab=' + id + ']').addClass('shown');

		return false;
	});

	$('#f1, #f2, #f3, #f4, #f5, #f6, #f7').on('change', function(e) {
        if (!isImage(this.files[0])) {
            alert('Необходимо выбрать изображение');
            return false;
        }

        if (fileSizeMore(this.files[0], 10485760)) {
            alert('Слишкой большой файл, не более 10 мб.');
            return false;
        }

        ExeRu.inProgress.on();

        var form = $(this).closest('form')[0],
		    formData = new FormData(form),
            xhr = new XMLHttpRequest();

        xhr.open('POST', form.action);

        xhr.onload = function(e) {
            var r = JSON.parse(e.target.responseText);
            if (typeof r.location !== 'undefined') {
                location.href = r.location;
            } else if (typeof r.response.error !== 'undefined') {
                $(':file', form).val('');
                alert(r.response.error);
            } else {
                $('img', form).attr('src', imageHost + r.response.filename);
            }
            console.debug(r);
            ExeRu.inProgress.off();
        };

        xhr.upload.onprogress = function (e) {
            console.debug(parseInt(e.loaded / e.total * 100));
        };

        xhr.send(formData);
	});

    ExeRu.modal.init(document);

    (function(){
        var handlerPopState = function(e) {
            // Восстанавливаем состояние окна с закрытыми модальными окнами
            $("body").css({'overflow-y':'auto', 'padding-right': '0'});
            $('iframe').css('visibility', 'visible');
            $('.modal--open').each(function(){
                $(ExeRu.modal.dialogs[this.id].el).trigger('closeDialog');
                ExeRu.modal.dialogs[this.id].isOpen = false;
                $('[data-modal="' + this.id + '"]').removeClass('active');
                $(this).removeClass('modal--open');
            });

            var path = /^\/(news|dialogs|friends|settings|help)(?:\/*)$/.exec(window.location.pathname);
            if (path != null) {
                var modalId = path[1] == 'help' ? path[1] : path[1] + '-modal';

                ExeRu.modal.dialogs[modalId].el.setAttribute('no-push-history', true);
                ExeRu.modal.dialogs[modalId].toggle();
                // Включаем первую табу
                $(ExeRu.modal.dialogs[modalId].el).find('.modal-tabs__list a:first').click();
                // А для фильтров - первый чекбох
                $(ExeRu.modal.dialogs[modalId].el).find('.checkbox-list input:first').click();
            } else {
                var id = /^\/id(\d+)(?:\/*)$/.exec(window.location.pathname);
                if (id != null) {
                    $('#forOpenByHash').attr({'data-uid':id[1], 'no-push-history':true})[0].click();
                }
            }
        };

        $(window).on('popstate', handlerPopState);

        handlerPopState(null);
    })();

    $('a.btn-main', document).on('click', ExeRu.call);

    $('#dev_news_add_file').on('change', ExeRu.dev.attachFile);

    $('.dev-news__uploaded-delete').on('click', ExeRu.dev.detachFile);


    $('.dev-news__item-tools-delete').on('click', function() {
            $(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);
            return false;
    });
    $('.dev-news__item-tools-delete-cancel').on('click', function() {
        $(this).parents('.dev-news__item-deleteBox').hide();
        return false;
    });

    $('.dev-billing .dev-billing__history-search input').on('keyup keydown focus blur', function(e) {
        if (e.type == 'keydown' && e.which == 13) {
            $('.dev-billing .dev-billing__history-search-do').trigger('click');
            return false;
        }

        var text = $.trim(this.value);
        if (text.length >= 5) {
            $('.dev-billing .dev-billing__history-search-error').hide();
        }

        if (text.length > 0) {
            $('.dev-billing .dev-billing__history-search-reset').show();
        } else {
            $('.dev-billing .dev-billing__history-search-reset').hide();
            $('.dev-billing .dev-billing__history-not-found').hide();
            $('.dev-billing .dev-billing__history-table').show();
            $('.dev-billing .dev-billing__history-table tbody tr').show();
        }
    });

    $('.dev-billing .dev-billing__history-search-reset').on('click', function() {
        $('.dev-billing .dev-billing__history-search input')
            .val('')
            .trigger('blur');
        return false;
    });

    $('.dev-billing .dev-billing__history-search-do').on('click', function() {
        var input = $('.dev-billing .dev-billing__history-search input'),
            text = input.val().trim();

        if (text.length < 5) {
            $('.dev-billing .dev-billing__history-search-error').show();
            input.val(text);
            input.focus();
            return false;
        }

        var table = $('.dev-billing .dev-billing__history-table'),
            result = table.find('tbody tr[data-uid="' + text + '"]'),
            noResult = $('.dev-billing .dev-billing__history-not-found');

        if (result.length == 0) {
            table.hide();
            noResult.show();
        } else {
            noResult.hide();
            table.show();
            table.find('tbody tr').hide();
            result.show();
        }

        return false;
    });

    $('.dev-news .dev-billing__history-search input').on('keyup keydown focus blur', function(e) {
        if (e.type == 'keydown' && e.which == 13) {
            $('.dev-billing__history-search-do').trigger('click');
            return false;
        }

        var text = $.trim(this.value);
        if (text.length >= 5) {
            $('.dev-billing__history-search-error').hide();
        }

        if (text.length > 0) {
            $('.dev-billing__history-search-reset').show();
        } else {
            $('.dev-billing__history-search-reset').hide();
            $('.dev-news__search-box').hide();
            $('.dev-news__history-box').show();
        }
    });

    $('.dev-news .dev-billing__history-search-reset').on('click', function() {
        $('.dev-billing__history-search input')
            .val('')
            .trigger('blur');
        $('.dev-news__search-box').hide();
        $('.dev-news__history-box').show();
        $('.dev-billing__history-search-error').hide();
    });

    $('.dev-news .dev-billing__history-search-do').on('click', ExeRu.dev.feedsSearch);

    $('.ui.dropdown').dropdown();

    $('.dev-roles input:checkbox').on('change', ExeRu.dev.changeAccess);


    jQuery.datetimepicker.setLocale('ru');
    jQuery('#datetimepicker').datetimepicker({
        format: 'd.m.Y H:i',
        lang:'ru',
        onChangeDateTime:function(dp,$input){
            if (dp) {
                $('#addNews form [name="time"]').val(Math.floor(dp.getTime()/1000));
            } else {
                $('#addNews form [name="time"]').val('');
            }
        }
    });


});

var ExeRu = {
	version: 75,
	call: function(e) {
		var module = this.getAttribute('data-module');
		var method = this.getAttribute('data-method');
		if (module !== null && method !== null) {
			ExeRu[module][method](this);
		}
		return false;
	}
};

ExeRu.dev = {
    urlFeedDelete: '/deleteFeed',
    urlFeedEdit: '/getFeed',
    urlFeedsGetNext: '/devAjax/feedsGetNext',
    urlFeedsSearch: '/devAjax/feedsSearch',
    urlRolesChangeAccess: '/roles/changeAccess',

    preview: function(e) {
        var form = $(e).closest('form');
        if (form.valid()) {
            $('[data-modal="preview"]')[0].click();
        }
        return false;
    },
    prepare: function() {
        var form = $('#addNews form'),
            data = {};
        data['game_title'] = form.find('[name="game_title"]').val();
        data['game_gid'] = form.find('[name="game_gid"]').val();
        data['game_image_150x150'] = form.find('[name="game_image_150x150"]').val();
        data['game_applink'] = form.find('[name="game_applink"]').val();

        data['title'] = form.find('[name="title"]').val();
        data['content'] = form.find('[name="content"]').val();
        data['params'] = encodeURIComponent(form.find('[name="params"]').val());
        data['image_url'] = document.getElementById('uploaded_file').getAttribute('src');
        data['timestamp'] = form.find('[name="time"]').val();

        if (data['timestamp'] == '') {
            var date = new Date();
        } else {
            var date = new Date(parseInt(data['timestamp']) * 1000);
        }

        out = {
            d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
            m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
            Y : date.getFullYear().toString()
        };
        data['date'] = out.d + '.' + out.m + '.' + out.Y[2] + out.Y[3];

        var html = new EJS({url:'/assets/js/dev/views/newsPreview.ejs?' + ExeRu.version}).render(data);
        $('#preview .dev-dialog__preview').html(html);

        return false;
    },
    attachFile: function() {
        var fileType = this.files[0].type,
            overhead = this.files[0].size - 10485760;

        if (fileType != 'image/gif' && fileType != 'image/jpg' && fileType != 'image/jpeg' && fileType != 'image/png') {
            alert('Необходимо выбрать изображение');
            return false;
        }

        if (overhead > 0) {
            alert('Слишкой большой файл, не более 10 мб.');
            return false;
        }

        var reader = new FileReader();
        reader.onload = function (e) {
            var img = document.getElementById("uploaded_file");
            img.setAttribute("src", e.target.result);

            $('#addNews form [name="file_state"]').val('');

            $('#addNews .dev-news__file').hide();
            $('#addNews .dev-news__uploaded').show();
         };

         reader.readAsDataURL(this.files[0]);
    },
    detachFile: function() {
        $('#addNews .dev-news__uploaded').hide();
        $('#addNews .dev-news__file').show();

        var img = document.getElementById("uploaded_file");
        img.setAttribute("src", '');

        $('#addNews form [name="file_state"]').val('detach');

        return false;
    },
    publish: function() {
        var form = $('#addNews form');
        if (form.valid()) {
            ExeRu.inProgress.on();
            form.submit();
        }
        return false;
    },
    create: function() {
        $('#datetimepicker').parent().show();
        var form = $('#addNews form');
        form.find('[name="fid"]').val('');
        form.find('[name="time"]').val('');
        form.find('[name="title"]').val('');
        form.find('[name="content"]').val('');
        form.find('[name="params"]').val('');
        $('#dev_news_add_file').val('');
        $('#uploaded_file').attr('src', '');

        $('#addNews .dev-dialog__header-title').html('Создание новости');
        $('#addNews .dev-news__uploaded').hide();
        $('#addNews .dev-news__file').show();
        $('#addNews .dev-dialog__footer-main, #preview .dev-dialog__footer-main').html('Опубликовать');

        $('[data-modal="addNews"]')[0].click();
        return false;
    },
    edit: function(eventElement) {
        var params = {
            fid: eventElement.getAttribute('data-fid'),
            gid: eventElement.getAttribute('data-gid'),
            appointed: eventElement.hasAttribute('data-type') && eventElement.getAttribute('data-type') == 'appointed'
        };

        if (params.appointed) {
            $('#datetimepicker').parent().show();
        } else {
            $('#datetimepicker').parent().hide();
        }

        ExeRu.inProgress.on();
        ExeRu.request.get(ExeRu.dev.urlFeedEdit, params, function(r) {
            var feed = r.response.result,
                form = $('#addNews form');

            form.find('[name="fid"]').val(feed.fid);
            form.find('[name="time"]').val(feed.time);
            form.find('[name="title"]').val(feed.title === null ? '' : feed.title);
            form.find('[name="content"]').val(feed.content);
            form.find('[name="params"]').val(decodeURIComponent(feed.params === null ? '' : feed.params));

            if (typeof feed.appointed !== 'undefined') {
                form.find('#datetimepicker').val(feed.appointed);
            }

            $('#addNews .dev-dialog__header-title').html('Редактирование новости');
            $('#addNews .dev-dialog__footer-main, #preview .dev-dialog__footer-main').html('Сохранить');

            $('#dev_news_add_file').val('');

            if (feed.image_url === null) {
                $('#uploaded_file').attr('src', '');
                $('#addNews .dev-news__uploaded').hide();
                $('#addNews .dev-news__file').show();
            } else {
                $('#uploaded_file').attr('src', imageHost + feed.image_url);
                $('#addNews .dev-news__file').hide();
                $('#addNews .dev-news__uploaded').show();
            }

            $('[data-modal="addNews"]')[0].click();

            ExeRu.inProgress.off();
        });


        return false;
    },
    delete: function(eventElement) {
        var params = {
            fid: eventElement.getAttribute('data-fid'),
            gid: eventElement.getAttribute('data-gid'),
            appointed: eventElement.hasAttribute('data-type') && eventElement.getAttribute('data-type') == 'appointed'
        };

        ExeRu.inProgress.on();
        ExeRu.request.post(ExeRu.dev.urlFeedDelete, params, function(r) {
            if (typeof(r.response.result.fid) !== 'undefined') {
                if (typeof(r.response.result.appointed) !== 'undefined' && r.response.result.appointed) {
                    var news = $('.dev-news__item[data-fid="' + r.response.result.fid + '"][data-type="appointed"]');
                    if (news.parent().find('div[data-type="appointed"]').length == 1) {
                        news.parents('.dev-news').find('.dev-game__head.appointed').remove();
                    }
                    news.remove();
                } else {
                    $('[data-fid="' + r.response.result.fid + '"].dev-news__item').remove();
                    // Меняем смещение кнопке показать ещё
                    var offset = parseInt($('.dev-billing__history-more [data-offset]').attr('data-offset'));
                    if (offset > 0) {
                        $('.dev-billing__history-more [data-offset]').attr('data-offset', (offset - 1));
                    }
                }
            }
            ExeRu.inProgress.off();
        });

        return false;
    },
    copy: function(eventElement) {
        ExeRu.inProgress.on();
        var form = $(eventElement).closest('form'),
            params = {
                fileIconType: form.find('[name="fileIconType"]').val(),
                action: 'copy'
            };

        ExeRu.request.post(form[0].action, params, function(r) {
            if (typeof r.response.filename !== 'undefined') {
                $('img', form).attr('src', imageHost + r.response.filename);
            }
            ExeRu.inProgress.off();
        });

        return false;
    },
    feedsGetNext: function(eventElement) {
        var params = {
            gid: eventElement.getAttribute('data-gid'),
            offset: eventElement.getAttribute('data-offset')
        };

        ExeRu.request.get(ExeRu.dev.urlFeedsGetNext, params, function(r) {
            var limit = parseInt(eventElement.getAttribute('data-limit'));
            var offset = parseInt(eventElement.getAttribute('data-offset')) + parseInt(r.length);
            eventElement.setAttribute('data-offset', offset);

            if (r.length > 0) {
                var form = $('#addNews form'),
                    data = {};
                data['game_title'] = form.find('[name="game_title"]').val();
                data['game_gid'] = form.find('[name="game_gid"]').val();
                data['game_image_150x150'] = form.find('[name="game_image_150x150"]').val();
                data['game_applink'] = form.find('[name="game_applink"]').val();

                for(var i in r) {
                    data['fid'] = r[i].fid;
                    data['title'] = r[i].title;
                    data['content'] = r[i].content;
                    data['params'] = encodeURIComponent(r[i].params);
                    data['image_url'] = r[i].image_url;
                    data['timestamp'] = r[i].time;

                    var date = new Date(parseInt(data['timestamp']) * 1000);
                    out = {
                        d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
                        m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
                        Y : date.getFullYear().toString()
                    };
                    data['date'] = out.d + '.' + out.m + '.' + out.Y[2] + out.Y[3];

                    var html = $(new EJS({url:'/assets/js/dev/views/onenews.ejs?' + ExeRu.version}).render(data));

                    $('a.btn-main', html).on('click', ExeRu.call);

                    $('.dev-news__item-tools-delete', html).on('click', function() {
                        $(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);
                        return false;
                    });
                    $('.dev-news__item-tools-delete-cancel', html).on('click', function() {
                        $(this).parents('.dev-news__item-deleteBox').hide();
                        return false;
                    });

                    html.appendTo('.dev-news__history-box');
                }
            }

            if (r.length < limit) {
                $('.dev-billing__history-more').hide();
            }
        });
        return false;
    },
    feedsSearch:  function(eventElement) {
        var input = $('.dev-billing__history-search input'),
            text = input.val().trim();

        if (text.length < 5) {
            $('.dev-billing__history-search-error').show();
            input.val(text);
            input.focus();
            return false;
        }

        ExeRu.inProgress.on();

        var params = {
            gid: eventElement.target.getAttribute('data-gid'),
            search: text
        };

        ExeRu.request.get(ExeRu.dev.urlFeedsSearch, params, function(r) {
            if (r.length < 1) {
                var html = $('<div class="not-found">По данному запросу ничего не найдено.</div>');
                $('.dev-news__search-box').html(html);
            } else {
                $('.dev-news__search-box').html('');

                var form = $('#addNews form'),
                    data = {};

                data['game_title'] = form.find('[name="game_title"]').val();
                data['game_gid'] = form.find('[name="game_gid"]').val();
                data['game_image_150x150'] = form.find('[name="game_image_150x150"]').val();
                data['game_applink'] = form.find('[name="game_applink"]').val();

                for(var i in r) {
                    data['fid'] = r[i].fid;
                    data['title'] = r[i].title;
                    data['content'] = r[i].content;
                    data['params'] = encodeURIComponent(r[i].params);
                    data['image_url'] = r[i].image_url;
                    data['timestamp'] = r[i].time;

                    var date = new Date(parseInt(data['timestamp']) * 1000);
                    out = {
                        d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
                        m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
                        Y : date.getFullYear().toString()
                    };
                    data['date'] = out.d + '.' + out.m + '.' + out.Y[2] + out.Y[3];

                    var html = $(new EJS({url:'/assets/js/dev/views/onenews.ejs?' + ExeRu.version}).render(data));

                    $('a.btn-main', html).on('click', ExeRu.call);

                    $('.dev-news__item-tools-delete', html).on('click', function() {
                        $(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);
                        return false;
                    });
                    $('.dev-news__item-tools-delete-cancel', html).on('click', function() {
                        $(this).parents('.dev-news__item-deleteBox').hide();
                        return false;
                    });

                    html.appendTo('.dev-news__search-box');
                }
            }

            $('.dev-news__history-box').hide();
            $('.dev-news__search-box').show();

            ExeRu.inProgress.off();
        });
    },

    prepareAddAdmin: function() {
        $('#addAdmin .dev-dialog__header-title').html('Добавить администратора');
        $('#addAdmin form').attr('action', '/roles/addAdmin');
        ExeRu.dev.resetAddAdmin();

        $('[data-modal="addAdmin"]')[0].click();
    },
    prepareAddTester: function() {
        $('#addAdmin .dev-dialog__header-title').html('Добавить тестировщика');
        $('#addAdmin form').attr('action', '/roles/addTester');
        ExeRu.dev.resetAddAdmin();

        $('[data-modal="addAdmin"]')[0].click();
    },
    resetAddAdmin: function() {
        var root = $('#addAdmin'),
            search = root.find('.dev-billing__history-search button'),
            submit = root.find('.dev-add-game__btns button'),
            input  = root.find('input:text'),
            box    = root.find('.dev-dialog__addAdmin-box'),
            rBox   = root.find('.dev-dialog__addAdmin-result'),
            dBox   = root.find('.dev-dialog__addAdmin-done');

        input.val('').focus();
        dBox.attr('data-uid', '');

        rBox.hide();
        dBox.hide();
        box.show();

        input.off('keyup focus').on('keyup focus', function(){
            rBox.hide();
        });

        // Кнопка поиска
        search.off('click').on('click', function() {
            var text = input.val().trim();
            if (text.length < 3 || (parseInt(text) != text)) {
                input.focus();
                return false;
            }

            ExeRu.inProgress.on();

            // Ищем пользователя
            ExeRu.request.get('/ajax/getInfoAboutUser/', {uid:text}, function(r) {
                if (r == false) {
                    alert('Пользователь не найден');
                    input.focus();
                } else {
                    var html = '<img src="' + imageHost + r.photo[70] + '" alt="" /><div class="dev-roles__list-item-name">' + r.name + ' ' + r.last_name + '</div>';
                    rBox.html(html);

                    rBox.off('click').on('click', function() {
                        box.hide();

                        dBox.attr('data-uid', text);

                        dBox.find('.dev-dialog__addAdmin-done-item-name').html(rBox.find('.dev-roles__list-item-name').html());
                        dBox.show();

                        dBox.find('.dev-dialog__addAdmin-done-item-delete').off('click').on('click', function () {
                            dBox.attr('data-uid', '');

                            dBox.hide();
                            rBox.hide();

                            box.show();
                            input.val('').focus();
                        });
                    });

                    rBox.show();
                }
                ExeRu.inProgress.off();
            });

            return false;
        });

        // Кнопка добавить
        submit.off('click').on('click', function() {
            var uid = dBox.attr('data-uid'),
                gid = dBox.attr('data-gid'),
                action = root.find('form').attr('action');
            if (uid == '' || gid == '' || action == '') {
                input.focus();
                return false;
            }

            ExeRu.inProgress.on();
            ExeRu.request.post(action, {uid:uid, gid:gid}, function(r) {
                if (typeof(r.response.errors) !== 'undefined') {
                    for(var i in r.response.errors) {
                        alert(r.response.errors[i]);
                    }
                    input.val('').focus();
                    dBox.attr('data-uid', '');

                    rBox.hide();
                    dBox.hide();
                    box.show();
                    ExeRu.inProgress.off();
                } else {
                    if (typeof(r.response.location) !== 'undefined') {
                        window.location.href = r.response.location;
                    }
                }
            });

            return false;
        });

    },
    prepareDeleteAdmin: function(eventElement) {
        var uid = $(eventElement).parents('.dev-roles__list-item').attr('data-uid');
        $('#deleteTester').attr({'data-uid':uid, 'data-action':'/roles/deleteAdmin'});
        $('#deleteTester .dev-dialog__header-title').html('Удалить администратора');

        ExeRu.dev.prepareDeleteButtons();

        $('[data-modal="deleteTester"]')[0].click();
    },
    prepareDeleteTester: function(eventElement) {
        var uid = $(eventElement).parents('.dev-roles__list-item').attr('data-uid');
        $('#deleteTester').attr({'data-uid':uid, 'data-action':'/roles/deleteTester'});
        $('#deleteTester .dev-dialog__header-title').html('Удалить тестировщика');

        ExeRu.dev.prepareDeleteButtons();

        $('[data-modal="deleteTester"]')[0].click();
    },
    prepareDeleteButtons: function() {
        var root = $('#deleteTester'),
            uid = root.attr('data-uid'),
            gid = root.attr('data-gid'),
            action = root.attr('data-action'),
            cancel = root.find('.dev-dialog__footer-additional'),
            submit = root.find('.dev-dialog__footer-main');

        cancel.off('click').on('click', function(){
            root.find('[data-modal-close]').click();
            return false;
        });

        submit.off('click').on('click', function() {
            if (uid == '' || gid == '') {
                return false;
            }
            ExeRu.inProgress.on();
            ExeRu.request.post(action, {uid: uid, gid: gid}, function(r) {
                if (typeof(r.response.errors) !== 'undefined') {
                    for(var i in r.response.errors) {
                        alert(r.response.errors[i]);
                    }
                } else {
                    if (typeof(r.response.location) !== 'undefined') {
                        window.location.href = r.response.location;
                    }
                }
            });

            return false;
        });
    },
    changeAccess: function(eventElement) {
        var checkbox = $(eventElement.target),
            uid = checkbox.parents('.dev-roles__list-item').attr('data-uid'),
            gid = checkbox.parents('.dev-roles__list-item').attr('data-gid'),
            checked = eventElement.target.checked;

        ExeRu.inProgress.on();
        ExeRu.request.post(ExeRu.dev.urlRolesChangeAccess, {uid:uid, gid:gid, checked:checked}, function(r){
            if (typeof(r.response.errors) !== 'undefined') {
                for(var i in r.response.errors) {
                    alert(r.response.errors[i]);
                }
            } else {
                if (typeof(r.response.location) !== 'undefined') {
                    window.location.href = r.response.location;
                }
            }
        });
    },
    helpReset: function() {
        $('#help form')[0].reset();
        $('#help .sel_title').html('Выберите тему обращения');
    },
    cashout: function(eventElement) {
        var root = $('#withdraw');
        $('.sum-info', root).attr('data-max-sum', eventElement.getAttribute('data-sum'));
        $('.sum-info', root).html(eventElement.getAttribute('data-sum-text'));
        $('[name="gid"]').val(eventElement.getAttribute('data-gid'));

        return false;
    },
    cashoutDo: function(eventElement) {
        var root = $('#withdraw'),
            maxSum = $('.sum-info', root).attr('data-max-sum'),
            sum = $('[name="cashout_sum"]', root).val().trim();

        if (sum == 0 || (maxSum - sum) < 0) {
            return false;
        }

        ajaxSend($('form', root)[0]);

        return false;
    },
    showTransaction: function(eventElement) {
        var root = $('#transaction');

        $('.transaction-id', root).html(eventElement.getAttribute('data-transaction-id'));
        $('.user-name', root).html('<a href="' + eventElement.getAttribute('data-user-href') + '" target="_blank">' + eventElement.getAttribute('data-user-name') + '</a>');
        $('.contract-number', root).html(eventElement.getAttribute('data-contract-number'));
        $('.withdraw-sum', root).html(eventElement.getAttribute('data-withdraw-sum'));
        $('.transaction-comment', root).html(eventElement.getAttribute('data-comment'));
        $('.transaction-date', root).html(eventElement.getAttribute('data-transaction-date'));
    }
};

/**
 * Created by ruslan on 03.02.16.
 */
$(document).ready(function() {
    $('#c1').on('change', function() {
        $('.dev-add-game form [name="agree_dub"]').trigger('click');
    });

    $('form#statusControl button').on('click', function(e) {
        if (e.currentTarget.value != 'reject' && e.currentTarget.value != 'accept') {
            return false;
        }

        var form = $('form#statusControl');

        if (e.currentTarget.value == 'reject') {
            var comment = $('[name="comment"]', form).val().trim();
            if (comment.length < 3) {
                $('#reject-comment-error').show();
                $('[name="comment"]', form).focus();
                $('[name="comment"]', form).one('change keypress', function() {
                    $('#reject-comment-error').hide();
                    $('[name="comment"]', form).unbind();
                });
                return false;
            }
        }

        $('[name="action"]').val(e.currentTarget.value);

        ajaxSend(form);
    });
    // Дополнительные методы валидации
    $.validator.addMethod(
        "minLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length < param) {
                return false;
            } else {
                return true;
            }
        }, "Не меньше 2-х символов");
    $(".dev-add-game form, .dev-game__info form:first").validate({
        rules: {
            'title': {
                required: true,
                minLen: 2
            },
            'description': {
                required: true,
                minLen: 2
            },
            'action_dub': {
                required: true
            },
            'agree_dub': {
                required: true
            }
        },
        messages: {
            'title': {
                required: "Добавьте название игры",
                minLen: "Добавьте название игры"
            },
            'description': {
                required: "Добавьте краткое описание игры",
                minLen: "Добавьте краткое описание игры"
            },
            'action_dub': {
                required: "Необходимо выбрать жанр игры"
            },
            'agree_dub': {
                required: "Необходимо подтвердить согласие с правилами"
            }
        },
        submitHandler: function(form) {
            ajaxSend(form);
        }
    });

    $(".dev-game__settings_p1").validate({
        rules: {
            'api_key': {
                minlength: 16,
                required: true
            }
        },
        messages: {
            'api_key': {
                minlength: "Длина ключа не менее 16 символов",
                required: "Необходимо придумать секретный ключ"
            }
        },
        submitHandler: function(form) {
            ajaxSend(form);
        }
    });

    $(".dev-game__settings_p2").validate({
        rules: {
            'height': {
                min: 250,
                max: 4000,
                required: true
            },
            'http': {
                url: true,
                required: true
            },
            'https': {
                url: true,
                required: true
            },
            'callback_url': {
                url: true,
                required: true
            }
        },
        messages: {
            'height': {
                min: "Минимальная высота 250px",
                max: "Максимальная высота 4000px",
                required: "Укажите высоту iframe"
            },
            'http': {
                url: "Укажите правильный http адрес фрейма с приложением",
                required: "Укажите http адрес фрейма"
            },
            'https': {
                url: "Укажите правильный https адрес фрейма с приложением",
                required: "Укажите https адрес фрейма"
            },
            'callback_url': {
                url: "Укажите правильный адрес для обратных запросов",
                required: "Укажите http адрес обратных для обратных вызовов"
            }
        },
        submitHandler: function(form) {
            ajaxSend(form);
        }
    });

    $("#addNews form").validate({
        rules: {
            'content': {
                required: true
            }
        },
        messages: {
            'content': {
                required: "Текст новости - обязательное поле."
            }
        }
    });
});

/*var ExeRu = ExeRu || {};*/
ExeRu.inProgress = {
    count: 0,
    on: function() {
        ++ExeRu.inProgress.count;
        if (ExeRu.inProgress.count != 1) {
            return false;
        }
        $('#inProgressShadow').show();
    },
    off: function() {
        --ExeRu.inProgress.count;
        if (ExeRu.inProgress.count != 0) {
            return false;
        }
        $('#inProgressShadow').hide();
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.modal = {
    scrollWidth: null,
    dialogs: {},
    init: function(container) {
        if (ExeRu.modal.scrollWidth == null) {
            var body = $('body'),
                overflowY = body.css('overflow-y');

            ExeRu.modal.scrollWidth = -body[0].offsetWidth;
            body.css('overflow-y', 'hidden');
            ExeRu.modal.scrollWidth += body[0].offsetWidth;

            body.css('overflow-y', overflowY);
        }
        // Обработчики открытия/закрытия диалоговых окон
        var dlgtriggers = $('[data-modal]', container);
        if (dlgtriggers.length > 0) {
            for(i=0; i<dlgtriggers.length; i++) {
                id = dlgtriggers[i].getAttribute('data-modal');
                if (ExeRu.modal.dialogs[id] === undefined) {
                    ExeRu.modal.dialogs[id] = new DialogFx(document.getElementById(id),
                        {
                            onOpenDialog : function(dialogObject) {
                                $("body").css({'overflow-y':'hidden', 'padding-right': ExeRu.modal.scrollWidth + 'px'});
                                if (dialogObject.el.hasAttribute('no-push-history')) {
                                    dialogObject.el.removeAttribute('no-push-history');
                                } else {
                                    if (dialogObject.el.id == 'news-modal') {
                                        history.pushState({}, '', '/news');
                                    } else if (dialogObject.el.id == 'dialogs-modal') {
                                        history.pushState({}, '', '/dialogs');
                                    } else if (dialogObject.el.id == 'friends-modal') {
                                        history.pushState({}, '', '/friends');
                                    } else if (dialogObject.el.id == 'settings-modal') {
                                        history.pushState({}, '', '/settings');
                                    } else if (dialogObject.el.id == 'help') {
                                        history.pushState({}, '', '/help');
                                    }
                                }

                                $(dialogObject.el).trigger('openDialog');
                                $('iframe').css('visibility', 'hidden');

                                var eventElement = dialogObject.options.event;
                                if (typeof(eventElement) == 'object') {
                                    var module = eventElement.getAttribute('data-module');
                                    var method = eventElement.getAttribute('data-method');

                                    if (module !== null && method !== null) {
                                        ExeRu[module][method](eventElement, dialogObject.el);
                                    }
                                }

                                $('a.icon__games.active').removeClass('active');

                                return false;
                            },
                            onCloseDialog : function(dialogObject) {
                                if (pageUrl != '/') {
                                    $('a.icon__games').addClass('active');
                                }

                                $('iframe').css('visibility', 'visible');
                                history.pushState({}, '', pageUrl);
                                $(dialogObject.el).trigger('closeDialog');
                                $(dialogObject.el).one('DialogFX_endAnimationClose', function(){
                                    $("body").css({'overflow-y':'auto', 'padding-right': '0'});
                                });

                                return false;
                            }
                        }
                    );
                }
                dlgtriggers[i].addEventListener('click', function(e) {
                    e.preventDefault();

                    currentName = this.getAttribute('data-modal');
                    $('.modal--open').each(function(){
                        if (this.id != currentName) {
                            $(ExeRu.modal.dialogs[this.id].el).trigger('closeDialog');
                            ExeRu.modal.dialogs[this.id].isOpen = false;
                            $('[data-modal="' + this.id + '"]').removeClass('active');
                            $(this).removeClass('modal--open');
                        }
                    });

                    ExeRu.modal.dialogs[currentName].options.event = this;
                    ExeRu.modal.dialogs[currentName].toggle();
                    if (currentName == 'login-modal') {
                        $('a.icon').removeClass('active');
                    }

                    return false;
                });
            }
        }
        // Обработка переключения по табам для диалоговых окон
        var tabTriggers = $('[data-tab-target]', container);
        if (tabTriggers.length > 0) {
            for(i=0; i<tabTriggers.length; i++) {
                $(tabTriggers[i]).on('click', function() {
                    if ($(this).hasClass('icon')) {
                        var parent = $('#' + this.getAttribute('data-modal') + ' .modal'),
                            currentElement = parent.find('[data-tab-target="' + this.getAttribute('data-tab-target') + '"]')[0],
                            canExec = $(this).hasClass('active');
                    } else {
                        var currentElement = this,
                            parent = $(currentElement).parents('.modal'),
                            canExec = true;
                    }

                    var tabControl = parent.find('.tabControl')[0];
                    if (tabControl.hasAttribute('data-tab-active')) {
                        $(tabControl).removeClass(tabControl.getAttribute('data-tab-active'));
                        parent.find('[data-tab-target=' + tabControl.getAttribute('data-tab-active') + ']').removeClass('active');
                    }
                    $(currentElement).addClass('active');
                    tabControl.setAttribute('data-tab-active', currentElement.getAttribute('data-tab-target'));
                    $(tabControl).addClass(currentElement.getAttribute('data-tab-target'));

                    if (currentElement.hasAttribute('data-module') && currentElement.hasAttribute('data-method') && canExec) {
                        var module = currentElement.getAttribute('data-module');
                        var method = currentElement.getAttribute('data-method');
                        if (module !== null && method !== null) {
                            ExeRu[module][method](currentElement);
                        }
                    }

                    $('#profile_main, #profile_access, form.sign-form', tabControl).each(function() {
                        $('label.error').hide();
                        this.reset();
                    });

                    return false;
                });

                if ($(tabTriggers[i]).hasClass('active')) {
                    $(tabTriggers[i]).click();
                }
            }
        }
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.request = {
    get: function(url, params, callback) {
        params.rnd = Math.random();
        $.get(url, params, function(r) {
            if (typeof(callback) == 'function') {
                callback(r);
            }
        });
    },
    post: function(url, params, callback) {
        $.post(url, params, function(r) {
            if (typeof(callback) == 'function') {
                callback(r);
            }
        });
    }
};