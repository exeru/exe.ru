$(document).ready(function(){
    // При получении ответа 403 - редирект на корень
/*    $.ajaxSetup({
        statusCode: {
            403: function() {
                if (forbiddenRedirect) {
                    window.location.href = '/';
                }
            }
        }
    });*/

    var onWindowResizeLoad = function () {
        var _f = $('.footer'),
            _m = $('.minHeight');

        _m.css('min-height', 'auto');

        var contentHeight = _f.offset().top + _f.outerHeight(),
            windowHeight  = $(window).height();

        if (windowHeight > contentHeight)
            _m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
    };

    onWindowResizeLoad();

    $(window).on('resize', onWindowResizeLoad);

    ExeRu.events.params.sid = window.sid;

    ExeRu.init.modules(document);
    ExeRu.init.buttons(document);

    (function(){
        var handlerPopState = function(e) {
            // Восстанавливаем состояние окна с закрытыми модальными окнами
            $("body").css({'overflow-y':'auto', 'padding-right': '0'});
            $('iframe').css('visibility', 'visible');
            $('.modal--open').each(function(){
                $(ExeRu.modal.dialogs[this.id].el).trigger('closeDialog');
                ExeRu.modal.dialogs[this.id].isOpen = false;
                $('[data-modal="' + this.id + '"]').removeClass('active');
                $(this).removeClass('modal--open');
            });

            var path = /^\/(news|dialogs|friends|settings|help)(?:\/*)$/.exec(window.location.pathname);
            if (path != null) {
                var modalId = path[1] == 'help' ? path[1] : path[1] + '-modal';

                ExeRu.modal.dialogs[modalId].el.setAttribute('no-push-history', true);
                ExeRu.modal.dialogs[modalId].toggle();
                // Включаем первую табу
                $(ExeRu.modal.dialogs[modalId].el).find('.modal-tabs__list a:first').click();
                // А для фильтров - первый чекбох
                $(ExeRu.modal.dialogs[modalId].el).find('.checkbox-list input:first').click();
            } else {
                var id = /^\/id(\d+)(?:\/*)$/.exec(window.location.pathname);
                if (id != null) {
                    if (uid > 0) {
                        $('#forOpenByHash').attr({'data-uid':id[1], 'no-push-history':true})[0].click();
                    } else {
                        var oldValue = $('#login-modal [name="target_url"]:first').val();
                        $('#login-modal [name="target_url"]').val(id[0]);
                        $(".sign-social-list a"). attr('data-href', id[0]);
                        $('[data-modal="login-modal"]')[0].click();
                        $('#login-modal').on('closeDialog', function() {
                            $('#login-modal [name="target_url"]').val(oldValue);
                            $(".sign-social-list a"). attr('data-href', oldValue);
                        });
                    }
                }
            }
        };

        $(window).on('popstate', handlerPopState);

        handlerPopState(null);
    })();

    // Форма помощи у авторизованного пользователя
    $('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        ajaxSend($('#help form'), function(r) {
            if (typeof r.result !== 'undefined' && r.result) {
                alert('Ваш вопрос отправлен.');
                $('#help [data-modal-close]').click();
                $('#help .sel_title').html('Выберите тему обращения');
                $("#help form input").attr('readonly', false);
                $("#help form")[0].reset();
            }
        });

        return false;
    });

    $('#blacklist--search_user .modal__search--submit-wrap').on('click', function(e) {
        e.preventDefault();
        var text = $('#blacklist--search_user input:text').val();
        if (text.length < 3) {
            return false;
        }
        ExeRu.inProgress.on();
        ExeRu.request.get('/ajax/search_users/', $('#blacklist--search_user').serialize(), function(r) {
            if (r.result.length == 0) {
                var html = new EJS({url:'/assets/js/views/blackListNotFound.ejs?' + ExeRu.version}).render();
                $('#blacklist-wrap').html(html);
            } else {
                r.source = 'search';
                var html = new EJS({url:'/assets/js/views/blackList.ejs?' + ExeRu.version}).render(r);
                $('#blacklist-wrap').html(html);
                ExeRu.modal.init($('#blacklist-wrap'));
                ExeRu.init.buttons($('#blacklist-wrap'));
            }
            ExeRu.inProgress.off();
        });
    });

    $('.profile-change-photo .upload-photo-input').on('change', function() {

        if (!isImage(this.files[0])) {
            alert('Необходимо выбрать изображение');
            return false;
        }

        if (fileSizeMore(this.files[0], 10485760)) {
            alert('Слишкой большой файл, не более 10 мб.');
            return false;
        }

        ExeRu.inProgress.on();

        $('#uploadPhotoForm').submit();
        $('#uploadPhotoForm .upload-photo-input').val('');
    });

    // Дополнительные методы валидации
    $.validator.addMethod(
        "minLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length < param) {
                return false;
            } else {
                return true;
            }
        }, "Не меньше 2-х символов");
    $.validator.addMethod(
        "maxLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length > param) {
                return false;
            } else {
                return true;
            }
        }, "Не больше 16 символов");
    $.validator.addMethod(
        "customDate",
        function (value, element) {
            var form = $(element).closest('form'),
                bday = parseInt(form.find('input[name="bday"]').val()),
                bmonth = parseInt(form.find('input[name="bmonth"]').val()),
                byear = parseInt(form.find('input[name="byear"]').val());

            if (bday > 0) {
                if ((bmonth == 4 || bmonth == 6 || bmonth == 9 || bmonth == 11) && bmonth == 31) {
                    return false;
                }

                if (bmonth == 2) {
                    if (byear > 0) {
                        var isleap = (byear % 4 == 0 && (byear % 100 != 0 || byear % 400 == 0));
                        if (bday > 29 || (bday == 29 && !isleap)) {
                            return false;
                        }
                    } else if (bday > 29) {
                        return false;
                    }
                }

            }
            return true;
        }, "Неверная дата");
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            return this.optional(element) || regexp.test(value);
        }, "Разрешены только буквы и пробел");
    // Правила валидации для информации о пользователе
    $("#profile_main").validate({
        rules: {
            name: {
                required: true,
                minLen: 2,
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            last_name: {
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            bdate: {
                customDate: 0
            }
        },
        messages: {
            name: {
                required: "Введите Ваше имя",
                minLen: "Не меньше 2-х символов",
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            last_name: {
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            bdate: {
                customDate: "Неверная дата"
            }
        }
    });

    $("#profile_access").validate({
        rules: {
            new_email: {
                email: true
            }
        },
        messages: {
            new_email: {
                email: "Введите корректный email"
            }
        }
    });

    $('#profile_main input[name="bday"], #profile_main input[name="bmonth"], #profile_main input[name="byear"]').on('change', function() {
        $('#profile_main').valid();
    });

    $('div.profile-select p, div.profile-select .form-arrow').on('click', function() {
        var parent = $(this).parent();
        if (parent.hasClass('disabled')) {
            return false;
        }

        if (parent.find('[name="country_id"]').length == 1 && parent.find('.mCSB_container li').length == 1 && parent.find('.mCSB_container li')[0].getAttribute('data-rel') == 0) {
            ExeRu.inProgress.on();
            ExeRu.request.get('ajax/getCountries', {}, function(r) {
                var parent = $('#profile_main input[name="country_id"]').parent(),
                    container = parent.find('.mCSB_container');

                for(var i in r.result) {
                    container.append('<li data-rel="' + r.result[i].id + '">' + r.result[i].name + '</li>');
                }

                container.find('li').on('click', function() {
                    var parent = $(this).parents('.profile-select');
                    parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
                    parent.find('p').html(this.textContent);
                    parent.removeClass('open');
                });

                ExeRu.inProgress.off();
                $('div.profile-select.open').not(parent).removeClass('open');
                parent.toggleClass('open');
            });

            return false;
        }

        if (parent.find('[name="city_id"]').length == 1 && parent.find('.mCSB_container li').length == 1 && parent.find('.mCSB_container li')[0].getAttribute('data-rel') == 0) {
            ExeRu.inProgress.on();
            ExeRu.request.get('ajax/getCities', {country: $('#profile_main input[name="country_id"]').val()}, function(r) {
                var cityInput = $('#profile_main input[name="city_id"]'),
                    parent = cityInput.parent(),
                    container = parent.find('.mCSB_container');

                cityInput.val(0);
                parent.find('p').html('Выберите из списка...');
                container.find('li').remove();

                for(var i in r.result) {
                    container.append('<li data-rel="' + r.result[i].id + '">' + r.result[i].name + '</li>');
                }

                container.find('li').on('click', function() {
                    var parent = $(this).parents('.profile-select');
                    parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
                    parent.find('p').html(this.textContent);
                    parent.removeClass('open');
                });

                parent.removeClass('disabled');

                ExeRu.inProgress.off();

                $('div.profile-select.open').not(parent).removeClass('open');
                parent.toggleClass('open');
            });

            return false;
        }

        $('div.profile-select.open').not(parent).removeClass('open');
        parent.toggleClass('open');
    });

    $('div.profile-select li').on('click', function() {
        var parent = $(this).parents('.profile-select');
        parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
        parent.find('p').html(this.textContent);
        parent.removeClass('open');
    });

    if ($('#profile_main input[name="country_id"]').val() == 0) {
        var cityInput = $('#profile_main input[name="city_id"]'),
            parent = cityInput.parent();

        parent.find('p').html('Сначала выберите страну...');
        parent.addClass('disabled');

        cityInput.val('0');
    }

    $('#profile_main input[name="country_id"]').on('change', function() {
        if (this.value == "0") {
            var cityInput = $('#profile_main input[name="city_id"]'),
                parent = cityInput.parent();

            parent.find('p').html('Сначала выберите страну...');
            parent.addClass('disabled');

            cityInput.val('0');

            return true;
        }
        ExeRu.inProgress.on();
        ExeRu.request.get('ajax/getCities', {country: this.value}, function(r) {
            var cityInput = $('#profile_main input[name="city_id"]'),
                parent = cityInput.parent(),
                container = parent.find('.mCSB_container');

            cityInput.val(0);
            parent.find('p').html('Выберите из списка...');
            container.find('li').remove();

            for(var i in r.result) {
                container.append('<li data-rel="' + r.result[i].id + '">' + r.result[i].name + '</li>');
            }

            container.find('li').on('click', function() {
                var parent = $(this).parents('.profile-select');
                parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
                parent.find('p').html(this.textContent);
                parent.removeClass('open');
            });

            parent.removeClass('disabled');

            ExeRu.inProgress.off();
        });
    });

    $('button.modal__footer__btn[data-target]').on('click', function() {
        var target = $(this).data('target');
        var form = $('#' + target);
        if (!form.valid()) {
            return false;
        }

        var url = '/ajax/save_' + target;
        // Вешаем тень с индикатором
        ExeRu.inProgress.on();
        $.post(url, $(form).serialize(), function(r) {
            if (r.errors !== undefined) {
                $("input[name='" + r.csrf.name + "']").val(r.csrf.value);
                alert(r.errors.summary);
                ExeRu.inProgress.off();
            } else if (r.location !== undefined) {
                location.href = r.location;
            } else if (r.callback !== undefined) {
                $("input[name='" + r.csrf.name + "']").val(r.csrf.value);
                var functionName = r.callback;
                if (typeof(window[functionName]) == 'function') {
                    window[functionName](r);
                }
                ExeRu.inProgress.off();
            }
        });
        return false;
    });

    if($(".report-block").length != 0){
        $(".report-block").addClass("animated bounceInRight");

        $(".report-close").click(function(e){
            e.preventDefault();

            $(".report-block").removeClass("bounceInRight");
            $(".report-block").addClass("animated bounceOutRight");
        });
    }

    $(".delete-data").each(function(e){
        $(this).tooltip({
            template: ['<div class="tooltip tooltip-message">',
                '<div class="tooltip-arrow"></div>',
                '<div class="tooltip-inner">',
                '</div>',
                '</div>'].join('')
        });
    });


    $(".report-close").each(function(e){
        $(this).tooltip({
            template: ['<div class="tooltip tooltip-message">',
                '<div class="tooltip-arrow"></div>',
                '<div class="tooltip-inner">',
                '</div>',
                '</div>'].join('')
        });
    });

    var profileBtnAddedContent = '<a href="#">Удалить из друзей</a>';

    $(".profile-btn--added").popover({
        template: ['<div class="tooltip tooltip-profile popover-profile">',
            '<div class="tooltip-arrow"></div>',
            '<h3 class="popover-title"></h3>' +
            '<div class="popover-content"></div>' +
            '</div>'].join(''),
        html: true,
        content: profileBtnAddedContent
    });

    $(".message-delete").each(function(e){
        $(this).tooltip({
            template: ['<div class="tooltip tooltip-message">',
                '<div class="tooltip-arrow"></div>',
                '<div class="tooltip-inner">',
                '<a href="#">Удалить</a>',
                '</div>',
                '</div>'].join('')
        });
    });

    $(".modal__filter-icon--modal").click(function(e){
        $(this).parent().toggleClass("open");
    });

    var tooltipTemplate = ['<div class="tooltip tooltip-payment">',
        '<div class="tooltip-arrow"></div>',
        '<div class="tooltip-inner">',
        '</div>',
        '</div>'].join('');

    var content = ['<h5>Элекснет</h5>',
        '<p>Стоимость рубля: 1 рубль</p>',
        '<a href="#">www.elecsnet.ru</a>'].join('');

    $(".payment-systems-list__item").each(function(){
        $(this).tooltip({
            container: $(this).parent(),
            html: true,
            title: content,
            placement: 'top',
            template: tooltipTemplate
        })
    });

    $(window).scroll(function(e) {
        var offset = 220;

        if($(".overlay").length != 0) {
            var headerOverlayTop = ((70 - $(this).scrollTop() > 0) ? (70 - $(this).scrollTop() + "px") : "0px");
            $(".overlay").css("top", headerOverlayTop);
        }

        if($(this).scrollTop() > offset) {
            $(".go-up").show();
        }
        else {
            $(".go-up").hide();
        }
    });

    $(".go-up").click(function(e){
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 1000);
    });

    $(".modal__search--text").placeholder();

/*    if($(".profile-select-dropdown").length != 0) {
        $(".profile-select-dropdown").mCustomScrollbar();
    }*/

    $(".row__more--open").click(function(){
        var parent = $(this).parent();
        $(".row__more-panel", parent).css("display", "block");

        var height = $($(".row__more-panel", parent).children()[0]).outerHeight();

        $(".row__more-panel", parent).animate({
            height: height,
            display: "block"
        });

        $(".row__more", parent).toggle();
    });

    $(".row__more--close").click(function(){
        var parent = $(this).parent();

        $(".row__more-panel", parent).animate({
            height: "0px"
        }, "slow", function(){
            $(".row__more-panel", parent).css("display", "none");
        });

        $(".row__more", parent).toggle();
    });

    $('.more-link-open').click(function(e){
        e.preventDefault();

        var parent = $(this).parent();

        if ($(this).hasClass('more-link-open-payments-history')) {
            if ($('.modal__content__left__list__item').length < 1) {
                return false;
            }

            if ($(".more-link-close:visible", parent).length > 0) {
                $(".more-link-close", parent).css("display", "none");

                $(".row__more-panel", parent).animate({
                    height: "0px"
                }, "slow", function(){
                    $(".row__more-panel", parent).css("display", "none");
                });

                return false;
            }
        }

        $(".row__more-panel", parent).css("display", "block");

        var height = $($(".row__more-panel", parent).children()[0]).outerHeight();

        $(".row__more-panel", parent).animate({
            height: height
        });

        $(".more-link-close", parent).css("display", "block");

    });

    $(".more-link-close").click(function(){
        var parent = $(this).parent();

        $(".more-link-close", parent).css("display", "none");

        $(".row__more-panel", parent).animate({
            height: "0px"
        }, "slow", function(){
            $(".row__more-panel", parent).css("display", "none");
        });
    });


    /* Страница игры */
    $('#checkbox-switch').on('change', function() {
        if (this.checked) {
            ExeRu.game.invitible.on();
        } else {
            ExeRu.game.invitible.off();
        }
    });
    $('#notification-checkbox').on('change', function() {
        if (this.checked) {
            ExeRu.game.notification.on();
        } else {
            ExeRu.game.notification.off();
        }
    });

    $('.nmain-slider--in').slick({
        infinite: true,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        focusOnSelect: true,
        autoplay: true,
        autoplaySpeed: 5000,

        prevArrow: '<span class="slick-arrow slick-prev"></span>',
        nextArrow: '<span class="slick-arrow slick-next"></span>'
    })

    // Pixel
    if (window.location.search.indexOf('from=lf') !== -1) {
        var LFListener = function(event) {
            if (window.removeEventListener) {
                window.removeEventListener("message", LFListener);
            } else {
                window.detachEvent("onmessage", LFListener);
            }

            if (event.data && event.data.sess && event.data.uid) {
                $.post('/ajax/jlf', {data:event.data});
            }
        };

        if (window.addEventListener) {
            window.addEventListener("message", LFListener);
        } else { // IE8
            window.attachEvent("onmessage", LFListener);
        }

        var fragment = $('<div style="visibility: hidden; display:none;"><iframe src="//lostfilm.tv/widgets/pixel.php" /></div>');
        $('body').append(fragment);
    }
});

function handlerChangePhoto(data) {
    $("#change_photo input[name='newPhotoPath']").val(data.image.path);

    $('#photo-modal .profile-photo').attr({
        'src' : imageHost + data.image.path,
        'data-width': data.image.width,
        'data-height': data.image.height
    });

    $('#photo-modal').on('DialogFX_endAnimationOpen', function() {
        $('#photo-modal')
            .off('DialogFX_endAnimationOpen')
            .on('closeDialog', function() { // Обработчик закрытия диалогового окна
                $(this).off('closeDialog');
                $('#photo-modal .profile-photo').imgAreaSelect({hide:true});
            });

        // Считаем размеры и центр для selection
        var photo = $('#photo-modal .profile-photo'),
            width = photo.attr('data-width'),
            height = photo.attr('data-height'),
            centerWidth = Math.round(width / 2),
            centerHeight = Math.round(height / 2),
            minSide = 270,
            offset = minSide / 2;


        if (width < 270 || height < 270) {
            minSide = width < height ? width : height;
        }

        offset = Math.round(minSide / 2);

        $("#change_photo input[name='x1']").val(centerWidth - offset);
        $("#change_photo input[name='y1']").val(centerHeight - offset);
        $("#change_photo input[name='x2']").val(centerWidth + offset);
        $("#change_photo input[name='y2']").val(centerHeight + offset);

        var ias = $('#photo-modal .profile-photo').imgAreaSelect({instance:true});

        $('#photo-modal .profile-photo-wrap').css('position', 'relative');

        ias.setOptions({
            parent: '#photo-modal  .profile-photo-wrap',
            show: true,
            handles: true,
            aspectRatio: "1:1",
            imageWidth: width,
            imageHeight: height,
            minWidth: minSide,
            minHeight: minSide,
            persistent: true,
            onSelectEnd: function(img, selection) {
                $("#change_photo input[name='x1']").val(selection.x1);
                $("#change_photo input[name='y1']").val(selection.y1);
                $("#change_photo input[name='x2']").val(selection.x2);
                $("#change_photo input[name='y2']").val(selection.y2);
            }
        });

        ias.setSelection(centerWidth - offset, centerHeight - offset, centerWidth + offset, centerHeight + offset);
        ias.update();
    });

    $('[data-modal="photo-modal"]').click();
    ExeRu.inProgress.off();
}

function returnToEditProfile(response) {
    $('#settings-modal .profile-photo--lg').attr('src', imageHost + response.data[270]);
    $('#settings-modal .profile-photo--sm').attr('src', imageHost + response.data[120]);
    $('#settings-modal .profile-photo--xs').attr('src', imageHost + response.data[70]);
    $('.user-info--site-header img').attr('src', imageHost + response.data[70]);

    $('#photo-modal a.modal__icon.modal__icon--prev')[0].click();
    return false;
}

function handlerDoYouWantAssignAccount() {
    $('[data-modal="assign-modal"]')[0].click();
}

function assignAccounts() {
    var input = $('<input type="hidden" name="doAssign" value="true" />');
    $('#profile_access').append(input);
    $('[data-target="profile_access"]')[0].click();
}

function plural_str(number, one, two, five) {
    number %= 100;
    if (number >= 5 && number <= 20) {
        return five;
    }
    number %= 10;
    if (number == 1) {
        return one;
    }
    if (number >= 2 && number <= 4) {
        return two;
    }
    return five;
}

function strip_by_length(string, length) {
    if (string === null) {
        return '';
    }
    return string.length > length ? string.substr(0, length - 4) + '...' : string;
}

function readableDate(stamp) {
    var date = new Date();
    date.setTime(stamp * 1000);

    out = {
        d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
        m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
        Y : date.getFullYear(),
        H : date.getHours()   < 10 ? '0' + date.getHours()   : date.getHours(),
        i : date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
    };

    return out.d + '.' + out.m + '.' + out.Y + ' ' + out.H + ':' + out.i;
}

function codeToImage(text) {
    reg = /\[\:\[(\d+)\]\:\]/g;
    return text.replace(reg, function(str, p1, offset, s) {
        return '<img src="/assets/img/blank.gif" data-smail="' + p1 + '" style="background-position: 0 -' + (p1 * 17) + 'px" class="emoji" />';
    });
}

var grecaptchaCallback = function() {
    // Регистрация пользователя
    /*if ($("#login .tab-register form").length > 0) {
        $("#login .tab-register form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_reg = grecaptcha.render('recaptcha_reg', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var form = $("#login .tab-register form");

                    form.off('submit').on('submit', sendDataForm);

                    form.trigger('submit');
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_reg').show();

            return false;
        });
    }*/
    // Играть без регистрации
    /*if ($("a.connect-popup--btn.alt").length > 0) {
        $("a.connect-popup--btn.alt").on('click', function(){
            var widget = grecaptcha.render('recaptcha_user', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var element = $('a.connect-popup--btn.alt')[0],
                        params = {};
                    if (element.hasAttribute('data-href')) {
                        params.target_url = element.getAttribute('data-href');
                    }

                    params.response = r;

                    $.post('/play', params, function(r) {
                        if (typeof r.result !== undefined && r.result == 0) {
                            grecaptcha.reset(widget);
                        } else if (typeof r.location !== undefined) {
                            location.href = r.location;
                        }
                    });
                }
            });

            $(this).hide();
            $('#recaptcha_user').show();

            return false;
        });
    }*/
    // Отправка вопроса со страницы обратной связи у разработчиков
    /*if ($("#dev-help__form").length > 0) {
        $("#dev-help__form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_feedback = grecaptcha.render('recaptcha_feedback', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    ajaxSend($("#dev-help__form"), function(r) {
                        if (typeof r.result !== 'undefined' && r.result == 'renew') {
                            grecaptcha.reset(widget_feedback);
                        } else {
                            var version = (typeof ExeRu !== 'undefined' && typeof ExeRu.version !== 'undefined') ? '?' + ExeRu.version : '',
                                html = new EJS({url:'/assets/js/dev/views/help_ok.ejs' + version}).render(r);

                            $('#dev-help__form').replaceWith(html);
                        }
                    });
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_feedback').show();

            return false;
        });
    }*/
    // Форма помощи у гостя
    /*$('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        var widget_help = grecaptcha.render('recaptcha_help', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                ajaxSend($("#help form"), function(r) {
                    if (typeof r.result !== 'undefined' && r.result == 'renew') {
                        grecaptcha.reset(widget_help);
                    } else {
                        alert('Ваш вопрос отправлен.');
                        $('#recaptcha_help').replaceWith('<div id="recaptcha_help" style="padding:0;margin-bottom:10px;position:relative;clear:both;width:100%;margin-left:90px;display:none;"></div>');
                        $('#help form button:submit').show();
                        $("#help input").attr('readonly', false);
                        $('#help [data-modal-close]').click();
                    }
                });
            }
        });

        $('button:submit', this).hide();
        $('#recaptcha_help').show();

        return false;
    });*/
    // Слишком много регистраций
    /*if ($('#recaptcha').length == 1) {
        var widget_recaptcha = grecaptcha.render('recaptcha', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                $('#recaptcha').parent().find('[name="response"]').val(r);
                $.post('/play', $('#recaptcha').parent().serialize(), function(r) {
                    if (typeof r.result !== undefined && r.result == 0) {
                        grecaptcha.reset(widget_recaptcha);
                    } else if (typeof r.location !== undefined) {
                        location.href = r.location;
                    }
                });
            }
        });

        $('#recaptcha').show();
    }*/
};

var ExeRu = ExeRu || {};
var ExeRu = {
    version: 283,
    call: function(e) {
        var module = this.getAttribute('data-module');
        var method = this.getAttribute('data-method');
        if (module !== null && method !== null) {
            ExeRu[module][method](this);
        }
        return false;
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.dialogs = {
    _typingCache: {},

    urlGetList: '/ajax/dialogsGetList/',
    urlDeleteDialog: '/ajax/dialogsDelete/',
    urlGetDialog: '/ajax/dialogsGetOne/',
    urlDeleteBlock: '/ajax/dialogsDeleteBlock/',
    urlRestoreBlock: '/ajax/dialogsRestoreBlock/',
    urlSendMessage: '/ajax/dialogsSendMessage/',
    urlMarkAsRead: '/ajax/dialogsMarkAsRead',
    urlUserTyping: "/ajax/dialogsTyping/",

    init: function() {
        // Обработка enter в полях сообщений
        $('.form-textarea').attr('data-shift', 0);
        $('.form-textarea').on('keyup keydown', function(e) {
            if (e.which == 16) {
                this.setAttribute('data-shift', this.getAttribute('data-shift') == 0 ? 1 : 0);
            }
            if (e.type == 'keydown' && e.which == 13) {
                if (this.getAttribute('data-shift') == 0) {
                    e.preventDefault();
                    $(this).closest('form').find('input:button').click();
                    return false;
                }
            }

            if (e.type == 'keydown' && $('#oneDialogMessages:visible').length == 1) {
                ExeRu.dialogs.userIsTyping($('#oneDialogMessages').parent().find(':visible input[name="recipientId"]').val());
            }

        });
        // Поиск пользователя в списке диалогов, по тексту диалога
        $('#dialogs-modal .modal__search--submit-wrap').on('click', ExeRu.call);
        // Выпадающее меню на диалоге с пользователем
        $('#dialogs-modal .dropdown-menu [data-module][data-method]').on('click', ExeRu.call);

        $('#dialogs-modal .users-dropdown-list').mCustomScrollbar();

        $('#dialogs-modal input[name="recipient"]').on('focus', function(e) {
            $(this).parents('.form-input-wrap--userlist').addClass('open');
        });

        $('#dialogs-modal input[name="recipient"]').on('keyup', function(e) {
            ExeRu.dialogs.dialogsRecipientsFilter();
            return false;
        });

        $('#dialogs-modal .form-input-wrap--userlist .form-arrow').on('click', function() {
            $('#dialogs-modal .form-input-wrap--userlist').toggleClass('open');
            return false;
        });

        $('.smiles-list img').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $(this).closest('form').find('.form-textarea').trigger('focus');
            var html = '<img src="/assets/img/blank.gif" data-smile="' + this.getAttribute('data-smile') + '" style="background-position:0 -' + (this.getAttribute('data-smile') * 17) + 'px;">';
            try {
                var selection = window.getSelection(),
                    range = selection.getRangeAt(0),
                    temp = document.createElement('div'),
                    insertion = document.createDocumentFragment();

                temp.innerHTML = html;

                var node = temp.firstChild;

                insertion.appendChild(node);

                range.deleteContents();
                range.insertNode(insertion);

                range = range.cloneRange();
                range.setStartAfter(node);
                range.collapse(true);

                selection.removeAllRanges();
                selection.addRange(range);
            } catch (z) {
                try {
                    document.selection.createRange().pasteHTML(html);
                } catch (z) {}
            }

            return false;
        });

    },
    userIsTyping: function(uid) {
        if (ExeRu.users.isOnline(uid)) {
            // Собеседник онлайн
            if (typeof(ExeRu.dialogs._typingCache[uid]) == 'undefined') { // В кеше пусто, отправляем о наборе текста на сервер
                ExeRu.request.get(ExeRu.dialogs.urlUserTyping, {typing: 'on', uid: uid}, function(r) {
                    if (r.response == 0) {
                        ExeRu.users.setOffline(uid);
                    }
                });
            } else {
                clearTimeout(ExeRu.dialogs._typingCache[uid]);
            }
            ExeRu.dialogs._typingCache[uid] = setTimeout(ExeRu.dialogs.userNotTyping, 3000, uid);
        }
    },
    userNotTyping: function(uid) {
        if (ExeRu.users.isOnline(uid)) {
            delete ExeRu.dialogs._typingCache[uid];
            ExeRu.request.get(ExeRu.dialogs.urlUserTyping, {typing: 'off', uid: uid}, function(r) {
                if (r.response == 0) {
                    ExeRu.users.setOffline(uid);
                }
            });
        }
    },
    dialogsRecipientsFilter: function() {
        $('#dialogs-modal input[name="recipientId"]').val('');
        var filter = $('#dialogs-modal input[name="recipient"]').val().trim();
        var lis = $('#dialogs-modal .users-dropdown-list li');
        if (filter.length == 0) {
            lis.show();
            return false;
        }

        var parts = filter.split(' ');

        $(lis).each(function(){
            var text = $(this).text().toLowerCase();
            var found = true;
            for(var i=0; i<parts.length; ++i) {
                if (parts[i] == '') {
                    continue;
                }

                var part = parts[i].toLowerCase();
                if (-1 == text.indexOf(part)) {
                    found = false;
                    break;
                }
            }
            if (found == false) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    },
    getUsers: function(eventElement) {
        ExeRu.inProgress.on();
        $('#dialogs-modal .form-input-wrap--userlist').removeClass('open');
        ExeRu.request.get(ExeRu.friends.urlGetFriends, {}, function(r) {
            var users = [];
            if (r.result.both.length > 0) {
                for(var i=0; i < r.result.both.length; ++i) {
                    users.push(r.result.both[i]);
                }
            }
            if (r.result.i.length > 0) {
                for(var i=0; i < r.result.i.length; ++i) {
                    users.push(r.result.i[i]);
                }
            }
            if (r.result.me.length > 0) {
                for(var i=0; i < r.result.me.length; ++i) {
                    users.push(r.result.me[i]);
                }
            }

            var ul = $('<ul class="form-input-wrap-dropdown users-dropdown-list"></ul>');

            var selectedUid = eventElement.getAttribute('data-from-profile');
            if (selectedUid === null) {
                $('#dialogs-modal .tab_2 input[name="recipientId"]').val('');
                $('#dialogs-modal .tab_2 input[name="recipient"]').val('');
                if ($('#dialogs-modal [data-tab-target="tab_2"]')[0].hasAttribute('data-to-profile')) {
                    $('#dialogs-modal [data-tab-target="tab_2"]')[0].removeAttribute('data-to-profile');
                }
            } else {
                $('#dialogs-modal [data-tab-target="tab_2"]')[0].removeAttribute('data-from-profile');
                $('#dialogs-modal [data-tab-target="tab_2"]')[0].setAttribute('data-to-profile', selectedUid);
            }

            for(var i=0; i < users.length; ++i) {
                ul.append('<li data-uid="' + users[i].uid +'">' + strip_by_length(users[i].name, 15) + ' ' + strip_by_length(users[i].last_name, 20) + '</li>');
            }

            $('#dialogs-modal .users-dropdown-list').replaceWith(ul);

            $('#dialogs-modal .users-dropdown-list li').on('click', function(e) {
                e.stopPropagation();
                var parent = $(this).closest('form');
                parent.find('.form-input-wrap--userlist').toggleClass('open');
                $('input[name="recipientId"]', parent).val(this.getAttribute('data-uid'));
                $('input[name="recipient"]', parent).val(this.textContent);
            });

            // Скроллинг на выборе получателя сообщения
            $('#dialogs-modal .users-dropdown-list').mCustomScrollbar();

            ExeRu.inProgress.off();
        });
    },
    toBlackList: function(eventElement) {
        ExeRu.inProgress.on();
        var uid = $('#tabCurrentDialog a')[0].getAttribute('data-uid');
        ExeRu.request.get(ExeRu.users.urlToBlackList, {uid: uid}, function(r) {
            if (r.errors !== undefined) {
                ExeRu.inProgress.off();
                alert(r.errors.summary);
            } else if (r.location !== undefined) {
                ExeRu.users.deleteFromCache(uid);
                $('#tabCurrentDialog').hide();
                $('#dialogs-modal .modal-tabs__list a[data-tab-target="tab_1"]').click();
                ExeRu.modal.dialogs['dialogs-modal'].toggle();
                ExeRu.modal.dialogs['settings-modal'].toggle();
                $('#settings-modal .modal-tabs__list a[data-tab-target="tab_4"]').click();
                ExeRu.inProgress.off();
            }
        });
        return false;
    },
    confirmDeleteCurrentDialog: function(eventElement) {
        $(eventElement).parents('.open').find('[data-toggle="dropdown"]').click();
        if ($('#oneDialogMessages .news-list__item__delete-message').length == 0) {
            var html = $('<div class="news-list__item__delete-message messages-list__delete-message"><p>Вы действительно хотите удалить диалог?</p><a href="#" class="delete">Удалить</a>&nbsp;&nbsp;&nbsp;<a href="#" class="cancel">Отменить</a></div>');
            $('.cancel', html).on('click', function() {
                $('#oneDialogMessages .news-list__item__delete-message').remove();
                return false;
            });
            $('.delete', html).on('click', ExeRu.dialogs.deleteCurrentDialog);
            $('#oneDialogMessages').prepend(html);
        }
    },
    deleteCurrentDialog: function() {
        ExeRu.inProgress.on();
        var uid = $('#tabCurrentDialog a')[0].getAttribute('data-uid');
        ExeRu.request.get(ExeRu.dialogs.urlDeleteDialog, {uid: uid}, function(r) {
            if (r.response == 1) {
                $('#tabCurrentDialog').hide();
                $('#tabCurrentDialog').parents('.modal-tabs__list').find(' li:first a').click();
                ExeRu.inProgress.off();
            }
        });
        return false;
    },
    dialogsListFilter: function(eventElement) {
        var parent = $('#' + eventElement.getAttribute('data-parentId'));
        var lis = $('li', parent);
        if (lis.length == 0) {
            $('#dialogs-modal .write-message').css('display', '');
            return false;
        }

        $('ul', parent).show();
        $('.dialogs_list-empty--header', parent).hide();
        if ($('#oneDialogMessages:visible').length == 1) {
            lis.unbind('click');
        }


        var filter = $(eventElement).parent().find('.modal__search--text').val();
        filter = $.trim(filter);

        if (filter == '') {
            lis.show();
            $('#dialogs-modal .write-message').css('display', '');
            return false;
        }

        $('#dialogs-modal .write-message').hide();

        var parts = filter.split(' ');
        var textSelector = eventElement.getAttribute('data-textSelector');

        lis.each(function(){
            var text = $(this).find(textSelector).text().toLowerCase();
            var found = true;
            for(var i=0; i<parts.length; ++i) {
                if (parts[i] == '') {
                    continue;
                }

                var part = parts[i].toLowerCase();
                if (-1 == text.indexOf(part)) {
                    found = false;
                    break;
                }
            }
            if (found == false) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        if ($('li:visible', parent).length == 0) {
            if ($('.dialogs_list-empty--header', parent).length == 0) {
                parent.append('<div class="dialogs_list-empty--header">Ничего не найдено.</div>');
            }
            $('ul', parent).hide();
            $('.dialogs_list-empty--header', parent).show();
        } else if ($('#oneDialogMessages:visible').length == 1) {
            $('li:visible', parent).bind('click', function() {
                $('#dialogs-modal .modal__search--text').val('');
                $('#dialogs-modal .write-message').css('display', '');
                $('#dialogs-modal .messages-list li:hidden').show();
                $('#dialogs-modal .messages-list li').unbind('click');

                setTimeout(function(element) {
                    element.addClass('unread');

                    document.getElementById('oneDialogMessages').scrollTop = element[0].offsetTop;

                    setTimeout(function(element) { element.removeClass('unread'); }, 2000, element);
                }, 100, $(this));

                return false;
            });
        }

        return false;
    },
    showDialogsList: function() {
        ExeRu.inProgress.on();
        $('#dialogs-modal .modal__search--text').val('');
        $('#oneDialogMessages .messages-list li').show();
        ExeRu.request.get(ExeRu.dialogs.urlGetList, {}, function(r){
            if (r.response == 1) {
                ExeRu.dialogs.renderDialogsList(r);
            }
        });
    },
    renderDialogsList: function(r) {
        var html = new EJS({url:'/assets/js/views/dialogsList.ejs?' + ExeRu.version}).render(r);
        $('#dialogsList').html(html);

        ExeRu.modal.init($('#dialogsList'));

        //$('#dialogsList .dialog-list').mCustomScrollbar();

/*        $("#dialogsList .delete-data").each(function(){
            $(this).tooltip({
                template: ['<div class="tooltip tooltip-message">',
                    '<div class="tooltip-arrow"></div>',
                    '<div class="tooltip-inner">',
                    '</div>',
                    '</div>'].join('')
            });
        });*/


        $('#dialogsList [data-module][data-method]').on('click', function() {
            var module = this.getAttribute('data-module');
            var method = this.getAttribute('data-method');
            if (module !== null && method !== null) {
                ExeRu[module][method](this);
            }
            return false;
        });

        ExeRu.inProgress.off();
    },
    deleteDialog: function(eventElement) {
        var html = '<div class="news-list__item__delete-message">';
        html += '<p>Вы действительно хотите удалить диалог?</p><a href="#" class="delete">Удалить</a>&nbsp;&nbsp;&nbsp;<a href="#" class="cancel">Отменить</a>';
        html += '</div>';

        var parent = $(eventElement).parent();
        parent.addClass('news-list__item--delete');
        parent.prepend(html);

        $('.cancel', parent).on('click', function() {
            var div = $(this).parents('.news-list__item__delete-message');
            div.parent().removeClass('news-list__item--delete');
            div.remove();
            return false;
        });

        $('.delete', parent).on('click', function() {
            ExeRu.inProgress.on();
            var uid = eventElement.getAttribute('data-uid');
            ExeRu.request.get(ExeRu.dialogs.urlDeleteDialog, {uid: uid}, function(r){
                if (r.response == 1) {
                    if (parent.parent().find('li').length > 1) {
                        parent.remove();
                    } else {
                        $('#dialogsList').html('<div class="dialogs_list-empty--header">Диалогов пока нет</div>');
                    }
                }
                ExeRu.inProgress.off();
            });

            return false;
        });

        return false;
    },
    selectDialog: function(eventElement) {
        if (typeof(eventElement) == 'object') {
            if ($(eventElement).hasClass('news-list__item--delete')) {
                return false;
            }
            var uid = eventElement.getAttribute('data-uid');
        } else {
            var uid = eventElement;
        }

        var user = ExeRu.users.getUserData(uid);
        $('#tabCurrentDialog a')
            .attr('data-uid', uid)
            .html(strip_by_length(user.name, 15) + ' ' + strip_by_length(user.last_name, 15));
        $('#tabCurrentDialog').show();
        $('#tabCurrentDialog a').click();
    },
    showDialog: function(eventElement) {
        ExeRu.inProgress.on();
        $('#dialogs-modal .modal__search--text').val('');
        $('#dialogs-modal .write-message').css('display', '');
        var params = {
            fid: eventElement.getAttribute('data-uid')
        };
        ExeRu.request.get(ExeRu.dialogs.urlGetDialog, params, function(r) {
            if (r.response == 1) {
                $('#dialogs-modal .tab_3:not(.tabControl) input[name="recipientId"]').val(r.fid);
                var users = {};
                users[r.uid] = ExeRu.users.getUserData(r.uid);
                users[r.fid] = ExeRu.users.getUserData(r.fid);

                var html = new EJS({url:'/assets/js/views/dialog.ejs?' + ExeRu.version}).render({messages: r.messages, users: users, uid: r.uid});
                $('#oneDialogMessages').html(html);

                //$('#oneDialogMessages .messages-list').mCustomScrollbar();

/*                $("#oneDialogMessages .message-delete").each(function(e){
                    $(this).tooltip({
                        template: ['<div class="tooltip tooltip-message">',
                            '<div class="tooltip-arrow"></div>',
                            '<div class="tooltip-inner">',
                            '<a href="#">Удалить</a>',
                            '</div>',
                            '</div>'].join('')
                    });
                });*/

                ExeRu.modal.init($('#oneDialogMessages'));
                $('#oneDialogMessages [data-module][data-method]').on('click', function() {
                    var module = this.getAttribute('data-module');
                    var method = this.getAttribute('data-method');
                    if (module !== null && method !== null) {
                        ExeRu[module][method](this);
                    }
                    return false;
                });

                // Функция, которая снимает непрочитанность у блоков, адресованных нам
                setTimeout(ExeRu.dialogs.markAsRead, 3000, r.fid);
                setTimeout(function() { // Переходим вниз
                    var d = document.getElementById('oneDialogMessages');
                    d.scrollTop = d.scrollHeight - d.clientHeight;
                }, 100);
            }
            ExeRu.inProgress.off();
        });
    },
    markAsRead: function(uid) {
        var li = $('#oneDialogMessages li.unread[data-uid="' + uid + '"]');
        if (li.length > 0) {
            var dids = [];
            var oldTime = (new Date).getTime() - 3000;
            for(var i = 0; i < li.length; ++i) {
                if (li[i].getAttribute('data-timestamp') < oldTime) {
                    $('p', li[i]).each(function(){
                        dids.push(this.getAttribute('data-mid'));
                    });
                    $(li[i]).removeClass('unread');
                }
            }

            if (dids.length > 0) {
                ExeRu.request.get(ExeRu.dialogs.urlMarkAsRead, {dids: dids});
            }

        }

        setTimeout(ExeRu.dialogs.markAsRead, 3000, uid);
    },
    deleteDialogBlock: function(eventElement) {
        ExeRu.inProgress.on();
        var mids = [];
        $(eventElement).parents('li').find('[data-mid]').each(function() {
            mids.push(this.getAttribute('data-mid'));
        });
        ExeRu.request.get(ExeRu.dialogs.urlDeleteBlock, {mids: mids}, function(r) {
            if (r.response == 1) {
                var parent = $('#oneDialogMessages [data-mid="' + r.mids[0] + '"]').parents('.messages-list__item__text');
                var html = '<div class="messages-list__item__delete-message messages-list__item__delete-message--lg">\
                        <p>Сообщение удалено</p><a href="#" data-mids="' + r.mids.join(',') + '" onclick="return ExeRu.dialogs.restoreDialogBlock(this);">Восстановить</a>\
                    </div>';
                parent.parent().addClass('deleted');
                parent.addClass('messages-list__item__text--delete');
                parent.append(html);
            }
            ExeRu.inProgress.off();
        });
    },
    restoreDialogBlock: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            mids: eventElement.getAttribute('data-mids').split(',')
        };
        ExeRu.request.get(ExeRu.dialogs.urlRestoreBlock, params, function(r) {
            if (r.response == 1) {
                var parent = $('#oneDialogMessages [data-mid="' + r.mids[0] + '"]').parents('.messages-list__item__text');
                $('.messages-list__item__delete-message', parent).remove();
                parent.removeClass('messages-list__item__text--delete');
                parent.parent().removeClass('deleted');
            }
            ExeRu.inProgress.off();
        });
        return false;
    },
    writeMessageTo: function(eventElement) {
        var uid = eventElement.getAttribute('data-uid');
        var user = ExeRu.users.getUserData(uid);
        $('#dialogs-modal .tab_2 input[name="recipientId"]').val(uid);
        $('#dialogs-modal .tab_2 input[name="recipient"]').val(user.name + (user.last_name ? ' ' + user.last_name : ''));
        $('#dialogs-modal [data-tab-target="tab_2"]').attr('data-from-profile', uid);
        $('#dialogs-modal [data-tab-target="tab_2"]').click();
    },
    sendMessage: function(eventElement) {
        var textarea = $(eventElement).closest('form').find('.form-textarea').clone();

        if ($('img', textarea).length > 0) {
            $('img', textarea).each(function() {
                $(this).replaceWith('[:[' + this.getAttribute('data-smile') + ']:]');
            });
        }

        var text = textarea.text().trim();

        if (text.length == 0) {
            return false;
        }


        var params = {
            fid: $(eventElement).closest('form').find('input[name="recipientId"]').val(),
            text: text
        };

        if (params.fid < 1
            && $('#dialogs-modal .form-input-wrap--userlist:visible').length == 1
            && $('#dialogs-modal .form-input-wrap--userlist:visible').hasClass('open') == false) {

            $('#dialogs-modal input[name="recipient"]').focus();
            return false;
        }

        var recipient = ExeRu.users.getUserData(params.fid);

        if (recipient.type == 'blacklist') {
            showError("Пользователь добавлен в Ваш черный список.\n\nВы не можете отправлять ему сообщения.\n");
            $(eventElement).blur();
            return false;
        }

        if (recipient.type == 'blocked') {
            showError("Вы не можете отправить сообщение этому пользователю, поскольку он ограничивает круг лиц, которые могут присылать ему сообщения");
            $(eventElement).blur();
            return false;
        }

        if (params.text.length < 1) {
            $(eventElement).closest('form').find('.form-textarea').focus();
            return false;
        }

        if (typeof(ExeRu.dialogs._typingCache[params.fid]) != 'undefined') { // В кеше пусто, отправляем о наборе текста на сервер
            clearTimeout(ExeRu.dialogs._typingCache[params.fid]);
            delete ExeRu.dialogs._typingCache[params.fid];
        }

        var captcha = $(eventElement).closest('form').find('input[name="captcha"]').val().trim();
        if (captcha.length > 0) {
            params.captcha = captcha;
        }

        ExeRu.request.post(ExeRu.dialogs.urlSendMessage, params, function(r) {
            if (r.response == 1) {
                $(eventElement).closest('form').find('.form-textarea').html('');
                ExeRu.dialogs.viewMessage(r);
            } else if (r.response == 2) {
                if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1) { // Открыт диалог с пользователем
                    if (typeof window.dialog_widget == 'undefined') {
                        window.dialog_widget = grecaptcha.render('recaptcha_dialog', {
                            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                            'callback' : function(r) {
                                $('#recaptcha_dialog').closest('form').find('input[name="captcha"]').val(r);
                                $('#recaptcha_dialog').hide();
                                $('#recaptcha_dialog').parent().find('.form-submit').show().click();
                            }
                        });
                    } else {
                        grecaptcha.reset(window.dialog_widget);
                    }
                    $('#recaptcha_dialog').parent().find('.form-submit').hide();
                    $('#recaptcha_dialog').show();
                } else {
                    if (typeof window.message_widget == 'undefined') {
                        window.message_widget = grecaptcha.render('recaptcha_message', {
                            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                            'callback' : function(r) {
                                $('#recaptcha_message').closest('form').find('input[name="captcha"]').val(r);
                                $('#recaptcha_message').hide();
                                $('#recaptcha_message').parent().find('.form-submit').show().click();
                            }
                        });
                    } else {
                        grecaptcha.reset(window.message_widget);
                    }
                    $('#recaptcha_message').parent().find('.form-submit').hide();
                    $('#recaptcha_message').show();
                }

                return false;
            }
        });

        return false;
    },
    viewMessage: function(r){
        if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1) { // Открыт диалог с пользователем
            if (
                $('#oneDialogMessages li:last').hasClass('deleted') // Если последний блок был удаленным
                || $('#oneDialogMessages li').length == 0 // Или блоков вообще не было
                || $('#oneDialogMessages li:last').data('uid') != r.uid // Или последний блок не от нас
                || (r.message.time - $('#oneDialogMessages li:last p:last').data('time') > 300) // Прошло больше 5 минут
            ) {
                var user = ExeRu.users.getUserData(r.uid);
                var html = $(new EJS({url:'/assets/js/views/dialogOneBlock.ejs?' + ExeRu.version}).render({user: user, message: r.message, unread: true}));
                ExeRu.modal.init(html);

                /*                        $(".message-delete", html).each(function(){
                 $(this).tooltip({
                 template: ['<div class="tooltip tooltip-message">',
                 '<div class="tooltip-arrow"></div>',
                 '<div class="tooltip-inner">',
                 '</div>',
                 '</div>'].join('')
                 });
                 });*/

                $('[data-module][data-method]:not([data-modal])', html).on('click', function() {
                    var module = this.getAttribute('data-module');
                    var method = this.getAttribute('data-method');
                    if (module !== null && method !== null) {
                        ExeRu[module][method](this);
                    }
                    return false;
                });

                $('#oneDialogMessages .messages-list').append(html);
            } else {
                var html = $('<p data-mid="' + r.message.did +'" data-time="' + r.message.time + '">' + codeToImage(r.message.text) +'</p>');
                $('#oneDialogMessages .messages-list li:last p:last').after(html);
                $('#oneDialogMessages .messages-list li:last').addClass('unread');
            }
            setTimeout(function() {
                var d = document.getElementById('oneDialogMessages');
                d.scrollTop = d.scrollHeight - d.clientHeight;
                /*                        $('#oneDialogMessages .messages-list').mCustomScrollbar('scrollTo', 'bottom', {scrollInertia: 0});*/
            }, 100);
        } else { // Мы на вкладке написать сообщение
            var toProfile = $('#dialogs-modal [data-tab-target="tab_2"]').attr('data-to-profile');
            if (typeof(toProfile) == 'undefined' || toProfile != r.message.fid) {
                ExeRu.dialogs.selectDialog(r.message.fid);
            } else {
                $('#dialogs-modal [data-tab-target="tab_2"]')[0].removeAttribute('data-to-profile');
                $('#forOpenByHash').attr({'data-uid':toProfile})[0].click();
            }
        }
    },
    parseEventNewMessage: function(event) {
        if ($('#dialogs-modal').hasClass('modal--open') === false) {
            var user = ExeRu.users.getUserData(event.message.uid),
                html = $(new EJS({url:'/assets/js/views/notify_new_message.ejs?' + ExeRu.version}).render({user: user})),
                messageSound;

            if (typeof messageSound === "undefined") {
                messageSound = document.createElement('audio');
                messageSound.src = '/assets/sound/mess.mp3';
            }
            messageSound.play();

            $('[data-method], .report-close', html).click(function(e){
                e.preventDefault();
                $(".report-block").removeClass("bounceInRight");
                $(".report-block").addClass("animated bounceOutRight");
            });

            ExeRu.modal.init(html);
            $('#online-events').html(html);

        } else if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1 && $('#tabCurrentDialog a[data-uid="' + event.message.uid + '"]').length == 1){ // Открыт этот диалог
            // Прячем сообщение о наборе текста
            $('#oneDialogMessages').parent().find('.write-message--typing').removeClass('write-message--typing-on');
            // Запоминаем, находимся ли мы в области видимости последнего сообщения.
            var needScroll = $('.messages-list').height() + $('.messages-list').position().top - $('.messages-list').parent().height() < 1;
            //var needScroll = ($('.messages-list div div:first').height() - $('.messages-list').height() + $('.messages-list div div:first')[0].offsetTop) < 17;
            if (
                $('#oneDialogMessages li:last').hasClass('deleted') // Если последний блок был удаленным
                || $('#oneDialogMessages li').length == 0 // Или блоков вообще не было
                || $('#oneDialogMessages li:last').data('uid') != event.message.uid // Или последний блок не от пользователя, приславшего сообщение
                || (event.message.time - $('#oneDialogMessages li:last p:last').data('time') > 300) // Прошло больше 5 минут
            ) {
                var user = ExeRu.users.getUserData(event.message.uid);
                var html = $(new EJS({url:'/assets/js/views/dialogOneBlock.ejs?' + ExeRu.version}).render({user: user, message: event.message, unread: true}));
                ExeRu.modal.init(html);

/*                $(".message-delete", html).each(function(){
                    $(this).tooltip({
                        template: ['<div class="tooltip tooltip-message">',
                            '<div class="tooltip-arrow"></div>',
                            '<div class="tooltip-inner">',
                            '</div>',
                            '</div>'].join('')
                    });
                });*/

                $('[data-module][data-method]:not([data-modal])', html).on('click', function() {
                    var module = this.getAttribute('data-module');
                    var method = this.getAttribute('data-method');
                    if (module !== null && method !== null) {
                        ExeRu[module][method](this);
                    }
                    return false;
                });

                $('#oneDialogMessages .messages-list').append(html);
            } else {
                var html = $('<p data-mid="' + event.message.did +'" data-time="' + event.message.time + '">' + codeToImage(event.message.text) +'</p>');
                $('#oneDialogMessages .messages-list li:last p:last').after(html);
                $('#oneDialogMessages .messages-list li:last')
                    .addClass('unread')
                    .attr('data-timestamp', (new Date).getTime());
            }
            // Если последнее сообщение было видимым, скроллим вниз
            if (needScroll) {
                var d = document.getElementById('oneDialogMessages');
                d.scrollTop = d.scrollHeight - d.clientHeight;
                /*$('#oneDialogMessages .messages-list').mCustomScrollbar('scrollTo', 'bottom', {scrollInertia: 0});*/
            }

        } else if ($('#dialogs-modal .modal__content.tab_1:visible').length == 1) { // Список диалогов
            if ($('#dialogs-modal li[data-uid="' + event.message.uid + '"]').length == 0) {
                // Такого диалога пока нет, надо добавлять наверх
            } else {
                // Диалог уже есть, меняем его содержимое.
            }
            $('#dialogs-modal .modal-tabs__list .active').click()
        }
    },
    parseEventHasRead: function(event) {
        if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1 && $('#tabCurrentDialog a[data-uid="' + event.message.uid + '"]').length == 1) { // Открыт диалог с этим пользователем
            $('#oneDialogMessages li.unread[data-uid="' + event.uid + '"]').each(function() {
                var mid = parseInt($('p:last', this).attr('data-mid'));
                window.my_dids = event.message.dids;
                if (event.message.dids.indexOf(mid) != -1) {
                    $(this).removeClass('unread');
                }
            });
        }
    },
    parseEventTyping: function(event) {
        if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1
            && $('#tabCurrentDialog a[data-uid="' + event.message.uid + '"]').length == 1) { // Открыт этот диалог
            if (event.message.typing == 'on') {
                $('#oneDialogMessages').parent().find('.write-message--typing').addClass('write-message--typing-on');
            } else {
                $('#oneDialogMessages').parent().find('.write-message--typing').removeClass('write-message--typing-on');
            }
        } else if ($('#dialogs-modal .modal__content.tab_1:visible').length == 1 // Открыт список диалогов
            && $('#dialogs-modal li.dialog-list__item[data-uid="' + event.message.uid + '"]').length == 1) { // и диалог с пользователем существует
            if (event.message.typing == 'on') {
                $('#dialogs-modal li.dialog-list__item[data-uid="' + event.message.uid + '"]').addClass('dialog-list__item--typing');
            } else {
                $('#dialogs-modal li.dialog-list__item[data-uid="' + event.message.uid + '"]').removeClass('dialog-list__item--typing');
            }
        }
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.events = {
    eventsUri: "/events/",
    params: {
        api_id: 1,
        format: 'json',
        method: 'getEvent',
        rnd: Math.random(),
        wait: 25
    },
    timer: 0,
    updateTimer: function() {
        if (ExeRu.events.timer !== 0) {
            clearTimeout(ExeRu.events.timer);
        }
        setTimeout(function() {
            $(".report-block").removeClass("bounceInRight");
            $(".report-block").addClass("animated bounceOutRight");
        }, 5000);
    },
    init: function() {
        $.getJSON(ExeRu.events.eventsUri, ExeRu.events.params, function(r) {
            ExeRu.events.params.rnd = Math.random();
            ExeRu.events.params.ts = r.ts;
            setTimeout(ExeRu.events.init, 1000);
            if (r.events.length > 0) {
                ExeRu.events.parse(r.events);
            }
        });
        return false;
    },
    acceptUser: function() {
        var params = {
            sid: sid,
            gid: gid,
            rnd: Math.random()
        };
        $.ajax({
            method: "GET",
            url: '/acceptUser',
            dataType: 'json',
            data: params
        });
    },
    parse: function(events) {
        $(events).each(function(i, event) {
            switch(event.type) {
                case 'online': {
                    var html = $(new EJS({url:'/assets/js/views/online.ejs?' + ExeRu.version}).render(event));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('#online-events').html(html);
                    ExeRu.users.setOnline(event.uid);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'invite_to_friend_by_game': {
                    var html = $(new EJS({url:'/assets/js/views/inviteToFriendByGame.ejs?' + ExeRu.version}).render(event.message));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('.invite-to-game', html).on('click', ExeRu.call);

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'request_to_friends': {
                    var html = $(new EJS({url:'/assets/js/views/notify_new_request_to_friends.ejs?' + ExeRu.version}).render(event.message));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'invite_to_game': {
                    var html = $(new EJS({url:'/assets/js/views/inviteToGame.ejs?' + ExeRu.version}).render(event.message));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('.invite-to-game', html).on('click', ExeRu.call);

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'new_message': {
                    ExeRu.dialogs.parseEventNewMessage(event);
                    ExeRu.events.updateTimer();
                    break;
                }
                case 'has_read_message': {
                    ExeRu.dialogs.parseEventHasRead(event);
                    break;
                }
                case 'user_is_typing': {
                    ExeRu.dialogs.parseEventTyping(event);
                    break;
                }
                case 'balance_change': {
                    var balanceTxt = plural_str(event.message.balance, ' рубль', ' рубля', ' рублей');
                    $('.balance span:first-child').html(event.message.balance);
                    $('.balance span:last-child').html(balanceTxt);

                    if (event.message.type == 'increment') {
                        var html = $(new EJS({url:'/assets/js/views/new_balance.ejs?' + ExeRu.version}).render({balance: event.message.balance + balanceTxt}));

                        $(".report-close", html).click(function(e){
                            e.preventDefault();
                            $(".report-block").removeClass("bounceInRight");
                            $(".report-block").addClass("animated bounceOutRight");
                        });

                        $('#online-events').html(html);

                        ExeRu.events.updateTimer();
                    }


                    if (!event.message.orderNumber || event.message.orderNumber != ExeRu.payments.windowOrderId) {
                        return;
                    }

                    ExeRu.payments.windowOrderId = 0;

                    ExeRu.payments.unsetWaitPayment();
                    break;
                }
                case 'balance_change_bonus': {
                    var balanceTxt = plural_str(event.message.balance, ' рубль', ' рубля', ' рублей');
                    $('.balance span:first-child').html(event.message.balance);
                    $('.balance span:last-child').html(balanceTxt);

                    var html = $(new EJS({url:'/assets/js/views/bonus.ejs?' + ExeRu.version}).render({balance: event.message.balance + balanceTxt}));

                    $(".report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();
                    break;
                }
                case 'purchase_restore': {
                    if (event.message.sid != gid) {
                        return false;
                    }

                    if (event.message.order_id && event.message.order_id == ExeRu.payments.windowOrderId) {
                        ExeRu.payments.unsetWaitPayment();
                    }

                    if (ExeRu.payments.windowBuyProccess) {
                        ExeRu.payments.showBuy(event.message);
                    }

                    if (parseInt(event.message.balance) >= parseInt(event.message.price)) {
                        $('#buy-modal .form-submit-buy:button').trigger('click');
                        if (!ExeRu.payments.windowRef.closed) {
                            ExeRu.payments.windowRef.close();
                        }
                    }
                    break;
                }
                case 'panel': {
                    ExeRu.panel.parseEvent(event);
                    break;
                }
                case 'to_blacklist':
                case 'from_blacklist': {
                    ExeRu.users.deleteFromCache(event.message.uid);
                    break;
                }
                case 'blocked': {
                    window.location.href = '/blocked';
                    break;
                }
                default: {
                    console.debug(event);
                }
            }
        });
    }
};
setInterval(ExeRu.events.acceptUser, 60000);
/*var ExeRu = ExeRu || {};*/
ExeRu.friends = {
    urlSearch: '/ajax/search_friends/',
    urlGetFriends: '/ajax/getFriends/',
    urlGetFriendsCount: '/ajax/getFriendsCount/',

    urlAcceptRequest: '/ajax/acceptRequest/',
    urlRejectRequest: '/ajax/rejectRequest/',

    urlRequestToFriends: '/ajax/appendFriend/',
    urlCancelRequest: '/ajax/cancelRequest/',

    urlGetFriendsForInvite: '/ajax/getFriendsForInvite/',
    urlInviteFriendsToGame: '/ajax/inviteToGame/',

    urlParseFriendsByGame: '/ajax/parseFriendsByGame/',
    urlRequestToFriendsByGame: '/ajax/requestToFriendsByGame',

    urlNotificationAppend: '/ajax/notificationAppend',

    init: function() {
        // Кнопка поиска в модельном окне друзей
        $('#friends-modal .modal__search--submit-wrap').on('click', ExeRu.friends.search);
        // Кнопка фильтра в пригласить друзей в игру
        $('#invite-friends-modal .modal__search--submit-wrap').on('click', ExeRu.friends.filterInviteFriends);
        // Кнопка фильтра в найти друзей по игре
        $('#search-friends-modal .modal__search--submit-wrap').on('click', ExeRu.friends.filterSearchFriendsByGame);
    },

    filterSearchFriendsByGame: function(e) {
        ExeRu.friends.applyGameFilter('#search-friends-modal', e.target);
        return false;
    },

    applyGameFilter: function(parentSelector, elementTarget) {
        var parent = $(parentSelector);
        var text = $(elementTarget).parents('.modal__search').find('input:text').val().trim();
        $(elementTarget).parents('.modal__search').find('input:text').val(text);

        var lis = $('li', parent);
        if (lis.length < 1) { // Некого приглашать.
            return false;
        }

        $('.friends-search-wrap ul', parent).show();
        $('.friends-search-wrap .dialogs_list-empty--header', parent).hide();

        if (text.length < 1) { // Пустой поиск - сброс фильтра
            $('li', parent)
                .removeClass('topline')
                .show();

            $('li:lt(3)', parent)
                .addClass('topline');
            return false;
        }

        var parts = text.split(' ');
        var textSelector = '.request-list__who__name';

        lis.removeClass('topline');
        lis.each(function(){
            var text = $(this).find(textSelector).text().toLowerCase();
            var found = true;
            for(var i=0; i<parts.length; ++i) {
                if (parts[i] == '') {
                    continue;
                }

                var part = parts[i].toLowerCase();
                if (-1 == text.indexOf(part)) {
                    found = false;
                    break;
                }
            }
            if (found == false) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        if ($('li:visible', parent).length == 0) {
            if ($('.dialogs_list-empty--header', parent).length == 0) {
                parent.find('.friends-search-wrap').append('<div class="dialogs_list-empty--header">Ничего не найдено.</div>');
            }
            $('.friends-search-wrap ul', parent).hide();
            $('.friends-search-wrap .dialogs_list-empty--header', parent).show();
        } else {
            $('li:visible:lt(3)', parent).addClass('topline');
        }
    },

    requestToFriendsByGame: function() {
        var inputs = $('#search-friends-modal .request-list__who input:checked:visible');
        if (inputs.length < 1) {
            return false;
        }

        ExeRu.inProgress.on();

        var params = inputs.serializeArray();
        params.push({name:'sid', value: gid});

        ExeRu.request.post(ExeRu.friends.urlRequestToFriendsByGame, params, function(r) {
            var button = $('#search-friends-modal [data-method="requestToFriendsByGame"]')[0];
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                $('#search-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
                $('#search-friends-modal').on('closeDialog', {type:'showPossibleFriends', data:r}, ExeRu.friends.apiReturnData);
            }

            $('#search-friends-modal [data-modal-close]').click();
            ExeRu.inProgress.off();
        });
    },

    searchFriendsByGame: function() {
        var root = $('#search-friends-modal');
        var button = $('[data-method="requestToFriendsByGame"]', root)[0];
        if (root[0].hasAttribute('data-source') && root[0].getAttribute('data-source') === 'api') {
            root[0].removeAttribute('data-source');
            button.setAttribute('data-source', 'api');
        } else {
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                button.removeAttribute('data-source');
            }
        }

        $('#search-friends-modal .modal__search input:text').val(''); // Сбрасываем фильтр

        $('#search-friends-modal .friends-active__count').html('0'); // Счетчик выбранных устанавливаем на 0

        var params = {
            api_id: 1,
            format: 'json',
            sid: gid,
            method: 'showPossible'
        };
        ExeRu.inProgress.on();
        ExeRu.request.get('/api/', params, function(r) {
            var uids = [];

            if (r.response.length > 0) {
                for (var i=0; i < r.response.length; ++i) {
                    uids.push(r.response[i].uid);
                }
            }

            ExeRu.request.get(ExeRu.friends.urlParseFriendsByGame, {uids: uids}, function(r) {
                ExeRu.friends.renderSearchFriendsByGame(r.result);
                ExeRu.inProgress.off();
            });

        });
    },

    renderSearchFriendsByGame: function(response) {
        var root = $('#search-friends-modal');

        if (response.length < 1) {
            if ($('.dialogs_list-empty--header', root).length == 0) {
                $('.friends-search-wrap', root).append('<div class="dialogs_list-empty--header">Вам пока некого пригласить.</div>');
            }
            $('.friends-search-wrap ul', root).hide();
            $('.friends-search-wrap .dialogs_list-empty--header', root).show();
            return false;
        }

        $('.friends-search-wrap .dialogs_list-empty--header', root).hide();
        $('.friends-search-wrap ul', root).show();

        //$('.friends-search', root).mCustomScrollbar();

        var html = new EJS({url:'/assets/js/views/searchFriendsByGame.ejs?' + ExeRu.version}).render({friends: response});
        $('.friends-search', root).html(html);

        $('.request-list__who:not(.request-list__who--played)', root).on('click', function() {
            var count = $('#search-friends-modal .request-list__who:not(.request-list__who--played) input:checked').length;
            $('#search-friends-modal .friends-active__count').html(count);
        });
    },

    filterInviteFriends: function(e) {
        ExeRu.friends.applyGameFilter('#invite-friends-modal', e.target);
        return false;
    },

    apiSearchFriendsByGame: function() {
        $('#search-friends-modal')[0].setAttribute('data-source', 'api');
        $('[data-method="searchFriendsByGame"]')[0].click(); // TODO Костыль, позволяет избежать двух вызовов метода. Надо переделать.
        $('#search-friends-modal').on('closeDialog', {type:'showPossibleFriends', data:[]}, ExeRu.friends.apiReturnData);
    },

    apiInviteFriends: function(e) {
        var root = $('#invite-friends-modal'), data = e.data;

        if (data && data.request_key && data.request_key.length > 0) {
            root[0].setAttribute('data-request_key', data.request_key);
        }

        root[0].setAttribute('data-source', 'api');

        $('[data-method="inviteFriends"]')[0].click();
        //root[0].click(); // TODO Костыль, позволяет избежать двух вызовов метода. Надо переделать.
        root.off('closeDialog', ExeRu.friends.apiReturnData);
        root.on('closeDialog', {type:'showInviteFriends', data:{response:[]}}, ExeRu.friends.apiReturnData);
    },

    apiReturnData: function(e) {
        $('#invite-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
        window.frames[0].postMessage({type:e.data.type, data:e.data.data}, '*');
    },

    inviteFriends: function(e) {
        var root = $('#invite-friends-modal');
        var button = $('[data-method="sendInviteFriends"]', root)[0];
        if (root[0].hasAttribute('data-source') && root[0].getAttribute('data-source') === 'api') {
            root[0].removeAttribute('data-source');
            button.setAttribute('data-source', 'api');
        } else {
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                button.removeAttribute('data-source');
            }
        }

        if (root[0].hasAttribute('data-request_key')) {
            button.setAttribute('data-request_key', root[0].getAttribute('data-request_key'));
            root[0].removeAttribute('data-request_key');
        } else {
            if (button.hasAttribute('data-request_key')) {
                button.removeAttribute('data-request_key');
            }
        }

        var params = {
            api_id: 1,
            format: 'json',
            sid: gid,
            method: 'showInvitible'
        };
        var apiResponse = null, friendsResponse = null;

        ExeRu.inProgress.on();

        ExeRu.request.get('/api/', params, function(r) {
            apiResponse = r;
            ExeRu.friends.renderInviteFriends(apiResponse, friendsResponse);
        });

        ExeRu.request.get(ExeRu.friends.urlGetFriendsForInvite, {sid: gid}, function(r) {
            friendsResponse = r;
            ExeRu.friends.renderInviteFriends(apiResponse, friendsResponse);
        });
    },

    renderInviteFriends: function(apiResponse, friendsResponse) {
        if (apiResponse == null || friendsResponse == null) {
            return false;
        }

        var root = $('#invite-friends-modal');

        if (friendsResponse.result.length < 1) {
            if ($('.dialogs_list-empty--header', root).length == 0) {
                $('.friends-search-wrap', root).append('<div class="dialogs_list-empty--header">Вам пока некого пригласить.</div>');
            }
            $('.friends-search-wrap ul', root).hide();
            $('.friends-search-wrap .dialogs_list-empty--header', root).show();
            ExeRu.inProgress.off();
            return false;
        }

        // Собираем из данных новый объект
        var users = {};
        for (i=0; i<apiResponse.response.length; ++i) {
            users[apiResponse.response[i].uid] = apiResponse.response[i];
        }

        for (var i=0; i<friendsResponse.result.length; ++i) {
            if (typeof(users[friendsResponse.result[i].uid]) == 'object') {
                friendsResponse.result[i].play = users[friendsResponse.result[i].uid].play;
            }
        }

        $('.friends-search-wrap .dialogs_list-empty--header', root).hide();
        $('.friends-search-wrap ul', root).show();

        var html = new EJS({url:'/assets/js/views/inviteFriends.ejs?' + ExeRu.version}).render({friends: friendsResponse.result});
        $('.friends-search', root).html(html);

        $('.modal__search input:text', root).val(''); // Сбрасываем фильтр

        $('.friends-active__count', root).html('0'); // Счетчик выбранных устанавливаем на 0
        $('.request-list__who:not(.request-list__who--played)', root).on('click', function() {
            var count = $('#invite-friends-modal .request-list__who:not(.request-list__who--played) input:checked').length;
            $('#invite-friends-modal .friends-active__count').html(count);
        });

        ExeRu.inProgress.off();
    },

    sendInviteFriends: function(eventElement) {
        var inputs = $('#invite-friends-modal .request-list__who:not(.request-list__who--played) input:checked:visible');
        if (inputs.length < 1) {
            return false;
        }

        ExeRu.inProgress.on();

        var params = inputs.serializeArray();
        params.push({name:'sid', value: gid});

        var button = $('#invite-friends-modal [data-method="sendInviteFriends"]')[0];

        if (button.hasAttribute('data-request_key')) {
            params.push({name:'request_key', value: button.getAttribute('data-request_key')});
        }

        ExeRu.request.post(ExeRu.friends.urlInviteFriendsToGame, params, function(r) {
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                $('#invite-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
                $('#invite-friends-modal').on('closeDialog', {type:'showInviteFriends', data:{response:r.users}}, ExeRu.friends.apiReturnData);
            }
            $('#invite-friends-modal [data-modal-close]').click();
            ExeRu.inProgress.off();
        });
    },

    // Профиль другого пользователя, перерисовка блока всех друзей и общих друзей по фильтру
    showFriends: function(element) {
        var uid = element.getAttribute('data-uid');
        var filter = element.getAttribute('data-filter');
        var user = ExeRu.users.getUserData(uid, true);
        var result = [];

        if (filter != 'search') {
            $(element).parents('.modal__search__list').find('a').removeClass('active');
            $(element).addClass('active');
            $(element).parents('.modal__search').find('.modal__search--text').val('');
        }

        var type = $(element).parents('.modal__search').find('a.active').data('filter');
        var text = $(element).parents('.modal__search').find('.modal__search--text').val().trim();

        if (type == '') {
            result = user.friends;
        } else if (type == 'both') {
            if (user.friends.length > 0) {
                for(i=0; i<user.friends.length; ++i) {
                    if (user.friends[i].friendType == type) {
                        result.push(user.friends[i]);
                    }
                }
            }
        }

        var friends = [];

        if (result.length > 0) {
            if (text.length < 3) {
                var friends = result;
            } else {
                var textParts = text.toLowerCase().split(' ');
                for(i=0; i<result.length; ++i) {
                    var found = true;
                    var sourceString = (result[i].name + ' ' + result[i].last_name).toLowerCase();
                    for(j=0; j<textParts.length; ++j) {
                        if (sourceString.indexOf(textParts[j]) == -1) {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        friends.push(result[i]);
                    }
                }
                type = 'search';
            }
        }

        var html = new EJS({url:'/assets/js/views/userProfile_friends.ejs?' + ExeRu.version}).render({friends: friends, listType: type});

        $(element).parents('.tabControl').find('.friends-search-wrap').html(html);

        $('#user-modal .tabControl .friends-search-wrap [data-module][data-method]').on('click', function() {
            $('html, body').animate({scrollTop: 0}, 1000);
            var module = this.getAttribute('data-module');
            var method = this.getAttribute('data-method');
            if (module !== null && method !== null) {
                ExeRu[module][method](this);
            }
            return false;
        });
    },

    // Подтвердить дружбу (с генерацией новости)
    userProfileAcceptRequest: function(element) {
        return ExeRu.friends.userProfileDoRequest(ExeRu.friends.urlAcceptRequest, element);
    },
    // Добавить в друзья (подписаться)
    userProfileRequestToFriends: function(element) {
        return ExeRu.friends.userProfileDoRequest(ExeRu.friends.urlRequestToFriends, element);
    },
    // Удалить из друзей, отменить заявку
    userProfileCancelRequest: function(element) {
        return ExeRu.friends.userProfileDoRequest(ExeRu.friends.urlCancelRequest, element);
    },
    // Удалить из черного списка
    userProfileDeleteFromBlackList: function(element) {
        ExeRu.inProgress.on();
        var parent = $(element).parents('.friend-type')[0];
        var uid = parent.getAttribute('data-uid');
        ExeRu.users.deleteFromCache(uid);
        ExeRu.request.get(ExeRu.users.urlFromBlackList, {uid: uid}, function(r) {
            if (r.location !== undefined) {
                ExeRu.users.getUser(parent);
                ExeRu.inProgress.off();
            }
        });
        return false;
    },
    // Действия практически идентичные, можно убрать в одну обвертку
    userProfileDoRequest: function(url, element) {
        ExeRu.inProgress.on();
        var parent = $(element).parents('.friend-type')[0];
        var params = {
            fid: parent.getAttribute('data-uid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(url, params, function(r) {
            if (r.response == 1) {
                ExeRu.users.getUser(parent);
                ExeRu.inProgress.off();
            }
        });
        return false;
    },

    // Заявки в друзья - принять заявку
    meAcceptRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlAcceptRequest, params, function(r) {
            ExeRu.friends.renderListNoReload(r);
            var length = parseInt($('#friends-modal .modal-tabs__list a:first span').html()) + 1;
            $('#friends-modal .modal-tabs__list a:first span').html(length);
        });
    },

    // Заявки в друзья - отклонить заявку
    meRejectRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlRejectRequest, params, ExeRu.friends.renderListNoReload);
    },

    // Мои заявки - отменить мою заявку
    myRequestsCancelRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlCancelRequest, params, ExeRu.friends.renderListNoReload);
    },

    // Отрисовывает списки без перезагрузки. Меняет значения в табах. Если пусто - переходит на друзей
    renderListNoReload: function(r) {
        if (r.response == 1) {
            var element = $('[data-fid="' + r.fid + '"]').parents('li');
            var parent = element.parent();
            element.remove();

            var length = parseInt($('#friends-modal .modal-tabs__list a.active span').html()) - 1;
            $('#friends-modal .modal-tabs__list a.active span').html('+' + length);

            if (length == 0) { // Не осталось элементов
                $('#friends-modal .modal-tabs__list li:first-child a').click();
            } else {
                //$('#friends-modal .modal-tabs__list a.active').click();
            }

            var showMore = parent.parents('.modal__content').find('.dev-billing__history-more a');
            if (showMore.length == 1) {
                oldValue = showMore.attr('data-count');
                showMore.attr('data-start', (parseInt(showMore.attr('data-start')) - 1));
                showMore.attr('data-count', 1);
                showMore.click();

                showMore.attr('data-count', oldValue);
            }

            ExeRu.inProgress.off();

            return false;
        }
    },

    // Поиск: Мне прислали заявку, я согласен дружить
    searchAcceptRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlAcceptRequest, params, function(r) {
            if (r.response == 1) {
                ExeRu.inProgress.off();
                $('#friends-modal .modal__search--submit-wrap').click();
            }
        });
    },

    // Поиск: Мне прислали заявку, я НЕ согласен с ним дружить (такого действия в поиске нет)
    searchRejectRequest: function(eventElement) {},

    // Поиск: Я отправляю заявку на дружбу с этим человеком
    searchRequestToFriends: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlRequestToFriends, params, function(r) {
            if (r.response == 1) {
                ExeRu.inProgress.off();
                $('#friends-modal .modal__search--submit-wrap').click();
            }
        });
    },

    // Поиск: Я отменяю свою заявку на дружбу с этим человеком
    searchCancelRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlCancelRequest, params, function(r) {
            if (r.response == 1) {
                ExeRu.inProgress.off();
                $('#friends-modal .modal__search--submit-wrap').click();
            }
        });
    },

    // Поиск: Я удаляю этого человека из друзей
    searchDeleteFromFriends: function(eventElement) {
        ExeRu.friends.searchCancelRequest(eventElement);
    },

    // Поиск
    search: function(e) {
        var text = $(e.target).parents('.modal__search').find('input:text').val();
        if (text.length < 3) {
            return false;
        }
        // Ставим тень
        ExeRu.inProgress.on();
        // Получаем найденных пользователей и отрисовываем блок
        ExeRu.request.get(ExeRu.friends.urlSearch, {search_string: text}, function(r) {
            // Перерисуем табы
            ExeRu.friends.renderTabs(r.tabs, 'tab_1');
            // Меняем содержимое окна
            ExeRu.friends.renderSearch(r);
            // Снимаем тень
            ExeRu.inProgress.off();
        });
        return false;
    },

    // Отрисовка содержимого блока поиска
    renderSearch: function(r) {
        var html = new EJS({url:'/assets/js/views/friendsSearch.ejs?' + ExeRu.version}).render(r);
        $('#friends-modal .friends-search-wrap').html(html);
        ExeRu.modal.init($('#friends-modal .friends-search-wrap'));
        ExeRu.init.buttons($('#friends-modal .friends-search-wrap'));
    },

    // Отрисовка табов в модальном окне друзей
    renderTabs: function(result, tabTarget) {
        var html = new EJS({url:'/assets/js/views/friendsTabs.ejs?' + ExeRu.version}).render(result);
        $('#friends-modal .modal__tabs').html(html);
        var activeTabElement = $('#friends-modal .modal-tabs__list .active');
        activeTabElement.removeClass('active');
        ExeRu.modal.init($('#friends-modal'));
        ExeRu.init.buttons($('#friends-modal'));
        $('#friends-modal .modal-tabs__list [data-tab-target="' + tabTarget + '"]').addClass('active');
    },

    // Отрисовка списка друзей
    renderList: function(eventElement) {

        ExeRu.inProgress.on();
        var tabTarget = eventElement.getAttribute('data-tab-target');
        if (tabTarget == null) {
            tabTarget = 'tab_1';
        }
        ExeRu.request.get(ExeRu.friends.urlGetFriendsCount, {}, function(r) {
            ExeRu.friends.renderTabs(r.result, tabTarget);

            var settings = {};

            if (tabTarget == 'tab_1') {
                settings.type = 'both';
                settings.template = 'friendsList';
                settings.count = 12;
                // Сбрасываем поиск
                $('#friends-modal input.modal__search--text').val('');
                if ($('#friends--both ul.request-list').length != 0) {
                    $('#friends--both ul.request-list').remove();
                }
            } else if (tabTarget == 'tab_2') {
                settings.type = 'me';
                settings.template = 'friendsRequest';
                settings.count = 5;
            } else if (tabTarget == 'tab_3') {
                settings.type = 'i';
                settings.template = 'friendsRequest';
                settings.count = 5;
            }

            if (r.result[settings.type] == 0) {
                var html = $(new EJS({url:'/assets/js/views/' + settings.template + '.ejs?' + ExeRu.version}).render({list:[], type: settings.type}));
                ExeRu.modal.init(html);
                ExeRu.init.buttons(html);
                $('#friends--' + settings.type).html(html);
                ExeRu.inProgress.off();
            } else {
                ExeRu.request.get(ExeRu.friends.urlGetFriends, {type:settings.type, start:0, count:settings.count}, function(list) {
                    var html = $(new EJS({url:'/assets/js/views/' + settings.template + '.ejs?' + ExeRu.version}).render({list:list, type: settings.type}));
                    ExeRu.modal.init(html);
                    ExeRu.init.buttons(html);
                    $('#friends--' + settings.type).html(html);
                    if (r.result[settings.type] > settings.count) {
                        var html = $(new EJS({url:'/assets/js/views/friendsButtonMore.ejs?' + ExeRu.version}).render(settings));
                        $('a', html).on('click', function() {
                            ExeRu.inProgress.on();
                            var settings = {
                                type: this.getAttribute('data-type'),
                                template: this.getAttribute('data-template'),
                                count: this.getAttribute('data-count'),
                                start: this.getAttribute('data-start')
                            };
                            ExeRu.request.get(ExeRu.friends.urlGetFriends, {type:settings.type, start:settings.start, count:settings.count}, function(list) {
                                var html = $(new EJS({url:'/assets/js/views/' + settings.template + '.ejs?' + ExeRu.version}).render({list:list, type: settings.type})),
                                    button = $('#friends--' + settings.type + ' .dev-billing__history-more');

                                $('li', html).each(function() {
                                    ExeRu.modal.init(this);
                                    ExeRu.init.buttons(this);
                                    $('#friends--' + settings.type + ' ul').append(this);
                                });


                                if (list.length < settings.count) {
                                    button.remove();
                                } else {
                                    $('a', button).attr('data-start', parseInt($('a', button).attr('data-start')) + parseInt(settings.count));
                                }
                                ExeRu.inProgress.off();
                            });
                            return false;
                        });
                        $('#friends--' + settings.type).append(html);
                    }
                    ExeRu.inProgress.off();
                });
            }
        });
    },
    showRequestBox: function(request) {
        var root = $('#add-request-modal'),
            user = ExeRu.users.getUserData(request.data.uid);

        root.find(".news-list__item__author p").html(request.data.text);
        root.find(".add-feed span").html(strip_by_length(user.name, 15) + ' ' + strip_by_length(user.last_name, 15));

        root.off('closeDialog');
        root.find('.btn-main').off('click');

        root.on('closeDialog', {}, function(e) {
            var root = $('#add-request-modal');
            root.off('closeDialog');
            root.find('.btn-main').off('click');
            window.frames[0].postMessage({type:'showRequestBox', data:{response:false}}, '*');
        });

        root.find('.btn-main').on('click', {data: request.data}, function(e) {
            ExeRu.inProgress.on();
            var root = $('#add-request-modal'),
                params = {
                    'sid':gid,
                    'data': e.data.data
                };
            root.off('closeDialog');
            root.find('.btn-main').off('click');

            $.get(ExeRu.friends.urlNotificationAppend, params, function(r){
                window.frames[0].postMessage({type:'showRequestBox', data:{response:true}}, '*');
                ExeRu.inProgress.off();
                $('#add-request-modal [data-modal-close]').click();
            });

            return false;
        });

        ExeRu.modal.dialogs['add-request-modal'].options.event = '';
        ExeRu.modal.dialogs['add-request-modal'].toggle();
    }
};

function listener(event) {
    switch(event.data.type) {
        case 'showRequestBox': {
            ExeRu.friends.showRequestBox(event.data);
            break;
        }
        case 'showInviteFriends': {
            ExeRu.friends.apiInviteFriends(event.data);
            break;
        }
        case 'showOrderBox': {
            ExeRu.payments.buy(event.data);
            break;
        }
        case 'showPublishBox': {
            ExeRu.news.showPublishBox(event.data);
            break;
        }
        case 'apiRequest': {
            $.ajax({
                method: "GET",
                url: '/api/',
                dataType: 'json',
                data: event.data.params
            }).success(function (r) {
                window.frames[0].postMessage({type:'apiResponse', data: r, callback: event.data.callback}, '*');
            }).error(function (r) {
                window.frames[0].postMessage({type:'apiResponse', data: false, callback: event.data.callback}, '*');
            });
            break;
        }
        case 'setHeight': {
            ExeRu.game.setHeight(event.data);
            break;
        }
        case 'setGood': {
            var yandexPixelId = false, epAlias = false;
            switch(event.data.data.app_id) {
                case 3:  { yandexPixelId = '5923452583841758097'; if (install == 1) epAlias='GrimmGood'; break;  } // Ферма Гримм
                case 4:  { yandexPixelId = '883514756404975728';  if (install == 1) epAlias='RodinaGood'; break;  } // Родина
                case 9:  { yandexPixelId = '1047551976226950972'; if (install == 1) epAlias='WarbannerGood'; break; } // Знамя войны
                case 10: { yandexPixelId = '3165387179987160625'; if (install == 1) epAlias='SturmGood'; break; } // Штурм
                case 11: { yandexPixelId = '8852841143843547337'; if (install == 1) epAlias='PiratesGood'; break; } // Гроза Морей
                case 12: { yandexPixelId = '2142528923122150705'; if (install == 1) epAlias='NanoGood'; break; } // Нано-Ферма
                case 14: { yandexPixelId = '7631959075412796562'; if (install == 1) epAlias='RoyalclashGood'; break; } // Королевский замес
                case 15: { yandexPixelId = '4196204346024485576'; if (install == 1) epAlias='RumbaGood'; break; } // Румба
                case 19: { yandexPixelId = '3564462929205210775'; if (install == 1) epAlias='BonVoyageGood'; break; } // Бон Вояж
                case 39: { yandexPixelId = '5213031403724456905'; if (install == 1) epAlias='HeroGood'; break; } // Путь война
                case 47: { yandexPixelId = '6431618609614690630'; if (install == 1) epAlias='MusicWarsGood'; break; } // Музвар
                case 62: { yandexPixelId = '3403263354588936414'; if (install == 1) epAlias='WarTankGood'; break; } // Битва Танков
                case 70: { yandexPixelId = '8850537116226743886'; if (install == 1) epAlias='NebesaGood'; break; } // Небеса
                case 76: { yandexPixelId = '2329184630858305291'; if (install == 1) epAlias='WalkingUndeadGood'; break; } // Ходячая Нежить
                case 89: { yandexPixelId = '3036045282580431516'; if (install == 1) epAlias='ImperialGood'; break; } // Империал Хироу
                case 97: { yandexPixelId = '8377556564264629938'; if (install == 1) epAlias='ObitelGood'; break; } // Обитель зла
                case 103: { yandexPixelId = '3216790040329473232'; if (install == 1) epAlias='VegaMixGood'; break; } // Вега Микс
                case 106: { yandexPixelId = '2879400923159599231'; if (install == 1) epAlias='ImperiaGood'; break; } // Империя Онлайн
                case 121: { yandexPixelId = '7766662350414205409'; if (install == 1) epAlias='StormGood'; break; } // Шторм
                case 142: { yandexPixelId = '7108745008784664529'; if (install == 1) epAlias='FazendaGood'; break; } // Фазенда
            }
            if (yandexPixelId !== false) {
                var yandexPixelImg = $('<div><img src="https://mc.yandex.ru/pixel/'+ yandexPixelId + '?rnd=' + Math.random() + '" style="position:absolute; left:-9999px;" alt="" /></div>');
                yandexPixelImg.prependTo('body');
            }
            if (epAlias !== false && epAlias !== '' && typeof epRiseEvent === 'function') {
                epRiseEvent(epAlias);
            }
            break;
        }
        case 'setSuperGood': {
            var yandexPixelId = false, epAlias = false;
            switch(event.data.data.app_id) {
                case 3:  { yandexPixelId = '5900136085920816917'; if (install == 1) epAlias='GrimmSupergood'; break;  } // Ферма Гримм
                case 4:  { yandexPixelId = '3604242323379749020'; if (install == 1) epAlias='RodinaSupergood'; break; } // Родина
                case 9:  { yandexPixelId = '6466355202244941666'; if (install == 1) epAlias='WarbannerSupergood'; break; } // Знамя войны
                case 10: { yandexPixelId = '6203494014439545446'; if (install == 1) epAlias='SturmSupergood'; break; } // Штурм
                case 11: { yandexPixelId = '5610260207562617698'; if (install == 1) epAlias='PiratesSupergood'; break; } // Гроза Морей
                case 12: { yandexPixelId = '1544819080505286572'; if (install == 1) epAlias='NanoSupergood'; break; } // Нано-Ферма
                case 14: { yandexPixelId = '8026351580886195610'; if (install == 1) epAlias='RoyalclashSupergood'; break; } // Королевский замес
                case 15: { yandexPixelId = '8978906350868819562'; if (install == 1) epAlias='RumbaSupergood'; break; } // Румба
                case 19: { yandexPixelId = '8274805363932425221'; if (install == 1) epAlias='BonVoyageSupergood'; break; } // Бон Вояж
                case 39: { yandexPixelId = '4133222358950810994'; if (install == 1) epAlias='HeroSupergood'; break; } // Путь война
                case 47: { yandexPixelId = '2265182111963594150'; if (install == 1) epAlias='MusicWarsSuperGood'; break; } // Музвар
                case 62: { yandexPixelId = '1557509870227811007'; if (install == 1) epAlias='WarTankSuperGood'; break; } // Битва Танков
                case 70: { yandexPixelId = '8381172897501154869'; if (install == 1) epAlias='NebesaSupergood'; break; } // Небеса
                case 76: { yandexPixelId = '6521968731721376546'; if (install == 1) epAlias='WalkingUndeadSupergood'; break; } // Ходячая Нежить
                case 89: { yandexPixelId = '7803724009774583264'; if (install == 1) epAlias='ImperialSupergood'; break; } // Империал Хироу
                case 97: { yandexPixelId = '2840090653474571545'; if (install == 1) epAlias='ObitelSupergood'; break; } // Обитель зла
                case 103: { yandexPixelId = '8485303420510273131'; if (install == 1) epAlias='VegaMixSuperGood'; break; } // Вега Микс
                case 106: { yandexPixelId = '3551126677975527595'; if (install == 1) epAlias='ImperiaSupergood'; break; } // Империя Онлайн
                case 121: { yandexPixelId = '3609716404667292401'; if (install == 1) epAlias='StormSupergood'; break; } // Шторм
                case 142: { yandexPixelId = '5614023024341370844'; if (install == 1) epAlias='FazendaSuperGood'; break; } // Фазенда
            }
            if (yandexPixelId !== false) {
                var yandexPixelImg = $('<div><img src="https://mc.yandex.ru/pixel/'+ yandexPixelId + '?rnd=' + Math.random() + '" style="position:absolute; left:-9999px;" alt="" /></div>');
                yandexPixelImg.prependTo('body');
            }
            if (epAlias !== false && epAlias !== '' && typeof epRiseEvent === 'function') {
                epRiseEvent(epAlias);
            }
            break;
        }
    }
}

if (window.addEventListener) {
    window.addEventListener("message", listener);
} else {
    // IE8
    window.attachEvent("onmessage", listener);
}
/*var ExeRu = ExeRu || {};*/
ExeRu.game = {
    urlGameSettings: '/ajax/setGameSettings/',
    urlGameDelete: '/ajax/deleteGame/',
    urlNotificationGrades: '/ajax/notificationGrade/',
    urlFriendsGrades: '/ajax/friendsGrade/',
    init: function() {
        // Удаление установленных игр
/*
        $('.my-games__item .news-list__item__delete').on('click', function() {
            ExeRu.game.deleteGame(this.getAttribute('data-relation'));
        });
*/


        // Уведомления от игр
        $('.notification .news-list__item__delete').on('click', function(e) {
            e.stopPropagation();
            ExeRu.game.negativeNotificationGrade($(this).parents('.notification')[0]);

        });
        $('.notification').on('click', function() {
            ExeRu.game.positiveNotificationGrade(this);
        });


        // Уведомления от друзей
        $('.friend-request .friend-request--game_link').on('click', function(e) {
            e.preventDefault();
            ExeRu.game.positiveFriendsGrade(this);
        });
        $('.friend-request .friend-request--game_link_delete').on('click', function(e) {
            e.preventDefault();
            ExeRu.game.negativeFriendsGrade(this);
        });


        if ($('.users-list-wrap').length > 0) {
            $.get('/api/', {api_id:1, format:'json', method:'whoPlay', type:1, sid:gid}, ExeRu.game.renderWhoPlay);

            setTimeout(function(){
                $.get('/api/', {api_id:1, format:'json', method:'whoPlay', type:1, sid:gid}, ExeRu.game.updateWhoPlay);
            }, 60000);

            $('.users-list__item--more').on('click', function(e) {
                e.stopPropagation();

/*
                if ($('#users-list-wrap-queue li').length < 5) {
                    $.get('/api/', {api_id:1, format:'json', method:'whoPlay', type:3, sid:gid}, ExeRu.game.updateWhoPlay);
                }

*/
                $('#users-list-wrap').trigger('forward');

                this.blur();

                return false;
            });
        }
    },
    renderWhoPlay: function(r) {
        var root = $('.users-list-wrap .users-list');
        root.html('');
        r.response.forEach(function(user) {
            var li;
            if (uid > 0) {
                li = $('<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="user-modal" data-module="users" data-method="getUser" data-uid="' + user.uid + '"><img src="' + user.photo_50 + '" /></a></li>');
            } else {
                li = $('<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="login-modal" data-module="users" data-method="setDestination" data-dest="-1"><img src="' + user.photo_50 + '" /></a></li>');
            }
            root.append(li);
        });
        ExeRu.modal.init(root);

        $('#users-list-wrap').scrollbox({
            linear: true,
            direction: 'h',
            switchItems: 1,
            queue: 'users-list-wrap-queue'
        });
    },
    updateWhoPlay: function(r) {
        var html = '';
        r.response.forEach(function(user) {
            if ($('#users-list-wrap [data-uid=63750], #users-list-wrap-queue [data-uid=' + user.uid + ']').length == 0) {
                if (uid > 0) {
                    html += '<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="user-modal" data-module="users" data-method="getUser" data-uid="' + user.uid + '"><img src="' + user.photo_50 + '" title="' + user.uid + '"/></a></li>';
                } else {
                    html += '<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="login-modal" data-module="users" data-method="setDestination" data-dest="24"><img src="' + user.photo_50 + '" /></a></li>';
                }
            }
        });
        if (html != '') {
            html = $(html);
            ExeRu.modal.init(html);

            $('#users-list-wrap-queue').append(html);
        }
    },
    positiveFriendsGrade: function(element) {
        ExeRu.inProgress.on();
        var params = {
            gid: element.getAttribute('data-gid'),
            fid: element.getAttribute('data-fid'),
            nid: element.getAttribute('data-nid'),
            request_key: element.getAttribute('data-rkey'),
            request_type: element.getAttribute('data-rtype'),
            type: 'positive'
        };
        ExeRu.game.request(ExeRu.game.urlFriendsGrades, params, function(r) {
            if (r.response == 1) {
                var location  = '/app' + r.game.id;
                window.location.href = location;
            }
        });
    },
    negativeFriendsGrade: function(element) {
        var params = {
            gid: element.getAttribute('data-gid'),
            fid: element.getAttribute('data-fid'),
            nid: element.getAttribute('data-nid'),
            type: 'negative'
        };
        ExeRu.game.request(ExeRu.game.urlFriendsGrades, params, function(r) {
            if (r.response == 1) {
                var element = $('.friend-request--game_link_delete[data-nid="' + r.game.nid + '"]').parents('.friend-request');
                // Если удаление из нижнего блока, или в нижнем блоке нет элементов, сдвигать ничего не нужно
                if ($(element).parents('.row--main--hide').length == 1 || $('.row--main--hide .friend-request--game_link_delete').length == 0) {
                    element.remove();
                } else { // Верхний блок, в нижнем есть элементы
                    var parent = $(element).parents('.friend-request-wrap');
                    element.remove();
                    parent.append($('.row--main--hide .friend-request:first'));
                }

                ExeRu.game.recountGradesBlock();
            }
        });
    },
    positiveNotificationGrade: function(element) {
        var params = {
            gid: element.getAttribute('data-gid'),
            nid: element.getAttribute('data-nid'),
            type: 'positive'
        };
        ExeRu.game.request(ExeRu.game.urlNotificationGrades, params, function(r) {
            if (r.response == 1) {
                var location  = '/app' + r.game.id;
                window.location.href = location;
            }
        });
    },
    negativeNotificationGrade: function(element) {
        var params = {
            gid: element.getAttribute('data-gid'),
            nid: element.getAttribute('data-nid'),
            type: 'negative'
        };
        ExeRu.game.request(ExeRu.game.urlNotificationGrades, params, function(r) {
            if (r.response == 1) {
                var element = $('.notification[data-nid="' + r.game.nid + '"]');
                // Если удаление из нижнего блока, или в нижнем блоке нет элементов, сдвигать ничего не нужно
                if ($(element).parents('.row--main--hide').length == 1 || $('.row--main--hide .notification').length == 0) {
                    element.remove();
                } else { // Верхний блок, в нижнем есть элементы
                    var parent = $(element).parent();
                    element.remove();
                    parent.append($('.row--main--hide .notification:first'));
                }

                ExeRu.game.recountGradesBlock();
            }
        });
    },
    recountGradesBlock: function() {
        var notificationLength = $('.notification').length;
        var friendRequestLength = $('.friend-request').length;

        if (notificationLength == 0 && friendRequestLength == 0) {
            $('.row--main:first').remove();
            $('.row--padding:first').removeClass('row--padding').addClass('row--main');
        } else {
            // Меняем количество игровых уведомлений
            if (notificationLength > 9) {
                var text = notificationLength;
            } else {
                var text = '0' + notificationLength;
            }
            $('.game__notification__count').html(text);

            // Меняем количество запросов от друзей
            if (friendRequestLength > 9) {
                var text = friendRequestLength;
            } else {
                var text = '0' + friendRequestLength;
            }
            $('.friends__notification__count').html(text);

            var sliderContainer = $('.row--main--hide');
            if (sliderContainer.length > 0) {
                if (notificationLength <= 2 && friendRequestLength <= 2) {
                    // Слайдер больше не нужен
                    $('.row--main--hide').remove();
                } else {
                    // Подтягиваем полоску слайдера, если блок развернут
                    if ($('.row__more--close:visible', sliderContainer).length == 1) {
                        var height = $('.row__more-panel', sliderContainer).height() - $('.row__more-panel .friend-request:first', sliderContainer).height() - 30;
                        $(".row__more-panel", sliderContainer).animate({
                            height: height
                        });
                    }
                }
            }
        }
    },
    showUnInstallGame: function(eventElement) {
        // Кнопка не удалять
        $('#uninstall-modal button[data-modal-close]')
            .off('click')
            .on('click', function() {
                $('#uninstall-modal div[data-modal-close]').click();
                return false;
            });

        // Кнопка удалить
        var params = { appId: $(eventElement).attr('data-gid') },
            type = $(eventElement).attr('data-type');

        $('#unInstall').off('click').on('click', function() {
            ExeRu.inProgress.on();

            if (type == 'main') {
                ExeRu.game.request(ExeRu.game.urlGameDelete, params, function(r) {
                    if (r.response == 1) {
                        var length = $('.my-games--user li').length;
                        var parent = $('[data-gid="' + r.appId + '"]').parent();
                        if ($('.my-games--user li').length > 1) {
                            length--;
                            if (length > 9) {
                                var text = length;
                            } else {
                                var text = '0' + length;
                            }
                            $(parent).parents('.row').find('.col__header__count').html(text); // Изменили количество
                            parent.remove(); // Удалили сам элемент
                        } else {
                            $(parent).parents('.row').remove();
                        }
                    }
                    $('#uninstall-modal div[data-modal-close]').click();
                    ExeRu.inProgress.off();
                });
            } else {
                ExeRu.game.request(ExeRu.game.urlGameDelete, params, function(r) {
                    if (r.response == 1) {
                        window.location.href = '/';
                    } else {
                        $('#uninstall-modal div[data-modal-close]').click();
                        ExeRu.inProgress.off();
                    }
                });
            }

            return false;
        });

        return false;
    },
    setHeight: function(request) {
        var height = parseInt(request.data);
        if (typeof request.data === "number" && height === request.data && height > 10 && $('.game iframe').length == 1) {
            $('.game iframe').attr('height', height);
            window.frames[0].postMessage({type:'apiResponse', data:{height:height}, callback:'setHeight'}, '*');
            return true;
        }

        window.frames[0].postMessage({type:'apiResponse', data:{response:false}, callback:'setHeight'}, '*');
        return false;
    },
    notification: {
        on: function() {
            var params = {
                gid: gid,
                type: 'notifications',
                set: 'on'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        },
        off: function() {
            var params = {
                gid: gid,
                type: 'notifications',
                set: 'off'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        }
    },
    invitible: {
        on: function() {
            var params = {
                gid: gid,
                type: 'invitible',
                set: 'on'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        },
        off: function() {
            var params = {
                gid: gid,
                type: 'invitible',
                set: 'off'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        }
    },
    request: function(url, params, callback) { // TODO Вырезать этот аппендикс.
        ExeRu.request.get(url, params, callback);
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.inProgress = {
    count: 0,
    on: function() {
        ++ExeRu.inProgress.count;
        if (ExeRu.inProgress.count != 1) {
            return false;
        }
        $('#inProgressShadow').show();
    },
    off: function() {
        --ExeRu.inProgress.count;
        if (ExeRu.inProgress.count != 0) {
            return false;
        }
        $('#inProgressShadow').hide();
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.init = {
    modules: function(container) {
        ExeRu.events.init(); // Ждем событий от сервера
        ExeRu.game.init();
        ExeRu.news.init();
        ExeRu.friends.init(); // Действия в модальном окне друзей.
        ExeRu.modal.init(container); // Диалоговые окна и переключение по табам
        ExeRu.dialogs.init();
    },
    buttons: function(container) {
        $('a.btn-main,.form-submit:button', container).on('click', ExeRu.call);
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.modal = {
    scrollWidth: null,
    dialogs: {},
    init: function(container) {
        if (ExeRu.modal.scrollWidth == null) {
            var body = $('body'),
                overflowY = body.css('overflow-y');

            ExeRu.modal.scrollWidth = -body[0].offsetWidth;
            body.css('overflow-y', 'hidden');
            ExeRu.modal.scrollWidth += body[0].offsetWidth;

            body.css('overflow-y', overflowY);
        }
        // Обработчики открытия/закрытия диалоговых окон
        var dlgtriggers = $('[data-modal]', container);
        if (dlgtriggers.length > 0) {
            for(i=0; i<dlgtriggers.length; i++) {
                id = dlgtriggers[i].getAttribute('data-modal');
                if (ExeRu.modal.dialogs[id] === undefined) {
                    ExeRu.modal.dialogs[id] = new DialogFx(document.getElementById(id),
                        {
                            onOpenDialog : function(dialogObject) {
                                $("body").css({'overflow-y':'hidden', 'padding-right': ExeRu.modal.scrollWidth + 'px'});
                                if (dialogObject.el.hasAttribute('no-push-history')) {
                                    dialogObject.el.removeAttribute('no-push-history');
                                } else {
                                    if (dialogObject.el.id == 'news-modal') {
                                        history.pushState({}, '', '/news');
                                    } else if (dialogObject.el.id == 'dialogs-modal') {
                                        history.pushState({}, '', '/dialogs');
                                    } else if (dialogObject.el.id == 'friends-modal') {
                                        history.pushState({}, '', '/friends');
                                    } else if (dialogObject.el.id == 'settings-modal') {
                                        history.pushState({}, '', '/settings');
                                    } else if (dialogObject.el.id == 'help') {
                                        history.pushState({}, '', '/help');
                                    }
                                }

                                $(dialogObject.el).trigger('openDialog');
                                $('iframe').css('visibility', 'hidden');

                                var eventElement = dialogObject.options.event;
                                if (typeof(eventElement) == 'object') {
                                    var module = eventElement.getAttribute('data-module');
                                    var method = eventElement.getAttribute('data-method');

                                    if (module !== null && method !== null) {
                                        ExeRu[module][method](eventElement, dialogObject.el);
                                    }
                                }

                                $('a.icon__games.active').removeClass('active');

                                return false;
                            },
                            onCloseDialog : function(dialogObject) {
                                if (pageUrl != '/') {
                                    $('a.icon__games').addClass('active');
                                }

                                $('iframe').css('visibility', 'visible');
                                history.pushState({}, '', pageUrl);
                                $(dialogObject.el).trigger('closeDialog');
                                $(dialogObject.el).one('DialogFX_endAnimationClose', function(){
                                    $("body").css({'overflow-y':'auto', 'padding-right': '0'});
                                });

                                return false;
                            }
                        }
                    );
                }
                dlgtriggers[i].addEventListener('click', function(e) {
                    e.preventDefault();

                    currentName = this.getAttribute('data-modal');
                    $('.modal--open').each(function(){
                        if (this.id != currentName) {
                            $(ExeRu.modal.dialogs[this.id].el).trigger('closeDialog');
                            ExeRu.modal.dialogs[this.id].isOpen = false;
                            $('[data-modal="' + this.id + '"]').removeClass('active');
                            $(this).removeClass('modal--open');
                        }
                    });

                    ExeRu.modal.dialogs[currentName].options.event = this;
                    ExeRu.modal.dialogs[currentName].toggle();
                    if (currentName == 'login-modal') {
                        $('a.icon').removeClass('active');
                    }

                    return false;
                });
            }
        }
        // Обработка переключения по табам для диалоговых окон
        var tabTriggers = $('[data-tab-target]', container);
        if (tabTriggers.length > 0) {
            for(i=0; i<tabTriggers.length; i++) {
                $(tabTriggers[i]).on('click', function() {
                    if ($(this).hasClass('icon')) {
                        var parent = $('#' + this.getAttribute('data-modal') + ' .modal'),
                            currentElement = parent.find('[data-tab-target="' + this.getAttribute('data-tab-target') + '"]')[0],
                            canExec = $(this).hasClass('active');
                    } else {
                        var currentElement = this,
                            parent = $(currentElement).parents('.modal'),
                            canExec = true;
                    }

                    var tabControl = parent.find('.tabControl')[0];
                    if (tabControl.hasAttribute('data-tab-active')) {
                        $(tabControl).removeClass(tabControl.getAttribute('data-tab-active'));
                        parent.find('[data-tab-target=' + tabControl.getAttribute('data-tab-active') + ']').removeClass('active');
                    }
                    $(currentElement).addClass('active');
                    tabControl.setAttribute('data-tab-active', currentElement.getAttribute('data-tab-target'));
                    $(tabControl).addClass(currentElement.getAttribute('data-tab-target'));

                    if (currentElement.hasAttribute('data-module') && currentElement.hasAttribute('data-method') && canExec) {
                        var module = currentElement.getAttribute('data-module');
                        var method = currentElement.getAttribute('data-method');
                        if (module !== null && method !== null) {
                            ExeRu[module][method](currentElement);
                        }
                    }

                    $('#profile_main, #profile_access, form.sign-form', tabControl).each(function() {
                        $('label.error').hide();
                        this.reset();
                    });

                    return false;
                });

                if ($(tabTriggers[i]).hasClass('active')) {
                    $(tabTriggers[i]).click();
                }
            }
        }
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.news = {
    urlNewsDelete: '/ajax/deleteFeed/',
    urlNewsRestore: '/ajax/restoreFeed/',
    urlPublishEvent: '/ajax/publishFeed/',
    urlNewsGet: '/ajax/getFeeds/',
    init: function() {
        // Фильтр
        $('#news-modal .checkbox-list input').on('change', function() {
            if (this.checked) {
                $('#news-modal .modal__content').find('ul, div.type-all, div.type-game, div.type-user').hide();
                $('#news-modal .modal__content').find('ul.' + this.value + ', div.' + this.value).show();
            }
        });
        // Загрузка новостей при открытие окна

        $('#news-modal .checkbox-list input').on('click', function() {
            ExeRu.news.getNews(this);
        });

    },
    restoreNews: function(element) {
        var params = {
            fid: element.getAttribute('data-fid')
        };
        ExeRu.request.get(ExeRu.news.urlNewsRestore, params, function(r) {
            if (r.response == 1) {
                var message = $('.news-list__item__delete-message a[data-fid="' + r.fid + '"]').parent();
                message.parent().removeClass('news-list__item--delete');
                message.remove();
            }
        });
    },
    deleteNews: function(element) {
        var params = {
            fid: element.getAttribute('data-fid')
        };
        ExeRu.request.get(ExeRu.news.urlNewsDelete, params, function(r) {
            if (r.response == 1) {
                var html = '<div class="news-list__item__delete-message">';
                html += '<p>Запись удалена</p><a href="#" data-fid="' + r.fid + '">Восстановить</a>';
                html += '</div>';

                var parent = $('#news-modal [data-fid="' + r.fid + '"]').parent();
                parent.addClass('news-list__item--delete');
                parent.prepend(html);
                $('.news-list__item__delete-message a', parent).on('click', function(e) {
                    e.preventDefault();
                    ExeRu.news.restoreNews(this);
                });
            }
        });
    },
    showPublishBox: function(request) {
        if (uid === 0) {
            $('[data-modal="login-modal"]').click();
            return;
        }
        var root = $('#add-feed-modal');
        root.find(".news-list__item__author p").html(request.data.text);
        root.find(".news-list__item__banner img").attr('src', request.data.image);

        root.off('closeDialog');
        root.find('.btn-main').off('click');

        root.on('closeDialog', {type:'showPublishBox', data:{response:false}}, ExeRu.news.apiReturnData);
        root.find('.btn-main').on('click', {data: request.data}, ExeRu.news.publish);

        ExeRu.modal.dialogs['add-feed-modal'].options.event = '';
        ExeRu.modal.dialogs['add-feed-modal'].toggle();
    },
    publish: function(event) {
        ExeRu.inProgress.on();

        var root = $('#add-feed-modal');
        root.off('closeDialog');

        // Отправляем запрос на сервер
        var params = {
            sid: gid,
            data: event.data.data
        };
        $.get(ExeRu.news.urlPublishEvent, params, function(r) {
            var root = $('#add-feed-modal');
            root.on('closeDialog', {type:'showPublishBox', data:{response:true}}, ExeRu.news.apiReturnData);

            ExeRu.inProgress.off();
            root.find('[data-modal-close]').click();
        });

        return false;
    },
    apiReturnData: function(e) {
        $('#invite-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
        window.frames[0].postMessage({type:e.data.type, data:e.data.data}, '*');
    },
    resetFilter: function() {
        $('#news-modal .checkbox-list input:first').trigger('click');
    },
    getNews: function(eventElement) {
        ExeRu.inProgress.on();

        var filter = eventElement.hasAttribute('data-type') ? eventElement.getAttribute('data-type') : eventElement.value;

        if (eventElement.hasAttribute('data-prev')) {
            var data = {
                next: $('#news-modal .modal__content ul.' + filter + ' li:last div:first').attr('data-fid'),
                filter: filter
            }
        } else {
            var data = {
                prev: $('#news-modal .modal__content ul.' + filter + ' li:eq(1) div:first').attr('data-fid') || 0,
                filter: filter
            }
        }

        ExeRu.request.get(ExeRu.news.urlNewsGet, data, function(r) {
            if (r.length > 0) {
                var root = $('#news-modal'),
                    parent;

                if ($('ul.' + filter, root).length > 0) {
                    parent = $('ul.' + filter, root);
                } else {
                    $('.modal__content div.' + filter, root).removeClass(filter).hide();

                    parent = $(new EJS({url:'/assets/js/views/newsNoEmpty.ejs?' + ExeRu.version}).render({type:filter}));
                    $('.modal__content', root).append(parent);
                }

                if (eventElement.hasAttribute('data-prev')) {
                    $('.dev-billing__history-more', parent).remove();
                } else {
                    r = r.reverse();
                }

                for(var i in r) {
                    var template = '', element;
                    if (typeof r[i].user == 'undefined') { // Новость игры (newsGame)
                        template = 'newsGame';
                    } else { // Блок новости пользователя
                        if (typeof r[i].game != 'undefined') {
                            if (r[i].type == 3) { // Достижение в игре (newsAchievement)
                                template = 'newsAchievement';
                            } else { // Установка игры (newsInstallGame)
                                template = 'newsInstallGame';
                            }
                        } else { // Добавление в друзья (newsAppendFriends)
                            template = 'newsAppendFriend';
                        }
                    }

                    element = $(new EJS({url:'/assets/js/views/' + template + '.ejs?' + ExeRu.version}).render({feed: r[i]}));

                    if (eventElement.hasAttribute('data-prev')) {
                        parent.append(element);
                    } else {
                        $('li:eq(0)', parent).after(element);
                    }

                    ExeRu.modal.init(element);

                    $('.news-list__item__delete', element).on('click', function() {
                        ExeRu.news.deleteNews(this);
                    });
                }

                if (r.length == 10) {

                    element = $(new EJS({url:'/assets/js/views/newsButtonMore.ejs?' + ExeRu.version}).render({type:filter}));
                    parent.append(element);

                    $('a', element).on('click', function() {
                        ExeRu.news.getNews(this);
                        return false;
                    });
                }
            } else {
                if (eventElement.hasAttribute('data-prev')) {
                    $('#news-modal ul.' + filter + ' .dev-billing__history-more').remove();
                }
            }

            ExeRu.panel.updateCount('news', 0);

            ExeRu.inProgress.off();
        });
    }
};
/**
 * Created by ruslan on 05.09.16.
 */
/*var ExeRu = ExeRu || {};*/
ExeRu.panel = {
    updateCount: function(type, count) {
        var element = $('.site-header__menu .icon__' + type + ' .panel__count');
        if (count == 0) {
            element
                .html(0)
                .hide();
        } else {
            element
                .html(count)
                .show();
        }
    },
    parseEvent: function(event) {
        var element = $('.site-header__menu .icon__' + event.subtype + ' .panel__count'),
            count;

        switch(event.subtype) {
            case 'news': {
                count = parseInt(element.html());
                if (event.action == 'add') {
                    count++;
                } else {
                    count--;
                }

                if (count < 0) {
                    count = 0;
                }
                break;
            }
            case 'friends':
            case 'comments': {
                count = event.count;
                break;
            }
        }


        ExeRu.panel.updateCount(event.subtype, parseInt(count));
    }
};

/**
 * Created by ruslan on 23.12.15.
 */
/*var ExeRu = ExeRu || {};*/
ExeRu.payments = {
    urlPrepare: '/ajax/orderPrepare/',
    urlBuyPrepare: '/ajax/buyPrepare/',
    urlBuy: '/ajax/buy/',
    urlBuyRemember: '/ajax/buyRemember/',
    urlBuyCancel: '/ajax/buyCancel/',

    windowRef: null,
    windowWait: false,
    windowTimerId: null,
    windowBuyProccess: false,
    windowOrderId: 0,

    rememberAndDoPayment: function(event) {
        var data = event.data.data;
        data['sid'] = gid;

        // Проставляем другое количество в форме напрямую
        $('#payment-uniform-modal').attr('data-preset-count', (data.price - data.balance));
        $('#payment-uniform-modal [name="purchaseId"]').val(data.order_id);
        ExeRu.payments.windowOrderId = data.order_id;

        ExeRu.request.get(ExeRu.payments.urlBuyRemember, {data: data});

        $('#buy-modal').off('closeDialog');
        $('#buy-modal .form-submit-buy:button').off('click');

        $('[data-modal="payment-modal"]:first')[0].click();

        $('#payment-modal, #payment-uniform-modal, #payment-phone-modal, #payment-system-modal, #payment-terminal-modal').on('closeDialog', function() {
            var opened = $('.modal--open');
            if (opened.length == 0) {
                ExeRu.payments.buyCancel();
            }
        });

    },
    preSetCount: function(eventElement) {
        var value = eventElement.getAttribute('data-preset-count');
        $('#payment-uniform-modal').attr('data-preset-count', value);
    },
    showBuy: function(r) {
        console.debug(r);
        if (typeof(r.error) === 'object' && r.error.code != 200) {
            console.debug('Error: ');
            console.debug(r.error);
            window.frames[0].postMessage({type:'showOrderBox', data:{"result":"fail"}}, '*');
        } else {
            if (!ExeRu.payments.windowBuyProccess) {
                ExeRu.payments.windowBuyProccess = true;
            }
            $('#buy-modal').off('closeDialog');
            $('#buy-modal .form-submit-buy:button').off('click');

            $('#buy-modal .modal__header h2').html('Покупка ' + r.title);
            $('#buy-modal .buy-image img').attr('src', r.photo_url);

            $('#buy-modal .buy-balance span').html(r.balance + plural_str(r.balance, ' рубль', ' рубля', ' рублей'));
            $('#buy-modal .buy-what span').html(r.title);
            $('#buy-modal .buy-cost span').html(r.price + plural_str(r.price, ' рубль', ' рубля', ' рублей'));

            $('#buy-modal').on('closeDialog', {}, ExeRu.payments.buyCancel);

            $('#buy-modal .form-submit').attr('data-purchase-id', r.order_id);

            if (typeof(r.error) === 'object' && r.error.code == 200
                || parseInt(r.balance) < parseInt(r.price) ) {
                $('#buy-modal .buy-no-money').show();

                if (typeof(r.testing_payments) !== 'undefined' && r.testing_payments == 'allowed' && $('#test_payment').length == 1) {
                    $('#test_payment').on('change', function() {
                        if (this.checked) {
                            $('#buy-modal .form-submit-buy:button').html('Купить');
                            $('#buy-modal .form-submit-buy:button').off('click').on('click', {data: r}, ExeRu.payments.buyButton);
                        } else {
                            $('#buy-modal .form-submit-buy:button').html('Пополнить баланс');
                            $('#buy-modal .form-submit-buy:button').off('click').on('click', {data: r}, ExeRu.payments.rememberAndDoPayment);
                        }
                    });
                    $('#test_payment').trigger('change');
                } else {
                    $('#buy-modal .form-submit-buy:button').html('Пополнить баланс');
                    $('#buy-modal .form-submit-buy:button').on('click', {data: r}, ExeRu.payments.rememberAndDoPayment);
                }

            } else {
                $('#buy-modal .buy-no-money').hide();
                $('#buy-modal .form-submit-buy:button').html('Купить');
                $('#buy-modal .form-submit-buy:button').on('click', {data: r}, ExeRu.payments.buyButton);
            }

            $('[data-modal="buy-modal"]').click();
        }
    },
    buy: function(data) {
/*        if (uid == 0) {
            $('[data-modal="login-modal"]:first')[0].click();
            window.frames[0].postMessage({type:'showOrderBox', data:{"response":"cancel"}}, '*');
            return false;
        }*/
        ExeRu.inProgress.on();
        data['sid'] = gid;

        ExeRu.request.get(ExeRu.payments.urlBuyPrepare, data, function(r) {
            ExeRu.payments.showBuy(r);
            ExeRu.inProgress.off();
        });
    },
    buySuccess: function(r) {
        $('#buy-success-modal .buy-image img').attr('src', r.photo_url);
        $('#buy-success-modal .buy-what span').html(r.title);
        if ($('#test_payment').length == 1 && $('#test_payment')[0].checked) {
            $('#buy-success-modal .buy-cost span').html('0 (тестовый платеж)');
        } else {
            $('#buy-success-modal .buy-cost span').html(r.price + plural_str(r.price, ' рубль', ' рубля', ' рублей'));
        }

        $('[data-modal="buy-success-modal"]').click();
    },
    buyButton: function(event) {
        var eventData = event.data.data, data = {
            sid: gid,
            item_id: eventData.item_id,
            order_id : eventData.order_id
        };
        if ($('#test_payment').length == 1 && $('#test_payment')[0].checked) {
            data['test_payment'] = 1;
        }

        ExeRu.inProgress.on();

        $('#buy-modal .form-submit-buy:button').off('click', ExeRu.payments.buyButton);

        ExeRu.request.get(ExeRu.payments.urlBuy, data, function(r) {
            $('#buy-modal').off('closeDialog', ExeRu.payments.buyCancel);

            window.frames[0].postMessage({type:'showOrderBox', data:{result: r.result}}, '*');

            ExeRu.inProgress.off();
            if (typeof(r) === 'object' && r.result == 'success') {
                ExeRu.payments.buySuccess(eventData);
                var yandexPixelId = false, yandexGoal = false; epAlias = false;
                switch(r.app_id) {
                    case 3:  { yandexPixelId = '8985851700733783520'; yandexGoal = 'payment_grimm'; epAlias = 'GrimmPay'; break; } // Ферма Гримм
                    case 4:  { yandexPixelId = '3118139205116343349'; yandexGoal = 'payment_Rodina'; epAlias = 'RodinaPay'; break; } // Родина
                    case 9:  { yandexPixelId = '548869107449420013'; yandexGoal = 'payment_warbanner';  epAlias = 'WarbannerPay'; break; } // Знамя войны
                    case 10: { yandexPixelId = '3030271326504040087'; yandexGoal = 'payment_sturm'; epAlias = 'SturmPay'; break; } // Штурм
                    case 11: { yandexPixelId = '4014119551221737540'; yandexGoal = 'payment_groza'; epAlias = 'PiratesPay'; break; } // Гроза Морей
                    case 12: { yandexPixelId = '780409584648124763'; yandexGoal = 'payment_nano';  epAlias = 'NanoPay'; break; } // Нано-Ферма
                    case 14: { yandexPixelId = '5272730995427958504'; yandexGoal = 'payment_zames'; epAlias = 'RoyalclashPay'; break; } // Королевский замес
                    case 15: { yandexPixelId = '4531528669980184328'; yandexGoal = 'payment_rumba'; epAlias = 'RumbaPay'; break; } // Румба
                    case 19: { yandexPixelId = '2572361535851200757'; yandexGoal = 'payment_bonvoyage'; epAlias = 'BonVoyagePay'; break; } // Бон Вояж
                    case 39: { yandexPixelId = '2895953050854365825'; yandexGoal = 'payment_hero'; epAlias = 'HeroPay'; break; } // Путь война
                    case 47: { yandexPixelId = '9205954418440378790'; yandexGoal = 'payment_muzwar'; epAlias = 'MusicWarsPay'; break; } // Музвар
                    case 62: { yandexPixelId = '1751294151258564301'; yandexGoal = 'payment_tanks'; epAlias = 'WarTankPay'; break; } // Битва Танков
                    case 70: { yandexPixelId = '740625241004169368'; yandexGoal = 'payment_nebesa';  epAlias = 'NebesaPay'; break; } // Небеса
                    case 76: { yandexPixelId = '5713467157391880223'; yandexGoal = 'payment_undead'; epAlias = 'WalkingUndeadPay'; break; } // Ходячая Нежить
                    case 89: { yandexPixelId = '395491761459506839'; yandexGoal = 'payment_imperialhero';  epAlias = 'ImperialPay'; break; } // Империал Хироу
                    case 97: { yandexPixelId = '564369819775036493'; yandexGoal = 'payment_obitel';  epAlias = 'ObitelPay'; break; } // Обитель зла
                    case 103:{ yandexPixelId = '2376258013296713328'; yandexGoal = 'payment_vegamix';  epAlias = 'VegaMixPay'; break; } // Вега Микс
                    case 106:{ yandexPixelId = '2813561034340998958'; yandexGoal = 'payment_imperia';  epAlias = 'ImperiaPay'; break; } // Империя Онлайн
                    case 121:{ yandexPixelId = '5315283837067113366'; yandexGoal = 'payment_storm';  epAlias = 'StormPay'; break; } // Шторм
                    case 142:{ yandexPixelId = '936329647689031917'; yandexGoal = 'payment_fazenda';  epAlias = 'FazendaPay'; break; } // Фазенда
                }
                if (yandexPixelId !== false) {
                    var yandexPixelImg = $('<div><img src="https://mc.yandex.ru/pixel/'+ yandexPixelId + '?rnd=' + Math.random() + '" style="position:absolute; left:-9999px;" alt="" /></div>');
                    yandexPixelImg.prependTo('body');
                }
                if (yandexGoal !== false && yandexGoal !== '') {
                    yaCounter41227294.reachGoal(yandexGoal);
                }
                if (epAlias !== false && epAlias !== '' && typeof epRiseEvent === 'function') {
                    epRiseEvent(epAlias);
                }
                yaCounter41227294.reachGoal('EXE_Payment');
            } else {
                alert('Не удалось завершить покупку');
                if (ExeRu.modal.dialogs['buy-modal'].isOpen) {
                    ExeRu.modal.dialogs['buy-modal'].toggle();
                }
            }
            ExeRu.payments.windowOrderId = 0;
            ExeRu.payments.windowBuyProccess = false;
        });
    },
    buyCancel: function(eventElement) {
        var data = {
            sid: gid,
            order_id: $('#buy-modal .form-submit-cancel').attr('data-purchase-id')
        };

        $('#test_payment').off('change');

        ExeRu.request.post(ExeRu.payments.urlBuyCancel, data);

        $('#buy-modal .form-submit').removeAttr('data-purchase-id');

        $('#payment-uniform-modal [name="purchaseId"]').val('');

        $('#buy-modal .form-submit-buy:button').off('click', ExeRu.payments.buyButton);
        $('#buy-modal').off('closeDialog', ExeRu.payments.buyCancel);
        $('#payment-modal, #payment-uniform-modal, #payment-phone-modal, #payment-system-modal, #payment-terminal-modal').off('closeDialog');
        window.frames[0].postMessage({type:'showOrderBox', data:{"result":"cancel"}}, '*');
        ExeRu.payments.windowBuyProccess = false;
        ExeRu.payments.windowOrderId = 0;
    },
    buyButtonCancel: function() {
        $('#buy-modal [data-modal-close]').click();
    },
    prepare: function(eventElement) {
        var form = $(eventElement).closest('form');

        if (form.find('[name="radiogroup"]:checked').length == 0) {
            eventElement.blur();
            return false;
        }

        var paymentType = form.find('[name="paymentType"]').val(),
            value = form.find('[name="radiogroup"]:checked').val();

        if (value == 'manual') {
            value = form.find('[name="count"]').val();
        }

        value = parseInt(value);

        if (isNaN(value) || value < 1) {
            eventElement.blur();
            return false;
        }

        if ((paymentType == 'MC' || paymentType == 'MTS' || paymentType == 'BEELINE' || paymentType == 'TELE2' || paymentType == 'MEGAFON') && value < 10) {
            alert('Минимальная сумма мобильного платежа 10 рублей.');
            eventElement.blur();
            return false;
        }

        if ((paymentType == 'PP') && value < 50) {
            alert('Минимальная сумма платежа 50 рублей.');
            eventElement.blur();
            return false;
        }

        if (value >= 100000) {
            alert('Максимально 99999 рублей одновременно.');
            eventElement.blur();
            return false;
        }

        ExeRu.inProgress.on();

        if (paymentType == 'QW' && eventElement.hasAttribute('data-pay-application')) {
            ExeRu.request.get(ExeRu.payments.urlPrepare, form.serialize(), function(r) {
                alert("После оплаты выставленного счета, рубли будут зачислены Вам на баланс.");
                $('#payment-uniform-modal:visible [data-modal-close]').click();
                ExeRu.inProgress.off();
            });
            return false;
        }


        // Готовим окно
        if (ExeRu.payments.windowRef != null) {
            ExeRu.payments.windowRef.close();
        }

        var params = '';
        ExeRu.payments.windowRef = window.open('', 'paymentProccess', params);

        ExeRu.request.get(ExeRu.payments.urlPrepare, form.serialize(), function(r) {
            if (r && r.errors) {
                ExeRu.payments.windowRef.close();
                ExeRu.inProgress.off();
                alert(r.errors);
            }

            var html = new EJS({url:'/assets/js/views/paymentsYandex.ejs?' + ExeRu.version}).render(r);

            ExeRu.payments.windowRef.document.write(html);
            ExeRu.payments.windowRef.document.close();

            if (ExeRu.payments.windowRef == null) {
                console.debug('Не удалось открыть окно.');
            }

            ExeRu.payments.windowRef.document.forms[0].submit();
            ExeRu.payments.setWaitPayment(); // Ставим ожидание оплаты

            if (ExeRu.payments.windowOrderId == 0) {
                ExeRu.payments.windowOrderId = r.elements.orderNumber;
            }
        });

        eventElement.blur();
        return false;
    },
    setWaitPayment: function() {
        ExeRu.payments.windowWait = true;

        ExeRu.payments.windowTimerId = setTimeout(function () {
            if (ExeRu.payments.windowRef.closed && ExeRu.payments.windowWait) {
                ExeRu.payments.buyCancel();
                ExeRu.payments.unsetWaitPayment();

                var paymentType = $('#payment-uniform-modal [name="paymentType"]').val();
                if (["QW", "SBERBANK", "ALFABANK", "PSB", "MC", "MTS", "BEELINE", "TELE2", "MEGAFON"].indexOf(paymentType) !== -1) {
                    alert("После оплаты выставленного счета, рубли будут зачислены Вам на баланс.");
                } else {
                    alert("Оплата была отменена");
                }
            } else {
                ExeRu.payments.windowTimerId = setTimeout(arguments.callee, 500);
            }
        }, 500);
    },
    unsetWaitPayment: function() {
        if (ExeRu.payments.windowWait == false) {
            return false;
        }
        clearTimeout(ExeRu.payments.windowTimerId);
        ExeRu.payments.windowWait = false;
        if (!ExeRu.payments.windowBuyProccess) {
            $('#payment-uniform-modal:visible [data-modal-close]').click();
        }
        ExeRu.inProgress.off();
    },
    prepareForm: function(eventElement) {
        var paymentType = eventElement.getAttribute('data-payment-type'),
            rate = eventElement.getAttribute('data-rate'),
            root = $('#payment-uniform-modal'),
            html, phone;

        if (paymentType == 'QW' || paymentType == 'MC' || paymentType == 'MTS'|| paymentType == 'BEELINE'|| paymentType == 'TELE2'|| paymentType == 'MEGAFON') {
            var phone = $(eventElement).parents('.modal').find('input[name="phone"]:first').val().trim();
            $('[name="phone"]', root).val(phone);
        } else {
            $('[name="phone"]', root).val('');
        }

        if (paymentType == 'PP') {
            $('#radio1', root).parent().hide();
        } else {
            $('#radio1', root).parent().show();
        }

        $('[name="paymentType"]', root).val(paymentType);
        $('[name="rate"]', root).val(rate);

        $('.pay-application').css({display:'none'});

        switch(paymentType) {
            case 'AC': { // Банковская карта
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата банковской картой</span></p>\
                        <ul class="card-type-list">\
                            <li><img src="/assets/img/mastercard-icon.png"></li>\
                            <li><img src="/assets/img/visa-icon.png"></li>\
                        </ul>\
                    </div>';
                break;
            }
            case 'MC':
            case 'MTS':
            case 'BEELINE':
            case 'TELE2':
            case 'MEGAFON': { // Мобильный телефон
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата с баланса мобильного телефона</span></p>\
                        </div>';

                if (phone.length >= 11) {
                    html += '<div class="payment-phone">\
                                <p>Оплата с номера <span>' + phone + '</span></p>\
                                <a href="#" data-modal="payment-phone-modal">Изменить</a>\
                            </div>';
                }

                html += '</div>';

                html = $(html);
                ExeRu.modal.init(html);
                break;
            }
            case 'QW': { // Системы онлайн оплаты, QIWI
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system1.png">\
                    </div>';
                $('.pay-application').css({display:'block'});
                break;
            }
            case 'PC': { // Системы оплаты, Яндекс.Деньги
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system2.png">\
                    </div>';
                break;
            }
            case 'WM': { // Системы оплаты, Webmoney
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system4.png">\
                    </div>';
                break;
            }
            case 'GP': { // Оплата через терминалы и салоны
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата через терминалы</span></p>\
                    </div>';
                break;
            }
            case 'SBERBANK': { // Оплата через интернет-банкинг, Сбербанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/sber.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'ALFABANK': { // Оплата через интернет-банкинг, Альфа-банк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/alfa.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'VTB24': { // Оплата через интернет-банкинг, ВТБ24
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/vtb.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'BRS': { // Оплата через интернет-банкинг, Русский Стандарт
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/brs.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'PSB': { // Оплата через интернет-банкинг, Промсвязьбанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/psb.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'PP': { // Системы оплаты, PayPal
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img webpayment-img-paypal" src="/assets/img/payment-system-paypal.png">\
                    </div>';
                break;
            }
        }

        $('.modal__content div:first', root).replaceWith($(html));

        // Расчитываем сумму для указанного количества рублей
        $('input[name="radiogroup"]', root).each(function(){
            this.checked = false;

            var parent = $(this).parent();
            var value = this.value;

            if (value == 'manual') {
                parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, 0, paymentType));
                parent.find('input[name="count"]').val('');
            } else {
                value = parseInt(value);
                if ( !isNaN(value) && value > 0) {
                    parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, value, paymentType));
                }
            }
        });

        // Снимаем и ставим заново обработчик на изменение количества в поле ручного ввода количества рублей
        $('input[name="count"]', root)
            .off('keyup change')
            .on('keyup change', function(){
                var parent = $(this).parent();
                var value = parseInt(this.value);
                parent.find('span').html(ExeRu.payments.getPriceString(rate, value, paymentType));
            });

        // Проверяем, есть ли у нас предустановленое значение другого количества
        var preSetCount = parseInt($(root).attr('data-preset-count'));

        if (isNaN(preSetCount) == false && preSetCount > 0) {
            $('#radio5')[0].checked = true;
            $('input[name="count"]', root).val(preSetCount);
            $('input[name="count"]', root).trigger('keyup');
        }
    },
    getPriceString: function(rate, count, paymentType) {
        if (count < 1) {
            return '&nbsp;';
        }

        var rubles = Math.ceil(eval(rate.replace('%count%', count))),
            commision = rubles - count;

        if (commision > 0) {
            if (typeof paymentType != 'undefined' && paymentType == 'PP') {
                return 'К оплате ' + rubles + plural_str(rubles, ' рубль', ' рубля', ' рублей') + ', в том числе комиссия PayPal ' + commision + plural_str(commision, ' рубль', ' рубля', ' рублей');
            } else {
                return 'К оплате ' + rubles + plural_str(rubles, ' рубль', ' рубля', ' рублей') + ', в том числе комиссия сотового оператора ' + commision + plural_str(commision, ' рубль', ' рубля', ' рублей');
            }
        }

        return '&nbsp;';
    }
};

/*var ExeRu = ExeRu || {};*/
ExeRu.request = {
    get: function(url, params, callback) {
        params.rnd = Math.random();
        $.get(url, params, function(r) {
            if (typeof(callback) == 'function') {
                callback(r);
            }
        });
    },
    post: function(url, params, callback) {
        $.post(url, params, function(r) {
            if (typeof(callback) == 'function') {
                callback(r);
            }
        });
    }
};
/*var ExeRu = ExeRu || {};*/
ExeRu.users = {
    urlGetUserInfo: '/ajax/getInfoAboutUser/',
    urlGetExtUserInfo: '/ajax/getExtendedInfoAboutUser/',

    urlGetBlackList: '/ajax/getBlackList/',
    urlToBlackList: '/ajax/toBlacklist/',
    urlFromBlackList: '/ajax/fromBlacklist/',

    urlSendEmailCheck: '/ajax/emailConfirm/',

    cache: {},
    extendedCache: {},

    init: function(container) {

    },
    setDestination: function(eventElement) {
        if ($(eventElement).hasClass('tab-register') && ($('#login-modal').length == 1)) {
            $('#login-modal [data-tab-target="tab_2"]').trigger('click');
        } else {
            $('#login-modal [data-tab-target="tab_1"]').trigger('click');
        }
        if (eventElement.hasAttribute('data-dest')) {
            var destinationId = eventElement.getAttribute('data-dest');
            $('form.sign-form [name="dest_id"]').val(destinationId);
            $('.sign-social-list a').each(function() {
                this.setAttribute('data-dest', destinationId);
            });
        } else {
            $('form.sign-form [name="dest_id"]').val('');
            $('.sign-social-list a').each(function() {
                this.removeAttribute('data-dest');
            });
        }
    },
    getUser: function(eventElement) {
        ExeRu.inProgress.on();

        if (typeof(eventElement) == 'object') {
            var uid = eventElement.getAttribute('data-uid');
        } else {
            var uid = eventElement;
        }
        var user = ExeRu.users.getUserData(uid, true);

        if (eventElement.hasAttribute('no-push-history')) {
            eventElement.removeAttribute('no-push-history');
        } else {
            history.pushState({}, '', '/id' + uid);
        }

        // Заголовок придется поменять отсуда, т.к. он не входит в шаблон
        /*$('#user-modal .modal__header h2').html(strip_by_length(user.name, 15) + ' ' + strip_by_length(user.last_name, 20));*/
        $('#user-modal .modal__header h2').html(user.name + ' ' + (user.last_name == null ? '' : user.last_name));

        // Внимание! Изменяя объект user, мы изменяем его свойства в кеше
        // Подготовим дату рождения и возраст
        if (typeof(user.birthday) === 'undefined') {
            user.birthday = '';
            var bday = user.bday;
            if (bday > 0) {
                if (bday < 1000) {
                    bday = '0' + bday;
                }
                var d = bday[2] + bday[3];
                var m = bday[0] + bday[1];
                user.birthday = 'День рождения: ' + parseInt(d) + ' ';
                user.birthday += ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'][parseInt(m) - 1];
            } else {
                var d = 1;
                var m = 7;
            }
        }

        if (typeof(user.age) === 'undefined') {
            user.age = '';
            if (user.byear > 1917) {
                var y = user.byear;
                var t = new Date();
                var a = ( t.getFullYear() - y - ((t.getMonth() - --m||t.getDate() - d)<0) );
                user.age = a + plural_str(a, ' год', ' года', ' лет');
            }
        }

        if (user.friendType == 'blocked') {
            var html = new EJS({url:'/assets/js/views/userProfile_blocked.ejs?' + ExeRu.version}).render(user);
            $('#user-modal [data-tab-target="tab_2"]').parent().hide();
        } else {
            var html = new EJS({url:'/assets/js/views/userProfile.ejs?' + ExeRu.version}).render(user);
            $('#user-modal [data-tab-target="tab_2"]').parent().show();
        }

        $('#user-modal .tabControl').html(html);

        $("#user-modal .row__more--open").click(function(){
            var parent = $(this).parent();
            $(".row__more-panel", parent).css("display", "block");

            var height = $($(".row__more-panel", parent).children()[0]).outerHeight();

            $(".row__more-panel", parent).animate({
                height: height,
                display: "block"
            });

            $(".row__more", parent).toggle();
        });

        $("#user-modal .row__more--close").click(function(){
            var parent = $(this).parent();

            $(".row__more-panel", parent).animate({
                height: "0px"
            }, "slow", function(){
                $(".row__more-panel", parent).css("display", "none");
            });

            $(".row__more", parent).toggle();
        });

        var popoverOptions = {
            template: '\
                    <div class="tooltip tooltip-profile popover-profile">\
                        <div class="tooltip-arrow"></div>\
                        <h3 class="popover-title"></h3>\
                        <div class="popover-content"></div>\
                    </div>',
            html: true
        };

        // Снимаем переход с ссылок-кнопок
        $("#user-modal .tabControl .friend-type a").on('click blur', function(e) {
            if (e.type == 'click') {
                $(this).popover("show");
            } else {
                $(this).popover("hide");
            }
            return false;
        });

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileCancelRequest(this);">Удалить из друзей</a>';
        $("#user-modal .tabControl .friend-type-both.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileAcceptRequest(this);">Подтвердить</a>';
        $("#user-modal .tabControl .friend-type-me.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileCancelRequest(this);">Отменить заявку</a>';
        $("#user-modal .tabControl .friend-type-i.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileRequestToFriends(this);">Добавить в друзья</a>';
        $("#user-modal .tabControl .friend-type-rejected.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileDeleteFromBlackList(this);">Удалить из черного списка</a>';
        $("#user-modal .tabControl .friend-type-blacklist.friends-button-left a").popover(popoverOptions);

        popoverOptions = {
            template: '\
                    <div class="tooltip tooltip-profile popover-profile popover-big">\
                        <div class="tooltip-arrow"></div>\
                        <h3 class="popover-title"></h3>\
                        <div class="popover-content"></div>\
                    </div>',
            html: true
        };

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileDeleteFromBlackList(this);">Удалить из черного списка</a>';
        $("#user-modal .tabControl .friend-type-blacklist.friends-button-right a").popover(popoverOptions);


        // TODO Обработчики
        $('#user-modal .tabControl [data-module][data-method]:not([data-modal])').on('click', function(e) {
            $('html, body').animate({scrollTop: 0}, 1000);
            var module = this.getAttribute('data-module');
            var method = this.getAttribute('data-method');
            if (module !== null && method !== null) {
                ExeRu[module][method](this);
            }
            return false;
        });

        ExeRu.modal.init('#user-modal .tabControl');
        $('#user-modal .modal__tabs a:first').click();
        ExeRu.inProgress.off();
    },
    debug: function(element){
        ExeRu.inProgress.on();
        var uid = $(element).parents('.friend-type')[0].getAttribute('data-uid');
        ExeRu.inProgress.off();
        return false;
    },
    getUserData: function(uid, extended) {
        if (extended === true) {
            if (typeof ExeRu.users.extendedCache[window.uid] === 'undefined' && window.uid != uid) {
                ExeRu.users.getUserData(window.uid, true);
            }
            // Расширенный кеш пока будет дублировать часть информации из основного
            if (typeof ExeRu.users.extendedCache[uid] === 'undefined') {
                $.ajax({
                    method: "GET",
                    url: ExeRu.users.urlGetExtUserInfo,
                    data: {uid: uid},
                    async: false
                }).success(function(r) {
                    ExeRu.users.extendedCache[uid] = r;
                    count = 500;
                    offset = 0;
                    more = true;
                    uids = [];
                    do {
                        $.ajax({
                            method: "GET",
                            url: '/api/',
                            data: {
                                api_id: 1,
                                format: 'json',
                                method: 'getFriends',
                                user_id: uid,
                                fields: 'name,last_name,photo_50,photo_100,photo_200_orig',
                                order: 'hints',
                                sid: sid,
                                count: count,
                                offset: offset,
                                rnd: Math.random()
                            },
                            async: false
                        }).success(function(r) {
                            if (r.response && r.response.length > 0) {
                                for(var i=0; i<r.response.length; i++) {
                                    var friend = r.response[i];
                                    friend['photo'] = {};
                                    friend['photo']['70'] = r.response[i].photo_50;
                                    friend['photo']['120'] = r.response[i].photo_100;
                                    friend['photo']['270'] = r.response[i].photo_200_orig;
                                    ExeRu.users.extendedCache[uid].friends.push(friend);
                                    uids.push(friend.uid);
                                }
                                if (r.response.length == count) {
                                    offset += count;
                                } else {
                                    more = false;
                                }
                            } else {
                                more = false;
                            }
                        }).error(function(r){
                            more = false;
                        });
                    } while(more);

                    ExeRu.users.extendedCache[uid].friends.reverse();

                    $.ajax({
                        method: "POST",
                        url: '/ajax/getFriendsTypeAndState/',
                        data: {uids: uids},
                        async: false
                    }).success(function(r) {
                        if (r.friends && r.friends.length > 0) {
                            for(var i=0; i<r.friends.length; i++) {
                                for(var j=0; j<ExeRu.users.extendedCache[uid].friends.length; j++) {
                                    if (r.friends[i].uid == ExeRu.users.extendedCache[uid].friends[j].uid) {
                                        ExeRu.users.extendedCache[uid].friends[j].online = r.friends[i].online;
                                        ExeRu.users.extendedCache[uid].friends[j].friendType = '';
                                    }
                                }
                            }
                        }
                        if (window.uid != uid && ExeRu.users.extendedCache[uid].friends.length > 0 && ExeRu.users.extendedCache[window.uid].friends.length > 0) {
                            for(var i=0; i<ExeRu.users.extendedCache[window.uid].friends.length; i++) {
                                for(var j=0; j<ExeRu.users.extendedCache[uid].friends.length; j++) {
                                    if (ExeRu.users.extendedCache[window.uid].friends[i].uid == ExeRu.users.extendedCache[uid].friends[j].uid) {
                                        ExeRu.users.extendedCache[uid].friends[j].friendType = 'both'; 
                                    }
                                }
                            }
                        }
                    });


/*                    $.get('/ajax/getFriendsTypeAndState/', {uids: uids}, function(r) {
                        if (r.friends && r.friends.length > 0) {
                            for(var i=0; i<r.friends.length; i++) {
                                for(var j=0; j<ExeRu.users.extendedCache[uid].friends.length; j++) {
                                    if (r.friends[i].uid == ExeRu.users.extendedCache[uid].friends[j].uid) {
                                        ExeRu.users.extendedCache[uid].friends[j].online = r.friends[i].online;
                                        ExeRu.users.extendedCache[uid].friends[j].friendType = r.friends[i].friendType;
                                    }
                                }
                            }
                        }
                    });*/
                });
            }
            return ExeRu.users.extendedCache[uid];
        } else {
            if (typeof ExeRu.users.cache[uid] === 'undefined') {
                $.ajax({
                    method: "GET",
                    url: ExeRu.users.urlGetUserInfo,
                    data: {uid: uid},
                    async: false
                }).success(function(r) {
                    ExeRu.users.cache[uid] = r;
                });
            }
            return ExeRu.users.cache[uid];
        }
    },
    deleteFromCache: function(uid) {
        delete ExeRu.users.cache[uid];
        delete ExeRu.users.extendedCache[uid];
    },

    renderBlacklist: function() {
        ExeRu.inProgress.on();
        ExeRu.request.get(ExeRu.users.urlGetBlackList, {}, function(r) {
            r.source = 'blacklist';
            $('#blacklist--search_user .modal__search--blacklist').val('');
            var html = new EJS({url:'/assets/js/views/blackList.ejs?' + ExeRu.version}).render(r);
            $('#blacklist-wrap').html(html);
            ExeRu.modal.init($('#blacklist-wrap'));
            ExeRu.init.buttons($('#blacklist-wrap'));
            ExeRu.inProgress.off();
        });
    },
    toBlackList: function(eventElement) {
        ExeRu.inProgress.on();
        var uid = eventElement.getAttribute('data-uid');
        var source = $(eventElement).parents('[data-source]')[0].getAttribute('data-source');
        ExeRu.request.get(ExeRu.users.urlToBlackList, {uid: uid}, function(r) {
            if (r.errors !== undefined) {
                ExeRu.inProgress.off();
                alert(r.errors.summary);
            } else if (r.location !== undefined) {
                ExeRu.users.deleteFromCache(uid);
                ExeRu.inProgress.off();
                if (source == 'search') {
                    $('#settings-modal .modal__search--submit').click()
                } else {
                    $('#settings-modal .active').click();
                }
            }
        });
    },
    fromBlackList: function(eventElement) {
        ExeRu.inProgress.on();
        var uid = eventElement.getAttribute('data-uid');
        var source = $(eventElement).parents('[data-source]')[0].getAttribute('data-source');
        ExeRu.request.get(ExeRu.users.urlFromBlackList, {uid: uid}, function(r) {
            if (r.errors !== undefined) {
                ExeRu.inProgress.off();
                alert(r.errors.summary);
            } else if (r.location !== undefined) {
                ExeRu.users.deleteFromCache(uid);
                ExeRu.inProgress.off();
                if (source == 'search') {
                    $('#settings-modal .modal__search--submit').click()
                } else if (source == 'friends') {
                    $('#friends-modal .modal__search--submit-wrap').click();
                } else {
                    $('#settings-modal .active').click();
                }
            }
        });
    },
    isOnline: function(uid) {
        if (typeof ExeRu.users.extendedCache[uid] !== 'undefined') {
            return ExeRu.users.extendedCache[uid].online;
        } else if (typeof ExeRu.users.cache[uid] !== 'undefined') {
            return ExeRu.users.cache[uid].online;
        } else {
            var user = ExeRu.users.getUserData(uid);
            return ExeRu.users.cache[uid].online;
        }
    },
    setOnline: function(uid) {
        if (typeof ExeRu.users.extendedCache[uid] !== 'undefined') {
            ExeRu.users.extendedCache[uid].online = true;
        }
        if (typeof ExeRu.users.cache[uid] !== 'undefined') {
            ExeRu.users.cache[uid].online = true;
        }
    },
    setOffline: function(uid) {
        if (typeof ExeRu.users.extendedCache[uid] !== 'undefined') {
            ExeRu.users.extendedCache[uid].online = false;
        }
        if (typeof ExeRu.users.cache[uid] !== 'undefined') {
            ExeRu.users.cache[uid].online = false;
        }
    },
    setTabAccessSettings: function(eventElement) {
        $('#settings-modal .modal-tabs__list a[data-tab-target="tab_6"]')[0].click();
        return false;
    },
    setTabPhotoSettings: function(eventElement) {
        $('#settings-modal .modal-tabs__list a[data-tab-target="tab_7"]')[0].click();
        return false;
    },
    sendEmailCheck: function(eventElement) {
        $.post(ExeRu.users.urlSendEmailCheck, {}, function(r) {
            alert(r.errors.summary);
        });
    }
};
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/validator_modal_help.js
 * Валидатор формы отправки сообщений (помощь)
 */
$(document).ready(function(){
    $('#help form').validate({
        rules:{
            'subject': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'name': {
                required: true
            },
            'question': {
                required: true
            }
        },
        messages:{
            'subject': {
                required: "Выберите тему вопроса"
            },
            'email': {
                required: "Введите адрес электронной почты",
                email: "Введите правильный адрес электронной почты"
            },
            'name': {
                required: "Введите ваше имя"
            },
            'question': {
                required: "Задайте ваш вопрос"
            }
        }/*,
        submitHandler: function(form) {
            ajaxSend(form, function(r) {
                if (typeof r.result !== 'undefined' && r.result) {
                    alert('Ваш вопрос отправлен.');
                    $('#help [data-modal-close]').click();
                }
            });
        }*/
    });
});
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/select_dropdown.js
 * Выпадающий список
 */
$(document).ready(function(){
    $('.sel_in').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            option_list = select.find('.sel_list');

        if (option_list.is(':visible')) {
            option_list.slideUp(200);
            select.removeClass('is-opened');
            self.find('.sel_arrow').removeClass('is-active');
        } else {
            if ($('.sel .sel_list:visible').length) {
                $('.sel .sel_list:visible').hide();
                $('.sel .sel_arrow').removeClass('is-active');
            }

            option_list.slideDown(200);
            select.addClass('is-opened');
            self.find('.arrow').addClass('is-active');
        }
    });

    $('.sel_list li').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            title = select.find('.sel_in .sel_title'),
            option = self.html(),
            depends = $(select.data('depends'));

        title.html(option);
        self.closest('.sel').find('input[type=hidden]').val(self.attr('data-value'));
        self.closest('.sel_list').find('li').removeClass('is-active');
        self.addClass('is-active');
        self.closest('.sel_list').slideUp(200);
        self.closest('.sel').removeClass('is-opened');
        self.closest('.sel').find('.sel_arrow').removeClass('is-active');

        depends.val(self.attr('data-value'));
        depends.trigger('keyup');
    });

    $(document).on('click', function (e) {
        if ($('.sel .sel_list:visible').length && !$(e.target).closest('.sel').length) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
            $('.sel .sel_arrow').removeClass('is-active');
        }
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
        }
    });

    $('.sel_list li.is-active').click();
});
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/function_ajaxSend.js
 * Отправка формы ajax-запросом. Чаще всего используется в валидаторах форм.
 */
var ajaxSend = function(form, callback) {
    if (typeof ExeRu !== 'undefined' && typeof ExeRu.inProgress !=='undefined') {
        var inProgress = ExeRu.inProgress;
    } else {
        var inProgress = {
            on: function(){ $('#inProgressShadow').show(); },
            off: function(){ $('#inProgressShadow').hide(); }
        }
    }

    inProgress.on();
    $.post($(form).attr('action'), $(form).serialize(), function(r) {
        if (typeof r.errors !== 'undefined') {
            for(var i in r.errors) {
                alert(r.errors[i]);
            }
            inProgress.off();
        }  else if (typeof callback == 'function') {
            inProgress.off();
            callback(r);
        } else if (typeof r.location !== 'undefined' && r.location != location.pathname) {
            location.href = r.location;
        } else if (typeof r.location !== 'undefined' && r.location == location.pathname) {
            inProgress.off();
        }
    });
};
/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_isImage.js
 * Проверяет, что тип файл является типом изображений.
 */
if (typeof isImage == 'undefined') {
    var isImage = function(file) {
        return file.type == 'image/gif' || file.type == 'image/jpg' || file.type == 'image/jpeg' || file.type == 'image/png';
    };
}
/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_fileSizeMore.jss
 * Проверяет, является ли файл файлом большего размера, чем требуется.
 */
if (typeof fileSizeMore == 'undefined') {
    var fileSizeMore = function (file, size) {
        return file.size - size > 0;
    }
}
/**
 * Created by ruslan on 02.06.16.
 * source: /assets/js/src/elements/function_showError.js
 * Обвертка для отображения ошибок.
 */
if (typeof showError == 'undefined') {
    var showError = function(text) {
        alert(text);
        if (typeof ExeRu != 'undefined' && typeof ExeRu.inProgress != 'undefined' && ExeRu.inProgress.count == 1) {
            ExeRu.inProgress.off();
        }
        return false;
    };
}