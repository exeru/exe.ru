(function(){


    var rsplit = function(string, regex) {
            var result = regex.exec(string),retArr = new Array(), first_idx, last_idx, first_bit;
            while (result != null)
            {
                first_idx = result.index; last_idx = regex.lastIndex;
                if ((first_idx) != 0)
                {
                    first_bit = string.substring(0,first_idx);
                    retArr.push(string.substring(0,first_idx));
                    string = string.slice(first_idx);
                }
                retArr.push(result[0]);
                string = string.slice(result[0].length);
                result = regex.exec(string);
            }
            if (! string == '')
            {
                retArr.push(string);
            }
            return retArr;
        },
        chop =  function(string){
            return string.substr(0, string.length - 1);
        },
        extend = function(d, s){
            for(var n in s){
                if(s.hasOwnProperty(n))  d[n] = s[n]
            }
        }


    EJS = function( options ){
        options = typeof options == "string" ? {view: options} : options
        this.set_options(options);
        if(options.precompiled){
            this.template = {};
            this.template.process = options.precompiled;
            EJS.update(this.name, this);
            return;
        }
        if(options.element)
        {
            if(typeof options.element == 'string'){
                var name = options.element
                options.element = document.getElementById(  options.element )
                if(options.element == null) throw name+'does not exist!'
            }
            if(options.element.value){
                this.text = options.element.value
            }else{
                this.text = options.element.innerHTML
            }
            this.name = options.element.id
            this.type = '['
        }else if(options.url){
            options.url = EJS.endExt(options.url, this.extMatch);
            this.name = this.name ? this.name : options.url;
            var url = options.url
            //options.view = options.absolute_url || options.view || options.;
            var template = EJS.get(this.name /*url*/, this.cache);
            if (template) return template;
            if (template == EJS.INVALID_PATH) return null;
            try{
                this.text = EJS.request( url+(this.cache ? '' : '?'+Math.random() ));
            }catch(e){}

            if(this.text == null){
                throw( {type: 'EJS', message: 'There is no template at '+url}  );
            }
            //this.name = url;
        }
        var template = new EJS.Compiler(this.text, this.type);

        template.compile(options, this.name);


        EJS.update(this.name, this);
        this.template = template;
    };
    /* @Prototype*/
    EJS.prototype = {
        /**
         * Renders an object with extra view helpers attached to the view.
         * @param {Object} object data to be rendered
         * @param {Object} extra_helpers an object with additonal view helpers
         * @return {String} returns the result of the string
         */
        render : function(object, extra_helpers){
            object = object || {};
            this._extra_helpers = extra_helpers;
            var v = new EJS.Helpers(object, extra_helpers || {});
            return this.template.process.call(object, object,v);
        },
        update : function(element, options){
            if(typeof element == 'string'){
                element = document.getElementById(element)
            }
            if(options == null){
                _template = this;
                return function(object){
                    EJS.prototype.update.call(_template, element, object)
                }
            }
            if(typeof options == 'string'){
                params = {}
                params.url = options
                _template = this;
                params.onComplete = function(request){
                    var object = eval( request.responseText )
                    EJS.prototype.update.call(_template, element, object)
                }
                EJS.ajax_request(params)
            }else
            {
                element.innerHTML = this.render(options)
            }
        },
        out : function(){
            return this.template.out;
        },
        /**
         * Sets options on this view to be rendered with.
         * @param {Object} options
         */
        set_options : function(options){
            this.type = options.type || EJS.type;
            this.cache = options.cache != null ? options.cache : EJS.cache;
            this.text = options.text || null;
            this.name =  options.name || null;
            this.ext = options.ext || EJS.ext;
            this.extMatch = new RegExp(this.ext.replace(/\./, '\.'));
        }
    };
    EJS.endExt = function(path, match){
        if(!path) return null;
        match.lastIndex = 0
        return path+ (match.test(path) ? '' : this.ext )
    }




    /* @Static*/
    EJS.Scanner = function(source, left, right) {

        extend(this,
            {left_delimiter: 	left +'%',
                right_delimiter: 	'%'+right,
                double_left: 		left+'%%',
                double_right:  	'%%'+right,
                left_equal: 		left+'%=',
                left_comment: 	left+'%#'})

        this.SplitRegexp = left=='[' ? /(\[%%)|(%%\])|(\[%=)|(\[%#)|(\[%)|(%\]\n)|(%\])|(\n)/ : new RegExp('('+this.double_left+')|(%%'+this.double_right+')|('+this.left_equal+')|('+this.left_comment+')|('+this.left_delimiter+')|('+this.right_delimiter+'\n)|('+this.right_delimiter+')|(\n)') ;

        this.source = source;
        this.stag = null;
        this.lines = 0;
    };

    EJS.Scanner.to_text = function(input){
        if(input == null || input === undefined)
            return '';
        if(input instanceof Date)
            return input.toDateString();
        if(input.toString)
            return input.toString();
        return '';
    };

    EJS.Scanner.prototype = {
        scan: function(block) {
            scanline = this.scanline;
            regex = this.SplitRegexp;
            if (! this.source == '')
            {
                var source_split = rsplit(this.source, /\n/);
                for(var i=0; i<source_split.length; i++) {
                    var item = source_split[i];
                    this.scanline(item, regex, block);
                }
            }
        },
        scanline: function(line, regex, block) {
            this.lines++;
            var line_split = rsplit(line, regex);
            for(var i=0; i<line_split.length; i++) {
                var token = line_split[i];
                if (token != null) {
                    try{
                        block(token, this);
                    }catch(e){
                        throw {type: 'EJS.Scanner', line: this.lines};
                    }
                }
            }
        }
    };


    EJS.Buffer = function(pre_cmd, post_cmd) {
        this.line = new Array();
        this.script = "";
        this.pre_cmd = pre_cmd;
        this.post_cmd = post_cmd;
        for (var i=0; i<this.pre_cmd.length; i++)
        {
            this.push(pre_cmd[i]);
        }
    };
    EJS.Buffer.prototype = {

        push: function(cmd) {
            this.line.push(cmd);
        },

        cr: function() {
            this.script = this.script + this.line.join('; ');
            this.line = new Array();
            this.script = this.script + "\n";
        },

        close: function() {
            if (this.line.length > 0)
            {
                for (var i=0; i<this.post_cmd.length; i++){
                    this.push(pre_cmd[i]);
                }
                this.script = this.script + this.line.join('; ');
                line = null;
            }
        }

    };


    EJS.Compiler = function(source, left) {
        this.pre_cmd = ['var ___ViewO = [];'];
        this.post_cmd = new Array();
        this.source = ' ';
        if (source != null)
        {
            if (typeof source == 'string')
            {
                source = source.replace(/\r\n/g, "\n");
                source = source.replace(/\r/g,   "\n");
                this.source = source;
            }else if (source.innerHTML){
                this.source = source.innerHTML;
            }
            if (typeof this.source != 'string'){
                this.source = "";
            }
        }
        left = left || '<';
        var right = '>';
        switch(left) {
            case '[':
                right = ']';
                break;
            case '<':
                break;
            default:
                throw left+' is not a supported deliminator';
                break;
        }
        this.scanner = new EJS.Scanner(this.source, left, right);
        this.out = '';
    };
    EJS.Compiler.prototype = {
        compile: function(options, name) {
            options = options || {};
            this.out = '';
            var put_cmd = "___ViewO.push(";
            var insert_cmd = put_cmd;
            var buff = new EJS.Buffer(this.pre_cmd, this.post_cmd);
            var content = '';
            var clean = function(content)
            {
                content = content.replace(/\\/g, '\\\\');
                content = content.replace(/\n/g, '\\n');
                content = content.replace(/"/g,  '\\"');
                return content;
            };
            this.scanner.scan(function(token, scanner) {
                if (scanner.stag == null)
                {
                    switch(token) {
                        case '\n':
                            content = content + "\n";
                            buff.push(put_cmd + '"' + clean(content) + '");');
                            buff.cr();
                            content = '';
                            break;
                        case scanner.left_delimiter:
                        case scanner.left_equal:
                        case scanner.left_comment:
                            scanner.stag = token;
                            if (content.length > 0)
                            {
                                buff.push(put_cmd + '"' + clean(content) + '")');
                            }
                            content = '';
                            break;
                        case scanner.double_left:
                            content = content + scanner.left_delimiter;
                            break;
                        default:
                            content = content + token;
                            break;
                    }
                }
                else {
                    switch(token) {
                        case scanner.right_delimiter:
                            switch(scanner.stag) {
                                case scanner.left_delimiter:
                                    if (content[content.length - 1] == '\n')
                                    {
                                        content = chop(content);
                                        buff.push(content);
                                        buff.cr();
                                    }
                                    else {
                                        buff.push(content);
                                    }
                                    break;
                                case scanner.left_equal:
                                    buff.push(insert_cmd + "(EJS.Scanner.to_text(" + content + ")))");
                                    break;
                            }
                            scanner.stag = null;
                            content = '';
                            break;
                        case scanner.double_right:
                            content = content + scanner.right_delimiter;
                            break;
                        default:
                            content = content + token;
                            break;
                    }
                }
            });
            if (content.length > 0)
            {
                // Chould be content.dump in Ruby
                buff.push(put_cmd + '"' + clean(content) + '")');
            }
            buff.close();
            this.out = buff.script + ";";
            var to_be_evaled = '/*'+name+'*/this.process = function(_CONTEXT,_VIEW) { try { with(_VIEW) { with (_CONTEXT) {'+this.out+" return ___ViewO.join('');}}}catch(e){e.lineNumber=null;throw e;}};";

            try{
                eval(to_be_evaled);
            }catch(e){
                if(typeof JSLINT != 'undefined'){
                    JSLINT(this.out);
                    for(var i = 0; i < JSLINT.errors.length; i++){
                        var error = JSLINT.errors[i];
                        if(error.reason != "Unnecessary semicolon."){
                            error.line++;
                            var e = new Error();
                            e.lineNumber = error.line;
                            e.message = error.reason;
                            if(options.view)
                                e.fileName = options.view;
                            throw e;
                        }
                    }
                }else{
                    throw e;
                }
            }
        }
    };


//type, cache, folder
    /**
     * Sets default options for all views
     * @param {Object} options Set view with the following options
     * <table class="options">
     <tbody><tr><th>Option</th><th>Default</th><th>Description</th></tr>
     <tr>
     <td>type</td>
     <td>'<'</td>
     <td>type of magic tags.  Options are '&lt;' or '['
     </td>
     </tr>
     <tr>
     <td>cache</td>
     <td>true in production mode, false in other modes</td>
     <td>true to cache template.
     </td>
     </tr>
     </tbody></table>
     *
     */
    EJS.config = function(options){
        EJS.cache = options.cache != null ? options.cache : EJS.cache;
        EJS.type = options.type != null ? options.type : EJS.type;
        EJS.ext = options.ext != null ? options.ext : EJS.ext;

        var templates_directory = EJS.templates_directory || {}; //nice and private container
        EJS.templates_directory = templates_directory;
        EJS.get = function(path, cache){
            if(cache == false) return null;
            if(templates_directory[path]) return templates_directory[path];
            return null;
        };

        EJS.update = function(path, template) {
            if(path == null) return;
            templates_directory[path] = template ;
        };

        EJS.INVALID_PATH =  -1;
    };
    EJS.config( {cache: true, type: '<', ext: '.ejs' } );



    /**
     * @constructor
     * By adding functions to EJS.Helpers.prototype, those functions will be available in the
     * views.
     * @init Creates a view helper.  This function is called internally.  You should never call it.
     * @param {Object} data The data passed to the view.  Helpers have access to it through this._data
     */
    EJS.Helpers = function(data, extras){
        this._data = data;
        this._extras = extras;
        extend(this, extras );
    };
    /* @prototype*/
    EJS.Helpers.prototype = {
        /**
         * Renders a new view.  If data is passed in, uses that to render the view.
         * @param {Object} options standard options passed to a new view.
         * @param {optional:Object} data
         * @return {String}
         */
        view: function(options, data, helpers){
            if(!helpers) helpers = this._extras
            if(!data) data = this._data;
            return new EJS(options).render(data, helpers);
        },
        /**
         * For a given value, tries to create a human representation.
         * @param {Object} input the value being converted.
         * @param {Object} null_text what text should be present if input == null or undefined, defaults to ''
         * @return {String}
         */
        to_text: function(input, null_text) {
            if(input == null || input === undefined) return null_text || '';
            if(input instanceof Date) return input.toDateString();
            if(input.toString) return input.toString().replace(/\n/g, '<br />').replace(/''/g, "'");
            return '';
        }
    };
    EJS.newRequest = function(){
        var factories = [function() { return new ActiveXObject("Msxml2.XMLHTTP"); },function() { return new XMLHttpRequest(); },function() { return new ActiveXObject("Microsoft.XMLHTTP"); }];
        for(var i = 0; i < factories.length; i++) {
            try {
                var request = factories[i]();
                if (request != null)  return request;
            }
            catch(e) { continue;}
        }
    }

    EJS.request = function(path){
        var request = new EJS.newRequest()
        request.open("GET", path, false);

        try{request.send(null);}
        catch(e){return null;}

        if ( request.status == 404 || request.status == 2 ||(request.status == 0 && request.responseText == '') ) return null;

        return request.responseText
    }
    EJS.ajax_request = function(params){
        params.method = ( params.method ? params.method : 'GET')

        var request = new EJS.newRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                if(request.status == 200){
                    params.onComplete(request)
                }else
                {
                    params.onComplete(request)
                }
            }
        }
        request.open(params.method, params.url)
        request.send(null)
    }


})();
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/validator_modal_help.js
 * Валидатор формы отправки сообщений (помощь)
 */
$(document).ready(function(){
    $('#help form').validate({
        rules:{
            'subject': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'name': {
                required: true
            },
            'question': {
                required: true
            }
        },
        messages:{
            'subject': {
                required: "Выберите тему вопроса"
            },
            'email': {
                required: "Введите адрес электронной почты",
                email: "Введите правильный адрес электронной почты"
            },
            'name': {
                required: "Введите ваше имя"
            },
            'question': {
                required: "Задайте ваш вопрос"
            }
        }/*,
        submitHandler: function(form) {
            ajaxSend(form, function(r) {
                if (typeof r.result !== 'undefined' && r.result) {
                    alert('Ваш вопрос отправлен.');
                    $('#help [data-modal-close]').click();
                }
            });
        }*/
    });
});
/**
 * Created by ruslan on 27.05.16.
 * source: /assets/js/src/elements/validator_dev-help__form.js
 * Dependencies: EJS, jQuery.Validate, AjaxSend
 *
 * Валидатор формы отправки сообщений в документации и в моих играх
 */
$(document).ready(function(){
    $('#dev-help__form').validate({
        rules:{
            'subject': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'name': {
                required: true
            },
            'question': {
                required: true
            }
        },
        messages:{
            'subject': {
                required: "Выберите тему вопроса"
            },
            'email': {
                required: "Введите адрес электронной почты",
                email: "Введите правильный адрес электронной почты"
            },
            'name': {
                required: "Введите ваше имя"
            },
            'question': {
                required: "Задайте ваш вопрос"
            }
        }
    });
});
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/select_dropdown.js
 * Выпадающий список
 */
$(document).ready(function(){
    $('.sel_in').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            option_list = select.find('.sel_list');

        if (option_list.is(':visible')) {
            option_list.slideUp(200);
            select.removeClass('is-opened');
            self.find('.sel_arrow').removeClass('is-active');
        } else {
            if ($('.sel .sel_list:visible').length) {
                $('.sel .sel_list:visible').hide();
                $('.sel .sel_arrow').removeClass('is-active');
            }

            option_list.slideDown(200);
            select.addClass('is-opened');
            self.find('.arrow').addClass('is-active');
        }
    });

    $('.sel_list li').on('click', function () {
        var self = $(this),
            select = self.closest('.sel'),
            title = select.find('.sel_in .sel_title'),
            option = self.html(),
            depends = $(select.data('depends'));

        title.html(option);
        self.closest('.sel').find('input[type=hidden]').val(self.attr('data-value'));
        self.closest('.sel_list').find('li').removeClass('is-active');
        self.addClass('is-active');
        self.closest('.sel_list').slideUp(200);
        self.closest('.sel').removeClass('is-opened');
        self.closest('.sel').find('.sel_arrow').removeClass('is-active');

        depends.val(self.attr('data-value'));
        depends.trigger('keyup');
    });

    $(document).on('click', function (e) {
        if ($('.sel .sel_list:visible').length && !$(e.target).closest('.sel').length) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
            $('.sel .sel_arrow').removeClass('is-active');
        }
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.sel').removeClass('is-opened');
            $('.sel .sel_list').slideUp(200);
        }
    });

    $('.sel_list li.is-active').click();
});
/**
 * Created by ruslan on 06.04.16.
 * source: /assets/js/src/elements/function_ajaxSend.js
 * Отправка формы ajax-запросом. Чаще всего используется в валидаторах форм.
 */
var ajaxSend = function(form, callback) {
    if (typeof ExeRu !== 'undefined' && typeof ExeRu.inProgress !=='undefined') {
        var inProgress = ExeRu.inProgress;
    } else {
        var inProgress = {
            on: function(){ $('#inProgressShadow').show(); },
            off: function(){ $('#inProgressShadow').hide(); }
        }
    }

    inProgress.on();
    $.post($(form).attr('action'), $(form).serialize(), function(r) {
        if (typeof r.errors !== 'undefined') {
            for(var i in r.errors) {
                alert(r.errors[i]);
            }
            inProgress.off();
        }  else if (typeof callback == 'function') {
            inProgress.off();
            callback(r);
        } else if (typeof r.location !== 'undefined' && r.location != location.pathname) {
            location.href = r.location;
        } else if (typeof r.location !== 'undefined' && r.location == location.pathname) {
            inProgress.off();
        }
    });
};
/**
 * Created by ruslan on 05.04.16.
 * source: /assets/js/dev/guest.js
 * Разработчики, общий для не-авторизованного пользователя
 */
$(document).ready(function() {
    $('[data-modal]').on('click', function() {
        var id = this.getAttribute('data-modal'),
            modal = $('#' + id);

        if (id == 'help' && pageUrl != '/blocked') {
            history.pushState({}, '', '/help');
            modal.find('form')[0].reset();
            modal.find('label.error').hide();
            $('#help .sel_title').html('Выберите тему обращения');
        }

        modal.removeClass('modal--close');
        modal.addClass('modal--open');

        hideScroll();

        modal.find('.overlay, [data-modal-close]').off('click').on('click', function(){
            var parent = $(this).parents('.modal-wrap'),
                path = /^\/(news|dialogs|friends|settings|help)(?:\/*)$/.exec(window.location.pathname);

            if (parent[0].id == 'help' && path != null) {
                history.pushState({}, '', '/');
            }

            parent
                .addClass('modal--close')
                .removeClass('modal--open');

            autoScroll();

            return false;
        });
        return false;
    });

    (function(){
        var handlerPopState = function(e) {
            // Восстанавливаем состояние окна с закрытыми модальными окнами
            autoScroll();

            var path = /^\/(help)(?:\/*)$/.exec(window.location.pathname);
            if (path != null) {
                var modalId = path[1] == 'help' ? path[1] : path[1] + '-modal';
                $('[data-modal="'+ modalId +'"]')[0].click();
            }
        };

        $(window).on('popstate', handlerPopState);

        handlerPopState(null);
    })();
});

function hideScroll() {
    var body = $('body'),
        overflowY = - body[0].clientWidth;

    body.css({'overflow-y':'hidden'});
    overflowY += body[0].clientWidth;

    if (overflowY > 0) {
        body.css({'margin-right': overflowY + 'px'});
    }
}

function autoScroll() {
    $('body').css({'overflow-y':'auto', 'margin-right':0});
}

/**
 * Created by ruslan on 12.08.15.
 */
jQuery(function() {
    var onWindowResizeLoad = function () {
        var _f = $('.dev-footer');

        if (_f.length) {
            var _m = $('.minHeight');
            _m.css('min-height', 'auto');

            var contentHeight = _f.offset().top + _f.outerHeight(),
                windowHeight  = $(window).height();

            if (windowHeight > contentHeight)
                _m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
        }
    };

    $(window).on('resize load', onWindowResizeLoad);

    // Открываем всплывайку
    $('.social__icon').on('click', function(){
        var params = "width=700,height=450,menubar=no,location=no,resizable=no,scrollbars=no,status=no",
            url = this.getAttribute('href') + '/popup';

        if (this.hasAttribute('data-dest')) {
            url += '?destId=' + this.getAttribute('data-dest');
        }

        window.open(url, "", params);

        return false;
    });

    // Показать форму авторизации/регистрации/восстановления пароля
    $("a.btn--site-header, a.btn-lg--sign-in, a.btn-lg--register, a.tab-login, a.tab-register, a.tab-restore").on("click", showLoginRegistrateRestoreForm);
    $(".connect-popup--btn.tab-login, .connect-popup--btn.tab-register").on("click", function(e) {
        $('#connect_1-modal').removeClass('modal--open');
        showLoginRegistrateRestoreForm(e);
        return false;
    });
    $("li.catalog-list__item, button.catalog-btn").not('.soon').on('click', function(e) {
        window.location.href = e.currentTarget.getAttribute('data-href');
        //var parent = $(e.currentTarget);
        //console.debug(e.currentTarget.getAttribute('data-href'));

/*            modal = $('#connect_1-modal');

        modal.find('.connect-popup--poster')
            .css({'border-bottom-color': parent.find('h4').css('color')});

        modal.find('.connect-popup--poster img')
            .attr({'src': parent.find('img').attr('src')});

        modal.find('.connect-popup--name')
            .css({'color': parent.find('h4').css('color')})
            .html(parent.find('h4').html());

        modal.find('.connect-popup--category')
            .html(parent.find('span').html());

        if (e.currentTarget.hasAttribute('data-href')) {
            modal.find('a.connect-popup--btn, .connect-popup--socials a, a.connect-popup--big').each(function(){
                this.setAttribute('data-href', e.currentTarget.getAttribute('data-href'));
            });
        } else {
            this.setAttribute('data-href', '');
        }

        if (e.currentTarget.hasAttribute('data-dest')) {
            modal.find('.connect-popup--socials a, a.connect-popup--big').each(function(){
                this.setAttribute('data-dest', e.currentTarget.getAttribute('data-dest'));
            });
        } else {
            this.setAttribute('data-dest', '');
        }

        $('[data-modal="connect_1-modal"]').click();*/
    });

    $("div.modal__close").on("click", closeLoginRegistrateRestoreForm);
    // Запоминаем цель при авторизации через соцсети
    $(".sign-social-list a, .connect-popup--socials a, a.connect-popup--big").on('click', function() {
        var url = this.getAttribute('href') + '/popup';
        if (this.hasAttribute('data-href')) {
            url += '?target_url=' + encodeURI(this.getAttribute('data-href'));
        }

        if (this.hasAttribute('data-dest')) {
            if (url.indexOf('?') === -1) {
                url += '?';
            } else {
                url += '&';
            }
            url += 'destId=' + this.getAttribute('data-dest');
        }

        var params = "width=700,height=450,menubar=no,location=no,resizable=no,scrollbars=no,status=no";
        window.open(url, "", params);

        return false;
    });

    $(".connect-popup--poster").on('click', function(){
        $("a.connect-popup--btn.alt:visible").trigger('click');
    });

    $("a.connect-popup--btn.alt").on('click', function(){
        return false;
    });

    // Правила валидации для авторизации
    $(".tab-login form").validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            },
            password: {
                required: "Введите пароль"
            }
        }
    });
    // Дополнительные методы валидации
    $.validator.addMethod(
        "minLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length < param) {
                return false;
            } else {
                return true;
            }
        }, "Не меньше 2-х символов");
    $.validator.addMethod(
        "maxLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length > param) {
                return false;
            } else {
                return true;
            }
        }, "Не больше 16 символов");
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            return this.optional(element) || regexp.test(value);
        }, "Разрешены только буквы и пробел");
    // Правила валидации для регистрации
    $(".tab-register form").validate({
        rules: {
            name: {
                required: true,
                minLen: 2,
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            email: {
                email: true,
                required: true
            },
            password: {
                required: true,
                minlength: 6
            },
            password_repeat: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            offer_agree: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Введите Ваше имя",
                minlen: "Не меньше 2-х символов",
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            },
            password: {
                required: "Введите пароль",
                minlength: "Пароль слишком короткий (меньше 6 символов)"
            },
            password_repeat: {
                required: "Введите пароль повторно",
                minlength: "Пароль слишком короткий (меньше 6 символов)",
                equalTo: "Пароли не совпадают"
            },
            offer_agree: {
                required: "Необходимо принять правила использования"
            }
        }
    });
    // Правила валидации для восстановления пароля
    $(".tab-restore form").validate({
        rules: {
            email: {
                email: true,
                required: true
            }
        },
        messages: {
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            }
        }
    });

    // Обработчики для авторизации и восстановления пароля
    $("#login .tab-login form, #login .tab-restore form").on("submit", sendDataForm);
    // Обработчик регистрации
    $("#login .tab-register form").on('submit', function() { return false; });

    // Pixel
    if (window.location.search.indexOf('from=lf') !== -1) {
        var LFListener = function(event) {
            if (window.removeEventListener) {
                window.removeEventListener("message", LFListener);
            } else {
                window.detachEvent("onmessage", LFListener);
            }

            if (event.data && event.data.sess && event.data.uid) {
                $.post('/ajax/jlf', {data:event.data});
            }

            if (event.data && event.data.sess && event.data.uid) {
                $.post('/ajax/jlf', {data:event.data});
            }
        };

        if (window.addEventListener) {
            window.addEventListener("message", LFListener);
        } else { // IE8
            window.attachEvent("onmessage", LFListener);
        }

        var fragment = $('<div style="visibility: hidden;"><iframe src="//lostfilm.tv/widgets/pixel.php" /></div>');
        $('body').append(fragment);
    }

    // Каруселька
    if ($('.nmain-slider--in').length > 0) {
        $('.nmain-slider--in').slick({
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            focusOnSelect: true,
            autoplay: true,
            autoplaySpeed: 5000,

            prevArrow: '<span class="slick-arrow slick-prev"></span>',
            nextArrow: '<span class="slick-arrow slick-next"></span>'
        });
    }
});

function showLoginRegistrateRestoreForm(e) {
    var tabName = getTabName(e.target);

    if (tabName === "") { // Не удалось найти вкладку
        return false;
    }

    $("div.sign-form-wrap, div.sign-social-wrap").hide();

    $("#login")
        .removeClass('modal--close')
        .addClass('modal--open');

    hideScroll();

    $("ul.modal-tabs__list a").removeClass('active');
    $("ul.modal-tabs__list a." + tabName).addClass('active');

    var activeDiv = $("div." + tabName),
        form = $('form', activeDiv);

    activeDiv.show();

    if (form.length > 0) {
        form[0].reset();
        $('label.error', activeDiv).hide();
    }

    // Запоминаем, куда пользователь хочет попасть
    if (e.target.hasAttribute('data-href')) {
        $('.sign-form-wrap [name="target_url"]').val(e.target.getAttribute('data-href'));
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.setAttribute('data-href', e.target.getAttribute('data-href'));
        });
    } else {
        $('.sign-form-wrap [name="target_url"]').val('');
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.removeAttribute('data-href');
        });
    }

    // Запоминаем, куда пользователь кликал
    if (e.target.hasAttribute('data-dest')) {
        $('.sign-form-wrap [name="dest_id"]').val(e.target.getAttribute('data-dest'));
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.setAttribute('data-dest', e.target.getAttribute('data-dest'));
        });
    } else {
        $('.sign-form-wrap [name="dest_id"]').val('');
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.removeAttribute('data-dest');
        });
    }

    return false;
}

function getTabName(el) {
    var classList = el.classList;

    for (i=classList.length - 1; i>=0; i--) {
        if ($.inArray(classList[i], ["btn--site-header", "btn-lg--sign-in", "catalog-btn", "tab-login"]) !== -1) {
            // Найден класс перехода на логин
            return "tab-login";
        } else if ($.inArray(classList[i], ["btn-lg--register", "tab-register"]) !== -1) {
            // Найден класс перехода на регистрацию
            return "tab-register";
        } else if ($.inArray(classList[i], ["tab-restore"]) !== -1) {
            // Найден класс восстановления пароля
            return "tab-restore";
        }
    }

    return "";
}

function closeLoginRegistrateRestoreForm() {
    $("#login")
        .addClass('modal--close')
        .removeClass('modal--open');

    autoScroll();

    return false;
}

function sendDataForm(e) {
    if ($('input.error', this).length > 0) {
        return false;
    }

    $('button:submit').blur();

    tabName = getTabName($('button', this)[0]);
    if (tabName === "") {
        return false;
    }

    switch(tabName) {
        case "tab-login": {
            action = "/ajax/login";
            break;
        }
        case "tab-register": {
            action = "/ajax/register";
            break;
        }
        case "tab-restore": {
            action = "/ajax/restore";
            break;
        }
    }

    $.post(action, $(this).serialize(), function(r){
        if (r.errors !== undefined) {
            $("input[name='" + r.csrf.name + "']").val(r.csrf.value);
            alert(r.errors.summary);
            if (typeof r.errors.type !== undefined) {
                if (r.errors.type == 'sent') {
                    closeLoginRegistrateRestoreForm();
                } else if (r.errors.type == 'reg') {
                    grecaptcha.reset();
                    $('#recaptcha_reg').hide();

                    var form = $('#login .tab-register form');
                    $('button:submit', form).show();
                    $('input', form).removeAttr('readonly');
                    form[0].reset();
                }
            }
        } else if (r.location !== undefined) {
            location.href = r.location;
        }
    });

    return false;
}
var grecaptchaCallback = function() {
    // Регистрация пользователя
    if ($("#login .tab-register form").length > 0) {
        $("#login .tab-register form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_reg = grecaptcha.render('recaptcha_reg', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var form = $("#login .tab-register form");

                    form.off('submit').on('submit', sendDataForm);

                    form.trigger('submit');
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_reg').show();

            return false;
        });
    }
    // Играть без регистрации
    if ($("a.connect-popup--btn.alt").length > 0) {
        $("a.connect-popup--btn.alt").on('click', function(){
            var widget = grecaptcha.render('recaptcha_user', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var element = $('a.connect-popup--btn.alt')[0],
                        params = {};
                    if (element.hasAttribute('data-href')) {
                        params.target_url = element.getAttribute('data-href');
                    }

                    params.response = r;

                    $.post('/play', params, function(r) {
                        if (typeof r.result !== undefined && r.result == 0) {
                            grecaptcha.reset(widget);
                        } else if (typeof r.location !== undefined) {
                            location.href = r.location;
                        }
                    });
                }
            });

            $(this).hide();
            $('#recaptcha_user').show();

            return false;
        });
    }
    // Отправка вопроса со страницы обратной связи у разработчиков
    if ($("#dev-help__form").length > 0) {
        $("#dev-help__form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_feedback = grecaptcha.render('recaptcha_feedback', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    ajaxSend($("#dev-help__form"), function(r) {
                        if (typeof r.result !== 'undefined' && r.result == 'renew') {
                            grecaptcha.reset(widget_feedback);
                        } else {
                            var version = (typeof ExeRu !== 'undefined' && typeof ExeRu.version !== 'undefined') ? '?' + ExeRu.version : '',
                                html = new EJS({url:'/assets/js/dev/views/help_ok.ejs' + version}).render(r);

                            $('#dev-help__form').replaceWith(html);
                        }
                    });
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_feedback').show();

            return false;
        });
    }
    // Форма помощи у гостя
    $('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        var widget_help = grecaptcha.render('recaptcha_help', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                ajaxSend($("#help form"), function(r) {
                    if (typeof r.result !== 'undefined' && r.result == 'renew') {
                        grecaptcha.reset(widget_help);
                    } else {
                        alert('Ваш вопрос отправлен.');
                        $('#recaptcha_help').replaceWith('<div id="recaptcha_help" style="padding:0;margin-bottom:10px;position:relative;clear:both;width:100%;margin-left:90px;display:none;"></div>');
                        $('#help form button:submit').show();
                        $("#help input").attr('readonly', false);
                        $('#help [data-modal-close]').click();
                    }
                });
            }
        });

        $('button:submit', this).hide();
        $('#recaptcha_help').show();

        return false;
    });
    // Слишком много регистраций
    if ($('#recaptcha').length == 1) {
        var widget_recaptcha = grecaptcha.render('recaptcha', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                $('#recaptcha').parent().find('[name="response"]').val(r);
                $.post('/play', $('#recaptcha').parent().serialize(), function(r) {
                    if (typeof r.result !== undefined && r.result == 0) {
                        grecaptcha.reset(widget_recaptcha);
                    } else if (typeof r.location !== undefined) {
                        location.href = r.location;
                    }
                });
            }
        });

        $('#recaptcha').show();
    }
};