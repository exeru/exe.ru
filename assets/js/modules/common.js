$(document).ready(function(){
    // При получении ответа 403 - редирект на корень
/*    $.ajaxSetup({
        statusCode: {
            403: function() {
                if (forbiddenRedirect) {
                    window.location.href = '/';
                }
            }
        }
    });*/

    var onWindowResizeLoad = function () {
        var _f = $('.footer'),
            _m = $('.minHeight');

        _m.css('min-height', 'auto');

        var contentHeight = _f.offset().top + _f.outerHeight(),
            windowHeight  = $(window).height();

        if (windowHeight > contentHeight)
            _m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
    };

    onWindowResizeLoad();

    $(window).on('resize', onWindowResizeLoad);

    ExeRu.events.params.sid = window.sid;

    ExeRu.init.modules(document);
    ExeRu.init.buttons(document);

    (function(){
        var handlerPopState = function(e) {
            // Восстанавливаем состояние окна с закрытыми модальными окнами
            $("body").css({'overflow-y':'auto', 'padding-right': '0'});
            $('iframe').css('visibility', 'visible');
            $('.modal--open').each(function(){
                $(ExeRu.modal.dialogs[this.id].el).trigger('closeDialog');
                ExeRu.modal.dialogs[this.id].isOpen = false;
                $('[data-modal="' + this.id + '"]').removeClass('active');
                $(this).removeClass('modal--open');
            });

            var path = /^\/(news|dialogs|friends|settings|help)(?:\/*)$/.exec(window.location.pathname);
            if (path != null) {
                var modalId = path[1] == 'help' ? path[1] : path[1] + '-modal';

                ExeRu.modal.dialogs[modalId].el.setAttribute('no-push-history', true);
                ExeRu.modal.dialogs[modalId].toggle();
                // Включаем первую табу
                $(ExeRu.modal.dialogs[modalId].el).find('.modal-tabs__list a:first').click();
                // А для фильтров - первый чекбох
                $(ExeRu.modal.dialogs[modalId].el).find('.checkbox-list input:first').click();
            } else {
                var id = /^\/id(\d+)(?:\/*)$/.exec(window.location.pathname);
                if (id != null) {
                    if (uid > 0) {
                        $('#forOpenByHash').attr({'data-uid':id[1], 'no-push-history':true})[0].click();
                    } else {
                        var oldValue = $('#login-modal [name="target_url"]:first').val();
                        $('#login-modal [name="target_url"]').val(id[0]);
                        $(".sign-social-list a"). attr('data-href', id[0]);
                        $('[data-modal="login-modal"]')[0].click();
                        $('#login-modal').on('closeDialog', function() {
                            $('#login-modal [name="target_url"]').val(oldValue);
                            $(".sign-social-list a"). attr('data-href', oldValue);
                        });
                    }
                }
            }
        };

        $(window).on('popstate', handlerPopState);

        handlerPopState(null);
    })();

    // Форма помощи у авторизованного пользователя
    $('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        ajaxSend($('#help form'), function(r) {
            if (typeof r.result !== 'undefined' && r.result) {
                alert('Ваш вопрос отправлен.');
                $('#help [data-modal-close]').click();
                $('#help .sel_title').html('Выберите тему обращения');
                $("#help form input").attr('readonly', false);
                $("#help form")[0].reset();
            }
        });

        return false;
    });

    $('#blacklist--search_user .modal__search--submit-wrap').on('click', function(e) {
        e.preventDefault();
        var text = $('#blacklist--search_user input:text').val();
        if (text.length < 3) {
            return false;
        }
        ExeRu.inProgress.on();
        ExeRu.request.get('/ajax/search_users/', $('#blacklist--search_user').serialize(), function(r) {
            if (r.result.length == 0) {
                var html = new EJS({url:'/assets/js/views/blackListNotFound.ejs?' + ExeRu.version}).render();
                $('#blacklist-wrap').html(html);
            } else {
                r.source = 'search';
                var html = new EJS({url:'/assets/js/views/blackList.ejs?' + ExeRu.version}).render(r);
                $('#blacklist-wrap').html(html);
                ExeRu.modal.init($('#blacklist-wrap'));
                ExeRu.init.buttons($('#blacklist-wrap'));
            }
            ExeRu.inProgress.off();
        });
    });

    $('.profile-change-photo .upload-photo-input').on('change', function() {

        if (!isImage(this.files[0])) {
            alert('Необходимо выбрать изображение');
            return false;
        }

        if (fileSizeMore(this.files[0], 10485760)) {
            alert('Слишкой большой файл, не более 10 мб.');
            return false;
        }

        ExeRu.inProgress.on();

        $('#uploadPhotoForm').submit();
        $('#uploadPhotoForm .upload-photo-input').val('');
    });

    // Дополнительные методы валидации
    $.validator.addMethod(
        "minLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length < param) {
                return false;
            } else {
                return true;
            }
        }, "Не меньше 2-х символов");
    $.validator.addMethod(
        "maxLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length > param) {
                return false;
            } else {
                return true;
            }
        }, "Не больше 16 символов");
    $.validator.addMethod(
        "customDate",
        function (value, element) {
            var form = $(element).closest('form'),
                bday = parseInt(form.find('input[name="bday"]').val()),
                bmonth = parseInt(form.find('input[name="bmonth"]').val()),
                byear = parseInt(form.find('input[name="byear"]').val());

            if (bday > 0) {
                if ((bmonth == 4 || bmonth == 6 || bmonth == 9 || bmonth == 11) && bmonth == 31) {
                    return false;
                }

                if (bmonth == 2) {
                    if (byear > 0) {
                        var isleap = (byear % 4 == 0 && (byear % 100 != 0 || byear % 400 == 0));
                        if (bday > 29 || (bday == 29 && !isleap)) {
                            return false;
                        }
                    } else if (bday > 29) {
                        return false;
                    }
                }

            }
            return true;
        }, "Неверная дата");
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            return this.optional(element) || regexp.test(value);
        }, "Разрешены только буквы и пробел");
    // Правила валидации для информации о пользователе
    $("#profile_main").validate({
        rules: {
            name: {
                required: true,
                minLen: 2,
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            last_name: {
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            bdate: {
                customDate: 0
            }
        },
        messages: {
            name: {
                required: "Введите Ваше имя",
                minLen: "Не меньше 2-х символов",
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            last_name: {
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            bdate: {
                customDate: "Неверная дата"
            }
        }
    });

    $("#profile_access").validate({
        rules: {
            new_email: {
                email: true
            }
        },
        messages: {
            new_email: {
                email: "Введите корректный email"
            }
        }
    });

    $('#profile_main input[name="bday"], #profile_main input[name="bmonth"], #profile_main input[name="byear"]').on('change', function() {
        $('#profile_main').valid();
    });

    $('div.profile-select p, div.profile-select .form-arrow').on('click', function() {
        var parent = $(this).parent();
        if (parent.hasClass('disabled')) {
            return false;
        }

        if (parent.find('[name="country_id"]').length == 1 && parent.find('.mCSB_container li').length == 1 && parent.find('.mCSB_container li')[0].getAttribute('data-rel') == 0) {
            ExeRu.inProgress.on();
            ExeRu.request.get('ajax/getCountries', {}, function(r) {
                var parent = $('#profile_main input[name="country_id"]').parent(),
                    container = parent.find('.mCSB_container');

                for(var i in r.result) {
                    container.append('<li data-rel="' + r.result[i].id + '">' + r.result[i].name + '</li>');
                }

                container.find('li').on('click', function() {
                    var parent = $(this).parents('.profile-select');
                    parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
                    parent.find('p').html(this.textContent);
                    parent.removeClass('open');
                });

                ExeRu.inProgress.off();
                $('div.profile-select.open').not(parent).removeClass('open');
                parent.toggleClass('open');
            });

            return false;
        }

        if (parent.find('[name="city_id"]').length == 1 && parent.find('.mCSB_container li').length == 1 && parent.find('.mCSB_container li')[0].getAttribute('data-rel') == 0) {
            ExeRu.inProgress.on();
            ExeRu.request.get('ajax/getCities', {country: $('#profile_main input[name="country_id"]').val()}, function(r) {
                var cityInput = $('#profile_main input[name="city_id"]'),
                    parent = cityInput.parent(),
                    container = parent.find('.mCSB_container');

                cityInput.val(0);
                parent.find('p').html('Выберите из списка...');
                container.find('li').remove();

                for(var i in r.result) {
                    container.append('<li data-rel="' + r.result[i].id + '">' + r.result[i].name + '</li>');
                }

                container.find('li').on('click', function() {
                    var parent = $(this).parents('.profile-select');
                    parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
                    parent.find('p').html(this.textContent);
                    parent.removeClass('open');
                });

                parent.removeClass('disabled');

                ExeRu.inProgress.off();

                $('div.profile-select.open').not(parent).removeClass('open');
                parent.toggleClass('open');
            });

            return false;
        }

        $('div.profile-select.open').not(parent).removeClass('open');
        parent.toggleClass('open');
    });

    $('div.profile-select li').on('click', function() {
        var parent = $(this).parents('.profile-select');
        parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
        parent.find('p').html(this.textContent);
        parent.removeClass('open');
    });

    if ($('#profile_main input[name="country_id"]').val() == 0) {
        var cityInput = $('#profile_main input[name="city_id"]'),
            parent = cityInput.parent();

        parent.find('p').html('Сначала выберите страну...');
        parent.addClass('disabled');

        cityInput.val('0');
    }

    $('#profile_main input[name="country_id"]').on('change', function() {
        if (this.value == "0") {
            var cityInput = $('#profile_main input[name="city_id"]'),
                parent = cityInput.parent();

            parent.find('p').html('Сначала выберите страну...');
            parent.addClass('disabled');

            cityInput.val('0');

            return true;
        }
        ExeRu.inProgress.on();
        ExeRu.request.get('ajax/getCities', {country: this.value}, function(r) {
            var cityInput = $('#profile_main input[name="city_id"]'),
                parent = cityInput.parent(),
                container = parent.find('.mCSB_container');

            cityInput.val(0);
            parent.find('p').html('Выберите из списка...');
            container.find('li').remove();

            for(var i in r.result) {
                container.append('<li data-rel="' + r.result[i].id + '">' + r.result[i].name + '</li>');
            }

            container.find('li').on('click', function() {
                var parent = $(this).parents('.profile-select');
                parent.find('input').val(this.getAttribute('data-rel')).trigger('change');
                parent.find('p').html(this.textContent);
                parent.removeClass('open');
            });

            parent.removeClass('disabled');

            ExeRu.inProgress.off();
        });
    });

    $('button.modal__footer__btn[data-target]').on('click', function() {
        var target = $(this).data('target');
        var form = $('#' + target);
        if (!form.valid()) {
            return false;
        }

        var url = '/ajax/save_' + target;
        // Вешаем тень с индикатором
        ExeRu.inProgress.on();
        $.post(url, $(form).serialize(), function(r) {
            if (r.errors !== undefined) {
                $("input[name='" + r.csrf.name + "']").val(r.csrf.value);
                alert(r.errors.summary);
                ExeRu.inProgress.off();
            } else if (r.location !== undefined) {
                location.href = r.location;
            } else if (r.callback !== undefined) {
                $("input[name='" + r.csrf.name + "']").val(r.csrf.value);
                var functionName = r.callback;
                if (typeof(window[functionName]) == 'function') {
                    window[functionName](r);
                }
                ExeRu.inProgress.off();
            }
        });
        return false;
    });

    if($(".report-block").length != 0){
        $(".report-block").addClass("animated bounceInRight");

        $(".report-close").click(function(e){
            e.preventDefault();

            $(".report-block").removeClass("bounceInRight");
            $(".report-block").addClass("animated bounceOutRight");
        });
    }

    $(".delete-data").each(function(e){
        $(this).tooltip({
            template: ['<div class="tooltip tooltip-message">',
                '<div class="tooltip-arrow"></div>',
                '<div class="tooltip-inner">',
                '</div>',
                '</div>'].join('')
        });
    });


    $(".report-close").each(function(e){
        $(this).tooltip({
            template: ['<div class="tooltip tooltip-message">',
                '<div class="tooltip-arrow"></div>',
                '<div class="tooltip-inner">',
                '</div>',
                '</div>'].join('')
        });
    });

    var profileBtnAddedContent = '<a href="#">Удалить из друзей</a>';

    $(".profile-btn--added").popover({
        template: ['<div class="tooltip tooltip-profile popover-profile">',
            '<div class="tooltip-arrow"></div>',
            '<h3 class="popover-title"></h3>' +
            '<div class="popover-content"></div>' +
            '</div>'].join(''),
        html: true,
        content: profileBtnAddedContent
    });

    $(".message-delete").each(function(e){
        $(this).tooltip({
            template: ['<div class="tooltip tooltip-message">',
                '<div class="tooltip-arrow"></div>',
                '<div class="tooltip-inner">',
                '<a href="#">Удалить</a>',
                '</div>',
                '</div>'].join('')
        });
    });

    $(".modal__filter-icon--modal").click(function(e){
        $(this).parent().toggleClass("open");
    });

    var tooltipTemplate = ['<div class="tooltip tooltip-payment">',
        '<div class="tooltip-arrow"></div>',
        '<div class="tooltip-inner">',
        '</div>',
        '</div>'].join('');

    var content = ['<h5>Элекснет</h5>',
        '<p>Стоимость рубля: 1 рубль</p>',
        '<a href="#">www.elecsnet.ru</a>'].join('');

    $(".payment-systems-list__item").each(function(){
        $(this).tooltip({
            container: $(this).parent(),
            html: true,
            title: content,
            placement: 'top',
            template: tooltipTemplate
        })
    });

    $(window).scroll(function(e) {
        var offset = 220;

        if($(".overlay").length != 0) {
            var headerOverlayTop = ((70 - $(this).scrollTop() > 0) ? (70 - $(this).scrollTop() + "px") : "0px");
            $(".overlay").css("top", headerOverlayTop);
        }

        if($(this).scrollTop() > offset) {
            $(".go-up").show();
        }
        else {
            $(".go-up").hide();
        }
    });

    $(".go-up").click(function(e){
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 1000);
    });

    $(".modal__search--text").placeholder();

/*    if($(".profile-select-dropdown").length != 0) {
        $(".profile-select-dropdown").mCustomScrollbar();
    }*/

    $(".row__more--open").click(function(){
        var parent = $(this).parent();
        $(".row__more-panel", parent).css("display", "block");

        var height = $($(".row__more-panel", parent).children()[0]).outerHeight();

        $(".row__more-panel", parent).animate({
            height: height,
            display: "block"
        });

        $(".row__more", parent).toggle();
    });

    $(".row__more--close").click(function(){
        var parent = $(this).parent();

        $(".row__more-panel", parent).animate({
            height: "0px"
        }, "slow", function(){
            $(".row__more-panel", parent).css("display", "none");
        });

        $(".row__more", parent).toggle();
    });

    $('.more-link-open').click(function(e){
        e.preventDefault();

        var parent = $(this).parent();

        if ($(this).hasClass('more-link-open-payments-history')) {
            if ($('.modal__content__left__list__item').length < 1) {
                return false;
            }

            if ($(".more-link-close:visible", parent).length > 0) {
                $(".more-link-close", parent).css("display", "none");

                $(".row__more-panel", parent).animate({
                    height: "0px"
                }, "slow", function(){
                    $(".row__more-panel", parent).css("display", "none");
                });

                return false;
            }
        }

        $(".row__more-panel", parent).css("display", "block");

        var height = $($(".row__more-panel", parent).children()[0]).outerHeight();

        $(".row__more-panel", parent).animate({
            height: height
        });

        $(".more-link-close", parent).css("display", "block");

    });

    $(".more-link-close").click(function(){
        var parent = $(this).parent();

        $(".more-link-close", parent).css("display", "none");

        $(".row__more-panel", parent).animate({
            height: "0px"
        }, "slow", function(){
            $(".row__more-panel", parent).css("display", "none");
        });
    });


    /* Страница игры */
    $('#checkbox-switch').on('change', function() {
        if (this.checked) {
            ExeRu.game.invitible.on();
        } else {
            ExeRu.game.invitible.off();
        }
    });
    $('#notification-checkbox').on('change', function() {
        if (this.checked) {
            ExeRu.game.notification.on();
        } else {
            ExeRu.game.notification.off();
        }
    });

    $('.nmain-slider--in').slick({
        infinite: true,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        focusOnSelect: true,
        autoplay: true,
        autoplaySpeed: 5000,

        prevArrow: '<span class="slick-arrow slick-prev"></span>',
        nextArrow: '<span class="slick-arrow slick-next"></span>'
    })

    // Pixel
    if (window.location.search.indexOf('from=lf') !== -1) {
        var LFListener = function(event) {
            if (window.removeEventListener) {
                window.removeEventListener("message", LFListener);
            } else {
                window.detachEvent("onmessage", LFListener);
            }

            if (event.data && event.data.sess && event.data.uid) {
                $.post('/ajax/jlf', {data:event.data});
            }
        };

        if (window.addEventListener) {
            window.addEventListener("message", LFListener);
        } else { // IE8
            window.attachEvent("onmessage", LFListener);
        }

        var fragment = $('<div style="visibility: hidden; display:none;"><iframe src="//lostfilm.tv/widgets/pixel.php" /></div>');
        $('body').append(fragment);
    }
});

function handlerChangePhoto(data) {
    $("#change_photo input[name='newPhotoPath']").val(data.image.path);

    $('#photo-modal .profile-photo').attr({
        'src' : imageHost + data.image.path,
        'data-width': data.image.width,
        'data-height': data.image.height
    });

    $('#photo-modal').on('DialogFX_endAnimationOpen', function() {
        $('#photo-modal')
            .off('DialogFX_endAnimationOpen')
            .on('closeDialog', function() { // Обработчик закрытия диалогового окна
                $(this).off('closeDialog');
                $('#photo-modal .profile-photo').imgAreaSelect({hide:true});
            });

        // Считаем размеры и центр для selection
        var photo = $('#photo-modal .profile-photo'),
            width = photo.attr('data-width'),
            height = photo.attr('data-height'),
            centerWidth = Math.round(width / 2),
            centerHeight = Math.round(height / 2),
            minSide = 270,
            offset = minSide / 2;


        if (width < 270 || height < 270) {
            minSide = width < height ? width : height;
        }

        offset = Math.round(minSide / 2);

        $("#change_photo input[name='x1']").val(centerWidth - offset);
        $("#change_photo input[name='y1']").val(centerHeight - offset);
        $("#change_photo input[name='x2']").val(centerWidth + offset);
        $("#change_photo input[name='y2']").val(centerHeight + offset);

        var ias = $('#photo-modal .profile-photo').imgAreaSelect({instance:true});

        $('#photo-modal .profile-photo-wrap').css('position', 'relative');

        ias.setOptions({
            parent: '#photo-modal  .profile-photo-wrap',
            show: true,
            handles: true,
            aspectRatio: "1:1",
            imageWidth: width,
            imageHeight: height,
            minWidth: minSide,
            minHeight: minSide,
            persistent: true,
            onSelectEnd: function(img, selection) {
                $("#change_photo input[name='x1']").val(selection.x1);
                $("#change_photo input[name='y1']").val(selection.y1);
                $("#change_photo input[name='x2']").val(selection.x2);
                $("#change_photo input[name='y2']").val(selection.y2);
            }
        });

        ias.setSelection(centerWidth - offset, centerHeight - offset, centerWidth + offset, centerHeight + offset);
        ias.update();
    });

    $('[data-modal="photo-modal"]').click();
    ExeRu.inProgress.off();
}

function returnToEditProfile(response) {
    $('#settings-modal .profile-photo--lg').attr('src', imageHost + response.data[270]);
    $('#settings-modal .profile-photo--sm').attr('src', imageHost + response.data[120]);
    $('#settings-modal .profile-photo--xs').attr('src', imageHost + response.data[70]);
    $('.user-info--site-header img').attr('src', imageHost + response.data[70]);

    $('#photo-modal a.modal__icon.modal__icon--prev')[0].click();
    return false;
}

function handlerDoYouWantAssignAccount() {
    $('[data-modal="assign-modal"]')[0].click();
}

function assignAccounts() {
    var input = $('<input type="hidden" name="doAssign" value="true" />');
    $('#profile_access').append(input);
    $('[data-target="profile_access"]')[0].click();
}

function plural_str(number, one, two, five) {
    number %= 100;
    if (number >= 5 && number <= 20) {
        return five;
    }
    number %= 10;
    if (number == 1) {
        return one;
    }
    if (number >= 2 && number <= 4) {
        return two;
    }
    return five;
}

function strip_by_length(string, length) {
    if (string === null) {
        return '';
    }
    return string.length > length ? string.substr(0, length - 4) + '...' : string;
}

function readableDate(stamp) {
    var date = new Date();
    date.setTime(stamp * 1000);

    out = {
        d : date.getDate()    < 10 ? '0' + date.getDate()          : date.getDate(),
        m : date.getMonth()   < 9  ? '0' + (date.getMonth() + 1)   : (date.getMonth() + 1),
        Y : date.getFullYear(),
        H : date.getHours()   < 10 ? '0' + date.getHours()   : date.getHours(),
        i : date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
    };

    return out.d + '.' + out.m + '.' + out.Y + ' ' + out.H + ':' + out.i;
}

function codeToImage(text) {
    reg = /\[\:\[(\d+)\]\:\]/g;
    return text.replace(reg, function(str, p1, offset, s) {
        return '<img src="/assets/img/blank.gif" data-smail="' + p1 + '" style="background-position: 0 -' + (p1 * 17) + 'px" class="emoji" />';
    });
}

var grecaptchaCallback = function() {
    // Регистрация пользователя
    /*if ($("#login .tab-register form").length > 0) {
        $("#login .tab-register form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_reg = grecaptcha.render('recaptcha_reg', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var form = $("#login .tab-register form");

                    form.off('submit').on('submit', sendDataForm);

                    form.trigger('submit');
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_reg').show();

            return false;
        });
    }*/
    // Играть без регистрации
    /*if ($("a.connect-popup--btn.alt").length > 0) {
        $("a.connect-popup--btn.alt").on('click', function(){
            var widget = grecaptcha.render('recaptcha_user', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var element = $('a.connect-popup--btn.alt')[0],
                        params = {};
                    if (element.hasAttribute('data-href')) {
                        params.target_url = element.getAttribute('data-href');
                    }

                    params.response = r;

                    $.post('/play', params, function(r) {
                        if (typeof r.result !== undefined && r.result == 0) {
                            grecaptcha.reset(widget);
                        } else if (typeof r.location !== undefined) {
                            location.href = r.location;
                        }
                    });
                }
            });

            $(this).hide();
            $('#recaptcha_user').show();

            return false;
        });
    }*/
    // Отправка вопроса со страницы обратной связи у разработчиков
    /*if ($("#dev-help__form").length > 0) {
        $("#dev-help__form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_feedback = grecaptcha.render('recaptcha_feedback', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    ajaxSend($("#dev-help__form"), function(r) {
                        if (typeof r.result !== 'undefined' && r.result == 'renew') {
                            grecaptcha.reset(widget_feedback);
                        } else {
                            var version = (typeof ExeRu !== 'undefined' && typeof ExeRu.version !== 'undefined') ? '?' + ExeRu.version : '',
                                html = new EJS({url:'/assets/js/dev/views/help_ok.ejs' + version}).render(r);

                            $('#dev-help__form').replaceWith(html);
                        }
                    });
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_feedback').show();

            return false;
        });
    }*/
    // Форма помощи у гостя
    /*$('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        var widget_help = grecaptcha.render('recaptcha_help', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                ajaxSend($("#help form"), function(r) {
                    if (typeof r.result !== 'undefined' && r.result == 'renew') {
                        grecaptcha.reset(widget_help);
                    } else {
                        alert('Ваш вопрос отправлен.');
                        $('#recaptcha_help').replaceWith('<div id="recaptcha_help" style="padding:0;margin-bottom:10px;position:relative;clear:both;width:100%;margin-left:90px;display:none;"></div>');
                        $('#help form button:submit').show();
                        $("#help input").attr('readonly', false);
                        $('#help [data-modal-close]').click();
                    }
                });
            }
        });

        $('button:submit', this).hide();
        $('#recaptcha_help').show();

        return false;
    });*/
    // Слишком много регистраций
    /*if ($('#recaptcha').length == 1) {
        var widget_recaptcha = grecaptcha.render('recaptcha', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                $('#recaptcha').parent().find('[name="response"]').val(r);
                $.post('/play', $('#recaptcha').parent().serialize(), function(r) {
                    if (typeof r.result !== undefined && r.result == 0) {
                        grecaptcha.reset(widget_recaptcha);
                    } else if (typeof r.location !== undefined) {
                        location.href = r.location;
                    }
                });
            }
        });

        $('#recaptcha').show();
    }*/
};

var ExeRu = ExeRu || {};
var ExeRu = {
    version: 283,
    call: function(e) {
        var module = this.getAttribute('data-module');
        var method = this.getAttribute('data-method');
        if (module !== null && method !== null) {
            ExeRu[module][method](this);
        }
        return false;
    }
};