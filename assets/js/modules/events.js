/*var ExeRu = ExeRu || {};*/
ExeRu.events = {
    eventsUri: "/events/",
    params: {
        api_id: 1,
        format: 'json',
        method: 'getEvent',
        rnd: Math.random(),
        wait: 25
    },
    timer: 0,
    updateTimer: function() {
        if (ExeRu.events.timer !== 0) {
            clearTimeout(ExeRu.events.timer);
        }
        setTimeout(function() {
            $(".report-block").removeClass("bounceInRight");
            $(".report-block").addClass("animated bounceOutRight");
        }, 5000);
    },
    init: function() {
        $.getJSON(ExeRu.events.eventsUri, ExeRu.events.params, function(r) {
            ExeRu.events.params.rnd = Math.random();
            ExeRu.events.params.ts = r.ts;
            setTimeout(ExeRu.events.init, 1000);
            if (r.events.length > 0) {
                ExeRu.events.parse(r.events);
            }
        });
        return false;
    },
    acceptUser: function() {
        var params = {
            sid: sid,
            gid: gid,
            rnd: Math.random()
        };
        $.ajax({
            method: "GET",
            url: '/acceptUser',
            dataType: 'json',
            data: params
        });
    },
    parse: function(events) {
        $(events).each(function(i, event) {
            switch(event.type) {
                case 'online': {
                    var html = $(new EJS({url:'/assets/js/views/online.ejs?' + ExeRu.version}).render(event));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('#online-events').html(html);
                    ExeRu.users.setOnline(event.uid);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'invite_to_friend_by_game': {
                    var html = $(new EJS({url:'/assets/js/views/inviteToFriendByGame.ejs?' + ExeRu.version}).render(event.message));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('.invite-to-game', html).on('click', ExeRu.call);

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'request_to_friends': {
                    var html = $(new EJS({url:'/assets/js/views/notify_new_request_to_friends.ejs?' + ExeRu.version}).render(event.message));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'invite_to_game': {
                    var html = $(new EJS({url:'/assets/js/views/inviteToGame.ejs?' + ExeRu.version}).render(event.message));

                    $("[data-method], .report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    ExeRu.modal.init(html);

                    $('.invite-to-game', html).on('click', ExeRu.call);

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();

                    break;
                }
                case 'new_message': {
                    ExeRu.dialogs.parseEventNewMessage(event);
                    ExeRu.events.updateTimer();
                    break;
                }
                case 'has_read_message': {
                    ExeRu.dialogs.parseEventHasRead(event);
                    break;
                }
                case 'user_is_typing': {
                    ExeRu.dialogs.parseEventTyping(event);
                    break;
                }
                case 'balance_change': {
                    var balanceTxt = plural_str(event.message.balance, ' рубль', ' рубля', ' рублей');
                    $('.balance span:first-child').html(event.message.balance);
                    $('.balance span:last-child').html(balanceTxt);

                    if (event.message.type == 'increment') {
                        var html = $(new EJS({url:'/assets/js/views/new_balance.ejs?' + ExeRu.version}).render({balance: event.message.balance + balanceTxt}));

                        $(".report-close", html).click(function(e){
                            e.preventDefault();
                            $(".report-block").removeClass("bounceInRight");
                            $(".report-block").addClass("animated bounceOutRight");
                        });

                        $('#online-events').html(html);

                        ExeRu.events.updateTimer();
                    }


                    if (!event.message.orderNumber || event.message.orderNumber != ExeRu.payments.windowOrderId) {
                        return;
                    }

                    ExeRu.payments.windowOrderId = 0;

                    ExeRu.payments.unsetWaitPayment();
                    break;
                }
                case 'balance_change_bonus': {
                    var balanceTxt = plural_str(event.message.balance, ' рубль', ' рубля', ' рублей');
                    $('.balance span:first-child').html(event.message.balance);
                    $('.balance span:last-child').html(balanceTxt);

                    var html = $(new EJS({url:'/assets/js/views/bonus.ejs?' + ExeRu.version}).render({balance: event.message.balance + balanceTxt}));

                    $(".report-close", html).click(function(e){
                        e.preventDefault();
                        $(".report-block").removeClass("bounceInRight");
                        $(".report-block").addClass("animated bounceOutRight");
                    });

                    $('#online-events').html(html);

                    ExeRu.events.updateTimer();
                    break;
                }
                case 'purchase_restore': {
                    if (event.message.sid != gid) {
                        return false;
                    }

                    if (event.message.order_id && event.message.order_id == ExeRu.payments.windowOrderId) {
                        ExeRu.payments.unsetWaitPayment();
                    }

                    if (ExeRu.payments.windowBuyProccess) {
                        ExeRu.payments.showBuy(event.message);
                    }

                    if (parseInt(event.message.balance) >= parseInt(event.message.price)) {
                        $('#buy-modal .form-submit-buy:button').trigger('click');
                        if (!ExeRu.payments.windowRef.closed) {
                            ExeRu.payments.windowRef.close();
                        }
                    }
                    break;
                }
                case 'panel': {
                    ExeRu.panel.parseEvent(event);
                    break;
                }
                case 'to_blacklist':
                case 'from_blacklist': {
                    ExeRu.users.deleteFromCache(event.message.uid);
                    break;
                }
                case 'blocked': {
                    window.location.href = '/blocked';
                    break;
                }
                default: {
                    console.debug(event);
                }
            }
        });
    }
};
setInterval(ExeRu.events.acceptUser, 60000);