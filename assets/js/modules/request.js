/*var ExeRu = ExeRu || {};*/
ExeRu.request = {
    get: function(url, params, callback) {
        params.rnd = Math.random();
        $.get(url, params, function(r) {
            if (typeof(callback) == 'function') {
                callback(r);
            }
        });
    },
    post: function(url, params, callback) {
        $.post(url, params, function(r) {
            if (typeof(callback) == 'function') {
                callback(r);
            }
        });
    }
};