/**
 * Created by ruslan on 23.12.15.
 */
/*var ExeRu = ExeRu || {};*/
ExeRu.payments = {
    urlPrepare: '/ajax/orderPrepare/',
    urlBuyPrepare: '/ajax/buyPrepare/',
    urlBuy: '/ajax/buy/',
    urlBuyRemember: '/ajax/buyRemember/',
    urlBuyCancel: '/ajax/buyCancel/',

    windowRef: null,
    windowWait: false,
    windowTimerId: null,
    windowBuyProccess: false,
    windowOrderId: 0,

    rememberAndDoPayment: function(event) {
        var data = event.data.data;
        data['sid'] = gid;

        // Проставляем другое количество в форме напрямую
        $('#payment-uniform-modal').attr('data-preset-count', (data.price - data.balance));
        $('#payment-uniform-modal [name="purchaseId"]').val(data.order_id);
        ExeRu.payments.windowOrderId = data.order_id;

        ExeRu.request.get(ExeRu.payments.urlBuyRemember, {data: data});

        $('#buy-modal').off('closeDialog');
        $('#buy-modal .form-submit-buy:button').off('click');

        $('[data-modal="payment-modal"]:first')[0].click();

        $('#payment-modal, #payment-uniform-modal, #payment-phone-modal, #payment-system-modal, #payment-terminal-modal').on('closeDialog', function() {
            var opened = $('.modal--open');
            if (opened.length == 0) {
                ExeRu.payments.buyCancel();
            }
        });

    },
    preSetCount: function(eventElement) {
        var value = eventElement.getAttribute('data-preset-count');
        $('#payment-uniform-modal').attr('data-preset-count', value);
    },
    showBuy: function(r) {
        console.debug(r);
        if (typeof(r.error) === 'object' && r.error.code != 200) {
            console.debug('Error: ');
            console.debug(r.error);
            window.frames[0].postMessage({type:'showOrderBox', data:{"result":"fail"}}, '*');
        } else {
            if (!ExeRu.payments.windowBuyProccess) {
                ExeRu.payments.windowBuyProccess = true;
            }
            $('#buy-modal').off('closeDialog');
            $('#buy-modal .form-submit-buy:button').off('click');

            $('#buy-modal .modal__header h2').html('Покупка ' + r.title);
            $('#buy-modal .buy-image img').attr('src', r.photo_url);

            $('#buy-modal .buy-balance span').html(r.balance + plural_str(r.balance, ' рубль', ' рубля', ' рублей'));
            $('#buy-modal .buy-what span').html(r.title);
            $('#buy-modal .buy-cost span').html(r.price + plural_str(r.price, ' рубль', ' рубля', ' рублей'));

            $('#buy-modal').on('closeDialog', {}, ExeRu.payments.buyCancel);

            $('#buy-modal .form-submit').attr('data-purchase-id', r.order_id);

            if (typeof(r.error) === 'object' && r.error.code == 200
                || parseInt(r.balance) < parseInt(r.price) ) {
                $('#buy-modal .buy-no-money').show();

                if (typeof(r.testing_payments) !== 'undefined' && r.testing_payments == 'allowed' && $('#test_payment').length == 1) {
                    $('#test_payment').on('change', function() {
                        if (this.checked) {
                            $('#buy-modal .form-submit-buy:button').html('Купить');
                            $('#buy-modal .form-submit-buy:button').off('click').on('click', {data: r}, ExeRu.payments.buyButton);
                        } else {
                            $('#buy-modal .form-submit-buy:button').html('Пополнить баланс');
                            $('#buy-modal .form-submit-buy:button').off('click').on('click', {data: r}, ExeRu.payments.rememberAndDoPayment);
                        }
                    });
                    $('#test_payment').trigger('change');
                } else {
                    $('#buy-modal .form-submit-buy:button').html('Пополнить баланс');
                    $('#buy-modal .form-submit-buy:button').on('click', {data: r}, ExeRu.payments.rememberAndDoPayment);
                }

            } else {
                $('#buy-modal .buy-no-money').hide();
                $('#buy-modal .form-submit-buy:button').html('Купить');
                $('#buy-modal .form-submit-buy:button').on('click', {data: r}, ExeRu.payments.buyButton);
            }

            $('[data-modal="buy-modal"]').click();
        }
    },
    buy: function(data) {
/*        if (uid == 0) {
            $('[data-modal="login-modal"]:first')[0].click();
            window.frames[0].postMessage({type:'showOrderBox', data:{"response":"cancel"}}, '*');
            return false;
        }*/
        ExeRu.inProgress.on();
        data['sid'] = gid;

        ExeRu.request.get(ExeRu.payments.urlBuyPrepare, data, function(r) {
            ExeRu.payments.showBuy(r);
            ExeRu.inProgress.off();
        });
    },
    buySuccess: function(r) {
        $('#buy-success-modal .buy-image img').attr('src', r.photo_url);
        $('#buy-success-modal .buy-what span').html(r.title);
        if ($('#test_payment').length == 1 && $('#test_payment')[0].checked) {
            $('#buy-success-modal .buy-cost span').html('0 (тестовый платеж)');
        } else {
            $('#buy-success-modal .buy-cost span').html(r.price + plural_str(r.price, ' рубль', ' рубля', ' рублей'));
        }

        $('[data-modal="buy-success-modal"]').click();
    },
    buyButton: function(event) {
        var eventData = event.data.data, data = {
            sid: gid,
            item_id: eventData.item_id,
            order_id : eventData.order_id
        };
        if ($('#test_payment').length == 1 && $('#test_payment')[0].checked) {
            data['test_payment'] = 1;
        }

        ExeRu.inProgress.on();

        $('#buy-modal .form-submit-buy:button').off('click', ExeRu.payments.buyButton);

        ExeRu.request.get(ExeRu.payments.urlBuy, data, function(r) {
            $('#buy-modal').off('closeDialog', ExeRu.payments.buyCancel);

            window.frames[0].postMessage({type:'showOrderBox', data:{result: r.result}}, '*');

            ExeRu.inProgress.off();
            if (typeof(r) === 'object' && r.result == 'success') {
                ExeRu.payments.buySuccess(eventData);
                var yandexPixelId = false, yandexGoal = false; epAlias = false;
                switch(r.app_id) {
                    case 3:  { yandexPixelId = '8985851700733783520'; yandexGoal = 'payment_grimm'; epAlias = 'GrimmPay'; break; } // Ферма Гримм
                    case 4:  { yandexPixelId = '3118139205116343349'; yandexGoal = 'payment_Rodina'; epAlias = 'RodinaPay'; break; } // Родина
                    case 9:  { yandexPixelId = '548869107449420013'; yandexGoal = 'payment_warbanner';  epAlias = 'WarbannerPay'; break; } // Знамя войны
                    case 10: { yandexPixelId = '3030271326504040087'; yandexGoal = 'payment_sturm'; epAlias = 'SturmPay'; break; } // Штурм
                    case 11: { yandexPixelId = '4014119551221737540'; yandexGoal = 'payment_groza'; epAlias = 'PiratesPay'; break; } // Гроза Морей
                    case 12: { yandexPixelId = '780409584648124763'; yandexGoal = 'payment_nano';  epAlias = 'NanoPay'; break; } // Нано-Ферма
                    case 14: { yandexPixelId = '5272730995427958504'; yandexGoal = 'payment_zames'; epAlias = 'RoyalclashPay'; break; } // Королевский замес
                    case 15: { yandexPixelId = '4531528669980184328'; yandexGoal = 'payment_rumba'; epAlias = 'RumbaPay'; break; } // Румба
                    case 19: { yandexPixelId = '2572361535851200757'; yandexGoal = 'payment_bonvoyage'; epAlias = 'BonVoyagePay'; break; } // Бон Вояж
                    case 39: { yandexPixelId = '2895953050854365825'; yandexGoal = 'payment_hero'; epAlias = 'HeroPay'; break; } // Путь война
                    case 47: { yandexPixelId = '9205954418440378790'; yandexGoal = 'payment_muzwar'; epAlias = 'MusicWarsPay'; break; } // Музвар
                    case 62: { yandexPixelId = '1751294151258564301'; yandexGoal = 'payment_tanks'; epAlias = 'WarTankPay'; break; } // Битва Танков
                    case 70: { yandexPixelId = '740625241004169368'; yandexGoal = 'payment_nebesa';  epAlias = 'NebesaPay'; break; } // Небеса
                    case 76: { yandexPixelId = '5713467157391880223'; yandexGoal = 'payment_undead'; epAlias = 'WalkingUndeadPay'; break; } // Ходячая Нежить
                    case 89: { yandexPixelId = '395491761459506839'; yandexGoal = 'payment_imperialhero';  epAlias = 'ImperialPay'; break; } // Империал Хироу
                    case 97: { yandexPixelId = '564369819775036493'; yandexGoal = 'payment_obitel';  epAlias = 'ObitelPay'; break; } // Обитель зла
                    case 103:{ yandexPixelId = '2376258013296713328'; yandexGoal = 'payment_vegamix';  epAlias = 'VegaMixPay'; break; } // Вега Микс
                    case 106:{ yandexPixelId = '2813561034340998958'; yandexGoal = 'payment_imperia';  epAlias = 'ImperiaPay'; break; } // Империя Онлайн
                    case 121:{ yandexPixelId = '5315283837067113366'; yandexGoal = 'payment_storm';  epAlias = 'StormPay'; break; } // Шторм
                    case 142:{ yandexPixelId = '936329647689031917'; yandexGoal = 'payment_fazenda';  epAlias = 'FazendaPay'; break; } // Фазенда
                }
                if (yandexPixelId !== false) {
                    var yandexPixelImg = $('<div><img src="https://mc.yandex.ru/pixel/'+ yandexPixelId + '?rnd=' + Math.random() + '" style="position:absolute; left:-9999px;" alt="" /></div>');
                    yandexPixelImg.prependTo('body');
                }
                if (yandexGoal !== false && yandexGoal !== '') {
                    yaCounter41227294.reachGoal(yandexGoal);
                }
                if (epAlias !== false && epAlias !== '' && typeof epRiseEvent === 'function') {
                    epRiseEvent(epAlias);
                }
                yaCounter41227294.reachGoal('EXE_Payment');
            } else {
                alert('Не удалось завершить покупку');
                if (ExeRu.modal.dialogs['buy-modal'].isOpen) {
                    ExeRu.modal.dialogs['buy-modal'].toggle();
                }
            }
            ExeRu.payments.windowOrderId = 0;
            ExeRu.payments.windowBuyProccess = false;
        });
    },
    buyCancel: function(eventElement) {
        var data = {
            sid: gid,
            order_id: $('#buy-modal .form-submit-cancel').attr('data-purchase-id')
        };

        $('#test_payment').off('change');

        ExeRu.request.post(ExeRu.payments.urlBuyCancel, data);

        $('#buy-modal .form-submit').removeAttr('data-purchase-id');

        $('#payment-uniform-modal [name="purchaseId"]').val('');

        $('#buy-modal .form-submit-buy:button').off('click', ExeRu.payments.buyButton);
        $('#buy-modal').off('closeDialog', ExeRu.payments.buyCancel);
        $('#payment-modal, #payment-uniform-modal, #payment-phone-modal, #payment-system-modal, #payment-terminal-modal').off('closeDialog');
        window.frames[0].postMessage({type:'showOrderBox', data:{"result":"cancel"}}, '*');
        ExeRu.payments.windowBuyProccess = false;
        ExeRu.payments.windowOrderId = 0;
    },
    buyButtonCancel: function() {
        $('#buy-modal [data-modal-close]').click();
    },
    prepare: function(eventElement) {
        var form = $(eventElement).closest('form');

        if (form.find('[name="radiogroup"]:checked').length == 0) {
            eventElement.blur();
            return false;
        }

        var paymentType = form.find('[name="paymentType"]').val(),
            value = form.find('[name="radiogroup"]:checked').val();

        if (value == 'manual') {
            value = form.find('[name="count"]').val();
        }

        value = parseInt(value);

        if (isNaN(value) || value < 1) {
            eventElement.blur();
            return false;
        }

        if ((paymentType == 'MC' || paymentType == 'MTS' || paymentType == 'BEELINE' || paymentType == 'TELE2' || paymentType == 'MEGAFON') && value < 10) {
            alert('Минимальная сумма мобильного платежа 10 рублей.');
            eventElement.blur();
            return false;
        }

        if ((paymentType == 'PP') && value < 50) {
            alert('Минимальная сумма платежа 50 рублей.');
            eventElement.blur();
            return false;
        }

        if (value >= 100000) {
            alert('Максимально 99999 рублей одновременно.');
            eventElement.blur();
            return false;
        }

        ExeRu.inProgress.on();

        if (paymentType == 'QW' && eventElement.hasAttribute('data-pay-application')) {
            ExeRu.request.get(ExeRu.payments.urlPrepare, form.serialize(), function(r) {
                alert("После оплаты выставленного счета, рубли будут зачислены Вам на баланс.");
                $('#payment-uniform-modal:visible [data-modal-close]').click();
                ExeRu.inProgress.off();
            });
            return false;
        }


        // Готовим окно
        if (ExeRu.payments.windowRef != null) {
            ExeRu.payments.windowRef.close();
        }

        var params = '';
        ExeRu.payments.windowRef = window.open('', 'paymentProccess', params);

        ExeRu.request.get(ExeRu.payments.urlPrepare, form.serialize(), function(r) {
            if (r && r.errors) {
                ExeRu.payments.windowRef.close();
                ExeRu.inProgress.off();
                alert(r.errors);
            }

            var html = new EJS({url:'/assets/js/views/paymentsYandex.ejs?' + ExeRu.version}).render(r);

            ExeRu.payments.windowRef.document.write(html);
            ExeRu.payments.windowRef.document.close();

            if (ExeRu.payments.windowRef == null) {
                console.debug('Не удалось открыть окно.');
            }

            ExeRu.payments.windowRef.document.forms[0].submit();
            ExeRu.payments.setWaitPayment(); // Ставим ожидание оплаты

            if (ExeRu.payments.windowOrderId == 0) {
                ExeRu.payments.windowOrderId = r.elements.orderNumber;
            }
        });

        eventElement.blur();
        return false;
    },
    setWaitPayment: function() {
        ExeRu.payments.windowWait = true;

        ExeRu.payments.windowTimerId = setTimeout(function () {
            if (ExeRu.payments.windowRef.closed && ExeRu.payments.windowWait) {
                ExeRu.payments.buyCancel();
                ExeRu.payments.unsetWaitPayment();

                var paymentType = $('#payment-uniform-modal [name="paymentType"]').val();
                if (["QW", "SBERBANK", "ALFABANK", "PSB", "MC", "MTS", "BEELINE", "TELE2", "MEGAFON"].indexOf(paymentType) !== -1) {
                    alert("После оплаты выставленного счета, рубли будут зачислены Вам на баланс.");
                } else {
                    alert("Оплата была отменена");
                }
            } else {
                ExeRu.payments.windowTimerId = setTimeout(arguments.callee, 500);
            }
        }, 500);
    },
    unsetWaitPayment: function() {
        if (ExeRu.payments.windowWait == false) {
            return false;
        }
        clearTimeout(ExeRu.payments.windowTimerId);
        ExeRu.payments.windowWait = false;
        if (!ExeRu.payments.windowBuyProccess) {
            $('#payment-uniform-modal:visible [data-modal-close]').click();
        }
        ExeRu.inProgress.off();
    },
    prepareForm: function(eventElement) {
        var paymentType = eventElement.getAttribute('data-payment-type'),
            rate = eventElement.getAttribute('data-rate'),
            root = $('#payment-uniform-modal'),
            html, phone;

        if (paymentType == 'QW' || paymentType == 'MC' || paymentType == 'MTS'|| paymentType == 'BEELINE'|| paymentType == 'TELE2'|| paymentType == 'MEGAFON') {
            var phone = $(eventElement).parents('.modal').find('input[name="phone"]:first').val().trim();
            $('[name="phone"]', root).val(phone);
        } else {
            $('[name="phone"]', root).val('');
        }

        if (paymentType == 'PP') {
            $('#radio1', root).parent().hide();
        } else {
            $('#radio1', root).parent().show();
        }

        $('[name="paymentType"]', root).val(paymentType);
        $('[name="rate"]', root).val(rate);

        $('.pay-application').css({display:'none'});

        switch(paymentType) {
            case 'AC': { // Банковская карта
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата банковской картой</span></p>\
                        <ul class="card-type-list">\
                            <li><img src="/assets/img/mastercard-icon.png"></li>\
                            <li><img src="/assets/img/visa-icon.png"></li>\
                        </ul>\
                    </div>';
                break;
            }
            case 'MC':
            case 'MTS':
            case 'BEELINE':
            case 'TELE2':
            case 'MEGAFON': { // Мобильный телефон
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата с баланса мобильного телефона</span></p>\
                        </div>';

                if (phone.length >= 11) {
                    html += '<div class="payment-phone">\
                                <p>Оплата с номера <span>' + phone + '</span></p>\
                                <a href="#" data-modal="payment-phone-modal">Изменить</a>\
                            </div>';
                }

                html += '</div>';

                html = $(html);
                ExeRu.modal.init(html);
                break;
            }
            case 'QW': { // Системы онлайн оплаты, QIWI
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system1.png">\
                    </div>';
                $('.pay-application').css({display:'block'});
                break;
            }
            case 'PC': { // Системы оплаты, Яндекс.Деньги
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system2.png">\
                    </div>';
                break;
            }
            case 'WM': { // Системы оплаты, Webmoney
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system4.png">\
                    </div>';
                break;
            }
            case 'GP': { // Оплата через терминалы и салоны
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата через терминалы</span></p>\
                    </div>';
                break;
            }
            case 'SBERBANK': { // Оплата через интернет-банкинг, Сбербанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/sber.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'ALFABANK': { // Оплата через интернет-банкинг, Альфа-банк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/alfa.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'VTB24': { // Оплата через интернет-банкинг, ВТБ24
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/vtb.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'BRS': { // Оплата через интернет-банкинг, Русский Стандарт
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/brs.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'PSB': { // Оплата через интернет-банкинг, Промсвязьбанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/psb.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'PP': { // Системы оплаты, PayPal
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img class="webpayment-img webpayment-img-paypal" src="/assets/img/payment-system-paypal.png">\
                    </div>';
                break;
            }
        }

        $('.modal__content div:first', root).replaceWith($(html));

        // Расчитываем сумму для указанного количества рублей
        $('input[name="radiogroup"]', root).each(function(){
            this.checked = false;

            var parent = $(this).parent();
            var value = this.value;

            if (value == 'manual') {
                parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, 0, paymentType));
                parent.find('input[name="count"]').val('');
            } else {
                value = parseInt(value);
                if ( !isNaN(value) && value > 0) {
                    parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, value, paymentType));
                }
            }
        });

        // Снимаем и ставим заново обработчик на изменение количества в поле ручного ввода количества рублей
        $('input[name="count"]', root)
            .off('keyup change')
            .on('keyup change', function(){
                var parent = $(this).parent();
                var value = parseInt(this.value);
                parent.find('span').html(ExeRu.payments.getPriceString(rate, value, paymentType));
            });

        // Проверяем, есть ли у нас предустановленое значение другого количества
        var preSetCount = parseInt($(root).attr('data-preset-count'));

        if (isNaN(preSetCount) == false && preSetCount > 0) {
            $('#radio5')[0].checked = true;
            $('input[name="count"]', root).val(preSetCount);
            $('input[name="count"]', root).trigger('keyup');
        }
    },
    getPriceString: function(rate, count, paymentType) {
        if (count < 1) {
            return '&nbsp;';
        }

        var rubles = Math.ceil(eval(rate.replace('%count%', count))),
            commision = rubles - count;

        if (commision > 0) {
            if (typeof paymentType != 'undefined' && paymentType == 'PP') {
                return 'К оплате ' + rubles + plural_str(rubles, ' рубль', ' рубля', ' рублей') + ', в том числе комиссия PayPal ' + commision + plural_str(commision, ' рубль', ' рубля', ' рублей');
            } else {
                return 'К оплате ' + rubles + plural_str(rubles, ' рубль', ' рубля', ' рублей') + ', в том числе комиссия сотового оператора ' + commision + plural_str(commision, ' рубль', ' рубля', ' рублей');
            }
        }

        return '&nbsp;';
    }
};
