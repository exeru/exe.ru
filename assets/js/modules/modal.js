/*var ExeRu = ExeRu || {};*/
ExeRu.modal = {
    scrollWidth: null,
    dialogs: {},
    init: function(container) {
        if (ExeRu.modal.scrollWidth == null) {
            var body = $('body'),
                overflowY = body.css('overflow-y');

            ExeRu.modal.scrollWidth = -body[0].offsetWidth;
            body.css('overflow-y', 'hidden');
            ExeRu.modal.scrollWidth += body[0].offsetWidth;

            body.css('overflow-y', overflowY);
        }
        // Обработчики открытия/закрытия диалоговых окон
        var dlgtriggers = $('[data-modal]', container);
        if (dlgtriggers.length > 0) {
            for(i=0; i<dlgtriggers.length; i++) {
                id = dlgtriggers[i].getAttribute('data-modal');
                if (ExeRu.modal.dialogs[id] === undefined) {
                    ExeRu.modal.dialogs[id] = new DialogFx(document.getElementById(id),
                        {
                            onOpenDialog : function(dialogObject) {
                                $("body").css({'overflow-y':'hidden', 'padding-right': ExeRu.modal.scrollWidth + 'px'});
                                if (dialogObject.el.hasAttribute('no-push-history')) {
                                    dialogObject.el.removeAttribute('no-push-history');
                                } else {
                                    if (dialogObject.el.id == 'news-modal') {
                                        history.pushState({}, '', '/news');
                                    } else if (dialogObject.el.id == 'dialogs-modal') {
                                        history.pushState({}, '', '/dialogs');
                                    } else if (dialogObject.el.id == 'friends-modal') {
                                        history.pushState({}, '', '/friends');
                                    } else if (dialogObject.el.id == 'settings-modal') {
                                        history.pushState({}, '', '/settings');
                                    } else if (dialogObject.el.id == 'help') {
                                        history.pushState({}, '', '/help');
                                    }
                                }

                                $(dialogObject.el).trigger('openDialog');
                                $('iframe').css('visibility', 'hidden');

                                var eventElement = dialogObject.options.event;
                                if (typeof(eventElement) == 'object') {
                                    var module = eventElement.getAttribute('data-module');
                                    var method = eventElement.getAttribute('data-method');

                                    if (module !== null && method !== null) {
                                        ExeRu[module][method](eventElement, dialogObject.el);
                                    }
                                }

                                $('a.icon__games.active').removeClass('active');

                                return false;
                            },
                            onCloseDialog : function(dialogObject) {
                                if (pageUrl != '/') {
                                    $('a.icon__games').addClass('active');
                                }

                                $('iframe').css('visibility', 'visible');
                                history.pushState({}, '', pageUrl);
                                $(dialogObject.el).trigger('closeDialog');
                                $(dialogObject.el).one('DialogFX_endAnimationClose', function(){
                                    $("body").css({'overflow-y':'auto', 'padding-right': '0'});
                                });

                                return false;
                            }
                        }
                    );
                }
                dlgtriggers[i].addEventListener('click', function(e) {
                    e.preventDefault();

                    currentName = this.getAttribute('data-modal');
                    $('.modal--open').each(function(){
                        if (this.id != currentName) {
                            $(ExeRu.modal.dialogs[this.id].el).trigger('closeDialog');
                            ExeRu.modal.dialogs[this.id].isOpen = false;
                            $('[data-modal="' + this.id + '"]').removeClass('active');
                            $(this).removeClass('modal--open');
                        }
                    });

                    ExeRu.modal.dialogs[currentName].options.event = this;
                    ExeRu.modal.dialogs[currentName].toggle();
                    if (currentName == 'login-modal') {
                        $('a.icon').removeClass('active');
                    }

                    return false;
                });
            }
        }
        // Обработка переключения по табам для диалоговых окон
        var tabTriggers = $('[data-tab-target]', container);
        if (tabTriggers.length > 0) {
            for(i=0; i<tabTriggers.length; i++) {
                $(tabTriggers[i]).on('click', function() {
                    if ($(this).hasClass('icon')) {
                        var parent = $('#' + this.getAttribute('data-modal') + ' .modal'),
                            currentElement = parent.find('[data-tab-target="' + this.getAttribute('data-tab-target') + '"]')[0],
                            canExec = $(this).hasClass('active');
                    } else {
                        var currentElement = this,
                            parent = $(currentElement).parents('.modal'),
                            canExec = true;
                    }

                    var tabControl = parent.find('.tabControl')[0];
                    if (tabControl.hasAttribute('data-tab-active')) {
                        $(tabControl).removeClass(tabControl.getAttribute('data-tab-active'));
                        parent.find('[data-tab-target=' + tabControl.getAttribute('data-tab-active') + ']').removeClass('active');
                    }
                    $(currentElement).addClass('active');
                    tabControl.setAttribute('data-tab-active', currentElement.getAttribute('data-tab-target'));
                    $(tabControl).addClass(currentElement.getAttribute('data-tab-target'));

                    if (currentElement.hasAttribute('data-module') && currentElement.hasAttribute('data-method') && canExec) {
                        var module = currentElement.getAttribute('data-module');
                        var method = currentElement.getAttribute('data-method');
                        if (module !== null && method !== null) {
                            ExeRu[module][method](currentElement);
                        }
                    }

                    $('#profile_main, #profile_access, form.sign-form', tabControl).each(function() {
                        $('label.error').hide();
                        this.reset();
                    });

                    return false;
                });

                if ($(tabTriggers[i]).hasClass('active')) {
                    $(tabTriggers[i]).click();
                }
            }
        }
    }
};