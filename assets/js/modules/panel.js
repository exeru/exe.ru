/**
 * Created by ruslan on 05.09.16.
 */
/*var ExeRu = ExeRu || {};*/
ExeRu.panel = {
    updateCount: function(type, count) {
        var element = $('.site-header__menu .icon__' + type + ' .panel__count');
        if (count == 0) {
            element
                .html(0)
                .hide();
        } else {
            element
                .html(count)
                .show();
        }
    },
    parseEvent: function(event) {
        var element = $('.site-header__menu .icon__' + event.subtype + ' .panel__count'),
            count;

        switch(event.subtype) {
            case 'news': {
                count = parseInt(element.html());
                if (event.action == 'add') {
                    count++;
                } else {
                    count--;
                }

                if (count < 0) {
                    count = 0;
                }
                break;
            }
            case 'friends':
            case 'comments': {
                count = event.count;
                break;
            }
        }


        ExeRu.panel.updateCount(event.subtype, parseInt(count));
    }
};
