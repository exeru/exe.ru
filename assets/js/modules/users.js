/*var ExeRu = ExeRu || {};*/
ExeRu.users = {
    urlGetUserInfo: '/ajax/getInfoAboutUser/',
    urlGetExtUserInfo: '/ajax/getExtendedInfoAboutUser/',

    urlGetBlackList: '/ajax/getBlackList/',
    urlToBlackList: '/ajax/toBlacklist/',
    urlFromBlackList: '/ajax/fromBlacklist/',

    urlSendEmailCheck: '/ajax/emailConfirm/',

    cache: {},
    extendedCache: {},

    init: function(container) {

    },
    setDestination: function(eventElement) {
        if ($(eventElement).hasClass('tab-register') && ($('#login-modal').length == 1)) {
            $('#login-modal [data-tab-target="tab_2"]').trigger('click');
        } else {
            $('#login-modal [data-tab-target="tab_1"]').trigger('click');
        }
        if (eventElement.hasAttribute('data-dest')) {
            var destinationId = eventElement.getAttribute('data-dest');
            $('form.sign-form [name="dest_id"]').val(destinationId);
            $('.sign-social-list a').each(function() {
                this.setAttribute('data-dest', destinationId);
            });
        } else {
            $('form.sign-form [name="dest_id"]').val('');
            $('.sign-social-list a').each(function() {
                this.removeAttribute('data-dest');
            });
        }
    },
    getUser: function(eventElement) {
        ExeRu.inProgress.on();

        if (typeof(eventElement) == 'object') {
            var uid = eventElement.getAttribute('data-uid');
        } else {
            var uid = eventElement;
        }
        var user = ExeRu.users.getUserData(uid, true);

        if (eventElement.hasAttribute('no-push-history')) {
            eventElement.removeAttribute('no-push-history');
        } else {
            history.pushState({}, '', '/id' + uid);
        }

        // Заголовок придется поменять отсуда, т.к. он не входит в шаблон
        /*$('#user-modal .modal__header h2').html(strip_by_length(user.name, 15) + ' ' + strip_by_length(user.last_name, 20));*/
        $('#user-modal .modal__header h2').html(user.name + ' ' + (user.last_name == null ? '' : user.last_name));

        // Внимание! Изменяя объект user, мы изменяем его свойства в кеше
        // Подготовим дату рождения и возраст
        if (typeof(user.birthday) === 'undefined') {
            user.birthday = '';
            var bday = user.bday;
            if (bday > 0) {
                if (bday < 1000) {
                    bday = '0' + bday;
                }
                var d = bday[2] + bday[3];
                var m = bday[0] + bday[1];
                user.birthday = 'День рождения: ' + parseInt(d) + ' ';
                user.birthday += ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'][parseInt(m) - 1];
            } else {
                var d = 1;
                var m = 7;
            }
        }

        if (typeof(user.age) === 'undefined') {
            user.age = '';
            if (user.byear > 1917) {
                var y = user.byear;
                var t = new Date();
                var a = ( t.getFullYear() - y - ((t.getMonth() - --m||t.getDate() - d)<0) );
                user.age = a + plural_str(a, ' год', ' года', ' лет');
            }
        }

        if (user.friendType == 'blocked') {
            var html = new EJS({url:'/assets/js/views/userProfile_blocked.ejs?' + ExeRu.version}).render(user);
            $('#user-modal [data-tab-target="tab_2"]').parent().hide();
        } else {
            var html = new EJS({url:'/assets/js/views/userProfile.ejs?' + ExeRu.version}).render(user);
            $('#user-modal [data-tab-target="tab_2"]').parent().show();
        }

        $('#user-modal .tabControl').html(html);

        $("#user-modal .row__more--open").click(function(){
            var parent = $(this).parent();
            $(".row__more-panel", parent).css("display", "block");

            var height = $($(".row__more-panel", parent).children()[0]).outerHeight();

            $(".row__more-panel", parent).animate({
                height: height,
                display: "block"
            });

            $(".row__more", parent).toggle();
        });

        $("#user-modal .row__more--close").click(function(){
            var parent = $(this).parent();

            $(".row__more-panel", parent).animate({
                height: "0px"
            }, "slow", function(){
                $(".row__more-panel", parent).css("display", "none");
            });

            $(".row__more", parent).toggle();
        });

        var popoverOptions = {
            template: '\
                    <div class="tooltip tooltip-profile popover-profile">\
                        <div class="tooltip-arrow"></div>\
                        <h3 class="popover-title"></h3>\
                        <div class="popover-content"></div>\
                    </div>',
            html: true
        };

        // Снимаем переход с ссылок-кнопок
        $("#user-modal .tabControl .friend-type a").on('click blur', function(e) {
            if (e.type == 'click') {
                $(this).popover("show");
            } else {
                $(this).popover("hide");
            }
            return false;
        });

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileCancelRequest(this);">Удалить из друзей</a>';
        $("#user-modal .tabControl .friend-type-both.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileAcceptRequest(this);">Подтвердить</a>';
        $("#user-modal .tabControl .friend-type-me.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileCancelRequest(this);">Отменить заявку</a>';
        $("#user-modal .tabControl .friend-type-i.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileRequestToFriends(this);">Добавить в друзья</a>';
        $("#user-modal .tabControl .friend-type-rejected.friends-button-left a").popover(popoverOptions);

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileDeleteFromBlackList(this);">Удалить из черного списка</a>';
        $("#user-modal .tabControl .friend-type-blacklist.friends-button-left a").popover(popoverOptions);

        popoverOptions = {
            template: '\
                    <div class="tooltip tooltip-profile popover-profile popover-big">\
                        <div class="tooltip-arrow"></div>\
                        <h3 class="popover-title"></h3>\
                        <div class="popover-content"></div>\
                    </div>',
            html: true
        };

        popoverOptions.content = '<a href="#" onclick="return ExeRu.friends.userProfileDeleteFromBlackList(this);">Удалить из черного списка</a>';
        $("#user-modal .tabControl .friend-type-blacklist.friends-button-right a").popover(popoverOptions);


        // TODO Обработчики
        $('#user-modal .tabControl [data-module][data-method]:not([data-modal])').on('click', function(e) {
            $('html, body').animate({scrollTop: 0}, 1000);
            var module = this.getAttribute('data-module');
            var method = this.getAttribute('data-method');
            if (module !== null && method !== null) {
                ExeRu[module][method](this);
            }
            return false;
        });

        ExeRu.modal.init('#user-modal .tabControl');
        $('#user-modal .modal__tabs a:first').click();
        ExeRu.inProgress.off();
    },
    debug: function(element){
        ExeRu.inProgress.on();
        var uid = $(element).parents('.friend-type')[0].getAttribute('data-uid');
        ExeRu.inProgress.off();
        return false;
    },
    getUserData: function(uid, extended) {
        if (extended === true) {
            if (typeof ExeRu.users.extendedCache[window.uid] === 'undefined' && window.uid != uid) {
                ExeRu.users.getUserData(window.uid, true);
            }
            // Расширенный кеш пока будет дублировать часть информации из основного
            if (typeof ExeRu.users.extendedCache[uid] === 'undefined') {
                $.ajax({
                    method: "GET",
                    url: ExeRu.users.urlGetExtUserInfo,
                    data: {uid: uid},
                    async: false
                }).success(function(r) {
                    ExeRu.users.extendedCache[uid] = r;
                    count = 500;
                    offset = 0;
                    more = true;
                    uids = [];
                    do {
                        $.ajax({
                            method: "GET",
                            url: '/api/',
                            data: {
                                api_id: 1,
                                format: 'json',
                                method: 'getFriends',
                                user_id: uid,
                                fields: 'name,last_name,photo_50,photo_100,photo_200_orig',
                                order: 'hints',
                                sid: sid,
                                count: count,
                                offset: offset,
                                rnd: Math.random()
                            },
                            async: false
                        }).success(function(r) {
                            if (r.response && r.response.length > 0) {
                                for(var i=0; i<r.response.length; i++) {
                                    var friend = r.response[i];
                                    friend['photo'] = {};
                                    friend['photo']['70'] = r.response[i].photo_50;
                                    friend['photo']['120'] = r.response[i].photo_100;
                                    friend['photo']['270'] = r.response[i].photo_200_orig;
                                    ExeRu.users.extendedCache[uid].friends.push(friend);
                                    uids.push(friend.uid);
                                }
                                if (r.response.length == count) {
                                    offset += count;
                                } else {
                                    more = false;
                                }
                            } else {
                                more = false;
                            }
                        }).error(function(r){
                            more = false;
                        });
                    } while(more);

                    ExeRu.users.extendedCache[uid].friends.reverse();

                    $.ajax({
                        method: "POST",
                        url: '/ajax/getFriendsTypeAndState/',
                        data: {uids: uids},
                        async: false
                    }).success(function(r) {
                        if (r.friends && r.friends.length > 0) {
                            for(var i=0; i<r.friends.length; i++) {
                                for(var j=0; j<ExeRu.users.extendedCache[uid].friends.length; j++) {
                                    if (r.friends[i].uid == ExeRu.users.extendedCache[uid].friends[j].uid) {
                                        ExeRu.users.extendedCache[uid].friends[j].online = r.friends[i].online;
                                        ExeRu.users.extendedCache[uid].friends[j].friendType = '';
                                    }
                                }
                            }
                        }
                        if (window.uid != uid && ExeRu.users.extendedCache[uid].friends.length > 0 && ExeRu.users.extendedCache[window.uid].friends.length > 0) {
                            for(var i=0; i<ExeRu.users.extendedCache[window.uid].friends.length; i++) {
                                for(var j=0; j<ExeRu.users.extendedCache[uid].friends.length; j++) {
                                    if (ExeRu.users.extendedCache[window.uid].friends[i].uid == ExeRu.users.extendedCache[uid].friends[j].uid) {
                                        ExeRu.users.extendedCache[uid].friends[j].friendType = 'both'; 
                                    }
                                }
                            }
                        }
                    });


/*                    $.get('/ajax/getFriendsTypeAndState/', {uids: uids}, function(r) {
                        if (r.friends && r.friends.length > 0) {
                            for(var i=0; i<r.friends.length; i++) {
                                for(var j=0; j<ExeRu.users.extendedCache[uid].friends.length; j++) {
                                    if (r.friends[i].uid == ExeRu.users.extendedCache[uid].friends[j].uid) {
                                        ExeRu.users.extendedCache[uid].friends[j].online = r.friends[i].online;
                                        ExeRu.users.extendedCache[uid].friends[j].friendType = r.friends[i].friendType;
                                    }
                                }
                            }
                        }
                    });*/
                });
            }
            return ExeRu.users.extendedCache[uid];
        } else {
            if (typeof ExeRu.users.cache[uid] === 'undefined') {
                $.ajax({
                    method: "GET",
                    url: ExeRu.users.urlGetUserInfo,
                    data: {uid: uid},
                    async: false
                }).success(function(r) {
                    ExeRu.users.cache[uid] = r;
                });
            }
            return ExeRu.users.cache[uid];
        }
    },
    deleteFromCache: function(uid) {
        delete ExeRu.users.cache[uid];
        delete ExeRu.users.extendedCache[uid];
    },

    renderBlacklist: function() {
        ExeRu.inProgress.on();
        ExeRu.request.get(ExeRu.users.urlGetBlackList, {}, function(r) {
            r.source = 'blacklist';
            $('#blacklist--search_user .modal__search--blacklist').val('');
            var html = new EJS({url:'/assets/js/views/blackList.ejs?' + ExeRu.version}).render(r);
            $('#blacklist-wrap').html(html);
            ExeRu.modal.init($('#blacklist-wrap'));
            ExeRu.init.buttons($('#blacklist-wrap'));
            ExeRu.inProgress.off();
        });
    },
    toBlackList: function(eventElement) {
        ExeRu.inProgress.on();
        var uid = eventElement.getAttribute('data-uid');
        var source = $(eventElement).parents('[data-source]')[0].getAttribute('data-source');
        ExeRu.request.get(ExeRu.users.urlToBlackList, {uid: uid}, function(r) {
            if (r.errors !== undefined) {
                ExeRu.inProgress.off();
                alert(r.errors.summary);
            } else if (r.location !== undefined) {
                ExeRu.users.deleteFromCache(uid);
                ExeRu.inProgress.off();
                if (source == 'search') {
                    $('#settings-modal .modal__search--submit').click()
                } else {
                    $('#settings-modal .active').click();
                }
            }
        });
    },
    fromBlackList: function(eventElement) {
        ExeRu.inProgress.on();
        var uid = eventElement.getAttribute('data-uid');
        var source = $(eventElement).parents('[data-source]')[0].getAttribute('data-source');
        ExeRu.request.get(ExeRu.users.urlFromBlackList, {uid: uid}, function(r) {
            if (r.errors !== undefined) {
                ExeRu.inProgress.off();
                alert(r.errors.summary);
            } else if (r.location !== undefined) {
                ExeRu.users.deleteFromCache(uid);
                ExeRu.inProgress.off();
                if (source == 'search') {
                    $('#settings-modal .modal__search--submit').click()
                } else if (source == 'friends') {
                    $('#friends-modal .modal__search--submit-wrap').click();
                } else {
                    $('#settings-modal .active').click();
                }
            }
        });
    },
    isOnline: function(uid) {
        if (typeof ExeRu.users.extendedCache[uid] !== 'undefined') {
            return ExeRu.users.extendedCache[uid].online;
        } else if (typeof ExeRu.users.cache[uid] !== 'undefined') {
            return ExeRu.users.cache[uid].online;
        } else {
            var user = ExeRu.users.getUserData(uid);
            return ExeRu.users.cache[uid].online;
        }
    },
    setOnline: function(uid) {
        if (typeof ExeRu.users.extendedCache[uid] !== 'undefined') {
            ExeRu.users.extendedCache[uid].online = true;
        }
        if (typeof ExeRu.users.cache[uid] !== 'undefined') {
            ExeRu.users.cache[uid].online = true;
        }
    },
    setOffline: function(uid) {
        if (typeof ExeRu.users.extendedCache[uid] !== 'undefined') {
            ExeRu.users.extendedCache[uid].online = false;
        }
        if (typeof ExeRu.users.cache[uid] !== 'undefined') {
            ExeRu.users.cache[uid].online = false;
        }
    },
    setTabAccessSettings: function(eventElement) {
        $('#settings-modal .modal-tabs__list a[data-tab-target="tab_6"]')[0].click();
        return false;
    },
    setTabPhotoSettings: function(eventElement) {
        $('#settings-modal .modal-tabs__list a[data-tab-target="tab_7"]')[0].click();
        return false;
    },
    sendEmailCheck: function(eventElement) {
        $.post(ExeRu.users.urlSendEmailCheck, {}, function(r) {
            alert(r.errors.summary);
        });
    }
};