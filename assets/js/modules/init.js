/*var ExeRu = ExeRu || {};*/
ExeRu.init = {
    modules: function(container) {
        ExeRu.events.init(); // Ждем событий от сервера
        ExeRu.game.init();
        ExeRu.news.init();
        ExeRu.friends.init(); // Действия в модальном окне друзей.
        ExeRu.modal.init(container); // Диалоговые окна и переключение по табам
        ExeRu.dialogs.init();
    },
    buttons: function(container) {
        $('a.btn-main,.form-submit:button', container).on('click', ExeRu.call);
    }
};