/*var ExeRu = ExeRu || {};*/
ExeRu.friends = {
    urlSearch: '/ajax/search_friends/',
    urlGetFriends: '/ajax/getFriends/',
    urlGetFriendsCount: '/ajax/getFriendsCount/',

    urlAcceptRequest: '/ajax/acceptRequest/',
    urlRejectRequest: '/ajax/rejectRequest/',

    urlRequestToFriends: '/ajax/appendFriend/',
    urlCancelRequest: '/ajax/cancelRequest/',

    urlGetFriendsForInvite: '/ajax/getFriendsForInvite/',
    urlInviteFriendsToGame: '/ajax/inviteToGame/',

    urlParseFriendsByGame: '/ajax/parseFriendsByGame/',
    urlRequestToFriendsByGame: '/ajax/requestToFriendsByGame',

    urlNotificationAppend: '/ajax/notificationAppend',

    init: function() {
        // Кнопка поиска в модельном окне друзей
        $('#friends-modal .modal__search--submit-wrap').on('click', ExeRu.friends.search);
        // Кнопка фильтра в пригласить друзей в игру
        $('#invite-friends-modal .modal__search--submit-wrap').on('click', ExeRu.friends.filterInviteFriends);
        // Кнопка фильтра в найти друзей по игре
        $('#search-friends-modal .modal__search--submit-wrap').on('click', ExeRu.friends.filterSearchFriendsByGame);
    },

    filterSearchFriendsByGame: function(e) {
        ExeRu.friends.applyGameFilter('#search-friends-modal', e.target);
        return false;
    },

    applyGameFilter: function(parentSelector, elementTarget) {
        var parent = $(parentSelector);
        var text = $(elementTarget).parents('.modal__search').find('input:text').val().trim();
        $(elementTarget).parents('.modal__search').find('input:text').val(text);

        var lis = $('li', parent);
        if (lis.length < 1) { // Некого приглашать.
            return false;
        }

        $('.friends-search-wrap ul', parent).show();
        $('.friends-search-wrap .dialogs_list-empty--header', parent).hide();

        if (text.length < 1) { // Пустой поиск - сброс фильтра
            $('li', parent)
                .removeClass('topline')
                .show();

            $('li:lt(3)', parent)
                .addClass('topline');
            return false;
        }

        var parts = text.split(' ');
        var textSelector = '.request-list__who__name';

        lis.removeClass('topline');
        lis.each(function(){
            var text = $(this).find(textSelector).text().toLowerCase();
            var found = true;
            for(var i=0; i<parts.length; ++i) {
                if (parts[i] == '') {
                    continue;
                }

                var part = parts[i].toLowerCase();
                if (-1 == text.indexOf(part)) {
                    found = false;
                    break;
                }
            }
            if (found == false) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        if ($('li:visible', parent).length == 0) {
            if ($('.dialogs_list-empty--header', parent).length == 0) {
                parent.find('.friends-search-wrap').append('<div class="dialogs_list-empty--header">Ничего не найдено.</div>');
            }
            $('.friends-search-wrap ul', parent).hide();
            $('.friends-search-wrap .dialogs_list-empty--header', parent).show();
        } else {
            $('li:visible:lt(3)', parent).addClass('topline');
        }
    },

    requestToFriendsByGame: function() {
        var inputs = $('#search-friends-modal .request-list__who input:checked:visible');
        if (inputs.length < 1) {
            return false;
        }

        ExeRu.inProgress.on();

        var params = inputs.serializeArray();
        params.push({name:'sid', value: gid});

        ExeRu.request.post(ExeRu.friends.urlRequestToFriendsByGame, params, function(r) {
            var button = $('#search-friends-modal [data-method="requestToFriendsByGame"]')[0];
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                $('#search-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
                $('#search-friends-modal').on('closeDialog', {type:'showPossibleFriends', data:r}, ExeRu.friends.apiReturnData);
            }

            $('#search-friends-modal [data-modal-close]').click();
            ExeRu.inProgress.off();
        });
    },

    searchFriendsByGame: function() {
        var root = $('#search-friends-modal');
        var button = $('[data-method="requestToFriendsByGame"]', root)[0];
        if (root[0].hasAttribute('data-source') && root[0].getAttribute('data-source') === 'api') {
            root[0].removeAttribute('data-source');
            button.setAttribute('data-source', 'api');
        } else {
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                button.removeAttribute('data-source');
            }
        }

        $('#search-friends-modal .modal__search input:text').val(''); // Сбрасываем фильтр

        $('#search-friends-modal .friends-active__count').html('0'); // Счетчик выбранных устанавливаем на 0

        var params = {
            api_id: 1,
            format: 'json',
            sid: gid,
            method: 'showPossible'
        };
        ExeRu.inProgress.on();
        ExeRu.request.get('/api/', params, function(r) {
            var uids = [];

            if (r.response.length > 0) {
                for (var i=0; i < r.response.length; ++i) {
                    uids.push(r.response[i].uid);
                }
            }

            ExeRu.request.get(ExeRu.friends.urlParseFriendsByGame, {uids: uids}, function(r) {
                ExeRu.friends.renderSearchFriendsByGame(r.result);
                ExeRu.inProgress.off();
            });

        });
    },

    renderSearchFriendsByGame: function(response) {
        var root = $('#search-friends-modal');

        if (response.length < 1) {
            if ($('.dialogs_list-empty--header', root).length == 0) {
                $('.friends-search-wrap', root).append('<div class="dialogs_list-empty--header">Вам пока некого пригласить.</div>');
            }
            $('.friends-search-wrap ul', root).hide();
            $('.friends-search-wrap .dialogs_list-empty--header', root).show();
            return false;
        }

        $('.friends-search-wrap .dialogs_list-empty--header', root).hide();
        $('.friends-search-wrap ul', root).show();

        //$('.friends-search', root).mCustomScrollbar();

        var html = new EJS({url:'/assets/js/views/searchFriendsByGame.ejs?' + ExeRu.version}).render({friends: response});
        $('.friends-search', root).html(html);

        $('.request-list__who:not(.request-list__who--played)', root).on('click', function() {
            var count = $('#search-friends-modal .request-list__who:not(.request-list__who--played) input:checked').length;
            $('#search-friends-modal .friends-active__count').html(count);
        });
    },

    filterInviteFriends: function(e) {
        ExeRu.friends.applyGameFilter('#invite-friends-modal', e.target);
        return false;
    },

    apiSearchFriendsByGame: function() {
        $('#search-friends-modal')[0].setAttribute('data-source', 'api');
        $('[data-method="searchFriendsByGame"]')[0].click(); // TODO Костыль, позволяет избежать двух вызовов метода. Надо переделать.
        $('#search-friends-modal').on('closeDialog', {type:'showPossibleFriends', data:[]}, ExeRu.friends.apiReturnData);
    },

    apiInviteFriends: function(e) {
        var root = $('#invite-friends-modal'), data = e.data;

        if (data && data.request_key && data.request_key.length > 0) {
            root[0].setAttribute('data-request_key', data.request_key);
        }

        root[0].setAttribute('data-source', 'api');

        $('[data-method="inviteFriends"]')[0].click();
        //root[0].click(); // TODO Костыль, позволяет избежать двух вызовов метода. Надо переделать.
        root.off('closeDialog', ExeRu.friends.apiReturnData);
        root.on('closeDialog', {type:'showInviteFriends', data:{response:[]}}, ExeRu.friends.apiReturnData);
    },

    apiReturnData: function(e) {
        $('#invite-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
        window.frames[0].postMessage({type:e.data.type, data:e.data.data}, '*');
    },

    inviteFriends: function(e) {
        var root = $('#invite-friends-modal');
        var button = $('[data-method="sendInviteFriends"]', root)[0];
        if (root[0].hasAttribute('data-source') && root[0].getAttribute('data-source') === 'api') {
            root[0].removeAttribute('data-source');
            button.setAttribute('data-source', 'api');
        } else {
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                button.removeAttribute('data-source');
            }
        }

        if (root[0].hasAttribute('data-request_key')) {
            button.setAttribute('data-request_key', root[0].getAttribute('data-request_key'));
            root[0].removeAttribute('data-request_key');
        } else {
            if (button.hasAttribute('data-request_key')) {
                button.removeAttribute('data-request_key');
            }
        }

        var params = {
            api_id: 1,
            format: 'json',
            sid: gid,
            method: 'showInvitible'
        };
        var apiResponse = null, friendsResponse = null;

        ExeRu.inProgress.on();

        ExeRu.request.get('/api/', params, function(r) {
            apiResponse = r;
            ExeRu.friends.renderInviteFriends(apiResponse, friendsResponse);
        });

        ExeRu.request.get(ExeRu.friends.urlGetFriendsForInvite, {sid: gid}, function(r) {
            friendsResponse = r;
            ExeRu.friends.renderInviteFriends(apiResponse, friendsResponse);
        });
    },

    renderInviteFriends: function(apiResponse, friendsResponse) {
        if (apiResponse == null || friendsResponse == null) {
            return false;
        }

        var root = $('#invite-friends-modal');

        if (friendsResponse.result.length < 1) {
            if ($('.dialogs_list-empty--header', root).length == 0) {
                $('.friends-search-wrap', root).append('<div class="dialogs_list-empty--header">Вам пока некого пригласить.</div>');
            }
            $('.friends-search-wrap ul', root).hide();
            $('.friends-search-wrap .dialogs_list-empty--header', root).show();
            ExeRu.inProgress.off();
            return false;
        }

        // Собираем из данных новый объект
        var users = {};
        for (i=0; i<apiResponse.response.length; ++i) {
            users[apiResponse.response[i].uid] = apiResponse.response[i];
        }

        for (var i=0; i<friendsResponse.result.length; ++i) {
            if (typeof(users[friendsResponse.result[i].uid]) == 'object') {
                friendsResponse.result[i].play = users[friendsResponse.result[i].uid].play;
            }
        }

        $('.friends-search-wrap .dialogs_list-empty--header', root).hide();
        $('.friends-search-wrap ul', root).show();

        var html = new EJS({url:'/assets/js/views/inviteFriends.ejs?' + ExeRu.version}).render({friends: friendsResponse.result});
        $('.friends-search', root).html(html);

        $('.modal__search input:text', root).val(''); // Сбрасываем фильтр

        $('.friends-active__count', root).html('0'); // Счетчик выбранных устанавливаем на 0
        $('.request-list__who:not(.request-list__who--played)', root).on('click', function() {
            var count = $('#invite-friends-modal .request-list__who:not(.request-list__who--played) input:checked').length;
            $('#invite-friends-modal .friends-active__count').html(count);
        });

        ExeRu.inProgress.off();
    },

    sendInviteFriends: function(eventElement) {
        var inputs = $('#invite-friends-modal .request-list__who:not(.request-list__who--played) input:checked:visible');
        if (inputs.length < 1) {
            return false;
        }

        ExeRu.inProgress.on();

        var params = inputs.serializeArray();
        params.push({name:'sid', value: gid});

        var button = $('#invite-friends-modal [data-method="sendInviteFriends"]')[0];

        if (button.hasAttribute('data-request_key')) {
            params.push({name:'request_key', value: button.getAttribute('data-request_key')});
        }

        ExeRu.request.post(ExeRu.friends.urlInviteFriendsToGame, params, function(r) {
            if (button.hasAttribute('data-source') && button.getAttribute('data-source') === 'api') {
                $('#invite-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
                $('#invite-friends-modal').on('closeDialog', {type:'showInviteFriends', data:{response:r.users}}, ExeRu.friends.apiReturnData);
            }
            $('#invite-friends-modal [data-modal-close]').click();
            ExeRu.inProgress.off();
        });
    },

    // Профиль другого пользователя, перерисовка блока всех друзей и общих друзей по фильтру
    showFriends: function(element) {
        var uid = element.getAttribute('data-uid');
        var filter = element.getAttribute('data-filter');
        var user = ExeRu.users.getUserData(uid, true);
        var result = [];

        if (filter != 'search') {
            $(element).parents('.modal__search__list').find('a').removeClass('active');
            $(element).addClass('active');
            $(element).parents('.modal__search').find('.modal__search--text').val('');
        }

        var type = $(element).parents('.modal__search').find('a.active').data('filter');
        var text = $(element).parents('.modal__search').find('.modal__search--text').val().trim();

        if (type == '') {
            result = user.friends;
        } else if (type == 'both') {
            if (user.friends.length > 0) {
                for(i=0; i<user.friends.length; ++i) {
                    if (user.friends[i].friendType == type) {
                        result.push(user.friends[i]);
                    }
                }
            }
        }

        var friends = [];

        if (result.length > 0) {
            if (text.length < 3) {
                var friends = result;
            } else {
                var textParts = text.toLowerCase().split(' ');
                for(i=0; i<result.length; ++i) {
                    var found = true;
                    var sourceString = (result[i].name + ' ' + result[i].last_name).toLowerCase();
                    for(j=0; j<textParts.length; ++j) {
                        if (sourceString.indexOf(textParts[j]) == -1) {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        friends.push(result[i]);
                    }
                }
                type = 'search';
            }
        }

        var html = new EJS({url:'/assets/js/views/userProfile_friends.ejs?' + ExeRu.version}).render({friends: friends, listType: type});

        $(element).parents('.tabControl').find('.friends-search-wrap').html(html);

        $('#user-modal .tabControl .friends-search-wrap [data-module][data-method]').on('click', function() {
            $('html, body').animate({scrollTop: 0}, 1000);
            var module = this.getAttribute('data-module');
            var method = this.getAttribute('data-method');
            if (module !== null && method !== null) {
                ExeRu[module][method](this);
            }
            return false;
        });
    },

    // Подтвердить дружбу (с генерацией новости)
    userProfileAcceptRequest: function(element) {
        return ExeRu.friends.userProfileDoRequest(ExeRu.friends.urlAcceptRequest, element);
    },
    // Добавить в друзья (подписаться)
    userProfileRequestToFriends: function(element) {
        return ExeRu.friends.userProfileDoRequest(ExeRu.friends.urlRequestToFriends, element);
    },
    // Удалить из друзей, отменить заявку
    userProfileCancelRequest: function(element) {
        return ExeRu.friends.userProfileDoRequest(ExeRu.friends.urlCancelRequest, element);
    },
    // Удалить из черного списка
    userProfileDeleteFromBlackList: function(element) {
        ExeRu.inProgress.on();
        var parent = $(element).parents('.friend-type')[0];
        var uid = parent.getAttribute('data-uid');
        ExeRu.users.deleteFromCache(uid);
        ExeRu.request.get(ExeRu.users.urlFromBlackList, {uid: uid}, function(r) {
            if (r.location !== undefined) {
                ExeRu.users.getUser(parent);
                ExeRu.inProgress.off();
            }
        });
        return false;
    },
    // Действия практически идентичные, можно убрать в одну обвертку
    userProfileDoRequest: function(url, element) {
        ExeRu.inProgress.on();
        var parent = $(element).parents('.friend-type')[0];
        var params = {
            fid: parent.getAttribute('data-uid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(url, params, function(r) {
            if (r.response == 1) {
                ExeRu.users.getUser(parent);
                ExeRu.inProgress.off();
            }
        });
        return false;
    },

    // Заявки в друзья - принять заявку
    meAcceptRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlAcceptRequest, params, function(r) {
            ExeRu.friends.renderListNoReload(r);
            var length = parseInt($('#friends-modal .modal-tabs__list a:first span').html()) + 1;
            $('#friends-modal .modal-tabs__list a:first span').html(length);
        });
    },

    // Заявки в друзья - отклонить заявку
    meRejectRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlRejectRequest, params, ExeRu.friends.renderListNoReload);
    },

    // Мои заявки - отменить мою заявку
    myRequestsCancelRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlCancelRequest, params, ExeRu.friends.renderListNoReload);
    },

    // Отрисовывает списки без перезагрузки. Меняет значения в табах. Если пусто - переходит на друзей
    renderListNoReload: function(r) {
        if (r.response == 1) {
            var element = $('[data-fid="' + r.fid + '"]').parents('li');
            var parent = element.parent();
            element.remove();

            var length = parseInt($('#friends-modal .modal-tabs__list a.active span').html()) - 1;
            $('#friends-modal .modal-tabs__list a.active span').html('+' + length);

            if (length == 0) { // Не осталось элементов
                $('#friends-modal .modal-tabs__list li:first-child a').click();
            } else {
                //$('#friends-modal .modal-tabs__list a.active').click();
            }

            var showMore = parent.parents('.modal__content').find('.dev-billing__history-more a');
            if (showMore.length == 1) {
                oldValue = showMore.attr('data-count');
                showMore.attr('data-start', (parseInt(showMore.attr('data-start')) - 1));
                showMore.attr('data-count', 1);
                showMore.click();

                showMore.attr('data-count', oldValue);
            }

            ExeRu.inProgress.off();

            return false;
        }
    },

    // Поиск: Мне прислали заявку, я согласен дружить
    searchAcceptRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlAcceptRequest, params, function(r) {
            if (r.response == 1) {
                ExeRu.inProgress.off();
                $('#friends-modal .modal__search--submit-wrap').click();
            }
        });
    },

    // Поиск: Мне прислали заявку, я НЕ согласен с ним дружить (такого действия в поиске нет)
    searchRejectRequest: function(eventElement) {},

    // Поиск: Я отправляю заявку на дружбу с этим человеком
    searchRequestToFriends: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlRequestToFriends, params, function(r) {
            if (r.response == 1) {
                ExeRu.inProgress.off();
                $('#friends-modal .modal__search--submit-wrap').click();
            }
        });
    },

    // Поиск: Я отменяю свою заявку на дружбу с этим человеком
    searchCancelRequest: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            fid: eventElement.getAttribute('data-fid')
        };
        ExeRu.users.deleteFromCache(params.fid);
        ExeRu.request.get(ExeRu.friends.urlCancelRequest, params, function(r) {
            if (r.response == 1) {
                ExeRu.inProgress.off();
                $('#friends-modal .modal__search--submit-wrap').click();
            }
        });
    },

    // Поиск: Я удаляю этого человека из друзей
    searchDeleteFromFriends: function(eventElement) {
        ExeRu.friends.searchCancelRequest(eventElement);
    },

    // Поиск
    search: function(e) {
        var text = $(e.target).parents('.modal__search').find('input:text').val();
        if (text.length < 3) {
            return false;
        }
        // Ставим тень
        ExeRu.inProgress.on();
        // Получаем найденных пользователей и отрисовываем блок
        ExeRu.request.get(ExeRu.friends.urlSearch, {search_string: text}, function(r) {
            // Перерисуем табы
            ExeRu.friends.renderTabs(r.tabs, 'tab_1');
            // Меняем содержимое окна
            ExeRu.friends.renderSearch(r);
            // Снимаем тень
            ExeRu.inProgress.off();
        });
        return false;
    },

    // Отрисовка содержимого блока поиска
    renderSearch: function(r) {
        var html = new EJS({url:'/assets/js/views/friendsSearch.ejs?' + ExeRu.version}).render(r);
        $('#friends-modal .friends-search-wrap').html(html);
        ExeRu.modal.init($('#friends-modal .friends-search-wrap'));
        ExeRu.init.buttons($('#friends-modal .friends-search-wrap'));
    },

    // Отрисовка табов в модальном окне друзей
    renderTabs: function(result, tabTarget) {
        var html = new EJS({url:'/assets/js/views/friendsTabs.ejs?' + ExeRu.version}).render(result);
        $('#friends-modal .modal__tabs').html(html);
        var activeTabElement = $('#friends-modal .modal-tabs__list .active');
        activeTabElement.removeClass('active');
        ExeRu.modal.init($('#friends-modal'));
        ExeRu.init.buttons($('#friends-modal'));
        $('#friends-modal .modal-tabs__list [data-tab-target="' + tabTarget + '"]').addClass('active');
    },

    // Отрисовка списка друзей
    renderList: function(eventElement) {

        ExeRu.inProgress.on();
        var tabTarget = eventElement.getAttribute('data-tab-target');
        if (tabTarget == null) {
            tabTarget = 'tab_1';
        }
        ExeRu.request.get(ExeRu.friends.urlGetFriendsCount, {}, function(r) {
            ExeRu.friends.renderTabs(r.result, tabTarget);

            var settings = {};

            if (tabTarget == 'tab_1') {
                settings.type = 'both';
                settings.template = 'friendsList';
                settings.count = 12;
                // Сбрасываем поиск
                $('#friends-modal input.modal__search--text').val('');
                if ($('#friends--both ul.request-list').length != 0) {
                    $('#friends--both ul.request-list').remove();
                }
            } else if (tabTarget == 'tab_2') {
                settings.type = 'me';
                settings.template = 'friendsRequest';
                settings.count = 5;
            } else if (tabTarget == 'tab_3') {
                settings.type = 'i';
                settings.template = 'friendsRequest';
                settings.count = 5;
            }

            if (r.result[settings.type] == 0) {
                var html = $(new EJS({url:'/assets/js/views/' + settings.template + '.ejs?' + ExeRu.version}).render({list:[], type: settings.type}));
                ExeRu.modal.init(html);
                ExeRu.init.buttons(html);
                $('#friends--' + settings.type).html(html);
                ExeRu.inProgress.off();
            } else {
                ExeRu.request.get(ExeRu.friends.urlGetFriends, {type:settings.type, start:0, count:settings.count}, function(list) {
                    var html = $(new EJS({url:'/assets/js/views/' + settings.template + '.ejs?' + ExeRu.version}).render({list:list, type: settings.type}));
                    ExeRu.modal.init(html);
                    ExeRu.init.buttons(html);
                    $('#friends--' + settings.type).html(html);
                    if (r.result[settings.type] > settings.count) {
                        var html = $(new EJS({url:'/assets/js/views/friendsButtonMore.ejs?' + ExeRu.version}).render(settings));
                        $('a', html).on('click', function() {
                            ExeRu.inProgress.on();
                            var settings = {
                                type: this.getAttribute('data-type'),
                                template: this.getAttribute('data-template'),
                                count: this.getAttribute('data-count'),
                                start: this.getAttribute('data-start')
                            };
                            ExeRu.request.get(ExeRu.friends.urlGetFriends, {type:settings.type, start:settings.start, count:settings.count}, function(list) {
                                var html = $(new EJS({url:'/assets/js/views/' + settings.template + '.ejs?' + ExeRu.version}).render({list:list, type: settings.type})),
                                    button = $('#friends--' + settings.type + ' .dev-billing__history-more');

                                $('li', html).each(function() {
                                    ExeRu.modal.init(this);
                                    ExeRu.init.buttons(this);
                                    $('#friends--' + settings.type + ' ul').append(this);
                                });


                                if (list.length < settings.count) {
                                    button.remove();
                                } else {
                                    $('a', button).attr('data-start', parseInt($('a', button).attr('data-start')) + parseInt(settings.count));
                                }
                                ExeRu.inProgress.off();
                            });
                            return false;
                        });
                        $('#friends--' + settings.type).append(html);
                    }
                    ExeRu.inProgress.off();
                });
            }
        });
    },
    showRequestBox: function(request) {
        var root = $('#add-request-modal'),
            user = ExeRu.users.getUserData(request.data.uid);

        root.find(".news-list__item__author p").html(request.data.text);
        root.find(".add-feed span").html(strip_by_length(user.name, 15) + ' ' + strip_by_length(user.last_name, 15));

        root.off('closeDialog');
        root.find('.btn-main').off('click');

        root.on('closeDialog', {}, function(e) {
            var root = $('#add-request-modal');
            root.off('closeDialog');
            root.find('.btn-main').off('click');
            window.frames[0].postMessage({type:'showRequestBox', data:{response:false}}, '*');
        });

        root.find('.btn-main').on('click', {data: request.data}, function(e) {
            ExeRu.inProgress.on();
            var root = $('#add-request-modal'),
                params = {
                    'sid':gid,
                    'data': e.data.data
                };
            root.off('closeDialog');
            root.find('.btn-main').off('click');

            $.get(ExeRu.friends.urlNotificationAppend, params, function(r){
                window.frames[0].postMessage({type:'showRequestBox', data:{response:true}}, '*');
                ExeRu.inProgress.off();
                $('#add-request-modal [data-modal-close]').click();
            });

            return false;
        });

        ExeRu.modal.dialogs['add-request-modal'].options.event = '';
        ExeRu.modal.dialogs['add-request-modal'].toggle();
    }
};

function listener(event) {
    switch(event.data.type) {
        case 'showRequestBox': {
            ExeRu.friends.showRequestBox(event.data);
            break;
        }
        case 'showInviteFriends': {
            ExeRu.friends.apiInviteFriends(event.data);
            break;
        }
        case 'showOrderBox': {
            ExeRu.payments.buy(event.data);
            break;
        }
        case 'showPublishBox': {
            ExeRu.news.showPublishBox(event.data);
            break;
        }
        case 'apiRequest': {
            $.ajax({
                method: "GET",
                url: '/api/',
                dataType: 'json',
                data: event.data.params
            }).success(function (r) {
                window.frames[0].postMessage({type:'apiResponse', data: r, callback: event.data.callback}, '*');
            }).error(function (r) {
                window.frames[0].postMessage({type:'apiResponse', data: false, callback: event.data.callback}, '*');
            });
            break;
        }
        case 'setHeight': {
            ExeRu.game.setHeight(event.data);
            break;
        }
        case 'setGood': {
            var yandexPixelId = false, epAlias = false;
            switch(event.data.data.app_id) {
                case 3:  { yandexPixelId = '5923452583841758097'; if (install == 1) epAlias='GrimmGood'; break;  } // Ферма Гримм
                case 4:  { yandexPixelId = '883514756404975728';  if (install == 1) epAlias='RodinaGood'; break;  } // Родина
                case 9:  { yandexPixelId = '1047551976226950972'; if (install == 1) epAlias='WarbannerGood'; break; } // Знамя войны
                case 10: { yandexPixelId = '3165387179987160625'; if (install == 1) epAlias='SturmGood'; break; } // Штурм
                case 11: { yandexPixelId = '8852841143843547337'; if (install == 1) epAlias='PiratesGood'; break; } // Гроза Морей
                case 12: { yandexPixelId = '2142528923122150705'; if (install == 1) epAlias='NanoGood'; break; } // Нано-Ферма
                case 14: { yandexPixelId = '7631959075412796562'; if (install == 1) epAlias='RoyalclashGood'; break; } // Королевский замес
                case 15: { yandexPixelId = '4196204346024485576'; if (install == 1) epAlias='RumbaGood'; break; } // Румба
                case 19: { yandexPixelId = '3564462929205210775'; if (install == 1) epAlias='BonVoyageGood'; break; } // Бон Вояж
                case 39: { yandexPixelId = '5213031403724456905'; if (install == 1) epAlias='HeroGood'; break; } // Путь война
                case 47: { yandexPixelId = '6431618609614690630'; if (install == 1) epAlias='MusicWarsGood'; break; } // Музвар
                case 62: { yandexPixelId = '3403263354588936414'; if (install == 1) epAlias='WarTankGood'; break; } // Битва Танков
                case 70: { yandexPixelId = '8850537116226743886'; if (install == 1) epAlias='NebesaGood'; break; } // Небеса
                case 76: { yandexPixelId = '2329184630858305291'; if (install == 1) epAlias='WalkingUndeadGood'; break; } // Ходячая Нежить
                case 89: { yandexPixelId = '3036045282580431516'; if (install == 1) epAlias='ImperialGood'; break; } // Империал Хироу
                case 97: { yandexPixelId = '8377556564264629938'; if (install == 1) epAlias='ObitelGood'; break; } // Обитель зла
                case 103: { yandexPixelId = '3216790040329473232'; if (install == 1) epAlias='VegaMixGood'; break; } // Вега Микс
                case 106: { yandexPixelId = '2879400923159599231'; if (install == 1) epAlias='ImperiaGood'; break; } // Империя Онлайн
                case 121: { yandexPixelId = '7766662350414205409'; if (install == 1) epAlias='StormGood'; break; } // Шторм
                case 142: { yandexPixelId = '7108745008784664529'; if (install == 1) epAlias='FazendaGood'; break; } // Фазенда
            }
            if (yandexPixelId !== false) {
                var yandexPixelImg = $('<div><img src="https://mc.yandex.ru/pixel/'+ yandexPixelId + '?rnd=' + Math.random() + '" style="position:absolute; left:-9999px;" alt="" /></div>');
                yandexPixelImg.prependTo('body');
            }
            if (epAlias !== false && epAlias !== '' && typeof epRiseEvent === 'function') {
                epRiseEvent(epAlias);
            }
            break;
        }
        case 'setSuperGood': {
            var yandexPixelId = false, epAlias = false;
            switch(event.data.data.app_id) {
                case 3:  { yandexPixelId = '5900136085920816917'; if (install == 1) epAlias='GrimmSupergood'; break;  } // Ферма Гримм
                case 4:  { yandexPixelId = '3604242323379749020'; if (install == 1) epAlias='RodinaSupergood'; break; } // Родина
                case 9:  { yandexPixelId = '6466355202244941666'; if (install == 1) epAlias='WarbannerSupergood'; break; } // Знамя войны
                case 10: { yandexPixelId = '6203494014439545446'; if (install == 1) epAlias='SturmSupergood'; break; } // Штурм
                case 11: { yandexPixelId = '5610260207562617698'; if (install == 1) epAlias='PiratesSupergood'; break; } // Гроза Морей
                case 12: { yandexPixelId = '1544819080505286572'; if (install == 1) epAlias='NanoSupergood'; break; } // Нано-Ферма
                case 14: { yandexPixelId = '8026351580886195610'; if (install == 1) epAlias='RoyalclashSupergood'; break; } // Королевский замес
                case 15: { yandexPixelId = '8978906350868819562'; if (install == 1) epAlias='RumbaSupergood'; break; } // Румба
                case 19: { yandexPixelId = '8274805363932425221'; if (install == 1) epAlias='BonVoyageSupergood'; break; } // Бон Вояж
                case 39: { yandexPixelId = '4133222358950810994'; if (install == 1) epAlias='HeroSupergood'; break; } // Путь война
                case 47: { yandexPixelId = '2265182111963594150'; if (install == 1) epAlias='MusicWarsSuperGood'; break; } // Музвар
                case 62: { yandexPixelId = '1557509870227811007'; if (install == 1) epAlias='WarTankSuperGood'; break; } // Битва Танков
                case 70: { yandexPixelId = '8381172897501154869'; if (install == 1) epAlias='NebesaSupergood'; break; } // Небеса
                case 76: { yandexPixelId = '6521968731721376546'; if (install == 1) epAlias='WalkingUndeadSupergood'; break; } // Ходячая Нежить
                case 89: { yandexPixelId = '7803724009774583264'; if (install == 1) epAlias='ImperialSupergood'; break; } // Империал Хироу
                case 97: { yandexPixelId = '2840090653474571545'; if (install == 1) epAlias='ObitelSupergood'; break; } // Обитель зла
                case 103: { yandexPixelId = '8485303420510273131'; if (install == 1) epAlias='VegaMixSuperGood'; break; } // Вега Микс
                case 106: { yandexPixelId = '3551126677975527595'; if (install == 1) epAlias='ImperiaSupergood'; break; } // Империя Онлайн
                case 121: { yandexPixelId = '3609716404667292401'; if (install == 1) epAlias='StormSupergood'; break; } // Шторм
                case 142: { yandexPixelId = '5614023024341370844'; if (install == 1) epAlias='FazendaSuperGood'; break; } // Фазенда
            }
            if (yandexPixelId !== false) {
                var yandexPixelImg = $('<div><img src="https://mc.yandex.ru/pixel/'+ yandexPixelId + '?rnd=' + Math.random() + '" style="position:absolute; left:-9999px;" alt="" /></div>');
                yandexPixelImg.prependTo('body');
            }
            if (epAlias !== false && epAlias !== '' && typeof epRiseEvent === 'function') {
                epRiseEvent(epAlias);
            }
            break;
        }
    }
}

if (window.addEventListener) {
    window.addEventListener("message", listener);
} else {
    // IE8
    window.attachEvent("onmessage", listener);
}