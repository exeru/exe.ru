/*var ExeRu = ExeRu || {};*/
ExeRu.dialogs = {
    _typingCache: {},

    urlGetList: '/ajax/dialogsGetList/',
    urlDeleteDialog: '/ajax/dialogsDelete/',
    urlGetDialog: '/ajax/dialogsGetOne/',
    urlDeleteBlock: '/ajax/dialogsDeleteBlock/',
    urlRestoreBlock: '/ajax/dialogsRestoreBlock/',
    urlSendMessage: '/ajax/dialogsSendMessage/',
    urlMarkAsRead: '/ajax/dialogsMarkAsRead',
    urlUserTyping: "/ajax/dialogsTyping/",

    init: function() {
        // Обработка enter в полях сообщений
        $('.form-textarea').attr('data-shift', 0);
        $('.form-textarea').on('keyup keydown', function(e) {
            if (e.which == 16) {
                this.setAttribute('data-shift', this.getAttribute('data-shift') == 0 ? 1 : 0);
            }
            if (e.type == 'keydown' && e.which == 13) {
                if (this.getAttribute('data-shift') == 0) {
                    e.preventDefault();
                    $(this).closest('form').find('input:button').click();
                    return false;
                }
            }

            if (e.type == 'keydown' && $('#oneDialogMessages:visible').length == 1) {
                ExeRu.dialogs.userIsTyping($('#oneDialogMessages').parent().find(':visible input[name="recipientId"]').val());
            }

        });
        // Поиск пользователя в списке диалогов, по тексту диалога
        $('#dialogs-modal .modal__search--submit-wrap').on('click', ExeRu.call);
        // Выпадающее меню на диалоге с пользователем
        $('#dialogs-modal .dropdown-menu [data-module][data-method]').on('click', ExeRu.call);

        $('#dialogs-modal .users-dropdown-list').mCustomScrollbar();

        $('#dialogs-modal input[name="recipient"]').on('focus', function(e) {
            $(this).parents('.form-input-wrap--userlist').addClass('open');
        });

        $('#dialogs-modal input[name="recipient"]').on('keyup', function(e) {
            ExeRu.dialogs.dialogsRecipientsFilter();
            return false;
        });

        $('#dialogs-modal .form-input-wrap--userlist .form-arrow').on('click', function() {
            $('#dialogs-modal .form-input-wrap--userlist').toggleClass('open');
            return false;
        });

        $('.smiles-list img').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $(this).closest('form').find('.form-textarea').trigger('focus');
            var html = '<img src="/assets/img/blank.gif" data-smile="' + this.getAttribute('data-smile') + '" style="background-position:0 -' + (this.getAttribute('data-smile') * 17) + 'px;">';
            try {
                var selection = window.getSelection(),
                    range = selection.getRangeAt(0),
                    temp = document.createElement('div'),
                    insertion = document.createDocumentFragment();

                temp.innerHTML = html;

                var node = temp.firstChild;

                insertion.appendChild(node);

                range.deleteContents();
                range.insertNode(insertion);

                range = range.cloneRange();
                range.setStartAfter(node);
                range.collapse(true);

                selection.removeAllRanges();
                selection.addRange(range);
            } catch (z) {
                try {
                    document.selection.createRange().pasteHTML(html);
                } catch (z) {}
            }

            return false;
        });

    },
    userIsTyping: function(uid) {
        if (ExeRu.users.isOnline(uid)) {
            // Собеседник онлайн
            if (typeof(ExeRu.dialogs._typingCache[uid]) == 'undefined') { // В кеше пусто, отправляем о наборе текста на сервер
                ExeRu.request.get(ExeRu.dialogs.urlUserTyping, {typing: 'on', uid: uid}, function(r) {
                    if (r.response == 0) {
                        ExeRu.users.setOffline(uid);
                    }
                });
            } else {
                clearTimeout(ExeRu.dialogs._typingCache[uid]);
            }
            ExeRu.dialogs._typingCache[uid] = setTimeout(ExeRu.dialogs.userNotTyping, 3000, uid);
        }
    },
    userNotTyping: function(uid) {
        if (ExeRu.users.isOnline(uid)) {
            delete ExeRu.dialogs._typingCache[uid];
            ExeRu.request.get(ExeRu.dialogs.urlUserTyping, {typing: 'off', uid: uid}, function(r) {
                if (r.response == 0) {
                    ExeRu.users.setOffline(uid);
                }
            });
        }
    },
    dialogsRecipientsFilter: function() {
        $('#dialogs-modal input[name="recipientId"]').val('');
        var filter = $('#dialogs-modal input[name="recipient"]').val().trim();
        var lis = $('#dialogs-modal .users-dropdown-list li');
        if (filter.length == 0) {
            lis.show();
            return false;
        }

        var parts = filter.split(' ');

        $(lis).each(function(){
            var text = $(this).text().toLowerCase();
            var found = true;
            for(var i=0; i<parts.length; ++i) {
                if (parts[i] == '') {
                    continue;
                }

                var part = parts[i].toLowerCase();
                if (-1 == text.indexOf(part)) {
                    found = false;
                    break;
                }
            }
            if (found == false) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    },
    getUsers: function(eventElement) {
        ExeRu.inProgress.on();
        $('#dialogs-modal .form-input-wrap--userlist').removeClass('open');
        ExeRu.request.get(ExeRu.friends.urlGetFriends, {}, function(r) {
            var users = [];
            if (r.result.both.length > 0) {
                for(var i=0; i < r.result.both.length; ++i) {
                    users.push(r.result.both[i]);
                }
            }
            if (r.result.i.length > 0) {
                for(var i=0; i < r.result.i.length; ++i) {
                    users.push(r.result.i[i]);
                }
            }
            if (r.result.me.length > 0) {
                for(var i=0; i < r.result.me.length; ++i) {
                    users.push(r.result.me[i]);
                }
            }

            var ul = $('<ul class="form-input-wrap-dropdown users-dropdown-list"></ul>');

            var selectedUid = eventElement.getAttribute('data-from-profile');
            if (selectedUid === null) {
                $('#dialogs-modal .tab_2 input[name="recipientId"]').val('');
                $('#dialogs-modal .tab_2 input[name="recipient"]').val('');
                if ($('#dialogs-modal [data-tab-target="tab_2"]')[0].hasAttribute('data-to-profile')) {
                    $('#dialogs-modal [data-tab-target="tab_2"]')[0].removeAttribute('data-to-profile');
                }
            } else {
                $('#dialogs-modal [data-tab-target="tab_2"]')[0].removeAttribute('data-from-profile');
                $('#dialogs-modal [data-tab-target="tab_2"]')[0].setAttribute('data-to-profile', selectedUid);
            }

            for(var i=0; i < users.length; ++i) {
                ul.append('<li data-uid="' + users[i].uid +'">' + strip_by_length(users[i].name, 15) + ' ' + strip_by_length(users[i].last_name, 20) + '</li>');
            }

            $('#dialogs-modal .users-dropdown-list').replaceWith(ul);

            $('#dialogs-modal .users-dropdown-list li').on('click', function(e) {
                e.stopPropagation();
                var parent = $(this).closest('form');
                parent.find('.form-input-wrap--userlist').toggleClass('open');
                $('input[name="recipientId"]', parent).val(this.getAttribute('data-uid'));
                $('input[name="recipient"]', parent).val(this.textContent);
            });

            // Скроллинг на выборе получателя сообщения
            $('#dialogs-modal .users-dropdown-list').mCustomScrollbar();

            ExeRu.inProgress.off();
        });
    },
    toBlackList: function(eventElement) {
        ExeRu.inProgress.on();
        var uid = $('#tabCurrentDialog a')[0].getAttribute('data-uid');
        ExeRu.request.get(ExeRu.users.urlToBlackList, {uid: uid}, function(r) {
            if (r.errors !== undefined) {
                ExeRu.inProgress.off();
                alert(r.errors.summary);
            } else if (r.location !== undefined) {
                ExeRu.users.deleteFromCache(uid);
                $('#tabCurrentDialog').hide();
                $('#dialogs-modal .modal-tabs__list a[data-tab-target="tab_1"]').click();
                ExeRu.modal.dialogs['dialogs-modal'].toggle();
                ExeRu.modal.dialogs['settings-modal'].toggle();
                $('#settings-modal .modal-tabs__list a[data-tab-target="tab_4"]').click();
                ExeRu.inProgress.off();
            }
        });
        return false;
    },
    confirmDeleteCurrentDialog: function(eventElement) {
        $(eventElement).parents('.open').find('[data-toggle="dropdown"]').click();
        if ($('#oneDialogMessages .news-list__item__delete-message').length == 0) {
            var html = $('<div class="news-list__item__delete-message messages-list__delete-message"><p>Вы действительно хотите удалить диалог?</p><a href="#" class="delete">Удалить</a>&nbsp;&nbsp;&nbsp;<a href="#" class="cancel">Отменить</a></div>');
            $('.cancel', html).on('click', function() {
                $('#oneDialogMessages .news-list__item__delete-message').remove();
                return false;
            });
            $('.delete', html).on('click', ExeRu.dialogs.deleteCurrentDialog);
            $('#oneDialogMessages').prepend(html);
        }
    },
    deleteCurrentDialog: function() {
        ExeRu.inProgress.on();
        var uid = $('#tabCurrentDialog a')[0].getAttribute('data-uid');
        ExeRu.request.get(ExeRu.dialogs.urlDeleteDialog, {uid: uid}, function(r) {
            if (r.response == 1) {
                $('#tabCurrentDialog').hide();
                $('#tabCurrentDialog').parents('.modal-tabs__list').find(' li:first a').click();
                ExeRu.inProgress.off();
            }
        });
        return false;
    },
    dialogsListFilter: function(eventElement) {
        var parent = $('#' + eventElement.getAttribute('data-parentId'));
        var lis = $('li', parent);
        if (lis.length == 0) {
            $('#dialogs-modal .write-message').css('display', '');
            return false;
        }

        $('ul', parent).show();
        $('.dialogs_list-empty--header', parent).hide();
        if ($('#oneDialogMessages:visible').length == 1) {
            lis.unbind('click');
        }


        var filter = $(eventElement).parent().find('.modal__search--text').val();
        filter = $.trim(filter);

        if (filter == '') {
            lis.show();
            $('#dialogs-modal .write-message').css('display', '');
            return false;
        }

        $('#dialogs-modal .write-message').hide();

        var parts = filter.split(' ');
        var textSelector = eventElement.getAttribute('data-textSelector');

        lis.each(function(){
            var text = $(this).find(textSelector).text().toLowerCase();
            var found = true;
            for(var i=0; i<parts.length; ++i) {
                if (parts[i] == '') {
                    continue;
                }

                var part = parts[i].toLowerCase();
                if (-1 == text.indexOf(part)) {
                    found = false;
                    break;
                }
            }
            if (found == false) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        if ($('li:visible', parent).length == 0) {
            if ($('.dialogs_list-empty--header', parent).length == 0) {
                parent.append('<div class="dialogs_list-empty--header">Ничего не найдено.</div>');
            }
            $('ul', parent).hide();
            $('.dialogs_list-empty--header', parent).show();
        } else if ($('#oneDialogMessages:visible').length == 1) {
            $('li:visible', parent).bind('click', function() {
                $('#dialogs-modal .modal__search--text').val('');
                $('#dialogs-modal .write-message').css('display', '');
                $('#dialogs-modal .messages-list li:hidden').show();
                $('#dialogs-modal .messages-list li').unbind('click');

                setTimeout(function(element) {
                    element.addClass('unread');

                    document.getElementById('oneDialogMessages').scrollTop = element[0].offsetTop;

                    setTimeout(function(element) { element.removeClass('unread'); }, 2000, element);
                }, 100, $(this));

                return false;
            });
        }

        return false;
    },
    showDialogsList: function() {
        ExeRu.inProgress.on();
        $('#dialogs-modal .modal__search--text').val('');
        $('#oneDialogMessages .messages-list li').show();
        ExeRu.request.get(ExeRu.dialogs.urlGetList, {}, function(r){
            if (r.response == 1) {
                ExeRu.dialogs.renderDialogsList(r);
            }
        });
    },
    renderDialogsList: function(r) {
        var html = new EJS({url:'/assets/js/views/dialogsList.ejs?' + ExeRu.version}).render(r);
        $('#dialogsList').html(html);

        ExeRu.modal.init($('#dialogsList'));

        //$('#dialogsList .dialog-list').mCustomScrollbar();

/*        $("#dialogsList .delete-data").each(function(){
            $(this).tooltip({
                template: ['<div class="tooltip tooltip-message">',
                    '<div class="tooltip-arrow"></div>',
                    '<div class="tooltip-inner">',
                    '</div>',
                    '</div>'].join('')
            });
        });*/


        $('#dialogsList [data-module][data-method]').on('click', function() {
            var module = this.getAttribute('data-module');
            var method = this.getAttribute('data-method');
            if (module !== null && method !== null) {
                ExeRu[module][method](this);
            }
            return false;
        });

        ExeRu.inProgress.off();
    },
    deleteDialog: function(eventElement) {
        var html = '<div class="news-list__item__delete-message">';
        html += '<p>Вы действительно хотите удалить диалог?</p><a href="#" class="delete">Удалить</a>&nbsp;&nbsp;&nbsp;<a href="#" class="cancel">Отменить</a>';
        html += '</div>';

        var parent = $(eventElement).parent();
        parent.addClass('news-list__item--delete');
        parent.prepend(html);

        $('.cancel', parent).on('click', function() {
            var div = $(this).parents('.news-list__item__delete-message');
            div.parent().removeClass('news-list__item--delete');
            div.remove();
            return false;
        });

        $('.delete', parent).on('click', function() {
            ExeRu.inProgress.on();
            var uid = eventElement.getAttribute('data-uid');
            ExeRu.request.get(ExeRu.dialogs.urlDeleteDialog, {uid: uid}, function(r){
                if (r.response == 1) {
                    if (parent.parent().find('li').length > 1) {
                        parent.remove();
                    } else {
                        $('#dialogsList').html('<div class="dialogs_list-empty--header">Диалогов пока нет</div>');
                    }
                }
                ExeRu.inProgress.off();
            });

            return false;
        });

        return false;
    },
    selectDialog: function(eventElement) {
        if (typeof(eventElement) == 'object') {
            if ($(eventElement).hasClass('news-list__item--delete')) {
                return false;
            }
            var uid = eventElement.getAttribute('data-uid');
        } else {
            var uid = eventElement;
        }

        var user = ExeRu.users.getUserData(uid);
        $('#tabCurrentDialog a')
            .attr('data-uid', uid)
            .html(strip_by_length(user.name, 15) + ' ' + strip_by_length(user.last_name, 15));
        $('#tabCurrentDialog').show();
        $('#tabCurrentDialog a').click();
    },
    showDialog: function(eventElement) {
        ExeRu.inProgress.on();
        $('#dialogs-modal .modal__search--text').val('');
        $('#dialogs-modal .write-message').css('display', '');
        var params = {
            fid: eventElement.getAttribute('data-uid')
        };
        ExeRu.request.get(ExeRu.dialogs.urlGetDialog, params, function(r) {
            if (r.response == 1) {
                $('#dialogs-modal .tab_3:not(.tabControl) input[name="recipientId"]').val(r.fid);
                var users = {};
                users[r.uid] = ExeRu.users.getUserData(r.uid);
                users[r.fid] = ExeRu.users.getUserData(r.fid);

                var html = new EJS({url:'/assets/js/views/dialog.ejs?' + ExeRu.version}).render({messages: r.messages, users: users, uid: r.uid});
                $('#oneDialogMessages').html(html);

                //$('#oneDialogMessages .messages-list').mCustomScrollbar();

/*                $("#oneDialogMessages .message-delete").each(function(e){
                    $(this).tooltip({
                        template: ['<div class="tooltip tooltip-message">',
                            '<div class="tooltip-arrow"></div>',
                            '<div class="tooltip-inner">',
                            '<a href="#">Удалить</a>',
                            '</div>',
                            '</div>'].join('')
                    });
                });*/

                ExeRu.modal.init($('#oneDialogMessages'));
                $('#oneDialogMessages [data-module][data-method]').on('click', function() {
                    var module = this.getAttribute('data-module');
                    var method = this.getAttribute('data-method');
                    if (module !== null && method !== null) {
                        ExeRu[module][method](this);
                    }
                    return false;
                });

                // Функция, которая снимает непрочитанность у блоков, адресованных нам
                setTimeout(ExeRu.dialogs.markAsRead, 3000, r.fid);
                setTimeout(function() { // Переходим вниз
                    var d = document.getElementById('oneDialogMessages');
                    d.scrollTop = d.scrollHeight - d.clientHeight;
                }, 100);
            }
            ExeRu.inProgress.off();
        });
    },
    markAsRead: function(uid) {
        var li = $('#oneDialogMessages li.unread[data-uid="' + uid + '"]');
        if (li.length > 0) {
            var dids = [];
            var oldTime = (new Date).getTime() - 3000;
            for(var i = 0; i < li.length; ++i) {
                if (li[i].getAttribute('data-timestamp') < oldTime) {
                    $('p', li[i]).each(function(){
                        dids.push(this.getAttribute('data-mid'));
                    });
                    $(li[i]).removeClass('unread');
                }
            }

            if (dids.length > 0) {
                ExeRu.request.get(ExeRu.dialogs.urlMarkAsRead, {dids: dids});
            }

        }

        setTimeout(ExeRu.dialogs.markAsRead, 3000, uid);
    },
    deleteDialogBlock: function(eventElement) {
        ExeRu.inProgress.on();
        var mids = [];
        $(eventElement).parents('li').find('[data-mid]').each(function() {
            mids.push(this.getAttribute('data-mid'));
        });
        ExeRu.request.get(ExeRu.dialogs.urlDeleteBlock, {mids: mids}, function(r) {
            if (r.response == 1) {
                var parent = $('#oneDialogMessages [data-mid="' + r.mids[0] + '"]').parents('.messages-list__item__text');
                var html = '<div class="messages-list__item__delete-message messages-list__item__delete-message--lg">\
                        <p>Сообщение удалено</p><a href="#" data-mids="' + r.mids.join(',') + '" onclick="return ExeRu.dialogs.restoreDialogBlock(this);">Восстановить</a>\
                    </div>';
                parent.parent().addClass('deleted');
                parent.addClass('messages-list__item__text--delete');
                parent.append(html);
            }
            ExeRu.inProgress.off();
        });
    },
    restoreDialogBlock: function(eventElement) {
        ExeRu.inProgress.on();
        var params = {
            mids: eventElement.getAttribute('data-mids').split(',')
        };
        ExeRu.request.get(ExeRu.dialogs.urlRestoreBlock, params, function(r) {
            if (r.response == 1) {
                var parent = $('#oneDialogMessages [data-mid="' + r.mids[0] + '"]').parents('.messages-list__item__text');
                $('.messages-list__item__delete-message', parent).remove();
                parent.removeClass('messages-list__item__text--delete');
                parent.parent().removeClass('deleted');
            }
            ExeRu.inProgress.off();
        });
        return false;
    },
    writeMessageTo: function(eventElement) {
        var uid = eventElement.getAttribute('data-uid');
        var user = ExeRu.users.getUserData(uid);
        $('#dialogs-modal .tab_2 input[name="recipientId"]').val(uid);
        $('#dialogs-modal .tab_2 input[name="recipient"]').val(user.name + (user.last_name ? ' ' + user.last_name : ''));
        $('#dialogs-modal [data-tab-target="tab_2"]').attr('data-from-profile', uid);
        $('#dialogs-modal [data-tab-target="tab_2"]').click();
    },
    sendMessage: function(eventElement) {
        var textarea = $(eventElement).closest('form').find('.form-textarea').clone();

        if ($('img', textarea).length > 0) {
            $('img', textarea).each(function() {
                $(this).replaceWith('[:[' + this.getAttribute('data-smile') + ']:]');
            });
        }

        var text = textarea.text().trim();

        if (text.length == 0) {
            return false;
        }


        var params = {
            fid: $(eventElement).closest('form').find('input[name="recipientId"]').val(),
            text: text
        };

        if (params.fid < 1
            && $('#dialogs-modal .form-input-wrap--userlist:visible').length == 1
            && $('#dialogs-modal .form-input-wrap--userlist:visible').hasClass('open') == false) {

            $('#dialogs-modal input[name="recipient"]').focus();
            return false;
        }

        var recipient = ExeRu.users.getUserData(params.fid);

        if (recipient.type == 'blacklist') {
            showError("Пользователь добавлен в Ваш черный список.\n\nВы не можете отправлять ему сообщения.\n");
            $(eventElement).blur();
            return false;
        }

        if (recipient.type == 'blocked') {
            showError("Вы не можете отправить сообщение этому пользователю, поскольку он ограничивает круг лиц, которые могут присылать ему сообщения");
            $(eventElement).blur();
            return false;
        }

        if (params.text.length < 1) {
            $(eventElement).closest('form').find('.form-textarea').focus();
            return false;
        }

        if (typeof(ExeRu.dialogs._typingCache[params.fid]) != 'undefined') { // В кеше пусто, отправляем о наборе текста на сервер
            clearTimeout(ExeRu.dialogs._typingCache[params.fid]);
            delete ExeRu.dialogs._typingCache[params.fid];
        }

        var captcha = $(eventElement).closest('form').find('input[name="captcha"]').val().trim();
        if (captcha.length > 0) {
            params.captcha = captcha;
        }

        ExeRu.request.post(ExeRu.dialogs.urlSendMessage, params, function(r) {
            if (r.response == 1) {
                $(eventElement).closest('form').find('.form-textarea').html('');
                ExeRu.dialogs.viewMessage(r);
            } else if (r.response == 2) {
                if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1) { // Открыт диалог с пользователем
                    if (typeof window.dialog_widget == 'undefined') {
                        window.dialog_widget = grecaptcha.render('recaptcha_dialog', {
                            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                            'callback' : function(r) {
                                $('#recaptcha_dialog').closest('form').find('input[name="captcha"]').val(r);
                                $('#recaptcha_dialog').hide();
                                $('#recaptcha_dialog').parent().find('.form-submit').show().click();
                            }
                        });
                    } else {
                        grecaptcha.reset(window.dialog_widget);
                    }
                    $('#recaptcha_dialog').parent().find('.form-submit').hide();
                    $('#recaptcha_dialog').show();
                } else {
                    if (typeof window.message_widget == 'undefined') {
                        window.message_widget = grecaptcha.render('recaptcha_message', {
                            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                            'callback' : function(r) {
                                $('#recaptcha_message').closest('form').find('input[name="captcha"]').val(r);
                                $('#recaptcha_message').hide();
                                $('#recaptcha_message').parent().find('.form-submit').show().click();
                            }
                        });
                    } else {
                        grecaptcha.reset(window.message_widget);
                    }
                    $('#recaptcha_message').parent().find('.form-submit').hide();
                    $('#recaptcha_message').show();
                }

                return false;
            }
        });

        return false;
    },
    viewMessage: function(r){
        if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1) { // Открыт диалог с пользователем
            if (
                $('#oneDialogMessages li:last').hasClass('deleted') // Если последний блок был удаленным
                || $('#oneDialogMessages li').length == 0 // Или блоков вообще не было
                || $('#oneDialogMessages li:last').data('uid') != r.uid // Или последний блок не от нас
                || (r.message.time - $('#oneDialogMessages li:last p:last').data('time') > 300) // Прошло больше 5 минут
            ) {
                var user = ExeRu.users.getUserData(r.uid);
                var html = $(new EJS({url:'/assets/js/views/dialogOneBlock.ejs?' + ExeRu.version}).render({user: user, message: r.message, unread: true}));
                ExeRu.modal.init(html);

                /*                        $(".message-delete", html).each(function(){
                 $(this).tooltip({
                 template: ['<div class="tooltip tooltip-message">',
                 '<div class="tooltip-arrow"></div>',
                 '<div class="tooltip-inner">',
                 '</div>',
                 '</div>'].join('')
                 });
                 });*/

                $('[data-module][data-method]:not([data-modal])', html).on('click', function() {
                    var module = this.getAttribute('data-module');
                    var method = this.getAttribute('data-method');
                    if (module !== null && method !== null) {
                        ExeRu[module][method](this);
                    }
                    return false;
                });

                $('#oneDialogMessages .messages-list').append(html);
            } else {
                var html = $('<p data-mid="' + r.message.did +'" data-time="' + r.message.time + '">' + codeToImage(r.message.text) +'</p>');
                $('#oneDialogMessages .messages-list li:last p:last').after(html);
                $('#oneDialogMessages .messages-list li:last').addClass('unread');
            }
            setTimeout(function() {
                var d = document.getElementById('oneDialogMessages');
                d.scrollTop = d.scrollHeight - d.clientHeight;
                /*                        $('#oneDialogMessages .messages-list').mCustomScrollbar('scrollTo', 'bottom', {scrollInertia: 0});*/
            }, 100);
        } else { // Мы на вкладке написать сообщение
            var toProfile = $('#dialogs-modal [data-tab-target="tab_2"]').attr('data-to-profile');
            if (typeof(toProfile) == 'undefined' || toProfile != r.message.fid) {
                ExeRu.dialogs.selectDialog(r.message.fid);
            } else {
                $('#dialogs-modal [data-tab-target="tab_2"]')[0].removeAttribute('data-to-profile');
                $('#forOpenByHash').attr({'data-uid':toProfile})[0].click();
            }
        }
    },
    parseEventNewMessage: function(event) {
        if ($('#dialogs-modal').hasClass('modal--open') === false) {
            var user = ExeRu.users.getUserData(event.message.uid),
                html = $(new EJS({url:'/assets/js/views/notify_new_message.ejs?' + ExeRu.version}).render({user: user})),
                messageSound;

            if (typeof messageSound === "undefined") {
                messageSound = document.createElement('audio');
                messageSound.src = '/assets/sound/mess.mp3';
            }
            messageSound.play();

            $('[data-method], .report-close', html).click(function(e){
                e.preventDefault();
                $(".report-block").removeClass("bounceInRight");
                $(".report-block").addClass("animated bounceOutRight");
            });

            ExeRu.modal.init(html);
            $('#online-events').html(html);

        } else if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1 && $('#tabCurrentDialog a[data-uid="' + event.message.uid + '"]').length == 1){ // Открыт этот диалог
            // Прячем сообщение о наборе текста
            $('#oneDialogMessages').parent().find('.write-message--typing').removeClass('write-message--typing-on');
            // Запоминаем, находимся ли мы в области видимости последнего сообщения.
            var needScroll = $('.messages-list').height() + $('.messages-list').position().top - $('.messages-list').parent().height() < 1;
            //var needScroll = ($('.messages-list div div:first').height() - $('.messages-list').height() + $('.messages-list div div:first')[0].offsetTop) < 17;
            if (
                $('#oneDialogMessages li:last').hasClass('deleted') // Если последний блок был удаленным
                || $('#oneDialogMessages li').length == 0 // Или блоков вообще не было
                || $('#oneDialogMessages li:last').data('uid') != event.message.uid // Или последний блок не от пользователя, приславшего сообщение
                || (event.message.time - $('#oneDialogMessages li:last p:last').data('time') > 300) // Прошло больше 5 минут
            ) {
                var user = ExeRu.users.getUserData(event.message.uid);
                var html = $(new EJS({url:'/assets/js/views/dialogOneBlock.ejs?' + ExeRu.version}).render({user: user, message: event.message, unread: true}));
                ExeRu.modal.init(html);

/*                $(".message-delete", html).each(function(){
                    $(this).tooltip({
                        template: ['<div class="tooltip tooltip-message">',
                            '<div class="tooltip-arrow"></div>',
                            '<div class="tooltip-inner">',
                            '</div>',
                            '</div>'].join('')
                    });
                });*/

                $('[data-module][data-method]:not([data-modal])', html).on('click', function() {
                    var module = this.getAttribute('data-module');
                    var method = this.getAttribute('data-method');
                    if (module !== null && method !== null) {
                        ExeRu[module][method](this);
                    }
                    return false;
                });

                $('#oneDialogMessages .messages-list').append(html);
            } else {
                var html = $('<p data-mid="' + event.message.did +'" data-time="' + event.message.time + '">' + codeToImage(event.message.text) +'</p>');
                $('#oneDialogMessages .messages-list li:last p:last').after(html);
                $('#oneDialogMessages .messages-list li:last')
                    .addClass('unread')
                    .attr('data-timestamp', (new Date).getTime());
            }
            // Если последнее сообщение было видимым, скроллим вниз
            if (needScroll) {
                var d = document.getElementById('oneDialogMessages');
                d.scrollTop = d.scrollHeight - d.clientHeight;
                /*$('#oneDialogMessages .messages-list').mCustomScrollbar('scrollTo', 'bottom', {scrollInertia: 0});*/
            }

        } else if ($('#dialogs-modal .modal__content.tab_1:visible').length == 1) { // Список диалогов
            if ($('#dialogs-modal li[data-uid="' + event.message.uid + '"]').length == 0) {
                // Такого диалога пока нет, надо добавлять наверх
            } else {
                // Диалог уже есть, меняем его содержимое.
            }
            $('#dialogs-modal .modal-tabs__list .active').click()
        }
    },
    parseEventHasRead: function(event) {
        if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1 && $('#tabCurrentDialog a[data-uid="' + event.message.uid + '"]').length == 1) { // Открыт диалог с этим пользователем
            $('#oneDialogMessages li.unread[data-uid="' + event.uid + '"]').each(function() {
                var mid = parseInt($('p:last', this).attr('data-mid'));
                window.my_dids = event.message.dids;
                if (event.message.dids.indexOf(mid) != -1) {
                    $(this).removeClass('unread');
                }
            });
        }
    },
    parseEventTyping: function(event) {
        if ($('#dialogs-modal .modal__content.tab_3:visible').length == 1
            && $('#tabCurrentDialog a[data-uid="' + event.message.uid + '"]').length == 1) { // Открыт этот диалог
            if (event.message.typing == 'on') {
                $('#oneDialogMessages').parent().find('.write-message--typing').addClass('write-message--typing-on');
            } else {
                $('#oneDialogMessages').parent().find('.write-message--typing').removeClass('write-message--typing-on');
            }
        } else if ($('#dialogs-modal .modal__content.tab_1:visible').length == 1 // Открыт список диалогов
            && $('#dialogs-modal li.dialog-list__item[data-uid="' + event.message.uid + '"]').length == 1) { // и диалог с пользователем существует
            if (event.message.typing == 'on') {
                $('#dialogs-modal li.dialog-list__item[data-uid="' + event.message.uid + '"]').addClass('dialog-list__item--typing');
            } else {
                $('#dialogs-modal li.dialog-list__item[data-uid="' + event.message.uid + '"]').removeClass('dialog-list__item--typing');
            }
        }
    }
};