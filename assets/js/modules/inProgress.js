/*var ExeRu = ExeRu || {};*/
ExeRu.inProgress = {
    count: 0,
    on: function() {
        ++ExeRu.inProgress.count;
        if (ExeRu.inProgress.count != 1) {
            return false;
        }
        $('#inProgressShadow').show();
    },
    off: function() {
        --ExeRu.inProgress.count;
        if (ExeRu.inProgress.count != 0) {
            return false;
        }
        $('#inProgressShadow').hide();
    }
};