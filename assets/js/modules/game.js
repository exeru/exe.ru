/*var ExeRu = ExeRu || {};*/
ExeRu.game = {
    urlGameSettings: '/ajax/setGameSettings/',
    urlGameDelete: '/ajax/deleteGame/',
    urlNotificationGrades: '/ajax/notificationGrade/',
    urlFriendsGrades: '/ajax/friendsGrade/',
    init: function() {
        // Удаление установленных игр
/*
        $('.my-games__item .news-list__item__delete').on('click', function() {
            ExeRu.game.deleteGame(this.getAttribute('data-relation'));
        });
*/


        // Уведомления от игр
        $('.notification .news-list__item__delete').on('click', function(e) {
            e.stopPropagation();
            ExeRu.game.negativeNotificationGrade($(this).parents('.notification')[0]);

        });
        $('.notification').on('click', function() {
            ExeRu.game.positiveNotificationGrade(this);
        });


        // Уведомления от друзей
        $('.friend-request .friend-request--game_link').on('click', function(e) {
            e.preventDefault();
            ExeRu.game.positiveFriendsGrade(this);
        });
        $('.friend-request .friend-request--game_link_delete').on('click', function(e) {
            e.preventDefault();
            ExeRu.game.negativeFriendsGrade(this);
        });


        if ($('.users-list-wrap').length > 0) {
            $.get('/api/', {api_id:1, format:'json', method:'whoPlay', type:1, sid:gid}, ExeRu.game.renderWhoPlay);

            setTimeout(function(){
                $.get('/api/', {api_id:1, format:'json', method:'whoPlay', type:1, sid:gid}, ExeRu.game.updateWhoPlay);
            }, 60000);

            $('.users-list__item--more').on('click', function(e) {
                e.stopPropagation();

/*
                if ($('#users-list-wrap-queue li').length < 5) {
                    $.get('/api/', {api_id:1, format:'json', method:'whoPlay', type:3, sid:gid}, ExeRu.game.updateWhoPlay);
                }

*/
                $('#users-list-wrap').trigger('forward');

                this.blur();

                return false;
            });
        }
    },
    renderWhoPlay: function(r) {
        var root = $('.users-list-wrap .users-list');
        root.html('');
        r.response.forEach(function(user) {
            var li;
            if (uid > 0) {
                li = $('<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="user-modal" data-module="users" data-method="getUser" data-uid="' + user.uid + '"><img src="' + user.photo_50 + '" /></a></li>');
            } else {
                li = $('<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="login-modal" data-module="users" data-method="setDestination" data-dest="-1"><img src="' + user.photo_50 + '" /></a></li>');
            }
            root.append(li);
        });
        ExeRu.modal.init(root);

        $('#users-list-wrap').scrollbox({
            linear: true,
            direction: 'h',
            switchItems: 1,
            queue: 'users-list-wrap-queue'
        });
    },
    updateWhoPlay: function(r) {
        var html = '';
        r.response.forEach(function(user) {
            if ($('#users-list-wrap [data-uid=63750], #users-list-wrap-queue [data-uid=' + user.uid + ']').length == 0) {
                if (uid > 0) {
                    html += '<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="user-modal" data-module="users" data-method="getUser" data-uid="' + user.uid + '"><img src="' + user.photo_50 + '" title="' + user.uid + '"/></a></li>';
                } else {
                    html += '<li class="users-list__item"><a href="/id' + user.uid + '" data-modal="login-modal" data-module="users" data-method="setDestination" data-dest="24"><img src="' + user.photo_50 + '" /></a></li>';
                }
            }
        });
        if (html != '') {
            html = $(html);
            ExeRu.modal.init(html);

            $('#users-list-wrap-queue').append(html);
        }
    },
    positiveFriendsGrade: function(element) {
        ExeRu.inProgress.on();
        var params = {
            gid: element.getAttribute('data-gid'),
            fid: element.getAttribute('data-fid'),
            nid: element.getAttribute('data-nid'),
            request_key: element.getAttribute('data-rkey'),
            request_type: element.getAttribute('data-rtype'),
            type: 'positive'
        };
        ExeRu.game.request(ExeRu.game.urlFriendsGrades, params, function(r) {
            if (r.response == 1) {
                var location  = '/app' + r.game.id;
                window.location.href = location;
            }
        });
    },
    negativeFriendsGrade: function(element) {
        var params = {
            gid: element.getAttribute('data-gid'),
            fid: element.getAttribute('data-fid'),
            nid: element.getAttribute('data-nid'),
            type: 'negative'
        };
        ExeRu.game.request(ExeRu.game.urlFriendsGrades, params, function(r) {
            if (r.response == 1) {
                var element = $('.friend-request--game_link_delete[data-nid="' + r.game.nid + '"]').parents('.friend-request');
                // Если удаление из нижнего блока, или в нижнем блоке нет элементов, сдвигать ничего не нужно
                if ($(element).parents('.row--main--hide').length == 1 || $('.row--main--hide .friend-request--game_link_delete').length == 0) {
                    element.remove();
                } else { // Верхний блок, в нижнем есть элементы
                    var parent = $(element).parents('.friend-request-wrap');
                    element.remove();
                    parent.append($('.row--main--hide .friend-request:first'));
                }

                ExeRu.game.recountGradesBlock();
            }
        });
    },
    positiveNotificationGrade: function(element) {
        var params = {
            gid: element.getAttribute('data-gid'),
            nid: element.getAttribute('data-nid'),
            type: 'positive'
        };
        ExeRu.game.request(ExeRu.game.urlNotificationGrades, params, function(r) {
            if (r.response == 1) {
                var location  = '/app' + r.game.id;
                window.location.href = location;
            }
        });
    },
    negativeNotificationGrade: function(element) {
        var params = {
            gid: element.getAttribute('data-gid'),
            nid: element.getAttribute('data-nid'),
            type: 'negative'
        };
        ExeRu.game.request(ExeRu.game.urlNotificationGrades, params, function(r) {
            if (r.response == 1) {
                var element = $('.notification[data-nid="' + r.game.nid + '"]');
                // Если удаление из нижнего блока, или в нижнем блоке нет элементов, сдвигать ничего не нужно
                if ($(element).parents('.row--main--hide').length == 1 || $('.row--main--hide .notification').length == 0) {
                    element.remove();
                } else { // Верхний блок, в нижнем есть элементы
                    var parent = $(element).parent();
                    element.remove();
                    parent.append($('.row--main--hide .notification:first'));
                }

                ExeRu.game.recountGradesBlock();
            }
        });
    },
    recountGradesBlock: function() {
        var notificationLength = $('.notification').length;
        var friendRequestLength = $('.friend-request').length;

        if (notificationLength == 0 && friendRequestLength == 0) {
            $('.row--main:first').remove();
            $('.row--padding:first').removeClass('row--padding').addClass('row--main');
        } else {
            // Меняем количество игровых уведомлений
            if (notificationLength > 9) {
                var text = notificationLength;
            } else {
                var text = '0' + notificationLength;
            }
            $('.game__notification__count').html(text);

            // Меняем количество запросов от друзей
            if (friendRequestLength > 9) {
                var text = friendRequestLength;
            } else {
                var text = '0' + friendRequestLength;
            }
            $('.friends__notification__count').html(text);

            var sliderContainer = $('.row--main--hide');
            if (sliderContainer.length > 0) {
                if (notificationLength <= 2 && friendRequestLength <= 2) {
                    // Слайдер больше не нужен
                    $('.row--main--hide').remove();
                } else {
                    // Подтягиваем полоску слайдера, если блок развернут
                    if ($('.row__more--close:visible', sliderContainer).length == 1) {
                        var height = $('.row__more-panel', sliderContainer).height() - $('.row__more-panel .friend-request:first', sliderContainer).height() - 30;
                        $(".row__more-panel", sliderContainer).animate({
                            height: height
                        });
                    }
                }
            }
        }
    },
    showUnInstallGame: function(eventElement) {
        // Кнопка не удалять
        $('#uninstall-modal button[data-modal-close]')
            .off('click')
            .on('click', function() {
                $('#uninstall-modal div[data-modal-close]').click();
                return false;
            });

        // Кнопка удалить
        var params = { appId: $(eventElement).attr('data-gid') },
            type = $(eventElement).attr('data-type');

        $('#unInstall').off('click').on('click', function() {
            ExeRu.inProgress.on();

            if (type == 'main') {
                ExeRu.game.request(ExeRu.game.urlGameDelete, params, function(r) {
                    if (r.response == 1) {
                        var length = $('.my-games--user li').length;
                        var parent = $('[data-gid="' + r.appId + '"]').parent();
                        if ($('.my-games--user li').length > 1) {
                            length--;
                            if (length > 9) {
                                var text = length;
                            } else {
                                var text = '0' + length;
                            }
                            $(parent).parents('.row').find('.col__header__count').html(text); // Изменили количество
                            parent.remove(); // Удалили сам элемент
                        } else {
                            $(parent).parents('.row').remove();
                        }
                    }
                    $('#uninstall-modal div[data-modal-close]').click();
                    ExeRu.inProgress.off();
                });
            } else {
                ExeRu.game.request(ExeRu.game.urlGameDelete, params, function(r) {
                    if (r.response == 1) {
                        window.location.href = '/';
                    } else {
                        $('#uninstall-modal div[data-modal-close]').click();
                        ExeRu.inProgress.off();
                    }
                });
            }

            return false;
        });

        return false;
    },
    setHeight: function(request) {
        var height = parseInt(request.data);
        if (typeof request.data === "number" && height === request.data && height > 10 && $('.game iframe').length == 1) {
            $('.game iframe').attr('height', height);
            window.frames[0].postMessage({type:'apiResponse', data:{height:height}, callback:'setHeight'}, '*');
            return true;
        }

        window.frames[0].postMessage({type:'apiResponse', data:{response:false}, callback:'setHeight'}, '*');
        return false;
    },
    notification: {
        on: function() {
            var params = {
                gid: gid,
                type: 'notifications',
                set: 'on'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        },
        off: function() {
            var params = {
                gid: gid,
                type: 'notifications',
                set: 'off'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        }
    },
    invitible: {
        on: function() {
            var params = {
                gid: gid,
                type: 'invitible',
                set: 'on'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        },
        off: function() {
            var params = {
                gid: gid,
                type: 'invitible',
                set: 'off'
            };
            ExeRu.game.request(ExeRu.game.urlGameSettings, params);
        }
    },
    request: function(url, params, callback) { // TODO Вырезать этот аппендикс.
        ExeRu.request.get(url, params, callback);
    }
};