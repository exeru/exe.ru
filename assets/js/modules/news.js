/*var ExeRu = ExeRu || {};*/
ExeRu.news = {
    urlNewsDelete: '/ajax/deleteFeed/',
    urlNewsRestore: '/ajax/restoreFeed/',
    urlPublishEvent: '/ajax/publishFeed/',
    urlNewsGet: '/ajax/getFeeds/',
    init: function() {
        // Фильтр
        $('#news-modal .checkbox-list input').on('change', function() {
            if (this.checked) {
                $('#news-modal .modal__content').find('ul, div.type-all, div.type-game, div.type-user').hide();
                $('#news-modal .modal__content').find('ul.' + this.value + ', div.' + this.value).show();
            }
        });
        // Загрузка новостей при открытие окна

        $('#news-modal .checkbox-list input').on('click', function() {
            ExeRu.news.getNews(this);
        });

    },
    restoreNews: function(element) {
        var params = {
            fid: element.getAttribute('data-fid')
        };
        ExeRu.request.get(ExeRu.news.urlNewsRestore, params, function(r) {
            if (r.response == 1) {
                var message = $('.news-list__item__delete-message a[data-fid="' + r.fid + '"]').parent();
                message.parent().removeClass('news-list__item--delete');
                message.remove();
            }
        });
    },
    deleteNews: function(element) {
        var params = {
            fid: element.getAttribute('data-fid')
        };
        ExeRu.request.get(ExeRu.news.urlNewsDelete, params, function(r) {
            if (r.response == 1) {
                var html = '<div class="news-list__item__delete-message">';
                html += '<p>Запись удалена</p><a href="#" data-fid="' + r.fid + '">Восстановить</a>';
                html += '</div>';

                var parent = $('#news-modal [data-fid="' + r.fid + '"]').parent();
                parent.addClass('news-list__item--delete');
                parent.prepend(html);
                $('.news-list__item__delete-message a', parent).on('click', function(e) {
                    e.preventDefault();
                    ExeRu.news.restoreNews(this);
                });
            }
        });
    },
    showPublishBox: function(request) {
        if (uid === 0) {
            $('[data-modal="login-modal"]').click();
            return;
        }
        var root = $('#add-feed-modal');
        root.find(".news-list__item__author p").html(request.data.text);
        root.find(".news-list__item__banner img").attr('src', request.data.image);

        root.off('closeDialog');
        root.find('.btn-main').off('click');

        root.on('closeDialog', {type:'showPublishBox', data:{response:false}}, ExeRu.news.apiReturnData);
        root.find('.btn-main').on('click', {data: request.data}, ExeRu.news.publish);

        ExeRu.modal.dialogs['add-feed-modal'].options.event = '';
        ExeRu.modal.dialogs['add-feed-modal'].toggle();
    },
    publish: function(event) {
        ExeRu.inProgress.on();

        var root = $('#add-feed-modal');
        root.off('closeDialog');

        // Отправляем запрос на сервер
        var params = {
            sid: gid,
            data: event.data.data
        };
        $.get(ExeRu.news.urlPublishEvent, params, function(r) {
            var root = $('#add-feed-modal');
            root.on('closeDialog', {type:'showPublishBox', data:{response:true}}, ExeRu.news.apiReturnData);

            ExeRu.inProgress.off();
            root.find('[data-modal-close]').click();
        });

        return false;
    },
    apiReturnData: function(e) {
        $('#invite-friends-modal').off('closeDialog', ExeRu.friends.apiReturnData);
        window.frames[0].postMessage({type:e.data.type, data:e.data.data}, '*');
    },
    resetFilter: function() {
        $('#news-modal .checkbox-list input:first').trigger('click');
    },
    getNews: function(eventElement) {
        ExeRu.inProgress.on();

        var filter = eventElement.hasAttribute('data-type') ? eventElement.getAttribute('data-type') : eventElement.value;

        if (eventElement.hasAttribute('data-prev')) {
            var data = {
                next: $('#news-modal .modal__content ul.' + filter + ' li:last div:first').attr('data-fid'),
                filter: filter
            }
        } else {
            var data = {
                prev: $('#news-modal .modal__content ul.' + filter + ' li:eq(1) div:first').attr('data-fid') || 0,
                filter: filter
            }
        }

        ExeRu.request.get(ExeRu.news.urlNewsGet, data, function(r) {
            if (r.length > 0) {
                var root = $('#news-modal'),
                    parent;

                if ($('ul.' + filter, root).length > 0) {
                    parent = $('ul.' + filter, root);
                } else {
                    $('.modal__content div.' + filter, root).removeClass(filter).hide();

                    parent = $(new EJS({url:'/assets/js/views/newsNoEmpty.ejs?' + ExeRu.version}).render({type:filter}));
                    $('.modal__content', root).append(parent);
                }

                if (eventElement.hasAttribute('data-prev')) {
                    $('.dev-billing__history-more', parent).remove();
                } else {
                    r = r.reverse();
                }

                for(var i in r) {
                    var template = '', element;
                    if (typeof r[i].user == 'undefined') { // Новость игры (newsGame)
                        template = 'newsGame';
                    } else { // Блок новости пользователя
                        if (typeof r[i].game != 'undefined') {
                            if (r[i].type == 3) { // Достижение в игре (newsAchievement)
                                template = 'newsAchievement';
                            } else { // Установка игры (newsInstallGame)
                                template = 'newsInstallGame';
                            }
                        } else { // Добавление в друзья (newsAppendFriends)
                            template = 'newsAppendFriend';
                        }
                    }

                    element = $(new EJS({url:'/assets/js/views/' + template + '.ejs?' + ExeRu.version}).render({feed: r[i]}));

                    if (eventElement.hasAttribute('data-prev')) {
                        parent.append(element);
                    } else {
                        $('li:eq(0)', parent).after(element);
                    }

                    ExeRu.modal.init(element);

                    $('.news-list__item__delete', element).on('click', function() {
                        ExeRu.news.deleteNews(this);
                    });
                }

                if (r.length == 10) {

                    element = $(new EJS({url:'/assets/js/views/newsButtonMore.ejs?' + ExeRu.version}).render({type:filter}));
                    parent.append(element);

                    $('a', element).on('click', function() {
                        ExeRu.news.getNews(this);
                        return false;
                    });
                }
            } else {
                if (eventElement.hasAttribute('data-prev')) {
                    $('#news-modal ul.' + filter + ' .dev-billing__history-more').remove();
                }
            }

            ExeRu.panel.updateCount('news', 0);

            ExeRu.inProgress.off();
        });
    }
};