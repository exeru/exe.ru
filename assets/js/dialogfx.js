;( function( window ) {

    'use strict';

    var support = { animations : Modernizr.cssanimations },
        animEndEventNames = { 'WebkitAnimation' : 'webkitAnimationEnd', 'OAnimation' : 'oAnimationEnd', 'msAnimation' : 'MSAnimationEnd', 'animation' : 'animationend' },
        animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
        onEndAnimation = function( el, callback ) {
            var onEndCallbackFn = function( ev ) {
                if( support.animations ) {
                    //if( ev.target != this ) return;
                    this.removeEventListener( animEndEventName, onEndCallbackFn );
                }
                if( callback && typeof callback === 'function' ) { callback.call(); }
            };
            if( support.animations ) {
                el.addEventListener( animEndEventName, onEndCallbackFn );
            }
            else {
                onEndCallbackFn();
            }
        };

    function extend( a, b ) {
        for( var key in b ) {
            if( b.hasOwnProperty( key ) ) {
                a[key] = b[key];
            }
        }
        return a;
    }

    function DialogFx( el, options ) {
        this.el = el;
        this.options = extend( {}, this.options );
        extend( this.options, options );
        this.ctrlClose = this.el.querySelector( '[data-modal-close]' );
        this.isOpen = false;
        this._initEvents();
    }

    DialogFx.prototype.options = {
        // callbacks
        onOpenDialog : function() { return false; },
        onCloseDialog : function() { return false; }
    }

    DialogFx.prototype._initEvents = function() {
        var self = this;

        // close action
        this.ctrlClose.addEventListener( 'click', this.toggle.bind(this) );

        // esc key closes dialog
        document.addEventListener( 'keydown', function( ev ) {
            var keyCode = ev.keyCode || ev.which;
            if( keyCode === 27 && self.isOpen ) {
                self.toggle();
            }
        } );

        this.el.querySelector( '.overlay' ).addEventListener( 'click', this.toggle.bind(this) );
    }

    DialogFx.prototype.toggle = function() {
        var self = this;

        if (this.isOpen) {
            classie.remove( this.el, 'modal--open' );
            classie.add( self.el, 'modal--close' );
            classie.remove( $("[data-modal='" + self.el.id + "']")[0], 'active');

            onEndAnimation( this.el.querySelector( '.modal' ), function() {
                classie.remove( self.el, 'modal--close' );
                $(self.el).trigger('DialogFX_endAnimationClose');
            } );

            // callback on close
            this.options.onCloseDialog( this );
        }
        else {

            classie.add( $("[data-modal='" + this.el.id + "']")[0], 'active');

            classie.add( this.el, 'modal--open' );

            // callback on open
            this.options.onOpenDialog( this );

            onEndAnimation( this.el.querySelector( '.modal' ), function() {
                $(self.el).trigger('DialogFX_endAnimationOpen');
            } );
        }
        this.isOpen = !this.isOpen;
    };

    // add to global namespace
    window.DialogFx = DialogFx;

})( window );