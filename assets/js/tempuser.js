jQuery(function() {
    // Дополнительные методы валидации
    $.validator.addMethod(
        "minLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length < param) {
                return false;
            } else {
                return true;
            }
        }, "Не меньше 2-х символов");
    $.validator.addMethod(
        "maxLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length > param) {
                return false;
            } else {
                return true;
            }
        }, "Не больше 16 символов");
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            return this.optional(element) || regexp.test(value);
        }, "Разрешены только буквы и пробел");
    // Правила валидации для авторизации
    $("#login-modal .tab-login form").validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            },
            password: {
                required: "Введите пароль"
            }
        }
    });
    // Правила валидации для регистрации
    $("#login-modal .tab-register form").validate({
        rules: {
            name: {
                required: true,
                minLen: 2,
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            email: {
                email: true,
                required: true
            },
            password: {
                required: true,
                minlength: 6
            },
            password_repeat: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            offer_agree: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Введите Ваше имя",
                minlen: "Не меньше 2-х символов",
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            },
            password: {
                required: "Введите пароль",
                minlength: "Пароль слишком короткий (меньше 6 символов)"
            },
            password_repeat: {
                required: "Введите пароль повторно",
                minlength: "Пароль слишком короткий (меньше 6 символов)",
                equalTo: "Пароли не совпадают"
            },
            offer_agree: {
                required: "Необходимо принять правила использования"
            }
        }
    });
    // Правила валидации для восстановления пароля
    $("#login-modal .tab-restore form").validate({
        rules: {
            email: {
                email: true,
                required: true
            }
        },
        messages: {
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            }
        }
    });

    // Правила валидации для формы с левой стороны
    $(".guest-box .tab-register form").validate({
        rules: {
            name: {
                required: true,
                minLen: 2,
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            email: {
                email: true,
                required: true
            },
            password: {
                required: true,
                minlength: 6
            }        },
        messages: {
            name: {
                required: "Введите Ваше имя",
                minlen: "Не меньше 2-х символов",
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            },
            password: {
                required: "Введите пароль",
                minlength: "Пароль слишком короткий (меньше 6 символов)"
            }
        }
    });

    // Обработчики для авторизации, регистрации и восстановления пароля
    $("#login-modal .tab-login form, #login-modal .tab-restore form, #login-modal .tab-register form, .guest-box .tab-register form").on("submit", sendDataForm);

    // Запоминаем цель при авторизации через соцсети
    $(".sign-social-list a, a.social__icon, .connect-popup--socials a").on('click', function(e) {
        e.stopPropagation();

        var url = this.getAttribute('href') + '/popup';
        if (this.hasAttribute('data-href')) {
            url += '?target_url=' + encodeURI(this.getAttribute('data-href'));
        }

        if (this.hasAttribute('data-dest')) {
            if (url.indexOf('?') === -1) {
                url += '?';
            } else {
                url += '&';
            }
            url += 'destId=' + this.getAttribute('data-dest');
        }

        if (window.init) {
            var parent = $(this).parents('.sign-social-wrap');
            if (parent.hasClass('tab-register')) {
                var playBefore = parent.parent().find('input[name="play_before"]').val();
                if (playBefore > 0) {
                    if (url.indexOf('?') === -1) {
                        url += '?';
                    } else {
                        url += '&';
                    }
                    url += 'play_before=' + window.playBefore;
                }
            }
        }

        var params = "width=700,height=450,menubar=no,location=no,resizable=no,scrollbars=no,status=no";
        window.open(url, "", params);

        return false;
    });

    if (window.init) {
        $(document).bind('mouseleave', function(e) {
            if (window.init && window.playBefore == 0 && e.clientY <= -1) {
                var modals = window.retentionModals,
                    activeModal = modals[Math.floor(Math.random() * modals.length)];

                window.playBefore = Math.floor((new Date() - initTime) / 1000 / 60);

                if (window.playBefore < 1) {
                    window.playBefore = 1;
                }

                $('#modalExit .m2-title span').html(window.playBefore + ' ' + plural_str(window.playBefore, 'минуту', 'минуты', 'минут'));
                $('#login-modal input[name="play_before"]').val(window.playBefore);

                $('[data-modal="' + activeModal + '"]').click();

                if (typeof yaCounter41227294 != 'undefined') {
                    switch(true) {
                        case window.playBefore == 1 && activeModal == 'modalExit': { yaCounter41227294.reachGoal('time_init'); break;}
                        case window.playBefore > 1  && window.playBefore <= 10 && activeModal == 'modalExit': { yaCounter41227294.reachGoal('time_good'); break;}
                        case window.playBefore > 10 && activeModal == 'modalExit': { yaCounter41227294.reachGoal('time_supergood'); break;}

                        case window.playBefore == 1 && activeModal == 'modalBookmarkN': { yaCounter41227294.reachGoal('zakladki_init'); break;}
                        case window.playBefore > 1  && window.playBefore <= 10 && activeModal == 'modalBookmarkN': { yaCounter41227294.reachGoal('zakladi_good'); break;}
                        case window.playBefore > 10 && activeModal == 'modalBookmarkN': { yaCounter41227294.reachGoal('zakladki_supergood'); break;}

                        case window.playBefore == 1 && activeModal == 'modalSignupN': { yaCounter41227294.reachGoal('registration_init'); break;}
                        case window.playBefore > 1  && window.playBefore <= 10 && activeModal == 'modalSignupN': { yaCounter41227294.reachGoal('registration_good'); break;}
                        case window.playBefore > 10 && activeModal == 'modalSignupN': { yaCounter41227294.reachGoal('registration_supergood'); break;}
                    }
                }
            }
        });
    }
});

function cancelLeave(a) {
    if (window.init) {
        $(a).parents('.m2-content').find('[data-modal-close]')[0].click();
        if (typeof yaCounter41227294 != 'undefined') {
            switch(true) {
                case window.playBefore == 1 && activeModal == 'modalExit': { yaCounter41227294.reachGoal('time_success_init'); break;}
                case window.playBefore > 1  && window.playBefore <= 10 && activeModal == 'modalExit': { yaCounter41227294.reachGoal('time_success_good'); break;}
                case window.playBefore > 10 && activeModal == 'modalExit': { yaCounter41227294.reachGoal('time_success_supergood'); break;}
            }
        }
    }

    return false;
}

function add_favorite(a) {
    if (window.init) {
        var title = document.title,
            url = document.location;

        try { // Internet Explorer
            window.external.AddFavorite(url, title);
        } catch(e) {
            try { // Mozilla
                window.sidebar.addPanel(title, url, "");
            } catch(e) { // Opera
                if (typeof(opera) == "object") {
                    a.rel = "sidebar";
                    a.title = title;
                    a.url = url;
                    $(a).parent('.modal').find('[data-modal-close]').click();

                    return true;
                } else { // Unknown
                    alert('Нажмите Ctrl-D чтобы добавить страницу в закладки');
                }
            }
        }

        $(a).parents('.modal').find('[data-modal-close]')[0].click();

        if (typeof yaCounter41227294 != 'undefined') {
            switch(true) {
                case window.playBefore == 1: { yaCounter41227294.reachGoal('zakladki_add_init'); break;}
                case window.playBefore > 1  && playBefore <= 10: { yaCounter41227294.reachGoal('zakladi_add_good'); break;}
                case window.playBefore > 10: { yaCounter41227294.reachGoal('zakladki_add_supergood'); break;}
            }
        }
    }

    return false;
}


function getTabName(el) {
    var classList = el.classList;

    for (i=classList.length - 1; i>=0; i--) {
        if ($.inArray(classList[i], ["btn--site-header", "btn-lg--sign-in", "catalog-btn", "tab-login"]) !== -1) {
            // Найден класс перехода на логин
            return "tab-login";
        } else if ($.inArray(classList[i], ["btn-lg--register", "tab-register", "guest-box--submit-button"]) !== -1) {
            // Найден класс перехода на регистрацию
            return "tab-register";
        } else if ($.inArray(classList[i], ["tab-restore"]) !== -1) {
            // Найден класс восстановления пароля
            return "tab-restore";
        }
    }

    return "";
}

function sendDataForm() {
    if ($('input.error:visible', this).length > 0) {
        return false;
    }

    $('button:submit').blur();

    var tabName = getTabName($('button', this)[0]);
    if (tabName === "") {
        return false;
    }

    switch(tabName) {
        case "tab-login": {
            action = "/ajax/login";
            break;
        }
        case "tab-register": {
            action = "/ajax/register";
            break;
        }
        case "tab-restore": {
            action = "/ajax/restore";
            break;
        }
    }

    $.post(action, $(this).serialize(), function(r){
        if (r.errors !== undefined) {
            $("input[name='" + r.csrf.name + "']").val(r.csrf.value);
            alert(r.errors.summary);
            if (typeof r.errors.type !== undefined) {
                if (r.errors.type == 'sent') {
                    $('#login-modal [data-modal-close]').click();
                } else if (r.errors.type == 'reg') {
                    grecaptcha.reset();
                    $('#recaptcha').hide();

                    var form = $('#login-modal .tab-register form');
                    $('button:submit', form).show();
                    $('input', form).removeAttr('readonly');
                    form[0].reset();
                }
            }

            if (typeof r.errors.type !== undefined && r.errors.type == 'sent') {
                $('#login-modal [data-modal-close]').click();
            }
        } else if (r.location !== undefined) {
            if (tabName == 'tab-register' && window.init && typeof yaCounter41227294 != 'undefined') {
                switch(true) {
                    case window.playBefore == 1: { yaCounter41227294.reachGoal('registration_success_init'); break;}
                    case window.playBefore > 1  && playBefore <= 10: { yaCounter41227294.reachGoal('registration_success_good'); break;}
                    case window.playBefore > 10: { yaCounter41227294.reachGoal('registration_success_supergood'); break;}
                }
                yaCounter41227294.reachGoal('registration_success_form');
            }
            location.href = r.location;
        }
    });

    return false;
}