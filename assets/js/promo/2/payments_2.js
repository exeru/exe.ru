/**
 * Created by ruslan on 07.06.17.
 */
$(document).ready(function(){
    ExeRu.payments.prepareForm = function(eventElement) {
        var paymentType = eventElement.getAttribute('data-payment-type'),
            rate = eventElement.getAttribute('data-rate'),
            root = $('#payment-uniform-modal'),
            html, phone;

        if (paymentType == 'QW' || paymentType == 'MC') {
            var phone = $(eventElement).parents('.modal').find('input[name="phone"]:first').val().trim();
            $('[name="phone"]', root).val(phone);
        } else {
            $('[name="phone"]', root).val('');
        }

        $('[name="paymentType"]', root).val(paymentType);
        $('[name="rate"]', root).val(rate);

        $('.pay-application').css({display:'none'});

        switch(paymentType) {
            case 'AC': { // Банковская карта
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата банковской картой</span></p>\
                        <ul class="card-type-list">\
                            <li><img src="/assets/img/mastercard-icon.png"></li>\
                            <li><img src="/assets/img/visa-icon.png"></li>\
                        </ul>\
                    </div>';
                break;
            }
            case 'MC': { // Мобильный телефон
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата с баланса мобильного телефона</span></p>\
                        </div>';

                if (phone.length >= 11) {
                    html += '<div class="payment-phone">\
                                <p>Оплата с номера <span>' + phone + '</span></p>\
                                <a href="#" data-modal="payment-phone-modal">Изменить</a>\
                            </div>';
                }

                html += '</div>';

                html = $(html);
                ExeRu.modal.init(html);
                break;
            }
            case 'QW': { // Системы онлайн оплаты, QIWI
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img style="top: 131px;" class="webpayment-img" src="/assets/img/payment-system1.png">\
                    </div>';
                $('.pay-application').css({display:'block'});
                break;
            }
            case 'PC': { // Системы оплаты, Яндекс.Деньги
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img style="top: 131px;" class="webpayment-img" src="/assets/img/payment-system2.png">\
                    </div>';
                break;
            }
            case 'WM': { // Системы оплаты, Webmoney
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span></p>\
                        </div>\
                        <img style="top: 131px;" class="webpayment-img" src="/assets/img/payment-system4.png">\
                    </div>';
                break;
            }
            case 'GP': { // Оплата через терминалы и салоны
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата через терминалы</span></p>\
                    </div>';
                break;
            }
            case 'SBERBANK': { // Оплата через интернет-банкинг, Сбербанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/sber.png" style="top: 131px; margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'ALFABANK': { // Оплата через интернет-банкинг, Альфа-банк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/alfa.png" style="top: 131px; margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'VTB24': { // Оплата через интернет-банкинг, ВТБ24
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/vtb.png" style="top: 131px; margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'BRS': { // Оплата через интернет-банкинг, Русский Стандарт
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/brs.png" style="top: 131px; margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'PSB': { // Оплата через интернет-банкинг, Промсвязьбанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/psb.png" style="top: 131px; margin-right: 40px;">\
                    </div>';
                break;
            }

        }

        $('.modal__content div:first', root).replaceWith($(html));

        // Расчитываем сумму для указанного количества рублей
        $('input[name="radiogroup"]', root).each(function(){
            this.checked = false;

            var parent = $(this).parent();
            var value = this.value;

            if (value == 'manual') {
                parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, 0));
                parent.find('input[name="count"]').val('');
            } else {
                value = parseInt(value);
                if ( !isNaN(value) && value > 0) {
                    parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, value));
                }
            }
        });

        // Снимаем и ставим заново обработчик на изменение количества в поле ручного ввода количества рублей
        $('input[name="count"]', root)
            .off('keyup change')
            .on('keyup change', function(){
                var parent = $(this).parent();
                var value = parseInt(this.value);
                var bonus = parseInt(value * 0.3);
                if (value == 0 || isNaN(value) || bonus == 0) {
                    parent.find('p').html('Другое количество');
                } else {
                    parent.find('p').html('Другое количество ('+ value + plural_str(value, ' рубль', ' рубля', ' рублей') + ' + '+ bonus + plural_str(bonus, ' рубль', ' рубля', ' рублей') + ' бонус)');
                }
                parent.find('span').html(ExeRu.payments.getPriceString(rate, value));
            });

        // Проверяем, есть ли у нас предустановленое значение другого количества
        var preSetCount = parseInt($(root).attr('data-preset-count'));

        if (isNaN(preSetCount) == false && preSetCount > 0) {
            $('#radio5')[0].checked = true;
            $('input[name="count"]', root).val(preSetCount);
            $('input[name="count"]', root).trigger('keyup');
        } else {
            $('input[name="count"]', root).trigger('keyup');
        }
    };
});