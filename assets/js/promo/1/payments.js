/**
 * Created by ruslan on 07.06.17.
 */
$(document).ready(function(){
    ExeRu.payments.prepareForm = function(eventElement) {
        var paymentType = eventElement.getAttribute('data-payment-type'),
            rate = eventElement.getAttribute('data-rate'),
            root = $('#payment-uniform-modal'),
            html, phone;

        if (paymentType == 'QW') {
            var phone = $(eventElement).parents('.modal').find('input[name="phone"]:first').val().trim();
            $('[name="phone"]', root).val(phone);
        } else {
            $('[name="phone"]', root).val('');
        }

        $('[name="paymentType"]', root).val(paymentType);
        $('[name="rate"]', root).val(rate);



        switch(paymentType) {
            case 'AC': { // Банковская карта
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата банковской картой</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        <ul class="card-type-list">\
                            <li><img src="/assets/img/mastercard-icon.png"></li>\
                            <li><img src="/assets/img/visa-icon.png"></li>\
                        </ul>\
                    </div>';
                break;
            }
            case 'MC': { // Мобильный телефон
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата с баланса мобильного телефона</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <div class="payment-phone">\
                            <p>Оплата с номера <span>+7 *** *** ** 78</span></p>\
                            <a href="#" data-modal="payment-phone-modal">Изменить</a>\
                        </div>\
                    </div>';

                html = $(html);
                ExeRu.modal.init(html);
                break;
            }
            case 'QW': { // Системы онлайн оплаты, QIWI
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system1.png">\
                    </div>';
                break;
            }
            case 'PC': { // Системы оплаты, Яндекс.Деньги
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system2.png">\
                    </div>';
                break;
            }
            case 'WM': { // Системы оплаты, Webmoney
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через платежную систему</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/payment-system4.png">\
                    </div>';
                break;
            }
            case 'GP': { // Оплата через терминалы и салоны
                html = '\
                    <div class="modal__content__header modal__content__header--card">\
                        <p><span>Оплата через терминалы</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                    </div>';
                break;
            }
            case 'SBERBANK': { // Оплата через интернет-банкинг, Сбербанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/sber.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'ALFABANK': { // Оплата через интернет-банкинг, Альфа-банк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/alfa.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'VTB24': { // Оплата через интернет-банкинг, ВТБ24
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/vtb.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'BRS': { // Оплата через интернет-банкинг, Русский Стандарт
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/brs.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }
            case 'PSB': { // Оплата через интернет-банкинг, Промсвязьбанк
                html = '\
                    <div>\
                        <div class="modal__content__header modal__content__header--card">\
                            <p><span>Оплата через интернет-банкинг</span><span style="color: #6752b2;"><b> +30% </b></span><span style="font-size:0.75em;color: #6752b2;">бонус к платежу (до 12.06.2017 23:59)</span></p>\
                        </div>\
                        <img class="webpayment-img" src="/assets/img/psb.png" style="margin-right: 40px;">\
                    </div>';
                break;
            }

        }

        $('.modal__content div:first', root).replaceWith($(html));

        // Расчитываем сумму для указанного количества рублей
        $('input[name="radiogroup"]', root).each(function(){
            this.checked = false;

            var parent = $(this).parent();
            var value = this.value;

            if (value == 'manual') {
                parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, 0));
                parent.find('input[name="count"]').val('');
            } else {
                value = parseInt(value);
                if ( !isNaN(value) && value > 0) {
                    parent.find('.radio-btn-content span').html(ExeRu.payments.getPriceString(rate, value) + ' + '  + ExeRu.payments.getPriceString(rate, parseInt(value * 0.3)) + ' бонус');
                }
            }
        });

        // Снимаем и ставим заново обработчик на изменение количества в поле ручного ввода количества рублей
        $('input[name="count"]', root)
            .off('keyup change')
            .on('keyup change', function(){
                var parent = $(this).parent(),
                    value = parseInt(this.value),
                    bonus = ExeRu.payments.getPriceString(rate, parseInt(value * 0.3));

                if (bonus.length > 0 && bonus !== '&nbsp;') {
                    bonus = ' + ' + bonus + ' бонус';
                }

                parent.find('span').html(ExeRu.payments.getPriceString(rate, value) + bonus);
            });

        // Проверяем, есть ли у нас предустановленое значение другого количества
        var preSetCount = parseInt($(root).attr('data-preset-count'));

        if (isNaN(preSetCount) == false && preSetCount > 0) {
            $('#radio5')[0].checked = true;
            $('input[name="count"]', root).val(preSetCount);
            $('input[name="count"]', root).trigger('keyup');
        }
    };


    $('.may-banner--close, .may-banner--overlay, .may-banner--link').on({
        click: function (e) {
            e.preventDefault();

            $('.may-banner').fadeOut(300);
        }
    });

    $('.may-banner').show();
});