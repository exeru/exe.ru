/**
 * Created by ruslan on 12.08.15.
 */
jQuery(function() {
    var onWindowResizeLoad = function () {
        var _f = $('.dev-footer');

        if (_f.length) {
            var _m = $('.minHeight');
            _m.css('min-height', 'auto');

            var contentHeight = _f.offset().top + _f.outerHeight(),
                windowHeight  = $(window).height();

            if (windowHeight > contentHeight)
                _m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
        }
    };

    $(window).on('resize load', onWindowResizeLoad);

    // Открываем всплывайку
    $('.social__icon').on('click', function(){
        var params = "width=700,height=450,menubar=no,location=no,resizable=no,scrollbars=no,status=no",
            url = this.getAttribute('href') + '/popup';

        if (this.hasAttribute('data-dest')) {
            url += '?destId=' + this.getAttribute('data-dest');
        }

        window.open(url, "", params);

        return false;
    });

    // Показать форму авторизации/регистрации/восстановления пароля
    $("a.btn--site-header, a.btn-lg--sign-in, a.btn-lg--register, a.tab-login, a.tab-register, a.tab-restore").on("click", showLoginRegistrateRestoreForm);
    $(".connect-popup--btn.tab-login, .connect-popup--btn.tab-register").on("click", function(e) {
        $('#connect_1-modal').removeClass('modal--open');
        showLoginRegistrateRestoreForm(e);
        return false;
    });
    $("li.catalog-list__item, button.catalog-btn").not('.soon').on('click', function(e) {
        window.location.href = e.currentTarget.getAttribute('data-href');
        //var parent = $(e.currentTarget);
        //console.debug(e.currentTarget.getAttribute('data-href'));

/*            modal = $('#connect_1-modal');

        modal.find('.connect-popup--poster')
            .css({'border-bottom-color': parent.find('h4').css('color')});

        modal.find('.connect-popup--poster img')
            .attr({'src': parent.find('img').attr('src')});

        modal.find('.connect-popup--name')
            .css({'color': parent.find('h4').css('color')})
            .html(parent.find('h4').html());

        modal.find('.connect-popup--category')
            .html(parent.find('span').html());

        if (e.currentTarget.hasAttribute('data-href')) {
            modal.find('a.connect-popup--btn, .connect-popup--socials a, a.connect-popup--big').each(function(){
                this.setAttribute('data-href', e.currentTarget.getAttribute('data-href'));
            });
        } else {
            this.setAttribute('data-href', '');
        }

        if (e.currentTarget.hasAttribute('data-dest')) {
            modal.find('.connect-popup--socials a, a.connect-popup--big').each(function(){
                this.setAttribute('data-dest', e.currentTarget.getAttribute('data-dest'));
            });
        } else {
            this.setAttribute('data-dest', '');
        }

        $('[data-modal="connect_1-modal"]').click();*/
    });

    $("div.modal__close").on("click", closeLoginRegistrateRestoreForm);
    // Запоминаем цель при авторизации через соцсети
    $(".sign-social-list a, .connect-popup--socials a, a.connect-popup--big").on('click', function() {
        var url = this.getAttribute('href') + '/popup';
        if (this.hasAttribute('data-href')) {
            url += '?target_url=' + encodeURI(this.getAttribute('data-href'));
        }

        if (this.hasAttribute('data-dest')) {
            if (url.indexOf('?') === -1) {
                url += '?';
            } else {
                url += '&';
            }
            url += 'destId=' + this.getAttribute('data-dest');
        }

        var params = "width=700,height=450,menubar=no,location=no,resizable=no,scrollbars=no,status=no";
        window.open(url, "", params);

        return false;
    });

    $(".connect-popup--poster").on('click', function(){
        $("a.connect-popup--btn.alt:visible").trigger('click');
    });

    $("a.connect-popup--btn.alt").on('click', function(){
        return false;
    });

    // Правила валидации для авторизации
    $(".tab-login form").validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            },
            password: {
                required: "Введите пароль"
            }
        }
    });
    // Дополнительные методы валидации
    $.validator.addMethod(
        "minLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length < param) {
                return false;
            } else {
                return true;
            }
        }, "Не меньше 2-х символов");
    $.validator.addMethod(
        "maxLen",
        function (value, element, param) {
            var value = this.elementValue(element).trim();
            if (value.length > param) {
                return false;
            } else {
                return true;
            }
        }, "Не больше 16 символов");
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            return this.optional(element) || regexp.test(value);
        }, "Разрешены только буквы и пробел");
    // Правила валидации для регистрации
    $(".tab-register form").validate({
        rules: {
            name: {
                required: true,
                minLen: 2,
                maxLen: 16,
                regex: /^[0-9а-яА-Яa-zA-ZёЁ\- ]+$/
            },
            email: {
                email: true,
                required: true
            },
            password: {
                required: true,
                minlength: 6
            },
            password_repeat: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            offer_agree: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Введите Ваше имя",
                minlen: "Не меньше 2-х символов",
                maxLen: "Не больше 16 символов",
                regex: "Разрешены только буквы и пробел"
            },
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            },
            password: {
                required: "Введите пароль",
                minlength: "Пароль слишком короткий (меньше 6 символов)"
            },
            password_repeat: {
                required: "Введите пароль повторно",
                minlength: "Пароль слишком короткий (меньше 6 символов)",
                equalTo: "Пароли не совпадают"
            },
            offer_agree: {
                required: "Необходимо принять правила использования"
            }
        }
    });
    // Правила валидации для восстановления пароля
    $(".tab-restore form").validate({
        rules: {
            email: {
                email: true,
                required: true
            }
        },
        messages: {
            email: {
                email: "Введите корректный email",
                required: "Введите email-адрес"
            }
        }
    });

    // Обработчики для авторизации и восстановления пароля
    $("#login .tab-login form, #login .tab-restore form").on("submit", sendDataForm);
    // Обработчик регистрации
    $("#login .tab-register form").on('submit', function() { return false; });

    // Pixel
    if (window.location.search.indexOf('from=lf') !== -1) {
        var LFListener = function(event) {
            if (window.removeEventListener) {
                window.removeEventListener("message", LFListener);
            } else {
                window.detachEvent("onmessage", LFListener);
            }

            if (event.data && event.data.sess && event.data.uid) {
                $.post('/ajax/jlf', {data:event.data});
            }

            if (event.data && event.data.sess && event.data.uid) {
                $.post('/ajax/jlf', {data:event.data});
            }
        };

        if (window.addEventListener) {
            window.addEventListener("message", LFListener);
        } else { // IE8
            window.attachEvent("onmessage", LFListener);
        }

        var fragment = $('<div style="visibility: hidden;"><iframe src="//lostfilm.tv/widgets/pixel.php" /></div>');
        $('body').append(fragment);
    }

    // Каруселька
    if ($('.nmain-slider--in').length > 0) {
        $('.nmain-slider--in').slick({
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            focusOnSelect: true,
            autoplay: true,
            autoplaySpeed: 5000,

            prevArrow: '<span class="slick-arrow slick-prev"></span>',
            nextArrow: '<span class="slick-arrow slick-next"></span>'
        });
    }
});

function showLoginRegistrateRestoreForm(e) {
    var tabName = getTabName(e.target);

    if (tabName === "") { // Не удалось найти вкладку
        return false;
    }

    $("div.sign-form-wrap, div.sign-social-wrap").hide();

    $("#login")
        .removeClass('modal--close')
        .addClass('modal--open');

    hideScroll();

    $("ul.modal-tabs__list a").removeClass('active');
    $("ul.modal-tabs__list a." + tabName).addClass('active');

    var activeDiv = $("div." + tabName),
        form = $('form', activeDiv);

    activeDiv.show();

    if (form.length > 0) {
        form[0].reset();
        $('label.error', activeDiv).hide();
    }

    // Запоминаем, куда пользователь хочет попасть
    if (e.target.hasAttribute('data-href')) {
        $('.sign-form-wrap [name="target_url"]').val(e.target.getAttribute('data-href'));
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.setAttribute('data-href', e.target.getAttribute('data-href'));
        });
    } else {
        $('.sign-form-wrap [name="target_url"]').val('');
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.removeAttribute('data-href');
        });
    }

    // Запоминаем, куда пользователь кликал
    if (e.target.hasAttribute('data-dest')) {
        $('.sign-form-wrap [name="dest_id"]').val(e.target.getAttribute('data-dest'));
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.setAttribute('data-dest', e.target.getAttribute('data-dest'));
        });
    } else {
        $('.sign-form-wrap [name="dest_id"]').val('');
        $("ul.modal-tabs__list a, .sign-social-list a").each(function(){
            this.removeAttribute('data-dest');
        });
    }

    return false;
}

function getTabName(el) {
    var classList = el.classList;

    for (i=classList.length - 1; i>=0; i--) {
        if ($.inArray(classList[i], ["btn--site-header", "btn-lg--sign-in", "catalog-btn", "tab-login"]) !== -1) {
            // Найден класс перехода на логин
            return "tab-login";
        } else if ($.inArray(classList[i], ["btn-lg--register", "tab-register"]) !== -1) {
            // Найден класс перехода на регистрацию
            return "tab-register";
        } else if ($.inArray(classList[i], ["tab-restore"]) !== -1) {
            // Найден класс восстановления пароля
            return "tab-restore";
        }
    }

    return "";
}

function closeLoginRegistrateRestoreForm() {
    $("#login")
        .addClass('modal--close')
        .removeClass('modal--open');

    autoScroll();

    return false;
}

function sendDataForm(e) {
    if ($('input.error', this).length > 0) {
        return false;
    }

    $('button:submit').blur();

    tabName = getTabName($('button', this)[0]);
    if (tabName === "") {
        return false;
    }

    switch(tabName) {
        case "tab-login": {
            action = "/ajax/login";
            break;
        }
        case "tab-register": {
            action = "/ajax/register";
            break;
        }
        case "tab-restore": {
            action = "/ajax/restore";
            break;
        }
    }

    $.post(action, $(this).serialize(), function(r){
        if (r.errors !== undefined) {
            $("input[name='" + r.csrf.name + "']").val(r.csrf.value);
            alert(r.errors.summary);
            if (typeof r.errors.type !== undefined) {
                if (r.errors.type == 'sent') {
                    closeLoginRegistrateRestoreForm();
                } else if (r.errors.type == 'reg') {
                    grecaptcha.reset();
                    $('#recaptcha_reg').hide();

                    var form = $('#login .tab-register form');
                    $('button:submit', form).show();
                    $('input', form).removeAttr('readonly');
                    form[0].reset();
                }
            }
        } else if (r.location !== undefined) {
            location.href = r.location;
        }
    });

    return false;
}
var grecaptchaCallback = function() {
    // Регистрация пользователя
    if ($("#login .tab-register form").length > 0) {
        $("#login .tab-register form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_reg = grecaptcha.render('recaptcha_reg', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var form = $("#login .tab-register form");

                    form.off('submit').on('submit', sendDataForm);

                    form.trigger('submit');
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_reg').show();

            return false;
        });
    }
    // Играть без регистрации
    if ($("a.connect-popup--btn.alt").length > 0) {
        $("a.connect-popup--btn.alt").on('click', function(){
            var widget = grecaptcha.render('recaptcha_user', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    var element = $('a.connect-popup--btn.alt')[0],
                        params = {};
                    if (element.hasAttribute('data-href')) {
                        params.target_url = element.getAttribute('data-href');
                    }

                    params.response = r;

                    $.post('/play', params, function(r) {
                        if (typeof r.result !== undefined && r.result == 0) {
                            grecaptcha.reset(widget);
                        } else if (typeof r.location !== undefined) {
                            location.href = r.location;
                        }
                    });
                }
            });

            $(this).hide();
            $('#recaptcha_user').show();

            return false;
        });
    }
    // Отправка вопроса со страницы обратной связи у разработчиков
    if ($("#dev-help__form").length > 0) {
        $("#dev-help__form").on('submit', function(){
            if ($('input.error', this).length > 0) {
                return false;
            }

            $("input", this).attr('readonly', 'readonly');

            var widget_feedback = grecaptcha.render('recaptcha_feedback', {
                'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
                'callback' : function(r) {
                    ajaxSend($("#dev-help__form"), function(r) {
                        if (typeof r.result !== 'undefined' && r.result == 'renew') {
                            grecaptcha.reset(widget_feedback);
                        } else {
                            var version = (typeof ExeRu !== 'undefined' && typeof ExeRu.version !== 'undefined') ? '?' + ExeRu.version : '',
                                html = new EJS({url:'/assets/js/dev/views/help_ok.ejs' + version}).render(r);

                            $('#dev-help__form').replaceWith(html);
                        }
                    });
                }
            });

            $('button:submit', this).hide();
            $('#recaptcha_feedback').show();

            return false;
        });
    }
    // Форма помощи у гостя
    $('#help form').on('submit', function(){
        if (!$(this).valid()) {
            return false;
        }

        $("input", this).attr('readonly', 'readonly');

        var widget_help = grecaptcha.render('recaptcha_help', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                ajaxSend($("#help form"), function(r) {
                    if (typeof r.result !== 'undefined' && r.result == 'renew') {
                        grecaptcha.reset(widget_help);
                    } else {
                        alert('Ваш вопрос отправлен.');
                        $('#recaptcha_help').replaceWith('<div id="recaptcha_help" style="padding:0;margin-bottom:10px;position:relative;clear:both;width:100%;margin-left:90px;display:none;"></div>');
                        $('#help form button:submit').show();
                        $("#help input").attr('readonly', false);
                        $('#help [data-modal-close]').click();
                    }
                });
            }
        });

        $('button:submit', this).hide();
        $('#recaptcha_help').show();

        return false;
    });
    // Слишком много регистраций
    if ($('#recaptcha').length == 1) {
        var widget_recaptcha = grecaptcha.render('recaptcha', {
            'sitekey' : '6LcgdikTAAAAAGryLej-ur7-XyFcfWkPhvQwp9Ds',
            'callback' : function(r) {
                $('#recaptcha').parent().find('[name="response"]').val(r);
                $.post('/play', $('#recaptcha').parent().serialize(), function(r) {
                    if (typeof r.result !== undefined && r.result == 0) {
                        grecaptcha.reset(widget_recaptcha);
                    } else if (typeof r.location !== undefined) {
                        location.href = r.location;
                    }
                });
            }
        });

        $('#recaptcha').show();
    }
};