$(document).ready(function () {

	var onWindowResizeLoad = function () {
		var _f = $('.dev-footer'),
			_m = $('.minHeight');

		_m.css('min-height', 'auto');

		var contentHeight = _f.offset().top + _f.outerHeight(),
			windowHeight  = $(window).height();

		if (windowHeight > contentHeight)
			_m.css('min-height', _m.outerHeight() + windowHeight - contentHeight);
	}

	$(window).on('resize load', onWindowResizeLoad);

	$('.sel').each(function () {
		var self = $(this);

		self.find('.sel_hidden').val(self.find('.sel_list li:first').attr('data-value'));
	});

	$('.sel_in').on('click', function () {
		var self = $(this),
			select = self.closest('.sel'),
			option_list = select.find('.sel_list');

		if (option_list.is(':visible')) {
			option_list.slideUp(200);
			select.removeClass('is-opened');
			self.find('.sel_arrow').removeClass('is-active');
		} else {
			if ($('.sel .sel_list:visible').length) {
				$('.sel .sel_list:visible').hide();
				$('.sel .sel_arrow').removeClass('is-active');
			}

			option_list.slideDown(200);
			select.addClass('is-opened');
			self.find('.arrow').addClass('is-active');
		}
	});

	$('.sel_list li').on('click', function () {
		var self = $(this),
			title = self.closest('.sel').find('.sel_in .sel_title'),
			option = self.html();

		title.html(option);
		self.closest('.sel').find('input[type=hidden]').val(self.attr('data-value'));
		self.closest('.sel_list').find('li').removeClass('is-active');
		self.addClass('is-active');
		self.closest('.sel_list').slideUp(200);
		self.closest('.sel').removeClass('is-opened');
		self.closest('.sel').find('.sel_arrow').removeClass('is-active');
	});

	$(document).on('click', function (e) {
		if ($('.sel .sel_list:visible').length && !$(e.target).closest('.sel').length) {
			$('.sel').removeClass('is-opened');
			$('.sel .sel_list').slideUp(200);
			$('.sel .sel_arrow').removeClass('is-active');
		}
	});

	$(document).keyup(function (e) {
		if (e.keyCode == 27) {
			$('.sel').removeClass('is-opened');
			$('.sel .sel_list').slideUp(200);
		}
	});

	$('.dev-charts__tabs-list-item').on('click', function (e) {
		var parent = $(this).parent(),
			id = $(this).data('tab'),
			element = this;

		if (this.hasAttribute('data-loaded') && this.getAttribute('data-loaded') == 'false') {
			$.getJSON('/AdminAjax/graph/', {graphId: id}, function(r) {
				$(element).parent().find('.dev-charts__tabs-list-period[data-tab="' + id + '"]').html(r.start + ' - ' + r.end);

				var options = {
					legend: {
						enabled: true,
						layout: 'horizontal',
						align: 'left',
						verticalAlign: 'bottom',
						itemStyle: {padding: '10px'}

					},
					tooltip: { xDateFormat: '%d %B %Y', shared: false },
					credits: { enabled: false },
					chart: { type: 'spline' },
					colors: ['#755ec6', '#5762d5', '#3079df', '#21b2ad', '#e77e23', '#d45c9e', '#f1c40f', '#ef717a', '#27ae60'],
					rangeSelector: { enabled: false },
					title: { text: '' },

					xAxis: {},
					yAxis: { min: 0 },
					series: r.series
				};

				if (r.xRange) {
					options.xAxis = {range: r.xRange};
				}

				$('.dev-charts__item.item-' + id).highcharts('StockChart', options);

				$(element).addClass('active');
				parent.find('a.active').not(element).removeClass('active');

				parent.find('span').removeClass('active');
				parent.find('span[data-tab="' + id + '"]').addClass('active');

				$(element).closest('.dev-charts__tabs').find('.dev-charts__tabs-box.shown').removeClass('shown').parent().find('*[data-tab=' + id + ']').addClass('shown');
				element.setAttribute('data-loaded', 'true');
			});
		} else {
			$(element).addClass('active');
			parent.find('a.active').not(element).removeClass('active');

			parent.find('span').removeClass('active');
			parent.find('span[data-tab="' + id + '"]').addClass('active');

			$(element).closest('.dev-charts__tabs').find('.dev-charts__tabs-box.shown').removeClass('shown').parent().find('*[data-tab=' + id + ']').addClass('shown');
		}

		return false;
	});

	var _b = $('.dev-dialog__addAdmin-box');

	_b.find('form').on('submit', function () {
		if (_b.find('input[type=text]').val() != '') {

			// SEARCH CODE HERE. ADD RESULT TO .dev-dialog__addAdmin-result

			var _r = _b.find('.dev-dialog__addAdmin-result');

			_r.show();

			_r.unbind();
			_r.on('click', function () {
				
				_b.find('input[type=text]').val('');
				_b.hide();

				_d = _b.parent().find('.dev-dialog__addAdmin-done');

				_d.find('.dev-dialog__addAdmin-done-item-name').html(_r.find('.dev-roles__list-item-name').html());
				_d.show();

				_d.find('.dev-dialog__addAdmin-done-item-delete').unbind();
				_d.find('.dev-dialog__addAdmin-done-item-delete').on('click', function () {
					_d.hide();
					_r.hide();

					_b.show();

					_b.find('input[type=text]').focus();
				});
			});
		} else {
			_b.find('input[type=text]').focus();
		}

		return false;
	});

	/*var dlgtrigger = document.querySelectorAll('[data-modal]');

	for ( i = 0; i < dlgtrigger.length; i++) {
		 somedialog = document.getElementById(dlgtrigger[i].getAttribute('data-modal'));
		 dlg = new DialogFx(somedialog);

		$(dlgtrigger[i]).on('click', function (e) {
			e.preventDefault ();
			e.stopPropagation ();

			dlg.toggle();

			$('body, html').animate({
				scrollTop: $('.modal--open .modal').offset().top - 100
			}, 500);

			return false;
		});
	}*/

	$('[data-modal]').each(function () {
		var id = $(this).data('modal');
		var dlg = new DialogFx(document.getElementById(id));

		$(this).on('click', function (e) {
			e.preventDefault ();
			e.stopPropagation ();

			dlg.toggle();

			return false;
		});
	});

	$('.ui.dropdown').dropdown();

	$('.dev-news__item-tools-delete').on({
		click: function (e) {
			e.preventDefault();
			e.stopPropagation();

			$(this).closest('.dev-news__item').find('.dev-news__item-deleteBox').fadeIn(200);

			return false;
		}
	});


	if ($('.date-range:not(.single)').length > 0) {
		$('.date-range:not(.single)').dateRangePicker({
			format: 'DD.MM.YYYY',
			separator: ' - ',
			autoClose: true,

			getValue: function () { return this.innerHTML; },
			setValue: function (s) { this.innerHTML = s; }
		}).bind('datepicker-change', function(event, obj) {
			$('#start').val(moment(obj.date1).format('DD.MM.YYYY'));
			$('#end').val(moment(obj.date2).format('DD.MM.YYYY'));
		});
	}

	if ($('.date-range.single').length > 0) {
		$('.date-range.single').dateRangePicker({
			singleDate: true,
			singleMonth: true,
			format: 'DD.MM.YYYY',
			autoClose: true,

			getValue: function () { return this.innerHTML; },
			setValue: function (s) { this.innerHTML = s; }
		}).bind('datepicker-change', function(event, obj) {
			console.debug(obj);
			$('#day').val(moment(obj.date1).format('DD.MM.YYYY'));
			/*$('#end').val(moment(obj.date2).format('DD.MM.YYYY'));*/
		});
	}

	$('.dev-uadmin__icon').not('.payments, .banners').on({
		click: function (e) {
			var td = $(this).parents('td'),
				type = td.attr('data-type'),
				uid = td.parent().attr('data-uid'),
				state = ! $(this).hasClass('icon-check');

			$(this).parents('table')
				.find('tr[data-uid = "' + uid + '"] td[data-type="' + type + '"] i')
				.removeClass('icon-check icon-close')
				.addClass('loading');

			$.post('/admin/set', {'uid': uid, 'type': type, 'set': state}, function(r) {
			    if (typeof r.error != 'undefined') {
			        alert(r.error);
                }

                var iconClass = (r.state === true ? 'icon-check' : 'icon-close');

                $('.dev-uadmin__table table')
                    .find('tr[data-uid = "' + r.uid + '"] td[data-type="' + r.type + '"] i')
                    .removeClass('loading')
                    .addClass(iconClass);
			});
		}
	});

	$('.dev-uadmin__icon.payments').on({
		click: function (e) {
			var td = $(this).parents('td'),
				orderId = td.attr('data-order-id');

			$(this).parents('table')
				.find('td[data-order-id="' + orderId + '"] i')
				.addClass('loading');


			$.post('/admin/set', {'type': 'payment', 'order_id': orderId}, function(r) {
				if (typeof r.error != 'undefined') {
					alert(r.error);
				}

				var td = $('.dev-uadmin__table table td[data-order-id = "' + r.order_id + '"]');
				td.css('color', '#755ec6');
				td.find('i').parent().html('Зачислен');
			});

		}
	});

	$('.dev-uadmin__icon.banners').on({
		click: function (e) {
			var td = $(this).parents('td'),
				gid = td.attr('data-gid'),
				state = ! $(this).hasClass('icon-check');

			$(this).parents('table')
				.find('td[data-gid="' + gid + '"] i')
				.removeClass('icon-check icon-close')
				.addClass('loading');

			$.post('/adminAjax/carusel', {'gid': gid, 'set': state}, function(r) {
				if (typeof r.error != 'undefined') {
					alert(r.error);
				}

				var iconClass = (r.state === true ? 'icon-check' : 'icon-close');

				$('.dev-uadmin__table table')
					.find('td[data-gid="' + r.gid + '"] i')
					.removeClass('loading')
					.addClass(iconClass);
			});
		}
	});

	$('.admin_page .admin-filter-user').on('click', function(eventElement){
		var uid = eventElement.currentTarget.getAttribute('data-uid'),
			form = $('form');

		form.find('[name="byid"]').val(uid);
		form.find('button:first').click();

		return false;
	});

	$('.admin_page .admin-filter-game').on('click', function(eventElement){
		var gid = eventElement.currentTarget.getAttribute('data-gid'),
			form = $('form');

		form.find('[name="bygid"]').val(gid);
		form.find('button:first').click();

		return false;
	});

	$('form.emails input, form.emails textarea').on('change keydown', function() {
		var form = $(this).parents('form');
		form.find('.sendreal').attr('disabled', 'disabled').css({background:'lightgrey', pointer:'auto'});
	});

	$('.sendtest').on('click', function() {
		var form = $(this).parents('form'),
			testemail = form.find('[name="testemail"]').val().trim();

		if (testemail.length < 3 || testemail.indexOf('@') === -1) {
			alert('Не указан тестовый email');
			return false;
		}

		$.post(form[0].action, form.serialize(), function(r) {
			alert(r.text);
			var button = $('.sendreal')[0];
			button.removeAttribute('disabled');
			button.style.background = '';
			button.style.cursor = 'pointer';
		});

		return false;
	});

	$('.sendreal').on('click', function() {
		if (this.hasAttribute('disabled')) {
			return false;
		}

		var form = $(this).parents('form');
		$.post(form[0].action + 'send', form.serialize(), function(r) {
			alert(r.text);
		});

		return false;
	});
});