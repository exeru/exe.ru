<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 31.07.15
 * Time: 18:41
 */
class BlitzTemplate
{
    public function __construct()
    {
        $ci = &get_instance();
        $ci->blitz = $this;
    }

    public function view($template, $data = [])
    {
        $templater = new Blitz();
        $templater->load('{{ include(\'' . APPPATH . 'views/' . $template . '\') }}');
        $templater->display($data);
    }

    public function render($template, $data = [])
    {
        $templater = new Blitz();
        $templater->load('{{ include(\'' . APPPATH . 'views/' . $template . '\') }}');
        return $templater->parse($data);
    }
}