<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14.01.16
 * Time: 14:35
 */

namespace Base\Provider\Payment;

use Base\Provider\IPaymentProvider;
use Base\Provider\Payment;

defined('BASEPATH') OR exit('No direct script access allowed');

class QIWI extends Payment implements IPaymentProvider
{
    const config = [
        false => [
            'action' => 'https://qiwi.com/order/external/main.action',
            'shopId' => '531167',
            'restId' => '73396501',
            'shopPassword' => 'WCUEuqyI9uHJqDigF0ZF',
            'notificationPassword' => 'krxVwfmKwqFxHBUVT3oP',
            //'shopId' => '497982',
            //'restId' => '54204953',
            //'shopPassword' => 'OQGgCwuUN8IFEOI8RYDS',
            //'notificationPassword' => 'YgAsIscGIQ017JtBgc6A',
            'shopSuccessURL' => 'gw/qiwi/',
            'shopFailURL' => 'gw/qiwi/',
        ],
        true => [
            'action' => 'https://qiwi.com/order/external/main.action',
            'shopId' => '531167',
            'restId' => '73396501',
            'shopPassword' => 'WCUEuqyI9uHJqDigF0ZF',
            'notificationPassword' => 'krxVwfmKwqFxHBUVT3oP',
            //'shopPassword' => '1Beuda1KGMLQlR2ZAXA7',
            //'notificationPassword' => 'Rk3wnc2NOdL6abkjWclh',
            //'shopId' => '503862',
            //'restId' => '54261978',
            'shopSuccessURL' => 'gw/qiwi/',
            'shopFailURL' => 'gw/qiwi/',
        ]
    ];

    private static function init($phone, $sum, $paymentType, $orderNumber, $uid)
    {
        $config = self::getConfig();

        $data = [
            'user' => 'tel:' . $phone,
            'amount' => $sum,
            'ccy' => 'RUB',
            'comment' => 'Оплата ' . $orderNumber,
            'lifetime' => date('Y-m-d\TH:i:s', time() + 86400),
            'pay_source' => $paymentType,
            'prv_name' => 'EXE.RU',
            'extras' => ['customerId' => $uid]
        ];

        $curl_options = [
            CURLOPT_URL => 'https://api.qiwi.com/api/v2/prv/' . $config['shopId'] . '/bills/' . $orderNumber,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $config['restId'] . ':' . $config['shopPassword'],
            CURLOPT_HTTPHEADER => ['Accept: application/json']
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);

        $result = curl_exec($ch);

        curl_close($ch);
    }

    public static function prepare($orderNumber, $uid, $sum, $paymentType, $phone = '')
    {
        $config = self::getConfig();

        self::init($phone, $sum, $paymentType, $orderNumber, $uid);

        get_instance()->load->helper('url');
        $host = base_url('/');

        $params = [
            'form' => [
                'action' => $config['action'],
                'method' => 'get',
                'target' => '_blank'
            ],
            'elements' => [
                'shop' => $config['shopId'],
                'transaction' => $orderNumber,
                //'frame' => false,
                'successUrl' => $host . $config['shopSuccessURL'],
                'failUrl' => $host . $config['shopFailURL'],
                'orderNumber' => $orderNumber
                //'target' => ''
            ]
        ];

        return $params;
    }

    public static function getPaymentDataFromParams($params)
    {
        return [
            'orderId' => self::testMode ? str_replace('_TEST_', '', $params['bill_id']) : $params['bill_id'],
            'uid' => NULL,
            'sum' => $params['amount'],
            'invoiceId' => NULL
        ];
    }

    public static function isSignValid($params)
    {
        $config = self::getConfig();

        if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != $config['shopId']
            || !isset($_SERVER['PHP_AUTH_PW']) || $_SERVER['PHP_AUTH_PW'] != $config['notificationPassword']) {
            return false;
        }

        return true;
    }

    public static function getResponseText($code)
    {
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?>';
        $xmlString.= '<result><result_code>' . $code . '</result_code></result>';

        return $xmlString;
    }
}