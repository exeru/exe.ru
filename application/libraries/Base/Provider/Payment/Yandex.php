<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28.12.15
 * Time: 13:28
 */

namespace Base\Provider\Payment;

use Base\Provider\IPaymentProvider;
use Base\Provider\Payment;

defined('BASEPATH') OR exit('No direct script access allowed');

class Yandex extends Payment implements IPaymentProvider
{
    const shopPassword = 'compilation';

    const config = [
        'default' => [
            false => [
                'action' => 'https://money.yandex.ru/eshop.xml',
                'shopId' => 146985,
                'scid' => 143541,
                'shopSuccessURL' => 'gw/yandex/',
                'shopFailURL' => 'gw/yandex/',
                'shopDefaultURL' => 'gw/yandex/'
            ],
            true => [ // Для тестового режима используются идентификаторы старого аккаунта
                'action' => 'https://demomoney.yandex.ru/eshop.xml',
                'shopId' => 108973,
                'scid' => 529687,
                'shopSuccessURL' => 'gw/yandex/',
                'shopFailURL' => 'gw/yandex/',
                'shopDefaultURL' => 'gw/yandex/'
            ]
        ],
        'MC' => [
            false => [ // Мобильные платежи, используется старый аккаунт
                'action' => 'https://money.yandex.ru/eshop.xml',
                'shopId' => 108973,
                'scid' => 81541,
                'shopSuccessURL' => 'gw/yandex/',
                'shopFailURL' => 'gw/yandex/',
                'shopDefaultURL' => 'gw/yandex/'
            ],
            true => [ // У мобильных платежей нет тестового режима
                'action' => 'https://money.yandex.ru/eshop.xml',
                'shopId' => 108973,
                'scid' => 81541,
                'shopSuccessURL' => 'gw/yandex/',
                'shopFailURL' => 'gw/yandex/',
                'shopDefaultURL' => 'gw/yandex/'
            ]
        ],
/*        'SB' => [
            false => [ // Сбербанк, используется старый аккаунт
                'action' => 'https://money.yandex.ru/eshop.xml',
                'shopId' => 108973,
                'scid' => 81541,
                'shopSuccessURL' => 'gw/yandex/',
                'shopFailURL' => 'gw/yandex/',
                'shopDefaultURL' => 'gw/yandex/'
            ],
            true => [ // Сбербанк, нет тестового режима
                'action' => 'https://money.yandex.ru/eshop.xml',
                'shopId' => 108973,
                'scid' => 81541,
                'shopSuccessURL' => 'gw/yandex/',
                'shopFailURL' => 'gw/yandex/',
                'shopDefaultURL' => 'gw/yandex/'
            ]
        ]*/
    ];

    public static function getConfig($paymentType = null)
    {
        $config = self::config;

        if (!empty($paymentType) && isset($config[$paymentType])) {
            return $config[$paymentType][self::testMode];
        }

        return $config['default'][self::testMode];
    }

    public static function isSignValid($params)
    {
        $action = $params['action'];
        $orderSumAmount = $params['orderSumAmount'];
        $orderSumCurrencyPaycash = $params['orderSumCurrencyPaycash'];
        $orderSumBankPaycash = $params['orderSumBankPaycash'];
        $shopId = $params['shopId'];
        $invoiceId = $params['invoiceId'];
        $customerNumber = $params['customerNumber'];
        $hash = $params['md5'];
        $paymentType = $params['paymentType'];

        if (self::getConfig($paymentType)['shopId'] != $shopId || !in_array($action, ['checkOrder', 'paymentAviso'])) {
            return false;
        }

        $string = implode(';', [$action, $orderSumAmount, $orderSumCurrencyPaycash, $orderSumBankPaycash, $shopId, $invoiceId, $customerNumber]) . ';' . self::shopPassword;

        return strtoupper(md5($string)) === $hash;
    }

    public static function prepare($orderNumber, $uid, $sum, $paymentType, $phone = '')
    {
        $config = self::getConfig($paymentType);

        get_instance()->load->helper('url');
        $host = base_url('/');

        $params = [
            'form' => [
                'action' => $config['action'],
                'method' => 'post',
                'target' => '_blank'
            ],
            'elements' => [
                'shopId' => $config['shopId'],
                'scid' => $config['scid'],
                'orderNumber' => $orderNumber,
                'sum' => $sum,
                'customerNumber' => $uid,
                'paymentType' => $paymentType,
                'shopSuccessURL' => $host . $config['shopSuccessURL'],
                'shopFailURL' => $host . $config['shopFailURL'],
                'shopDefaultURL' => $host . $config['shopDefaultURL']
            ]
        ];

        if ($paymentType == 'MC' && $phone != '') {
            $params['elements']['cps_phone'] = $phone;
        }

        return $params;
    }

    public static function getResponseText($params, $code)
    {
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?>';
        $xmlString.= '<' . $params['action'].'Response ';
        $xmlString.= 'performedDatetime="' . date('Y-m-d\TH:i:s.000P') . '" ';
        $xmlString.= 'code="' .$code. '" invoiceId="' . $params['invoiceId'] . '" shopId="' . $params['shopId'] . '"/>';

        return $xmlString;
    }

    public static function getPaymentDataFromParams($params)
    {
        return [
            'orderId' => $params['orderNumber'],
            'uid' => $params['customerNumber'],
            'sum' => $params['orderSumAmount'],
            'invoiceId' => $params['invoiceId']
        ];
    }
}