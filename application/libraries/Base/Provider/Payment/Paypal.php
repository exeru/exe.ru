<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06.08.18
 * Time: 14:36
 */

namespace Base\Provider\Payment;

use Base\Provider\IPaymentProvider;
use Base\Provider\Payment;

class Paypal extends Payment implements IPaymentProvider
{
    /** Response from PayPal indicating validation was successful */
    const VALID = 'VERIFIED';
    /** Response from PayPal indicating validation failed */
    const INVALID = 'INVALID';

    const config = [
        false => [
            'action' => 'https://www.paypal.com/cgi-bin/websc',
            'verify_ipn' => 'https://ipnpb.paypal.com/cgi-bin/webscr',

            'verify_pdt' => 'https://www.paypal.com/cgi-bin/webscr',
            'token_pdt' => 'aX2MgRKR3UDf84RsRWQDicoOSgVwqYxG1zoTK8WNwaudCWgbOqiRiChRdqq',

            'owner' => 'office@exe.ru',
            //'clientId' => 'ARTRyUiVNf3tFiXDA5veJEt47IJWeLb1jaZ0u8a9j5qObQGXzed-ODuBYxmcAoYFCa2O2Ih9z6dWtTfi',
            //'clientSecret' => 'EIQntIvhF2sFGkVRrBnVoF6aNAPo0Gd0_1ELqvy564TotYAyCm2ELgFqh7wlWhk-aEP0RA5Bt01gEzVf',
            'shopNotifyURL' => 'gw/paypal/',
            'shopSuccessURL' => 'gw/paypal/',
            'shopFailURL' => 'gw/paypal/',
        ],
        true => [
            'action' => 'https://www.sandbox.paypal.com/cgi-bin/websc',
            'verify_ipn' => 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr',

            'verify_pdt' => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
            'token_pdt' => 'TcClcijHq5Zi4BjkdpmvS-egJaIpqf5sWPfSKG6LcARXF8JCrsDTXpW_xdG',

            'owner' => 'xstudent@yandex.ru',
            //'clientId' => 'ARTRyUiVNf3tFiXDA5veJEt47IJWeLb1jaZ0u8a9j5qObQGXzed-ODuBYxmcAoYFCa2O2Ih9z6dWtTfi',
            //'clientSecret' => 'EIQntIvhF2sFGkVRrBnVoF6aNAPo0Gd0_1ELqvy564TotYAyCm2ELgFqh7wlWhk-aEP0RA5Bt01gEzVf',
            'shopNotifyURL' => 'gw/paypal/',
            'shopSuccessURL' => 'gw/paypal/',
            'shopFailURL' => 'gw/paypal/',
        ]
    ];

    /**
     * https://test.exe.ru/gw/paypal/?
     * amt=114.51&
     * cc=RUB&
     * cm=eyJ1aWQiOiIxMzAzMTk1Iiwib3JkZXJfaWQiOjQzNDl9&
     * item_name=%D0%9F%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B0%20100%20%D1%80%D1%83%D0%B1%D0%BB%D0%B5%D0%B9&
     * st=Completed&
     * tx=23P530701M484401L
*/
    public static function prepare($orderNumber, $uid, $sum, $paymentType, $count = 0)
    {
        $config = self::getConfig();

        get_instance()->load->helper(['url', 'plural']);
        $host = base_url('/');

        // https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/
        $params = [
            'form' => [
                'action' => $config['action'],
                'method' => 'post',
                'target' => '_blank'
            ],
            'elements' => [
                'cmd' => '_xclick',
                'business' => $config['owner'],
                'paymentaction' => 'sale',
                'no_shipping' => 1,
                'currency_code' => 'RUB',
                'lc' => 'RU',
                'bn' => 'BuyNowBF',

                'item_name' => 'Покупка '. $count . plural_str($count, ' рубль', ' рубля', ' рублей'),
                'invoice' => $orderNumber,
                'quantity' => 1,
                'amount' => $sum,
                'orderNumber' => $orderNumber,

                'custom' => base64_encode(json_encode(['uid' => $uid, 'order_id' => $orderNumber])),

                'return' => $host . $config['shopSuccessURL'],
                'cancel_return' => $host . $config['shopFailURL'],
                'notify_url' => $host . $config['shopNotifyURL'],
            ]
        ];

        return $params;
    }

    public static function getPaymentDataFromParams($params)
    {
        $custom = json_decode(base64_decode($params['custom']), true);

        return [
            'orderId' => $custom['order_id'],
            'uid' => $custom['uid'],
            'sum' => $params['mc_gross'],
            'invoiceId' => $params['txn_id']
        ];
    }

    public static function isSignValid($params)
    {
        $config = self::getConfig();

        if (isset($params['business']) && $params['business'] == $config['owner']) {
            return true;
        }

        return false;
    }

    public static function getDataFromIPN($input)
    {
        if (strlen($input) == 0 || strpos($input, '&') === false || strpos($input, '=') === false) {
            return false;
        }

        $raw_post_array = explode('&', $input);

        $data = self::prepareInputData($raw_post_array);

        log_message('debug', 'Method getDataFromIPN');
        log_message('debug', var_export($data, true));

        if (self::IPNCheck($data)) {
            return $data;
        }

        return false;
    }

    public static function getDataFromPDT($getData)
    {
        if (!$getData || !is_array($getData) || count($getData) < 1 || !isset($getData['tx']) || strlen($getData['tx']) == 0) {
            return false;
        }

        $config = self::getConfig();

        $data = [
            'cmd' => '_notify-synch',
            'tx' => $getData['tx'],
            'at' => $config['token_pdt']
        ];

        $ch = curl_init($config['verify_pdt']);

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data, null, '&'));
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: PHP-IPN-Verification-Script',
            'Connection: Close',
        ));

        $res = curl_exec($ch);

        if (!$res) {
            return false;
        }

        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];
        if ($http_code != 200) {
            return false;
        }

        $data = explode("\n", $res);

        if ($data[0] !== 'SUCCESS') {
            return false;
        }

        unset($data[0]);

        return self::prepareInputData($data);
    }

    /**
     * Написано на основе кода
     * https://github.com/paypal/ipn-code-samples/blob/master/php/PaypalIPN.php
     */
    private static function prepareInputData($data)
    {
        $myPost = array();
        foreach ($data as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                if ($keyval[0] === 'payment_date') {
                    if (substr_count($keyval[1], '+') === 1) {
                        $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                    }
                }
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }

        return $myPost;
    }

    /**
     * Написано на основе кода
     * https://github.com/paypal/ipn-code-samples/blob/master/php/PaypalIPN.php
     */
    private static function IPNCheck($params)
    {
        $config = self::getConfig();

        // Build the body of the verification post request, adding the _notify-validate command.
        $req = 'cmd=_notify-validate';
        $get_magic_quotes_exists = false;
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }

        foreach ($params as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        log_message('debug', $req);

        // Post the data back to PayPal, using curl. Throw exceptions if errors occur.
        $ch = curl_init($config['verify_ipn']);

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        // This is often required if the server is missing a global cert bundle, or is using an outdated one.

        /*        if ($this->use_local_certs) {
                    curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/cert/cacert.pem");
                }*/

        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: PHP-IPN-Verification-Script',
            'Connection: Close',
        ));

        $res = curl_exec($ch);

        if ( ! ($res)) {
            $errno = curl_errno($ch);
            $errstr = curl_error($ch);
            curl_close($ch);

            log_message('error', "cURL error: [$errno] $errstr");
            return false;
        }

        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];
        if ($http_code != 200) {
            curl_close($ch);

            log_message('error', "PayPal responded with http code $http_code");
            return false;
        }

        curl_close($ch);

        // Check if PayPal verifies the IPN data, and if so, return true.
        if ($res == self::VALID) {
            return true;
        } else {
            return false;
        }
    }


}