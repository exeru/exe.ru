<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12.01.16
 * Time: 14:49
 */

namespace Base\Provider\Payment;

use Base\Provider\IPaymentProvider;
use Base\Provider\Payment;

defined('BASEPATH') OR exit('No direct script access allowed');

class Paymaster extends Payment implements IPaymentProvider
{
    const testMode = ENVIRONMENT == 'production' ? false : true;

    const shopPassword = 'compilation';

    const config = [
        false => [
            'action' => 'https://paymaster.ru/Payment/Init',
            //'shopId' => '106555c0-6f04-4cd8-b721-e8ef403877d9', // Старый, на интернет-парк
            'shopId' => 'f30da0d9-23c6-4faf-a3b9-e8df3bbf0cbd', // Новый, пока не подписан договор

            'shopInvoiceURL' => 'gw/paymaster/',
            'shopPaymentURL' => 'gw/paymaster/',
            'shopSuccessURL' => 'gw/paymaster/',
            'shopFailURL' => 'gw/paymaster/'
        ],
        true => [
            'action' => 'https://paymaster.ru/Payment/Init',
            'shopId' => 'f4ad01a8-42a1-47db-865b-33d0b1f2c975',

            'shopInvoiceURL' => 'gw/paymaster/',
            'shopPaymentURL' => 'gw/paymaster/',
            'shopSuccessURL' => 'gw/paymaster/',
            'shopFailURL' => 'gw/paymaster/',
/* Дополнительное поле, определяющее режим тестирования. Действует только в режиме тестирования и может принимать одно из следующих значений:
0 или отсутствует: Для всех тестовых платежей сервис будет имитировать успешное выполнение;
1: Для всех тестовых платежей сервис будет имитировать выполнение с ошибкой (платеж не выполнен);
2: Около 80% запросов на платеж будут выполнены успешно, а 20% - не выполнены. */
            'testMode' => 2
        ]
    ];

    public static function prepare($orderNumber, $uid, $sum, $paymentType, $phone = '')
    {
        $config = self::getConfig();

        get_instance()->load->helper('url');
        $host = base_url('/');

        $params = [
            'form' => [
                'action' => $config['action'],
                'method' => 'post',
                'target' => '_blank'
            ],
            'elements' => [
                'LMI_MERCHANT_ID' => $config['shopId'],
                'LMI_PAYMENT_NO' => $orderNumber,
                'LMI_PAYMENT_AMOUNT' => $sum,
                'LMI_CURRENCY' => 'RUB',
                'LMI_PAYMENT_DESC_BASE64' => base64_encode('Покупка рублей'), // TODO Прописать описание в соответствии с правилами
                'LMI_PAYMENT_METHOD' => $paymentType,
                'orderNumber' => $orderNumber,
                'LMI_INVOICE_CONFIRMATION_URL' => $host . $config['shopInvoiceURL'],
                'LMI_PAYMENT_NOTIFICATION_URL' => $host . $config['shopPaymentURL'],
                'LMI_SUCCESS_URL' => $host . $config['shopSuccessURL'],
                'LMI_FAILURE_URL' => $host . $config['shopFailURL'],
                'customerNumber' => $uid
            ]
        ];

        if (in_array($paymentType, ['MTS', 'Beeline', 'Tele2', 'Megafon']) && $phone != '') {
            if (strpos($phone, '+') === 0) {
                $phone = substr($phone, 1);
            }

            $params['elements']['LMI_PAYER_PHONE_NUMBER'] = $phone;
        }

        if (self::testMode) {
            $params['elements']['LMI_SIM_MODE'] = $config['testMode'];
        }

        return $params;
    }

    public static function getPaymentDataFromParams($params)
    {
        return [
            'orderId' => $params['LMI_PAYMENT_NO'],
            'uid' => $params['customerNumber'],
            'sum' => $params['LMI_PAYMENT_AMOUNT'],
            'invoiceId' => isset($params['LMI_SYS_PAYMENT_ID']) ? $params['LMI_SYS_PAYMENT_ID'] : NULL
        ];
    }

    public static function isSignValid($params)
    {
        if (self::getConfig()['shopId'] != $params['LMI_MERCHANT_ID']) {
            return false;
        }

        $hash = $params['LMI_HASH'];

        $fieldsArray = [
            $params['LMI_MERCHANT_ID'],
            $params['LMI_PAYMENT_NO'],
            $params['LMI_SYS_PAYMENT_ID'],
            $params['LMI_SYS_PAYMENT_DATE'],
            $params['LMI_PAYMENT_AMOUNT'],
            $params['LMI_CURRENCY'],
            $params['LMI_PAID_AMOUNT'],
            $params['LMI_PAID_CURRENCY'],
            $params['LMI_PAYMENT_SYSTEM'],
            $params['LMI_SIM_MODE'],
            self::shopPassword
        ];

        $string = implode(';', $fieldsArray);

        return base64_encode(hash('sha256', $string, true)) === $hash;
    }
}