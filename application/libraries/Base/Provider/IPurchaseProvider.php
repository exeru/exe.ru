<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.01.16
 * Time: 14:28
 */

namespace Base\Provider;

defined('BASEPATH') OR exit('No direct script access allowed');

interface IPurchaseProvider
{
    public static function getItemInfo($gid, $uid, $itemId);

    public static function purchaseItem($gid, $uid, $item, $orderId);
}