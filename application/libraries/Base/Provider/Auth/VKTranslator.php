<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.09.15
 * Time: 11:40
 */

namespace Base\Provider\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

trait VKTranslator
{
    protected function translateUserData($data)
    {
        $data = json_decode($data);
        $data = (array) $data->response[0];

        if (isset($data['bdate']) && !empty($data['bdate'])) {
            $parts = explode('.', $data['bdate']);
            if (count($parts) == 3) {
                list($day, $month, $year) = $parts;
            } elseif(count($parts) == 2) {
                list($day, $month) = $parts;
                $year = '';
            } else {
                $day = $month = $year = '';
            }
        } else {
            $day = $month = $year = '';
        }

        if (isset($data['country']) && !empty($data['country'])) {
            $data['country'] = (array) $data['country'];
            $country = $data['country']['title'];
        } else {
            $country = '';
        }

        if (isset($data['city']) && !empty($data['city'])) {
            $data['city'] = (array) $data['city'];
            $city = $data['city']['title'];
        } else {
            $city = '';
        }

        return [
            'social_type' => 'vk',
            'social_id' => (string) $data['id'],
            'mail' => $this->email,
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'sex' => ($data['sex'] == 2 ? 1 : ($data['sex'] == 1 ? 0 : -1)),
            'country_name' => $country,
            'city_name' => $city,
            'picture' => ''
        ];
    }

    protected function translateUserPhoto($photos)
    {
        $photos = json_decode($photos);
        $photos = (array) $photos->response;

        if (count($photos) < 1) {
            return false;
        }

        $photo = (array) $photos[0];

        $priority = [
            'src_xxbig',
            'src_xbig',
            'src_big',
            'src'
        ];

        $return = false;

        foreach($priority as $size) {
            if (isset($photo[$size]) && !empty($photo[$size])) {
                $return = $photo[$size];
                break;
            }
        }

        return $return;
    }
}