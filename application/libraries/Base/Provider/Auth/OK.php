<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 09.09.15
 * Time: 18:06
 */

namespace Base\Provider\Auth;

use Base\Provider\Auth;
use Base\Provider\IAuth;

defined('BASEPATH') OR exit('No direct script access allowed');

class OK extends Auth implements IAuth
{
    use OKTranslator;

    protected $config = [
        'DEVELOPMENT' => [
            false => [ // exe.ru.dev
                'APP_ID' => '1153957632',
                'APP_SECRET' => '3521CE5F44C055031648E489',
                'APP_PUBLIC' => 'CBAQPIMFEBABABABA'
            ],
            true => [ // dev.exe.ru.dev
                'APP_ID' => '1153957632',
                'APP_SECRET' => '3521CE5F44C055031648E489',
                'APP_PUBLIC' => 'CBAQPIMFEBABABABA'
            ]
        ],
        'TESTING' => [
            false  => [ // test.exe.ru
                'APP_ID' => '1153957632',
                'APP_SECRET' => '3521CE5F44C055031648E489',
                'APP_PUBLIC' => 'CBAQPIMFEBABABABA'
            ],
            true => [ // devtest.exe.ru
                'APP_ID' => '1153957632',
                'APP_SECRET' => '3521CE5F44C055031648E489',
                'APP_PUBLIC' => 'CBAQPIMFEBABABABA'
            ]
        ],
        'PRODUCTION' => [ // Аккаунт Кости
            false  => [ // exe.ru
                'APP_ID' => '1248821760',
                'APP_SECRET' => '7B68AB9F546029CB2A63A434',
                'APP_PUBLIC' => 'CBADIQGLEBABABABA'
            ],
            true => [ // dev.exe.ru
                'APP_ID' => '1248821760',
                'APP_SECRET' => '7B68AB9F546029CB2A63A434',
                'APP_PUBLIC' => 'CBADIQGLEBABABABA'
            ]
        ]
    ];

    private
        $_URL_AUTH = 'https://connect.ok.ru/oauth/authorize',
        $_URL_ACCESS_TOKEN = 'http://api.odnoklassniki.ru/oauth/token.do',
        $_URL_GET_USER = 'http://api.odnoklassniki.ru/fb.do';

    private
        $accessToken = null;

    public function getAccessToken($code, $callback)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'client_secret' => $config['APP_SECRET'],
            'code' => $code,
            'redirect_uri' => $callback,
            'grant_type' => 'authorization_code'
        ];

        $url = $this->prepareGetString($this->_URL_ACCESS_TOKEN, $params);

        $result = $this->getResponse($url, [], true);

        $response = json_decode($result);

        $this->accessToken = $response->access_token;

        return $this->accessToken;
    }

    public function getLoginUrl($callback, $popup)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'redirect_uri' => $callback,
            'response_type' => 'code',
            'scope' => 'GET_EMAIL'
        ];

        return $this->prepareGetString($this->_URL_AUTH, $params);
    }

    public function getUserData()
    {
        $config = $this->getConfig();

        $params = [
            'application_key' => $config['APP_PUBLIC'],
            'fields' =>
                'uid,locale,first_name,last_name,name,gender,age,'.
                'birthday,has_email,current_status,current_status_id,'.
                'current_status_date,online,location,email,pic_full',
            'method' => 'users.getCurrentUser',
        ];

        ksort($params);

        $sign = '';
        foreach($params as $name=>$value) {
            $sign .= $name . '=' . $value;
        }

        $params['sig'] = strtolower(md5($sign . md5($this->accessToken . $config['APP_SECRET'])));
        $params['access_token'] = $this->accessToken;

        $result = $this->getResponse($this->_URL_GET_USER, $params);

        return $this->translateUserData($result);
    }

    public function getUserPhoto(Array $userData)
    {
        $photos = []; // TODO

        if (($photo = $this->translateUserPhoto($photos)) !== false) {
            return $photo;
        }

        return $userData['picture'];
    }
}