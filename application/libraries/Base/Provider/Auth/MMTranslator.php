<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 15.12.16
 * Time: 10:39
 */

namespace Base\Provider\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

trait MMTranslator
{
    protected function translateUserData($data)
    {
        $data = (array) json_decode($data);
        $data = (array) $data[0];

        if (isset($data['birthday']) && !empty($data['birthday'])) {
            $parts = explode('.', $data['birthday']);
            if (count($parts) == 3) {
                list($day, $month, $year) = $parts;
            } elseif(count($parts) == 2) {
                list($day, $month) = $parts;
                $year = '';
            } else {
                $day = $month = $year = '';
            }
        } else {
            $day = $month = $year = '';
        }

        $country = $city = '';

        if (isset($data['has_pic']) && $data['has_pic'] == 1 && isset($data['pic_big']) && !empty($data['pic_big'])) {
            $data['picture'] = $data['pic_big'];
        } else {
            $data['picture'] = '';
        }

        return [
            'social_type' => 'mm',
            'social_id' => $data['uid'],
            'mail' => $data['email'],
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'sex' => ($data['sex'] == 0 ? 1 : ($data['sex'] == 1 ? 0 : -1)),
            'country_name' => $country,
            'city_name' => $city,
            'picture' => $data['picture']
        ];
    }

    protected function translateUserPhoto($photos)
    { // TODO
        return false;
    }
}