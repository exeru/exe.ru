<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 15.12.16
 * Time: 10:27
 */

namespace Base\Provider\Auth;

use Base\Provider\Auth;
use Base\Provider\IAuth;

defined('BASEPATH') OR exit('No direct script access allowed');

class MM extends Auth implements IAuth
{
    use MMTranslator;

    protected $config = [
        'DEVELOPMENT' => [
            false => [ // exe.ru.dev
                'APP_ID' => '750664',
                'APP_PRIVATE' => '615bf32993cf847f62c83a77fae6eab2',
                'APP_SECRET' => '8faa54785b06227013501440fa49a480'
            ],
            true => [ // dev.exe.ru.dev
                'APP_ID' => '751952',
                'APP_PRIVATE' => '8848951d3c2da80b288d606de79cf0e6',
                'APP_SECRET' => '068965baf6669e2ec4b8e370f8085d57'
            ]
        ],
        'TESTING' => [
            false  => [ // test.exe.ru
                'APP_ID' => '751947',
                'APP_PRIVATE' => '7940f5d29bf3bd12629de5d290cd27a5',
                'APP_SECRET' => 'd8a96cea1e9d324d18807f402f8e3454'
            ],
            true => [ // devtest.exe.ru
                'APP_ID' => '751953',
                'APP_PRIVATE' => '3dc80406d9ef466a2b04f71958cbf7f8',
                'APP_SECRET' => '609dc814272f8a795939a228430626f4'
            ]
        ],
        'PRODUCTION' => [ // Аккаунт Кости
            false  => [ // exe.ru
                'APP_ID' => '751904',
                'APP_PRIVATE' => '',
                'APP_SECRET' => 'cc73c95b8c3b4f53c331162f8e853fd5'
            ],
            true => [ // dev.exe.ru
                'APP_ID' => '751951',
                'APP_PRIVATE' => '',
                'APP_SECRET' => 'aa546b331c3fe8331a80c7a6598edd2b'
            ]
        ]
    ];

    private
        $_URL_AUTH = 'https://connect.mail.ru/oauth/authorize',
        $_URL_ACCESS_TOKEN = 'https://connect.mail.ru/oauth/token',
        $_URL_GET_USER = 'http://www.appsmail.ru/platform/api';

    private
        $accessToken = null;

    public function getLoginUrl($callback, $popup)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'redirect_uri' => $callback,
            'response_type' => 'code'/*,
            'scope' => 'email,user_birthday,user_location'*/
        ];

        return $this->prepareGetString($this->_URL_AUTH, $params);
    }

    public function getAccessToken($code, $callback)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'client_secret' => $config['APP_SECRET'],
            'grant_type' => 'authorization_code',
            'code' => trim($code),
            'redirect_uri' => $callback,
        ];

        $result = $this->getResponse($this->_URL_ACCESS_TOKEN, $params, true);

        $response = json_decode($result);

        $this->accessToken = $response->access_token;

        return $this->accessToken;
    }

    public function getUserData()
    {
        $config = $this->getConfig();

        $params = [
            'method' => 'users.getInfo',
            'app_id' => $config['APP_ID'],
            'session_key' => $this->accessToken,
            'secure' => 1
        ];

        ksort($params);

        $paramsString = '';

        array_walk($params, function($value, $key) use (&$paramsString) {
            $paramsString .= $key . '=' .$value;
        });

        $params['sig'] = md5($paramsString . $config['APP_SECRET']);

        $result = $this->getResponse($this->_URL_GET_USER, $params);

        return $this->translateUserData($result);
    }

    public function getUserPhoto(Array $userData)
    {
        $photos = []; // TODO

        if (($photo = $this->translateUserPhoto($photos)) !== false) {
            return $photo;
        }

        return $userData['picture'];
    }
}
/*
[
    {
        "pic_50":"http://avt-13.foto.mail.ru/mail/sevpalmira/_avatar50",
        "video_count":0,
        "friends_count":0,
        "show_age":1,
        "nick":"Руслан Шарифуллин",
        "is_friend":0,
        "is_online":1,
        "email":"sevpalmira@mail.ru",
        "has_pic":0,
        "follower":0,
        "pic_190":"http://avt-18.foto.mail.ru/mail/sevpalmira/_avatar190",
        "referer_id":"",
        "app_count":{
            "web":3,
            "mob_web":0
        },
        "following":0,
        "pic_32":"http://avt-17.foto.mail.ru/mail/sevpalmira/_avatar32",
        "referer_type":"wo_ref",
        "last_visit":"1486632175",
        "uid":"6398217874684745263",
        "app_installed":1,
        "status_text":"",
        "pic_22":"http://avt-6.foto.mail.ru/mail/sevpalmira/_avatar22",
        "has_my":1,
        "age":36,
        "last_name":"Шарифуллин",
        "is_verified":1,
        "pic_big":"http://avt-14.foto.mail.ru/mail/sevpalmira/_avatarbig",
        "vip":0,
        "birthday":"04.04.1980",
        "link":"https://my.mail.ru/mail/sevpalmira/",
        "pic_128":"http://avt-19.foto.mail.ru/mail/sevpalmira/_avatar128",
        "sex":0,
        "pic":"http://avt-28.foto.mail.ru/mail/sevpalmira/_avatar",
        "pic_small":"http://avt-30.foto.mail.ru/mail/sevpalmira/_avatarsmall",
        "pic_180":"http://avt-19.foto.mail.ru/mail/sevpalmira/_avatar180",
        "first_name":"Руслан",
        "pic_40":"http://avt-12.foto.mail.ru/mail/sevpalmira/_avatar40"
    }
]
 */