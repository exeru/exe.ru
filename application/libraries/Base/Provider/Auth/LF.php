<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 09.09.15
 * Time: 17:56
 */

namespace Base\Provider\Auth;

use Base\Provider\Auth;
use Base\Provider\IAuth;

defined('BASEPATH') OR exit('No direct script access allowed');

class LF extends Auth implements IAuth
{
    use LFTranslator;

    protected $config = [
        'DEVELOPMENT' => [
            false => [ // exe.ru.dev
                'APP_ID' => '1',
                'APP_SECRET' => 'c4ca4238a0b923820dcc509a6f75849b'
            ],
            true => [ // dev.exe.ru.dev
                'APP_ID' => '1',
                'APP_SECRET' => 'c4ca4238a0b923820dcc509a6f75849b'
            ]
        ],
        'TESTING' => [
            false  => [ // test.exe.ru
                'APP_ID' => '1',
                'APP_SECRET' => 'c4ca4238a0b923820dcc509a6f75849b'
            ],
            true => [ // devtest.exe.ru
                'APP_ID' => '1',
                'APP_SECRET' => 'c4ca4238a0b923820dcc509a6f75849b'
            ]
        ],
        'PRODUCTION' => [ // Аккаунт Кости
            false  => [ // exe.ru
                'APP_ID' => '1',
                'APP_SECRET' => 'c4ca4238a0b923820dcc509a6f75849b'
            ],
            true => [ // dev.exe.ru
                'APP_ID' => '1',
                'APP_SECRET' => 'c4ca4238a0b923820dcc509a6f75849b'
            ]
        ]
    ];

    private
        $_URL_AUTH = 'https://lostfilm.tv/oauth/',
        $_URL_ACCESS_TOKEN = 'https://lostfilm.tv/oauth/',
        $_URL_GET_USER = 'https://lostfilm.tv/api/v1/';

    private
        $accessToken = null;

    public function getAccessToken($code, $callback)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'client_secret' => $config['APP_SECRET'],
            'code' => $code,
            'redirect_uri' => $callback,
            'grant_type' => 'authorization_code'
        ];

        $result = $this->getResponse($this->_URL_ACCESS_TOKEN, $params, true);

        $response = json_decode($result);

        $this->accessToken = $response->access_token;

        return $this->accessToken;
    }

    public function getLoginUrl($callback, $popup)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'redirect_uri' => $callback,
            'response_type' => 'code'
        ];

        return $this->prepareGetString($this->_URL_AUTH, $params);
    }

    public function getUserData()
    {
        $params = [
            'method' => 'user',
            'fields' => 'username,avatar,email,bday,sex',
            'access_token' => $this->accessToken,
        ];

        $result = $this->getResponse($this->_URL_GET_USER, $params);

        return $this->translateUserData($result);
    }

    public function getUserPhoto(Array $userData)
    {
        $photos = []; // TODO

        if (($photo = $this->translateUserPhoto($photos)) !== false) {
            return $photo;
        }

        return $userData['picture'];
    }
}