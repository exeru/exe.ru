<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.09.15
 * Time: 11:45
 */

namespace Base\Provider\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

trait FBTranslator
{
    protected function translateUserData($data)
    {
        $data = (array) json_decode($data);

        if (isset($data['birthday']) && !empty($data['birthday'])) {
            $parts = explode('/', $data['birthday']);
            if (count($parts) == 3) {
                list($month, $day, $year) = $parts;
            } elseif(count($parts) == 2) {
                list($day, $month) = $parts;
                $year = '';
            } else {
                $day = $month = $year = '';
            }
        } else {
            $day = $month = $year = '';
        }

        if (isset($data['location']) && !empty($data['location'])) {
            $data['location'] = (array) $data['location'];
            list($city, $country) = explode(', ', $data['location']['name']);
        } else {
            $country = $city = '';
        }

        if (isset($data['picture']) && !empty($data['picture'])) {
            $data['picture'] = (array) $data['picture'];
            $data['picture']['data'] = (array) $data['picture']['data'];
            if ($data['picture']['data']['is_silhouette']) { // Это силуэт
                $data['picture']['data']['url'] = '';
            }
        }

        return [
            'social_type' => 'fb',
            'social_id' => $data['id'],
            'mail' => $data['email'],
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'sex' => ($data['gender'] == 'male' ? 1 : ($data['gender'] == 'female' ? 0 : -1)),
            'country_name' => $country,
            'city_name' => $city,
            'picture' => $data['picture']['data']['url']
        ];
    }

    protected function translateUserPhoto($photos)
    { // TODO
        return false;
    }
}