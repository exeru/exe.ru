<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.09.15
 * Time: 11:43
 */

namespace Base\Provider\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

trait OKTranslator
{
    protected function translateUserData($data)
    {
        $data = (array) json_decode($data);

        if (isset($data['birthday']) && !empty($data['birthday'])) {
            list($year, $month, $day) = explode('-', $data['birthday']);
        } else {
            $day = $month = $year = '';
        }

        if (isset($data['location']) && !empty($data['location'])) {
            $data['location'] = (array) $data['location'];
            $country = $data['location']['countryName'];
            $city = $data['location']['city'];
        } else {
            $country = $city = '';
        }

        return [
            'social_type' => 'ok',
            'social_id' => $data['uid'],
            'mail' => '', // TODO Писать в поддержку за правами для приложения
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'sex' => ($data['gender'] == 'male' ? 1 : ($data['gender'] == 'female' ? 0 : -1)),
            'country_name' => $country,
            'city_name' => $city,
            'picture' => ((strpos($data['pic_full'], 'i.mycdn.me/res/stub_') === false) ? $data['pic_full'] : '')
        ];
    }

    protected function translateUserPhoto($photos)
    { // TODO
        return false;
    }
}