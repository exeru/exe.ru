<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 09.09.15
 * Time: 18:12
 */

namespace Base\Provider\Auth;

use Base\Provider\Auth;
use Base\Provider\IAuth;

defined('BASEPATH') OR exit('No direct script access allowed');

class FB extends Auth implements IAuth
{
    use FBTranslator;

    protected $config = [
        'DEVELOPMENT' => [
            false => [ // exe.ru.dev
                'APP_ID' => '511900902319746',
                'APP_SECRET' => 'cbbf703170a0b17a44e76b0c7b6aa2de'
            ],
            true => [ // dev.exe.ru.dev
                'APP_ID' => '511900902319746',
                'APP_SECRET' => 'cbbf703170a0b17a44e76b0c7b6aa2de'
            ]
        ],
        'TESTING' => [
            false  => [ // test.exe.ru
                'APP_ID' => '511900902319746',
                'APP_SECRET' => 'cbbf703170a0b17a44e76b0c7b6aa2de'
            ],
            true => [ // devtest.exe.ru
                'APP_ID' => '511900902319746',
                'APP_SECRET' => 'cbbf703170a0b17a44e76b0c7b6aa2de'
            ]
        ],
        'PRODUCTION' => [ // Аккаунт Кости
            false  => [ // exe.ru
                'APP_ID' => '649089955271489',
                'APP_SECRET' => '05e95dcc9b5526158a65bea8e6ca8593'
            ],
            true => [ // dev.exe.ru
                'APP_ID' => '649089955271489',
                'APP_SECRET' => '05e95dcc9b5526158a65bea8e6ca8593'
            ]
        ]
    ];

    private
        $_URL_AUTH = 'https://www.facebook.com/dialog/oauth',
        $_URL_ACCESS_TOKEN = 'https://graph.facebook.com/oauth/access_token',
        $_URL_GET_USER = 'https://graph.facebook.com/v2.4/me';

    private
        $accessToken = null;

    public function getAccessToken($code, $callback)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'client_secret' => $config['APP_SECRET'],
            'code' => $code,
            'redirect_uri' => $callback,
        ];

        $result = $this->getResponse($this->_URL_ACCESS_TOKEN, $params);

        $response = json_decode($result);

        $this->accessToken = $response->access_token;

        return $this->accessToken;
    }

    public function getLoginUrl($callback, $popup)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'redirect_uri' => $callback,
            'response_type' => 'code',
            'scope' => 'email,user_birthday,user_location'
        ];

        if ($popup != false) {
            $params['display'] = 'popup';
        }

        return $this->prepareGetString($this->_URL_AUTH, $params);
    }

    public function getUserData()
    {
        $params = [
            'access_token' => $this->accessToken,
            'fields' => 'id,email,birthday,location,first_name,last_name,gender,picture.width(540).height(540)'
        ];

        $result = $this->getResponse($this->_URL_GET_USER, $params);

        return $this->translateUserData($result);
    }

    public function getUserPhoto(Array $userData)
    {
        $photos = []; // TODO

        if (($photo = $this->translateUserPhoto($photos)) !== false) {
            return $photo;
        }

        return $userData['picture'];
    }
}