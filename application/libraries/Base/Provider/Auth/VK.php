<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 09.09.15
 * Time: 17:29
 */

namespace Base\Provider\Auth;

use Base\Provider\Auth;
use Base\Provider\IAuth;

defined('BASEPATH') OR exit('No direct script access allowed');

class VK extends Auth implements IAuth
{
    use VKTranslator;

    protected $config = [
        'DEVELOPMENT' => [
            false => [ // exe.ru.dev
                'APP_VERSION' => '5.37',
                'APP_ID' => '5062125',
                'APP_SECRET' => 'mw6AqSm3WovWfiA09GTR'
            ],
            true => [ // dev.exe.ru.dev
                'APP_VERSION' => '5.37',
                'APP_ID' => '5062125',
                'APP_SECRET' => 'mw6AqSm3WovWfiA09GTR'
            ]
        ],
        'TESTING' => [
            false  => [ // test.exe.ru
                'APP_VERSION' => '5.37',
                'APP_ID' => '5062125',
                'APP_SECRET' => 'mw6AqSm3WovWfiA09GTR'
            ],
            true => [ // devtest.exe.ru
                'APP_VERSION' => '5.37',
                'APP_ID' => '5062125',
                'APP_SECRET' => 'mw6AqSm3WovWfiA09GTR'
            ]
        ],
        'PRODUCTION' => [ // Аккаунт Кости
            false  => [ // exe.ru
                'APP_VERSION' => '5.37',
                'APP_ID' => '5729708',
                'APP_SECRET' => 'BEIb7MnCtpvjPpqllzKv'
            ],
            true => [ // dev.exe.ru
                'APP_VERSION' => '5.37',
                'APP_ID' => '5729708',
                'APP_SECRET' => 'BEIb7MnCtpvjPpqllzKv'
            ]
        ]
    ];

    private
        $_URL_AUTH = 'http://oauth.vk.com/authorize',
        $_URL_ACCESS_TOKEN = 'https://oauth.vk.com/access_token',
        $_URL_GET_USER = 'https://api.vk.com/method/users.get',
        $_URL_GET_PHOTOS = 'https://api.vk.com/method/photos.get';

    private
        $accessToken = null,
        $uid = null,
        $email = null;

    public function getAccessToken($code, $callback)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'client_secret' => $config['APP_SECRET'],
            'code' => $code,
            'redirect_uri' => $callback,
        ];

        $url = $this->prepareGetString($this->_URL_ACCESS_TOKEN, $params);

        $result = $this->getResponse($url, $params);

        $response = json_decode($result);

        $this->accessToken = $response->access_token;
        $this->uid = $response->user_id;
        $this->email = $response->email;

        return $this->accessToken;
    }

    public function getLoginUrl($callback, $popup)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'redirect_uri' => $callback,
            'response_type' => 'code',
            'scope' => 'email'
        ];

        if ($popup != false) {
            $params['display'] = 'popup';
        }

        return $this->_URL_AUTH . "?" . http_build_query($params, null, '&');
    }

    public function getUserData()
    {
        $config = $this->getConfig();

        $params = [
            'uids' => $this->uid,
            'v' => $config['APP_VERSION'],
            'fields' => 'first_name,last_name,sex,bdate,city,country,photo_max_orig',
            'access_token' => $this->accessToken,
        ];

        $info = $this->getResponse($this->_URL_GET_USER, $params);

        return $this->translateUserData($info);
    }

    public function getUserPhoto(Array $userData)
    {
        $config = $this->getConfig();

        $params = [
            'owner_id' => $userData['social_id'],
            'album_id' => 'profile',
            'rev' => 1,
            'extended' => 0,
            'version' => $config['APP_VERSION']
        ];

        $photos = $this->getResponse($this->_URL_GET_PHOTOS, $params);

        if (($photo = $this->translateUserPhoto($photos)) !== false) {
            return $photo;
        }

        return $userData['picture'];
    }
}