<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.09.15
 * Time: 11:44
 */

namespace Base\Provider\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

trait LFTranslator
{
    protected function translateUserData($data)
    {
        $data = (array) json_decode($data);
        $data = (array) $data['result'];

        if (isset($data['bday']) && !empty($data['bday'])) {
            $parts = explode('-', $data['bday']);
            if (count($parts) == 3) {
                list($year, $month, $day) = $parts;
            }
            if ($year=='0000' || $month == '00' || $day == '00') {
                $day = $month = $year = '';
            }
        } else {
            $day = $month = $year = '';
        }

        return [
            'social_type' => 'lf',
            'social_id' => $data['id'],
            'mail' => $data['email'],
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'first_name' => $data['username'],
            'last_name' => '', //$data['family_name'],
            'sex' => ($data['sex'] == '1' ? 1 : ($data['sex'] == '2' ? 0 : -1)),
            'country_name' => '',
            'city_name' => '',
            'picture' => (strpos($data['avatar'], '/no-avatar.jpg') === false ? $data['avatar'] : '')
        ];
    }

    protected function translateUserPhoto($photos)
    { // TODO
        return false;
    }
}