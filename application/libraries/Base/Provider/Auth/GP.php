<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 09.09.15
 * Time: 17:56
 */

namespace Base\Provider\Auth;

use Base\Provider\Auth;
use Base\Provider\IAuth;

defined('BASEPATH') OR exit('No direct script access allowed');

class GP extends Auth implements IAuth
{
    use GPTranslator;

    protected $config = [
        'DEVELOPMENT' => [
            false => [ // exe.ru.dev
                'APP_ID' => '742026037239-0bgmou48b4hs6klf1314tuqoqibe3d36.apps.googleusercontent.com',
                'APP_SECRET' => 'ZkHV2B4gROr0SjsUByKIZezQ'
            ],
            true => [ // dev.exe.ru.dev
                'APP_ID' => '742026037239-0bgmou48b4hs6klf1314tuqoqibe3d36.apps.googleusercontent.com',
                'APP_SECRET' => 'ZkHV2B4gROr0SjsUByKIZezQ'
            ]
        ],
        'TESTING' => [
            false  => [ // test.exe.ru
                'APP_ID' => '742026037239-0bgmou48b4hs6klf1314tuqoqibe3d36.apps.googleusercontent.com',
                'APP_SECRET' => 'ZkHV2B4gROr0SjsUByKIZezQ'
            ],
            true => [ // devtest.exe.ru
                'APP_ID' => '742026037239-0bgmou48b4hs6klf1314tuqoqibe3d36.apps.googleusercontent.com',
                'APP_SECRET' => 'ZkHV2B4gROr0SjsUByKIZezQ'
            ]
        ],
        'PRODUCTION' => [ // Аккаунт Кости
            false  => [ // exe.ru
                'APP_ID' => '443985581240-2pckuksghku25bap9790b5vs13ro6e25.apps.googleusercontent.com',
                'APP_SECRET' => 'MWXUpYE6H-wwAoAjPtWAhKMZ'
            ],
            true => [ // dev.exe.ru
                'APP_ID' => '443985581240-2pckuksghku25bap9790b5vs13ro6e25.apps.googleusercontent.com',
                'APP_SECRET' => 'MWXUpYE6H-wwAoAjPtWAhKMZ'
            ]
        ]
    ];

    private
        $_URL_AUTH = 'https://accounts.google.com/o/oauth2/auth',
        $_URL_ACCESS_TOKEN = 'https://accounts.google.com/o/oauth2/token',
        $_URL_GET_USER = 'https://www.googleapis.com/oauth2/v1/userinfo';

    private
        $accessToken = null;

    public function getAccessToken($code, $callback)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'client_secret' => $config['APP_SECRET'],
            'code' => $code,
            'redirect_uri' => $callback,
            'grant_type' => 'authorization_code'
        ];

        $result = $this->getResponse($this->_URL_ACCESS_TOKEN, $params, true);

        $response = json_decode($result);

        $this->accessToken = $response->access_token;

        return $this->accessToken;
    }

    public function getLoginUrl($callback, $popup)
    {
        $config = $this->getConfig();

        $params = [
            'client_id' => $config['APP_ID'],
            'redirect_uri' => $callback,
            'response_type' => 'code',
            'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
        ];

        return $this->prepareGetString($this->_URL_AUTH, $params);
    }

    public function getUserData()
    {
        $params = [
            'access_token' => $this->accessToken,
        ];

        $result = $this->getResponse($this->_URL_GET_USER, $params);

        return $this->translateUserData($result);
    }

    public function getUserPhoto(Array $userData)
    {
        $photos = []; // TODO

        if (($photo = $this->translateUserPhoto($photos)) !== false) {
            return $photo;
        }

        return $userData['picture'];
    }
}