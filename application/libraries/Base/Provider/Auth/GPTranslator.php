<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.09.15
 * Time: 11:44
 */

namespace Base\Provider\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

trait GPTranslator
{
    protected function translateUserData($data)
    {
        $data = (array) json_decode($data);

        return [
            'social_type' => 'gp',
            'social_id' => $data['id'],
            'mail' => $data['email'],
            'day' => '', // TODO ?
            'month' => '', // TODO ?
            'year' => '', // TODO ?
            'first_name' => $data['given_name'],
            'last_name' => $data['family_name'],
            'sex' => ($data['gender'] == 'male' ? 1 : ($data['gender'] == 'female' ? 0 : -1)),
            'country_name' => '', // TODO ?
            'city_name' => '', // TODO ?
            'picture' => (strpos($data['picture'], '4252rscbv5M/photo.jpg') === false ? $data['picture'] : '')
        ];
    }

    protected function translateUserPhoto($photos)
    { // TODO
        return false;
    }
}