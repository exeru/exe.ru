<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.01.16
 * Time: 17:02
 */

namespace Base\Provider;

defined('BASEPATH') OR exit('No direct script access allowed');

interface IPaymentProvider
{
    public static function prepare($orderNumber, $uid, $sum, $paymentType);

    public static function getPaymentDataFromParams($params);

    public static function isSignValid($params);
}