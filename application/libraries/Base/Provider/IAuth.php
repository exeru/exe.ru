<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.09.15
 * Time: 11:58
 */

namespace Base\Provider;

defined('BASEPATH') OR exit('No direct script access allowed');

interface IAuth
{
    public function getAccessToken($code, $callback);

    public function getLoginUrl($callback, $popup);

    public function getUserData();

    public function getUserPhoto(Array $socialId);
}