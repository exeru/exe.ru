<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.09.15
 * Time: 11:57
 */

namespace Base\Provider;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @package Base\Provider
 * @method string getAccessToken(string $code, string $callback)
 * @method string getLoginUrl($callback, $popup);
 * @method array getUserData()
 * @method string getUserPhoto(array $userData)
 */
class Auth
{
    protected static $debug = true;

    /**
     * @param $type string
     * @return \Base\Provider\Auth
     */
    public static function get($type = null)
    {
        if (get_parent_class() !== false || is_null($type)) {
            return false;
        }

        $class = '\Base\Provider\Auth\\' . strtoupper($type);
        $obj =  new $class();
        return $obj;
    }

    protected function translateUserData($data)
    {
        return json_decode($data);
    }

    protected function translateUserPhoto($data)
    {
        return json_decode($data);
    }

    final public function getResponse($url, $params, $usePost = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($usePost) {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        } else {
            $url = $this->prepareGetString($url, $params);
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        $result = curl_exec($ch);
        curl_close($ch);

        if (self::$debug) {
            file_put_contents(APPPATH . 'logs/auth_social.log', var_export($result, true) . "\n\n", FILE_APPEND);
        }

        return $result;
    }

    final protected function prepareGetString($url, $params)
    {
        return $url . '?' . http_build_query($params, null, '&');
    }

    final protected function getConfig()
    {
        return $this->config[strtoupper(ENVIRONMENT)][\Base\Service\Project::isDev() || \Base\Service\Project::isAdmin()];
    }
}