<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.01.16
 * Time: 11:58
 */

namespace Base\Provider;

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment
{
    const testMode = ENVIRONMENT == 'production' ? false : true;

    const PAYMENT_YANDEX_AC = 1;
    const PAYMENT_YANDEX_MC = 2;
    const PAYMENT_YANDEX_QW = 3;
    const PAYMENT_YANDEX_PC = 4;
    const PAYMENT_YANDEX_WM = 5;
    const PAYMENT_YANDEX_GP = 6;
    const PAYMENT_PAYMASTER_WEBMONEY = 7;
    const PAYMENT_QIWI_QW = 8;
    const PAYMENT_PAYMASTER_SBERBANK = 9;
    const PAYMENT_PAYMASTER_ALFABANK = 10;
    const PAYMENT_PAYMASTER_VTB24 = 11;
    const PAYMENT_PAYMASTER_BRS = 12;
    const PAYMENT_PAYMASTER_BPSB = 13;
    const PAYMENT_YANDEX_SBERBANK = 14;
    const PAYMENT_YANDEX_ALFABANK = 15;
    const PAYMENT_YANDEX_BPSB = 16;
    const PAYMENT_EXE_INTERNAL = 17;
    const PAYMENT_PAYMASTER_MTS = 18;
    const PAYMENT_PAYMASTER_BEELINE = 19;
    const PAYMENT_PAYMASTER_TELE2 = 20;
    const PAYMENT_PAYMASTER_MEGAFON = 21;
    const PAYMENT_PAYPAL = 22;

    private static $rates = [
        self::PAYMENT_YANDEX_AC => '%count% * ',
        self::PAYMENT_YANDEX_MC => '%count% * 1.22 * ',
        self::PAYMENT_YANDEX_QW => '%count% * ',
        self::PAYMENT_YANDEX_PC => '%count% * ',
        self::PAYMENT_YANDEX_WM => '%count% * ',
        self::PAYMENT_YANDEX_GP => '%count% * ',
        self::PAYMENT_YANDEX_SBERBANK => '%count% * ',
        self::PAYMENT_YANDEX_ALFABANK => '%count% * ',
        self::PAYMENT_YANDEX_BPSB => '%count% * ',

        self::PAYMENT_QIWI_QW => '%count% * ',

        self::PAYMENT_PAYMASTER_WEBMONEY => '%count% * ',
        self::PAYMENT_PAYMASTER_SBERBANK => '%count% * ',
        self::PAYMENT_PAYMASTER_ALFABANK => '%count% * ',
        self::PAYMENT_PAYMASTER_VTB24 => '%count% * ',
        self::PAYMENT_PAYMASTER_BRS => '%count% * ',
        self::PAYMENT_PAYMASTER_BPSB => '%count% * ',
        self::PAYMENT_PAYMASTER_MTS => '%count% * 1.125 *',
        self::PAYMENT_PAYMASTER_BEELINE => '%count% * 1.22 * ',
        self::PAYMENT_PAYMASTER_TELE2 => '%count% * 1.175 * ',
        self::PAYMENT_PAYMASTER_MEGAFON => '%count% * ',

        self::PAYMENT_PAYPAL => '%count% * '
    ];

    public static function getRate($paymentSystemId, $oneBonusCost)
    {
        return self::$rates[$paymentSystemId] . $oneBonusCost;
    }

    public static function prepareFormData($orderNumber, $uid, $sum, $paymentTypeId, $phone, $count)
    {
        switch($paymentTypeId) {
            // Yandex.Money
            case self::PAYMENT_YANDEX_AC:
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'AC']);
            case self::PAYMENT_YANDEX_MC:
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'MC', $phone]);
            case self::PAYMENT_YANDEX_QW:
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'QW']);
            case self::PAYMENT_YANDEX_PC:
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'PC']);
            case self::PAYMENT_YANDEX_WM:
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'WM']);
            case self::PAYMENT_YANDEX_GP: {
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'GP']);
            }
            case self::PAYMENT_YANDEX_SBERBANK: {
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'SB']);
            }
            case self::PAYMENT_YANDEX_ALFABANK: {
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'AB']);
            }
            case self::PAYMENT_YANDEX_BPSB: {
                return call_user_func_array('Base\Provider\Payment\Yandex::prepare', [$orderNumber, $uid, $sum, 'PB']);
            }
            // Paymaster
            case self::PAYMENT_PAYMASTER_WEBMONEY: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'WebMoney']);
            }
            case self::PAYMENT_PAYMASTER_SBERBANK: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'SberbankOnline']);
            }
            case self::PAYMENT_PAYMASTER_ALFABANK: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'AlfaBank']);
            }
            case self::PAYMENT_PAYMASTER_VTB24: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'VTB24']);
            }
            case self::PAYMENT_PAYMASTER_BRS: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'RSB']);
            }
            case self::PAYMENT_PAYMASTER_BPSB: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'PSB']);
            }
            case self::PAYMENT_PAYMASTER_MTS: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'MTS', $phone]);
            }
            case self::PAYMENT_PAYMASTER_BEELINE: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'Beeline', $phone]);
            }
            case self::PAYMENT_PAYMASTER_TELE2: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'Tele2', $phone]);
            }
            case self::PAYMENT_PAYMASTER_MEGAFON: {
                return call_user_func_array('Base\Provider\Payment\Paymaster::prepare', [$orderNumber, $uid, $sum, 'Megafon', $phone]);
            }
            // QIWI
            case self::PAYMENT_QIWI_QW: {
                return call_user_func_array('Base\Provider\Payment\QIWI::prepare', [$orderNumber, $uid, $sum, 'qw', $phone]);
            }
            // PayPal
            case self::PAYMENT_PAYPAL: {
                return call_user_func_array('Base\Provider\Payment\Paypal::prepare', [$orderNumber, $uid, $sum, 'pp', $count]);
            }
        }
    }

    protected static function getConfig($paymentType = null)
    {
        $config = static::config;

        return $config[static::testMode];
    }
}