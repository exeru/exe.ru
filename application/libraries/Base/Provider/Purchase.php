<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.01.16
 * Time: 14:28
 */

namespace Base\Provider;

use Base\Service\Purchases;

defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase
{
    public static function getItemInfo($gid, $uid, $itemId, $gateway, $apiKey)
    {
        $params = [
            'action' => 'get_item',
            'app_id' => $gid,
            'item' => $itemId,
            'user_id' => $uid
        ];

        $params['sig'] = self::sign($params, $apiKey);

        $request = http_build_query($params);

        $result = json_decode(self::request($gateway, $request), true);

        if (isset($result['response']['error']) || isset($result['error'])) {
            return false;
        }

        return $result['response'];
    }

    public static function purchaseItem($gid, $uid, $item, $orderId, $gateway, $apiKey, $testMode)
    {
        $params = [
            'action' => 'buy_item',
            'app_id' => $gid,
            'item' => $item,
            'user_id' => $uid,

            'date' => time(),
            'order_id' => $orderId,
            'status' => 'complete',
        ];

        if ($testMode) {
            $params['test_mode'] = 1;
        }

        $params['sig'] = self::sign($params, $apiKey);

        $request = http_build_query($params);

        Purchases::writeLog($gid, 'request', $request, $testMode);

        $response = self::request($gateway, $request);

        Purchases::writeLog($gid, 'response', $response, $testMode);

        $result = json_decode($response, true);

        if (empty($result)
            || !isset($result['response']['order_id'])
            || isset($result['response']['error'])
            || $result['response']['order_id'] != $orderId
        ) {
                return false;
        }

        return $result['response'];
    }

    private static function sign($params, $apiKey)
    {
        ksort($params);

        return md5(urldecode(http_build_query($params, null, '')) . $apiKey);
    }

    private static function request($gateway, $request)
    {
        $curl_options = [
            CURLOPT_URL => $gateway,
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => ['Accept: application/json']
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);

        $result = curl_exec($ch);

        curl_close($ch);

        \Base\Service\Log::push('purchasing', var_export($request, true));
        \Base\Service\Log::push('purchasing', var_export($result, true));

        return $result;
    }
}