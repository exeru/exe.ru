<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28.12.15
 * Time: 17:53
 */

namespace Base\Model;


class Payments extends DB
{
    const TABLE_PAYMENTS_NAME = 'payments';
    const TABLE_NAME = 'yandex_money';

    public static function getAmountByDays($start, $end)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "orderNumber",
            "datetime",
            "count"
          FROM
            "yandex_money",
            "payments"
          WHERE
            "yandex_money"."status" IN (2,3)
          AND
            "datetime" BETWEEN ' . $start . ' AND ' . $end . '
          AND
            "yandex_money"."orderNumber" = "payments"."payment_id"
        ';

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getPaymentsDataByIds(array $ids, array $fields)
    {
        $db = self::get_client();

        $db->select($fields);
        $db->from(self::TABLE_PAYMENTS_NAME);
        $db->where_in('payment_id', $ids);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getRequestsLog($start, $end, $byid, $filter, $limit, $offset)
    {
        $db = self::get_slave();

        $db->select('COUNT(*) as count');
        $db->from(self::TABLE_NAME);

        if (!is_null($byid)) {
            $db->where(['uid' => $byid]);
        }

        if (count($filter) > 0) {
            $db->where_in('type', $filter);
        }

        $db->where([
            'datetime >=' => $start,
            'datetime <=' => $end,
        ]);

        $count = $db->get()->row()->count;

        $db->select();
        $db->from(self::TABLE_NAME);

        if (!is_null($byid)) {
            $db->where(['uid' => $byid]);
        }

        if (count($filter) > 0) {
            $db->where_in('type', $filter);
        }

        $db->where([
            'datetime >=' => $start,
            'datetime <=' => $end,
        ]);

        $db->order_by('orderNumber', 'desc');
        $db->order_by('status', 'desc');
        $db->limit($limit, $offset);

        $result = $db->get()->result_array();

        return ['count' => $count, 'result' => $result];
    }

    public static function updatePayment($paymentId, $invoiceId, $status)
    {
        $db = self::get_client();

        $db->update(self::TABLE_PAYMENTS_NAME, ['invoice_id'=>$invoiceId, 'status'=>$status], ['payment_id'=>$paymentId]);
    }

    public static function getPayment($paymentId)
    {
        $db = self::get_client();

        $db->select(['payment_id', 'uid', 'payment_system_id', 'sum', 'invoice_id', 'status', 'count']);
        $db->from(self::TABLE_PAYMENTS_NAME);
        $db->where(['payment_id' => $paymentId]);
        $db->limit(1);

        $result = $db->get()->row_array();

        return $result;
    }

    public static function createPayment($orderNumber, $uid, $paymentSystemId, $count, $rate, $sum)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_PAYMENTS_NAME, [
            'payment_id' => $orderNumber,
            'uid' => $uid,
            'payment_system_id' => $paymentSystemId,
            'sum' => $sum,
            'status' => 0,
            'count' => $count,
            'rate' => $rate
        ]);
    }

    public static function saveRequest($uid, $type, $request, $orderNumber, $status, $response)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_NAME, [
            'uid' => $uid,
            'datetime' => time(),
            'type' => $type,
            'request' => is_array($request) ? serialize($request) : $request,
            'orderNumber' => $orderNumber,
            'status' => $status,
            'response' => is_array($response) ? serialize($response) : $response
        ]);
    }
}