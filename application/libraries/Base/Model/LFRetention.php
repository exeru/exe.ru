<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17.05.17
 * Time: 16:18
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class LFRetention extends DB
{
    const _TABLE_LF_RETENTION = "lf_retention";
    const _TABLE_LF_ASSIGN = "lf_assign";

    public static function getAllAssigns()
    {
        $db = self::get_client();

        $query = 'SELECT "lf_uid", "exe_uid" FROM "lf_assign" ORDER BY "lf_uid"';

        $result = $db->query($query)->result_array();

        return $result;
    }


    public static function appendAssign($lfUid, $uids)
    {
        $data = [];

        foreach($uids as $uid) {
            $data[] = [
                'lf_uid' => $lfUid,
                'exe_uid' => $uid
            ];
        }

        $db = self::get_client();

        $db->insert_batch(self::_TABLE_LF_ASSIGN, $data);

        return true;
    }

    public static function getLFUidsByUids($allUids)
    {
        $db = self::get_client();

        $db->select('DISTINCT "lf_uid" as "lf_uid"', false);
        $db->from(self::_TABLE_LF_ASSIGN);
        $db->where_in('exe_uid', $allUids);
        $result = $db->get()->result_array();

        return $result;
    }

    public static function getUidsByLFUid($lfUid)
    {
        $db = self::get_client();

        $db->select(['exe_uid']);
        $db->from(self::_TABLE_LF_ASSIGN);
        $db->where(['lf_uid' => $lfUid]);
        $result = $db->get()->result_array();

        return $result;
    }

    public static function getLFUsers($start, $end)
    {
        $db = self::get_client();
        $db->select(['lf_uid']);
        $db->from(self::_TABLE_LF_RETENTION);
        $db->where([
            'time >=' => $start,
            'time <' => $end,
            'old' => false
        ]);
        $db->group_by('lf_uid');

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getDataByGameId($start, $end, $appId)
    {
        $db = self::get_client();
        $db->select(['lf_uid', 'key']);
        $db->from(self::_TABLE_LF_RETENTION);
        $db->where([
            'time >=' => $start,
            'time <' => $end,
            'gid' => $appId,
            'old' => false
        ]);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getGameIds($start, $end, $gids)
    {
        $db = self::get_client();

        $db->select('DISTINCT "gid" as "gid"', false);
        $db->from(self::_TABLE_LF_RETENTION);
        $db->where_in('gid', $gids);
        $db->where([
            'time >=' => $start,
            'time <' => $end,
            'old' => false
        ]);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function update($lfid, $games)
    {
        foreach($games as $index => $value) {
            $games[$index]['lf_uid'] = $lfid;
            $games[$index]['old'] = false;
        }

        $db = self::get_client();

        $db->delete(self::_TABLE_LF_RETENTION, ['lf_uid' => $lfid]);

        if (count($games) > 0) {
            $db->insert_batch(self::_TABLE_LF_RETENTION, $games);
        }

        return true;
    }
}