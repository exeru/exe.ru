<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17.08.15
 * Time: 11:05
 */

namespace Base\Model;

use Base\Service\Hash;

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends DB
{
    const TABLE_NAME = 'tokens';

    public static function createToken($email)
    {
        $token = Hash::getHash($email, true);

        $db = self::get_client();;

        $db->insert(self::TABLE_NAME, [
            'mail' => $email,
            'token' => $token,
            'generation_time' => time()
        ]);

        return $token;
    }

    public static function isTokenExists($email, $token)
    {
        $db = self::get_client();
        $db->select("generation_time");
        $db->from(self::TABLE_NAME);
        $db->where([
            'mail' => $email,
            'token' => $token
        ]);
        $db->limit(1);

        $result = $db->get();

        if ($result->num_rows() == 0) {
            return false;
        }

        return true;
    }

    public static function deleteToken($email, $token) {
        $db = self::get_client();;
        $db->delete(self::TABLE_NAME, [
            'mail' => $email,
            'token' => $token
        ]);
        return true;
    }
}