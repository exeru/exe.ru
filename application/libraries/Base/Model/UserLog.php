<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04.10.16
 * Time: 13:46
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class UserLog extends DB
{
    const TABLE_NAME = 'userlog';

    public static function getInstalledGidsByPeriod($start, $end)
    {
        $db = self::get_client();

        $query = 'SELECT DISTINCT "gid" FROM "' . self::TABLE_NAME . '" WHERE "time" >= \''.$start.'\' AND "time" < \''.$end.'\' AND "action" = \''.\Base\Service\UserLog::INSTALL_GAME.'\'';

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getInstalledByGroupsByPeriod($start, $end, $fieldGroup, $installedGids)
    {
        $db = self::get_client();

        $query = '
          SELECT "'.$fieldGroup.'", "gid", COUNT(DISTINCT "uid") FROM (
            SELECT 
               "' . self::TABLE_NAME . '"."marker",
               "' . self::TABLE_NAME . '"."uid",
               "' . self::TABLE_NAME . '"."gid"
            FROM
               "' . self::TABLE_NAME . '"
            WHERE
               "time" >= \''.$start.'\' AND "time" < \''.$end.'\' AND "action" = 25';

        if (is_array($installedGids) && count($installedGids) > 0) {
            $query .= ' AND "gid" IN ('.implode(', ', $installedGids).')';
        }

        $query .=' AND "marker" IN (
              SELECT "marker" FROM "sessions" WHERE "time_start" >= \''.$start.'\' AND "time_start" < \''.$end.'\' AND "init"
              )
            ) as "t1"
            LEFT JOIN (
              SELECT "marker", "'.$fieldGroup.'" FROM "sessions" WHERE "time_start" >= \''.$start.'\' AND "time_start" < \''.$end.'\' AND "init"
            ) as "t2"
            ON "t1"."marker" = "t2"."marker"
          GROUP BY "'.$fieldGroup.'", "gid"';

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getInstalledNewMaxTime()
    {
        $query = 'SELECT "time" FROM "' . self::TABLE_NAME . '" WHERE "action" = \''.\Base\Service\UserLog::INSTALL_GAME.'\' AND "marker" IN (SELECT "marker" FROM "sessions" WHERE "init" ORDER BY "time_start" DESC LIMIT 50) ORDER BY "time" DESC LIMIT 1';

        $db = self::get_client();
        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return NULL;
    }

    public static function getInstalledNewMinTime()
    {
        $query = 'SELECT "time" FROM "' . self::TABLE_NAME . '" WHERE "action" = \''.\Base\Service\UserLog::INSTALL_GAME.'\' AND "marker" IN (SELECT "marker" FROM "sessions" WHERE "init" ORDER BY "time_start" ASC LIMIT 50) ORDER BY "time" ASC LIMIT 1';

        $db = self::get_client();
        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return NULL;
    }

    /**
     * Используется для построения графиков в административной части
     */
    public static function getMaxRegistrationTime()
    {
        $query = 'SELECT "time" FROM "' . self::TABLE_NAME . '"
            WHERE "action" IN ('.\Base\Service\UserLog::REGISTER_SOCIAL.', '.\Base\Service\UserLog::REGISTER_TEMPORARY_SOCIAL.', '.\Base\Service\UserLog::REGISTER.', '.\Base\Service\UserLog::REGISTER_TEMPORARY.')
            ORDER BY "time" DESC LIMIT 1';

        $db = self::get_client();
        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    /**
     * Используется для построения графиков в административной части
     */
    public static function getMinRegistrationTime()
    {
        $query = 'SELECT "time" FROM "' . self::TABLE_NAME . '"
            WHERE "action" IN ('.\Base\Service\UserLog::REGISTER_SOCIAL.', '.\Base\Service\UserLog::REGISTER_TEMPORARY_SOCIAL.', '.\Base\Service\UserLog::REGISTER.', '.\Base\Service\UserLog::REGISTER_TEMPORARY.')
            ORDER BY "time" LIMIT 1';

        $db = self::get_client();
        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    /**
     * Используется для построения графиков в административной части
     */
    public static function getRegistationsDataByPeriod($start, $end, $fieldGroup)
    {
        $query = 'SELECT "'.$fieldGroup.'", "action", "params", COUNT(DISTINCT "uid")
        FROM "'.self::TABLE_NAME.'"
        WHERE "action" IN ('.\Base\Service\UserLog::REGISTER_SOCIAL.', '.\Base\Service\UserLog::REGISTER_TEMPORARY_SOCIAL.', '.\Base\Service\UserLog::REGISTER.', '.\Base\Service\UserLog::REGISTER_TEMPORARY.')
        AND "time" >= '.$start.' AND "time" < '.$end.'
        GROUP BY "'.$fieldGroup.'", "action", "params"';

        $db = self::get_client();
        $result = $db->query($query)->result_array();

        return $result;
    }

    private static function getTableNameForWrite()
    {
        return self::TABLE_NAME . '_' . date('Ymd');
    }

    public static function save($uid, $gid, $action, $time, $param = null, $marker = null, $sessionNumber = null)
    {
        if (!is_null($param)) {
            if (!is_array($param)) {
                $param = [$param];
            }
            $param = serialize($param);
        }

        $db = self::get_client();

        $db->insert(self::getTableNameForWrite(), [
            'uid' => $uid,
            'action' => $action,
            'time' => $time,
            'params' => $param,
            'gid' => $gid,
            'marker' => $marker,
            'session_number' => $sessionNumber,
            'yyyymm' => date('Ym', $time),
            'yyyymmdd' => date('Ymd', $time),
            'yyyymmddhh' => date('YmdH', $time)
        ]);
    }

    /**
     * Используется для вывода действий пользователя в административной части
     */
    public static function getLog($start, $end, $byid, $sessionNumbers, $bygid, $bots, $temporary, $filter, $limit, $offset)
    {
        $db = self::get_client();

        $db->select('COUNT(*) as count');
        $db->from(self::TABLE_NAME);

        if (!is_null($byid)) {
            $db->where(['uid' => $byid]);
        }

        if (!is_null($sessionNumbers)) {
            $db->where_in('session_number', $sessionNumbers);
        }

        if (!is_null($bygid)) {
            $db->where(['gid' => $bygid]);
        }

        if (count($filter) > 0) {
            $db->where_in('action', $filter);
        }

        if ($bots === false) {
            $db->where('"uid" NOT IN (SELECT "uid" FROM "users_ext" WHERE "is_bot")', NULL, false);
        }

        if ($temporary === false) {
            $db->where('"uid" IN (SELECT "uid" FROM "users_ext" WHERE "verified" = 1)', NULL, false);
        }

        $db->where([
            'time >=' => $start,
            'time <=' => $end,
        ]);

        $count = $db->get()->row()->count;

        $db->select();
        $db->from(self::TABLE_NAME);

        if (!is_null($byid)) {
            $db->where(['uid' => $byid]);
        }

        if (!is_null($sessionNumbers)) {
            $db->where_in('session_number', $sessionNumbers);
        }

        if (!is_null($bygid)) {
            $db->where(['gid' => $bygid]);
        }

        if ($bots === false) {
            $db->where('"uid" NOT IN (SELECT "uid" FROM "users_ext" WHERE "is_bot")', NULL, false);
        }

        if ($temporary === false) {
            $db->where('"uid" IN (SELECT "uid" FROM "users_ext" WHERE "verified" = 1)', NULL, false);
        }

        if (count($filter) > 0) {
            $db->where_in('action', $filter);
        }

        $db->where([
            'time >=' => $start,
            'time <=' => $end,
        ]);

        $db->order_by('id', 'desc');
        $db->limit($limit, $offset);

        $result = $db->get()->result_array();

        if (count($result) > 0) {
            foreach($result as &$row) {
                if (!is_null($row['params'])) {
                    $row['params'] = unserialize($row['params']);
                }
            }
        }

        return ['count' => $count, 'result' => $result];
    }

    /*
     * Используется для вывода действий на протяжении сессии в административной части
     * ! Используется для сбора информации при генерации записей в таблицу сессий (sessions_)
     * (closeUserMarker)
     */
    public static function getLogByMarker($marker, $start, $end)
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::TABLE_NAME);
        $db->where(['marker' => $marker]);

        if ($start > 0 && $end > 0) {
            $db->where(['time >=' => $start, 'time <= ' => $end]);
        }

        $db->order_by('id', 'desc');

        $result = $db->get()->result_array();

        if (count($result) > 0) {
            foreach($result as &$row) {
                if (!is_null($row['params'])) {
                    $row['params'] = unserialize($row['params']);
                }
            }
        }

        return $result;
    }

    /**
     * Используется для вывода действий пользователя в административной части
     */
    public static function getPayersDataByDayByPeriod($start, $end, $isInit)
    {
        $query = 'SELECT "yyyymmddhh", COUNT("id") as "count_payments", COUNT(DISTINCT "uid") as "count_payers"
            FROM "' . self::TABLE_NAME . '"
            WHERE "action" = 11 AND "time" >= \''.$start.'\' AND "time" < \''.$end.'\'';

        if ($isInit === true) {
            $query .= ' AND "session_number" = 1';
        } elseif ($isInit === false) {
            $query .= ' AND "session_number" > 1';
        }

        $query .=' GROUP BY "yyyymmddhh"';

        $db = self::get_client();

        $result = $db->query($query)->result_array();

        return $result;
    }

    /**
     * Похоже, больше не используется
     *
     * @deprecated
     */
    public static function getMarkersForPeriodByAction($start, $end, $action)
    {
        $db = self::get_client();

        $db->select('marker', false);
        $db->from(self::TABLE_NAME);
        $db->where(['"marker" IS NOT NULL AND "time" >= ' => $start, '"time" < ' => $end], null, false);

        if (!empty($action))
        {
            $db->where(['action' => $action]);
        }

        $result = $db->get()->result_array();

        if (count($result) > 0) {
            return array_column($result, 'marker', 'marker');
        }

        return [];
    }
}