<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23.09.15
 * Time: 11:01
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends DB
{
    const _TABLE_USER_CHANGES = 'user_changes';
    const _TABLE_GAMERS_CHANGES = 'gamers_changes';
    const _TABLE_GAMES_CHANGES = 'games_changes';

    public static function acceptUser($uid, $gid, $sid, $cacherIds)
    {
        // TODO ???
        return true;
    }

    public static function usersChange($uid, $change, $cacherIds)
    {
        $db = self::get_client();

        $rows = [];
        foreach($cacherIds as $cid) {
            $rows[] = [
                'uid' => $uid,
                'change' => $change,
                'cid' => $cid
            ];
        }

        return $db->insert_batch(self::_TABLE_USER_CHANGES, $rows);
    }

    public static function gamersChange($uid, $gid, $state, $timestamp, $cacherIds)
    {
        $db = self::get_client();

        $rows = [];
        foreach($cacherIds as $cid) {
            $rows[] = [
                'uid' => $uid,
                'gid' => $gid,
                'state' => $state,
                'last_visit' => $timestamp,
                'cid' => $cid
            ];
        }

        return $db->insert_batch(self::_TABLE_GAMERS_CHANGES, $rows);
    }

    public static function gamesChange($gid, $state, $cacherIds)
    {
        $db = self::get_client();

        $rows = [];
        foreach($cacherIds as $cid) {
            $rows[] = [
                'gid'   => $gid,
                'state' => $state,
                'cid' => $cid
            ];
        }

        return $db->insert_batch(self::_TABLE_GAMES_CHANGES, $rows);
    }
}