<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 03.09.15
 * Time: 15:15
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Blacklist extends DB
{
    const TABLE_NAME = "blacklist";

    public static function getBlackList($uid)
    {
        $db = self::get_client();;
        $db->select('buid');
        $db->from(self::TABLE_NAME);
        $db->where('uid', $uid);

        $result = $db->get()->result_array();

        return array_column($result, 'buid');
    }

    public static function appendToBlackList($uid, $buid)
    {
        $db = self::get_client();;
        $db->insert(self::TABLE_NAME, ['uid'=>$uid, 'buid'=>$buid]);

        return true;
    }

    public static function removeFromBlackList($uid, $buid)
    {
        $db = self::get_client();
        $db->delete(self::TABLE_NAME, ['uid'=>$uid, 'buid'=>$buid]);

        return true;
    }
}