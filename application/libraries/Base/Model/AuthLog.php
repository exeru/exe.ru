<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 02.02.17
 * Time: 10:59
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class AuthLog extends DB
{
    const TABLE_NAME = 'authlog';

    public static function getLogByDestination($start, $end)
    {
        $db = self::get_client();

        $query = "SELECT \"dest\", COUNT(\"id\") FROM \"authlog\" WHERE \"time\" >= '$start' AND \"time\" <= '$end' AND \"type\" = 0 GROUP BY \"dest\"";

        $result = $db->query($query);

        $forReturn = [
            0 => $result->result_array()
        ];

        $query = "SELECT \"dest\", COUNT(\"id\") FROM \"authlog\" WHERE \"time\" >= '$start' AND \"time\" <= '$end' AND \"type\" = 1 GROUP BY \"dest\"";

        $result = $db->query($query);

        $forReturn[1] = $result->result_array();

        return $forReturn;
    }

    public static function save($uid, $type, $time, $destinationId)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_NAME, [
            'uid' => $uid,
            'type' => $type,
            'time' => $time,
            'dest' => $destinationId
        ]);

        return $db->insert_id() > 0;
    }
}