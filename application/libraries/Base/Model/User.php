<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.08.15
 * Time: 17:05
 */

namespace Base\Model;

use Base\Service\Hash;

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends DB
{
    const
        TABLE_NAME = "users",
        TABLE_EXT_NAME = "users_ext",
        TABLE_SOCIAL_NAME = "users_social",
        TABLE_USER_SESSION = "user_sessions",
        TABLE_STATS_REFERER = "stats_user_referers",
        TABLE_STATS_REFERER_V2 = "stats_user_referers_v2",
        TABLE_UID_TEMPUID_REFS = "users_temporary",
        TABLE_SESSIONS = "sessions",
        TABLE_SESSION_NUMBER_BY_MAIN_MARKER = "users_session_number_by_main_marker";

    public static function getRetentionTime($uid, $time)
    {
        $db = self::get_client();

        $query = 'SELECT "time_start" FROM "sessions" WHERE
            (
              ("time_start" > '.$time.' - 2 * 86400  AND "time_start" <= '.$time.' - 86400)
              OR ("time_start" > '.$time.' - 7 * 86400  AND "time_start" <= '.$time.' - 6 * 86400)
              OR ("time_start" > '.$time.' - 30 * 86400  AND "time_start" <= '.$time.' - 29 * 86400)
            )
            AND "uid" = '.$uid.' AND "init"
            LIMIT 1
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return false;
        }

        return $result->row()->time_start;
    }

    public static function getLastPaymentTime()
    {
        $query = 'SELECT "time_start" as "time" FROM "sessions" WHERE "pay_count" > 0 ORDER BY "time_start" DESC LIMIT 1';

        $db = self::get_client();

        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getFirstPaymentTime()
    {
        $query = 'SELECT "time_start" as "time" FROM "sessions" WHERE "pay_count" > 0 ORDER BY "time_start" LIMIT 1';

        $db = self::get_client();

        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getPayersDataByPeriod($start, $end, $groupField, $isInit)
    {
        $query = 'SELECT
            "'.$groupField.'", COUNT(DISTINCT "id") as "count_payments", COUNT(DISTINCT "uid") as "count_payers"
            FROM "sessions" 
            WHERE "time_start" >= \''.$start.'\' AND "time_start" < \''.$end.'\'';

        if ($isInit === true) {
            $query .= ' AND "init"';
        } elseif ($isInit === false) {
            $query .= ' AND NOT "init"';
        }

        $query .= ' AND "pay_count" > 0
            GROUP BY "'.$groupField.'"';

        $db = self::get_client();

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getUsersMaxTime($init)
    {
        $query = 'SELECT MAX("time_start") as "time" FROM "sessions" WHERE '.($init === false ? ' NOT' : '').' "init" LIMIT 1';

        $db = self::get_client();

        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getUsersMinTime($init)
    {
        $query = 'SELECT MIN("time_start") as "time" FROM "sessions" WHERE '.($init === false ? ' NOT' : '').' "init" LIMIT 1';

        $db = self::get_client();

        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getMaxActiveTimeForUids($uids, $start, $end)
    {
        $db = self::get_client();

        $db->select('MAX("last_action") as "max"', false);
        $db->from(self::TABLE_USER_SESSION);
        $db->where_in('uid', $uids);
        $db->where(["last_action >=" => $start, "last_action < " => $end]);

        $result = $db->get()->row()->max;

        return $result;
    }

    public static function getUsersDurationByParams($start, $end, $max, $min, $fieldGroup, $init)
    {
        $db = self::get_client();

        $query = 'SELECT "'.$fieldGroup.'", COUNT(DISTINCT "marker") as "count" FROM "sessions"
            WHERE "time_start" >= \''.$start.'\' AND "time_start" < \''.$end.'\'
            AND "time_duration" >= \''.$min.'\' AND "time_duration" < \''.$max.'\'';

        if ($init === true) {
            $query .= ' AND "init"';
        } elseif ($init === false) {
            $query .= ' AND NOT "init"';
        }

        $query .= ' GROUP BY "'.$fieldGroup.'"';

        $result = $db->query($query)->result_array();

        return $result;

    }

    public static function getAdvertMinTime()
    {
        $db = self::get_client();

        $query = 'SELECT MIN("time_start") as "min" FROM "sessions"';

        $result = $db->query($query)->row()->min;

        return $result;
    }

    public static function getAdvertMaxTime()
    {
        $db = self::get_client();

        $query = 'SELECT MAX("time_start") as "max" FROM "sessions"';

        $result = $db->query($query)->row()->max;

        return $result;
    }

    public static function getAdvertGroupsByPeriod($start, $end)
    {
        $db = self::get_client();

        $query = 'SELECT DISTINCT "adv_service" FROM "sessions" WHERE "time_start" >= \''.$start.'\' AND "time_start" < \''.$end.'\' ORDER BY "adv_service"';

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getAdvertByGroupsByPeriod($start, $end, $fieldGroup)
    {
        $db = self::get_client();

        $query = 'SELECT "adv_service", "'. $fieldGroup.'", COUNT("id") as "count" FROM "sessions" WHERE "time_start" >= \''.$start.'\' AND "time_start" < \''.$end.'\' GROUP BY "adv_service", "'. $fieldGroup. '"';

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function pushSession($data)
    {
        $table = self::TABLE_SESSIONS . '_' . date('Ymd', $data['time_start']);

        $db = self::get_client();

        $query = 'SELECT COUNT("id") as "count" FROM "sessions" WHERE "id" = '.$data['id'].' AND "time_start" = '.$data['time_start'].' LIMIT 1';
        $count = $db->query($query)->row()->count;

        if (!isset($_SERVER['HTTP_HOST']) || empty($_SERVER['HTTP_HOST'])) {
            if ($count == 0) {
                echo "Model\\pushSession: ok, insert id " . $data['id'] . "\n";
            } else {
                echo "Model\\pushSession: error, id " . $data['id'] . "\n";
                $query = 'SELECT * FROM "sessions"  WHERE "id" = '.$data['id'].' AND "time_start" = '.$data['time_start'].' LIMIT 1';
                $result = $db->query($query)->result_array();
                echo "Model\\pushSession: old data: \n";
                var_export($result[0]);
                echo "Model\\pushSession: new data: \n";
                var_export($data);
            }
        }

        if ($count == 0) {
            $db->insert($table, $data);
        } else {
            $id = $data['id'];
            unset($data['id']);

            $db->update($table, $data, ['id' => $id]);
        }

        return $db->affected_rows() == 1;
    }

    public static function getUidByTempUid($uid)
    {
        $db = self::get_client();

        $db->select(['uid']);
        $db->from(self::TABLE_UID_TEMPUID_REFS);
        $db->where(['tempuid' => $uid]);
        $db->limit(1);

        $result = $db->get();

        if ($result->num_rows() < 1) {
            return false;
        }

        return $result->row()->uid;
    }

    public static function assignMarkerWithLFId($marker, $uid)
    {
        $db = self::get_client();

        $db->update(self::TABLE_STATS_REFERER_V2, ['lf_uid' => $uid], ['marker' => $marker]);
    }

    public static function getReferByMarker($marker)
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::TABLE_STATS_REFERER_V2);
        $db->where(['marker' => $marker]);
        $db->limit(1);

        return $db->get()->row_array();
    }

    public static function getLFIdByMarker($marker)
    {
        $db = self::get_client();

        $db->select(['lf_uid', 'start']);
        $db->from(self::TABLE_STATS_REFERER_V2);
        $db->where(['marker' => $marker]);
        $db->where(['lf_uid > ' => 0]);
        $db->limit(1);

        $result = $db->get()->result_array();

        if (count($result) < 1) {
            return false;
        }

        return $result;
    }

    public static function closeReferer($marker, $time)
    {
        $db = self::get_client();

        $query = '
          UPDATE
            "' . self::TABLE_STATS_REFERER_V2 . '"
          SET
            "end" = \'' . $time . '\',
            "duration" = ' . $time . ' - "start"
          WHERE
            "marker" = \'' . $marker . '\'';

        $db->query($query);

        return $db->affected_rows() == 1;
    }

    public static function pushReferer($mainMarker, $sessionNumber, $marker, $time, $point, $referer, $uid = null, $openStatData = null, $ip, $agent)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_STATS_REFERER_V2, [
            'marker' => $marker,
            'start' => $time,
            'point' => $point,
            'referer' => $referer,
            'uid' => $uid,
            'adv_service' => $openStatData['adv_service'],
            'adv_campaign' => $openStatData['adv_campaign'],
            'adv_marker' => $openStatData['adv_marker'],
            'adv_place' => $openStatData['adv_place'],
            'main_marker' => $mainMarker,
            'session_number' => $sessionNumber,
            'ip' => $ip,
            'agent' => $agent
        ]);

        return $db->insert_id() > 0;
    }

    public static function createSession($uid)
    {
        $userSessionId = md5($uid . microtime(true));

        $db = self::get_client();
        $db->insert(self::TABLE_USER_SESSION, [
            'sid' => $userSessionId,
            'uid' => $uid,
            'last_action' => time()
        ]);

        return $userSessionId;
    }

    public static function uptimeSession($lastActiveSid, $time)
    {
        $db = self::get_client();
        $db->update(self::TABLE_USER_SESSION, ['last_action' => $time], ['sid'=>$lastActiveSid]);

        return ($db->affected_rows() == 1);
    }

    public static function getSessionId($uid)
    {
        $db = self::get_client();
        $db->select('sid');
        $db->from(self::TABLE_USER_SESSION);
        $db->where(['uid'=>$uid]);
        $db->limit(1);

        $result = $db->get();

        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->row()->sid;
    }

    public static function deleteOldSessions($time)
    {
        $db = self::get_client();

        return $db->delete(self::TABLE_USER_SESSION, ['"last_action" < ' => $time]);
    }

    public static function searchUsersByName($uid, $parts, $except = [], $onlyFrom = [], $limit = 10, $offset = 0)
    { // TODO Нужна сортировка!
        $query = "
            SELECT
                \"" . self::TABLE_EXT_NAME . "\".\"uid\",
                \"name\",
                \"last_name\",
                \"photo\"
            FROM
                \"" . self::TABLE_EXT_NAME . "\"
            LEFT JOIN
                \"" . self::TABLE_NAME . "\"
            ON
                \"" . self::TABLE_EXT_NAME . "\".\"uid\" = \"" . self::TABLE_NAME . "\".\"uid\"
        ";

        if (count($except) > 0) {
            $except[] = (int) $uid;
            $query .= "
            WHERE
                \"" . self::TABLE_NAME . "\".\"uid\" NOT IN (". implode(", ", $except) .")
            ";
        } else {
            $query .= "
            WHERE
                \"" . self::TABLE_NAME . "\".\"uid\" != " . $uid . "
            ";
        }

        if (count($onlyFrom) > 0) {
            $query .= "
            AND
                \"" . self::TABLE_NAME . "\".\"uid\" IN (". implode(", ", $onlyFrom) .")
            ";
        }

        foreach($parts as $part) {
            $query .= "
            AND
                (\"" . self::TABLE_EXT_NAME . "\".\"name\" ILIKE '".addslashes($part)."%' ESCAPE '!' OR \"" . self::TABLE_EXT_NAME . "\".\"last_name\" ILIKE '".addslashes($part)."%' ESCAPE '!')
            ";
        }

        $query .= "
            ORDER BY \"users_ext\".\"rating\" DESC, \"users_ext\".\"uid\" ASC 
            LIMIT ".$limit." OFFSET ".$offset."
        ";

        $db = self::get_client();

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getUserIdBySocialId($socialId)
    {
        $db = self::get_client();
        $db->select('uid');
        $db->from(self::TABLE_SOCIAL_NAME);
        $db->where('social_id', $socialId);
        $db->limit(1);

        $result = $db->get();

        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->row()->uid;
    }

    public static function getUserIdByEmail($email)
    {
        $db = self::get_client();
        $db->select('uid');
        $db->from(self::TABLE_NAME);
        $db->where('LOWER("mail") = \'' . strtolower($email) . '\'', null, false);
        $db->limit(1);

        $result = $db->get();

        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->row()->uid;
    }

    public static function getUserIdByAuthData($email, $password)
    {
        $db = self::get_client();
        $db->select('uid');
        $db->from(self::TABLE_NAME);
        $db->where('pass', Hash::getHash($password));
        $db->where('LOWER("mail") = \'' . strtolower($email) . '\'', null, false);
        $db->limit(1);

        $result = $db->get();

        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->row()->uid;
    }

    public static function assignUidWithTempUid($uid, $tempUid)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_UID_TEMPUID_REFS, [
            'uid' => $uid,
            'tempuid' => $tempUid
        ]);
    }

    public static function assignUidWithSocialId($uid, $socialId)
    {
        $db = self::get_client();

        $db->delete(self::TABLE_SOCIAL_NAME, [
            'social_id'=>$socialId
        ]);

        $db->insert(self::TABLE_SOCIAL_NAME, [
            'social_id' => $socialId,
            'uid' => $uid
        ]);

        return true;
    }

    public static function createUser($name, $email, $password, $phone = '', $status = 1, $emailChecked = true, $lastFidId = 0)
    {
        $db = self::get_client();
        $db->insert(self::TABLE_NAME, [
            'pass' => Hash::getHash($password),
            'mail' => $email,
            'phone' => $phone,
            'status' => $status
        ]);

        $uid = $db->insert_id();

        $db->insert(self::TABLE_EXT_NAME, [
            'uid' => $uid,
            'name' => $name,
            'email_checked' => $emailChecked,
            'last_fid' => $lastFidId
        ]);

        return $uid;
    }

    public static function setUserPassword($uid, $password)
    {
        $db = self::get_client();
        $db->update(self::TABLE_NAME, ['pass' => Hash::getHash($password)], ['uid' => $uid]);

        return true;
    }

    public static function getUserInfo($uid)
    {
        $db = self::get_client();
        $db->select([
            'uid',
            'bday', 'byear',
            'country', 'country_id',
            'city', 'city_id',
            'name', 'middle_name', 'last_name',
            'photo',
            'verified',
            'sex',
            'balance',
            'email_checked',
            'last_fid',
            'rating',
            'is_bot',
            'approved',
            'subscribe',
            'is_spammer'
        ]);
        $db->from(self::TABLE_EXT_NAME);
        $db->where("uid", $uid);
        $db->limit(1);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function setUserInfo($uid, $data)
    {
        $db = self::get_client();
        $db->update(self::TABLE_EXT_NAME, $data, ['uid' => $uid]);

        return true;
    }

    public static function getUserStatus($uid)
    {
        $db = self::get_client();
        $db->select('status');
        $db->from(self::TABLE_NAME);
        $db->where('uid', $uid);
        $db->limit(1);

        return $db->get()->row()->status;
    }

    public static function setUserStatus($uid, $status)
    {
        $db = self::get_client();
        $db->update(self::TABLE_NAME, ['status' => $status], ['uid' => $uid]);

        return true;
    }

    public static function getUserEmail($uid)
    {
        $db = self::get_client();
        $db->select('mail');
        $db->from(self::TABLE_NAME);
        $db->where('uid', $uid);
        $db->limit(1);

        return $db->get()->row()->mail;
    }

    public static function setUserEmail($uid, $email)
    {
        $db = self::get_client();
        $db->update(self::TABLE_NAME, ['mail' => $email], ['uid' => $uid]);

        return true;
    }

    public static function getEmailsForAdvertising()
    {
        $query = "SELECT DISTINCT \"mail\" FROM \"users\" WHERE \"uid\" IN (SELECT \"uid\" FROM \"users_ext\" WHERE \"email_checked\" AND NOT \"is_bot\")";

        $db = self::get_client();
        $result = $db->query($query);

        return $result->result_array();
    }

    public static function getVerifiedEmails($start, $end, $gid)
    {
        $query = "SELECT DISTINCT \"mail\" FROM \"users\" WHERE \"uid\" IN (SELECT \"uid\" FROM \"users_ext\" WHERE \"email_checked\" AND NOT \"is_bot\" AND \"subscribe\")";

        if ((strlen($start) > 0 && strlen($end) > 0) || $gid > 0) {
            $sqtime = false;
            if (strlen($start) > 0 && strlen($end) > 0) {
                $start = strtotime($start . ' 00:00:00');
                $end = strtotime($end . ' 00:00:00');
                $sqtime = '"last_visit" >= \'' . $start . '\' AND "last_visit" < \'' . $end . '\'';
            }

            $sqgame = false;
            if ($gid > 0 ) {
                $sqgame = '"gid" = \'' . $gid . '\'';
            }

            $subquery = 'SELECT DISTINCT "uid" FROM "gamers" WHERE ';
            if ($sqtime && $sqgame) {
                $subquery .= $sqgame . ' AND ' . $sqtime;
            } elseif($sqtime) {
                $subquery .= $sqtime;
            } else {
                $subquery .= $sqgame;
            }

            $query .= ' AND "uid" IN ('.$subquery.')';
        }

        $db = self::get_client();
        $result = $db->query($query);

        return $result->result_array();
    }

    public static function getCountUsersBySocial()
    {
        $query = "SELECT COUNT(\"users_ext\".\"uid\"), substring(\"users_social\".\"social_id\" from '@(.+)$') as \"social\"  FROM \"users_social\", \"users_ext\" WHERE \"users_social\".\"uid\" = \"users_ext\".\"uid\" AND \"verified\" = '1' GROUP BY \"social\"";

        $db = self::get_client();
        $result = $db->query($query);

        return $result->result_array();
    }

    public static function getCountUsersNotSocial()
    {
        $query = "SELECT COUNT(\"uid\") as \"count\" FROM \"users_ext\" WHERE \"verified\" = 1 AND NOT \"is_bot\" AND \"uid\" NOT IN (SELECT \"uid\" FROM \"users_social\")";

        $db = self::get_client();
        $result = $db->query($query);

        return $result->row()->count;
    }

    public static function getCountUsers()
    {
        $query = "SELECT COUNT(\"uid\") as \"count\" FROM \"users_ext\" WHERE \"verified\" = 1 AND NOT \"is_bot\"";

        $db = self::get_client();
        $result = $db->query($query);

        return $result->row()->count;
    }

    public static function getMainMarkerByMarker($marker)
    {
        $db = self::get_client();

        $db->select(['main_marker'])
            ->from(self::TABLE_SESSION_NUMBER_BY_MAIN_MARKER)
            ->where(['marker' => $marker])
            ->limit(1);

        $result = $db->get();

        if ($result->num_rows() > 0) {
            return $result->row()->main_marker;
        }

        return false;
    }

    public static function getMarkerByMainMarker($mainMarker)
    {
        $db = self::get_client();

        $db->select(['marker'])
            ->from(self::TABLE_SESSION_NUMBER_BY_MAIN_MARKER)
            ->where(['main_marker' => $mainMarker])
            ->limit(1);

        $result = $db->get();

        if ($result->num_rows() > 0) {
            return $result->row()->marker;
        }

        return false;
    }

    public static function setMarkerByMainMarker($mainMarker, $marker)
    {
        $db = self::get_client();

        $db->update(self::TABLE_SESSION_NUMBER_BY_MAIN_MARKER, ['marker' => $marker, 'last_active_time' => time()], ['main_marker' => $mainMarker]);

        $affected = $db->affected_rows();

        if ($affected == 0) {
            $db->insert(self::TABLE_SESSION_NUMBER_BY_MAIN_MARKER, ['marker' => $marker, 'main_marker' => $mainMarker, 'last_active_time' => time()]);
            $affected = $db->affected_rows();
        }

        return $affected == 1;
    }

    public static function getLastSessionNumberByMainMarker($mainMarker)
    {
        $db = self::get_client();

        $db->select(['last_session_number'])
            ->from(self::TABLE_SESSION_NUMBER_BY_MAIN_MARKER)
            ->where(['main_marker' => $mainMarker])
            ->limit(1);

        $result = $db->get();

        if ($result->num_rows() > 0) {
            return (int) $result->row()->last_session_number;
        }

        return false;
    }

    public static function setLastSessionNumberByMainMarker($mainMarker, $sessionNumber)
    {
        $db = self::get_client();

        $db->update(self::TABLE_SESSION_NUMBER_BY_MAIN_MARKER, ['last_session_number' => $sessionNumber, 'last_active_time' => time()], ['main_marker' => $mainMarker]);

        $affected = $db->affected_rows();

        if ($affected == 0) {
            $db->insert(self::TABLE_SESSION_NUMBER_BY_MAIN_MARKER, ['last_session_number' => $sessionNumber, 'main_marker' => $mainMarker, 'last_active_time' => time()]);
            $affected = $db->affected_rows();
        }

        return $affected == 1;
    }
}