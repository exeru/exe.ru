<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29.09.15
 * Time: 15:46
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Feeds extends DB
{
    const _TABLE_FEEDS = 'feed';
    const _TABLE_APPOINTED_TIME_FEEDS = 'feeds_future';

    const _MAX_COUNT_SECONDS = 30 * 86400;

    public static function getMaxFeedId()
    {
        $query = "SELECT MAX(\"fid\") as \"max\" FROM \"feed\"";

        $db = self::get_client();

        $result = $db->query($query);

        return $result->row()->max;
    }

    public static function deleteAppointedFeed($fid, $gid)
    {
        $db = self::get_client();

        $db->update(self::_TABLE_APPOINTED_TIME_FEEDS, ['deleted'=>TRUE], ['fid'=>$fid, 'gid'=>$gid]);

        return $db->affected_rows() == 1;
    }

    public static function getAppointedFeeds($time)
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::_TABLE_APPOINTED_TIME_FEEDS);
        $db->where(['time < ' => $time, 'deleted' => FALSE]);
        $db->order_by('time', 'asc');
        $db->limit(1);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function updateAppointedFeed($fid, $gid, $updated)
    {
        $db = self::get_client();

        $db->update(self::_TABLE_APPOINTED_TIME_FEEDS, $updated, ['fid'=>$fid, 'gid'=>$gid]);

        return $db->affected_rows();
    }

    public static function searchFeeds($gid, $feedsBlackList, $search)
    {
        $db = self::get_client();

        $db->select();  // TODO Перечислить поля
        $db->from(self::_TABLE_FEEDS);

        if (count($feedsBlackList) > 0) {
            $db->where_not_in('fid', $feedsBlackList);
        }

        $db->where(['gid' => $gid, 'deleted' => FALSE]);
        $db->group_start();
        $db->like('title', $search, true);
        $db->or_like('content', $search, true);
        $db->group_end();

        $db->order_by('fid', 'desc');

        return $db->get()->result_array();
    }

    public static function updateGameFeed($fid, $gid, $updated)
    {
        $db = self::get_client();

        $db->update(self::_TABLE_FEEDS, $updated, ['fid'=>$fid, 'gid'=>$gid]);

        return $db->affected_rows();
    }

    public static function deleteFeed($fid, $gid)
    {
        $db = self::get_client();

        $db->update(self::_TABLE_FEEDS, ['deleted'=>TRUE], ['fid'=>$fid, 'gid'=>$gid]);

        return $db->affected_rows();
    }

    public static function getFeed($fid, $gid)
    {
        $db = self::get_client();

        $db->select(); // TODO Перечислить поля
        $db->from(self::_TABLE_FEEDS);
        $db->where(['fid' => $fid, 'gid' => $gid, 'deleted' => FALSE]);
        $db->limit(1);

        $result = $db->get();

        return $result->row_array();
    }

    public static function addPendingFeed($appointedTime, $uid, $gid, $type, $content, $fid = NULL, $image = NULL, $params = NULL, $title)
    {
        $db = self::get_client();

        return $db->insert(self::_TABLE_APPOINTED_TIME_FEEDS, [
            'type' => $type,
            'uid' => $uid,
            'gid' => $gid,
            'time' => $appointedTime,
            'content' => $content,
            'fuid' => $fid,
            'image_url' => $image,
            'params' => $params,
            'title' => $title
        ]);
    }

    public static function addFeed($uid, $gid, $type, $content, $fid = NULL, $image = NULL, $params = NULL, $title, $localImage = null)
    {
        $db = self::get_client();

        return $db->insert(self::_TABLE_FEEDS, [
            'type' => $type,
            'uid' => $uid,
            'gid' => $gid,
            'time' => time(),
            'content' => $content,
            'fuid' => $fid,
            'image_url' => $image,
            'params' => $params,
            'title' => $title,
            'local_image_url' => $localImage
        ]);
    }

    public static function getCountFeeds($gameIds, $userIds, $feedsBlackList = [], $lastFId = 0, $activeGamesId)
    {
        if (count($gameIds) < 1 && count($userIds) < 1) {
            return 0;
        }

        $db = self::get_client();

        $db->select('COUNT("fid") as "count"', false);
        $db->from(self::_TABLE_FEEDS);
        if (count($gameIds) > 0 && count($userIds) > 0) {
            $db->group_start();

            $db->group_start();
            $db->where('uid IS NULL');
            $db->where_in('gid', $gameIds);
            $db->group_end();
            $db->or_group_start();
            $db->where_in('uid', $userIds);

            if (!is_null($activeGamesId) && count($activeGamesId) >0) {
                $db->where_in('gid', $activeGamesId);
            }

            $db->group_end();

            $db->group_end();
        } elseif (count($gameIds) > 0) {
            $db->where('uid IS NULL');
            $db->where_in('gid', $gameIds);
        } else {
            $db->where_in('uid', $userIds);

            if (!is_null($activeGamesId) && count($activeGamesId) >0) {
                $db->where_in('gid', $activeGamesId);
            }
        }

        if (count($feedsBlackList) > 0) {
            $db->where_not_in('fid', $feedsBlackList);
        }

        $db->where('fid > ' . $lastFId);

        $db->where('deleted', false);

        return $db->get()->row()->count;
    }

    public static function getPendingFeeds($gid)
    {
        $db = self::get_client();

        $db->select(['fid', 'type', 'uid', 'gid', 'time', 'content', 'fuid', 'image_url', 'params', 'title']);
        $db->from(self::_TABLE_APPOINTED_TIME_FEEDS);
        $db->where(['gid'=>$gid, 'deleted' => false]);
        $db->order_by('time', 'asc');

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getPendingFeed($fid, $gid)
    {
        $db = self::get_client();

        $db->select(['fid', 'type', 'gid', 'time', 'content', 'image_url', 'params', 'title']);
        $db->from(self::_TABLE_APPOINTED_TIME_FEEDS);
        $db->where(['fid' => $fid, 'gid' => $gid, 'deleted' => FALSE]);
        $db->limit(1);

        $result = $db->get();

        return $result->row_array();
    }

    public static function getFeeds($gameIds, $userIds, $feedsBlackList = [], $limit = NULL, $offset = NULL, $where = NULL, $activeGamesId = NULL)
    {
        if (count($gameIds) < 1 && count($userIds) < 1) {
            return [];
        }

        $db = self::get_client();

        $db->select(['fid', 'type', 'uid', 'gid', 'time', 'content', 'fuid', 'image_url', 'params', 'title', 'local_image_url']);
        $db->from(self::_TABLE_FEEDS);
        if (count($gameIds) > 0 && count($userIds) > 0) {
            $db->group_start();

            $db->group_start();
            $db->where('uid IS NULL');
            $db->where_in('gid', $gameIds);
            $db->group_end();
            $db->or_group_start();
            $db->where_in('uid', $userIds);
            $db->group_end();

            $db->group_end();
        } elseif (count($gameIds) > 0) {
            $db->where('uid IS NULL');
            $db->where_in('gid', $gameIds);
        } else {
            $db->where_in('uid', $userIds);
        }

        if (!is_null($activeGamesId) && count($activeGamesId) > 0) {
            $db->group_start();
            $db->where_in('gid', $activeGamesId);
            $db->or_where('gid', NULL);
            $db->group_end();
        }

        if (count($feedsBlackList) > 0) {
            $db->where_not_in('fid', $feedsBlackList);
        }

        if (!is_null($where)) {
            $db->where($where);
        }

        $db->where([
            'deleted' => FALSE
        ]);

        $db->order_by('fid', 'DESC');

        if (!is_null($limit) && $limit > 0) {
            $db->limit($limit);
            if (!is_null($offset) && $offset > 0) {
                $db->offset($offset);
            }
        }

        $result = $db->get()->result_array();

        return $result;
    }
}