<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.12.17
 * Time: 17:10
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class WebPush extends DB
{
    const
        TABLE_NAME = 'users_subscribe',
        TABLE_CHANNELS = 'users_subscribe_channels',
        TABLE_LOG = 'users_subscribe_log',
        TABLE_STATE = 'users_subscribe_state';

    public static function getSubscribeStat()
    {
        $db = self::get_client();

        $db->select('COUNT(DISTINCT "uid") as "count", "state"', false)
            ->from(self::TABLE_STATE)
            ->group_by("state")
            ->order_by("count");

        $result = $db->get()->result_array();

        return $result;
    }

    public static function setSubscribeState($uid, $state)
    {
        $db = self::get_client();

        $db->delete(self::TABLE_STATE, ['uid' => $uid]);

        $db->insert(self::TABLE_STATE, ['uid' => $uid, 'state' => $state]);

        return true;
    }

    public static function getSubscribes($uid, $channel)
    {
        $query = 'SELECT "subscribe" FROM "' . self::TABLE_NAME . '" WHERE "enabled" AND "subscribe_id" IN (
          SELECT "subscribe_id" FROM "' . self::TABLE_CHANNELS . '" WHERE "uid" = ' . $uid . ' AND "channel" = \'' . $channel . '\'
        )';

        $db = self::get_client();

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return false;
        }

        return $result->result_array();
    }

    public static function disableChannel($uid, $channel, $subscribeId)
    {
        $db = self::get_client();

        $db->delete(self::TABLE_CHANNELS, [
            'uid' => $uid,
            'channel' => $channel,
            'subscribe_id' => $subscribeId
        ]);

        return $db->affected_rows() > 0;
    }

    public static function enableChannel($uid, $channel, $subscribeId)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_CHANNELS, [
            'uid' => $uid,
            'channel' => $channel,
            'subscribe_id' => $subscribeId
        ]);

        return $db->affected_rows() > 0;
    }

    public static function log($uid, $type, $channel, $shown, $subscribe, $subscribeId)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_LOG, [
            'time' => time(),
            'uid' => $uid,
            'type' => $type,
            'channel' => $channel,
            'shown' => $shown,
            'subscribe' => $subscribe,
            'subscribe_id' => $subscribeId
        ]);

        return $db->affected_rows() > 0;
    }

    public static function subscribe($uid, $subscribe, $subscribeId)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_NAME, [
            'uid' => $uid,
            'subscribe' => $subscribe,
            'subscribe_id' => $subscribeId
        ]);

        return $db->affected_rows() > 0;
    }

    public static function getAll()
    {
        $db = self::get_client();

        $db ->select(['uid', 'subscribe'])
            ->from(self::TABLE_NAME);

        $result = $db->get()->result_array();

        return $result;
    }
}