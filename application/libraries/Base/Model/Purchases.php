<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 20.01.16
 * Time: 13:58
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases extends DB
{
    const TABLE_PURCHASES_NAME = 'purchases';
    const TABLE_PURCHASES_LOG_NAME = 'purchases_log';

    public static function getLog($gid, $limit)
    {
        $db = self::get_client();

        $db->select(['type', 'text', 'time', 'test_mode']);
        $db->from(self::TABLE_PURCHASES_LOG_NAME);
        $db->where(['gid' => $gid]);
        $db->order_by('id', 'desc');
        $db->limit($limit);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function writeLog($gid, $type, $text, $time, $testMode)
    {
        $db = self::get_client();

        return $db->insert(self::TABLE_PURCHASES_LOG_NAME, [
            'gid'  => $gid,
            'type' => $type,
            'text' => $text,
            'time' => $time,
            'test_mode' => $testMode
        ]);
    }

    public static function create($orderId, $uid, $gid, $count, $sum, $invoiceId = NULL, $status)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_PURCHASES_NAME, [
            'purchase_id' => $orderId,
            'uid' => $uid,
            'gid' => $gid,
            'count' => $count,
            'sum' => $sum,
            'invoice_id' => $invoiceId,
            'status' => $status
        ]);
    }

    public static function update($orderId, $appOrderId = NULL, $status)
    {
        $db = self::get_client();

        $db->update(self::TABLE_PURCHASES_NAME, [
            'invoice_id' => $appOrderId,
            'status' => $status
        ], [
            'purchase_id' => $orderId
        ]);
    }

    public static function get($orderId)
    {
        $db = self::get_client();

        $db->select(['uid', 'gid', 'count', 'sum', 'status']);
        $db->from(self::TABLE_PURCHASES_NAME);
        $db->where(['purchase_id' => $orderId]);
        $db->limit(1);

        $result = $db->get()->row_array();

        return $result;
    }
}