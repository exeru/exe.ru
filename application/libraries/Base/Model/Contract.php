<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28.06.17
 * Time: 17:50
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends DB
{
    const TABLE_NAME = "contracts";
    const TABLE_TRANSACTIONS_NAME = "contract_transactions";

    public static function getTransactionsAtPeriod($cid, $period)
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::TABLE_TRANSACTIONS_NAME);
        $db->where(['cid' => $cid, 'period' => $period]);
        $db->order_by('timestamp', 'desc');

        $result = $db->get()->result_array();

        return $result;
    }

    public static function addWithdraw($cid, $gid, $uid, $count, $period)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_TRANSACTIONS_NAME, [
            'cid' => $cid,
            'gid' => $gid,
            'uid' => $uid,
            'timestamp' => time(),
            'count' => $count,
            'period' => $period
        ]);
    }

    public static function getWithdrawAtPeriodByGids($gids, $period)
    {
        $db = self::get_client();

        $db->select('"gid", SUM("count") as "sum"');
        $db->from(self::TABLE_TRANSACTIONS_NAME);
        $db->where_in('gid', $gids);
        $db->where(['period' => $period]);
        $db->group_by('gid');

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getContracts()
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::TABLE_NAME);
        $db->order_by('id', 'desc');

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getContractById($cid)
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::TABLE_NAME);
        $db->where('id', $cid);
        $db->limit(1);

        $result = $db->get()->row_array();

        return $result;
    }

    public static function add($data)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_NAME, $data);

        return $db->insert_id();
    }

    public static function setContractDataById($cid, $data)
    {
        $db = self::get_client();

        $db->update(self::TABLE_NAME, $data, ['id' => $cid]);

        return $db->affected_rows() === 1;
    }
}