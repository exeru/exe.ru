<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.08.15
 * Time: 13:48
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Dictionary extends DB
{
    const PREFIX = 'dictionary_';

    private static $allowed = [
        'country',
        'city'
    ];

    public static function getList($name, $filter = [])
    {
        if (!in_array($name, self::$allowed)) {
            return false;
        }

        $db = self::get_client();
        $db->select(['id', $name . '_name_ru']);
        $db->from(self::PREFIX . $name);

        if (count($filter) > 0) {
            $db->where($filter);
        }
        $db->order_by($name . '_name_ru', 'asc');

        $result = $db->get()->result_array();

        if (count($result) < 1) {
            return false;
        }

        $prepared = [];
        foreach($result as $item) {
            $prepared[$item['id']] = $item[$name . '_name_ru'];
        }

        return $prepared;
    }
}