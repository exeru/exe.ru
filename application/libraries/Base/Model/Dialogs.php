<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 22.10.15
 * Time: 15:35
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Dialogs extends DB
{
    const TABLE_NAME = "dialogs";

    public static function getUsersByMId($mid)
    {
        $db = self::get_client();

        $db->select(['uid', 'fid']);
        $db->from(self::TABLE_NAME);
        $db->where('did', $mid);
        $db->limit(1);

        $result = $db->get()->result_array();

        return $result[0];
    }

    public static function getCountUnReadDialogs($uid)
    {
        $db = self::get_client();

        $db->select('COUNT(DISTINCT "uid") as "count"', false);
        $db->from(self::TABLE_NAME);
        $db->where([
            'fid' => $uid,
            'deleted_by_fid' => false,
            'was_read_fid' => false
        ]);

        return $db->get()->row()->count;
    }

    public static function markAsRead($uid, $dids)
    {
        $db = self::get_client();

        $db->set(['was_read_fid' => true]);
        $db->where('fid', $uid);
        $db->where_in('did', $dids);

        return $db->update(self::TABLE_NAME);
    }

    public static function restoreMessages($uid, $mids)
    {
        $db = self::get_client();

        return $db->update(self::TABLE_NAME, ['deleted_by_uid' => false], '"uid" = ' . $uid . ' AND "did" IN ('.implode(',', $mids).')')
            && $db->update(self::TABLE_NAME, ['deleted_by_fid' => false], '"fid" = ' . $uid . ' AND "did" IN ('.implode(',', $mids).')');
    }

    public static function deleteMessages($uid, $mids)
    {
        $db = self::get_client();

        return $db->update(self::TABLE_NAME, ['deleted_by_uid' => true], '"uid" = ' . $uid . ' AND "did" IN ('.implode(',', $mids).')')
            && $db->update(self::TABLE_NAME, ['deleted_by_fid' => true], '"fid" = ' . $uid . ' AND "did" IN ('.implode(',', $mids).')');
    }

    public static function deleteDialog($uid, $fid)
    {
        $db = self::get_client();

        return (
            $db->update(self::TABLE_NAME, ['deleted_by_uid' => true], ['uid' => $uid, 'fid' => $fid])
            &&
            $db->update(self::TABLE_NAME, ['deleted_by_fid' => true], ['uid' => $fid, 'fid' => $uid])
        );
    }

    public static function sendMessage($uid, $fid, $text)
    {
        $db = self::get_client();

        $db->insert(self::TABLE_NAME, [
            'uid' => $uid,
            'fid' => $fid,
            'time' => time(),
            'text' => $text
        ]);

        return $db->insert_id();
    }

    public static function getDialog($uid, $fid)
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::TABLE_NAME);
        $db->group_start();
        $db->where(['uid'=>$uid, 'fid'=>$fid, 'deleted_by_uid'=>false]);
        $db->group_end();
        $db->or_group_start();
        $db->where(['uid'=>$fid, 'fid'=>$uid, 'deleted_by_fid'=>false]);
        $db->group_end();
        $db->order_by('did', 'ASC');
        $result = $db->get()->result_array();

        return $result;
    }

    public static function getList($uid)
    {
        $db = self::get_client();

        $db->select();
        $db->from(self::TABLE_NAME);

        $db->group_start();
        $db->where(['uid'=>$uid, 'deleted_by_uid'=>false]);
        $db->group_end();

        $db->or_group_start();
        $db->where(['fid'=>$uid, 'deleted_by_fid'=>false]);
        $db->group_end();

        $db->order_by('did', 'DESC');
        $result = $db->get()->result_array();

        return $result;
    }
}