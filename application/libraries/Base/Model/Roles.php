<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 03.03.16
 * Time: 13:41
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends DB
{
    const _TABLE_ROLES = "roles";

    public static function getUsers($gid)
    {
        $db = self::get_client();
        $db->select(['uid', 'group']);
        $db->from(self::_TABLE_ROLES);
        $db->where(['gid' => $gid]);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function flushAccess($uid, $gid)
    {
        $db = self::get_client();

        return $db->delete(self::_TABLE_ROLES, ['gid' => $gid, 'uid' => $uid]);
    }

    public static function add($gid, $uid, $group)
    {
        $db = self::get_client();

        $db->insert(self::_TABLE_ROLES, ['gid' => $gid, 'uid' => $uid, 'group' => $group]);

        return true;
    }

    public static function getGames($uid)
    {
        $db = self::get_client();

        $db->select(['gid', 'group']);
        $db->from(self::_TABLE_ROLES);
        $db->where(['uid' => $uid]);

        $result = $db->get()->result_array();

        return $result;
    }
}