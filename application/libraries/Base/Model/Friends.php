<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.09.15
 * Time: 14:19
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Модель доступа к данным, которые относятся к друзьям и уведомлениям от друзей
 *
 * @package Base\Model
 */
class Friends extends DB
{
    /**
     * Название таблицы отношений между пользователями
     *
     * @access internal
     */
    const _TABLE_FRIENDS = 'friends_new_v2';

    /**
     * Название таблицы приглашенией в игры от пользователей
     *
     * @access internal
     */
    const _TABLE_NOTIFICATIONS = 'friends_notifications';


    /** Работа с черным списком */

    /**
     * Возвращает идентификаторы пользователей, которые были заблокированы
     *
     * @param int $uid Идентификатор пользователя
     * @param int $offset Смещение относительно начала списка. По умолчанию 0
     * @param int $limit Количество, которое необходимо вернуть. По умолчанию 1000
     * @param string $order Название колонки сортировки. По умолчанию fid (по идентификатору)
     * @param string $orderDesc Направление сортировки. по умолчанию ASC (по возрастанию)
     * @return array набор ассоциативных массивов вида [fid]
     */
    public static function getBlackList($uid, $offset = 0, $limit = 1000, $order = 'fid', $orderDesc = 'asc')
    {
        return self::getFriendsByParams(
            ['fid'],
            ['uid'=>$uid, 'block' => true],
            [$order, $orderDesc],
            [$offset, $limit]
        );
    }

    /**
     * Помещает пользователя в черный список другого пользователя
     *
     * У первого проставляется флаг блокировки.
     * У второго ставится флаг, что он заблокирован другим пользователем.
     *
     * @param int $uid Идентификатор пользователя, который помещает
     * @param int $fid Идентификатор пользователя, которого помещают
     *
     * @return true в случае успеха
     */
    public static function toBlackList($uid, $fid)
    {
        $db = self::get_client();

        $db->trans_begin();

        // Пробуем обновить первую запись
        $db->update(self::_TABLE_FRIENDS, ['block' => true], ['uid' => $uid, 'fid' => $fid]);
        if ($db->affected_rows() != 1) {
            // Записи не было, добавляем
            $db->insert(self::_TABLE_FRIENDS, ['uid' => $uid, 'fid' => $fid, 'block' => true, 'auto_created' => true]);
        }

        // Пробуем обновить вторую
        $db->update(self::_TABLE_FRIENDS, ['blocked' => true], ['uid' => $fid, 'fid' => $uid]);
        if ($db->affected_rows() != 1) {
            // Записи не было, добавляем
            $db->insert(self::_TABLE_FRIENDS, ['uid' => $fid, 'fid' => $uid, 'blocked' => true, 'auto_created' => true]);
        }

        $db->trans_commit();

        return true;
    }

    /**
     * Удаляет пользователя из черного списка другого пользователя
     *
     * У первого снимается флаг блокировки.
     * У второго снимается флаг, что он заблокирован другим пользователем.
     *
     * @param int $uid Идентификатор пользователя, из черного списка которого удаляется другой пользователь
     * @param int $fid Идентификатор пользователя, которого удаляют из черного списка
     *
     * @return true в случае успеха
     */
    public static function fromBlackList($uid, $fid)
    {
        $db = self::get_client();

        $db->trans_begin();
        // Если у обоих пользователей состояние заявок исходное, грохнем обе
        $db->select(['uid', 'fid']);
        $db->from(self::_TABLE_FRIENDS);
        $db->group_start();
        $db->where(['uid' => $uid, 'fid' => $fid, 'rejected' => false, 'is_friends' => false, 'block' => true, 'blocked' => false, 'auto_created' => true]);
        $db->group_end();
        $db->or_group_start();
        $db->where(['uid' => $fid, 'fid' => $uid, 'rejected' => false, 'is_friends' => false, 'block' => false, 'blocked' => true, 'auto_created' => true]);
        $db->group_end();
        $result = $db->get();

        $count = $result->num_rows();

        if ($count == 2) {
            $db->delete(self::_TABLE_FRIENDS, ['uid' => $uid, 'fid' => $fid, 'rejected' => false, 'is_friends' => false, 'block' => true, 'blocked' => false, 'auto_created' => true]);
            $db->delete(self::_TABLE_FRIENDS, ['uid' => $fid, 'fid' => $uid, 'rejected' => false, 'is_friends' => false, 'block' => false, 'blocked' => true, 'auto_created' => true]);

            $db->trans_commit();
            return true;

        } else if ($count == 1) {
            $db->delete(self::_TABLE_FRIENDS, ['uid' => $uid, 'fid' => $fid, 'rejected' => false, 'is_friends' => false, 'block' => true, 'blocked' => false, 'auto_created' => true]);
            if ($db->affected_rows() == 1) {
                $db->update(self::_TABLE_FRIENDS, ['blocked' => false], ['uid' => $fid, 'fid' => $uid]);
                if ($db->affected_rows() == 1) {
                    $db->trans_commit();
                    return true;
                }
            } else {
                $db->delete(self::_TABLE_FRIENDS, ['uid' => $fid, 'fid' => $uid, 'rejected' => false, 'is_friends' => false, 'block' => false, 'blocked' => true, 'auto_created' => true]);
                if ($db->affected_rows() == 1) {
                    $db->update(self::_TABLE_FRIENDS, ['block' => false], ['uid' => $uid, 'fid' => $fid]);
                    if ($db->affected_rows() == 1) {
                        $db->trans_commit();
                        return true;
                    }
                }

            }

            $db->trans_rollback();
            return false;
        }

        // Пробуем обновить первую запись
        $db->update(self::_TABLE_FRIENDS, ['block' => false], ['uid' => $uid, 'fid' => $fid]);
        if ($db->affected_rows() != 1) {
            // Записи не было, такое по идее невозможно
            $db->trans_rollback();

            return false;
        }

        // Пробуем обновить вторую
        $db->update(self::_TABLE_FRIENDS, ['blocked' => false], ['uid' => $fid, 'fid' => $uid]);
        if ($db->affected_rows() != 1) {
            // Записи не было, такое по идее невозможно
            $db->trans_rollback();

            return false;
        }

        $db->trans_commit();

        return true;
    }


    /** Массивы идентификаторов пользователей */

    /**
     * Возвращает идентификаторы пользователей, которым пользователь отправил приглашение дружить.
     *
     * Также возращается идентификатор игры, даже если приглашение с игрой не связано.
     * Идентификаторы возвращаются даже если другой пользователь уже отклонил запрос.
     * Пользователи не были отправлены в блокировку этим пользователем.
     *
     * @param int $uid Идентификатор пользователя
     * @param int $offset Смещение относительно начала списка. По умолчанию 0
     * @param int $limit Количество, которое необходимо вернуть. По умолчанию 1000
     * @param string $order Название колонки сортировки. По умолчанию fid (по идентификатору)
     * @param string $orderDesc Направление сортировки. по умолчанию ASC (по возрастанию)
     * @return array набор ассоциативных массивов вида [fid, gid]
     */
    public static function getFriendsI($uid, $offset = 0, $limit = 1000, $order = 'fid', $orderDesc = 'asc')
    {
        $query = '
            SELECT "t1".* FROM (
              SELECT
                "fid",
                "gid"
              FROM
                "' . self::_TABLE_FRIENDS . '"
              WHERE 
                "uid" = \''.$uid.'\'
                AND NOT "is_friends"
                AND NOT "block"
                AND NOT "blocked"
              ) AS "t1" 
            INNER JOIN (
              SELECT
                "uid",
                "rating"
              FROM
              "users_ext"
            ) AS "t2"
            ON "t1"."fid" = "t2"."uid"
            ORDER BY 
              "rating" DESC,
              "' . $order . '" ' . $orderDesc . '
            LIMIT ' . $limit . ($offset > 0 ? ' OFFSET ' . $offset : '');

        $db = self::get_client();

        $result = $db->query($query)->result_array();

        return $result;
    }

    /**
     * Возвращает идентификаторы пользователей, которые отправили данному пользователю приглашение дружить.
     *
     * Также возращается идентификатор игры, даже если приглашение с игрой не связано.
     * Исключаются идентификаторы тех пользователей, заявки которых уже были отклонены.
     * Пользователи не были отправлены в блокировку этим пользователем.
     *
     * @param int $uid Идентификатор пользователя
     * @param int $offset Смещение относительно начала списка. По умолчанию 0
     * @param int $limit Количество, которое необходимо вернуть. По умолчанию 1000
     * @param string $order Название колонки сортировки. По умолчанию uid (по идентификатору другого пользователя)
     * @param string $orderDesc Направление сортировки. по умолчанию ASC (по возрастанию)
     * @return array набор ассоциативных массивов вида [uid, gid]
     */
    public static function getFriendsMe($uid, $offset = 0, $limit = 1000, $order = 'uid', $orderDesc = 'asc')
    {
        $query = '
            SELECT "t1".* FROM (
              SELECT
                "uid",
                "gid"
              FROM
                "' . self::_TABLE_FRIENDS . '"
              WHERE 
                "fid" = \''.$uid.'\'
                AND NOT "rejected"
                AND NOT "is_friends"
                AND NOT "block"
                AND NOT "blocked"
              ) AS "t1" 
            INNER JOIN (
              SELECT
                "uid",
                "rating"
              FROM
              "users_ext"
            ) AS "t2"
            ON "t1"."uid" = "t2"."uid"
            ORDER BY 
              "rating" DESC,
              "' . $order . '" ' . $orderDesc . '
            LIMIT ' . $limit . ($offset > 0 ? ' OFFSET ' . $offset : '');

        $db = self::get_client();

        $result = $db->query($query)->result_array();

        return $result;
    }

    /**
     * Возвращает идентификаторы пользователей, которые дружат с данным пользователем.
     *
     * Также возращается идентификатор игры, даже если приглашение с игрой не связано.
     * Отклоненность заявки считается невозможной, поэтому игнорируется.
     * Пользователи не были отправлены в блокировку этим пользователем.
     *
     * @param int $uid Идентификатор пользователя
     * @param int $offset Смещение относительно начала списка. По умолчанию 0
     * @param int $limit Количество, которое необходимо вернуть. По умолчанию 1000
     * @param string $order Название колонки сортировки. По умолчанию fid (по идентификатору)
     * @param string $orderDesc Направление сортировки. по умолчанию ASC (по возрастанию)
     * @return array набор ассоциативных массивов вида [fid, gid]
     */
    public static function getFriendsBoth($uid, $offset = 0, $limit = 1000, $order = 'fid', $orderDesc = 'ASC')
    {
        $query = '
            SELECT "t1".* FROM (
              SELECT
                "fid",
                "gid"
              FROM
                "' . self::_TABLE_FRIENDS . '"
              WHERE 
                "uid" = \''.$uid.'\'
                AND "is_friends"
                AND NOT "block"
                AND NOT "blocked"
              ) AS "t1" 
            INNER JOIN (
              SELECT
                "uid",
                "rating"
              FROM
              "users_ext"
            ) AS "t2"
            ON "t1"."fid" = "t2"."uid"
            ORDER BY 
              "rating" DESC,
              "' . $order . '" ' . $orderDesc . '
            LIMIT ' . $limit . ($offset > 0 ? ' OFFSET ' . $offset : '');

        $db = self::get_client();

        $result = $db->query($query)->result_array();

        return $result;
    }

    /**
     * Возвращает идентификаторы пользователей, заявки которых были отклонены
     *
     * Также возращается идентификатор игры, даже если приглашение с игрой не связано.
     * Пользователи не были отправлены в блокировку этим пользователем.
     *
     * @param int $uid Идентификатор пользователя
     * @param int $offset Смещение относительно начала списка. По умолчанию 0
     * @param int $limit Количество, которое необходимо вернуть. По умолчанию 1000
     * @param string $order Название колонки сортировки. По умолчанию fid (по идентификатору)
     * @param string $orderDesc Направление сортировки. по умолчанию ASC (по возрастанию)
     * @return array набор ассоциативных массивов вида [fid, gid]
     */
    public static function getFriendsRejected($uid, $offset = 0, $limit = 1000, $order = 'fid', $orderDesc = 'asc')
    {
        return self::getFriendsByParams(
            ['fid', 'gid'],
            ['fid'=> $uid, 'rejected' => true, 'block' => false, 'blocked' => false],
            [$order, $orderDesc],
            [$offset, $limit]
        );
    }

    /**
     * Вспомогательная функция для получения наборов идентификаторов пользователей.
     *
     * @internal
     *
     * @param array $columns названия колонок, которые необходимо выбрать
     * @param array $where ассоциативный массив с условиями выборки
     * @param array $order массив с указанием сортировки [колонка, направление]
     * @param array $limit массив с указанием смещения и количества [смещение, количество]
     * @return array наборы ассоциативных массивов с выбранными колонками
     */
    private static function getFriendsByParams($columns, $where, $order, $limit)
    {
        $db = self::get_client();

        $db->select($columns);
        $db->from(self::_TABLE_FRIENDS);
        $db->where($where);
        $db->order_by($order[0], $order[1]);
        $db->limit($limit[1], $limit[0]);

        $result = $db->get()->result_array();

        return $result;
    }


    /** Количества различных групп пользователей */

    /**
     * Возвращает количество пользователей, которым пользователь отправил приглашение дружить.
     *
     * Пользователи, которые уже отклонили запрос, также учитываются.
     * Пользователи не были отправлены в блокировку этим пользователем.
     *
     * @param integer $uid идентификатор пользователя
     * @return integer
     */
    public static function getFriendsICount($uid)
    {
        return self::getFriendsCountByParams(["uid"=>$uid, "is_friends"=>false, "block"=>false, "blocked"=>false]);
    }

    /**
     * Возвращает количество пользователей, которые отправили пользователю приглашение дружить.
     *
     * Отклоненные заявки не учитываются.
     * Пользователи не были отправлены в блокировку этим пользователем.
     *
     * @param integer $uid идентификатор пользователя
     * @return integer
     */
    public static function getFriendsMeCount($uid)
    {
        return self::getFriendsCountByParams(["fid"=>$uid, "rejected"=>false, "is_friends"=>false, "block"=>false, "blocked"=>false]);
    }

    /**
     * Возвращает количество пользователей, которые дружат с данным пользователем.
     *
     * Отклоненность заявки считается невозможной, поэтому игнорируется.
     * Пользователи не были отправлены в блокировку этим пользователем.
     *
     * @param integer $uid идентификатор пользователя
     * @return integer
     */
    public static function getFriendsBothCount($uid)
    {
        return self::getFriendsCountByParams(["uid"=>$uid, "is_friends"=>true, "block"=>false, "blocked"=>false]);
    }

    /**
     * Вспомогательная функция для получения количества пользователей по заданным условиям.
     *
     * @internal
     *
     * @param array $columns названия колонок, которые необходимо выбрать
     * @param array $where ассоциативный массив с условиями выборки
     * @param array $order массив с указанием сортировки [колонка, направление]
     * @param array $limit массив с указанием смещения и количества [смещение, количество]
     * @return array наборы ассоциативных массивов с выбранными колонками
     */
    private static function getFriendsCountByParams($where)
    {
        $db = self::get_client();

        $db->select("COUNT(fid) as count");
        $db->from(self::_TABLE_FRIENDS);
        $db->where($where);

        return $db->get()->row()->count;
    }


    /** Запросы на добавление и удаление друзей */

    /**
     * Добавляет запрос в друзья.
     *
     * Если есть встречный запрос, у обоих пользователей будет выставлен признак дружбы
     *
     * @param integer $uid идентификатор пользователя
     * @param integer $fid идентификатор второго пользователя
     * @param null|integer $gid идентификатор игры, если есть
     * @return boolean true в случае успеха
     */
    public static function appendFriend($uid, $fid, $gid = null)
    {
        $db = self::get_client();

        //TODO Внимательно посмотреть все точки захода!

        $db->update(self::_TABLE_FRIENDS, [
            'rejected' => false,
            'is_friends' => true
        ], [
            'uid' => $fid,
            'fid' => $uid,
            'block' => false
        ]);

        $affectedRows = $db->affected_rows();

        if ($affectedRows == 1 && $gid < 1) {
            $db->select(['gid']);
            $db->from(self::_TABLE_FRIENDS);
            $db->where(['uid' => $fid, 'fid' => $uid,]);
            $gid = $db->get()->row()->gid;
        }

        return $db->insert(self::_TABLE_FRIENDS, [
            'uid' => $uid,
            'fid' => $fid,
            'rejected' => false,
            'is_friends' => ($affectedRows == 1 ? true : false),
            'block' => false,
            'gid' => $gid > 0 ? $gid : 0]);
    }

    /**
     * Удаляет собственную заявку в друзья.
     *
     * Если есть встречный запрос, снимает флаг дружбы. Снимает признак отклоненности заявки у другого пользователя
     *
     * @param integer $uid идентификатор пользователя
     * @param integer $fid идентификатор второго пользователя
     * @return boolean true в случае успеха
     */
    public static function deleteFriend($uid, $fid)
    {
        $db = self::get_client();

        $db->delete(self::_TABLE_FRIENDS, ['uid'=>$uid, 'fid'=>$fid]);

        return $db->update(self::_TABLE_FRIENDS, ['rejected' => false, 'is_friends' => false], ['uid' => $fid, 'fid'=>$uid]);
    }

    /**
     * Маркирует заявку другого пользователя как отклоненную.
     *
     * @param integer $uid идентификатор пользователя
     * @param integer $fid идентификатор второго пользователя
     * @return boolean true в случае успеха
     */
    public static function rejectFriend($uid, $fid)
    {
        $db = self::get_client();

        return $db->update(self::_TABLE_FRIENDS, ['rejected' => true, 'is_friends' => false], ['uid' => $fid, 'fid'=>$uid]);
    }


    /** Уведомления */

    /**
     * Возвращает идентификатор отправителя по номеру уведомления
     *
     * @param integer $nid идентификатор уведомления
     * @return integer идентификатор пользователя
     */
    public static function getFIdByNId($nid)
    {
        $db = self::get_client();

        $db->select(['fid']);
        $db->from(self::_TABLE_NOTIFICATIONS);
        $db->where(['nid'=>$nid]);
        $db->limit(1);

        return $db->get()->row()->fid;
    }

    /**
     * Добавляет уведомления от пользователя другим
     *
     * @param array integer $uids идентификаторы пользователей, которым необходимо отправить уведомления
     * @param integer $fid идентификатор пользователя, от которого отправляются уведомления
     * @param integer $gid идентификатор игры, о которой идет речь
     * @param integer $time timestamp создания уведомления
     * @param string $content текст уведомления
     * @param null|string $params дополнительная строка, будет передана в iframe при переходе по ссылки на игру
     * @param string $requestType тип запроса (request|invite) - запрос помощи или приглашение в игру.
     * @return boolean true в случае успеха
     */
    public static function appendNotification($uids, $fid, $gid, $time, $content, $params = NULL, $requestType = 'request')
    {
        $db = self::get_client();

        $data = [];
        foreach($uids as $uid) {
            $data[] = [
                'uid' => $uid,
                'fid' => $fid,
                'gid' => $gid,
                'time' => $time,
                'content' => $content,
                'params' => $params,
                'request_type' => $requestType
            ];
        }

        return $db->insert_batch(self::_TABLE_NOTIFICATIONS, $data);
    }

    /**
     * Возвращает уведомления для данного пользователя от других
     *
     * @param integer $uid идентификатор пользователя
     * @param array $blacklist массив идентификаторов пользователей, уведомления от которых должны быть исключены
     * @return array набор ассоциативных массивов ['fid', 'gid', 'content', 'nid', 'params', 'request_type']
     */
    public static function getNotifications($uid, $blacklist)
    {
        $db = self::get_client();

        $db->select(['fid', 'gid', 'content', 'nid', 'params', 'request_type']);
        $db->from(self::_TABLE_NOTIFICATIONS);
        $db->where(['uid'=>$uid]);

        if (count($blacklist) > 0) {
            $db->where_not_in('fid', $blacklist);
        }

        $result = $db->get()->result_array();

        return $result;
    }

    /**
     * Удаляет уведомление.
     *
     * Если указан идентификатор уведомления, поиск будет осуществлен по нему.
     * В противном случае будут удалены все сообщения с комбинацией $uid, $fid, $gid
     *
     * @param integer $uid идентификатор получателя
     * @param integer $fid идентификатор отправителя
     * @param integer $gid идентификатор игры
     * @param integer $nid идентификатор сообщения
     * @return boolean true в случае успеха
     */
    public static function deleteNotification($uid, $fid, $gid, $nid)
    {
        $db = self::get_client();

        if ($nid > 0) {
            $params = [
                'nid' => $nid
            ];
        } else {
            $params = [
                'uid' => $uid,
                'fid' => $fid,
                'gid' => $gid
            ];
        }

        return $db->delete(self::_TABLE_NOTIFICATIONS, $params);
    }


    /** deprecated */

    /**
     * @deprecated use getFriendsI
     */
    public static function getFriendsAsI($uid, $blacklist)
    {
        $db = self::get_client();

        $db->select(['fid', 'gid']);
        $db->from(self::_TABLE_FRIENDS);
        $db->where(['uid'=>$uid]);

        if (count($blacklist) > 0) {
            $db->where_not_in('fid', $blacklist);
        }

        $result = $db->get()->result_array();

        $friends = [];
        foreach($result as $row) {
            $friends[$row['fid']] = is_null($row['gid']) ? 0 : $row['gid'];
        }

        return $friends;
    }

    /**
     * @deprecated use getFriendsMe
     */
    public static function getFriendsAsMe($uid, $blacklist)
    {
        $db = self::get_client();

        $db->select(['uid', 'gid']);
        $db->from(self::_TABLE_FRIENDS);
        $db->where(['fid'=>$uid]);

        if (count($blacklist) > 0) {
            $db->where_not_in('uid', $blacklist);
        }

        $result = $db->get()->result_array();;

        $friends = [];
        foreach($result as $row) {
            $friends[$row['uid']] = is_null($row['gid']) ? 0 : $row['gid'];
        }

        return $friends;
    }
}