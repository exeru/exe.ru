<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.04.16
 * Time: 16:13
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Stats extends DB
{
    const TABLE_CRUDE_NAME = 'stats_crude';
    const TABLE_MONTHS_NAME = 'stats_by_months';
    const TABLE_USER_SESSIONS = 'stats_user_sessions';

    public static function insertCrude($data)
    {
        $db = self::get_client();

        return $db->insert_batch(self::TABLE_CRUDE_NAME, $data);
    }

    public static function insertVisitsByMonths($gid, $monthNumber, $actual)
    {
        $db = self::get_client();

        $data = array_merge(['gid' => $gid, 'month_number' => $monthNumber], $actual);

        return $db->insert(self::TABLE_MONTHS_NAME, $data);
    }

    public static function updateVisitsByMonths($gid, $monthNumber, $actual)
    {
        $db = self::get_client();

        return $db->update(self::TABLE_MONTHS_NAME, $actual, ['gid' => $gid, 'month_number' => $monthNumber]);
    }

    public static function getCityFromInterval($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $result = $db->query('
            SELECT
              COUNT(DISTINCT "uid") as "unique",
              "city_id"
            FROM
              "' . self::TABLE_CRUDE_NAME . '"
            WHERE
              "gid"=' . $gid . '
            AND
              "day_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
            AND
              "city_id" IS NOT NULL
            GROUP BY
              "city_id"
            ORDER BY
              "unique" DESC
        ');

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getCountryFromInterval($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $result = $db->query('
            SELECT
              COUNT(DISTINCT "uid") as "unique",
              "country_id"
            FROM
              "' . self::TABLE_CRUDE_NAME . '"
            WHERE
              "gid"=' . $gid . '
            AND
              "day_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
            AND
              "country_id" IS NOT NULL
            GROUP BY
              "country_id"
            ORDER BY
              "unique" DESC
        ');

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getAgeSexFromInterval($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $result = $db->query('
            SELECT
              COUNT(DISTINCT "uid") as "unique",
              "sex",
              "age_group"
            FROM
              "' . self::TABLE_CRUDE_NAME . '"
            WHERE
              "gid" = ' .$gid. '
            AND
              "day_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
            AND
              "sex" IS NOT NULL
            AND
              "age_group" IS NOT NULL
            GROUP BY
              "age_group", "sex"
        ');

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getVisitsFromMonthInterval($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $result = $db->query('
          SELECT
            "month_number",
            "unique",
            "nounique"
          FROM
            "' . self::TABLE_MONTHS_NAME . '"
          WHERE
            "gid" = ' . $gid . '
          AND
            "month_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
        ');

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getVisitsFromIntervalSummary($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $result = $db->query('
          SELECT
            COUNT(DISTINCT "uid") as "unique",
            COUNT("uid") as "nounique"
          FROM
            "' . self::TABLE_CRUDE_NAME . '"
          WHERE
            "gid"=' . $gid . '
          AND
            "day_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
        ');

        return $result->row_array();
    }

    public static function getVisitsFromInterval($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $result = $db->query('
          SELECT
            COUNT(DISTINCT "uid") as "unique",
            COUNT("uid") as "nounique",
            "day_number"
          FROM
            "' . self::TABLE_CRUDE_NAME . '"
          WHERE
            "gid" = ' . $gid . '
          AND "day_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
          GROUP BY "day_number"
        ');

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

/*    public static function appendUserSession($uid, $number, $start, $end, $duration)
    {
        $db = self::get_client();

        return $db->insert(self::TABLE_USER_SESSIONS, [
            'uid' => (int) $uid,
            't_start' => (int) $start,
            't_end' => (int) $end,
            'duration' => (int) $duration,
            'number' => (int) $number
        ]);
    }*/

/*    public static function updateUserSession($uid, $number, $start, $end, $duration)
    {
        $db = self::get_client();

        return $db->update(self::TABLE_USER_SESSIONS, [
            't_start' => (int) $start,
            't_end' => (int) $end,
            'duration' => (int) $duration
        ],  [
            'uid' => (int) $uid,
            'number' => (int) $number
        ]);
    }*/
}