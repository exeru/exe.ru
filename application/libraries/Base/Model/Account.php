<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 20.01.16
 * Time: 11:40
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends DB
{
    const TABLE_BALANCE_NAME = 'users_ext';
    const TABLE_BALANCE_LOG_NAME = 'balance_log';
    const TABLE_BALANCE_GAME_NAME = 'games_new';

    public static function getMinPaymentTime()
    {
        $query = 'SELECT "time" FROM "'.self::TABLE_BALANCE_LOG_NAME.'" WHERE "type" = 0 ORDER BY "time" LIMIT 1';

        $db = self::get_client();
        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getMaxPaymentTime()
    {
        $query = 'SELECT "time" FROM "'.self::TABLE_BALANCE_LOG_NAME.'" WHERE "type" = 0 ORDER BY "time" DESC LIMIT 1';

        $db = self::get_client();
        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getPaySystemsByPeriod($start, $end, $groupField)
    {
        $db = self::get_client();

        $query = 'SELECT "'.$groupField.'", "receiver", SUM("count") as "sum"
            FROM "'.self::TABLE_BALANCE_LOG_NAME.'"
            WHERE "time" >= \''.$start.'\' AND "time" < \''.$end.'\' AND "type" = 0
            GROUP BY "'.$groupField.'", "receiver"';

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getPurchasesMaxTime()
    {
        $db = self::get_client();

        $query = 'SELECT MAX("time") as "time" FROM "'.self::TABLE_BALANCE_LOG_NAME.'"';

        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getPurchasesMinTime()
    {
        $db = self::get_client();

        $query = 'SELECT MIN("time") as "time" FROM "'.self::TABLE_BALANCE_LOG_NAME.'"';

        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            return $result->row()->time;
        }

        return false;
    }

    public static function getPurchesesSumByGidsByPeriod($gids, $start, $end, $fieldGroup)
    {
        $db = self::get_client();

        $query = 'SELECT "'.$fieldGroup.'", "receiver" as "gid", SUM("count") as "sum"
                  FROM "'.self::TABLE_BALANCE_LOG_NAME.'"
                  WHERE "time" >= \''.$start.'\' AND "time" < \''.$end.'\' AND "type" = 1 AND "receiver" IN ('.implode(', ', $gids).') 
                  GROUP BY "'.$fieldGroup.'", "receiver"';

        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getLog($uid, $limit = 50)
    {
        $db = self::get_client();

        $db->select(['time', 'type', 'receiver', 'count']);
        $db->from(self::TABLE_BALANCE_LOG_NAME);
        $db->where(['uid' => $uid]);
        $db->order_by('time', 'DESC');
        $db->where_in('type', [\Base\Service\Account::increment, \Base\Service\Account::decrement]);
        $db->order_by('receiver', 'DESC');
        $db->limit($limit);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getLogByReceiver($gidOrPid, $limit = 50)
    {
        $db = self::get_client();

        $db->select(['uid', 'time', 'type', 'receiver', 'count']);
        $db->from(self::TABLE_BALANCE_LOG_NAME);
        $db->where_in('type', [\Base\Service\Account::decrement, \Base\Service\Account::decrementGame]);
        $db->where(['receiver' => $gidOrPid]);
        $db->where('"count" IS NOT NULL', null, false);
        $db->order_by('time', 'DESC');
        $db->limit($limit);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function appendLog($uid, $sum, $type, $gidOrPid, $count)
    {
        $db = self::get_client();

        $time = time();

        $db->insert(self::TABLE_BALANCE_LOG_NAME, [
            'uid' => $uid,
            'time' => $time,
            'value' => $sum,
            'type' => $type,
            'receiver' => $gidOrPid,
            'count' => $count,
            'yyyymm' => date('Ym'),
            'yyyymmdd' => date('Ymd'),
            'yyyymmddhh' => date('YmdH')
        ]);
    }

    public static function incrementBalance($uid, $count)
    {
        $db = self::get_client();

        $query = 'UPDATE "' . self::TABLE_BALANCE_NAME . '" SET "balance" = "balance" + ' . $count . ' WHERE "uid" = ' . $uid;
        $db->query($query);
    }

    public static function decrementBalance($uid, $count)
    {
        $db = self::get_client();

        $query = 'UPDATE "' . self::TABLE_BALANCE_NAME . '" SET "balance" = "balance" - ' . $count . ' WHERE "uid" = ' . $uid;
        $db->query($query);
    }

    public static function incrementGameBalance($gid, $count)
    {
        $db = self::get_client();

        $query = 'UPDATE "' . self::TABLE_BALANCE_GAME_NAME . '" SET "balance" = "balance" + ' . $count . ' WHERE "gid" = ' . $gid;
        $db->query($query);
    }

    public static function decrementGameBalance($gid, $count)
    {
        $db = self::get_client();

        $query = 'UPDATE "' . self::TABLE_BALANCE_GAME_NAME . '" SET "balance" = "balance" - ' . $count . ' WHERE "gid" = ' . $gid;
        $db->query($query);
    }
}