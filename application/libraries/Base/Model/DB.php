<?php
/**
 * User: ruslan
 * Date: 27.11.15
 * Time: 12:37
 */

namespace Base\Model;

/**
 * Standalone класс работы с БД
 *
 * @package Base\Model
 */
class DB
{
    /**
     * Указатель на класс работы с БД
     * @access private
     * @var object
     */
    private static
        $master = null,
        $slave = null;

    /**
     * Возвращает класс работы с БД.
     *
     * Если соединение ещё не устанавливалось - устанавливает его.
     * @access protected
     * @return \CI_DB_query_builder $db
     */
    protected static function get_client()
    {
        return self::get_master();
    }

    protected static function get_master()
    {
        if (!is_null(self::$master)) {
            return self::$master;
        }

        $ci = & get_instance();
        self::$master = $ci->load->database('master', TRUE);

        return self::$master;
    }

    protected static function get_slave()
    {
        if (!is_null(self::$slave)) {
            return self::$slave;
        }

        $ci = & get_instance();
        self::$slave = $ci->load->database('slave', TRUE);

        return self::$slave;
    }
}