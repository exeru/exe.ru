<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 21.09.15
 * Time: 13:38
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends DB
{
    const _TABLE_SESSION = "game_sessions";
    const _TABLE_GAMES = "games_new";
    const _TABLE_GAMERS = "gamers";
    const _TABLE_GAMES_CHECK = "log_games_check";
    const _TABLE_GAMES_EDIT_LOG = "log_games_edit";

    public static function getLastVisitTimeByUidsAtGidsAtPeriod($uids, $gids, $start, $end)
    {
        $db = self::get_client();

        $db->select('"gid", MAX("last_visit") as "max"', false);
        $db->from(self::_TABLE_GAMERS);
        $db->where_in("uid", $uids);
        $db->where_in("gid", $gids);
        $db->where(['last_visit >=' => $start, 'last_visit < ' => $end, 'installed' => 1]);
        $db->group_by("gid");

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getLastVisitTimeByUids($uids)
    {
        $db = self::get_client();

        $db->select('"gid", MAX("last_visit") as "time"', false);
        $db->from(self::_TABLE_GAMERS);
        $db->where_in("uid", $uids);
        $db->where(['installed' => 1]);
        $db->group_by("gid");

        return $db->get()->result_array();
    }

    public static function getGamesByContractId($cid)
    {
        $db = self::get_client();

        $db->select(['gid', 'title', 'owner_uid', 'contract', 'balance']);

        $db->from(self::_TABLE_GAMES);
        $db->where('"contract" IS NOT NULL', null, false);
        $db->where(['contract' => $cid]);
        $db->order_by('balance', 'desc');

        return $db->get()->result_array();
    }

    public static function getGamesWithContracts()
    {
        $db = self::get_client();

        $db->select(['gid', 'title', 'owner_uid', 'contract']);

        $db->from(self::_TABLE_GAMES);
        $db->where('"contract" IS NOT NULL', null, false);
        $db->where('"contract" != \'\'', null, false);
        $db->order_by('contract', 'desc');

        return $db->get()->result_array();
    }

    public static function getCheckLog($gid)
    {
        $db = self::get_client();

        $db->select(['active', 'comment', 'datetime']);
        $db->from(self::_TABLE_GAMES_CHECK);
        $db->where(['gid' => $gid]);
        $db->order_by('cid', 'desc');

        return $db->get()->result_array();
    }

    public static function pushCheckLog($gid, $uid, $active, $comment, $time)
    {
        $db = self::get_client();

        return $db->insert(self::_TABLE_GAMES_CHECK, [
            'gid' => $gid,
            'uid' => $uid,
            'active' => $active,
            'comment' => $comment,
            'datetime' => $time
        ]);
    }

    public static function getEditLog($gid)
    {
        $db = self::get_client();

        $db->select(['datetime', 'fields']);
        $db->from(self::_TABLE_GAMES_EDIT_LOG);
        $db->where(['gid' => $gid]);
        $db->order_by('id', 'DESC');

        $result = $db->get()->result_array();

        return $result;
    }

    public static function pushEditLog($gid, $uid, $fields, $time)
    {
        $db = self::get_client();

        return $db->insert(self::_TABLE_GAMES_EDIT_LOG, [
            'gid' => $gid,
            'uid' => $uid,
            'datetime' => $time,
            'fields' => implode(', ', $fields)
        ]);
    }

    public static function getGamersId($gid)
    {
        $db = self::get_client();

        $db->select(['uid']);
        $db->from(self::_TABLE_GAMERS);
        $db->where([
            'gid' => $gid,
            'installed' => 1
        ]);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function getCountPlayers($gid)
    {
        $db = self::get_client();

        $db->select('COUNT(DISTINCT "uid") as "count"', false);
        $db->from(self::_TABLE_GAMERS);
        $db->where(['gid'=>$gid]);

        return $db->get()->row()->count;
    }

    public static function getAllGamesSortedByStatus($limit, $offset)
    {
        $db = self::get_client();

        $query = 'SELECT "gid", "active", "enabled", "title", "check_status\', \'check_comment\', \'datetime\', \'dev_last_changes\', \'dev_last_changes_checked\' 
        ';

        $db->select(['gid', 'active', 'enabled', 'title', 'check_status', 'check_comment', 'datetime', 'dev_last_changes', 'dev_last_changes_checked']);
        $db->from(self::_TABLE_GAMES);
        $db->order_by('CASE WHEN("active") THEN 0 WHEN("check_status") THEN 1 ELSE 2 END', null, false);
        $db->order_by('gid', 'DESC');

        $db->limit($limit);
        $db->offset($offset);

        return $db->get()->result_array();
    }

    public static function getActiveGamesId()
    {
        $db = self::get_client();

        $db->select(['gid']);
        $db->from(self::_TABLE_GAMES);
        $db->where(['active' => true, 'enabled' => true]);

        return $db->get()->result_array();
    }

    public static function getAllGamesId()
    {
        $db = self::get_client();

        $db->select(['gid']);
        $db->from(self::_TABLE_GAMES);

        return $db->get()->result_array();
    }

    public static function setGameDataFieldsById($gid, $updatedFields)
    {
        $db = self::get_client();

        return $db->update(self::_TABLE_GAMES, $updatedFields, ['gid' => $gid]);
    }

    public static function getGameDataFieldsById($gid, $fields)
    {
        $db = self::get_client();

        $db->select($fields);
        $db->from(self::_TABLE_GAMES);
        $db->where(['gid' => $gid]);
        $db->limit(1);

        $result = $db->get();
        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->row_array();
    }

    public static function addGame($uid, $type, $title, $description, $apiKey, $height)
    {
        $db = self::get_client();

        $db->insert(self::_TABLE_GAMES, [
            'title' => $title,
            'type' => $type,
            'owner_uid' => $uid,
            'description' => $description,
            'api_key' => $apiKey,
            'height' => $height,
            'datetime' => time()
        ]);

        return $db->insert_id();
    }

    public static function unInstall($uid, $gid, $timestamp)
    {
        $db = self::get_client();

        $db->update(self::_TABLE_GAMERS . '_' . ($uid % 10), [
            'last_visit' => $timestamp,
            'installed' => 0
        ], [
            'uid' => $uid,
            'gid' => $gid
        ]);

        return true;
    }

    public static function getGameById($gid, $hidden = false)
    {
        $db = self::get_client();

        $db->select(['gid', 'title', 'type', 'http', 'https', 'height', 'image_64x64', 'image_16x16', 'image_150x150', 'image_810x500', 'image_80x80', 'image_guest', 'repairs']);
        $db->from(self::_TABLE_GAMES);
        $db->where(['gid'=>$gid]);

        if ($hidden === false) {
            $db->where(['active' => true, 'enabled' => true]);
        }

        $db->limit(1);

        $result = $db->get()->result_array();

        if (count($result) !== 1) {
            return false;
        }

        return $result[0];
    }

    public static function getPurchasesInfoByGameId($gid)
    {
        $db = self::get_client();

        $db->select(['api_key', 'callback_url']);
        $db->from(self::_TABLE_GAMES);
        $db->where(['gid' => $gid]);
        $db->limit(1);

        $result = $db->get();
        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->row_array();
    }

    public static function getSecretKey($gid)
    {
        $db = self::get_client();

        $db->select(['api_key']);
        $db->from(self::_TABLE_GAMES);
        $db->where(['gid'=>$gid]);
        $db->limit(1);

        $result = $db->get();

        return $result->row()->api_key;
    }

    public static function getGameIdBySid($sid)
    {
        \Base\Service\Log::profile(__METHOD__);

        $db = self::get_client();

        $db->select(['gid']);
        $db->from(self::_TABLE_SESSION);
        $db->where(['sid'=>$sid]);
        $db->limit(1);

        $result = $db->get()->row();

        if (is_null($result)) {
            \Base\Service\Log::profile(__METHOD__);
            return false;
        }

        \Base\Service\Log::profile(__METHOD__);
        return $result->gid;
    }

    public static function setGameSettings($uid, $gid, $values)
    {
        $db = self::get_client();

        return $db->update(self::_TABLE_GAMERS . '_' . ($uid % 10), $values, ['uid'=>$uid, 'gid'=>$gid]);
    }

    public static function getGameSettings($uid, $gid)
    {
        $db = self::get_client();

        $db->select(['notifications', 'invitible']);
        $db->from(self::_TABLE_GAMERS . '_' . ($uid % 10));
        $db->where(['uid'=>$uid, 'gid'=>$gid]);
        $db->limit(1);

        $result = $db->get()->result_array()[0];

        return $result;
    }

    public static function getGamesByIds($ids)
    {
        $db = self::get_client();

        $db->select(['gid', 'title', 'type', 'image_256x256']);
        $db->from(self::_TABLE_GAMES);
        $db->where_in('gid', $ids);

        return $db->get()->result_array();
    }

    public static function getCatalog($active = true, $enabled = true, $limit = 100)
    {
        $db = self::get_client();

        $db->select(['gid', 'type', 'title', 'image_810x500']);
        $db->from(self::_TABLE_GAMES);
        $db->where(['active'=>(bool) $active, 'enabled'=> (bool) $enabled]);
        $db->limit($limit);

        $result = $db->get()->result_array();

        return $result;
    }

    public static function appendGamer($uid, $gid)
    {
        $db = self::get_client();

        return $db->insert(self::_TABLE_GAMERS . '_' . ($uid % 10), [
            'uid' => $uid,
            'gid' => $gid,
            'last_visit' => time(),
            'installed' => 1,
            'notifications' => 1,
            'invitible' => 1
        ]);
    }

    public static function uptimeGamer($uid, $gid)
    {
        \Base\Service\Log::profile(__METHOD__);

        $db = self::get_client();

        $result = $db->update(self::_TABLE_GAMERS . '_' . ($uid % 10), [
            'last_visit' => time()
        ], [
            'uid' => $uid,
            'gid' => $gid
        ]);

        \Base\Service\Log::profile(__METHOD__);

        return $result;
    }

    public static function setInstalledAndUptimeGamer($uid, $gid)
    {
        $db = self::get_client();

        return $db->update(self::_TABLE_GAMERS . '_' . ($uid % 10), [
            'installed' => 1,
            'last_visit' => time()
        ], [
            'uid' => $uid,
            'gid' => $gid
        ]);
    }

    public static function getUserGamesForCache($uid)
    {
        \Base\Service\Log::profile(__METHOD__);

        $db = self::get_client();

        $db->select('gid, installed');
        $db->from(self::_TABLE_GAMERS . '_' . ($uid % 10));

        $db->where(['uid'=>$uid]);

        $result = $db->get()->result_array();

        \Base\Service\Log::profile(__METHOD__);

        return $result;
    }

    public static function createSession($uid, $gid)
    {
        $userSessionId = md5($uid . microtime(true));
        $gidSessionId = md5($gid . microtime(true));

        $db = self::get_client();
        $db->insert(self::_TABLE_SESSION, [
            'sid' => $userSessionId . $gidSessionId,
            'uid' => $uid,
            'gid' => $gid,
            'time' => time()
        ]);

        return $userSessionId . $gidSessionId;
    }

    public static function uptimeSession($uid, $gid)
    {
        \Base\Service\Log::profile(__METHOD__);

        $db = self::get_client();

        $result = $db->update(self::_TABLE_SESSION, [
            'time' => time()
        ], [
            'uid'=>$uid,
            'gid'=>$gid
        ]);

        \Base\Service\Log::profile(__METHOD__);

        return $result;
    }

    public static function getSessionId($uid, $gid)
    {
        $db = self::get_client();

        $db->select('sid');
        $db->from(self::_TABLE_SESSION);
        $db->where(['uid'=>$uid, 'gid'=>$gid]);
        $db->limit(1);

        $result = $db->get();

        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->row()->sid;
    }

    public static function deleteOldSessions($time)
    {
        $db = self::get_client();

        return $db->delete(self::_TABLE_SESSION, ['"time" < ' => $time]);
    }
}