<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.04.16
 * Time: 17:25
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends DB
{
    const _TABLE_FRIENDS_NOTIFICATIONS = 'friends_notifications';
    const _TABLE_GAME_NOTIFICATIONS = 'game_notifications';
    const _TABLE_STATS_NOTIFICATIONS = 'stats_notifications';
    const _TABLE_STATS_NOTIFICATIONS_BY_MONTHS = 'stats_notifications_by_months';

    public static function getByType($gid, $day)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "ntype",
            SUM("allnot") as "all",
            SUM("filtrnot") as "filter",
            SUM("succesnot") as "success",
            SUM("failurenot") as "fail"
          FROM
            "stats_notifications"
          WHERE
            "gid" = ' . $gid . '
          AND
            "time" = ' . $day . '
          AND
            "type" != 2
          GROUP BY "ntype"
          ORDER BY "ntype"
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }


    public static function updateDataByMonths($set, $where)
    {
        $db = self::get_client();

        return $db->update(self::_TABLE_STATS_NOTIFICATIONS_BY_MONTHS, $set, $where);
    }

    public static function insertDataByMonths($currentData)
    {
        $db = self::get_client();

        return $db->insert_batch(self::_TABLE_STATS_NOTIFICATIONS_BY_MONTHS, $currentData);
    }

    public static function getAgregatedDataForCollect($month)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "gid",
            "type",
            "ntype",
            "all",
            "filter",
            "success",
            "fail"
          FROM
            "' . self::_TABLE_STATS_NOTIFICATIONS_BY_MONTHS . '"
          WHERE
            "month_number" = ' . $month. '
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getDataForCollect($dateStart, $dateEnd)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "gid",
            "type",
            "ntype",
            SUM("allnot") as "all",
            SUM("filtrnot") as "filter",
            SUM("succesnot") as "success",
            SUM("failurenot") as "fail"
          FROM
            "' . self::_TABLE_STATS_NOTIFICATIONS . '"
          WHERE
            "time" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
          GROUP BY "gid", "type", "ntype"
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getFromIntervalByMonths($gid, $dateStart, $dateEnd, $all = false)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "month_number",
            SUM("all") as "all",
            SUM("filter") as "filter",
            SUM("success") as "success",
            SUM("fail") as "fail"
          FROM
            "' . self::_TABLE_STATS_NOTIFICATIONS_BY_MONTHS . '"
          WHERE
            "gid" = ' . $gid .'
          AND "type" ' . ($all ? '=' : '!=') .  ' 2
          AND "month_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
          GROUP BY "month_number"
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getFromIntervalByDaysAndTypes($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "time" as "day_number",
            "ntype",
            SUM("allnot") as "all",
            SUM("filtrnot") as "filter",
            SUM("succesnot") as "success",
            SUM("failurenot") as "fail"
          FROM
            "' . self::_TABLE_STATS_NOTIFICATIONS . '"
          WHERE
            "gid" = ' . $gid .'
          AND "type" != 2
          AND "time" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
          GROUP BY "time", "ntype"
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function getAllFromInterval($gid, $dateStart, $dateEnd, $all = false)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "time" as "day_number",
            SUM("allnot") as "all",
            SUM("filtrnot") as "filter",
            SUM("succesnot") as "success",
            SUM("failurenot") as "fail"
          FROM
            "' . self::_TABLE_STATS_NOTIFICATIONS . '"
          WHERE
            "gid" = ' . $gid .'
          AND "type" ' . ($all ? '=' : '!=') .  ' 2
          AND "time" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
          GROUP BY "time"
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function updateStat($gid, $dayNumber, $type, $ntype, $incrementField)
    {
        $db = self::get_client();

        // TODO Придумать, как не выполнять прямой запрос к базе.
        return $db->query(
            'UPDATE "' . self::_TABLE_STATS_NOTIFICATIONS .
            '" SET "'.$incrementField.'" = "'.$incrementField.'" + 1 WHERE "gid" = ' . $gid .
            ' AND "time" = ' . $dayNumber . ' AND "type" = ' . $type . ' AND "ntype" = ' . $ntype );
    }

    public static function deleteGameNotification($nid)
    {
        $db = self::get_client();

        return $db->delete(self::_TABLE_GAME_NOTIFICATIONS, ['nid' => $nid]);
    }

    public static function getGameNotifications($uid)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "gid", "text", "nid"
          FROM (
            SELECT
              row_number() OVER (PARTITION BY "gid" ORDER BY "type" DESC, "nid" DESC), "gid", "text", "nid"
            FROM
             "' . self::_TABLE_GAME_NOTIFICATIONS . '"
            WHERE
              "uid" = ' . $uid . '
            ) as "w"
          WHERE
            "row_number" = 1
          ORDER BY
            "nid"
        ';


        $result = $db->query($query)->result_array();

        return $result;
    }

    public static function getGameNotificationByUIdAndNId($uid, $gid, $nid)
    {
        // Избыточные параметры передаются специально, для дополнительного контроля данных
        $db = self::get_client();

        $db->select(['type', 'ntype', 'time']);
        $db->from(self::_TABLE_GAME_NOTIFICATIONS);
        $db->where([
            'gid' => $gid,
            'uid' => $uid,
            'nid' => $nid
        ]);
        $db->limit(1);

        $result = $db->get();
        if ($result->num_rows() == 0) {
            return false;
        }

        return $result->result_array()[0];
    }
}