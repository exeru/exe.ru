<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.05.16
 * Time: 13:45
 */

namespace Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class StatsGame extends DB
{
    const _TABLE_LOG = 'stats_install_delete_log',
        _TABLE_COLLECTED = 'stats_install_delete',
        _TABLE_INSTALLED = 'stats_installed',
        _TABLE_SOURCES = 'stats_sources';

    public static function getSources($gid, $dateStart = null, $dateEnd = null)
    {
        $db = self::get_client();

        $query = '
          SELECT
            COUNT("id") as "count",
            "group_id"
          FROM
            "' . self::_TABLE_SOURCES . '"
          WHERE
            "gid" = ' . $gid . '
        ';

        if (!is_null($dateStart) && !is_null($dateEnd)) {
            $query .= '
                AND "day_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
            ';
        }

        $query .= '
          GROUP BY "group_id"
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function insertInstall($gid, $dayNumber, $groupId)
    {
        $db = self::get_client();

        return $db->insert(self::_TABLE_SOURCES, [
            'gid' => $gid,
            'day_number' => $dayNumber,
            'group_id' => $groupId
        ]);
    }

    public static function getCollectedInstalled($gid)
    {
        $db = self::get_client();

        $db->select(['day_number', 'count']);
        $db->from(self::_TABLE_INSTALLED);
        $db->where(['gid' => $gid]);
        $db->order_by('day_number', 'ASC');

        $result = $db->get();

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function insertInstalled($data)
    {
        $db = self::get_client();

        return $db->insert_batch(self::_TABLE_INSTALLED, $data);
    }

    public static function updateCollectedInstallDelete($set, $where)
    {
        $db = self::get_client();

        return $db->update(self::_TABLE_COLLECTED, $set, $where);
    }

    public static function getCollectedInstallDelete($gid, $dateStart, $dateEnd)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "gid",
            "day_number",
            "installed",
            "deleted"
          FROM
            "' . self::_TABLE_COLLECTED . '"
          WHERE
            "gid" ' . (is_array($gid) ? 'IN (' . implode(', ', $gid) . ')' : ' = ' . $gid) . '
          AND
            "day_number" BETWEEN ' . $dateStart . ' AND ' . $dateEnd . '
          ORDER BY "day_number"
        ';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function insertCollectedInstallDelete($stat)
    {
        $db = self::get_client();

        $db->insert_batch(self::_TABLE_COLLECTED, $stat);
    }

    public static function getInstallDeleteForCollect($dayNumber)
    {
        $db = self::get_client();

        $query = '
          SELECT
            "gid",
            "install",
            COUNT(DISTINCT "uid") as "count"
          FROM
            "' . self::_TABLE_LOG . '"
          WHERE
            "day_number"= ' . $dayNumber . '
          GROUP BY
            "gid",
            "install"';

        $result = $db->query($query);

        if ($result->num_rows() < 1) {
            return [];
        }

        return $result->result_array();
    }

    public static function insertInstallDelete($gid, $dayNumber, $uid, $installed)
    {
        $db = self::get_client();

        return $db->insert(self::_TABLE_LOG, [
            'gid' => $gid,
            'day_number' => $dayNumber,
            'uid' => $uid,
            'install' => $installed
        ]);
    }
}