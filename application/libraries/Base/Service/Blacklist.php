<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 03.09.15
 * Time: 15:12
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Blacklist
{
    /**
     * @deprecated use Friends::getBlackList
     */
    public static function getBlackList($uid)
    {
        return Friends::getBlackList($uid);
    }

    /**
     * @deprecated use Friends::toBlackList
     */
    public static function appendToBlackList($uid, $buid)
    {
        return Friends::toBlackList($uid, $buid);
    }

    /**
     * @deprecated use Friends::fromBlackList
     */
    public static function removeFromBlackList($uid, $buid)
    {
        return Friends::fromBlackList($uid, $buid);
    }
}