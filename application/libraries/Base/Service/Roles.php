<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 03.03.16
 * Time: 13:39
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Roles
{
    const MODERATORS = [
        'development' => [
            1303195 // Ruslan
        ],
        'testing' => [
            1303195, // Ruslan
            1303203, // Костя
            1303222, // Федя,
            1303914, // Валентина
        ],
        'production' => [
            23, // Руслан
            24, // Руслан
            25, // Руслан
            26, // Руслан
            27, // Костя
            //189, // Саша
            190, // Александр
            192, // Шамиль
        ]
    ];

    const
        USER_OWNER = 1,
        USER_ADMIN = 2,
        USER_PAYMENT_TESTER = 3,
        USER_TESTER = 4;

    const
        ACCESS_DELETE = 1 << 0,
        ACCESS_CHANGE_KEY = 1 << 1,
        ACCESS_APPEND_DELETE_ADMIN = 1 << 2,
        ACCESS_APPEND_DELETE_TESTER = 1 << 3,
        ACCESS_PAYMENTS = 1 << 4,
        ACCESS_VIEW = 1 << 5,
        ACCESS_CHANGE_SETTINGS = 1 << 6,
        ACCESS_CONTRACT = 1 << 7;

    const GROUPS = [
        self::USER_OWNER =>
            self::ACCESS_DELETE |
            self::ACCESS_CHANGE_KEY |
            self::ACCESS_APPEND_DELETE_ADMIN |
            self::ACCESS_APPEND_DELETE_TESTER |
            self::ACCESS_PAYMENTS |
            self::ACCESS_VIEW |
            self::ACCESS_CHANGE_SETTINGS |
            self::ACCESS_CONTRACT,
        self::USER_ADMIN =>
            self::ACCESS_APPEND_DELETE_TESTER |
            self::ACCESS_PAYMENTS |
            self::ACCESS_VIEW |
            self::ACCESS_CHANGE_SETTINGS,
        self::USER_PAYMENT_TESTER =>
            self::ACCESS_PAYMENTS |
            self::ACCESS_VIEW,
        self::USER_TESTER =>
            self::ACCESS_VIEW
    ];

    private static $_cache = null;

    public static function setAccess($uid, $gid, $group)
    {
        self::flushAccess($uid, $gid);

        return self::add($gid, $uid, $group);
    }

    public static function flushAccess($uid, $gid)
    {
        return Model\Roles::flushAccess($uid, $gid);
    }

    public static function add($gid, $uid, $group)
    {
        return Model\Roles::add($gid, $uid, $group);
    }

    public static function getGames($uid)
    {
        self::fill_cache($uid);

        return array_keys(self::$_cache[$uid]);
    }

    public static function isModerator($uid) {
        if (in_array($uid, self::MODERATORS[ENVIRONMENT])) {
            return true;
        }

        return false;
    }

    public static function hasAccess($uid, $gid, $access)
    {
        if (self::isModerator($uid)) {
            return true;
        }

        self::fill_cache($uid);

        if (!isset(self::$_cache[$uid][$gid]) || self::$_cache[$uid][$gid] == 0) {
            return false;
        }

        return ( self::$_cache[$uid][$gid] & $access) === $access;
    }

    public static function getUsersByGroups($gid)
    {
        $users = Model\Roles::getUsers($gid);

        $usersByGroups = [];

        if (count($users) > 0) {
            foreach($users as $user) {
                if (is_null($user['group'])) {
                    continue;
                }
                $usersByGroups[$user['uid']] = (int) $user['group'];
            }
        }

        return $usersByGroups;
    }

    public static function getUserGroup($uid, $gid)
    {
        self::fill_cache($uid);
        if (!isset(self::$_cache[$uid][$gid]) || self::$_cache[$uid][$gid] == 0) {
            return false;
        }

        $group = array_search(self::$_cache[$uid][$gid], self::GROUPS);

        return $group;
    }

    private static function fill_cache($uid)
    {
        if (isset(self::$_cache[$uid])) {
            return null;
        }

        self::$_cache[$uid] = [];

        $result = Model\Roles::getGames($uid);
        if (count($result) > 0) {
            // Делаю через промежуточную переменную, т.к. IDE непонятно почему подсвечивает ошибку в синтаксисе self::GROUPS[]
            $groups = self::GROUPS;

            $result = array_column($result, 'group', 'gid');

            foreach($result as $ggid=>$ggroup) {
                $ggroup = (int) $ggroup;

                self::$_cache[$uid][$ggid] = isset($groups[$ggroup]) ? $groups[$ggroup] : 0;
            }
        }

        return null;
    }

    public static function checkPermission($uid, $gid = null, $permission = self::ACCESS_CHANGE_SETTINGS)
    {
        if (!self::hasAccess($uid, $gid, $permission)) {
            Roles::forbidden();
            exit();
        }
    }

    public static function forbidden()
    {
        show_error('У Вас недостаточно прав для выполнения данного действия', 403, 'Недостаточно прав');
    }
}