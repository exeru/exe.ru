<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04.10.16
 * Time: 13:32
 */

namespace Base\Service;
use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class UserLog
{
    const
        AUTH = 1, // Авторизовался на сайте
        LOGOUT = 2, // Разлогинился на сайте
        START_GAME = 3, // Начал играть
        UNINSTALL_GAME = 4, // Удалил игру
        APPEND_FRIEND = 5, // Добавил друга
        DELETE_FRIEND = 6, // Удалил друга
        CHANGE_PHOTO = 7, // Изменил фото профиля
        CHANGE_INFO = 8, // Изменил информацию в профиле
        TO_BLACKLIST = 9, // Добавил в черный список
        FROM_BLACKLIST = 10, // Удалил из черного списка
        TO_BALANCE = 11, // Пополнил баланс
        PAY_TO_GAME = 12, // Заплатил в игре
        REGISTER = 13, // Зарегистрировался на сайте
        REGISTER_SOCIAL = 14, // Зарегистрировался через социалку
        AUTH_SOCIAL = 15, // Авторизовался через социалку
        PAY_TO_GAME_TEST = 16, // Заплатил в игре в тестовом режиме
        REGISTER_TEMPORARY = 17, // Временный пользователь зарегистрировался
        NEW_TEMPORARY = 18, // Новый временный пользователь
        REGISTER_TEMPORARY_SOCIAL = 19, // Временный пользователь зарегистрировался через социалку
        USER_TO_BLACKLIST = 20, // Пользователь, который был добавлен в черный список
        USER_FROM_BLACKLIST = 21, // Пользователь, который был удален из черного списка
        USER_TO_FRIENDS = 22, // Пользователь, которого добавили в друзья
        USER_FROM_FRIENDS = 23, // Пользователь, которого удалили из друзей
        PASSWORD_RESTORE = 24, // Пользователь запросил восстановление пароля
        INSTALL_GAME = 25, // Установил игру
        MODERATOR_TO_BALANCE = 26, // Модератор зачислил платеж
        USER_WRITE_MESSAGE = 27, // Пользователь написал сообщение
        BEGIN_BUY_ITEM = 28, // Вызов окна покупки в игре
        BUY_ITEM_NO_MONEY = 29, // У пользователя недостаточно средств для покупки
        BUY_ITEM_CANCEL = 30, // Пользователь отказался от покупки
        GO_TO_PAYMENT_SYSTEM = 31, // Пользователь перешел на страницу платежной системы
        BUY_ITEM_NO_MONEY_AFTER_RECHARGE = 32, // Недостаточно средст для покупки после пополнения баланса
        RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY = 33, // Вернулся из платежной системы не выполняя оплату
        RATING_UPDATE = 34, // Обновлен рейтинг
        TEMPORARY_AUTH_AS_USER = 35, // Временный пользователь авторизовался как зарегистрированный
        USER_AUTH_FROM_TEMPORARY = 36, // Зарегистрированный авторизовался из временного аккаунта
        BONUS_TO_BALANCE = 37; // Пользователю начислен бонус

    const TEXTS = [
        self::AUTH => 'Авторизовался на сайте',
        self::LOGOUT => 'Вышел с сайта через настройки',
        self::START_GAME => 'Начал играть в игру %game%',
        self::UNINSTALL_GAME => 'Удалил игру %game%',
        self::APPEND_FRIEND => 'Добавил в друзья %user%',
        self::DELETE_FRIEND => 'Удалил %user% из друзей',
        self::CHANGE_PHOTO => 'Изменил фотографию в профиле',
        self::CHANGE_INFO => 'Изменил информацию в профиле',
        self::TO_BLACKLIST => 'Добавил %user% в черный список',
        self::FROM_BLACKLIST => 'Удалил %user% из черного списка',
        self::TO_BALANCE => 'Ввел на баланс %count% [%balance%]',
        self::PAY_TO_GAME => 'Заплатил %count% в игре %game% [%balance%]',
        self::REGISTER => 'Зарегистрировался на сайте',
        self::REGISTER_SOCIAL => 'Зарегистрировался на сайте через социальную сеть %s',
        self::AUTH_SOCIAL => 'Авторизовался на сайте через социальную сеть %s',
        self::PAY_TO_GAME_TEST => 'Заплатил %count% в игре %game% в тестовом режиме',
        self::REGISTER_TEMPORARY => 'Временный пользователь зарегистрировался',
        self::NEW_TEMPORARY => 'Новый временный пользователь',
        self::REGISTER_TEMPORARY_SOCIAL => 'Временный пользователь зарегистрировался через социальную сеть %s',
        self::USER_TO_BLACKLIST => 'Был добавлен в черный список %user%',
        self::USER_FROM_BLACKLIST => 'Был удален из черного списка у %user%',
        self::USER_TO_FRIENDS => 'Был добавлен в друзья %user%',
        self::USER_FROM_FRIENDS => 'Был удален из друзей у %user%',
        self::PASSWORD_RESTORE => 'Запросил восстановление пароля',
        self::INSTALL_GAME => 'Установил игру %game%',
        self::MODERATOR_TO_BALANCE => 'Модератор %muid% зачислил платеж %orderId% на сумму %sum% [%balance%]',
        self::USER_WRITE_MESSAGE => 'Пользователь %uid% написал сообщение пользователю %fid% [%text%]',
        self::BEGIN_BUY_ITEM => 'Начал покупку в игре %game% на сумму %count%, балланс %balance%',
        self::BUY_ITEM_NO_MONEY => 'Недостаточно средств для покупки в игре %game%, нужно %count%, есть %balance%',
        self::BUY_ITEM_CANCEL => 'Пользователь отменил покупку в игре %game%',
        self::GO_TO_PAYMENT_SYSTEM => 'Перешел на оплату [ %system% ], сумма %sum%',
        self::BUY_ITEM_NO_MONEY_AFTER_RECHARGE => 'После пополнения недостаточно средств, есть %balance%, нужно %count%',
        self::RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY => 'Вернулся из платежной системы [%system%] без оплаты',
        self::RATING_UPDATE => 'Рейтинг изменен на %d',
        self::TEMPORARY_AUTH_AS_USER => '%tempuser% авторизовался как %user%',
        self::USER_AUTH_FROM_TEMPORARY => '%user% авторизовался из %tempuser%',
        self::BONUS_TO_BALANCE => 'Начислен бонус %count%, новый баланс %balance%'
    ];

    const FILTERS = [
        //self::AUTH => 'Авторизовался на сайте', //
        //self::LOGOUT => 'Вышел с сайта через настройки', //
        //self::START_GAME => 'Начал играть', //
        self::UNINSTALL_GAME => 'Удалил игру', //
        //self::APPEND_FRIEND => 'Добавил в друзья', //
        //self::DELETE_FRIEND => 'Удалил из друзей', //
        //self::CHANGE_PHOTO => 'Изменил фото', //
        //self::CHANGE_INFO => 'Изменил профиль', //
        //self::TO_BLACKLIST => 'Добавил в черный список', //
        //self::FROM_BLACKLIST => 'Удалил из черного списка', //
        self::TO_BALANCE => 'Пополнил баланс', //
        self::PAY_TO_GAME => 'Заплатил в игре', //
        self::REGISTER => 'Зарегистрировался', //
        self::REGISTER_SOCIAL => 'Зарегистрировался через соц. сеть', //
        //self::AUTH_SOCIAL => 'Авторизовался через соц. сеть', //
        //self::PAY_TO_GAME_TEST => 'Заплатил в тестовом режиме',
        self::REGISTER_TEMPORARY => 'Временный зарегистрировался', //
        self::NEW_TEMPORARY => 'Новый временный', //
        self::REGISTER_TEMPORARY_SOCIAL => 'Временный зарег. через соц. сеть', //
        //self::USER_TO_BLACKLIST => 'Добавлен в блеклист', //
        //self::USER_FROM_BLACKLIST => 'Удален из блеклиста', //
        //self::USER_TO_FRIENDS => 'Добавлен в друзья', //
        //self::USER_FROM_FRIENDS => 'Удален из друзей', //
        //self::PASSWORD_RESTORE => 'Запросил пароль', //
        self::INSTALL_GAME => 'Установил игру', //
        self::MODERATOR_TO_BALANCE => 'Зачислен платеж',
        self::USER_WRITE_MESSAGE => 'Написал сообщение', //
        self::BEGIN_BUY_ITEM => 'Начал покупку', //
        self::BUY_ITEM_NO_MONEY => 'Недостаточно средств', //
        self::BUY_ITEM_CANCEL => 'Отменил покупку',
        self::GO_TO_PAYMENT_SYSTEM => 'Перешел на плат. систему.', //
        self::BUY_ITEM_NO_MONEY_AFTER_RECHARGE => 'После пополения не хватает',
        self::RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY => 'Вернулся без оплаты',
        //self::RATING_UPDATE => 'Изменился рейтинг',
        //self::TEMPORARY_AUTH_AS_USER => 'Авторизовался как',
        //self::USER_AUTH_FROM_TEMPORARY => 'Авторизовался из',
        self::BONUS_TO_BALANCE => 'Получил бонус'
    ];

    const GROUPS = [
        self::AUTH => 'auth',
        self::AUTH_SOCIAL => 'auth',
        self::REGISTER => 'auth',
        self::REGISTER_SOCIAL => 'auth',
        self::PASSWORD_RESTORE => 'auth',
        self::NEW_TEMPORARY => 'auth',
        self::REGISTER_TEMPORARY => 'auth',
        self::REGISTER_TEMPORARY_SOCIAL => 'auth',
        self::LOGOUT => 'auth',
        self::TEMPORARY_AUTH_AS_USER => 'auth',
        self::USER_AUTH_FROM_TEMPORARY => 'auth',

        self::INSTALL_GAME => 'game',
        self::START_GAME => 'game',
        self::UNINSTALL_GAME => 'game',

        self::BEGIN_BUY_ITEM => 'payments',
        self::BUY_ITEM_NO_MONEY => 'payments',
        self::BUY_ITEM_CANCEL => 'payments',
        self::BUY_ITEM_NO_MONEY_AFTER_RECHARGE => 'payments',
        self::TO_BALANCE => 'payments',
        self::PAY_TO_GAME => 'payments',
        self::PAY_TO_GAME_TEST => 'payments',
        self::MODERATOR_TO_BALANCE => 'payments',
        self::GO_TO_PAYMENT_SYSTEM => 'payments',
        self::RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY => 'payments',
        self::BONUS_TO_BALANCE => 'payments',

        self::CHANGE_PHOTO => 'profile',
        self::CHANGE_INFO => 'profile',
        self::RATING_UPDATE => 'profile',

        self::APPEND_FRIEND => 'users',
        self::DELETE_FRIEND => 'users',
        self::TO_BLACKLIST => 'users',
        self::FROM_BLACKLIST => 'users',
        self::USER_TO_BLACKLIST => 'users',
        self::USER_FROM_BLACKLIST => 'users',
        self::USER_TO_FRIENDS => 'users',
        self::USER_FROM_FRIENDS => 'users',
        self::USER_WRITE_MESSAGE => 'users'
    ];

    public static function getInstalledGidsByPeriod($start, $end)
    {
        return Model\UserLog::getInstalledGidsByPeriod($start, $end);
    }

    public static function getInstalledByGroupsByPeriod($start, $end, $fieldGroup, $installedGids = [])
    {
        return Model\UserLog::getInstalledByGroupsByPeriod($start, $end, $fieldGroup, $installedGids);
    }

    public static function getInstalledNewMaxTime()
    {
        return Model\UserLog::getInstalledNewMaxTime();
    }

    public static function getInstalledNewMinTime()
    {
        return Model\UserLog::getInstalledNewMinTime();
    }

    public static function getMaxRegistrationTime()
    {
        return Model\UserLog::getMaxRegistrationTime();
    }

    public static function getMinRegistrationTime()
    {
        return Model\UserLog::getMinRegistrationTime();
    }

    public static function getRegistationsDataByPeriod($start, $end, $fieldGroup)
    {
        return Model\UserLog::getRegistationsDataByPeriod($start, $end, $fieldGroup);
    }

    public static function save($uid, $gid, $action, $param = null, $marker = null)
    {
        Log::profile(__METHOD__);
        $filters = self::FILTERS;

        if (isset($filters[$action])) {
            $sessionNumber = null;

            if (!is_null($marker)) {
                $sessionNumber = User::getUserSessionNumberByMarker($marker);
                if ($sessionNumber === false) {
                    $mainMarker = User::getMarkerByMainMarker($marker);
                    $sessionNumber = User::getLastSessionNumberByMainMarker($mainMarker);
                    if ($sessionNumber === false) {
                        $sessionNumber = null;
                    }
                }
            }

            Log::profile(__METHOD__);
            return Model\UserLog::save($uid, $gid, $action, time(), $param, $marker, $sessionNumber);
        }

        Log::profile(__METHOD__);
        return false;
    }

    public static function getLog($start, $end, $byid, $sessionNumbers, $bygid, $bots, $temporary, $filter, $limit, $offset)
    {
        return Model\UserLog::getLog($start, $end, $byid, $sessionNumbers, $bygid, $bots, $temporary, $filter, $limit, $offset);
    }

    public static function getLogByMarker($marker, $start = 0, $end = 0)
    {
        return Model\UserLog::getLogByMarker($marker, $start, $end);
    }

    public static function getPayersDataByDayByPeriod($start, $end, $isInit)
    {
        return Model\UserLog::getPayersDataByDayByPeriod($start, $end, $isInit);
    }

    /**
     * @deprecated
     */
    public static function getMarkersForPeriodByAction($start, $end, $action = null)
    {
        return Model\UserLog::getMarkersForPeriodByAction($start, $end, $action);
    }
}