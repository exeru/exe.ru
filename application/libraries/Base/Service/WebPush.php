<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.12.17
 * Time: 17:08
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class WebPush
{
    public static function getSubscribeStat()
    {
        $data = Model\WebPush::getSubscribeStat();

        if (count($data) > 0) {
            return array_column($data, 'count', 'state');
        }

        return [];
    }

    public static function setSubscribeState($uid, $state)
    {
        return Model\WebPush::setSubscribeState($uid, $state);
    }

    public static function eventManager($eventType, $params)
    {
        switch($eventType) {
            case 'EVENT_USER_SEND_MESSAGE': {
                self::pushNewMessage($params['uids'], $params['message']);
                break;
            }
        }
    }

    // Активность, вызывать через события
    private static function pushNewMessage(Array $uids, $jsonMessage)
    {
        if (count($uids) < 1) {
            return false;
        }

        if (count($uids) > 10000) {
            set_time_limit(0);
        }

        $auth = array(
            'GCM' => 'AAAAs6yWuR4:APA91bEfhwY2_9AEWd0p4I7imdN00vDbvOsPLQSEOTI5uBfa9Uzq-D7C-NHgy5FMKmp1HPYxCBvXGM29AunUFMbEWdpe3-FYAYuc9bHBpbJD7Ezsm2jQfuOAIB0DCCLd2DeuVP-QohlZ', // deprecated and optional, it's here only for compatibility reasons
            'VAPID' => array(
                'subject' => ENVIRONMENT == 'production' ? 'https://exe.ru/' : 'https://test.exe.ru/', // can be a mailto: or your website address
                'publicKey' => 'BAqwfUlTj4ZvwPW8OiVJQ54VoKzJaKGQ3ty4R/1PuFjTf7iIfWlWyiVcuOpSYHT/ceZWQ24By3FU6JxugNfdQRc=', // (recommended) uncompressed public key P-256 encoded in Base64-URL
                'privateKey' => 'd+K/PlemH3sbuWohsP7C9zffjUAx6mepzTTf3kmFjc8=', // (recommended) in fact the secret multiplier of the private key encoded in Base64-URL
                //'pemFile' => 'path/to/pem', // if you have a PEM file and can link to it on your filesystem
                //'pem' => 'pemFileContent', // if you have a PEM file and want to hardcode its content
            ),
        );

        $webPush = new \Minishlink\WebPush\WebPush($auth);

        foreach($uids as $uid) {
            $subscribes = \Base\Service\WebPush::getSubscribes($uid, 'new_messages');

            if ($subscribes === false || count($subscribes) < 1) {
                continue;
            }

            $message = json_decode($jsonMessage, true);
            $user = User::getUserInfo($message['message']['uid']);

            foreach($subscribes as $subscribe) {
                $subscribe = json_decode($subscribe['subscribe'], true);

                $payload = json_encode([
                    'title' => 'Новое сообщение',
                    'message'  => 'Получено новое сообщение.',
                    'icon'  => 'https://' . (ENVIRONMENT == 'production' ? 'exe.ru' : 'test.exe.ru') . $user['photo'][70],
                    'url'   => 'https://' . (ENVIRONMENT == 'production' ? 'exe.ru' : 'test.exe.ru') . '/dialogs?source=push'
                ]);

                $webPush->sendNotification($subscribe['endpoint'], $payload, $subscribe['keys']['p256dh'], $subscribe['keys']['auth']);
            }

            $webPush->flush();
        }
    }

    public static function getSubscribes($uid, $channel)
    {
        return Model\WebPush::getSubscribes($uid, $channel);
    }

    public static function disableChannel($uid, $channel, $subscribeId)
    {
        return Model\WebPush::disableChannel($uid, $channel, $subscribeId);
    }

    public static function enableChannel($uid, $channel, $subscribeId)
    {
        return Model\WebPush::enableChannel($uid, $channel, $subscribeId);
    }

    public static function log($uid, $type, $channel, $show, $subscribe, $subscribeId)
    {
        return Model\WebPush::log($uid, $type, $channel, $show, $subscribe, $subscribeId);
    }

    public static function subscribe($uid, $subscribe, $subscribeId)
    {
        return Model\WebPush::subscribe($uid, $subscribe, $subscribeId);
    }

    public static function getAll()
    {
        $result = Model\WebPush::getAll();

        return array_column($result, 'subscribe');
    }
}