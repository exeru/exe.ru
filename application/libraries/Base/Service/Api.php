<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 22.09.15
 * Time: 15:53
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Api
{
    const CONFIG = [
        'development' => [
            'GAME_API' => [
                1 => 'http://185.45.144.62:45332/',
                2 => 'http://185.45.144.62:45333/'
                //3 => 'http://185.45.144.62:45334/' // Остановлен, автозапуск отключен.
            ],
            'NOTIFICATION_API' => [
                1 => 'http://185.45.144.62:45990/'
            ],
            'ACTIVITY_API' => [
                1 => 'http://185.45.144.62:45890/'
            ]
        ],
        'testing' => [
            'GAME_API' => [
                1 => 'http://185.45.144.62:45332/',
                2 => 'http://185.45.144.62:45333/'
                //3 => 'http://185.45.144.62:45334/' // Остановлен, автозапуск отключен.
            ],
            'NOTIFICATION_API' => [
                1 => 'http://185.45.144.62:45990/'
            ],
            'ACTIVITY_API' => [
                1 => 'http://185.45.144.62:45890/'
            ]
        ],
        'production' => [
            'GAME_API' => [
                1 => 'http://172.16.101.55:45332/',
                2 => 'http://172.16.101.55:45333/'
            ],
            'NOTIFICATION_API' => [
                1 => 'http://172.16.101.55:45990/'
            ],
            'ACTIVITY_API' => [
                1 => 'http://172.16.101.55:45890/'
            ]
        ]
    ];

    const _API_ID = 1;
    const _FORMAT = 'json';

    public static $API_REQUESTS_ENABLED = true;

    public static function eventManager($eventType, $params)
    {
        switch($eventType) {
            case 'EVENT_USER_INSTALL_GAME': {
                self::usersChange($params['uid'], 4);
                $notifications = Game::getGameSettings($params['uid'], $params['gid'])['notifications'];
                self::gamersChange($params['uid'], $params['gid'], 1, $notifications, time());
                break;
            }
            case 'EVENT_USER_BLOCKED':
            case 'EVENT_USER_APPEND_TO_BLACKLIST':
            case 'EVENT_USER_REMOVE_FROM_BLACKLIST':
            case 'EVENT_USER_PURCHASE_RESTORE':
            case 'EVENT_USER_BALANCE_CHANGE':
            case 'EVENT_USER_BALANCE_CHANGE_BONUS':
            case 'EVENT_USER_ONLINE':
            case 'EVENT_USER_TYPING_ON':
            case 'EVENT_USER_TYPING_OFF':
            case 'EVENT_USER_SEND_MESSAGE':
            case 'EVENT_USER_HAS_READ_MESSAGE':
            case 'EVENT_USER_INVITE_TO_GAME': {
                self::informEvent($params['uids'], $params['message']);
                break;
            }
            case 'EVENT_USER_APPEND_FRIEND': {
                self::informEvent([$params['fid']], $params['message']);
                break;
            }
            case 'EVENT_USER_INVITE_TO_FRIEND_BY_GAME': {
                self::informEvent($params['fids'], $params['message']);
                break;
            }
            case 'EVENT_FRIENDS_CHANGE_COUNT': // Изменилось количество заявок в друзья
            case 'EVENT_DIALOGS_CHANGE_COUNT': // Изменилось количество диалогов
            case 'EVENT_NEWS_ADD': // Новость была добавлена
            case 'EVENT_NEWS_DELETE': { // Новость была удалена
                self::informEvent($params['fids'], $params['message']); // Сообщаем пользователям
                break;
            }
        }
    }

    // Активность, вызывать через события
    private static function informEvent(Array $uids, $jsonMessage)
    {
        if (count($uids) < 1) {
            return false;
        }

        $config = self::CONFIG[ENVIRONMENT];

        if (count($uids) > 10000) {
            set_time_limit(0);
        }

        $parts = array_chunk($uids, 500);

        foreach($parts as $part) {
            $params = [
                'api_id' => self::_API_ID,
                'format' => self::_FORMAT,
                'method' => 'informEvent',
                'user_ids' => implode(',', $part),
                'msg' => $jsonMessage
            ];

            self::send($config['ACTIVITY_API'], $params);
        }
    }

    // Игровое API
    /*public static function preloadData($uid, $sid)
    {
        $config = self::CONFIG[ENVIRONMENT];

        // $urlUsersGet1 = $apiHost . ':80' . $defaultRequest . 'method=getUserInfo&fields=sex,bdate,photo_50&user_ids=' . $uid . '&sid=' . $sid;
        $params = [
            'api_id' => self::_API_ID,
            'format' => self::_FORMAT,
            'method' => 'getUserInfo',
            'user_ids' => $uid,
            'sid' => $sid
        ];

        $errors = self::send(['http://185.45.144.62:80/', 'http://185.45.144.62:81/'], $params);
        var_dump($errors);

        // $urlIsAppUser1 = $apiHost . ':80' . $defaultRequest . 'method=isAppUser&user_id=' . $uid . '&sid=' . $sid;
        $params = [
            'api_id' => self::_API_ID,
            'format' => self::_FORMAT,
            'method' => 'isAppUser',
            'user_id' => $uid,
            'sid' => $sid
        ];

        $errors = self::send(['http://185.45.144.62:80/', 'http://185.45.144.62:81/'], $params);
        var_dump($errors);
    }*/

    // Игровое API
    public static function getLastActive($uid)
    {
        $params = [
            'api_id' => self::_API_ID,
            'format' => self::_FORMAT,
            'method' => 'lastActive',
            'user_id' => $uid
        ];

        $config = self::CONFIG[ENVIRONMENT];

        $maxTime = max(self::send($config['GAME_API'], $params, true));

        if ($maxTime < 1000) {
            return false;
        }

        return $maxTime;
    }

    // Игровое API
    public static function acceptUser($uid, $gid, $sid)
    {
        \Base\Service\Log::profile(__METHOD__);

        $params = [
            'api_id' => self::_API_ID,
            'format' => self::_FORMAT,
            'method' => 'acceptUser',
            'uid' => $uid,
            'gid' => $gid,
            'sid' => $sid
        ];

        $config = self::CONFIG[ENVIRONMENT];

        $errors = self::send($config['GAME_API'], $params);

        if (count($errors) == 0) {
            \Base\Service\Log::profile(__METHOD__);

            return true;
        }

        $result = Model\Api::acceptUser($uid, $gid, $sid, $errors);

        \Base\Service\Log::profile(__METHOD__);

        return $result;
    }

    // Игровое API
    public static function usersChange($uid, $change)
    {
        $params = [
            'api_id' => self::_API_ID,
            'format' => self::_FORMAT,
            'method' => 'updateUser',
            'user_id' => $uid,
            'change' => $change
        ];

        $config = self::CONFIG[ENVIRONMENT];

        $errors = self::send($config['GAME_API'], $params);

        if (count($errors) == 0) {
            return true;
        }

        return Model\Api::usersChange($uid, $change, $errors);
    }

    // Нотификации, изменения у игрока
    public static function gamersChange($uid, $gid, $installed, $notifications, $timestamp)
    {
        // Определяем маску состояния.
        $state = $installed + 2 * $notifications;

        $params = [
            'api_id' => self::_API_ID,
            'format' => self::_FORMAT,
            'method' => 'gamersChange',
            'uid' => $uid,
            'gid' => $gid,
            'state' => $state,
            'last_visit' => $timestamp
        ];

        $config = self::CONFIG[ENVIRONMENT];

        $errors = self::send($config['NOTIFICATION_API'], $params);

        if (count($errors) == 0) {
            return true;
        }

        return Model\Api::gamersChange($uid, $gid, $state, $timestamp, $errors);
    }

    // Нотификации, изменение видимости у игры
    public static function gamesChange($gid, $active, $enabled)
    {
        // Определяем маску состояния.
        $state = $active + 2 * $enabled;

        $params = [
            'api_id' => self::_API_ID,
            'format' => self::_FORMAT,
            'method' => 'gamesChange',
            'gid' => $gid,
            'state' => $state
        ];

        $config = self::CONFIG[ENVIRONMENT];

        $errors = self::send($config['NOTIFICATION_API'], $params);

        if (count($errors) == 0) {
            return true;
        }

        return Model\Api::gamesChange($gid, $state, $errors);
    }

    private static function send($urlsArray, $params, $returnResponse = false)
    {
        \Base\Service\Log::profile(__METHOD__);

        if (self::$API_REQUESTS_ENABLED !== true) {
            \Base\Service\Log::profile(__METHOD__);
            return false;
        }

        $params['rnd'] = rand();
        $queryString = '?' . http_build_query($params, null, '&');

        $mh = curl_multi_init();

        $ch = [];
        foreach($urlsArray as $cid => $url) {
            $ch[$cid] = curl_init();

            curl_setopt_array($ch[$cid], [
                CURLOPT_TIMEOUT => 1,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url . $queryString
            ]);

            curl_multi_add_handle($mh, $ch[$cid]);
        }

        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);

        $errors = [];
        foreach($ch as $cid=>$resource) {
            $response = curl_multi_getcontent($ch[$cid]);
            $requestInfo = curl_getinfo($ch[$cid]);

            curl_multi_remove_handle($mh, $ch[$cid]);
            curl_close($ch[$cid]);

            log_message('debug', var_export($response, true));
            log_message('debug', var_export($requestInfo, true));

            if ($requestInfo['http_code'] !== 200) {
                $errors[] = $cid;
                continue;
            }

            $responseObject = json_decode($response);

            if (!isset($responseObject->response) || ($responseObject->response !== true && $returnResponse === false)) {
                $errors[] = $cid;
            } elseif ($returnResponse === true){
                $errors[$cid] = $responseObject->response;
            }
        }

        \Base\Service\Log::profile(__METHOD__);
        return $errors;
    }
}