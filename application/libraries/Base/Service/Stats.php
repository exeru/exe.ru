<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14.04.16
 * Time: 16:04
 */

namespace Base\Service;

use \Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Stats extends Stat
{
    const STATS_GAME_SHAREDQUEUE = '__stats::game::sharedQueue';
    const STATS_GAME_MONTHS_UPDATE = '__stats::game::updated';

    public static function parseCrude()
    {
        if (!self::lock(__METHOD__)) {
            echo "locked \n";
            return false;
        }

        $queue = Redis::lRange(self::STATS_GAME_SHAREDQUEUE, 0, 1000);

        if (count($queue) < 1) {
            self::unlock(__METHOD__);
            echo "parsed: 0\n";
            return false;
        }

        $rows = [];
        foreach($queue as $item) {
            $item = unserialize($item);

            if (!isset($item['uid']) || $item['uid'] < 1) {
                var_dump($item);
                continue;
            }

            $user = User::getUserInfo($item['uid']);

            $row = [
                'gid' => $item['gid'],
                'uid' => $item['uid'],
                'day_number' => self::getDay($item['time']),
                'time' => $item['time'],
                'sex' => NULL,
                'birthday' => NULL,
                'country_id' => NULL,
                'city_id' => NULL,
                'age_group' => NULL
            ];

            if (isset($user['sex']) && ($user['sex'] == 0 || $user['sex'] == 1)) {
                $row['sex'] = $user['sex'];
            }

            if (isset($user['bday']) && $user['bday'] > 0 && isset($user['byear']) && $user['byear'] > 0) {
                $row['birthday'] = (strlen($user['bday']) == 4 ? $user['bday'] : '0' . $user['bday']) . $user['byear'];
                $row['age_group'] = self::getAgeGroup($row['birthday']);
            }

            if (isset($user['country_id']) && $user['country_id'] > 0) {
                $row['country_id'] = $user['country_id'];
            }

            if (isset($user['city_id']) && $user['city_id'] > 0) {
                $row['city_id'] = $user['city_id'];
            }
            $rows[] = $row;

            Redis::lPush(self::STATS_GAME_MONTHS_UPDATE, serialize(['gid' => $row['gid'], 'day_number' => $row['day_number']]));
        }

        Model\Stats::insertCrude($rows);


        Redis::lTrim(self::STATS_GAME_SHAREDQUEUE, count($queue));

        self::unlock(__METHOD__);

        echo "parsed: " . count($rows) . "\n";
        return true;
    }

    public static function updateMonths()
    {
        if (!self::lock(__METHOD__)) {
            echo "locked \n";
            return false;
        }

        if (Redis::lLen(self::STATS_GAME_MONTHS_UPDATE) === 0) {
            self::unlock(__METHOD__);
            echo "parsed: 0\n";
            return false;
        }

        // Подготавливаем массив обновлений
        $data = Redis::lRange(self::STATS_GAME_MONTHS_UPDATE, 0, 1000);
        $updated = [];
        foreach($data as $item) {
            $item = unserialize($item);
            $gid = $item['gid'];
            $timeByDayNumber = $item['day_number'] * 86400;
            $monthNumber = self::getMonth($timeByDayNumber);
            if (!isset($updated[$gid][$monthNumber])) {
                $updated[$gid][$monthNumber] = [
                    'dateStart' => (int) ceil(strtotime(date('01-m-Y 00:00:00', $timeByDayNumber)) / 86400),
                    'dateEnd' => (int) ceil(strtotime(date('t-m-Y 00:00:00', $timeByDayNumber)) / 86400)
                ];
            }
        }

        foreach($updated as $gid => $months) {
            foreach($months as $monthNumber => $dates) {
                // Актуальные значения:
                $actual = Model\Stats::getVisitsFromIntervalSummary($gid, $dates['dateStart'], $dates['dateEnd']);
                $old = Model\Stats::getVisitsFromMonthInterval($gid, $monthNumber, $monthNumber);
                if (count($old) > 0) {
                    $old = $old[0];
                    if ($old['unique'] != $actual['unique'] || $old['nounique'] != $actual['nounique']) {
                        echo "Updated " .$gid . ", " . $monthNumber . "\n";
                        Model\Stats::updateVisitsByMonths($gid, $monthNumber, $actual);
                    }
                } else {
                    echo "Inserted " .$gid . ", " . $monthNumber . "\n";
                    Model\Stats::insertVisitsByMonths($gid, $monthNumber, $actual);
                }
            }
        }

        Redis::lTrim(self::STATS_GAME_MONTHS_UPDATE, count($data));

        self::unlock(__METHOD__);
        return true;
    }

    public static function getStats($gid)
    {
        $data = [];
        $data['visits']['days'] = self::getVisitsByDays($gid);
        $data['visits']['months'] = self::getVisitsByMonths($gid);
        $data['age_gender'] = self::getAgeGender($gid);
        $data['geo'] = self::getGeo($gid);

        return $data;
    }

    public static function push($gid, $uid)
    {
        Log::profile(__METHOD__);
        $data = [
            'gid' => $gid,
            'uid' => $uid,
            'time' => time()
        ];

        Redis::lPush(self::STATS_GAME_SHAREDQUEUE, serialize($data));
        Log::profile(__METHOD__);
    }

    private static function getGeo($gid)
    {
        $today = self::getDay();
        $weekAgo = $today - 6;
        $monthAgo = $today - 29;

        $data = [];
        // День
        $country = Model\Stats::getCountryFromInterval($gid, $today, $today);
        $data['day']['country'] = array_column($country, 'unique', 'country_id');
        $city = Model\Stats::getCityFromInterval($gid, $today, $today);
        $data['day']['city'] =  array_column($city, 'unique', 'city_id');
        // Неделя
        $country = Model\Stats::getCountryFromInterval($gid, $weekAgo, $today);
        $data['week']['country'] = array_column($country, 'unique', 'country_id');
        $city = Model\Stats::getCityFromInterval($gid, $weekAgo, $today);
        $data['week']['city'] =  array_column($city, 'unique', 'city_id');
        // Месяц
        $country = Model\Stats::getCountryFromInterval($gid, $monthAgo, $today);
        $data['month']['country'] = array_column($country, 'unique', 'country_id');
        $city = Model\Stats::getCityFromInterval($gid, $monthAgo, $today);
        $data['month']['city'] =  array_column($city, 'unique', 'city_id');
        // За все время
        $country = Model\Stats::getCountryFromInterval($gid, 0, $today);
        $data['all']['country'] = array_column($country, 'unique', 'country_id');
        $city = Model\Stats::getCityFromInterval($gid, 0, $today);
        $data['all']['city'] =  array_column($city, 'unique', 'city_id');

        return $data;
    }

    private static function getAgeGender($gid)
    {
        $today = self::getDay();
        $weekAgo = $today - 6;
        $monthAgo = $today - 29;

        $groups = range(1, 8, 1);
        $weekData = $monthData = [
            'male' => array_fill_keys($groups, 0),
            'female' => array_fill_keys($groups, 0),
        ];

        $data = Model\Stats::getAgeSexFromInterval($gid, $weekAgo, $today);
        if (count($data) > 0) {
            foreach($data as $item) {
                $sex = ($item['sex'] == 1 ? 'male' : 'female');
                $weekData[$sex][$item['age_group']] = (int) $item['unique'];
            }
        }

        $data = Model\Stats::getAgeSexFromInterval($gid, $monthAgo, $today);
        if (count($data) > 0) {
            foreach($data as $item) {
                $sex = ($item['sex'] == 1 ? 'male' : 'female');
                $monthData[$sex][$item['age_group']] = (int) $item['unique'];
            }
        }

        return ['week' => $weekData, 'month' => $monthData];
    }

    private static function getVisitsByMonths($gid)
    {
        $thisMonth = self::getMonth();
        $yearAgo = $thisMonth - 11;

        $unique = $nounique = array_fill_keys(range($yearAgo, $thisMonth, 1), 0);

        $data = Model\Stats::getVisitsFromMonthInterval($gid, $yearAgo, $thisMonth);
        if (count($data) > 0) {
            $uniqueVisits = array_column($data, 'unique', 'month_number');
            $nouniqueVisits = array_column($data, 'nounique', 'month_number');
            foreach($uniqueVisits as $monthNumber => $count) {
                $unique[$monthNumber] = (int) $uniqueVisits[$monthNumber];
                $nounique[$monthNumber] = (int) $nouniqueVisits[$monthNumber];
            }
        }

        $startYear = (int) ($yearAgo / 12);
        $startMonth = $yearAgo - ($startYear * 12);

        $visits = [
            'unique' => $unique,
            'nounique' => $nounique,
            'start' => strtotime($startYear . '-' . $startMonth , '-01') + 10800,
            'end' => time()
        ];

        return $visits;
    }

    private static function getVisitsByDays($gid)
    {
        $today = self::getDay();
        $monthAgo = $today - 29;
        $unique = $nounique = array_fill_keys(range($monthAgo, $today, 1), 0);

        $data = Model\Stats::getVisitsFromInterval($gid, $monthAgo, $today);
        if (count($data) > 0) {
            foreach($data as $item) {
                $unique[$item['day_number']] = (int) $item['unique'];
                $nounique[$item['day_number']] = (int) $item['nounique'];
            }
        }

        $visits = [];
        $visits['unique'] = $unique;
        $visits['nounique'] = $nounique;
        $visits['sum_avarage'] = array_sum($unique) / 30;
        $visits['sum_unique'] = Model\Stats::getVisitsFromIntervalSummary($gid, $monthAgo, $today)['unique'];
        $visits['start'] = $monthAgo * 86400;
        $visits['end'] = $today * 86400;

        return $visits;
    }

    public static function getAgeGroup($birthday)
    {
        if (strlen($birthday) == 7) {
            $birthday = '0' . $birthday;
        }

        $day = $birthday[0] . $birthday[1];
        $month = $birthday[2] . $birthday[3];
        $year = $birthday[4] . $birthday[5] . $birthday[6] . $birthday[7];

        $old = date('Y') - $year;
        if ($month > date('m') || ($month == date('m') && $day > date('d'))) {
            $old--;
        }

        switch(true) {
            case $old < 18: {
                return 1; //'_18';
            }
            case $old >= 18 && $old < 21: {
                return 2; //'18_21';
            }
            case $old >= 21 && $old < 24: {
                return 3; //'21_24';
            }
            case $old >= 24 && $old < 27: {
                return 4; //'24_27';
            }
            case $old >= 27 && $old < 30: {
                return 5; //'27_30';
            }
            case $old >= 30 && $old < 35: {
                return 6; //'30_35';
            }
            case $old >= 35 && $old < 45: {
                return 7; //'35_45';
            }
            case $old >= 45: {
                return 8; //'45_';
            }
        }
    }

/*    public static function appendUserSession($uid, $number, $start, $end)
    {
        return Model\Stats::appendUserSession($uid, $number, $start, $end, ($end-$start));
    }*/

/*    public static function updateUserSession($uid, $number, $start, $end)
    {
        return Model\Stats::updateUserSession($uid, $number, $start, $end, ($end-$start));
    }*/
}