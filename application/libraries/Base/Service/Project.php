<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28.01.16
 * Time: 15:45
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Project
{
    public static function isMain()
    {
        return isset($_SERVER['HTTP_HOST']) && (strpos($_SERVER['HTTP_HOST'], 'test.exe.ru') === 0 || strpos($_SERVER['HTTP_HOST'], 'exe.ru') === 0);
    }

    public static function isDev()
    {
        return isset($_SERVER['HTTP_HOST']) && (strpos($_SERVER['HTTP_HOST'], 'dev.exe.ru') === 0 || strpos($_SERVER['HTTP_HOST'], 'devtest.exe.ru') === 0);
    }

    public static function isAdmin()
    {
        return isset($_SERVER['HTTP_HOST']) && (strpos($_SERVER['HTTP_HOST'], 'admin.exe.ru') === 0 || strpos($_SERVER['HTTP_HOST'], 'admintest.exe.ru') === 0);
    }

    public static function getBaseURI($type)
    {
        switch($type) {
            case 'dev': {
                return ENVIRONMENT == 'production' ? 'https://dev.exe.ru/' : (ENVIRONMENT == 'testing' ? 'https://devtest.exe.ru/' : 'http://dev.exe.ru.lan');
                break;
            }
            case 'admin': {
                return ENVIRONMENT == 'production' ? 'https://admin.exe.ru/' : (ENVIRONMENT == 'testing' ? 'https://admintest.exe.ru/' : 'http://admin.exe.ru.lan');
                break;
            }
            case 'main':
            default: {
                return ENVIRONMENT == 'production' ? 'https://exe.ru/' : (ENVIRONMENT == 'testing' ? 'https://test.exe.ru/' : 'http://exe.ru.lan');
            }
        }
    }

    public static function getEmails()
    {
        return ['xstudent@yandex.ru', 'admin@exe.ru'];
    }
}