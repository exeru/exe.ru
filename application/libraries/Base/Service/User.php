<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.08.15
 * Time: 16:54
 */

namespace Base\Service;
use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class User
{
    const TIME_NOTIFICATION_ONLINE = 60;

    const MAX_PREVIEW_WIDTH = 834;
    const MAX_PREVIEW_HEIGHT = 480;

    private static $cache_users_by_uid = null;

    public static function getRetentionLabel($uid, $time)
    {
        $retentionTime = Model\User::getRetentionTime($uid, $time);

        if ($retentionTime === false) {
            return false;
        }

        $retentionDay = (int) ceil(($time - $retentionTime) / 86400);

        if (!in_array($retentionDay, [2, 7, 30])) {
            return false;
        }

        return $retentionDay;
    }

    public static function getLastPaymentTime()
    {
        return Model\User::getLastPaymentTime();
    }

    public static function getFirstPaymentTime()
    {
        return Model\User::getFirstPaymentTime();
    }

    public static function getPayersDataByPeriod($start, $end, $groupField, $isInit)
    {
        return Model\User::getPayersDataByPeriod($start, $end, $groupField, $isInit);
    }

    public static function getUsersMaxTime($init)
    {
        return Model\User::getUsersMaxTime($init);
    }

    public static function getUsersMinTime($init)
    {
        return Model\User::getUsersMinTime($init);
    }

    public static function getMaxActiveTimeForUids($uids, $start, $end)
    {
        return Model\User::getMaxActiveTimeForUids($uids, $start, $end);
    }

    public static function getUsersDurationByParams($start, $end, $max, $min, $fieldGroup, $init)
    {
        return Model\User::getUsersDurationByParams($start, $end, $max, $min, $fieldGroup, $init);
    }

    public static function getAdvertMinTime()
    {
        return Model\User::getAdvertMinTime();
    }

    public static function getAdvertMaxTime()
    {
        return Model\User::getAdvertMaxTime();
    }

    public static function getAdvertGroupsByPeriod($start, $end)
    {
        return Model\User::getAdvertGroupsByPeriod($start, $end);
    }

    public static function getAdvertByGroupsByPeriod($start, $end, $fieldGroup)
    {
        return Model\User::getAdvertByGroupsByPeriod($start, $end, $fieldGroup);
    }

    public static function updateMarkersDuration($time)
    {
        if (!self::lock(__METHOD__, 3600)) {
            echo "Locked \n";
            return false;
        }

        echo "\n\n Start updateMarkersDuration \n";

        $markers = Redis::hKeys(RedisKeys::userMarkerTime);

        echo "Count markers: " . count($markers) . "\n";
        if (count($markers) > 0) {
            foreach($markers as $marker) {
                echo "Update marker '" . $marker . "'\n";
                $markerTime = Redis::hGet(RedisKeys::userMarkerTime, $marker);
                echo "RedisKeys::userMarkerTime '" . $markerTime . "'\n";
                $markerUid = Redis::hGet(RedisKeys::userMarkerUid, $marker);
                echo "RedisKeys::userMarkerUid '" . $markerUid . "'\n";
                if ($markerUid > 0) {
                    // Проверим, что у пользователя не изменился маркер
                    $mainMarker = Redis::hGet(RedisKeys::userMainMarkerByUid, $markerUid);
                    echo "RedisKeys::userMainMarkerByUid (mainMarker) '" . $mainMarker . "'\n";
                    $currentMarker = self::getMarkerByMainMarker($mainMarker);
                    echo "RedisKeys::userMarkerByMainMarker (currentMarker) '" . $currentMarker . "'\n";
                    if ($currentMarker == $marker) {
                        echo "mainMarker == currentMarker\n";
                        $timeFromApi = Api::getLastActive($markerUid);
                        echo "time from API '" . $timeFromApi . "'\n";
                        if ($timeFromApi !== false && $markerTime < $timeFromApi) {
                            echo "markerTime('" . $markerTime . "') < timeFromAPI('" . $timeFromApi . "'), set markerTime = timeFromApi \n";
                            $markerTime = $timeFromApi;
                        }
                    }
                }

                if ($markerTime < $time) {
                    echo "Close {$marker}: markerTime " . date('d/m/Y H:i:s', $markerTime) . ', 30 minutes ago '. date('d/m/Y H:i:s', $time) . "\n";
                    self::closeUserMarker($marker, $markerTime);
                } else {
                    echo "Update markerTime in Redis \n";
                    Redis::hSet(RedisKeys::userMarkerTime, $marker, $markerTime);
                }
                echo "End update marker '".$marker."'\n\n";
            }
        }

        echo "\n\n End updateMarkersDuration \n";

        self::unlock(__METHOD__);
    }

    public static function assignMarkerWithLFId($marker, $uid)
    {
        Model\User::assignMarkerWithLFId($marker, $uid);
    }

    public static function closeUserMarker($marker, $time)
    {
        $crontask = !isset($_SERVER['HTTP_HOST']) || empty($_SERVER['HTTP_HOST']);

        if ($crontask) {
            echo "closeUserMarker: Close UserMarker '".$marker."' with time " . $time . "\n";
        }

        Redis::hDel(RedisKeys::userMarkerTime, $marker);
        Redis::hDel(RedisKeys::userMarkerUid, $marker);
        Redis::hDel(RedisKeys::userMarkerSessionNumber, $marker);

        $mainMarker = Model\User::getMainMarkerByMarker($marker);
        if ($mainMarker) {
            self::setMarkerByMainMarker($mainMarker, null);
            Redis::del($mainMarker, Redis::__DB_MARKER_BY_MAIN_MARKER);
        }

        $redisKeyMarkerSidTime = RedisKeys::userMarkerUserSession . $marker . '::';
        $sids = Redis::hKeys($redisKeyMarkerSidTime);

        if ($crontask) {
            echo "Sids keys: " . count($sids) . "\n";
        }

        if (count($sids) > 0) {
            foreach($sids as $sid) {
                $sidTime = Redis::hGet($redisKeyMarkerSidTime, $sid);
                if ($sidTime > 0) {
                    if ($crontask) {
                        echo "Update sid '" . $sid . "', time " . date('d-m-Y H:i:s', $sidTime) . "\n";
                    }
                    self::uptimeSession($sid, $sidTime);
                }
            }
            Redis::del($redisKeyMarkerSidTime);
        }

        if ($markerData = self::getReferByMarker($marker)) {
            if ($crontask) {
                echo "closeUserMarker: markerData " . var_export($markerData, true) . "\n";
            }
            $lfUid = $markerData['lf_uid'];
            $start = $markerData['start'];
            $end = $time;

            $log = UserLog::getLogByMarker($marker, $start, $end);
            if ($crontask) {
                echo "closeUserMarker: userlog:  " . var_export($log, true) . "\n";
            }
            if (count($log) > 0) {
                $sessionData = [
                    'id' => $markerData['id'],
                    'marker' => $markerData['marker'],
                    'ip' => $markerData['ip'],
                    'agent' => $markerData['agent'],
                    'entry_point' => $markerData['point'],
                    'referer' => $markerData['referer'],
                    'adv_service' => $markerData['adv_service'],
                    'adv_campaign' => $markerData['adv_campaign'],
                    'adv_marker' => $markerData['adv_marker'],
                    'adv_place' => $markerData['adv_place'],
                    'time_start' => $markerData['start'],
                    'time_end' => $time,
                    'time_duration' => ($time - $markerData['start']) > 0 ? ($time - $markerData['start']) : 0,
                    'uid' => null,
                    'init' => false,
                    'pay_count' => 0,
                    'pay_sum' => 0,
                    'yyyymmdd' => date('Ymd', $markerData['start']),
                    'yyyymmddhh' => date('YmdH', $markerData['start']),
                    'yyyymm' => date('Ym', $markerData['start'])
                ];

                $actions = array_unique(array_column($log, 'action'));

                if (count(array_intersect($actions, [UserLog::REGISTER, UserLog::REGISTER_SOCIAL, UserLog::NEW_TEMPORARY])) > 0
                    && count(array_intersect($actions, [UserLog::TEMPORARY_AUTH_AS_USER, UserLog::USER_AUTH_FROM_TEMPORARY])) == 0) {
                    $sessionData['init'] = true;
                }

                if (in_array(UserLog::TO_BALANCE, $actions)) {
                    foreach($log as $row) {
                        if ($row['action'] == UserLog::TO_BALANCE) {
                            $sessionData['pay_count']++;
                            $sessionData['pay_sum'] += (isset($row['params']['count']) ? $row['params']['count'] : $row['params'][0]);
                        }
                    }
                }
                if ($crontask) {
                    echo "closeUserMarker: actions:  " . var_export($actions, true) . "\n";
                }
                // Если есть страдательные действия, исключим их из лога
                if (
                    in_array(UserLog::USER_TO_BLACKLIST, $actions)
                    || in_array(UserLog::USER_FROM_BLACKLIST, $actions)
                    || in_array(UserLog::USER_TO_FRIENDS, $actions)
                    || in_array(UserLog::USER_FROM_FRIENDS, $actions)
                ) {
                    foreach($log as $index=>$row) {
                        if (in_array($row['action'], [UserLog::USER_TO_BLACKLIST, UserLog::USER_FROM_BLACKLIST, UserLog::USER_TO_FRIENDS, UserLog::USER_FROM_FRIENDS])) {
                            echo "closeUserMarker: unset element userlog, index '" . $index ."':  " . var_export($log, true) . "\n";
                            unset($log[$index]);
                        }
                    }
                    if ($crontask) {
                        echo "closeUserMarker: fixed userlog:  " . var_export($log, true) . "\n";
                    }
                }

                $uids = array_unique(array_column($log, 'uid'));
                if ($crontask) {
                    echo "closeUserMarker: userUids:  " . var_export($uids, true) . "\n";
                }
                if (count($uids) > 0) {
                    // Обновим временные метки в таблицах игроков и игровых сессий
                    $redisKeyMarkerGSidTime = RedisKeys::userMarkerGameSession . $marker . '::';
                    $gsids = Redis::hKeys($redisKeyMarkerGSidTime);

                    if ($crontask) {
                        echo "GSids keys: " . count($gsids) . "\n";
                    }

                    if (count($gsids) > 0) {
                        foreach($gsids as $gsid) {
                            $gsidTime = Redis::hGet($redisKeyMarkerGSidTime, $gsid);
                            if ($gsidTime > 0) {
                                if ($crontask) {
                                    echo "Update gsid '" . $gsid . "', time " . date('d-m-Y H:i:s', $gsidTime) . "\n";
                                }
                                $gid = Game::getGameIdBySid($gsid);
                                foreach($uids as $uid) {
                                    Game::uptimeGamer($uid, $gid);
                                    Game::uptimeSession($uid, $gid);
                                }
                            }
                        }
                        Redis::del($redisKeyMarkerGSidTime);
                    }

                    if ($lfUid > 0) {
                        LFRetention::setAssign($lfUid, $uids);
                        if ($crontask) {
                            echo "closeUserMarker: setAssign: lf_uid = " . $lfUid . ", uids " . var_export($uids, true) . "\n";
                        }
                    }
                    // Получаем основные идентификаторы пользователя
                    $allUids = [];
                    foreach($uids as $uid) {
                        if (self::isTemporaryUser($uid)) {
                            $allUids[] = $uid;
                            $temp = self::getUidByTempUid($uid);
                            if ($temp !== false) {
                                $allUids[] = $temp;
                            }
                            if (is_null($sessionData['uid'])) {
                                $sessionData['uid'] = $uid;
                            }
                        } else {
                            $allUids[] = $uid;
                            $sessionData['uid'] = $uid;
                        }
                    }

                    Model\User::pushSession($sessionData);

                    // Получаем идентификаторы лоста по нашим идентификаторам
                    $lfUids = LFRetention::getLFUidsByUids($allUids);

                    if (count($lfUids) > 0) {
                        $games = Game::getLastVisitTimeByUids($allUids);

                        if (count($games) > 0) {
                            foreach($lfUids as $lfUid) {
                                foreach($games as $index=>$value) {
                                    $games[$index]['key'] = md5($lfUid . microtime(true) . $value['gid']);
                                }
                            }
                        }

                        LFRetention::update($lfUid, $games);
                    }
                }
            }
        }

        Model\User::closeReferer($marker, $time);
    }

    public static function getUidByTempUid($uid)
    {
        $result = Model\User::getUidByTempUid($uid);

        return $result;
    }

    public static function getLFDataByMarker($marker)
    {
        return Model\User::getLFIdByMarker($marker);
    }

    public static function getUserMainMarker($mainMarker)
    {
        Log::profile(__METHOD__);
        if (Auth::isAuth()) {
            $savedMarker = Redis::hGet(RedisKeys::userMainMarkerByUid, Auth::getUserId());
            if ($savedMarker) {
                Log::profile(__METHOD__);
                return $savedMarker;
            }

            if (empty($mainMarker) || !preg_match('|^[0-9a-f]{32}$|', $mainMarker)) {
                $mainMarker = md5('_main_' . microtime(true));
            }

            Redis::hSet(RedisKeys::userMainMarkerByUid, Auth::getUserId(), $mainMarker);
            Log::profile(__METHOD__);
            return $mainMarker;
        }

        if (empty($mainMarker)) {
            $mainMarker = md5('_main_' . microtime(true));
        }

        Log::profile(__METHOD__);
        return $mainMarker;
    }

    public static function getUserMarker($mainMarker, $marker = null, $openstat = null)
    {
        Log::profile(__METHOD__);

        $markerByMainMarker = self::getMarkerByMainMarker($mainMarker);

        if ($markerByMainMarker) {
            $marker = $markerByMainMarker;
        }

        $outerRefer = self::getOuterRefer();

        if (!empty($marker) && preg_match('|^[0-9a-f]{32}$|', $marker)) { // Есть маркер, заход из любой точки
            // Проверяем, прошла ли минута с последней активности?
            $markerTime = Redis::hGet(RedisKeys::userMarkerTime, $marker);
            if ($markerTime > 0 && (time() - $markerTime) <= 60) { // Маркер свежий
                Redis::hSet(RedisKeys::userMarkerTime, $marker, time());
                Log::profile(__METHOD__);
                return $marker;
            }
        }

        $sessionNumber = self::getLastSessionNumberByMainMarker($mainMarker);
        if ($sessionNumber < 1) {
            $sessionNumber = 0;
        }

        if (empty($marker) || !preg_match('|^[0-9a-f]{32}$|', $marker) || $outerRefer !== false) { // Маркера нет, или переход не внутренний
            $marker = md5(microtime(true));

            self::setLastSessionNumberByMainMarker($mainMarker, ++$sessionNumber);
            self::setMarkerByMainMarker($mainMarker, $marker);
            Redis::hSet(RedisKeys::userMarkerTime, $marker, time());
            Redis::hSet(RedisKeys::userMarkerSessionNumber, $marker, $sessionNumber);

            if (Auth::isAuth()) {
                Redis::hSet(RedisKeys::userMarkerUid, $marker, Auth::getUserId());
            }

            self::pushReferer($mainMarker, $sessionNumber, $marker, $openstat);

            Log::profile(__METHOD__);
            return $marker;
        }

        $markerTime = Redis::hGet(RedisKeys::userMarkerTime, $marker);
        if (Auth::isAuth()) {
            $timeFromApi = Api::getLastActive(Auth::getUserId());
            if ($timeFromApi !== false && $markerTime < $timeFromApi) {
                $markerTime = $timeFromApi;
                Redis::hSet(RedisKeys::userMarkerTime, $marker, $timeFromApi);
            }
        }

        if ($markerTime < 1 || (time() - ($markerTime + 1800)) > 0) { // Маркер устарел
            $marker = md5(microtime(true));
            self::setLastSessionNumberByMainMarker($mainMarker, ++$sessionNumber);
            self::setMarkerByMainMarker($mainMarker, $marker);
            Redis::hSet(RedisKeys::userMarkerTime, $marker, time());
            Redis::hSet(RedisKeys::userMarkerSessionNumber, $marker, $sessionNumber);

            self::pushReferer($mainMarker, $sessionNumber, $marker, $openstat);
        } else {
            // Продляем маркер
            Redis::hSet(RedisKeys::userMarkerTime, $marker, time());
        }

        Log::profile(__METHOD__);
        return $marker;
    }

    public static function getUserSessionNumberByMarker($marker)
    {
        return Redis::hGet(RedisKeys::userMarkerSessionNumber, $marker);
    }

    public static function getReferByMarker($marker)
    {
        return Model\User::getReferByMarker($marker);
    }

    private static function getOuterRefer()
    {
        if (empty($_SERVER['HTTP_REFERER'])) {
            return true;
        }

        $refererHost = @parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        $ownHost = parse_url(config_item('base_url'), PHP_URL_HOST);

        $referer = ($refererHost && $refererHost !== $ownHost);

        if (strpos($_SERVER['REQUEST_URI'], '/auth_') === 0
            || strpos($_SERVER['REQUEST_URI'], '/acceptUser') === 0
            || strpos($_SERVER['REQUEST_URI'], '/gw/') === 0) {
            return false;
        }

        if ($referer === false) {
            return false;
        }

        return $_SERVER['HTTP_REFERER'];
    }

    private static function pushReferer($mainMarker, $sessionNumber, $marker, $openstat = null)
    {
        Log::profile(__METHOD__);
        $uid = null;
        if (Auth::isAuth()) {
            $uid = Auth::getUserId();
        }

        if (!empty($openstat)) {
            $openstat = base64_decode($openstat);
            if (preg_match('|^[a-zA-Z0-9:;\-._ а-яА-Яй]+$|u', $openstat)) {
                $parts = explode(';', $openstat);
            }
        }

        $openStatData = [];
        $openStatData['adv_service']  = isset($parts[0]) ? $parts[0] : null;
        $openStatData['adv_campaign'] = isset($parts[1]) ? $parts[1] : null;
        $openStatData['adv_marker']   = isset($parts[2]) ? $parts[2] : null;
        $openStatData['adv_place']    = isset($parts[3]) ? $parts[3] : null;

        $ip = $_SERVER['REMOTE_ADDR'];
        $agent = trim($_SERVER['HTTP_USER_AGENT']);

        if (empty($_SERVER['HTTP_REFERER'])) {
            // Записываем звездочку, прямой переход
            Model\User::pushReferer($mainMarker, $sessionNumber, $marker, time(), $_SERVER['REQUEST_URI'], '*', $uid, $openStatData, $ip, $agent);
            Log::profile(__METHOD__);
            return;
        } else {
            $refererHost = @parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
            $ownHost = parse_url(config_item('base_url'), PHP_URL_HOST);

            $referer = ($refererHost && $refererHost !== $ownHost);
            if ($referer !== false) {
                Model\User::pushReferer($mainMarker, $sessionNumber, $marker, time(),  $_SERVER['REQUEST_URI'], $_SERVER['HTTP_REFERER'], $uid, $openStatData, $ip, $agent);
                Log::profile(__METHOD__);
                return;
            } else {
                // Источник перехода - страница нашего сайта, ничего не пишем
                Model\User::pushReferer($mainMarker, $sessionNumber, $marker, time(), $_SERVER['REQUEST_URI'], '*', $uid, $openStatData, $ip, $agent);
                Log::profile(__METHOD__);
                return;
            }
        }
    }

    public static function createNewTemporary()
    {
        Log::profile(__METHOD__);

        $name = Hash::getRandomChars();
        $password = Hash::getRandomChars();
        // Регистрируем нового пользователя
        User::registrate($name, $name . '@local', $password);

        // Получаем идентификатор нового пользователя
        $uid = User::getUserIdByAuthData($name . '@local', $password);

        Auth::loginAsUser($uid);
        Auth::remember();

        UserLog::save($uid, null, UserLog::NEW_TEMPORARY, null, Auth::getUserMarker());

        User::setUserInfo($uid, [
            'name' => 'Пользователь  ' . $uid,
            'verified' => 0
        ]);

        Log::profile(__METHOD__);
        return $uid;
    }

    public static function isBot($uid)
    {
        $user = self::getUserInfo($uid, true);

        return ($user['is_bot'] === 't');
    }

    public static function getSessionId($uid)
    {
        $time = time();

        $redisKeyTime = RedisKeys::userLastActive . 'time::';
        $redisKeySid = RedisKeys::userLastActive . 'sid::';
        $redisKeyMarkerSidTime = RedisKeys::userMarkerUserSession . Auth::getUserMarker() . '::';

        $lastActiveTime = Redis::hGet($redisKeyTime, $uid);
        $lastActiveSid = Redis::hGet($redisKeySid, $uid);

        if ($lastActiveTime < 1 || $lastActiveSid < 1 || !self::uptimeSession($lastActiveSid, $time)) {
            $lastActiveSid = Model\User::getSessionId($uid);
            if ($lastActiveSid === false) {
                $lastActiveSid = Model\User::createSession($uid);
            }
            Redis::hSet($redisKeySid, $uid, $lastActiveSid);
        }

        Redis::hSet($redisKeyMarkerSidTime, $lastActiveSid, $time);
        Redis::hSet($redisKeyTime, $uid, $time);

        return $lastActiveSid;
    }

    public static function uptimeSession($lastActiveSid, $time = false)
    {
        if ($time === false) {
            $time = time();
        }

        return Model\User::uptimeSession($lastActiveSid, $time);
    }

    public static function deleteOldSessions()
    {
        return Model\User::deleteOldSessions(time() - 31 * 86400);
    }

    public static function getIsOnlineByUids($uids)
    {
        if (count($uids) > 0) {
            $keys = $users = $result = [];
            foreach($uids as $uid) {
                $keys[] = self::getRedisKeyByType('online', $uid);
                $users[] = $uid;
            }

            $times = Redis::mGet($keys);
            foreach($times as $index=>$time) {
                if ($time === false || $time < (time() - self::TIME_NOTIFICATION_ONLINE)) {
                    $result[$users[$index]] = false;
                } else {
                    $result[$users[$index]] = true;
                }
            }

            return $result;
        }

        return false;
    }

    public static function isOnline($uid)
    {
        $key = self::getRedisKeyByType('online', $uid);

        $time = Redis::get($key);

        if ($time === false || $time < (time() - self::TIME_NOTIFICATION_ONLINE)) {
            return false;
        }

        return true;
    }

    public static function isSocialEmail($email)
    {
        return preg_match('|^\d+@\w{2}$|', $email);
    }

    public static function isUserExistsByEmail($email)
    {
        return self::getUserIdByEmail($email) !== false;
    }

    public static function isEmailChecked($uid)
    {
        return self::getUserInfo($uid, true)['email_checked'] === 't';
    }

    public static function setEmailUnChecked($uid)
    {
        self::setUserInfo($uid, ['email_checked' => false]);

        self::updateRating($uid);

        return true;
    }

    public static function setEmailChecked($uid)
    {
        self::setUserInfo($uid, ['email_checked' => true, 'bounced' => false]);

        self::updateRating($uid);

        return true;
    }

    public static function getUserStatus($uid)
    {
        return Model\User::getUserStatus($uid);
    }

    public static function setUserStatus($uid, $status)
    {
        return Model\User::setUserStatus($uid, $status);
    }

    public static function getUserEmail($uid)
    {
        return Model\User::getUserEmail($uid);
    }

    public static function getUserInfo($uid, $privateFields = false)
    {
        \Base\Service\Log::profile(__METHOD__);

        if (is_null(self::$cache_users_by_uid) || !isset(self::$cache_users_by_uid[$uid])) {
            $redisKey = self::getRedisKeyByType('userinfo', $uid);
            $data = Redis::get($redisKey);
            if ($data !== false) {
                $data = unserialize($data);
            } else {
                $intUid = (int) $uid;

                if ($intUid != $uid) {
                    ob_start();
                    debug_print_backtrace();
                    $a = ob_get_contents();
                    ob_end_clean();
                    log_message('error', $a);
                    $uid = $intUid;
                }

                $data = Model\User::getUserInfo($uid);
                if (count($data) == 0) {
                    return false;
                } else {
                    $data = $data[0];
                }
                Redis::set($redisKey, serialize($data), 3600);
            }
            $data['photo'] = self::getUserPhotos($uid, $data['photo'], [70, 120, 270]);
            self::$cache_users_by_uid[$uid] = $data;
        } else {
            $data = self::$cache_users_by_uid[$uid];
        }

        $data['online'] = self::isOnline($uid);

        // Если не указано явно на необходимость возвращать частные поля, не будем их возвращать.
        if ($privateFields !== true) {
            unset($data['verified'], $data['balance'], $data['email_checked'], $data['is_bot'], $data['approved']);
        }

        \Base\Service\Log::profile(__METHOD__);

        return $data;
    }

    public static function isTemporaryUser($uid) {
        return self::getUserInfo($uid, true)['verified'] != 1;
    }

    public static function getUserIdByAuthData($email, $password)
    {
        return Model\User::getUserIdByAuthData($email, $password);
    }

    public static function getUserIdBySocialId($socialId)
    {
        return Model\User::getUserIdBySocialId($socialId);
    }

    public static function getUserIdByEmail($email)
    {
        return Model\User::getUserIdByEmail($email);
    }

    public static function getSetOnline($uid)
    {
        $online = self::isOnline($uid);

        $key = self::getRedisKeyByType('online', $uid);
        Redis::set($key, time(), self::TIME_NOTIFICATION_ONLINE + 60);

        if ($online === false) {
            $userData = self::getUserInfo($uid);
            Events::rise('EVENT_USER_ONLINE', [
                'uids' => Friends::getFriendsByTypeArray($uid)['both'],
                'message' => json_encode([
                    'type' => 'online',
                    'uid' => $uid,
                    'name' => $userData['name'],
                    'last_name' => $userData['last_name'],
                    'image' => $userData['photo'][70]
                ])
            ]);
        }

        return $online;
    }

    public static function setEmailComplaint($uid)
    {
        self::setUserInfo($uid, ['bounced' => true, 'subscribe' => false]);

        self::updateRating($uid);

        return true;
    }

    public static function setEmailBounced($uid)
    {
        self::setUserInfo($uid, ['email_checked' => false, 'bounced' => true]);

        self::updateRating($uid);

        return true;
    }

    public static function setUserEmail($uid, $email)
    {
        return Model\User::setUserEmail($uid, $email);
    }

    public static function setUserPhoto($uid, $localFilename)
    {
        $filename = (false === $pos = strrpos($localFilename, '/')) ? $localFilename : substr($localFilename, $pos + 1);
        $filename = (false === $pos = strrpos($filename, '.')) ? $filename : substr($filename, 0, $pos);

        $imageInfo = getimagesize($localFilename);

        switch($imageInfo['mime']) {
            case 'image/jpeg': {
                $filename .= '.jpg';
                break;
            }
            case 'image/png': {
                $filename .= '.png';
                break;
            }
            case 'image/gif': {
                $filename .= '.gif';
                break;
            }
        }

        $subPath = self::getSubPathByUid($uid);

        $paths = [
            [
                'width' => 270,
                'height' => 270,
                'path' => FCPATH . 'assets/img/users/270/' . $subPath . '/'. $uid . '_' . $filename,
                'symlink' => FCPATH . 'assets/img/users/270/' . $subPath . '/' . $uid . '_200_orig' . $filename
            ],
            [
                'width' => 120,
                'height' => 120,
                'path' => FCPATH . 'assets/img/users/120/' . $subPath . '/' . $uid . '_' . $filename,
                'symlink' => FCPATH . 'assets/img/users/120/' . $subPath . '/' . $uid . '_100' . $filename
            ],
            [
                'width' => 70,
                'height' => 70,
                'path' => FCPATH . 'assets/img/users/70/' . $subPath . '/' . $uid . '_' . $filename,
                'symlink' => FCPATH . 'assets/img/users/70/' . $subPath . '/' . $uid . '_50' . $filename
            ]
        ];

        foreach($paths as $image) {
            $dirname = dirname($image['path']);
            if (!is_dir($dirname)) {
                mkdir($dirname, 0755, true);
            }
            copy($localFilename, $image['path']);
            chmod($image['path'], 0755);

            Image::resize($image['path'], $image['width'], $image['height']);

            File::upload($image['path']);
            log_message('debug', File::$result);
            log_message('debug', var_export(File::$info, true));

            if (isset($image['symlink'])) {
                File::symlink($image['path'], $image['symlink']);
                log_message('debug', File::$result);
                log_message('debug', var_export(File::$info, true));
            }
        }

        $data = [
            'photo' => $filename
        ];

        return self::setUserInfo($uid, $data);
    }

    public static function setUserInfo($uid, $data)
    {
        Model\User::setUserInfo($uid, $data);

        self::invalidateUserInfoCache($uid);

        Api::usersChange($uid, 1);

        return true;
    }

    public static function setUserPasswordByEmail($email, $password)
    {
        $uid = Model\User::getUserIdByEmail($email);

        return Model\User::setUserPassword($uid, $password);
    }

    public static function assignUidWithTempUid($uid, $tempUid)
    {
        $currentUserMainMarker = Redis::hGet(RedisKeys::userMainMarkerByUid, $tempUid);
        $currentUserMarkerByMainMarker = self::getMarkerByMainMarker($currentUserMainMarker);

        $anotherUserMainMarker = Redis::hGet(RedisKeys::userMainMarkerByUid, $uid);
        if ($anotherUserMainMarker) { // У другого пользователя есть главный маркер

            $lastSessionNumber = self::getLastSessionNumberByMainMarker($anotherUserMainMarker);

            $anotherUserMarkerByMainMarker = self::getMarkerByMainMarker($anotherUserMainMarker);
            if ($anotherUserMarkerByMainMarker) { // И есть незакрытая текущая сессия
                $anotherUserMarkerTime = Redis::hGet(RedisKeys::userMarkerTime, $anotherUserMarkerByMainMarker);
                $timeFromApi = Api::getLastActive($uid);
                if ($timeFromApi !== false && $anotherUserMarkerTime < $timeFromApi) {
                    $anotherUserMarkerTime = $timeFromApi;
                }
                if ($anotherUserMarkerTime > 0) {
                    self::closeUserMarker($anotherUserMarkerByMainMarker, $anotherUserMarkerTime);
                }
            }

            self::setLastSessionNumberByMainMarker($anotherUserMainMarker, ++$lastSessionNumber);
            self::setMarkerByMainMarker($anotherUserMainMarker, $currentUserMarkerByMainMarker);
            Redis::hSet(RedisKeys::userMarkerSessionNumber, $currentUserMarkerByMainMarker, $lastSessionNumber);
        } else {
            Redis::hDel(RedisKeys::userMainMarkerByUid, $tempUid);
            Redis::hSet(RedisKeys::userMainMarkerByUid, $uid, $currentUserMainMarker);
        }

        Redis::hSet(RedisKeys::userMarkerUid, $currentUserMarkerByMainMarker, $uid);

        UserLog::save($uid, null, UserLog::USER_AUTH_FROM_TEMPORARY, ['uid' => $uid, 'tempuid' => $tempUid], Auth::getUserMarker());
        UserLog::save($tempUid, null, UserLog::TEMPORARY_AUTH_AS_USER, ['uid' => $uid, 'tempuid' => $tempUid], Auth::getUserMarker());

        return Model\User::assignUidWithTempUid($uid, $tempUid);
    }

    public static function assignUidWithSocialId($uid, $socialId)
    {
        return Model\User::assignUidWithSocialId($uid, $socialId);
    }

    public static function registrate($name, $email, $password, $phone = '', $status = 1, $emailChecked = false)
    {
        if (Model\User::getUserIdByEmail($email) !== false) {
            return false;
        }

        $uid = Model\User::createUser($name, $email, $password, $phone, $status, $emailChecked, Feeds::getMaxFeedId());

        return $uid > 0;
    }

    public static function searchUsersByName($uid, $searchString, $except = [], $onlyFrom = [], $limit = 10, $offset = 0)
    {
        $parts = explode(' ', $searchString);
        // Отбросим все с длинной менее 3-х символов
        foreach($parts as $key=>$value) {
            if (strlen($value) < 3) {
                unset($parts[$key]);
            }
        }

        if (count($parts) < 1) {
            return [];
        }

        $users = Model\User::searchUsersByName($uid, $parts, $except, $onlyFrom, $limit, $offset);

        // Для каждого найденного пользователя получим его фотографии
        foreach($users as $key=>$user) {
            $users[$key]['photo'] = self::getUserPhotos($user['uid'], $user['photo'], [120])[120];
        }

        return $users;
    }

    public static function updateRating($uid)
    {
        $isApproved = self::isApproved($uid);
        $isEmailChecked = self::isEmailChecked($uid);

        $rating = 0;

        if (self::isTemporaryUser($uid)) {
            $rating = 0;
        } else if (self::isBot($uid)) {
            $rating = 3;
        } else if (!$isApproved && !$isEmailChecked) {
            $rating = 1;
        } else if (!$isApproved && $isEmailChecked) {
            $rating = 2;
        } else if ($isApproved && !$isEmailChecked) {
            $rating = 4;
        } else if ($isApproved && $isEmailChecked) {
            $rating = 5;
        }

        self::setUserInfo($uid, ['rating' => $rating]);

        UserLog::save($uid, null, UserLog::RATING_UPDATE, [$rating]);
    }

    public static function isApproved($uid)
    {
        return 't' === self::getUserInfo($uid, true)['approved'];
    }

    public static function setUnApproved($uid)
    {
        self::setUserInfo($uid, ['approved' => false]);

        self::updateRating($uid);

        return true;
    }

    public static function setApproved($uid)
    {
        self::setUserInfo($uid, ['approved' => true]);

        self::updateRating($uid);

        return true;
    }

    private static function getSubPathByUid($uid)
    {
        if ($uid < 10) {
            $uid = '00'. $uid;
        } elseif($uid < 100) {
            $uid = '0'. $uid;
        } else {
            $uid = (string) $uid;
        }

        return substr($uid, -3, 1) . '/' . substr($uid, -2, 1) . '/' . substr($uid, -1, 1);
    }

    private static function getUserPhotos($uid = '', $filename = '', $sizes = [70, 120, 270])
    {
        if (is_string($sizes) || is_numeric($sizes)) {
            $sizes = (int) $sizes;
        }

        if (is_int($sizes)) {
            $sizes = (array) $sizes;
        }

        $photos = [];
        foreach ($sizes as $size) {
            if ($size <= 0) {
                continue;
            }

            if (empty($uid) || empty($filename)) {
                $photos[$size] = '/assets/img/users/no_photo_' . $size . '.jpg';
                continue;
            }

            $subPath = self::getSubPathByUid($uid);

            $photos[$size] = '/assets/img/users/' . $size . '/' . $subPath . '/' . $uid . '_' . $filename;
        }

        return $photos;
    }

    private static function getRedisKeyByType($type = 'online', $uid)
    {
        switch($type) {
            case 'online': {
                return RedisKeys::userOnline . $uid . '::';
            }
            case 'userinfo': {
                return RedisKeys::userUserInfo . $uid . '::';
            }
        }

    }

    public static function invalidateUserInfoCache($uid)
    {
        if (!is_null(self::$cache_users_by_uid) && isset(self::$cache_users_by_uid[$uid])) {
            unset(self::$cache_users_by_uid[$uid]);
        }

        $redisKey = self::getRedisKeyByType('userinfo', $uid);
        Redis::del($redisKey);
    }

    public static function getEmailsForAdvertising()
    {
        return array_column(Model\User::getEmailsForAdvertising(), 'mail');
    }

    public static function getVerifiedEmails($start = '', $end = '', $gid = 0)
    {
        return array_column(Model\User::getVerifiedEmails($start, $end, $gid), 'mail');
    }

    public static function getCountUsersBySocial()
    {
        $result = Model\User::getCountUsersBySocial();

        return array_column($result, 'count', 'social');
    }

    public static function getCountUsersNotSocial()
    {
        return Model\User::getCountUsersNotSocial();
    }

    public static function getCountUsers()
    {
        return Model\User::getCountUsers();
    }

    protected static function lock($key, $ttl = 0)
    {
        $result = Redis::getSet($key . '::lock', 1) !== '1';

        if ($result && $ttl > 0) {
            Redis::expire($key . '::lock', $ttl);
        }

        return $result;
    }

    protected static function unlock($key)
    {
        return Redis::del($key . '::lock');
    }

    public static function setMarkerByMainMarker($mainMarker, $marker)
    {
        Model\User::setMarkerByMainMarker($mainMarker, $marker);
        return Redis::set($mainMarker, $marker, 3600, Redis::__DB_MARKER_BY_MAIN_MARKER);
    }

    public static function getMarkerByMainMarker($mainMarker)
    {
        if ($mainMarker === false) {
            log_message('error', 'MAIN MARKER IS FALSE');
            log_message('debug', var_export(debug_backtrace(), true));
            return false;
        }

        $marker = Redis::get($mainMarker, Redis::__DB_MARKER_BY_MAIN_MARKER);
        if ($marker === false) {
            $marker = Model\User::getMarkerByMainMarker($mainMarker);
            if ($marker) {
                Redis::set($mainMarker, $marker, 3600, Redis::__DB_MARKER_BY_MAIN_MARKER);
            }
        }

        return $marker;
    }

    public static function getLastSessionNumberByMainMarker($mainMarker)
    {
        $lastSessionNumber = Redis::get($mainMarker, Redis::__DB_SESSION_NUMBER_BY_MAIN_MARKER);

        if ($lastSessionNumber < 1) {
            $lastSessionNumber = Model\User::getLastSessionNumberByMainMarker($mainMarker);
            if ($lastSessionNumber > 0) {
                Redis::set($mainMarker, $lastSessionNumber, 3600, Redis::__DB_SESSION_NUMBER_BY_MAIN_MARKER);
            }
        }

        return $lastSessionNumber;
    }

    public static function setLastSessionNumberByMainMarker($mainMarker, $sessionNumber)
    {
        Model\User::setLastSessionNumberByMainMarker($mainMarker, $sessionNumber);
        return Redis::set($mainMarker, $sessionNumber, 3600, Redis::__DB_SESSION_NUMBER_BY_MAIN_MARKER);
    }
}