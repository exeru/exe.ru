<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.09.17
 * Time: 10:50
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Graph
{
    public static function getRegistrationsAllByDays()
    {
        $minTime = UserLog::getMinRegistrationTime();
        $maxTime = UserLog::getMaxRegistrationTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = strtotime(date('Y-m-d 00:00:00', $minTime));
        $end = strtotime(date('Y-m-d 23:59:59', $maxTime)) + 1;

        $range = [];

        for($i = $start; $i < $end; $i += 86400) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'form' => [
                    'name' => 'Форма',
                    'values' => $range
                ],
                'vk' => [
                    'name' => 'VK',
                    'values' => $range
                ],
                'fb' => [
                    'name' => 'FB',
                    'values' => $range
                ],
                'gp' => [
                    'name' => 'G+',
                    'values' => $range
                ],
                'ok' => [
                    'name' => 'OK',
                    'values' => $range
                ],
                'mm' => [
                    'name' => 'MM',
                    'values' => $range
                ],
                'lf' => [
                    'name' => 'LF',
                    'values' => $range
                ]
            ]
        ];

        $data = UserLog::getRegistationsDataByPeriod($minTime, $maxTime + 1, 'yyyymmdd');

        if (count($data) > 0) {
            foreach($data as $row) {
                if ($row['action'] == 13 || $row['action'] == 17) {
                    $result['series']['form']['values'][$row['yyyymmdd']] += (int) $row['count'];
                } else {
                    $params = unserialize($row['params']);
                    $result['series'][$params[0]]['values'][$row['yyyymmdd']] += (int) $row['count'];
                }
            }
        }

        foreach($result['series'] as $seriaKey => $seria) {
            $result['series'][$seriaKey]['values'] = array_values($result['series'][$seriaKey]['values']);
        }

        return $result;
    }

    public static function getRegistrationsAllByMonths()
    {
        $minTime = UserLog::getMinRegistrationTime();
        $maxTime = UserLog::getMaxRegistrationTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = new \DateTime(date('Y-m-15 00:00:00', $minTime));
        $end = strtotime(date('Y-m-15 00:00:00', $maxTime));

        $range = [];

        do {
            $current = $start->format('Ym15');
            $currentTime = $start->format('U');
            $range[$current] = 0;

            $start->add(new \DateInterval('P1M'));
        } while($currentTime < $end);

        $result = [
            'start' => $minTime,
            'end' => $maxTime,
            'series' => [
                'form' => [
                    'name' => 'Форма',
                    'values' => $range
                ],
                'vk' => [
                    'name' => 'VK',
                    'values' => $range
                ],
                'fb' => [
                    'name' => 'FB',
                    'values' => $range
                ],
                'gp' => [
                    'name' => 'G+',
                    'values' => $range
                ],
                'ok' => [
                    'name' => 'OK',
                    'values' => $range
                ],
                'mm' => [
                    'name' => 'MM',
                    'values' => $range
                ],
                'lf' => [
                    'name' => 'LF',
                    'values' => $range
                ]
            ]
        ];

        $data = UserLog::getRegistationsDataByPeriod($minTime, $maxTime + 1, 'yyyymm');

        if (count($data) > 0) {
            foreach($data as $row) {
                if ($row['action'] == 13 || $row['action'] == 17) {
                    $result['series']['form']['values'][$row['yyyymm'] . '15'] += (int) $row['count'];
                } else {
                    $params = unserialize($row['params']);
                    $result['series'][$params[0]]['values'][$row['yyyymm'] . '15'] += (int) $row['count'];
                }
            }
        }

        return $result;
    }

    public static function getRegistrationsByMonth()
    {
        $end = strtotime(date('Y-m-d 23:59:59')) + 1;
        $start = $end - 30 * 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'form' => [
                    'name' => 'Форма',
                    'values' => $range
                ],
                'vk' => [
                    'name' => 'VK',
                    'values' => $range
                ],
                'fb' => [
                    'name' => 'FB',
                    'values' => $range
                ],
                'gp' => [
                    'name' => 'G+',
                    'values' => $range
                ],
                'ok' => [
                    'name' => 'OK',
                    'values' => $range
                ],
                'mm' => [
                    'name' => 'MM',
                    'values' => $range
                ],
                'lf' => [
                    'name' => 'LF',
                    'values' => $range
                ]
            ]
        ];

        $data = UserLog::getRegistationsDataByPeriod($start, $end, 'yyyymmdd');

        if (count($data) > 0) {
            foreach($data as $row) {
                if ($row['action'] == 13 || $row['action'] == 17) {
                    $result['series']['form']['values'][$row['yyyymmdd']] += (int) $row['count'];
                } else {
                    $params = unserialize($row['params']);
                    $result['series'][$params[0]]['values'][$row['yyyymmdd']] += (int) $row['count'];
                }
            }
        }

        foreach($result['series'] as $seriaKey => $seria) {
            $result['series'][$seriaKey]['values'] = array_values($result['series'][$seriaKey]['values']);
        }

        return $result;
    }

    public static function getRegistrationsByDay()
    {
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = $start + 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('YmdH', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'form' => [
                    'name' => 'Форма',
                    'values' => $range
                ],
                'vk' => [
                    'name' => 'VK',
                    'values' => $range
                ],
                'fb' => [
                    'name' => 'FB',
                    'values' => $range
                ],
                'gp' => [
                    'name' => 'G+',
                    'values' => $range
                ],
                'ok' => [
                    'name' => 'OK',
                    'values' => $range
                ],
                'mm' => [
                    'name' => 'MM',
                    'values' => $range
                ],
                'lf' => [
                    'name' => 'LF',
                    'values' => $range
                ]
            ]
        ];

        $data = UserLog::getRegistationsDataByPeriod($start, $end, 'yyyymmddhh');

        if (count($data) > 0) {
            foreach($data as $row) {
                if ($row['action'] == 13 || $row['action'] == 17) {
                    $result['series']['form']['values'][$row['yyyymmddhh']] += (int) $row['count'];
                } else {
                    $params = unserialize($row['params']);
                    $result['series'][$params[0]]['values'][$row['yyyymmddhh']] += (int) $row['count'];
                }
            }
        }

        foreach($result['series'] as $seriaKey => $seria) {
            $result['series'][$seriaKey]['values'] = implode(', ', $result['series'][$seriaKey]['values']);
        }

        return $result;
    }


    public static function getPaySystemsAllByDays()
    {
        $minTime = Account::getMinPaymentTime();
        $maxTime = Account::getMaxPaymentTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = strtotime(date('Y-m-d 00:00:00', $minTime));
        $end = strtotime(date('Y-m-d 23:59:59', $maxTime)) + 1;

        $range = [];

        for($i = $start; $i < $end; $i += 86400) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => []
        ];

        $data = Account::getPaySystemsByPeriod($start, $end, 'yyyymmdd');
        if (count($data) > 0) {
            foreach($data as $row) {
                if (!isset($result['series'][$row['receiver']])) {
                    $result['series'][$row['receiver']] = [
                        'name' => Payments::$humansText[$row['receiver']],
                        'values' => $range
                    ];
                }
                $result['series'][$row['receiver']]['values'][$row['yyyymmdd']] = (int) $row['sum'];
            }

            foreach ($result['series'] as $receiver => $seria) {
                $result['series'][$receiver]['values'] = array_values($seria['values']);
            }

            $result['series'] = array_values($result['series']);

        }

        return $result;
    }

    public static function getPaySystemsAllByMonths()
    {
        $minTime = Account::getMinPaymentTime();
        $maxTime = Account::getMaxPaymentTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = new \DateTime(date('Y-m-15 00:00:00', $minTime));
        $end = strtotime(date('Y-m-15 00:00:00', $maxTime));

        $range = [];

        do {
            $current = $start->format('Ym15');
            $currentTime = $start->format('U');
            $range[$current] = 0;

            $start->add(new \DateInterval('P1M'));
        } while($currentTime < $end);

        $result = [
            'start' => $minTime,
            'end' => $maxTime,
            'series' => []
        ];

        $data = Account::getPaySystemsByPeriod($minTime, $maxTime, 'yyyymm');
        if (count($data) > 0) {
            foreach($data as $row) {
                if (!isset($result['series'][$row['receiver']])) {
                    $result['series'][$row['receiver']] = [
                        'name' => Payments::$humansText[$row['receiver']],
                        'values' => $range
                    ];
                }
                $result['series'][$row['receiver']]['values'][$row['yyyymm'] . '15'] = (int) $row['sum'];
            }

            $result['series'] = array_values($result['series']);
        }

        return $result;
    }

    public static function getPaySystemsByMonth()
    {
        $end = strtotime(date('Y-m-d 23:59:59')) + 1;
        $start = $end - 30 * 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => []
        ];

        $data = Account::getPaySystemsByPeriod($start, $end, 'yyyymmdd');
        if (count($data) > 0) {
            foreach($data as $row) {
                if (!isset($result['series'][$row['receiver']])) {
                    $result['series'][$row['receiver']] = [
                        'name' => Payments::$humansText[$row['receiver']],
                        'values' => $range
                    ];
                }
                $result['series'][$row['receiver']]['values'][$row['yyyymmdd']] = (int) $row['sum'];
            }

            foreach ($result['series'] as $receiver => $seria) {
                $result['series'][$receiver]['values'] = array_values($seria['values']);
            }

            $result['series'] = array_values($result['series']);

        }

        return $result;
    }

    public static function getPaySystemsByDay()
    {
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = $start + 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('YmdH', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => []
        ];

        $data = Account::getPaySystemsByPeriod($start, $end, 'yyyymmddhh');
        if (count($data) > 0) {
            foreach($data as $row) {
                if (!isset($result['series'][$row['receiver']])) {
                    $result['series'][$row['receiver']] = [
                        'name' => Payments::$humansText[$row['receiver']],
                        'values' => $range
                    ];
                }
                $result['series'][$row['receiver']]['values'][$row['yyyymmddhh']] = (int) $row['sum'];
            }

            foreach ($result['series'] as $receiver => $seria) {
                $result['series'][$receiver]['values'] = implode(', ', $seria['values']);
            }

            $result['series'] = array_values($result['series']);

        }

        return $result;
    }


    public static function getBalanceAllByDays()
    {
        $minTime = User::getFirstPaymentTime();
        $maxTime = User::getLastPaymentTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = strtotime(date('Y-m-d 00:00:00', $minTime));
        $end = strtotime(date('Y-m-d 23:59:59', $maxTime)) + 1;

        $range = [];

        for($i = $start; $i < $end; $i += 86400) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'group_1' => [
                    'name' => 'количество платежей',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'количество плательщиков',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'количество платежей от новых',
                    'values' => $range
                ],
                'group_4' => [
                    'name' => 'количество новых плательщиков',
                    'values' => $range
                ],
                'group_5' => [
                    'name' => 'количество платежей от старых',
                    'values' => $range
                ],
                'group_6' => [
                    'name' => 'количество старых плательщиков',
                    'values' => $range
                ]
            ]
        ];

        $data = User::getPayersDataByPeriod($start, $end, 'yyyymmdd', null);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymmdd']] = (int) $row['count_payments'];
                $result['series']['group_2']['values'][$row['yyyymmdd']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_1']['values'] = array_values($result['series']['group_1']['values']);
        $result['series']['group_2']['values'] = array_values($result['series']['group_2']['values']);

        $data = User::getPayersDataByPeriod($start, $end, 'yyyymmdd', true);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymmdd']] = (int) $row['count_payments'];
                $result['series']['group_4']['values'][$row['yyyymmdd']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_3']['values'] = array_values($result['series']['group_3']['values']);
        $result['series']['group_4']['values'] = array_values($result['series']['group_4']['values']);

        $data = User::getPayersDataByPeriod($start, $end, 'yyyymmdd', false);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_5']['values'][$row['yyyymmdd']] = (int) $row['count_payments'];
                $result['series']['group_6']['values'][$row['yyyymmdd']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_5']['values'] = array_values($result['series']['group_5']['values']);
        $result['series']['group_6']['values'] = array_values($result['series']['group_6']['values']);

        return $result;
    }

    public static function getBalanceAllByMonths()
    {
        $minTime = User::getFirstPaymentTime();
        $maxTime = User::getLastPaymentTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = new \DateTime(date('Y-m-15 00:00:00', $minTime));
        $end = strtotime(date('Y-m-15 00:00:00', $maxTime));

        $range = [];

        do {
            $current = $start->format('Ym15');
            $currentTime = $start->format('U');
            $range[$current] = 0;

            $start->add(new \DateInterval('P1M'));
        } while($currentTime < $end);

        $result = [
            'start' => $minTime,
            'end' => $maxTime,
            'series' => [
                'group_1' => [
                    'name' => 'количество платежей',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'количество плательщиков',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'количество платежей от новых',
                    'values' => $range
                ],
                'group_4' => [
                    'name' => 'количество новых плательщиков',
                    'values' => $range
                ],
                'group_5' => [
                    'name' => 'количество платежей от старых',
                    'values' => $range
                ],
                'group_6' => [
                    'name' => 'количество старых плательщиков',
                    'values' => $range
                ]
            ]
        ];

        $data = User::getPayersDataByPeriod($minTime, $maxTime, 'yyyymm', null);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymm'] . '15'] = (int) $row['count_payments'];
                $result['series']['group_2']['values'][$row['yyyymm'] . '15'] = (int) $row['count_payers'];
            }
        }

        $data = User::getPayersDataByPeriod($minTime, $maxTime, 'yyyymm', true);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymm'] . '15'] = (int) $row['count_payments'];
                $result['series']['group_4']['values'][$row['yyyymm'] . '15'] = (int) $row['count_payers'];
            }
        }

        $data = User::getPayersDataByPeriod($minTime, $maxTime, 'yyyymm', false);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_5']['values'][$row['yyyymm'] . '15'] = (int) $row['count_payments'];
                $result['series']['group_6']['values'][$row['yyyymm'] . '15'] = (int) $row['count_payers'];
            }
        }

        return $result;
    }

    public static function getBalanceByMonth()
    {
        $end = strtotime(date('Y-m-d 23:59:59')) + 1;
        $start = $end - 30 * 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'group_1' => [
                    'name' => 'количество платежей',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'количество плательщиков',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'количество платежей от новых',
                    'values' => $range
                ],
                'group_4' => [
                    'name' => 'количество новых плательщиков',
                    'values' => $range
                ],
                'group_5' => [
                    'name' => 'количество платежей от старых',
                    'values' => $range
                ],
                'group_6' => [
                    'name' => 'количество старых плательщиков',
                    'values' => $range
                ]
            ]
        ];

        $data = User::getPayersDataByPeriod($start, $end, 'yyyymmdd', null);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymmdd']] = (int) $row['count_payments'];
                $result['series']['group_2']['values'][$row['yyyymmdd']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_1']['values'] = array_values($result['series']['group_1']['values']);
        $result['series']['group_2']['values'] = array_values($result['series']['group_2']['values']);

        $data = User::getPayersDataByPeriod($start, $end, 'yyyymmdd', true);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymmdd']] = (int) $row['count_payments'];
                $result['series']['group_4']['values'][$row['yyyymmdd']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_3']['values'] = array_values($result['series']['group_3']['values']);
        $result['series']['group_4']['values'] = array_values($result['series']['group_4']['values']);

        $data = User::getPayersDataByPeriod($start, $end, 'yyyymmdd', false);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_5']['values'][$row['yyyymmdd']] = (int) $row['count_payments'];
                $result['series']['group_6']['values'][$row['yyyymmdd']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_5']['values'] = array_values($result['series']['group_5']['values']);
        $result['series']['group_6']['values'] = array_values($result['series']['group_6']['values']);

        return $result;
    }

    public static function getBalanceByDay()
    {
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = $start + 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('YmdH', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'group_1' => [
                    'name' => 'количество платежей',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'количество плательщиков',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'количество платежей от новых',
                    'values' => $range
                ],
                'group_4' => [
                    'name' => 'количество новых плательщиков',
                    'values' => $range
                ],
                'group_5' => [
                    'name' => 'количество платежей от старых',
                    'values' => $range
                ],
                'group_6' => [
                    'name' => 'количество старых плательщиков',
                    'values' => $range
                ]
            ]
        ];

        $data = UserLog::getPayersDataByDayByPeriod($start, $end, null);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymmddhh']] = (int) $row['count_payments'];
                $result['series']['group_2']['values'][$row['yyyymmddhh']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_1']['values'] = implode(', ', $result['series']['group_1']['values']);
        $result['series']['group_2']['values'] = implode(', ', $result['series']['group_2']['values']);

        $data = UserLog::getPayersDataByDayByPeriod($start, $end, true);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymmddhh']] = (int) $row['count_payments'];
                $result['series']['group_4']['values'][$row['yyyymmddhh']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_3']['values'] = implode(', ', $result['series']['group_3']['values']);
        $result['series']['group_4']['values'] = implode(', ', $result['series']['group_4']['values']);

        $data = UserLog::getPayersDataByDayByPeriod($start, $end, false);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_5']['values'][$row['yyyymmddhh']] = (int) $row['count_payments'];
                $result['series']['group_6']['values'][$row['yyyymmddhh']] = (int) $row['count_payers'];
            }
        }
        $result['series']['group_5']['values'] = implode(', ', $result['series']['group_5']['values']);
        $result['series']['group_6']['values'] = implode(', ', $result['series']['group_6']['values']);

        return $result;
    }


    public static function getPurchasesAllByDays()
    {
        $minTime = Account::getPurchasesMinTime();
        $maxTime = Account::getPurchasesMaxTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = strtotime(date('Y-m-d 00:00:00', $minTime));
        $end = strtotime(date('Y-m-d 23:59:59', $maxTime)) + 1;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'values' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 86400) {
            $range[date('Ymd', $i)] = 0;
        }

        $installedGids = Game::getActiveGamesId();

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = Account::getSumByGidsByPeriod($installedGids, $start, $end, 'yyyymmdd');

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymmdd']] = (int) $row['sum'];
                }
            }

            foreach ($series as $gid => $seria) {
                $series[$gid]['values'] = array_values($seria['values']);
            }

            $result['series'] = array_values($series);
        }

        return $result;
    }

    public static function getPurchasesAllByMonths()
    {
        $minTime = Account::getPurchasesMinTime();
        $maxTime = Account::getPurchasesMaxTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $result = [
            'start' => strtotime(date('Y-m-15 00:00:00', $minTime)),
            'end' => $end = strtotime(date('Y-m-15 00:00:00', $maxTime)),
            'values' => []
        ];

        $start = new \DateTime(date('Y-m-15 00:00:00', $minTime));
        $end = strtotime(date('Y-m-15 00:00:00', $maxTime));

        $range = [];

        do {
            $current = $start->format('Ym15');
            $currentTime = $start->format('U');
            $range[$current] = 0;

            $start->add(new \DateInterval('P1M'));
        } while($currentTime < $end);

        $installedGids = Game::getActiveGamesId();

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = Account::getSumByGidsByPeriod($installedGids, $minTime, $maxTime, 'yyyymm');

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymm'] . '15'] = (int) $row['sum'];
                }
            }

            $result['values'] = array_values($series);
        }

        return $result;
    }

    public static function getPurchasesByMonth()
    {
        $end = strtotime(date('Y-m-d 23:59:59')) + 1;
        $start = $end - 30 * 86400;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'values' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('Ymd', $i)] = 0;
        }

        $installedGids = Game::getActiveGamesId();

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = Account::getSumByGidsByPeriod($installedGids, $start, $end, 'yyyymmdd');

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymmdd']] = (int) $row['sum'];
                }
            }

            foreach ($series as $gid => $seria) {
                $series[$gid]['values'] = array_values($seria['values']);
            }

            $result['series'] = array_values($series);
        }

        return $result;
    }

    public static function getPurchasesByDay()
    {
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = $start + 86400;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('YmdH', $i)] = 0;
        }

        $installedGids = Game::getActiveGamesId();

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = Account::getSumByGidsByPeriod($installedGids, $start, $end, 'yyyymmddhh');

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymmddhh']] = (int) $row['sum'];
                }
            }

            foreach ($series as $gid => $seria) {
                $series[$gid]['values'] = implode(', ', $seria['values']);
            }

            $result['series'] = array_values($series);
        }

        return $result;
    }


    public static function getUsersDurationAllByDays($init)
    {
        $minTime = User::getUsersMinTime($init);
        $maxTime = User::getUsersMaxTime($init);

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = strtotime(date('Y-m-d 00:00:00', $minTime));
        $end = strtotime(date('Y-m-d 23:59:59', $maxTime)) + 1;

        $range = [];

        for($i = $start; $i < $end; $i += 86400) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $minTime,
            'end' => $maxTime,
            'series' => [
                'group_1' => [
                    'name' => 'меньше минуты',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'меньше 10 минут',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'больше 10 минут',
                    'values' => $range
                ],
            ]
        ];

        $data = User::getUsersDurationByParams($start, $end, 60, 0, 'yyyymmdd', $init);

        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymmdd']] = (int) $row['count'];
            }
        }

        $result['series']['group_1']['values'] = array_values($result['series']['group_1']['values']);

        $data = User::getUsersDurationByParams($start, $end, 600, 60, 'yyyymmdd', $init);

        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_2']['values'][$row['yyyymmdd']] = (int) $row['count'];
            }
        }
        $result['series']['group_2']['values'] = array_values($result['series']['group_2']['values']);

        $data = User::getUsersDurationByParams($start, $end, 86400, 600, 'yyyymmdd', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymmdd']] = (int) $row['count'];
            }
        }
        $result['series']['group_3']['values'] = array_values($result['series']['group_3']['values']);

        return $result;
    }

    public static function getUsersDurationAllByMonth($init)
    {
        $minTime = User::getUsersMinTime($init);
        $maxTime = User::getUsersMaxTime($init);

        $start = new \DateTime(date('Y-m-15 00:00:00', $minTime));
        $end = strtotime(date('Y-m-15 00:00:00', $maxTime));

        $range = [];

        do {
            $current = $start->format('Ym15');
            $currentTime = $start->format('U');
            $range[$current] = 0;

            $start->add(new \DateInterval('P1M'));
        } while($currentTime < $end);

        $result = [
            'start' => $minTime,
            'end' => $maxTime,
            'series' => [
                'group_1' => [
                    'name' => 'меньше минуты',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'меньше 10 минут',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'больше 10 минут',
                    'values' => $range
                ],
            ]
        ];

        $data = User::getUsersDurationByParams($minTime, $maxTime, 60, 0, 'yyyymm', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymm'] . '15'] = (int) $row['count'];
            }
        }

        $data = User::getUsersDurationByParams($minTime, $maxTime, 600, 60, 'yyyymm', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_2']['values'][$row['yyyymm'] . '15'] = (int) $row['count'];
            }
        }

        $data = User::getUsersDurationByParams($minTime, $maxTime, 86400, 600, 'yyyymm', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymm'] . '15'] = (int) $row['count'];
            }
        }

        return $result;
    }

    public static function getUsersDurationByMonth($init)
    {
        $end = strtotime(date('Y-m-d 23:59:59')) + 1;
        $start = $end - 30 * 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('Ymd', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'group_1' => [
                    'name' => 'меньше минуты',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'меньше 10 минут',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'больше 10 минут',
                    'values' => $range
                ],
            ]
        ];

        $data = User::getUsersDurationByParams($start, $end, 60, 0, 'yyyymmdd', $init);

        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymmdd']] = (int) $row['count'];
            }
        }

        $result['series']['group_1']['values'] = array_values($result['series']['group_1']['values']);

        $data = User::getUsersDurationByParams($start, $end, 600, 60, 'yyyymmdd', $init);

        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_2']['values'][$row['yyyymmdd']] = (int) $row['count'];
            }
        }
        $result['series']['group_2']['values'] = array_values($result['series']['group_2']['values']);

        $data = User::getUsersDurationByParams($start, $end, 86400, 600, 'yyyymmdd', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymmdd']] = (int) $row['count'];
            }
        }
        $result['series']['group_3']['values'] = array_values($result['series']['group_3']['values']);

        return $result;
    }

    public static function getUsersDurationByDay($init)
    {
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = $start + 86400;

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('YmdH', $i)] = 0;
        }

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => [
                'group_1' => [
                    'name' => 'меньше минуты',
                    'values' => $range
                ],
                'group_2' => [
                    'name' => 'меньше 10 минут',
                    'values' => $range
                ],
                'group_3' => [
                    'name' => 'больше 10 минут',
                    'values' => $range
                ],
            ]
        ];

        $data = User::getUsersDurationByParams($start, $end, 60, 0, 'yyyymmddhh', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_1']['values'][$row['yyyymmddhh']] = (int) $row['count'];
            }
        }
        $result['series']['group_1']['values'] = implode(', ', $result['series']['group_1']['values']);

        $data = User::getUsersDurationByParams($start, $end, 600, 60, 'yyyymmddhh', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_2']['values'][$row['yyyymmddhh']] = (int) $row['count'];
            }
        }
        $result['series']['group_2']['values'] = implode(', ', $result['series']['group_2']['values']);

        $data = User::getUsersDurationByParams($start, $end, 86400, 600, 'yyyymmddhh', $init);
        if (count($data) > 0) {
            foreach($data as $row) {
                $result['series']['group_3']['values'][$row['yyyymmddhh']] = (int) $row['count'];
            }
        }
        $result['series']['group_3']['values'] = implode(', ', $result['series']['group_3']['values']);

        return $result;
    }


    public static function getNewGamersAllByDays()
    {
        $minTime = UserLog::getInstalledNewMinTime();
        $maxTime = UserLog::getInstalledNewMaxTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $start = strtotime(date('Y-m-d 00:00:00', $minTime));
        $end = strtotime(date('Y-m-d 23:59:59', $maxTime)) + 1;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'values' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 86400) {
            $range[date('Ymd', $i)] = 0;
        }

        $installedGids = array_intersect(Game::getActiveGamesId(), array_column(UserLog::getInstalledGidsByPeriod($start, $end), 'gid'));

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = UserLog::getInstalledByGroupsByPeriod($start, $end, 'yyyymmdd', $installedGids);

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymmdd']] = (int) $row['count'];
                }

                foreach ($series as $gid => $seria) {
                    $series[$gid]['values'] = array_values($seria['values']);
                }
            }

            $result['series'] = array_values($series);
        }

        return $result;
    }

    public static function getNewGamersAllByMonths()
    {
        $minTime = Game::getInstalledNewMinTime();
        $maxTime = Game::getInstalledNewMaxTime();

        if (empty($minTime)) {
            $minTime = time();
        }

        if (empty($maxTime)) {
            $maxTime = time();
        }

        $result = [
            'start' => strtotime(date('Y-m-15 00:00:00', $minTime)),
            'end' => $end = strtotime(date('Y-m-15 00:00:00', $maxTime)),
            'values' => []
        ];

        $start = new \DateTime(date('Y-m-15 00:00:00', $minTime));
        $end = strtotime(date('Y-m-15 00:00:00', $maxTime));

        $range = [];

        do {
            $current = $start->format('Ym15');
            $currentTime = $start->format('U');
            $range[$current] = 0;

            $start->add(new \DateInterval('P1M'));
        } while($currentTime < $end);

        $installedGids = array_intersect(Game::getActiveGamesId(), array_column(UserLog::getInstalledGidsByPeriod($minTime, $maxTime + 1), 'gid'));

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid, true);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = UserLog::getInstalledByGroupsByPeriod($minTime, $maxTime, 'yyyymm', $installedGids);

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymm'] . '15'] = (int) $row['count'];
                }
            }

            $result['values'] = array_values($series);
        }

        return $result;
    }

    public static function getNewGamersByMonth()
    {
        $end = strtotime(date('Y-m-d 23:59:59')) + 1;
        $start = $end - 30 * 86400;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'values' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('Ymd', $i)] = 0;
        }

        $installedGids = array_intersect(Game::getActiveGamesId(), array_column(UserLog::getInstalledGidsByPeriod($start, $end), 'gid'));

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid, true);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = UserLog::getInstalledByGroupsByPeriod($start, $end, 'yyyymmdd', $installedGids);

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymmdd']] = (int) $row['count'];
                }
            }

            foreach($series as $gid => $seria) {
                $series[$gid]['values'] = array_values($seria['values']);
            }

            $result['series'] = array_values($series);
        }

        return $result;
    }

    public static function getNewGamersByDay()
    {
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = $start + 86400;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'series' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('YmdH', $i)] = 0;
        }

        $installedGids = array_intersect(Game::getActiveGamesId(), array_column(UserLog::getInstalledGidsByPeriod($start, $end), 'gid'));

        if (count($installedGids) > 0) {
            $series = [];
            foreach($installedGids as $gid) {
                $game = Game::getGameById($gid);
                $series[$gid] = [
                    'name' => $game['title'],
                    'values' => $range
                ];
            }

            $dataByPeriod = UserLog::getInstalledByGroupsByPeriod($start, $end, 'yyyymmddhh', $installedGids);

            if (count($dataByPeriod) > 0) {
                foreach($dataByPeriod as $row) {
                    $series[$row['gid']]['values'][$row['yyyymmddhh']] = (int) $row['count'];
                }

                foreach ($series as $gid => $seria) {
                    $series[$gid]['values'] = implode(', ', $seria['values']);
                }
            }

            $result['series'] = array_values($series);
        }

        return $result;
    }


    public static function getAdvertAllByMonths($nullGroupName)
    {
        $minTime = User::getAdvertMinTime();
        $maxTime = User::getAdvertMaxTime();

        $result = [
            'start' => strtotime(date('Y-m-15 00:00:00', $minTime)),
            'end' => $end = strtotime(date('Y-m-15 00:00:00', $maxTime)),
            'values' => []
        ];

        $start = new \DateTime(date('Y-m-15 00:00:00', $minTime));
        $end = strtotime(date('Y-m-15 00:00:00', $maxTime));

        $range = [];

        do {
            $current = $start->format('Ym15');
            $currentTime = $start->format('U');
            $range[$current] = 0;

            $start->add(new \DateInterval('P1M'));
        } while($currentTime < $end);

        $groups = User::getAdvertGroupsByPeriod($minTime, $maxTime);

        if (count($groups) > 0) {
            foreach($groups as $group) {
                if (empty($group['adv_service'])) {
                    $group['adv_service'] = $nullGroupName;
                }
                $result['values'][$group['adv_service']] = $range;
            }
        }

        $data = User::getAdvertByGroupsByPeriod($minTime, $maxTime, 'yyyymm');

        if (count($data) > 0) {
            foreach($data as $values) {
                if (empty($values['adv_service'])) {
                    $values['adv_service'] = $nullGroupName;
                }

                $result['values'][$values['adv_service']][$values['yyyymm'] . '15'] = (int) $values['count'];
            }
        }

        return $result;
    }

    public static function getAdvertAllByDays($nullGroupName)
    {
        $start = strtotime(date('Y-m-d 00:00:00', User::getAdvertMinTime()));
        $end = strtotime(date('Y-m-d 23:59:59', User::getAdvertMaxTime())) + 1;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'values' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 86400) {
            $range[date('Ymd', $i)] = 0;
        }

        $groups = User::getAdvertGroupsByPeriod($start, $end);

        if (count($groups) > 0) {
            foreach($groups as $group) {
                if (empty($group['adv_service'])) {
                    $group['adv_service'] = $nullGroupName;
                }
                $result['values'][$group['adv_service']] = $range;
            }
        }


        $data = User::getAdvertByGroupsByPeriod($start, $end, 'yyyymmdd');

        if (count($data) > 0) {
            foreach($data as $values) {
                if (empty($values['adv_service'])) {
                    $values['adv_service'] = $nullGroupName;
                }

                $result['values'][$values['adv_service']][$values['yyyymmdd']] = (int) $values['count'];
            }
        }

        if (count($result['values']) > 0) {
            foreach($result['values'] as $groupName => $valuesArray) {
                $result['values'][$groupName] = array_values($valuesArray);
            }
        }

        return $result;
    }

    public static function getAdvertByMonth($nullGroupName)
    {
        $end = strtotime(date('Y-m-d 23:59:59')) + 1;
        $start = $end - 30 * 86400;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'values' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('Ymd', $i)] = 0;
        }

        $groups = User::getAdvertGroupsByPeriod($start, $end);

        if (count($groups) > 0) {
            foreach($groups as $group) {
                if (empty($group['adv_service'])) {
                    $group['adv_service'] = $nullGroupName;
                }
                $result['values'][$group['adv_service']] = $range;
            }
        }


        $data = User::getAdvertByGroupsByPeriod($start, $end, 'yyyymmdd');

        if (count($data) > 0) {
            foreach($data as $values) {
                if (empty($values['adv_service'])) {
                    $values['adv_service'] = $nullGroupName;
                }

                $result['values'][$values['adv_service']][$values['yyyymmdd']] = (int) $values['count'];
            }
        }

        if (count($result['values']) > 0) {
            foreach($result['values'] as $groupName => $valuesArray) {
                $result['values'][$groupName] = array_values($valuesArray);
            }
        }

        return $result;
    }

    public static function getAdvertByDay($nullGroupName)
    {
        $start = strtotime(date('Y-m-d 00:00:00'));
        $end = $start + 86400;

        $result = [
            'start' => $start,
            'end' => $end - 1,
            'values' => []
        ];

        $range = [];

        for($i = $start; $i < $end; $i += 3600) {
            $range[date('YmdH', $i)] = 0;
        }

        $groups = User::getAdvertGroupsByPeriod($start, $end);

        if (count($groups) > 0) {
            foreach($groups as $group) {
                if (empty($group['adv_service'])) {
                    $group['adv_service'] = $nullGroupName;
                }
                $result['values'][$group['adv_service']] = $range;
            }
        }


        $data = User::getAdvertByGroupsByPeriod($start, $end, 'yyyymmddhh');

        if (count($data) > 0) {
            foreach($data as $values) {
                if (empty($values['adv_service'])) {
                    $values['adv_service'] = $nullGroupName;
                }

                $result['values'][$values['adv_service']][$values['yyyymmddhh']] = $values['count'];
            }
        }

        if (count($result['values']) > 0) {
            foreach($result['values'] as $groupName => $valuesArray) {
                $result['values'][$groupName] = implode(', ', $valuesArray);
            }
        }

        return $result;
    }
}