<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27.11.15
 * Time: 15:32
 */

namespace Base\Service;


class RedisKeys
{
    // Данные пользователей
    const userUserInfo = '__user::userInfo::'; // Информация об одном пользователе
    const userOnline = '__user::online::'; // Статус пользователя
    const userLastActive = '__user::lastActive::'; // Время последней активности пользователя
    const userTempSessionNumber = '__user::temporary::sessionId'; // Хеш для хранения номеров сессий временных пользователей

    // Более не используется.
    // const userMainMarker = '__user::mainMarker::'; // Хеш для хранения номеров сессий пользователей

    const userMainMarkerByUid = '__user::mainMarkerByUid::'; // Хеш для хранения главных маркеров для авторизованных пользователей
    const userMarkerByMainMarker = '__user::markerByMainMarker::'; // Хеш для хранения текущего маркера по главному маркеру
    const userMarkerTime = '__user::markerTime::'; // Хеш для хранения времени последней активности в данной сессии пользователя
    const userMarkerUid = '__user::markerUid::'; // Хеш для хранения идентификатора авторизованного пользователя для этой сессии
    const userMarkerSessionNumber = '__user::markerSessionNumber::'; // Хеш для хранения номера сессие для данного маркера

    /* Хеш для хранения времени последнего вызова acceptUser. Структура хранения:
     * Ключ - __user::userMarkerUserSession::__маркер__текущей__пользовательской__сессии(маркер)__::
     * Хеш - Идентификатор сессии пользователя (sid), таблица user_sessions
     * Значение - time()
     */
    const userMarkerUserSession = '__user::userMarkerUserSession::';
    /*
     * Аналогичный хеш для игр
     */
    const userMarkerGameSession = '__user::userMarkerGameSession::';


    // Данные карусели
    const gameCarusel = '__game::carusel::';
    // Данные каталога игр
    const gameCatalogGuest = '__game::catalog::guest::'; // Каталог игр для не-авторизованного пользователя
    // Игры
    const gameActiveGameIds = '__game::activeIds::'; // Идентификаторы активных и включенных игр
    const gameUserGames = '__game::user::games::'; // Установленные и не-установленные игры пользователя
    const gameGameInfo = '__game::gameInfo::'; // Информация об одной игре
    // Словари
    const dictionary = '__dictionary::'; // Префикс для всех словарей
    // Черный список
    const blacklist = '__blacklist::';
    // Друзья
    const friendsByTypeArray = '__friends::friendsByTypeArray::'; // Ассоциативный массив друзей по типам дружбы
    // Префикс ключа для приглашения друзей в игру
    // Полный ключ должен иметь вид friendsUserInviteToGame . userId . '::' . gameId . '::' . friendId . '::'
    const friendsUserInviteToGame = '__friends::userInviteToGame::';
    // Префикс ключа для информации о покупке, на которую не хватило средств.
    const purchasesInfo = '__purchases::info::';
    // Префикс для связи идентификатора пополнения баланса и идентификатора покупки
    const orderToPurchase = '__payments::orderIdToPurchaseIdLink::';
    // Обратная связь для orderToPurchase
    const purchaseToOrder = '__payments::purchaseIdToOrderIdLink::';
    // Префикс для записи последних 100 регистраций за 10 минут
    const max100per10min = '__max100per10min::';
    // Префикс для записи последних 5 регистраций за 10 минут с одного IP
    const max5per10minByIp = '__max5per10minByIp::';
    // Префикс для кеша страниц каптчи при превышении максимального количества регистраций
    const maxTemporaryPage = '__maxTemporaryHTML::';
    // Префикс для кеша страниц каптчи при превышении максимального количества регистраций
    const gameRetentionPage = '__gameRetentionHTML::';

    // Префик для времени последнего отправленного сообщения
    const lastMessageTime = '__user::lastMessageTime::';

    const mailingQueue = '_mailingQueue::';
}