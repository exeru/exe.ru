<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 24.10.16
 * Time: 16:12
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class File
{
    private static
        $_config = [
            'development' => [
                'host' => 'http://test.exe.ru:81',
                'image_host' => 'http://test.exe.ru',
                'secret' => '123'
            ],
            'production' => [
                'host' => 'http://172.16.101.56:81',
                'image_host' => 'http://exe.ru',
                'secret' => '123'
            ]
        ];

    public static
        $result = null,
        $info = null;

    public static function putFileLocally($fullFilePath)
    {
        if (ENVIRONMENT == 'testing') {
            return true;
        }

        $config = self::$_config[ENVIRONMENT];
        $shortFilePath = substr($fullFilePath, strlen(FCPATH) - 1);

        file_put_contents($fullFilePath, file_get_contents($config['image_host'] . $shortFilePath));

        return true;
    }

    public static function upload($fullFilePath)
    {
        if (ENVIRONMENT == 'testing') {
            return true;
        }

        clearstatcache();

        $config = self::$_config[ENVIRONMENT];
        $shortFilePath = substr($fullFilePath, strlen(FCPATH) - 1);

        $fp = fopen($fullFilePath, 'rb');

        $ch = curl_init();

        $options = [
            CURLOPT_URL => $config['host'] . $shortFilePath,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_UPLOAD => TRUE,
            CURLOPT_INFILE => $fp,
            CURLOPT_INFILESIZE => filesize($fullFilePath),
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_HEADER => FALSE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => ['Auth: ' . md5($shortFilePath . $config['secret'])]
        ];

        curl_setopt_array($ch, $options);

        self::$result = curl_exec($ch);
        self::$info = curl_getinfo($ch);

        fclose($fp);
        curl_close($ch);

        unlink($fullFilePath);
    }

    public static function symlink($fullFilePath, $name)
    {
        if (ENVIRONMENT == 'testing') {
            symlink($fullFilePath, $name);
            return true;
        }

        $config = self::$_config[ENVIRONMENT];
        $shortFilePath = substr($fullFilePath, strlen(FCPATH) - 1);

        $ch = curl_init();

        $options = [
            CURLOPT_URL => $config['host'] . $shortFilePath,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => ['Auth: ' . md5($shortFilePath . $config['secret'])],
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'name' => substr($name, strlen(FCPATH) - 1)
            ]
        ];

        curl_setopt_array($ch, $options);

        self::$result = curl_exec($ch);
        self::$info = curl_getinfo($ch);

        curl_close($ch);
    }

    public static function delete($fullFilePath)
    {
        if (ENVIRONMENT == 'testing') {
            return true;
        }

        $config = self::$_config[ENVIRONMENT];
        $shortFilePath = substr($fullFilePath, strlen(FCPATH) - 1);

        $ch = curl_init();

        $options = [
            CURLOPT_URL => $config['host'] . $shortFilePath,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => ['Auth: ' . md5($shortFilePath . $config['secret'])],
            CURLOPT_CUSTOMREQUEST => 'DELETE'
        ];

        curl_setopt_array($ch, $options);

        self::$result = curl_exec($ch);
        self::$info = curl_getinfo($ch);

        curl_close($ch);
    }
}