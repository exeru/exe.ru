<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.08.15
 * Time: 18:33
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Hash
{
    private static $_salt = 'buiohdf98hfcuibnweiovh2389hfnhoinhv9ewh894gfh34iognvnj09jwer4gm';

    public static function getHash($string, $notRecoverable = false)
    {
        return md5(md5($string) . self::$_salt . ($notRecoverable ? microtime(true) : ''));
    }

    public static function getRandomChars($length = 8)
    {
        $chars = '';
        $randomHash = md5(rand(1, 1000000000));

        for($i=$length; $i>0; $i--) {
            $chars .= $randomHash[rand(0, 31)];
        }

        return $chars;
    }
}