<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 29.09.15
 * Time: 15:43
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Feeds
{
    const _REDIS_KEY_BLACKLIST_PREFIX = 'feedsBlackList::';

    const FEEDS_COUNT_FOR_DEV = 5;

    public static function eventManager($eventType, $params)
    {
        switch($eventType) {
            case 'EVENT_USER_INSTALL_GAME': {
                self::addFeed($params['uid'], $params['gid'], 1, 'Установил игру');
                break;
            }
            case 'EVENT_USER_ACCEPT_FRIEND': {
                self::addFeed($params['uid'], NULL, 2, 'Новый друг', $params['fid']);
                self::addFeed($params['fid'], NULL, 2, 'Новый друг', $params['uid']);
                break;
            }
        }
    }

    public static function getMaxFeedId()
    {
        return Model\Feeds::getMaxFeedId();
    }

    public static function deleteAppointedFeed($fid, $gid)
    {
        return Model\Feeds::deleteAppointedFeed($fid, $gid);
    }

    public static function getAppointedFeeds($time)
    {
        return Model\Feeds::getAppointedFeeds($time);
    }

    public static function updateAppointedFeed($fid, $gid, $updated)
    {
        if (count($updated) < 1) {
            return false;
        }
        return Model\Feeds::updateAppointedFeed($fid, $gid, $updated) == 1;
    }

    public static function updateGameFeed($fid, $gid, $updated)
    {
        if (count($updated) < 1) {
            return false;
        }
        return Model\Feeds::updateGameFeed($fid, $gid, $updated) == 1;
    }

    public static function deleteFeed($fid, $gid)
    {
        if ($gid > 0 && Game::getGameById($gid) !== false) {
            $users = array_column(Game::getGamersId($gid), 'uid');
        }

        if (count($users) > 0) {
            Events::rise('EVENT_NEWS_DELETE', [
                'fids' => $users,
                'message' => json_encode([
                    'type' => 'panel',
                    'subtype' => 'news',
                    'action' => 'sub'
                ])
            ]);
        }

        return Model\Feeds::deleteFeed($fid, $gid) == 1;
    }

    public static function removeFromBlackList($uid, $fid)
    {
        return;

        $key = $redisPrefix = self::_REDIS_KEY_BLACKLIST_PREFIX . $uid . '::' . $fid;

        return \Base\Service\Redis::del($key);
    }

    public static function appendToBlackList($uid, $fid)
    {
        return;

        $key = $redisPrefix = self::_REDIS_KEY_BLACKLIST_PREFIX . $uid . '::' . $fid;

        return \Base\Service\Redis::set($key, true, Model\Feeds::_MAX_COUNT_SECONDS);
    }

    public static function addPendingFeed($appointedTime, $userId, $gid, $type, $content, $fid = NULL, $image = NULL, $params = NULL, $title = NULL)
    {
        return Model\Feeds::addPendingFeed($appointedTime, $userId, $gid, $type, $content, $fid, $image, $params, $title);
    }

    public static function addFeed($userId, $gid, $type, $content, $fid = NULL, $image = NULL, $params = NULL, $title = NULL)
    {
        $users = [];

        if (!is_null($userId) && $userId > 0) {
            $friendsByType = Friends::getFriendsByTypeArray($userId);
            $users = array_merge(array_keys($friendsByType['both']), array_keys($friendsByType['me']));
        } else if ($gid > 0 && Game::getGameById($gid) !== false) {
            $users = array_column(Game::getGamersId($gid), 'uid');
        }

        if (count($users) > 0) {
            Events::rise('EVENT_NEWS_ADD', [
                'fids' => $users,
                'message' => json_encode([
                    'type' => 'panel',
                    'subtype' => 'news',
                    'action' => 'add'
                ])
            ]);
        }

        $shortFilePath = null;
        if (!empty($image)) {
            log_message('debug', 'ADD FEED, IMAGE URL ' . $image);
            list($remoteFileName,) = explode('?', $image);
            $remoteFileExt = substr($remoteFileName, strrpos($remoteFileName, '.') + 1);

            $hash = md5($image);
            $hashFilePath = rtrim(FCPATH, '/') . '/assets/img/cache/' . $hash[0] . '/' . $hash[1] . '/' . $hash[2] . '/' . $hash . '.' . $remoteFileExt;

            if (!file_exists($hashFilePath)) {
                $imageContent = @file_get_contents($image);

                if ($imageContent === false) {
                    $image = null;
                } else {
                    $hashDirName = dirname($hashFilePath);
                    if (!is_dir($hashDirName)) {
                        mkdir($hashDirName, 0755, true);
                    }

                    file_put_contents($hashFilePath, $imageContent);
                    $imageSize = @getimagesize($hashFilePath);
                    if ($imageSize) {
                        if ($imageSize[0] > 610) {
                            \Base\Service\Image::resize($hashFilePath, 610, 0);
                        }

                        \Base\Service\File::upload($hashFilePath);

                        touch($hashFilePath);

                        $shortFilePath = substr($hashFilePath, strlen(FCPATH) - 1);
                    } else {
                        $image = null;
                    }
                }
            } else {
                $shortFilePath = substr($hashFilePath, strlen(FCPATH) - 1);
            }
        }

        return Model\Feeds::addFeed($userId, $gid, $type, $content, $fid, $image, $params, $title, $shortFilePath);
    }

    public static function searchFeeds($uid, $gid, $search)
    {
        $feedsBlackList = self::getBlackList($uid);

        $feeds = Model\Feeds::searchFeeds($gid, $feedsBlackList, $search);

        return $feeds;
    }

    public static function getCountFeeds($uid, $gameIds, $userIds, $activeGamesId)
    {
        $feedsBlackList = self::getBlackList($uid);

        // Получаем последнюю просмотренную новость
        $user = User::getUserInfo($uid);
        $lastFId = isset($user['last_fid']) ? $user['last_fid'] : 0;

        return Model\Feeds::getCountFeeds($gameIds, $userIds, $feedsBlackList, $lastFId, $activeGamesId);
    }

    public static function getPendingFeeds($gid)
    {
        return Model\Feeds::getPendingFeeds($gid);
    }

    public static function getPendingFeed($fid, $gid)
    {
        return Model\Feeds::getPendingFeed($fid, $gid);
    }

    public static function getFeeds($uid, $gameIds, $userIds, $limit = NULL, $offset = NULL, $where = NULL, $activeGamesId = NULL)
    {
        \Base\Service\Log::profile(__METHOD__);

        $feedsBlackList = is_null($uid) ? [] : self::getBlackList($uid);

        $feeds = Model\Feeds::getFeeds($gameIds, $userIds, $feedsBlackList, $limit, $offset, $where, $activeGamesId);

        \Base\Service\Log::profile(__METHOD__);
        return $feeds;
    }

    public static function getFeed($fid, $gid)
    {
        return Model\Feeds::getFeed($fid, $gid);
    }

    private static function getBlackList($uid)
    {
        $feedsBlackList = [];

        return $feedsBlackList;

        $redisPrefix = self::_REDIS_KEY_BLACKLIST_PREFIX . $uid . '::';
        $keys = \Base\Service\Redis::getKeys($redisPrefix . '*');

        if (count($keys) > 0) {
            $prefixLength = strlen($redisPrefix);
            foreach($keys as $key) {
                $feedsBlackList[] = substr($key, $prefixLength);
            }
        }

        return $feedsBlackList;
    }
}