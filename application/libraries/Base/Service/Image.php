<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.05.16
 * Time: 16:31
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Image
{
    public static function resize($path, $width, $height)
    {
        $image = new \Imagick($path);
        $image->stripImage();
        $image->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1);
        $image->setImageCompression(\Imagick::COMPRESSION_JPEG);
        $image->setImageCompressionQuality(90);
        $image->writeImage($path);
        $image->destroy();

        return true;
    }

    public static function crop($path, $x, $y, $width, $height)
    {
        $image = new \Imagick($path);
        $image->cropImage($width, $height, $x, $y);
        $image->writeImage($path);
        $image->destroy();

        return true;
    }
}