<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 02.08.17
 * Time: 13:43
 */
namespace Base\Service;

use \Base\Service\Redis;

defined('BASEPATH') OR exit('No direct script access allowed');

class TwigCacheRedis implements \Twig_CacheInterface
{
    private $environment;

    public function __construct($environment)
    {
        $this->environment = $environment;
    }

    /**
     * Generates a cache key for the given template class name.
     *
     * @param string $name      The template name
     * @param string $className The template class name
     *
     * @return string
     */
    public function generateKey($name, $className)
    {
        $hash = hash('sha256', $className);

        return $hash;
    }

    /**
     * Writes the compiled template to cache.
     *
     * @param string $key     The cache key
     * @param string $content The template representation as a PHP class
     */
    public function write($key, $content)
    {
        Redis::hSet('__twig::cache::' . $this->environment . '::', $key, $content, Redis::__DB_CACHE_TEMPLATES);
        Redis::hSet('__twig::cache::' . $this->environment . '::time::', $key, time(), Redis::__DB_CACHE_TEMPLATES);
    }

    /**
     * Loads a template from the cache.
     *
     * @param string $key The cache key
     */
    public function load($key)
    {
        $data = \Base\Service\Redis::hGet('__twig::cache::' . $this->environment . '::', $key, Redis::__DB_CACHE_TEMPLATES);

        if ($data) {
            eval('?>' . $data);
        }

        return $data;
    }

    /**
     * Returns the modification timestamp of a key.
     *
     * @param string $key The cache key
     *
     * @return int
     */
    public function getTimestamp($key)
    {
        $data = \Base\Service\Redis::hGet('__twig::cache::' . $this->environment . '::time::', $key, $key, Redis::__DB_CACHE_TEMPLATES);

        return $data;
    }
}