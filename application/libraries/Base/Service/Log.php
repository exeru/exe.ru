<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 05.04.17
 * Time: 11:57
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Log
{
    public static function push($filename, $message)
    {
        if (is_array($message)) {
            $message = implode("\n", $message);
        }

        file_put_contents(FCPATH . 'application/logs/' . $filename . '.log', $message . "\n\n", FILE_APPEND);
    }

    public static function profile($method) {
        if (!defined('PROFILE_FILE')) {
            return;
        }

        static $cache = [], $init = false;

        if (false === $init) {
            file_put_contents(PROFILE_FILE, (Auth::isAuth() ? Auth::getUserId() : 'guest') . "\n", FILE_APPEND);
            $init = microtime(true);
        }

        $mktime = microtime(true);
        if (isset($cache[$method])) {
            file_put_contents(PROFILE_FILE, sprintf('%f', ($mktime - $init)) . ' : end ' . $method . ': ' . sprintf('%f', $mktime) . ', diff: ' . sprintf('%f', ($mktime - $cache[$method])) . "\n", FILE_APPEND);
            unset($cache[$method]);
        } else {
            file_put_contents(PROFILE_FILE, sprintf('%f', ($mktime - $init)) . ' : start ' . $method . ': ' . sprintf('%f', $mktime) . "\n", FILE_APPEND);
            $cache[$method] = $mktime;
        }
    }
}