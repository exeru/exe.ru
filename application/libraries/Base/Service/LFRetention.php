<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17.05.17
 * Time: 16:12
 */

namespace Base\Service;
use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');


class LFRetention
{
    public static function getLFUidsByUids($allUids)
    {
        $result = Model\LFRetention::getLFUidsByUids($allUids);

        if (count($result) < 1) {
            return [];
        }

        return array_column($result, 'lf_uid');
    }

    public static function setAssign($lfUid, $uids)
    {
        // Уже существующие
        $existsUids = Model\LFRetention::getUidsByLFUid($lfUid);

        if (count($existsUids) > 0) {
            $existsUids = array_column($existsUids, 'exe_uid');
            $uids = array_diff($uids, $existsUids);
        }

        if (count($uids) > 0) {
            Model\LFRetention::appendAssign($lfUid, $uids);
        }
    }

    public static function uploadAll()
    {
        // Берем все идентификаторы lostfilm
        $assigns = [];
        $lfUids = Model\LFRetention::getAllAssigns();

        // Собираем связи lf_uid => [exe_uid, ....]
        foreach($lfUids as $assign) {
            $assigns[$assign['lf_uid']][] = $assign['exe_uid'];
        }

        if (count($assigns) < 1) {
            return;
        }

        // Выгружаем общий список
        $appId = 0;
        $content = '';
        foreach($assigns as $lfUid => $exeUids) {
            $content .= $lfUid . ':' . md5($lfUid . microtime(true) . $appId) . "\n";
        }

        // Сохраняем контент в файл, выгружаем список
        $filename = FCPATH . 'assets/uploads/' . 'app' . $appId . '.txt';
        file_put_contents($filename, $content);
        self::uploadFile('app' . $appId, $filename);

        // Получаем список активных на данный момент игр.
        $activeGameIds = Game::getActiveGamesId();

        if (count($activeGameIds) < 1) {
            return;
        }

        // Удаляем старые файлы, проставляем пути
        foreach($activeGameIds as $gid => $values) {
            $filename = FCPATH . 'assets/uploads/' . 'app' . $gid . '.txt';
            if (file_exists($filename)) {
                unlink($filename);
            }
            $activeGameIds[$gid] = $filename;
        }

        // Генерируем файлы по играм
        foreach($assigns as $lfUid => $exeUids) {
            // Установленные игры у пользователя
            $userGames = [];
            foreach($exeUids as $exeUid) {
                $userGames = array_merge($userGames, Game::getUserGameIds($exeUid));
            }

            if (count($userGames) < 1) {
                continue;
            }

            $userGames = array_intersect(array_keys($activeGameIds), array_unique($userGames));

            if (count($userGames) > 0) {
                foreach($userGames as $gid) {
                    $string =  $lfUid . ':' . md5($gid . microtime(true) . $gid) . "\n";
                    file_put_contents($activeGameIds[$gid], $string, FILE_APPEND);
                }
            }
        }

        // Выгружаем файлы
        foreach($activeGameIds as $gid => $filename) {
            if (file_exists($filename)) {
                self::uploadFile('app' . $gid, $filename);
            }
        }
    }

    public static function upload($daysForAll = 30, $daysForGamers = 7)
    {
        $end = time();

        // Берем все идентификаторы lostfilm
        $assigns = [];
        $lfUids = Model\LFRetention::getAllAssigns();

        // Собираем связи lf_uid => [exe_uid, ....]
        foreach($lfUids as $assign) {
            $assigns[$assign['lf_uid']][] = $assign['exe_uid'];
        }

        // Получаем время последней активности пользователя в заданном интервале
        // Если у нас нет ответа, пользователь не заходил на сайт в выбранном периоде
        // и мы не будем выгружать его ни в один список
        // Если ответ есть - сразу выгружаем в общий список
        $appId = 0;
        $content = '';
        $start = $end - ($daysForAll * 24 * 60 * 60);
        foreach($assigns as $lfUid => $exeUids) {
            $lastActionTime = User::getMaxActiveTimeForUids($exeUids, $start, $end);
            if (empty($lastActionTime)) {
                unset($assigns[$lfUid]);
                continue;
            }
            $content .= $lfUid . ':' . md5($lfUid . microtime(true) . $appId) . "\n";
        }

        if (count($assigns) < 1) {
            return false;
        }

        // Сохраняем контент в файл, выгружаем список
        $filename = FCPATH . 'assets/uploads/' . 'app' . $appId . '.txt';
        file_put_contents($filename, $content);
        self::uploadFile('app' . $appId, $filename);

        // Получаем список активных на данный момент игр.
        $activeGameIds = Game::getActiveGamesId();
        // Удаляем старые файлы, проставляем пути
        foreach($activeGameIds as $gid => $values) {
            $filename = FCPATH . 'assets/uploads/' . 'app' . $gid . '.txt';
            if (file_exists($filename)) {
                unlink($filename);
            }
            $activeGameIds[$gid] = $filename;
        }

        // Генерируем файлы по играм
        $start = $end - ($daysForGamers * 24 * 60 * 60);
        foreach($assigns as $lfUid => $exeUids) {
            // Получаем последнюю активность пользователя во включенных играх за установленный интервал
            $lastActionTime = Game::getLastVisitTimeByUidsAtGidsAtPeriod($exeUids, array_keys($activeGameIds), $start, $end);
            if (count($lastActionTime) < 1) {
                continue;
            }

            foreach($lastActionTime as $oneGameVisit) {
                $string =  $lfUid . ':' . md5($oneGameVisit['gid'] . microtime(true) . $oneGameVisit['gid']) . "\n";
                file_put_contents($activeGameIds[$oneGameVisit['gid']], $string, FILE_APPEND);
            }
        }

        // Выгружаем файлы
        foreach($activeGameIds as $gid => $filename) {
            if (file_exists($filename)) {
                self::uploadFile('app' . $gid, $filename);
            }
        }
    }

    public static function update($lfid, $games)
    {
        return Model\LFRetention::update($lfid, $games);
    }

    public static function uploadFile($alias, $filename)
    {
        echo "Upload file alias: " . $alias . ", filename: " . $filename . "\n";

        if (ENVIRONMENT == 'production') {
            $postData = [
                'act'   => 'upload',
                'alias' => $alias,
            ];

            ksort($postData);
            $params = '';
            foreach ($postData as $k => $v)
            {
                if($k != 'sig' && $k != 'file')
                    $params .= $k.'='.$v;
            }

            $postData['sig'] = md5($params . '47f9f51c17860af8d608b3116877853e');
            $postData['file'] = curl_file_create($filename);

            $ch = curl_init('http://lkn.lostfilm.tv/api/');
            curl_setopt_array($ch, [
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData,
                CURLOPT_RETURNTRANSFER => true
            ]);

            $content = curl_exec($ch);

            echo $content . "\n\n";

            curl_close($ch);
        } else {
            echo "Enviropment " . ENVIRONMENT . ", not uploaded.\n\n";
        }
    }
}