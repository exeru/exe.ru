<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 18.08.15
 * Time: 10:48
 */

namespace Base\Service;

// TODO заменить нормальным классом отправки почты
class Mail
{
    public static function send($to, $subject, $content, $from = '', $replyTo = '')
    {
        if ($from == '') {
            $from = '=?utf-8?B?' . base64_encode('Игровой портал exe.ru') . '?= <admin@' . (ENVIRONMENT == 'production' ? 'exe.ru' : (ENVIRONMENT == 'testing' ? 'test.exe.ru' : 'exe.ru.dev')) . '>';
        }

        if ($replyTo == '') {
            $replyTo = 'admin@' . (ENVIRONMENT == 'production' ? 'exe.ru' : (ENVIRONMENT == 'testing' ? 'test.exe.ru' : 'exe.ru.dev'));
        }

        $subject = '=?utf-8?B?' . base64_encode($subject) . "?=";

        $headers = '';
        $headers .= "From: " . $from . "\r\n";
        $headers .= "Reply-To: " . $replyTo. "\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";

        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";

        mail($to, $subject, $content, $headers);
    }

    public static function simple($subject, $body)
    {
        $emails = Project::getEmails();
        if (!is_array($emails)) {
            $emails = [$emails];
        }

        foreach($emails as $email) {
            self::send($email, $subject, $body);
        }

        return true;
    }
}