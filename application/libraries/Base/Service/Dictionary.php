<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.08.15
 * Time: 13:47
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Dictionary
{
    private static $_cache = [];

    public static function getItem($dictionary, $id)
    {
        return self::getList($dictionary)[$id];
    }

    public static function getList($name, $filter = [])
    {
        Log::profile(__METHOD__);
        $key = RedisKeys::dictionary . $name . '::' . md5(serialize($filter)) . '::';
        if (isset(self::$_cache[$key])) {
            return self::$_cache[$key];
        }
        $result = Redis::get($key);

        if ($result !== false) {
            $result = unserialize($result);
            self::$_cache[$key] = $result;
        } else {
            $result = Model\Dictionary::getList($name, $filter);

            if ($result != false) {
                $old = null;
                foreach($result as $index=>$value) {
                    if (!is_null($old) && $value === $old) {
                        unset($result[$index]);
                        continue;
                    }
                    $old = $value;
                }
            }
        }

        if ($result === false) {
            Log::profile(__METHOD__);
            return false;
        }

        Redis::set($key, serialize($result));
        self::$_cache[$key] = $result;

        Log::profile(__METHOD__);
        return self::$_cache[$key];
    }
}