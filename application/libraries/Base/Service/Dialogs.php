<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 22.10.15
 * Time: 15:25
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Dialogs
{
    public static function getUsersByMId($mid)
    {
        return Model\Dialogs::getUsersByMId($mid);
    }

    public static function markAsRead($uid, $dids)
    {
        return Model\Dialogs::markAsRead($uid, $dids);
    }

    public static function restoreMessages($uid, $mids)
    {
        return Model\Dialogs::restoreMessages($uid, $mids);
    }

    public static function deleteMessages($uid, $mids)
    {
        return Model\Dialogs::deleteMessages($uid, $mids);
    }

    public static function deleteDialog($uid, $fid)
    {
        return Model\Dialogs::deleteDialog($uid, $fid);
    }

    public static function sendMessage($uid, $fid, $text)
    {

        return Model\Dialogs::sendMessage($uid, $fid, $text);
    }

    public static function getDialog($uid, $fid)
    {
        $messages = Model\Dialogs::getDialog($uid, $fid);

        return $messages;
    }

    public static function getCountUnReadDialogs($uid)
    {
        return Model\Dialogs::getCountUnReadDialogs($uid);
    }

    public static function getList($uid)
    {
        $blacklist = Friends::getBlackList($uid);
        $messages = Model\Dialogs::getList($uid);
        $dialogs = [];
        foreach($messages as $message) {
            $cuid = $message['uid'] == $uid ? $message['fid'] : $message['uid'];
            if (!isset($dialogs[$cuid])) {
                $user = User::getUserInfo($cuid);
                $dialogs[$cuid] = [
                    'unread' => 0,
                    'message' => $message,
                    'user' => [
                        'uid' => $user['uid'],
                        'name' => $user['name'],
                        'last_name' => $user['last_name'],
                        'photo' => $user['photo'][70]
                    ]
                ];
                $friendType = Friends::getFriendType($uid, $cuid);
                if ($friendType == 'blacklist' || $friendType == 'blocked') {
                    $dialogs[$cuid]['user']['type'] = $friendType;
                } else {
                    $dialogs[$cuid]['user']['online'] = $user['online'];
                }
            }
            if ($message['fid'] == $uid && $message['was_read_fid'] == 'f') {
                ++$dialogs[$cuid]['unread'];
            }
        }

        return array_values($dialogs);
    }
}