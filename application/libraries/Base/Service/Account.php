<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 20.01.16
 * Time: 11:29
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Account
{
    const ONE_BONUS_COST = 1; // Стоимость одного рубля

    const increment = 0; // Добавление денег на баланс у пользователя
    const decrement = 1; // Расход денег с баланса пользователя
    const decrementGame = 2; // Заказ вывода средств на контракт

    public static function getMaxPaymentTime()
    {
        return Model\Account::getMaxPaymentTime();
    }

    public static function getMinPaymentTime()
    {
        return Model\Account::getMinPaymentTime();
    }

    public static function getPaySystemsByPeriod($start, $end, $groupField)
    {
        return Model\Account::getPaySystemsByPeriod($start, $end, $groupField);
    }

    public static function getPurchasesMaxTime()
    {
        return Model\Account::getPurchasesMaxTime();
    }

    public static function getPurchasesMinTime()
    {
        return Model\Account::getPurchasesMinTime();
    }

    public static function getSumByGidsByPeriod($gids, $start, $end, $fieldGroup)
    {
        return Model\Account::getPurchesesSumByGidsByPeriod($gids, $start, $end, $fieldGroup);
    }

    public static function incrementBalance($uid, $sum, $receiverId, $count)
    {
        Model\Account::appendLog($uid, $sum, self::increment, $receiverId, $count);
        Model\Account::incrementBalance($uid, $count);

        User::invalidateUserInfoCache($uid);
    }

    protected static function decrementBalance($uid, $sum, $receiverId, $count)
    {
        Model\Account::appendLog($uid, $sum, self::decrement, $receiverId, $count);
        Model\Account::decrementBalance($uid, $count);
        Model\Account::incrementGameBalance($receiverId, $count);

        User::invalidateUserInfoCache($uid);

        $balance = User::getUserInfo($uid, true)['balance'];

        Events::rise('EVENT_USER_BALANCE_CHANGE', [
            'uids' => [$uid],
            'balance' => $balance,
            'type' => 'decrement',
            'message' => json_encode([
                'type' => 'balance_change',
                'message' => [
                    'type' => 'decrement',
                    'balance' => $balance
                ]
            ])
        ]);
    }

    protected static function decrementGameBalance($uid, $gid, $count)
    {

        Model\Account::appendLog($uid, $count * self::ONE_BONUS_COST, self::decrementGame, $gid, $count);
        Model\Account::decrementGameBalance($gid, $count);

        return true;
    }

    public static function getHistoryAsReciever($gid, $limit = 25)
    {
        return Model\Account::getLogByReceiver($gid, $limit);
    }

    public static function getHistory($uid, $limit = 50)
    {
        $data = Model\Account::getLog($uid, $limit);

        if (count($data) > 0) {

            get_instance()->load->helper('plural');

            foreach($data as $index=>&$row) {
                if (is_null($row['count'])) {
                    unset($data[$index]);
                    continue;
                }
                $row['date'] = date('d.m.Y', $row['time']);

                if ($row['type'] == self::increment) {
                    $row['countText'] = '+';

                    $paymentData = Payments::getHistoryData($row['receiver']);
                    $row = array_merge($row, $paymentData);
                } else {
                    $row['countText'] = '-';

                    $purchaseData = Purchases::getHistoryData($row['receiver']);
                    $row = array_merge($row, $purchaseData);
                }

                $row['countText'] .= $row['count'] . plural_str($row['count'], ' рубль', ' рубля', ' рублей');
            }
        }

        return $data;
    }

    protected static function getNewOrderNumber()
    {
        return Redis::incr("payments::lastOrder");
    }
}