<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.04.16
 * Time: 13:49
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Stat
{
    protected static function getDay($time = null)
    {
        $time = is_null($time) ? time() : $time;

        return (int) floor($time / 86400);
    }

    protected static function getMonth($time = null)
    {
        $time = is_null($time) ? time() : $time;

        return (int) (date('Y', $time) * 12 + date('m', $time));
    }

    protected static function lock($key)
    {
        return Redis::getSet($key . '::lock', 1) !== '1';
    }

    protected static function unlock($key)
    {
        return Redis::del($key . '::lock');
    }
}