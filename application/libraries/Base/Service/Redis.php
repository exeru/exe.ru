<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.08.15
 * Time: 14:09
 */
namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Redis
{
    const
        __DB_DEFAULT = 0,
        __DB_CACHE_TEMPLATES = 1,
        __DB_EXCEEDED_TEMPORARY = 2,
        __DB_SESSION_NUMBER_BY_MAIN_MARKER = 3,
        __DB_MARKER_BY_MAIN_MARKER = 4,
        __DB_LAST_MESSAGE_TIME = 5;

    private static
        $_client = null;

    /**
     * @return null|\Redis
     * @throws \RedisException
     */
    private static function get_client()
    {
        \Base\Service\Log::profile(__METHOD__);

        if (!is_null(self::$_client)) {
            \Base\Service\Log::profile(__METHOD__);

            return self::$_client;
        }

        $CI = & get_instance();
        $CI->config->load('redis');
        $hostname = $CI->config->item('hostname');
        $port = $CI->config->item('port');

        $redis = new \Redis();
        $result = $redis->connect($hostname, $port);
        if ($result === TRUE) {
            \Base\Service\Log::profile(__METHOD__);

            return self::$_client = & $redis;
        }

        throw new \RedisException('Can`t connect to server');
    }

    public static function lRange($key, $start = 0, $count = 1000, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->lRange($key, $start, $count - 1)
            ->exec();

        return $result[1];
    }

    public static function lTrim($key, $start = 1000, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->lTrim($key, $start, -1)
            ->exec();

        return $result[1];
    }

    public static function lPopAll($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->lRange($key, 0, 999)
            ->lTrim($key, 1000, -1)
            ->exec();

        return $result[1];
    }

    public static function getSet($key, $value, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->getSet($key, $value)
            ->exec();

        return $result[1];
    }

    public static function lLen($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->lLen($key)
            ->exec();

        return $result[1];
    }

    public static function lPush($key, $value, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->lPush($key, $value)
            ->exec();

        return $result[1];
    }

    public static function lPop($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->lPop($key)
            ->exec();

        return $result[1];
    }

    public static function rPush($key, $value, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->rPush($key, $value)
            ->exec();

        return $result[1];
    }

    public static function hGet($key, $hash, $db = self::__DB_DEFAULT)
    {
        \Base\Service\Log::profile(__METHOD__);

        $result = self::get_client()
            ->multi()
            ->select($db)
            ->hGet($key, $hash)
            ->exec();

        \Base\Service\Log::profile(__METHOD__);

        return $result[1];
    }

    public static function hGetAll($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->hGetAll($key)
            ->exec();

        return $result[1];
    }

    public static function hScan($key, $cursor = 0, $pattern = '*', $count = 10, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->hScan($key, $cursor, $pattern, $count);

        return $result;
    }

    public static function hKeys($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->hKeys($key)
            ->exec();

        return $result[1];
    }

    public static function hDel($key, $hash, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->hDel($key, $hash)
            ->exec();

        return $result[1];
    }


    public static function hLen($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->hLen($key)
            ->exec();

        return $result[1];
    }

    public static function hSet($key, $hash, $value, $db = self::__DB_DEFAULT)
    {
        \Base\Service\Log::profile(__METHOD__);

        $result = self::get_client()
            ->multi()
            ->select($db)
            ->hSet($key, $hash, $value)
            ->exec();

        \Base\Service\Log::profile(__METHOD__);

        return $result[1];
    }

    public static function hMSet($key, $hashesKeys, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->hMset($key, $hashesKeys)
            ->exec();

        return $result[1];
    }

    public static function incr($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->incr($key)
            ->exec();

        return $result[1];
    }

    public static function incrBy($key, $count, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->incrBy($key, $count)
            ->exec();

        return $result[1];
    }

    public static function mGet($keys, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->mGet($keys)
            ->exec();

        return $result[1];
    }

    public static function get($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->get($key)
            ->exec();

        return $result[1];
    }

    public static function ttl($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->ttl($key)
            ->exec();

        return $result[1];
    }

    public static function expire($key, $ttl, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->expire($key, $ttl)
            ->exec();

        return $result[1];
    }

    /**
     * @param string $pattern
     * @throws \RedisException
     * @return array
     */
    public static function getKeys($pattern = '*', $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->getKeys($pattern)
            ->exec();

        return $result[1];
    }

    public static function set($key, $value, $ttl = 0, $db = self::__DB_DEFAULT)
    {
        if ($ttl > 0) {
            $result = self::get_client()
                ->multi()
                ->select($db)
                ->setex($key, $ttl, $value)
                ->exec();

            return $result[1];
        }

        $result = self::get_client()
            ->multi()
            ->select($db)
            ->set($key, $value)
            ->exec();

        return $result[1];
    }

    public static function del($key, $db = self::__DB_DEFAULT)
    {
        $result = self::get_client()
            ->multi()
            ->select($db)
            ->del($key)
            ->exec();

        return $result[1];
    }
}