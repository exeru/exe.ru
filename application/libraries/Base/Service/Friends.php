<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.09.15
 * Time: 14:17
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Friends
 * @package Base\Service
 */
class Friends
{
    /**
     * @deprecated использовалась до появления флага в базе
     */
    const _REDIS_KEY_REJECTLIST_PREFIX = 'friendsRejected::';

    /**
     * @var array внутренний микро-кеш друзей
     */
    private static $_cacheFriends = [];

    /** Реакция на события */

    public static function eventManager($eventType, $params)
    {
        switch ($eventType) {
            case 'EVENT_USER_APPEND_FRIEND':
            case 'EVENT_USER_ACCEPT_FRIEND': {
                self::appendFriend($params['uid'], $params['fid']);
                break;
            }
            case 'EVENT_USER_INVITE_TO_GAME': {
                self::appendNotification($params['uids'], $params['fid'], $params['gid'], time(), 'Приглашает в игру %game_link%', $params['request_key'], 'invite');
                break;
            }
            case 'EVENT_USER_INVITE_TO_FRIEND_BY_GAME': {
                foreach ($params['fids'] as $fid) {
                    self::appendFriend($params['uid'], $fid, $params['gid']);
                }
                break;
            }
        }
    }


    /** Работа с черным списком */

    public static function getBlackList($uid)
    {
        return array_column(Model\Friends::getBlackList($uid), 'fid', 'fid');
    }

    public static function toBlackList($uid, $fid)
    {
        $result = Model\Friends::toBlackList($uid, $fid);

        UserLog::save($uid, null, UserLog::TO_BLACKLIST, $fid, Auth::getUserMarker());
        UserLog::save($fid, null, UserLog::USER_TO_BLACKLIST, $uid, Auth::getUserMarker());

        self::invalidateFriendsCache($uid);
        self::invalidateFriendsCache($fid);

        return $result;
    }

    public static function fromBlackList($uid, $fid)
    {
        $result = Model\Friends::fromBlackList($uid, $fid);

        UserLog::save($uid, null, UserLog::FROM_BLACKLIST, $fid, Auth::getUserMarker());
        UserLog::save($fid, null, UserLog::USER_FROM_BLACKLIST, $uid, Auth::getUserMarker());

        self::invalidateFriendsCache($uid);
        self::invalidateFriendsCache($fid);

        return $result;
    }


    /** Массивы пользователей */

    public static function getFriendsI($uid, $start = 0, $limit = 1000)
    {
        $rating = $result = [];

        $data = Model\Friends::getFriendsI($uid, $start, $limit);

        if (count($data) > 0) {
            $gameIdsWithPermissions = Game::getActiveGamesIdWithPermissions($uid);

            foreach($data as $row) {
                $user = User::getUserInfo($row['fid']);
                if (isset($row['gid']) && $row['gid'] > 0 && in_array($row['gid'], $gameIdsWithPermissions)) {
                    $user['game'] = Game::getGameById($row['gid'], true);
                    $user['game']['image'] = '/assets/img/app/'.$row['gid'].'/80x80/'. DPRIMAGEPREFIX .$user['game']['image_80x80'];
                }
                if (!isset($rating[$user['rating']])) {
                    $rating[$user['rating']] = [];
                }

                $rating[$user['rating']][] = $user;
            }

            if (count($rating) > 0) {
                krsort($rating);

                foreach($rating as $values) {
                    $result = array_merge($result, $values);
                }
            }
        }

        return $result;
    }

    public static function getFriendsMe($uid, $start = 0, $limit = 1000)
    {
        $rating = $result = [];

        $data = Model\Friends::getFriendsMe($uid, $start, $limit);

        if (count($data) > 0) {
            $gameIdsWithPermissions = Game::getActiveGamesIdWithPermissions($uid);

            foreach($data as $row) {
                $user = User::getUserInfo($row['uid']);

                if (isset($row['gid']) && $row['gid'] > 0 && in_array($row['gid'], $gameIdsWithPermissions)) {
                    $user['game'] = Game::getGameById($row['gid'], true);
                    $user['game']['image'] = '/assets/img/app/'.$row['gid'].'/80x80/'. DPRIMAGEPREFIX .$user['game']['image_80x80'];
                }

                if (!isset($rating[$user['rating']])) {
                    $rating[$user['rating']] = [];
                }

                $rating[$user['rating']][] = $user;
            }

            if (count($rating) > 0) {
                krsort($rating);

                foreach($rating as $values) {
                    $result = array_merge($result, $values);
                }
            }
        }

        return $result;
    }

    public static function getFriendsBoth($uid, $start = 0, $limit = 1000)
    {
        $rating = $result = [];

        $data = Model\Friends::getFriendsBoth($uid, $start, $limit);

        if (count($data) > 0) {
            foreach($data as $row) {
                $user = User::getUserInfo($row['fid']);

                if (!isset($rating[$user['rating']])) {
                    $rating[$user['rating']] = [];
                }

                $rating[$user['rating']][] = $user;
            }

            if (count($rating) > 0) {
                krsort($rating);

                foreach($rating as $values) {
                    $result = array_merge($result, $values);
                }
            }

        }

        return $result;
    }


    /** Запросы на добавление и удаление друзей */

    public static function appendFriend($uid, $fid, $gid = null)
    {
        // Убираем пользователя у себя из отклоненных
        $key = self::_REDIS_KEY_REJECTLIST_PREFIX . $uid . '::' . $fid;
        Redis::del($key);

        Model\Friends::appendFriend($uid, $fid, $gid);

        UserLog::save($uid, null, UserLog::APPEND_FRIEND, $fid, Auth::getUserMarker());
        UserLog::save($fid, null, UserLog::USER_TO_FRIENDS, $uid, Auth::getUserMarker());

        self::invalidateFriendsCache([$uid, $fid]);

        \Base\Service\Api::usersChange($uid, 2);
        \Base\Service\Api::usersChange($fid, 2);

        return;
    }

    public static function cancelFriend($uid, $fid)
    {
        Model\Friends::deleteFriend($uid, $fid);

        UserLog::save($uid, null, UserLog::DELETE_FRIEND, $fid, Auth::getUserMarker());
        UserLog::save($fid, null, UserLog::USER_FROM_FRIENDS, $uid, Auth::getUserMarker());

        self::invalidateFriendsCache([$uid, $fid]);

        \Base\Service\Api::usersChange($uid, 2);

        return;
    }

    public static function rejectFriend($uid, $fid)
    {
        self::invalidateFriendsCache([$uid, $fid]);

        return Model\Friends::rejectFriend($uid, $fid);
    }


    /** Уведомления */

    /**
     * Добавляет уведомления от пользователя другим
     *
     * @param array integer $uids идентификаторы пользователей, которым необходимо отправить уведомления
     * @param integer $fid идентификатор пользователя, от которого отправляются уведомления
     * @param integer $gid идентификатор игры, о которой идет речь
     * @param integer $time timestamp создания уведомления
     * @param string $content текст уведомления
     * @param null|string $params дополнительная строка, будет передана в iframe при переходе по ссылки на игру
     * @param string $requestType тип запроса (request|invite) - запрос помощи или приглашение в игру.
     * @return boolean true в случае успеха
     */
    public static function appendNotification($uids, $fid, $gid, $time, $content, $params = NULL, $requestType = 'request')
    {
        if (!is_array($uids)) {
            $uids = [$uids];
        }

        $redisKeyPrefix = RedisKeys::friendsUserInviteToGame . $fid . '::' . $gid . '::';
        array_walk($uids, function ($uid) use ($redisKeyPrefix) {
            $key = $redisKeyPrefix . $uid . '::';
            Redis::set($key, true);
        });

        return Model\Friends::appendNotification($uids, $fid, $gid, $time, $content, $params, $requestType);
    }

    /**
     * Возвращает уведомления для данного пользователя от других
     *
     * @param integer $uid идентификатор пользователя
     * @return array набор ассоциативных массивов ['fid', 'gid', 'content', 'nid', 'params', 'request_type']
     */
    public static function getNotifications($uid)
    {
        $blacklist = Friends::getBlackList($uid);
        return Model\Friends::getNotifications($uid, $blacklist);
    }

    /**
     * Удаляет уведомление.
     *
     * Если указан идентификатор уведомления, поиск будет осуществлен по нему.
     * В противном случае будут удалены все сообщения с комбинацией $uid, $fid, $gid
     *
     * @param integer $uid идентификатор получателя
     * @param integer $fid идентификатор отправителя
     * @param integer $gid идентификатор игры
     * @param integer $nid идентификатор сообщения
     * @return boolean true в случае успеха
     */
    public static function deleteNotification($uid, $fid, $gid, $nid)
    {
        if ($fid === 0) {
            $fid = Model\Friends::getFIdByNId($nid);
        }

        $redisKey = RedisKeys::friendsUserInviteToGame . $fid . '::' . $gid . '::' . $uid . '::';
        Redis::del($redisKey);

        return Model\Friends::deleteNotification($uid, $fid, $gid, $nid);
    }


    /** Разное */

    public static function isRejectedFriend($uid, $fid) { //TODO
        return (false !== Redis::get(self::_REDIS_KEY_REJECTLIST_PREFIX . $uid . '::' . $fid));
    }

    public static function hasInviteToGame($uid, $fid, $gid)
    {
        $redisKey = RedisKeys::friendsUserInviteToGame . $uid . '::' . $gid . '::' . $fid . '::';

        return Redis::get($redisKey) == 1;
    }

    public static function invalidateFriendsCache($uid)
    {
        if (is_array($uid)) {
            array_walk($uid, function($oneUid) {
                self::invalidateFriendsCache($oneUid);
            });
        } else {
            if (isset(self::$_cacheFriends[$uid])) {
                unset(self::$_cacheFriends[$uid]);
            }
            Redis::del(RedisKeys::friendsByTypeArray . $uid . '::');
        }
    }

    public static function getCountFriendsByTypes($uid)
    {
        $data = [
            'both' => Model\Friends::getFriendsBothCount($uid),
            'i' => Model\Friends::getFriendsICount($uid),
            'me' => self::getFriendsMeCount($uid)
        ];

        return $data;
    }

    public static function getFriendsMeCount($uid)
    {
        return Model\Friends::getFriendsMeCount($uid);
    }

    public static function getFriendType($uid, $fid)
    { // TODO

        \Base\Service\Log::profile(__METHOD__);

        if ($uid === $fid) {
            \Base\Service\Log::profile(__METHOD__);
            return 'self';
        }

        $blacklist = Friends::getBlackList($uid);
        if (isset($blacklist[$fid])) {
            \Base\Service\Log::profile(__METHOD__);
            return 'blacklist'; // В собственном черном списке
        }

        $blacklist = Friends::getBlackList($fid);
        if (isset($blacklist[$uid])) {
            \Base\Service\Log::profile(__METHOD__);
            return 'blocked'; // В чужом черном списке
        }

        $friendsByTypeArray = self::getFriendsByTypeArray($uid, true);

        switch(true) {
            case isset($friendsByTypeArray['both'][$fid]): {
                \Base\Service\Log::profile(__METHOD__);
                return 'both';
            }
            case isset($friendsByTypeArray['i'][$fid]): {
                \Base\Service\Log::profile(__METHOD__);
                return 'i';
            }
            case isset($friendsByTypeArray['me'][$fid]): {
                \Base\Service\Log::profile(__METHOD__);
                return (self::isRejectedFriend($uid, $fid) ? 'rejected' : 'me');
            }
            default: {
                \Base\Service\Log::profile(__METHOD__);
                return '';
            }
        }
    }

    public static function getFriendsByTypeArray($userId, $rejected = false)
    {
        Log::profile(__METHOD__);

        if (isset(self::$_cacheFriends[$userId][$rejected])) {
            Log::profile(__METHOD__);
            return self::$_cacheFriends[$userId][$rejected];
        }

        $redisKey = RedisKeys::friendsByTypeArray . $userId . '::';
        $data = Redis::get($redisKey);
        if ($data !== false) {
            self::$_cacheFriends[$userId] = unserialize($data);

            if (isset(self::$_cacheFriends[$userId][$rejected])) {
                Log::profile(__METHOD__);
                return self::$_cacheFriends[$userId][$rejected];
            }
        }

        $i = array_column(Model\Friends::getFriendsI($userId), 'fid', 'fid'); // Я отправил запрос, меня не добавили
        $me = array_column(Model\Friends::getFriendsMe($userId), 'uid', 'uid'); // Мне отправляли запрос, я не добавил
        $both = array_column(Model\Friends::getFriendsBoth($userId), 'fid', 'fid'); // Встречное пересечение, дружим

        self::$_cacheFriends[$userId][false] = [ // Кеш, который не содержит отклоненные мной заявки на добавление в друзья
            'both' => $both, // Встречное пересечение, дружим
            'i' => array_diff_key($i, $both), // Я отправил запрос, меня не добавили
            'me' => array_diff_key($me, $both) // Мне отправляли запрос, я не добавил
        ];

        $i += array_column(Model\Friends::getFriendsRejected($userId), 'fid', 'fid');

        self::$_cacheFriends[$userId][true] = [ // Кеш, который содержит отклоненные мной заявки на добавление в друзья
            'both' => $both, // Встречное пересечение, дружим
            'i' => array_diff_key($i, $both), // Я отправил запрос, меня не добавили
            'me' => array_diff_key($me, $both) // Мне отправляли запрос, я не добавил
        ];

        Redis::set($redisKey, serialize(self::$_cacheFriends[$userId]), 3600);

        Log::profile(__METHOD__);

        return self::$_cacheFriends[$userId][$rejected];
    }
}