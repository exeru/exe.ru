<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 21.08.15
 * Time: 11:00
 */

namespace Base\Service;

use \Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Game
{
    const countCachedCatalogGuestHTML = 10;

    const appImagesPath = '/assets/img/app/';

    const actionTypes = [ // http://office.ugo.ru/issues/4495
        1 => ['title' => 'Азартные', 'color' => '#d45c9e', 'single' => 'Азартная'],
        //4. Азартные - d45c9e (пинк)

        2 => ['title' => 'Аркады', 'color' => '#5762d5', 'single' => 'Аркада'],
        //3. Аркады - 5762d5 (синий)

        3 => ['title' => 'Гонки', 'color' => '#2980b9', 'single' => 'Гонка'],
        //12. Гонки - 2980b9 (серо-голубой)

        4 => ['title' => 'Логические', 'color' => '#8eb021', 'single' => 'Логическая'],
        //10. Логические - 8eb021 (лайм)

        5 => ['title' => 'Приключения', 'color' => '#21b2ad', 'single' => 'Приключения'],
        //5. Приключения - 21b2ad (морская волна)

        6 => ['title' => 'Ролевые', 'color' => '#27ae60', 'single' => 'Ролевая'],
        //6. Ролевые - 27ae60 (зеленый)

        7 => ['title' => 'Симуляторы', 'color' => '#3a6f81', 'single' => 'Симулятор'],
        //8. Симуляторы - 3a6f81 (нави)

        8 => ['title' => 'Спортивные', 'color' => '#5065a1', 'single' => 'Спортивная'],
        //11. Спортивные - 5065a1 (серо-синий)

        9 => ['title' => 'Стратегии', 'color' => '#3079df', 'single' => 'Стратегия'],
        //1. Стратегии - 3079df (голубой)

        10 => ['title' => 'Фермы', 'color' => '#e74c3c', 'single' => 'Ферма'],
        //2. Фермы - e74c3c (красный)

        11 => ['title' => 'Шутеры', 'color' => '#ffa800', 'single' => 'Шутер'],
        //9. Шутеры - ffa800 (желтый)

        12 => ['title' => 'Экономические', 'color' => '#e77e23', 'single' => 'Экономическая'],
        //7. Экономические - e77e23 (оранжевый)

        13 => ['title' => 'Другие', 'color' => '#8e44ad', 'single' => ''],
        //13. Другие - 8e44ad (магента)

    ];

    private static $_userGamesCache = [], $_activeGamesIdCache = null;

    public static function getGamesByContractId($cid)
    {
        return Model\Game::getGamesByContractId($cid);
    }

    public static function getGamesWithContracts()
    {
        return Model\Game::getGamesWithContracts();
    }

    public static function getCheckLog($gid)
    {
        return Model\Game::getCheckLog($gid);
    }

    public static function pushCheckLog($gid, $uid, $active, $comment)
    {
        return Model\Game::pushCheckLog($gid, $uid, $active, $comment, time());
    }

    public static function getEditLog($gid)
    {
        return Model\Game::getEditLog($gid);
    }

    private static function pushEditLog($gid, $uid, $fields, $time)
    {
        return Model\Game::pushEditLog($gid, $uid, $fields, $time);
    }

    public static function getGamersId($gid)
    {
        return Model\Game::getGamersId($gid);
    }

    public static function setGameDataFieldsById($gid, $updatedFields)
    {
        $uid = Auth::getUserId();

        if (!Roles::isModerator($uid)) {
            self::pushEditLog($gid, $uid, array_keys($updatedFields), time());
            $updatedFields['dev_last_changes'] = time();
            $updatedFields['dev_last_changes_checked'] = 0;
        }

        Model\Game::setGameDataFieldsById($gid, $updatedFields);

        self::resetGameCache($gid);
        self::resetActiveGamesIdCache();
        self::resetCatalogCache();
        self::resetGamersCache($gid);

        return true;
    }

    public static function getGameDataFieldsById($gid, $fields)
    {
        $data = Model\Game::getGameDataFieldsById($gid, $fields);

        array_walk($data, function(&$value, $key) use ($gid) {
            if (strpos($key, 'image_') === 0 && !is_null($value)) {
                $value = self::appImagesPath . $gid . '/' . substr($key, 6) . '/' . $value;
            }
        });

        return $data;
    }

    public static function setGameSettings($uid, $gid, $values)
    {
        return Model\Game::setGameSettings($uid, $gid, $values);
    }

    public static function getGameSettings($uid, $gid)
    {
        return Model\Game::getGameSettings($uid, $gid);
    }

    public static function addGame($uid, $type, $title, $description, $apiKey, $height)
    {
        $gid = Model\Game::addGame($uid, $type, $title, $description, $apiKey, $height);

        Roles::add($gid, $uid, Roles::USER_OWNER);

        return $gid;
    }

    public static function unInstall($uid, $gid, $timestamp)
    {
        self::fillUserGamesCache($uid);

        if (!self::isGamer($uid, $gid)) {
            return false;
        }

        Model\Game::unInstall($uid, $gid, $timestamp);

        if (isset(self::$_userGamesCache[$uid]['installed'][$gid])) {
            unset(self::$_userGamesCache[$uid]['installed'][$gid]);
            self::saveUserGamesCache($uid);
        }

        StatsGame::insertInstallDelete($gid, $uid, false);

        return true;
    }

    public static function getGameById($gid, $hidden = false)
    {
        Log::profile(__METHOD__);

        $redisKey = RedisKeys::gameGameInfo . ENVIRONMENT . '::' . $gid . '::' . ((int) $hidden) . '::';
        $game = Redis::get($redisKey);

        if (!$game) {
            Log::profile(__METHOD__ . '::DB');
            $game = Model\Game::getGameById($gid, $hidden);
            Log::profile(__METHOD__ . '::DB');
            Redis::set($redisKey, serialize($game), 1800);
        } else {
            $game = unserialize($game);
        }

        if ($game == false) {
            Log::profile(__METHOD__);
            return false;
        }

        Log::profile(__METHOD__);
        return $game;
    }

    public static function getUserGames($uid)
    {
        \Base\Service\Log::profile(__METHOD__);

        $installedGameIds = self::getUserGameIds($uid);
        if (count($installedGameIds) < 1) {
            return [];
        }

        $userGames = Model\Game::getGamesByIds($installedGameIds);

        foreach($userGames as $index=>$game) {
            if (mb_strlen($game['title']) > 14) {
                $game['title'] = mb_substr($game['title'], 0, 14);
                $pos = mb_strrpos($game['title'], ' ');
                $userGames[$index]['title'] = mb_substr($game['title'], 0, $pos) . '...';
            }
        }

        \Base\Service\Log::profile(__METHOD__);

        return $userGames;
    }

    public static function getUserGameIds($uid)
    {
        self::fillUserGamesCache($uid);

        return array_keys(self::$_userGamesCache[$uid]['installed']);
    }

    public static function uptimeGamer($uid, $gid)
    {
        return Model\Game::uptimeGamer($uid, $gid);
    }

    public static function uptimeGame($uid, $gid)
    {
        if (self::isInstalled($uid, $gid)) { // Игра установлена, обновляем время захода в игру
            $wasInstalled = true;
            self::uptimeGamer($uid, $gid);
        } elseif (self::isGamer($uid, $gid)) { // Игра была удалена, отмечаем установку, и обновляем время захода
            $wasInstalled = false;
            // Отметить игру установленной и обновить время
            Model\Game::setInstalledAndUptimeGamer($uid, $gid);

            self::$_userGamesCache[$uid]['installed'][$gid] = true;
            self::saveUserGamesCache($uid);
        } else { // Игра не устанавливалась, устанавливаем игру и время
            $wasInstalled = false;
            Model\Game::appendGamer($uid, $gid);

            self::$_userGamesCache[$uid]['all'][$gid] = true;
            self::$_userGamesCache[$uid]['installed'][$gid] = true;
            self::saveUserGamesCache($uid);
        }

        return $wasInstalled;
    }

    public static function deleteOldSessions()
    {
        return Model\Game::deleteOldSessions(time() - 8 * 86400);
    }

    private static function isGamer($uid, $gid)
    {
        self::fillUserGamesCache($uid);

        return isset(self::$_userGamesCache[$uid]['all'][$gid]);
    }

    private static function isInstalled($uid, $gid)
    {
        self::fillUserGamesCache($uid);

        return isset(self::$_userGamesCache[$uid]['installed'][$gid]);
    }

    private static function fillUserGamesCache($uid)
    {
        if (isset(self::$_userGamesCache[$uid])) {
            return false;
        }

        return self::loadUserGamesCache($uid);
    }

    private static function loadUserGamesCache($uid)
    {
        \Base\Service\Log::profile(__METHOD__);
        $key = RedisKeys::gameUserGames . $uid . '::';
        $data = Redis::get($key);

        if ($data !== false && !is_numeric($data)) {
            $data = unserialize($data);
            $activeGamesId = self::getActiveGamesIdWithPermissions($uid);

            $diff = array_diff(array_keys($data['all']), $activeGamesId);
            if (count($diff) == 0) {
                self::$_userGamesCache[$uid] = $data;
                \Base\Service\Log::profile(__METHOD__);
                return true;
            }
        }

        self::$_userGamesCache[$uid] = [
            'all' => [],
            'installed' => []
        ];

        $data = Model\Game::getUserGamesForCache($uid);
        if (count($data) > 0) {
            $activeGamesId = self::getActiveGamesIdWithPermissions($uid);

            foreach($data as $row) {
                if (!isset($activeGamesId[$row['gid']])) {
                    continue;
                }
                self::$_userGamesCache[$uid]['all'][$row['gid']] = true;
                if ($row['installed'] == 1) {
                    self::$_userGamesCache[$uid]['installed'][$row['gid']] = true;
                }
            }
        }

        self::saveUserGamesCache($uid);

        \Base\Service\Log::profile(__METHOD__);

        return true;
    }

    private static function saveUserGamesCache($uid)
    {
        \Base\Service\Log::profile(__METHOD__);

        if (!isset(self::$_userGamesCache[$uid])) {
            \Base\Service\Log::profile(__METHOD__);
            return false;
        }

        $key = RedisKeys::gameUserGames . $uid . '::';

        Redis::set($key, serialize(self::$_userGamesCache[$uid]), 86400);

        \Base\Service\Log::profile(__METHOD__);
        return true;
    }

    public static function getGameIdBySid($sid)
    {
        return Model\Game::getGameIdBySid($sid);
    }

    public static function uptimeSession($uid, $gid)
    {
        return Model\Game::uptimeSession($uid, $gid);
    }

    public static function getSessionId($uid, $gid)
    {  // TODO ? Вынести в отдельный сервис ?
        $gameSessionId = Model\Game::getSessionId($uid, $gid);

        if ($gameSessionId !== false) {
            self::uptimeSession($uid, $gid);
        } else {
            $gameSessionId = Model\Game::createSession($uid, $gid);
        }

        $redisKeyMarkerGSidTime = RedisKeys::userMarkerGameSession . Auth::getUserMarker() . '::';
        Redis::hSet($redisKeyMarkerGSidTime, $gameSessionId, time());

        return $gameSessionId;
    }

    public static function notificationGrade($uid, $gid, $nid, $type)
    {
        $notification = Notifications::getGameNotificationByUIdAndNId($uid, $gid, $nid);
        if ($notification == false) {
            return false;
        }

        Notifications::deleteGameNotification($nid);

        $incrementField = ($type == 'positive') ? 'succesnot': 'failurenot';

        return Notifications::updateStat($gid, $notification['time'], $notification['type'], $notification['ntype'], $incrementField);
    }

    public static function getCountPlayers($gid)
    {
        return (int) Model\Game::getCountPlayers($gid);
    }

    public static function getAllGamesSortedByStatus($limit = 100, $offset = 0)
    {
        return Model\Game::getAllGamesSortedByStatus($limit, $offset);
    }
    public static function getAllGamesId()
    {
        return array_column(Model\Game::getAllGamesId(), 'gid', 'gid');
    }

    public static function getLastVisitTimeByUidsAtGidsAtPeriod($uids, $gids, $start, $end)
    {
        return Model\Game::getLastVisitTimeByUidsAtGidsAtPeriod($uids, $gids, $start, $end);
    }

    public static function getLastVisitTimeByUids($uids)
    {
        return Model\Game::getLastVisitTimeByUids($uids);
    }

    public static function getPurchasesInfoByGameId($gid)
    {
        return Model\Game::getPurchasesInfoByGameId($gid);
    }

    public static function getAuthKey($uid, $gid)
    {
        $secretKey = Model\Game::getSecretKey($gid);

        return md5($gid . '_' . $uid . '_' . $secretKey);
    }

    public static function getActiveGamesId()
    {
        if (!is_null(self::$_activeGamesIdCache)) {
            return self::$_activeGamesIdCache;
        }

        $redisKey = RedisKeys::gameActiveGameIds;
        $ids =  Redis::get($redisKey);

        if ($ids !== false) {
            return self::$_activeGamesIdCache = unserialize($ids);
        }

        $ids = array_column(Model\Game::getActiveGamesId(), 'gid', 'gid');
        Redis::set($redisKey, serialize($ids), 3600);

        return self::$_activeGamesIdCache = $ids;
    }

    public static function getActiveGamesIdWithPermissions($uid)
    {
        \Base\Service\Log::profile(__METHOD__);

        static $gamesId = null;

        if (isset($gamesId[$uid])) {
            \Base\Service\Log::profile(__METHOD__);
            return $gamesId[$uid];
        }

        $gamesIdByPermissions = Roles::getGames($uid);

        if (count($gamesIdByPermissions) == 0) {
            \Base\Service\Log::profile(__METHOD__);
            return $gamesId = self::getActiveGamesId();
        }

        $gamesIdByPermissions = array_flip($gamesIdByPermissions) + self::getActiveGamesId();

        \Base\Service\Log::profile(__METHOD__);
        return $gamesId[$uid] = array_combine(array_keys($gamesIdByPermissions), array_keys($gamesIdByPermissions));
    }

    private static function resetActiveGamesIdCache()
    {
        Redis::del(RedisKeys::gameActiveGameIds);
        self::$_activeGamesIdCache = null;

        return true;
    }

    private static function resetCatalogCache()
    {
        $keys = Redis::getKeys(RedisKeys::gameCatalogGuest . '*');
        if (count($keys) > 0) {
            foreach($keys as $key) {
                Redis::del($key);
            }
        }
    }

    private static function resetGamersCache($gid)
    {
        $gamers = array_column(self::getGamersId($gid), 'uid', 'uid');
        if (count($gamers) > 0) {
            foreach($gamers as $uid) {
                self::resetUserGamesCache((int) $uid);
            }
        }
    }

    public static function resetUserGamesCache($uid)
    {
        Redis::del(RedisKeys::gameUserGames . $uid . '::');
        unset(self::$_userGamesCache[$uid]);

        return true;
    }

    public static function resetGameCache($gid)
    {
        $redisKey = RedisKeys::gameGameInfo . ENVIRONMENT . '::' . $gid . '::' . ((int) false) . '::';
        Redis::del($redisKey);
        $redisKey = RedisKeys::gameGameInfo . ENVIRONMENT . '::' . $gid . '::' . ((int) true) . '::';
        Redis::del($redisKey);
    }

    public static function setCaruselData($data)
    {
        return Redis::set(RedisKeys::gameCarusel, serialize($data));
    }

    public static function getCaruselData()
    {
        $data = Redis::get(RedisKeys::gameCarusel);

        if ($data === false) {
            $data = [
                [
                    'gid'   => 3,
                    'title' => 'Гримм: Ферма с историями',
                    'text'  => 'Станьте хозяином сказочной фермы! Соберите самый сочный урожай, торгуйте лучшими товарами и участвуйте в волшебных историях!',
                    'enabled' => false
                ],
                [
                    'gid'   => 4,
                    'title' => 'Родина',
                    'text'  => 'Быть фермером теперь легко и увлекательно. Присоединяйтесь и наслаждайтесь игрой!',
                    'enabled' => false
                ],
                [
                    'gid'   => 9,
                    'title' => 'Знамя Войны',
                    'text'  => 'В игре «Знамя Войны» вам предстоит взять на себя роль полководца одной из сверхдержав периода XVIII-XIX веков!',
                    'enabled' => true
                ],
                [
                    'gid'   => 19,
                    'title' => 'Бон Вояж',
                    'text'  => 'Увлекательная игра-путешествие в жанре "три в ряд".',
                    'enabled' => false
                ],
                [
                    'gid'   => 10,
                    'title' => 'Штурм',
                    'text'  => 'Военная мощь сосредоточена в Ваших руках, но сможете ли Вы изменить ход истории?',
                    'enabled' => false
                ],
                [
                    'gid'   => 11,
                    'title' => 'Гроза Морей',
                    'text'  => 'Захватывающие приключения пирата и его команды!',
                    'enabled' => false
                ],
                [
                    'gid'   => 12,
                    'title' => 'Нано-Ферма',
                    'text'  => 'На этой ферме все необычное, начиная с грядок и животных и заканчивая лесом с монстрами, который окружает ваш участок!',
                    'enabled' => true
                ],
                [
                    'gid'   => 14,
                    'title' => 'Королевский Замес',
                    'text'  => 'Собирай легендарную армию и круши чужие королевства!',
                    'enabled' => false
                ],
                [
                    'gid'   => 39,
                    'title' => 'Путь Воина: Битва Героев',
                    'text'  => 'Сражайтесь с другими воинами, побеждайте монстров, проходите приключения и принимайте участие в турнирах!',
                    'enabled' => false
                ],
                [
                    'gid'   => 47,
                    'title' => 'Music Wars',
                    'text'  => 'Сражайтесь за любимый музыкальный стиль!',
                    'enabled' => false
                ],
                [
                    'gid'   => 62,
                    'title' => 'Битва Танков',
                    'text'  => 'Спасите свою Родину от бедствия войны!',
                    'enabled' => false
                ],
                [
                    'gid'   => 70,
                    'title' => 'Небеса: уникальная MMORPG',
                    'text'  => 'Небеса - это удивительный мир, который уже полюбили миллионы игроков!',
                    'enabled' => true
                ],
                [
                    'gid'   => 76,
                    'title' => 'Ходячая Нежить',
                    'text'  => 'Присоединяйся к сражению с ордами зомби и спаси этот мир! Все зависит только от тебя!',
                    'enabled' => false
                ],
                [
                    'gid'   => 89,
                    'title' => 'Imperial Hero II',
                    'text'  => 'Сражайтесь с опасными разбойниками, разыскивайте мифические артефакты, окунитесь в борьбу за власть!',
                    'enabled' => false
                ],
                [
                    'gid'   => 97,
                    'title' => 'Обитель Зла',
                    'text'  => 'Присоединяйтесь к кровавому сражению с мутантами и одержите победу над загадочным «Вирус Т»!',
                    'enabled' => false
                ],
                [
                    'gid'   => 103,
                    'title' => 'Вега Микс: три в ряд',
                    'text'  => 'Выбор настоящего фаната жанра "три в ряд" - головоломка Вега Микс!',
                    'enabled' => true
                ],
                [
                    'gid'   => 106,
                    'title' => 'Империя Онлайн 2',
                    'text'  => 'Пришло время занять трон и создать величайшую Империю!',
                    'enabled' => true
                ],
                [
                    'gid'   => 121,
                    'title' => 'Шторм Онлайн',
                    'text'  => 'Используй меч и магию, чтобы вернуть мир объятой адским пламенем земле!',
                    'enabled' => true
                ],
            ];
            self::setCaruselData($data);
        } else {
            $data = unserialize($data);
        }

        return $data;
    }

    public static function getCarusel()
    {
        $data = self::getCaruselData();

        $carusel = [];

        foreach($data as $row) {
            if ($row['enabled']) {

                $game = Game::getGameById($row['gid']);
                if ($game) {
                    $row['color'] = self::actionTypes[$game['type']]['color'];
                }

                array_push($carusel, $row);
            }
        }

        shuffle($carusel);

        return $carusel;
    }

    public static function getCatalog()
    {
        $redisKey = RedisKeys::gameCatalogGuest;
        $catalog =  Redis::get($redisKey);

        if ($catalog !== false) {
            $catalog = unserialize($catalog);

            $activeGamesId = self::getActiveGamesId();
            $catalogGamesId = array_column($catalog, 'gid', 'gid');

            $diff = array_diff($catalogGamesId, $activeGamesId);

            if (count($diff) == 0) {
                shuffle($catalog);

                return $catalog;
            }
        }

        $catalog = Model\Game::getCatalog(true, true, 24);

        foreach($catalog as $index=>$game) {
            if (mb_strlen($game['title']) > 40) {
                $game['title'] = mb_substr($game['title'], 0, 40);
                $pos = mb_strrpos($game['title'], ' ');
                $catalog[$index]['title'] = mb_substr($game['title'], 0, $pos) . '...';
            }
        }

        Redis::set($redisKey, serialize($catalog), 3600);

        shuffle($catalog);

        return $catalog;
    }
}