<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.06.17
 * Time: 14:17
 */

namespace Base\Service;

use Base\Provider\Payment;

defined('BASEPATH') OR exit('No direct script access allowed');

class Promo
{
    const debug = false;

    const VIEW = [
        1 => [
            'head_script' => '/assets/js/promo/1/payments.js',
            'css' => '/assets/css/promo/1/style.css',
            'body' => 'promo/1/banner.twig',
            'payment' => 'promo/1/payment.twig'
        ],
        2 => [
            'head_script' => [
                1 => '/assets/js/promo/2/payments_1.js',
                2 => '/assets/js/promo/2/payments_2.js',
                3 => '/assets/js/promo/2/payments_ge3.js'
            ],
            'css' => '/assets/css/promo/2/style.css',
            'topline' => 'promo/2/topline.twig',
            'body' => 'promo/2/banner.twig',
            'payment' => 'promo/2/payment.twig'
        ],
        3 => [
            'head_script' => [
                1 => '/assets/js/promo/3/payments_1-2.js',
                2 => '/assets/js/promo/3/payments_ge3.js',
            ],
            'css' => '/assets/css/promo/3/style.css',
            'topline' => 'promo/3/topline.twig',
            'body' => 'promo/3/banner.twig',
            'payment' => 'promo/3/payment.twig'
        ],
        4 => [
            'head_script' => [
                1 => '/assets/js/promo/4/payments_1-2.js',
                2 => '/assets/js/promo/4/payments_ge3.js',
            ],
            'css' => '/assets/css/promo/4/style.css',
            'topline' => 'promo/4/topline.twig',
            'body' => 'promo/4/banner.twig',
            'payment' => 'promo/4/payment.twig'
        ]
    ];

    public static function eventManager($eventType, $params)
    {
        log_message('debug', __METHOD__);
        switch($eventType) {
            case 'EVENT_USER_BALANCE_CHANGE': {
                self::checkConditionsAndApply($eventType, $params);
                break;
            }
        }
    }

    private static function checkConditionsAndApply($eventType, $params)
    {
        log_message('debug', __METHOD__);
        log_message('debug', $eventType);
        switch(true) {
            case (self::debug === 1 || date('Ymd') === '20170612')
                && $eventType == 'EVENT_USER_BALANCE_CHANGE' && $params['type'] == 'increment': {
                $bonusSum = 100000 - Redis::get('__promo::1::bonusSum::');
                if ($bonusSum > 1) {
                    $data = Payments::getPaymentsDataByIds([$params['paymentId']], ['payment_id', 'uid', 'payment_system_id', 'sum', 'invoice_id', 'status', 'count']);
                    if (count($data) > 0) {
                        $data = $data[0];
                        $bonus = (int) ($data['count'] * 0.3);
                        if ($bonus > 0 && $bonusSum >= $bonus) {
                            Redis::incrBy('__promo::1::bonusSum::', $bonus);
                            Account::incrementBalance($data['uid'], $bonus, Payment::PAYMENT_EXE_INTERNAL, $bonus);
                            $balance = User::getUserInfo($data['uid'], true)['balance'];
                            UserLog::save($data['uid'], null, UserLog::BONUS_TO_BALANCE, ['count' => $bonus, 'balance' => $balance]);
                            Events::rise('EVENT_USER_BALANCE_CHANGE_BONUS', [
                                'uids' => [$data['uid']],
                                'message' => json_encode([
                                    'type' => 'balance_change_bonus',
                                    'message' => [
                                        'type' => 'increment',
                                        'balance' => $balance
                                    ]
                                ])
                            ]);

                        }
                    }
                }
                break;
            }
            case (self::debug === 2 || date('Ymd') === '20171229')
                && $eventType == 'EVENT_USER_BALANCE_CHANGE' && $params['type'] == 'increment': {
                $bonusSum = 100000 - Redis::get('__promo::2::bonusSum::');
                if ($bonusSum > 1) {
                    $data = Payments::getPaymentsDataByIds([$params['paymentId']], ['payment_id', 'uid', 'payment_system_id', 'sum', 'invoice_id', 'status', 'count']);
                    if (count($data) > 0) {
                        $data = $data[0];
                        $bonus = (int) ($data['count'] * 0.3);
                        if ($bonus > 0 && $bonusSum >= $bonus) {
                            Redis::incrBy('__promo::2::bonusSum::', $bonus);
                            Account::incrementBalance($data['uid'], $bonus, Payment::PAYMENT_EXE_INTERNAL, $bonus);
                            $balance = User::getUserInfo($data['uid'], true)['balance'];
                            UserLog::save($data['uid'], null, UserLog::BONUS_TO_BALANCE, ['count' => $bonus, 'balance' => $balance]);
                            Events::rise('EVENT_USER_BALANCE_CHANGE_BONUS', [
                                'uids' => [$data['uid']],
                                'message' => json_encode([
                                    'type' => 'balance_change_bonus',
                                    'message' => [
                                        'type' => 'increment',
                                        'balance' => $balance
                                    ]
                                ])
                            ]);

                        }
                    }
                }
                break;
            }
            case (self::debug === 3 || in_array(date('Ymd'), ['20180710', '20180711', '20180712']))
                && $eventType == 'EVENT_USER_BALANCE_CHANGE' && $params['type'] == 'increment': {
                $bonusSum = 100000 - Redis::get('__promo::3::bonusSum::');
                if ($bonusSum > 1) {
                    $data = Payments::getPaymentsDataByIds([$params['paymentId']], ['payment_id', 'uid', 'payment_system_id', 'sum', 'invoice_id', 'status', 'count']);
                    if (count($data) > 0) {
                        $data = $data[0];
                        $bonus = (int) ($data['count'] * 0.3);
                        if ($bonus > 0 && $bonusSum >= $bonus) {
                            Redis::incrBy('__promo::3::bonusSum::', $bonus);
                            Account::incrementBalance($data['uid'], $bonus, Payment::PAYMENT_EXE_INTERNAL, $bonus);
                            $balance = User::getUserInfo($data['uid'], true)['balance'];
                            UserLog::save($data['uid'], null, UserLog::BONUS_TO_BALANCE, ['count' => $bonus, 'balance' => $balance]);
                            Events::rise('EVENT_USER_BALANCE_CHANGE_BONUS', [
                                'uids' => [$data['uid']],
                                'message' => json_encode([
                                    'type' => 'balance_change_bonus',
                                    'message' => [
                                        'type' => 'increment',
                                        'balance' => $balance
                                    ]
                                ])
                            ]);

                        }
                    }
                }
                break;
            }
            case (self::debug === 4 || in_array(date('Ymd'), ['20180904', '20180905', '20180906']))
                && $eventType == 'EVENT_USER_BALANCE_CHANGE' && $params['type'] == 'increment': {
                $bonusSum = 100000 - Redis::get('__promo::4::bonusSum::');
                if ($bonusSum > 1) {
                    $data = Payments::getPaymentsDataByIds([$params['paymentId']], ['payment_id', 'uid', 'payment_system_id', 'sum', 'invoice_id', 'status', 'count']);
                    if (count($data) > 0) {
                        $data = $data[0];
                        $bonus = (int) ($data['count'] * 0.3);
                        if ($bonus > 0 && $bonusSum >= $bonus) {
                            Redis::incrBy('__promo::4::bonusSum::', $bonus);
                            Account::incrementBalance($data['uid'], $bonus, Payment::PAYMENT_EXE_INTERNAL, $bonus);
                            $balance = User::getUserInfo($data['uid'], true)['balance'];
                            UserLog::save($data['uid'], null, UserLog::BONUS_TO_BALANCE, ['count' => $bonus, 'balance' => $balance]);
                            Events::rise('EVENT_USER_BALANCE_CHANGE_BONUS', [
                                'uids' => [$data['uid']],
                                'message' => json_encode([
                                    'type' => 'balance_change_bonus',
                                    'message' => [
                                        'type' => 'increment',
                                        'balance' => $balance
                                    ]
                                ])
                            ]);

                        }
                    }
                }
                break;
            }
        }
    }

    public static function userSatisfy($pageType = 'index', $uid = 0)
    {
        switch(true) {
            case self::debug === 1 || date('Ymd') === '20170612': {
                $bonusSum = 100000 - Redis::get('__promo::1::bonusSum::');
                if ($bonusSum > 0) {
                    return 1;
                }
                break;
            }
            case self::debug === 2 || date('Ymd') === '20171229': {
                $bonusSum = 100000 - Redis::get('__promo::2::bonusSum::');
                if ($bonusSum > 0) {
                    return 2;
                }
                break;
            }
            case self::debug === 3 || in_array(date('Ymd'), ['20180710', '20180711', '20180712']): {
                $bonusSum = 100000 - Redis::get('__promo::3::bonusSum::');
                if ($bonusSum > 0) {
                    return 3;
                }
                break;
            }
            case self::debug === 4 || in_array(date('Ymd'), ['20180904', '20180905', '20180906']): {
                $bonusSum = 100000 - Redis::get('__promo::4::bonusSum::');
                if ($bonusSum > 0) {
                    return 4;
                }
                break;
            }
        }

        return false;
    }

    public static function getViewData($pageType = 'index', $promoId = 0)
    {
        $view = self::VIEW;
        if (isset($view[$promoId])) {
            if ($promoId == 1 && $pageType != 'game') {
                unset($view[$promoId]['css'], $view[$promoId]['body']);
            }
            if ($promoId == 2) {
                if ($pageType != 'game') {
                    $view[$promoId]['head_script'] = $view[$promoId]['head_script'][2];
                    unset($view[$promoId]['css'], $view[$promoId]['body']);
                } else {
                    $marker = Auth::getUserMarker();
                    $sessionNumber = User::getUserSessionNumberByMarker($marker);
                    if ($sessionNumber >= 3) {
                        $view[$promoId]['head_script'] = $view[$promoId]['head_script'][3];
                    } else {
                        $view[$promoId]['head_script'] = $view[$promoId]['head_script'][1];
                    }
                }
            }
            if ($promoId == 3) {
                $marker = Auth::getUserMarker();
                $sessionNumber = User::getUserSessionNumberByMarker($marker);
                if ($sessionNumber >= 3) {
                    $view[$promoId]['head_script'] = $view[$promoId]['head_script'][2];
                } else {
                    $view[$promoId]['head_script'] = $view[$promoId]['head_script'][1];
                }
            }
            if ($promoId == 4) {
                $marker = Auth::getUserMarker();
                $sessionNumber = User::getUserSessionNumberByMarker($marker);
                if ($sessionNumber >= 3) {
                    $view[$promoId]['head_script'] = $view[$promoId]['head_script'][2];
                } else {
                    $view[$promoId]['head_script'] = $view[$promoId]['head_script'][1];
                }
            }
            return $view[$promoId];
        }

        return false;
    }
}