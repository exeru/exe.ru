<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28.06.17
 * Time: 17:47
 */

namespace Base\Service;

use \Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends Account
{
    public static function getTransactionsAtPeriod($cid, $period)
    {
        return Model\Contract::getTransactionsAtPeriod($cid, $period);
    }

    public static function withdraw($cid, $gid, $uid, $count)
    {
        $period = date('Ym');

        Model\Contract::addWithdraw($cid, $gid, $uid, $count, $period);

        return self::decrementGameBalance($uid, $gid, $count);
    }

    public static function getWithdrawAtPeriodByGids($gids, $period)
    {
        if (count($gids) < 1) {
            return [];
        }

        return Model\Contract::getWithdrawAtPeriodByGids($gids, $period);
    }

    public static function getContracts()
    {
        return Model\Contract::getContracts();
    }

    public static function getContractById($cid)
    {
        return Model\Contract::getContractById($cid);
    }

    public static function add($data)
    {
        return Model\Contract::add($data);
    }

    public static function setContractDataById($cid, $data)
    {
        return Model\Contract::setContractDataById($cid, $data);
    }
}