<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.04.16
 * Time: 17:25
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends Stat
{
    const STATS_NOTIFICATION_UPDATE = '__stats::notification::updated';

    /**
     * Обновляет агрегированную информацию по месяцам, данные об обновленных значениях получает из редиса
     */
    public static function updateCollectedStat()
    {

    }

    /**
     * Просчитывает информацию за текущий месяц, заносит её в таблицу агрегированной информации по месяцам
     */
    public static function collectStat()
    {
        if (!self::lock(__METHOD__)) {
            echo "Locked \n";
            return false;
        }

        $time = time();
        $monthNumber = self::getMonth($time);
        $timeMonthStart = strtotime(date('Y/m/01'), $time);
        $timeMonthEnd = strtotime(date('Y/m/t'), $time) + 86399;

        $updated = 0;
        $forInsert = [];

        $currentData = Model\Notifications::getDataForCollect(self::getDay($timeMonthStart), self::getDay($timeMonthEnd));
        $agregatedData = Model\Notifications::getAgregatedDataForCollect($monthNumber);
        if (count($currentData) > 0) {
            if (count($agregatedData) > 0) {
                // Надо смотреть, что обновлять, а что добавлять
                $preparedData = [];
                foreach($agregatedData as $row) {
                    $preparedData[$row['gid']][$row['type']][$row['ntype']] = $row;
                }
                foreach($currentData as $row) {
                    if (isset($preparedData[$row['gid']][$row['type']][$row['ntype']])) {
                        // Обновление, если отличаются
                        if ($preparedData[$row['gid']][$row['type']][$row['ntype']]['all'] != $row['all']
                            || $preparedData[$row['gid']][$row['type']][$row['ntype']]['filter'] != $row['filter']
                            || $preparedData[$row['gid']][$row['type']][$row['ntype']]['success'] != $row['success']
                            || $preparedData[$row['gid']][$row['type']][$row['ntype']]['fail'] != $row['fail']
                        ) {
                            Model\Notifications::updateDataByMonths([
                                'all' => $row['all'],
                                'filter' => $row['filter'],
                                'success' => $row['success'],
                                'fail' => $row['fail']
                            ], [
                                'gid' => $row['gid'],
                                'month_number' => $monthNumber,
                                'type' => $row['type'],
                                'ntype' => $row['ntype']
                            ]);
                            $updated++;
                        }
                    } else {
                        // Добавление данных, будем делать одним запросом, поэтому просто их копим
                        $row['month_number'] = $monthNumber;
                        $forInsert[] = $row;
                    }
                }
                if ($updated > 0) {
                    echo "Updated: " . $updated . "\n";
                }
            } else {
                // Просто добавляем данные
                foreach($currentData as &$value) {
                    $value['month_number'] = $monthNumber;
                };
                $forInsert = $currentData;
            }

            if (count($forInsert) > 0) {
                Model\Notifications::insertDataByMonths($forInsert);
                echo "Inserted: " . count($forInsert) . "\n";
            }
        }

        if (count($forInsert) == 0 && $updated == 0) {
            echo "Parsed: 0 \n";
        }

        self::unlock(__METHOD__);
        return true;
    }

    public static function getStats($gid)
    {
        $data = [];
        $data['all']['days'] = self::getByDays($gid, true);
        $data['all']['months'] = self::getAllByMonths($gid, true);

        $data['groups']['days'] = self::getByDays($gid);
        $data['groups']['months'] = self::getAllByMonths($gid);

        $data['types'] = self::getTypesByDays($gid);

        $data['feedback']['today'] = self::getByType($gid);

        return $data;
    }

    private static function getTypesByDays($gid)
    {
        $today = self::getDay();
        $monthAgo = $today - 29;

        $days = array_fill_keys(range($monthAgo, $today, 1), 0);
        $types = array_fill_keys(range(1, 15, 1), $days);

        $data = Model\Notifications::getFromIntervalByDaysAndTypes($gid, $monthAgo, $today);

        if (count($data) > 0) {
            foreach($data as $row) {
                if ($row['success'] > 0) {
                    $types[$row['ntype']][$row['day_number']] = floor($row['success'] * 100 / $row['filter']);
                }
            }
        }

        return [
            'success' => $types,
            'start' => $monthAgo * 86400,
            'end' => $today * 86400
        ];
    }

    private static function getByType($gid)
    {
        return Model\Notifications::getByType($gid, self::getDay());
    }

    private static function getByDays($gid, $allGroup = false)
    {
        $today = self::getDay();
        $monthAgo = $today - 29;
        $all = $filter = $success = $fail = array_fill_keys(range($monthAgo, $today, 1), 0);

        $data = Model\Notifications::getAllFromInterval($gid, $monthAgo, $today, $allGroup);
        if (count($data) > 0) {
            foreach($data as $row) {
                $all[$row['day_number']] = (int) $row['all'];
                $filter[$row['day_number']] = (int) $row['filter'];
                $success[$row['day_number']] = (int) $row['success'];
                $fail[$row['day_number']] = (int) $row['fail'];
            }
        }

        return [
            'all' => $all,
            'filter' => $filter,
            'success' => $success,
            'fail' => $fail,
            'start' => $monthAgo * 86400,
            'end' => $today * 86400
        ];
    }

    private static function getAllByMonths($gid, $allGroup = false)
    {
        $current = self::getMonth();
        $yearAgo = $current - 11;
        $all = $filter = $success = $fail = array_fill_keys(range($yearAgo, $current, 1), 0);

        $data = Model\Notifications::getFromIntervalByMonths($gid, $yearAgo, $current, $allGroup);

        if (count($data) > 0) {
            foreach($data as $row) {
                $all[$row['month_number']] = (int) $row['all'];
                $filter[$row['month_number']] = (int) $row['filter'];
                $success[$row['month_number']] = (int) $row['success'];
                $fail[$row['month_number']] = (int) $row['fail'];
            }
        }

        $startYear = (int) ($yearAgo / 12);
        $startMonth = $yearAgo - ($startYear * 12);

        return [
            'all' => $all,
            'filter' => $filter,
            'success' => $success,
            'fail' => $fail,
            'start' => strtotime($startYear . '-' . $startMonth , '-01') + 10800,
            'end' => time()
        ];
    }

    public static function updateStat($gid, $time, $type, $ntype, $incrementField)
    {
        $data = [
            'gid' => $gid,
            'day_number' => self::getDay($time),
            'type' => $type,
            'ntype' => $ntype
        ];

        $hashString = implode("::", $data);

        Redis::hSet(self::STATS_NOTIFICATION_UPDATE, md5($hashString), serialize($data));

        return Model\Notifications::updateStat($gid, $data['day_number'], $type, $ntype, $incrementField);
    }

    public static function deleteGameNotification($nid)
    {
        return Model\Notifications::deleteGameNotification($nid);
    }

    public static function getGameNotifications($uid)
    {
        return Model\Notifications::getGameNotifications($uid);
    }

    public static function getGameNotificationByUIdAndNId($uid, $gid, $nid)
    {
        return Model\Notifications::getGameNotificationByUIdAndNId($uid, $gid, $nid);
    }
}