<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.01.16
 * Time: 13:48
 */

namespace Base\Service;

use Base\Model;

use Base\Provider\Payment;

defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends Account
{
    const PAYMENT_STATUS_INIT = 0; // Оплата инициирована
    const PAYMENT_STATUS_CHECKED = 1; // Платеж проверен
    const PAYMENT_STATUS_ACCEPTED = 2; // Зачисление подтверждено платежной системой
    const PAYMENT_STATUS_MANUAL_ACCEPTED = 3; // Зачисление подтверждено модератором

    const startPayment = 'init';
    const checkPayment = 'check';
    const acceptPayment = 'accept';
    const manualAcceptPayment = 'manual';

    // Обязательно сохранять ключи, не удалять старые (будут проблемы с историей платежей)!
    private static $types = [
        1 => Payment::PAYMENT_YANDEX_AC, // Банковские карты
        2 => Payment::PAYMENT_YANDEX_MC, // Мобильные телефоны
        3 => Payment::PAYMENT_YANDEX_QW, // Платежные системы, QIWI через Яндекс.Деньги
        4 => Payment::PAYMENT_YANDEX_PC, // Платежные системы, Яндекс.Деньги
        5 => Payment::PAYMENT_YANDEX_WM, // Платежные системы, WebMoney
        6 => Payment::PAYMENT_YANDEX_GP, // Терминалы оплаты, салоны связи
        7 => Payment::PAYMENT_PAYMASTER_WEBMONEY, // Paymaster, платежные системы, WebMoney
        8 => Payment::PAYMENT_QIWI_QW, // Платежные системы, QIWI
        9 => Payment::PAYMENT_PAYMASTER_SBERBANK, // Paymaster, интернет-банкинг, Сбербанк
        10 => Payment::PAYMENT_PAYMASTER_ALFABANK, // Paymaster, интернет-банкинг, Альфа-банк
        11 => Payment::PAYMENT_PAYMASTER_VTB24, // Paymaster, интернет-банкинг, ВТБ24
        12 => Payment::PAYMENT_PAYMASTER_BRS, // Paymaster, интернет-банкинг, Русский Стандарт
        13 => Payment::PAYMENT_PAYMASTER_BPSB, // Paymaster, интернет-банкинг, Промсвязьбанк,
        14 => Payment::PAYMENT_YANDEX_SBERBANK, // Яндекс.Деньги, интернет-банкинг, Сбербанк
        15 => Payment::PAYMENT_YANDEX_ALFABANK, // Яндекс.Деньги, интернет-банкинг, Альфа-банк
        16 => Payment::PAYMENT_YANDEX_BPSB, // Яндекс.Деньги, интернет-банкинг, Промсвязьбанк,
        17 => Payment::PAYMENT_EXE_INTERNAL, // Внутреннее начисление
        18 => Payment::PAYMENT_PAYMASTER_MTS, // Paymaster, MTS
        19 => Payment::PAYMENT_PAYMASTER_BEELINE, // Paymaster, Beeline
        20 => Payment::PAYMENT_PAYMASTER_TELE2, // Paymaster, Tele2
        21 => Payment::PAYMENT_PAYMASTER_MEGAFON, // Paymaster, Megafon
        22 => Payment::PAYMENT_PAYPAL, // PayPal
    ];

    public static $humansText = [
        1 => 'Карта через Яндекс',
        2 => 'Мобильный через Яндекс',
        3 => 'QIWI через Яндекс',
        4 => 'Яндекс.Деньги',
        5 => 'Webmoney через Яндекс',
        6 => 'Терминалы через Яндекс',
        7 => 'Webmoney через Paymaster',
        8 => 'QIWI',
        9 => 'Сбербанк через Paymaster',
        10 => 'Альфабанк через Paymaster',
        11 => 'ВТБ24 через Paymaster',
        12 => 'Русский Стандарт через Paymaster',
        13 => 'Промсвязьбанк через Paymaster',
        14 => 'Сбербанк через Яндекс',
        15 => 'Альфа-банк через Яндекс',
        16 => 'Промсвязьбанк через Яндекс',
        17 => 'Внутренние начисления',
        18 => 'МТС через Paymaster',
        19 => 'Beeline через Paymaster',
        20 => 'Tele2 через Paymaster',
        21 => 'Megafon через Paymaster',
        22 => 'Paypal'
    ];

    // Ключи должны совпадать с ключами в $types
    private static $groups = [
        1 => 'card',
        2 => 'phone',
        3 => 'system',
        4 => 'system',
        5 => 'system',
        6 => 'terminal',
        7 => 'system',
        8 => 'system',
        9 => 'banking',
        10 => 'banking',
        11 => 'banking',
        12 => 'banking',
        13 => 'banking',
        14 => 'banking',
        15 => 'banking',
        16 => 'banking',
        17 => 'promo',
        18 => 'phone',
        19 => 'phone',
        20 => 'phone',
        21 => 'phone',
        22 => 'system'
    ];

    private static $groupTypes = [
        'card' => [
            'title' => 'Банковская карта',
            'sourceImage' => '/assets/img/browser.png',
            'sourceText' => 'Пополнение с банковской карты'
        ],
        'phone' => [
            'title' => 'Мобильный телефон',
            'sourceImage' => '/assets/img/phone.png',
            'sourceText' => 'Пополнение с баланса мобильного телефона'
        ],
        'system' => [
            'title' => 'Платежная система',
            'sourceImage' => '/assets/img/wallet.png',
            'sourceText' => 'Пополнение через платежные системы'
        ],
        'terminal' => [
            'title' => 'Терминал оплаты',
            'sourceImage' => '/assets/img/banknote.png',
            'sourceText' => 'Пополнение через терминалы оплаты'
        ],
        'banking' => [
            'title' => 'Интернет-банкинг',
            'sourceImage' => '/assets/img/banknote.png',
            'sourceText' => 'Пополнение через интернет-банкинг'
        ],
        'promo' => [
            'title' => 'Внутренние начисления',
            'sourceImage' => '/assets/img/browser.png',
            'sourceText' => 'Бонус'
        ]
    ];

    // Роутер, обеспечивает связь между внешним значеним способа оплаты и установленным на данный момент провайдером
    private static $router = [
        'AC' => 1,
        'MC' => 2,
        'QW' => 8,
        'PC' => 4,
        'WM' => 7, // Переключение webmoney на paymaster
        'GP' => 6,
        'SBERBANK' => 14, // Сбербанк переключаем на Яндекс
        'ALFABANK' => 15, // Альфабанк на Яндекс
        // 'VTB24' => 11, // Отключаем
        // 'BRS' => 12, // Отключаем
        'PSB' => 16, // Промсвязьбанк на Яндекс
        'MTS' => 18,
        'BEELINE' => 19,
        'TELE2' => 20,
        'MEGAFON' => 21,
        'PP' => 22 // PayPal
    ];

    public static function getAmountByDays($start, $end)
    {
        $data = Model\Payments::getAmountByDays($start, $end);

        $result = array_fill_keys(range($start, $end, 86400), 0);

        $forReturn = [];
        if (count($data) > 0) {
            foreach($data as $row) {
                $index = strtotime(date('d-m-Y', $row['datetime'])) + 86400;
                if (!isset($result[$index])) {
                    $result[$index] = 0;
                }
                $result[$index] += $row['count'];
            }
            foreach($result as $time => $count)
            {
                $forReturn[] = '[' . $time * 1000 . ',' . $count . ']';
            }
        }

        return '[' . implode(',', $forReturn) . ']';
    }

    public static function getPaymentsDataByIds(array $ids, array $fields)
    {
        return Model\Payments::getPaymentsDataByIds($ids, $fields);
    }

    public static function getRequestsLog($start, $end, $byid, $filter, $limit, $offset)
    {
        return Model\Payments::getRequestsLog($start, $end, $byid, $filter, $limit, $offset);
    }

    public static function getHistoryData($receiver)
    {
        $data = [];

        $group = self::$groups[$receiver];
        $data['sourceImage'] = self::$groupTypes[$group]['sourceImage'];
        $data['sourceText'] = self::$groupTypes[$group]['sourceText'];

        return $data;
    }

    public static function getRates()
    {
        $rates = self::$router;

        foreach($rates as $paymentExternalCode => $paymentInternalCode) {
            $rates[$paymentExternalCode] = ['rate' => Payment::getRate(self::$types[$paymentInternalCode], self::ONE_BONUS_COST)];
        }

        return $rates;
    }

    public static function getTypes()
    {
        return array_keys(self::$router);
    }

    public static function prepare($uid, $count, $paymentType, $phone = '', $purchaseId)
    {
        $count = (int) $count;

        $paymentTypeId = self::$types[self::$router[$paymentType]];

        $rate = Payment::getRate($paymentTypeId, self::ONE_BONUS_COST);
        $cost = str_replace('%count%', $count, $rate);
        $cost = eval("return $cost;");

        if (in_array($paymentType, ['MC', 'MTS', 'BEELINE', 'TELE2', 'MEGAFON'])) {
            $cost = ceil($cost);
        }

        $orderNumber = self::getNewOrderNumber();

        $sum = number_format($cost, 2, '.', '');

        $params = Payment::prepareFormData($orderNumber, $uid, $sum, $paymentTypeId, $phone, $count);

        // Создаем платеж
        Model\Payments::createPayment($orderNumber, $uid, $paymentTypeId, $count, $rate, $sum);

        // Логируем запрос и ответ
        $requestData = [
            'uid' => $uid,
            'count' => $count,
            'paymentType' => $paymentType,
            'phone' => $phone
        ];

        Model\Payments::saveRequest($uid, self::startPayment, $requestData, $orderNumber, self::PAYMENT_STATUS_INIT, $params);

        if ($purchaseId > 0) {
            self::remember($uid, $orderNumber, $purchaseId);
        }

        UserLog::save($uid, null, UserLog::GO_TO_PAYMENT_SYSTEM, ['system' => $paymentTypeId, 'sum' => $cost], Auth::getUserMarker());

        return $params;
    }

    public static function getPurchaseIdByOrderId($uid, $orderId)
    {
        $key = \Base\Service\RedisKeys::orderToPurchase . $uid;
        return Redis::hGet($key, $orderId);
    }

    public static function forget($uid, $purchaseId)
    {
        $key = \Base\Service\RedisKeys::purchaseToOrder . $uid;
        $orderId = Redis::hGet($key, $purchaseId);
        Redis::hDel($key, $purchaseId);

        $key = \Base\Service\RedisKeys::orderToPurchase . $uid;
        Redis::hDel($key, $orderId);
    }

    public static function remember($uid, $orderNumber, $purchaseId)
    {
        $key = \Base\Service\RedisKeys::orderToPurchase . $uid;
        Redis::hSet($key, $orderNumber, $purchaseId);

        $key = \Base\Service\RedisKeys::purchaseToOrder . $uid;
        Redis::hSet($key, $purchaseId, $orderNumber);
    }

    private static function acceptPayment($paymentId)
    {
        $data = Model\Payments::getPayment($paymentId);

        if ($data['status'] ==  self::PAYMENT_STATUS_ACCEPTED) {
            // Добавляем на баланс пользователя
            self::incrementBalance($data['uid'], $data['sum'], $data['payment_system_id'], $data['count']);

            $balance = User::getUserInfo($data['uid'], true)['balance'];

            $mainMarker = Redis::hGet(RedisKeys::userMainMarkerByUid, $data['uid']);
            $marker = User::getMarkerByMainMarker($mainMarker);
            UserLog::save($data['uid'], null, UserLog::TO_BALANCE, ['count' => $data['count'], 'balance' => $balance], $marker);

            Events::rise('EVENT_USER_BALANCE_CHANGE', [
                'uids' => [$data['uid']],
                'balance' => $balance,
                'paymentId' => $paymentId,
                'type' => 'increment',
                'message' => json_encode([
                    'type' => 'balance_change',
                    'message' => [
                        'type' => 'increment',
                        'balance' => $balance,
                        'orderNumber' => $paymentId
                    ]
                ])
            ]);

            $key = \Base\Service\RedisKeys::orderToPurchase . $data['uid'];

            $purchaseId = (int) Redis::hGet($key, $paymentId);

            if ($purchaseId > 0) {
                Purchases::restore($data['uid'], $purchaseId);
            }
        }
    }

    public static function checkAndAcceptPayment($paramsData)
    {
        $paymentData = Model\Payments::getPayment($paramsData['orderId']);

        if (is_null($paramsData['uid'])) { /// QIWI не передает uid пользователя
            $paramsData['uid'] = $paymentData['uid'];
        }

        if (is_null($paymentData) || $paymentData['uid'] != $paramsData['uid'] || $paymentData['sum'] != $paramsData['sum']
            || (!is_null($paymentData['invoice_id']) && $paymentData['invoice_id'] != $paramsData['invoiceId'])) {
            return false;
        }

        if ($paymentData['status'] == self::PAYMENT_STATUS_CHECKED) {
            Model\Payments::updatePayment($paramsData['orderId'], $paramsData['invoiceId'], self::PAYMENT_STATUS_ACCEPTED);

            self::acceptPayment($paramsData['orderId']);

            return self::PAYMENT_STATUS_ACCEPTED;
        }

        return true;
    }

    public static function checkAndUpdatePayment($paramsData)
    {
        $paymentData = Model\Payments::getPayment($paramsData['orderId']);

        if (is_null($paramsData['uid'])) { /// QIWI не передает uid пользователя
            $paramsData['uid'] = $paymentData['uid'];
        }

        if (is_null($paymentData) || $paymentData['uid'] != $paramsData['uid'] || $paymentData['sum'] != $paramsData['sum']) {
            return false;
        }

        if (is_null($paymentData['invoice_id']) && $paymentData['status'] == self::PAYMENT_STATUS_INIT) {
            Model\Payments::updatePayment($paramsData['orderId'], $paramsData['invoiceId'], self::PAYMENT_STATUS_CHECKED);
            return self::PAYMENT_STATUS_CHECKED;
        }

        return true;
    }

    public static function saveRequest($uid, $textStatus, $request, $paymentId, $status, $response)
    {
        Model\Payments::saveRequest($uid, $textStatus, $request, $paymentId, $status, $response);
    }

    public static function manualAcceptPayment($uid, $orderId)
    {
        $paymentData = Model\Payments::getPayment($orderId);

        self::saveRequest($paymentData['uid'], self::manualAcceptPayment, $uid, $orderId, Payments::PAYMENT_STATUS_MANUAL_ACCEPTED, '');

        Model\Payments::updatePayment($orderId, NULL, self::PAYMENT_STATUS_MANUAL_ACCEPTED);

        // Добавляем на баланс пользователя
        self::incrementBalance($paymentData['uid'], $paymentData['sum'], $paymentData['payment_system_id'], $paymentData['count']);

        $balance = User::getUserInfo($paymentData['uid'], true)['balance'];

        UserLog::save($paymentData['uid'], null, UserLog::TO_BALANCE, [
            'count' => $paymentData['sum'],
            'balance' => $balance
        ]);

        Events::rise('EVENT_USER_BALANCE_CHANGE', [
            'uids' => [$paymentData['uid']],
            'balance' => $balance,
            'paymentId' => $orderId,
            'type' => 'increment',
            'message' => json_encode([
                'type' => 'balance_change',
                'message' => [
                    'type' => 'increment',
                    'balance' => $balance,
                    'orderNumber' => $orderId
                ]
            ])
        ]);

        UserLog::save($paymentData['uid'], null, UserLog::MODERATOR_TO_BALANCE, [
            'muid' => $uid,
            'payment_id'=> $orderId,
            'sum' => $paymentData['count'],
            'balance' => User::getUserInfo($paymentData['uid'], true)['balance']
        ]);

        return true;
    }
}