<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.10.17
 * Time: 15:45
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class Viber
{
    const
        userRuslan = 'i0uoc03m6XRf7WyWcDdC1g==',
        userKonstantin = 'fJ6rrNsl95ZKtHywu+5zXQ==',
        admins = [self::userRuslan, self::userKonstantin];

    const config = [
        'sendMessageUrl' => 'https://chatapi.viber.com/pa/send_message',
        'apiToken' => '46d4b896ec27d5a4-970525f24e63f1fb-bafd3164d72adf41'
    ];

    public static function send($receivers = self::admins, $message = '')
    {
        if (empty($message)) {
            return false;
        }

        if (!is_array($receivers)) {
            $receivers = [$receivers];
        }

        foreach($receivers as $receiver) {
            $data = [
                'receiver' => $receiver,
                'sender' => [
                    'name' => 'PHP EXE.RU',
                ],
                'type' => 'text',
                'text' => $message
            ];

            $ch = curl_init();

            $options = [
                CURLOPT_URL => self::config['sendMessageUrl'],
                CURLOPT_HTTPHEADER => ['X-Viber-Auth-Token: ' . self::config['apiToken']],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => false,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false
            ];

            curl_setopt_array($ch, $options);

            curl_exec($ch);
            curl_close($ch);
        }

        return true;
    }
}