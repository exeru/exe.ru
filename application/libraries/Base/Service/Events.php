<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27.11.15
 * Time: 13:25
 */

namespace Base\Service;


class Events
{
    private static $_events = null;

    public static function rise($eventType, $params)
    {
        if (is_null(self::$_events)) {
            self::init();
        }

        if (isset(self::$_events[$eventType]) && count(self::$_events[$eventType]) > 0) {
            foreach(self::$_events[$eventType] as $listener) {
                if (is_callable($listener)) {
                    call_user_func_array($listener, [$eventType, $params]);
                }
            }
        }
    }

    private static function init()
    {
        $ci = & get_instance();
        $ci->config->load('events');

        self::$_events = $ci->config->item('events');
    }
}