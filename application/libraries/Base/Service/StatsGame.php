<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.05.16
 * Time: 13:42
 */

namespace Base\Service;

use \Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class StatsGame extends Stat
{

    const STATS_INSTALL_GROUP = [
        // Внутренние
        'catalog' => ['id' => 1, 'title' => 'Каталог игр'],
        'news' => ['id' => 2, 'title' => 'Новости'],
        'friends_news' => ['id' => 3, 'title' => 'Новости друзей'],
        'invite' => ['id' => 4, 'title' => 'Приглашения'],
        'request' => ['id' => 5, 'title' => 'Запросы друзей'],
        'profile' => ['id' => 6, 'title' => 'Профили'],
        'game_request' => ['id' => 7, 'title' => 'Заявки по игре'],
        // Внешние
        'direct' => ['id' => 8, 'title' => 'Прямые ссылки'],
        'special' => ['id' => 9, 'title' => 'Спец. размещение']
    ];

    public static function eventManager($eventType, $params)
    {
        switch($eventType) {
            case 'EVENT_USER_INSTALL_GAME': {
                self::insertInstallDelete($params['gid'], $params['uid'], true);
                self::pushInstall($params['gid'], $params['source'], $params['referrer']);
                break;
            }
        }
    }


    public static function getSources($gid)
    {
        $today = self::getDay();

        $data = [
            'day' => [$today, $today],
            'week' => [$today - 6, $today],
            'month' => [$today - 29, $today],
            'all' => [null, null]
        ];

        foreach($data as $period=>$dates) {
            $periodData = Model\StatsGame::getSources($gid, $dates[0], $dates[1]);
            $data[$period] = $periodData;
        }

        return $data;
    }

    private static function pushInstall($gid, $source, $referrer)
    {
        $STATS_INSTALL_GROUP = self::STATS_INSTALL_GROUP;
        $host = (ENVIRONMENT == 'development' ? 'exe.ru.dev' : (ENVIRONMENT == 'testing' ? 'test.exe.ru' : 'exe.ru'));

        if (empty($referrer) || is_null($source)
            || ($host != parse_url($referrer, PHP_URL_HOST) && $source != 'special') // Внешний переход, но не спец. размещение
            //|| ($host == parse_url($referrer, PHP_URL_HOST) && $source == 'special') // Локальный переход, но спец. размещение
            || !isset($STATS_INSTALL_GROUP[$source]) ) { // У нас нет такой группы
            $group = 'direct';
        } else {
            $group = $source;
        }

        Model\StatsGame::insertInstall($gid, self::getDay(), $STATS_INSTALL_GROUP[$group]['id']);

        return true;
    }


    public static function getCollectedInstalled($gid)
    {
        return Model\StatsGame::getCollectedInstalled($gid);
    }

    public static function collectStatInstalled()
    {
        if (!self::lock(__METHOD__)) {
            echo "Locked \n";
            return false;
        }

        ob_implicit_flush();

        $forInsert = [];
        $dayNumber = self::getDay() - 1;
        $summaryTime = microtime(true);

        $gids = Game::getAllGamesId();
        if (count($gids) > 0) {
            echo "Count of game: " . count($gids) . " \n";
            foreach($gids as $gid) {
                $time = microtime(true);
                $count = Game::getCountPlayers($gid);
                $forInsert[] = [
                    'gid' => $gid,
                    "day_number" => $dayNumber,
                    "count" => $count
                ];

                echo sprintf("Gid %d, count %d\n", $gid, $count);

                $executionTime = microtime(true) - $time;
                if ($executionTime > 1) {
                    echo sprintf("Long request (%01.5f), gid %d, count %d, sleep 1s\n", $executionTime, $gid, $count);
                    sleep(1);
                }
            }
        }

        Model\StatsGame::insertInstalled($forInsert);

        echo sprintf("End, summary executive time %01.5f\n", microtime(true) - $summaryTime);

        self::unlock(__METHOD__);
        return true;
    }

    public static function getDay($time = null)
    {
        return parent::getDay($time);
    }

    public static function getCollectedInstallDelete($gid, $dateStart = null, $dateEnd = null)
    {
        if (is_null($dateEnd)) {
            $dateEnd = self::getDay();
            $dateStart = $dateEnd - 29;
        }

        return Model\StatsGame::getCollectedInstallDelete($gid, $dateStart, $dateEnd);
    }

    public static function collectStatInstallDelete($today = null)
    {
        if (!self::lock(__METHOD__)) {
            echo "Locked \n";
            return false;
        }

        if (is_null($today)) {
            $today = self::getDay();
        }

        $forInsert = [];
        $updated = 0;

        $data = Model\StatsGame::getInstallDeleteForCollect($today);

        if (count($data) > 0) {
            $stat = [];
            foreach($data as $row) {
                if (!isset($stat[$row['gid']])) {
                    $stat[$row['gid']] = [
                        'gid' => $row['gid'],
                        'day_number' => $today,
                        'installed' => 0,
                        'deleted' => 0
                    ];
                }
                if ($row['install'] == 't') {
                    $stat[$row['gid']]['installed'] = (int) $row['count'];
                } else {
                    $stat[$row['gid']]['deleted'] = (int) $row['count'];
                }
            }

            $collected = Model\StatsGame::getCollectedInstallDelete(array_keys($stat), $today, $today);

            if (count($collected) > 0) {
                $installed = array_column($collected, 'installed', 'gid');
                $deleted = array_column($collected, 'deleted', 'gid');

                foreach($stat as $row) {
                    if (isset($installed[$row['gid']])) {
                        if ($installed[$row['gid']] != $row['installed'] || $deleted[$row['gid']] != $row['deleted']) {
                            Model\StatsGame::updateCollectedInstallDelete([
                                'installed' => (int) $stat[$row['gid']]['installed'],
                                'deleted' => (int) $stat[$row['gid']]['deleted']
                            ], [
                                'gid' => $row['gid'],
                                'day_number' => $row['day_number']
                            ]);

                            $updated++;
                        }
                    } else {
                        $forInsert[$row['gid']] = $stat[$row['gid']];
                    }
                }
                if ($updated > 0) {
                    echo "Updated: " . $updated . "\n";
                }
            } else {
                $forInsert = $stat;
            }

            if (count($forInsert) > 0) {
                Model\StatsGame::insertCollectedInstallDelete($forInsert);
                echo "Inserted: " . count($forInsert) . "\n";
            }
        }

        if (count($forInsert) == 0 && $updated == 0) {
            echo "Parsed: 0 \n";
        }

        self::unlock(__METHOD__);
        return true;
    }

    public static function insertInstallDelete($gid, $uid, $installed)
    {
        return Model\StatsGame::insertInstallDelete($gid, self::getDay(), $uid, $installed);
    }
}