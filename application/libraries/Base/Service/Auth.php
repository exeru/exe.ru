<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.08.15
 * Time: 18:18
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth
{
    public static function isExceededTemporary()
    {
        Log::profile(__METHOD__);
        $max100per10Prefix = RedisKeys::max100per10min;

        if (count(Redis::getKeys($max100per10Prefix . '*', Redis::__DB_EXCEEDED_TEMPORARY)) >= 4 * 100) {
            return true;
        }

        $ci = & get_instance();

        $ip = $ci->input->ip_address();

        $max5per10minByIpPrefix = RedisKeys::max5per10minByIp;

        if (count(Redis::getKeys($max5per10minByIpPrefix. $ip . '::*', Redis::__DB_EXCEEDED_TEMPORARY)) >= 4 * 5) {
            return true;
        }

        Redis::set($max100per10Prefix . time(), 1, 600, Redis::__DB_EXCEEDED_TEMPORARY);
        Redis::set($max5per10minByIpPrefix. $ip . '::' . time(), 1, 600, Redis::__DB_EXCEEDED_TEMPORARY);

        Log::profile(__METHOD__);
        return false;
    }

    public static function getUserId()
    {
        $ci = & get_instance();

        $uid = (int) $ci->input->cookie('sudo');

        if ($uid > 0) {
            self::loginAsUser($uid);
            return $uid;
        }

        if (!is_null($ci->input->cookie('ci_session')) && $ci->input->cookie('ci_session') != 'deleted' && !isset($ci->session)) {
            $ci->load->library('session', 'core');
        }

        if (isset($ci->session) ) {
            $uid = $ci->session->userdata('user_id');
        }

        if ($uid > 0) {
            return $uid;
        }

        $data = $ci->input->cookie('auth');
        if (is_null($data) || $data == 'deleted' || !preg_match('|^[0-9a-zA-Z\+\/\=]+$|', $data)) {
            return false;
        }

        $data = base64_decode($data);
        if ($data === false ) {
            return false;
        }

        $data = unserialize($data);
        if (!is_array($data) || !isset($data['user']) || !isset($data['token']) || !preg_match('|^[0-9a-f]{32}$|', $data['token'])) {
            return false;
        }

        if ($data['user'] == false) {
            return false;
        }

        $ci = & get_instance();

        $log = [];
        foreach($ci->input->request_headers() as $name => $value) {
            $log[] = $name . ': ' . $value;
        }
        $log[] = 'Time: ' . date('d/m/Y H:i:s.u');
        $log[] = 'Method: ' . $ci->input->method(true);
        $log[] = 'Ip: ' . $ci->input->ip_address();
        $log[] = 'Agent: ' . $ci->input->user_agent();
        $log[] = 'Data user: ' . $data['user'];
        $log[] = 'Data token: ' . $data['token'];

        if (self::isTokenExists($data['user'], $data['token'], false)) {
            self::loginAsUser($data['user']);

            $log[] = 'Auth: true';
            \Base\Service\Log::push('auth_by_cookie', $log);

            return $data['user'];
        }

        $ci->input->set_cookie('auth', '');

        $log[] = 'Auth: false';
        \Base\Service\Log::push('auth_by_cookie', $log);

        return false;
    }

    public static function isAuth()
    {
        return self::getUserId() > 0;
    }

    public static function logout()
    {
        $ci = get_instance();

        $data = (string) $ci->input->cookie('auth');
        if ($data !== '') {
            if (preg_match('|^[0-9a-zA-Z\+\/\=]+$|', $data)) {
                $data = base64_decode($data);
                if ($data !== false ) {
                    $data = unserialize($data);
                    if (is_array($data) && isset($data['user']) && isset($data['token']) && preg_match('|^[0-9a-f]{32}$|', $data['token'])) {
                        Model\Auth::deleteToken((string) $data['user'], $data['token']);
                    }
                }
            }
            $ci->input->set_cookie('auth', '');
        }

        self::deleteUserMarker();

        if (!is_null($ci->input->cookie('ci_session'))) {
            if (!isset($ci->session)) {
                $ci->load->library('session', 'core');
            }
            $ci->session->set_userdata(['user_id' => 0]);

            session_destroy();
        }
    }

    public static function remember()
    {
        $uid = self::getUserId();

        // Генерируем токен для входа
        $token =  Model\Auth::createToken($uid);

        $data = serialize([
            'user' => $uid,
            'token' => $token
        ]);

        get_instance()->input->set_cookie('auth', base64_encode($data), 31536000);

        return true;
    }

    public static function login($email, $password)
    {
        if (false === $uid = User::getUserIdByAuthData($email, $password)) {
            return false;
        }

        return self::loginAsUser($uid);
    }

    public static function loginAsUser($uid)
    {
        $ci = get_instance();

        if (!isset($ci->session)) {
            $ci->load->library('session', 'core');
        }

        $ci->session->user_id = $uid;

        return true;
    }

    public static function createTokenByEmail($email)
    {
        // Проверяем существование пользователя
        if (false === User::isUserExistsByEmail($email))
        {
            return false;
        }

        // Генерируем токен для входа
        return Model\Auth::createToken($email);
    }

    public static function resetPassword($email, $token)
    {
        if (!self::isTokenExists($email, $token)) {
            return false;
        }

        $newPassword = Hash::getRandomChars(rand(8, 12));

        if (!User::setUserPasswordByEmail($email, $newPassword)) {
            return false;
        }

        return $newPassword;
    }

    public static function isTokenExists($email, $token, $delete = true)
    {
        if (!Model\Auth::isTokenExists($email, $token)) {
            return false;
        }

        if ($delete) {
            Model\Auth::deleteToken($email, $token);
        }

        return true;
    }

    public static function deleteUserMarker()
    {
        if (!is_cli()) {
            $ci = get_instance();

            $ci->input->set_cookie('meum', '');
            $ci->input->set_cookie('eum', '');

            return true;
        }

        return false;
    }

    public static function getUserMarker()
    {
        Log::profile(__METHOD__);
        static $mainMarker = null, $marker = null;

        if (!is_null($marker)) {
            Log::profile(__METHOD__);
            return $marker;
        }

        if (!is_cli()) {
            Log::profile(__METHOD__ . '::get_instance');
            $ci = get_instance();
            Log::profile(__METHOD__ . '::get_instance');

            $mainMarker = User::getUserMainMarker($ci->input->cookie('meum'));
            $marker = User::getUserMarker($mainMarker, null, $ci->input->get('_openstat'));

            //var_dump($mainMarker);
            //var_dump($marker);

            $ci->input->set_cookie('meum', $mainMarker, 31536000);
            $ci->input->set_cookie('eum', $marker, 1800);
        }

        Log::profile(__METHOD__);
        return $marker;
    }
}