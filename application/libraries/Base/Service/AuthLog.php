<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 02.02.17
 * Time: 10:58
 */

namespace Base\Service;

use Base\Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class AuthLog
{
    const DESTINATION_MAIN_PAGE_TOP_SOCIAL = 1;
    const DESTINATION_GAME_PAGE_GREY_SOCIAL = 2;
    const DESTINATION_MAIN_PAGE_POPUP_LOGIN = 3;
    const DESTINATION_MAIN_PAGE_POPUP_LOGIN_SOCIAL = 4;
    const DESTINATION_GAME_PAGE_GREY_POPUP_LOGIN = 5;
    const DESTINATION_GAME_PAGE_GREY_POPUP_LOGIN_SOCIAL = 6;
    const DESTINATION_GAME_PAGE_TOP_MENU_POPUP_LOGIN = 7;
    const DESTINATION_GAME_PAGE_TOP_MENU_POPUP_LOGIN_SOCIAL = 8;
    const DESTINATION_GAME_PAGE_BALANCE_POPUP_LOGIN = 9;
    const DESTINATION_GAME_PAGE_BALANCE_POPUP_LOGIN_SOCIAL = 10;
    const DESTINATION_MAIN_PAGE_CATALOG_POPUP_SOCIAL = 11;
    const DESTINATION_MAIN_PAGE_CATALOG_POPUP_LOGIN_POPUP = 12;
    const DESTINATION_MAIN_PAGE_CATALOG_POPUP_LOGIN_POPUP_SOCIAL = 13;
    const DESTINATION_GAME_PAGE_POPUP_SOCIAL = 14;
    const DESTINATION_GAME_PAGE_POPUP_LOGIN_POPUP = 15;
    const DESTINATION_GAME_PAGE_POPUP_LOGIN_POPUP_SOCIAL = 16;
    const DESTINATION_GAME_PAGE_RETENTION_POPUP_SOCIAL = 17;
    const DESTINATION_GAME_PAGE_RETENTION_POPUP_LOGIN_POPUP = 18;
    const DESTINATION_GAME_PAGE_RETENTION_POPUP_LOGIN_POPUP_SOCIAL = 19;

    const DESTINATION_GAME_PAGE_LEFT_BLOCK = 20;
    const DESTINATION_GAME_PAGE_LEFT_BLOCK_SOCIAL = 21;
    const DESTINATION_GAME_PAGE_LEFT_BLOCK_LOGIN_POPUP = 22;
    const DESTINATION_GAME_PAGE_LEFT_BLOCK_LOGIN_POPUP_SOCIAL = 23;

    const DESTINATION_GAME_PAGE_WHO_PLAY = 24;
    const DESTINATION_GAME_PAGE_WHO_PLAY_SOCIAL = 25;


    const DESTINATIONS = [
        self::DESTINATION_MAIN_PAGE_TOP_SOCIAL => '<b>Гость:</b> главная/верхняя панель/социальные сети',
        self::DESTINATION_MAIN_PAGE_POPUP_LOGIN => '<b>Гость:</b> главная/форма входа',
        self::DESTINATION_MAIN_PAGE_POPUP_LOGIN_SOCIAL => '<b>Гость:</b> главная/форма входа/социальные сети',
        self::DESTINATION_MAIN_PAGE_CATALOG_POPUP_SOCIAL => '<b>Гость:</b> главная/попап каталога/социальные сети',
        self::DESTINATION_MAIN_PAGE_CATALOG_POPUP_LOGIN_POPUP => '<b>Гость:</b> главная/попап каталога/форма входа',
        self::DESTINATION_MAIN_PAGE_CATALOG_POPUP_LOGIN_POPUP_SOCIAL => '<b>Гость:</b> главная/попап каталога/форма входа/социальные сети',


        self::DESTINATION_GAME_PAGE_TOP_MENU_POPUP_LOGIN => '<b>Временный:</b> верхняя панель/форма входа',
        self::DESTINATION_GAME_PAGE_TOP_MENU_POPUP_LOGIN_SOCIAL => '<b>Временный:</b> верхняя панель/форма входа/социальные сети',
        self::DESTINATION_GAME_PAGE_GREY_SOCIAL => '<b>Временный:</b> игра/серая полоска/социальные сети',
        self::DESTINATION_GAME_PAGE_GREY_POPUP_LOGIN => '<b>Временный:</b> игра/серая полоска/форма входа',
        self::DESTINATION_GAME_PAGE_GREY_POPUP_LOGIN_SOCIAL => '<b>Временный:</b> игра/серая полоска/форма входа/социальные сети',
        self::DESTINATION_GAME_PAGE_POPUP_SOCIAL => '<b>Временный:</b> игра/попап/социальные сети',
        self::DESTINATION_GAME_PAGE_POPUP_LOGIN_POPUP => '<b>Временный:</b> игра/попап/форма входа',
        self::DESTINATION_GAME_PAGE_POPUP_LOGIN_POPUP_SOCIAL => '<b>Временный:</b> игра/попап/форма входа/социальные сети',
        self::DESTINATION_GAME_PAGE_BALANCE_POPUP_LOGIN => '<b>Временный:</b> игра/количество рублей/форма входа',
        self::DESTINATION_GAME_PAGE_BALANCE_POPUP_LOGIN_SOCIAL => '<b>Временный:</b> игра/количество рублей/форма входа/социальные сети',

        self::DESTINATION_GAME_PAGE_WHO_PLAY => '<b>Временный:</b> игра/кто играет/форма входа',
        self::DESTINATION_GAME_PAGE_WHO_PLAY_SOCIAL =>'<b>Временный:</b> игра/кто играет/форма входа/социальные сети',

        self::DESTINATION_GAME_PAGE_LEFT_BLOCK => '<b>Временный:</b> игра/левый блок',
        self::DESTINATION_GAME_PAGE_LEFT_BLOCK_SOCIAL => '<b>Временный:</b> игра/левый блок/социальные сети',
        self::DESTINATION_GAME_PAGE_LEFT_BLOCK_LOGIN_POPUP => '<b>Временный:</b> игра/левый блок/форма входа',
        self::DESTINATION_GAME_PAGE_LEFT_BLOCK_LOGIN_POPUP_SOCIAL => '<b>Временный:</b> игра/левый блок/форма входа/социальные сети',


        self::DESTINATION_GAME_PAGE_RETENTION_POPUP_SOCIAL => '<b>Retention:</b> игра/попап/социальные сети',
        self::DESTINATION_GAME_PAGE_RETENTION_POPUP_LOGIN_POPUP => '<b>Retention:</b> игра/попап/форма входа',
        self::DESTINATION_GAME_PAGE_RETENTION_POPUP_LOGIN_POPUP_SOCIAL => '<b>Retention:</b> игра/попап/форма входа/социальные сети',
    ];

    public static function getLogByDestination($start, $end)
    {
        $result = Model\AuthLog::getLogByDestination($start, $end);

        $auth = array_column($result[0], 'count', 'dest');
        $reg = array_column($result[1], 'count', 'dest');

        $data = [];

        foreach(self::DESTINATIONS as $destinationId => $destinationTitle) {
            $data[$destinationId] = [
                'title' => $destinationTitle,
                'auth' => (isset($auth[$destinationId]) ? $auth[$destinationId] : 0),
                'reg' => (isset($reg[$destinationId]) ? $reg[$destinationId] : 0)
            ];
        }

        return $data;
    }

    public static function save($uid, $type, $destinationId)
    {
        $destionations = self::DESTINATIONS;
        if (isset($destionations[$destinationId])) {
            return Model\AuthLog::save($uid, $type, time(), $destinationId);
        }

        return false;
    }
}