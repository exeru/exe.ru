<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.09.16
 * Time: 12:16
 */

namespace Base\Service;

defined('BASEPATH') OR exit('No direct script access allowed');

class GReCaptcha
{
    const URL = 'https://www.google.com/recaptcha/api/siteverify';
    const SECRET = '6LcgdikTAAAAAHXcBJpW_-vIISfxStMmQxzD4sZ7';

    public static function check($response)
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => self::URL,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [
                'secret' => self::SECRET,
                'response' => $response
            ]
        ]);

        $response = curl_exec($ch);
        $requestInfo = curl_getinfo($ch);

        curl_close($ch);

        log_message('debug', var_export($response, true));
        log_message('debug', var_export($requestInfo, true));

        if ($requestInfo['http_code'] === 200) {
            $responseObject = json_decode($response);
            if (isset($responseObject->success) && $responseObject->success === true) {
                return true;
            }
        }

        return false;
    }
}