<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 20.01.16
 * Time: 12:24
 */

namespace Base\Service;

use Base\Model;

use Base\Provider\Purchase;

defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases extends Account
{
    const PAYMENT_STATUS_INIT = 0; // Инициирован
    const PAYMENT_STATUS_ACCEPTED = 2; // Зачислено

    public static function eventManager($eventType, $params)
    {
        switch($eventType) {
            case 'EVENT_USER_BALANCE_CHANGE': {
                break;
            }
        }
    }

    public static function getLog($gid, $limit)
    {
        return Model\Purchases::getLog($gid, $limit);
    }

    public static function writeLog($gid, $type, $json, $testMode)
    {
        return Model\Purchases::writeLog($gid, $type, $json, time(), $testMode);
    }

    public static function restore($uid, $purchaseId)
    {
        $key = \Base\Service\RedisKeys::purchasesInfo . $uid;

        $data = Redis::hGet($key, $purchaseId);

        if ($data === false) {
            return false;
        }

        $data = unserialize($data);

        $balance = User::getUserInfo($uid, true)['balance'];

        $order = Model\Purchases::get($data['order_id']);

        if ($data['price'] > $balance) { // Денег все равно не хватает
            UserLog::save($uid, $order['gid'], UserLog::BUY_ITEM_NO_MONEY_AFTER_RECHARGE, ['balance' => $balance, 'count' => $data['price']]);
        }

        if ($order['status'] != self::PAYMENT_STATUS_INIT) { // У заказа изменен статус, не трогаем его
            return false;
        }

        unset($data['error']);
        $data['balance'] = $balance;

        Events::rise('EVENT_USER_PURCHASE_RESTORE', [
            'uids' => [$uid],
            'message' => json_encode([
                'type' => 'purchase_restore',
                'message' => $data
            ])
        ]);

        return true;
    }

    public static function forget($uid, $purchaseId)
    {
        $key = \Base\Service\RedisKeys::purchasesInfo . $uid;

        Redis::hDel($key, $purchaseId);
    }

    public static function remember($uid, $data)
    {
        $key = \Base\Service\RedisKeys::purchasesInfo . $uid;

        Redis::hSet($key, $data['order_id'], serialize($data));
    }

    public static function getItemInfo($gid, $uid, $itemId)
    {
        $gameInfo = Game::getPurchasesInfoByGameId($gid);

        $info = Purchase::getItemInfo($gid, $uid, $itemId, $gameInfo['callback_url'], $gameInfo['api_key']);

        return $info;
    }

    public static function get($uid, $orderId)
    {
        $key = \Base\Service\RedisKeys::purchasesInfo . $uid;

        $data = Redis::hGet($key, $orderId);

        return $data;
    }

    public static function purchaseItem($gid, $uid, $item, $orderId, $testMode)
    {
        $data = Model\Purchases::get($orderId);

        if (is_null($data) || $data['uid'] != $uid || $data['gid'] != $gid || $data['status'] != self::PAYMENT_STATUS_INIT) {
            return false;
        }

        if (!$testMode) {
            // Проверяем баланс у пользователя
            $balance = User::getUserInfo($uid, true)['balance'];
            if ($balance < $data['count']) {
                return false;
            }
        }

        $gameInfo = Game::getPurchasesInfoByGameId($gid);

        $result = Purchase::purchaseItem($gid, $uid, $item, $orderId, $gameInfo['callback_url'], $gameInfo['api_key'], $testMode);

        if ($result === false) {
            return false;
        }

        Model\Purchases::update($orderId, $result['app_order_id'], self::PAYMENT_STATUS_ACCEPTED);

        if (!$testMode) {
            self::decrementBalance($uid, $data['sum'], $gid, $data['count']);
            $balance = User::getUserInfo($uid, true)['balance'];
            UserLog::save($uid, $gid, UserLog::PAY_TO_GAME, ['gid' => $gid, 'count' => $data['count'], 'balance' => $balance], Auth::getUserMarker());
        } else {
            UserLog::save($uid, $gid, UserLog::PAY_TO_GAME_TEST, ['gid' => $gid, 'count' => $data['count']], Auth::getUserMarker());
        }

        return true;
    }

    public static function prepare($uid, $gid, $count)
    {
        $orderId = self::getNewOrderNumber();
        $sum = number_format(self::ONE_BONUS_COST * $count, 2, '.', '');

        Model\Purchases::create($orderId, $uid, $gid, $count, $sum, NULL, self::PAYMENT_STATUS_INIT);

        return $orderId;
    }

    public static function getHistoryData($receiver)
    {
        $data = [];

        $game = Game::getGameById($receiver);

        $data['sourceImage'] = IMAGE_HOST . '/assets/img/app/' . $game['gid'] . '/150x150/'. DPRIMAGEPREFIX .  $game['image_150x150'];
        $data['sourceText'] = 'Покупка в игре ' . $game['title'];

        return $data;
    }
}