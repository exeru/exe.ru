<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 31.07.15
 * Time: 16:36
 */
class Twig
{
    private $CI;
    private $_twig;
    private $_template_dir;
    private $_cache_dir;
    private $cacheType;

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->config->load('twig');

        log_message('debug', "Twig Autoloader Loaded");

        $this->_template_dir = $this->CI->config->item('template_dir');
        $loader = new Twig_Loader_Filesystem($this->_template_dir);

        $this->cacheType = 'Redis';
        $cache = new \Base\Service\TwigCacheRedis(ENVIRONMENT);

        if (ENVIRONMENT === 'development') {
            $this->_twig = new Twig_Environment($loader);
        } else {
            $this->_twig = new Twig_Environment($loader, ['cache' => $cache]);
        }


        $this->CI->load->helper('plural');
        $this->_twig->addFilter('plural_str', new Twig_Filter_Function('plural_str'));

        $hasAccess = new Twig_SimpleFunction('hasAccess', function ($uid, $gid, $access) {
            return \Base\Service\Roles::hasAccess($uid, $gid, $access);
        });
        $this->_twig->addFunction($hasAccess);

        $isModerator = new Twig_SimpleFunction('isModerator', function ($uid) {
            return \Base\Service\Roles::isModerator($uid);
        });
        $this->_twig->addFunction($isModerator);

    }

    /**
     * Возвращает сформированный контент
     * @param string $template - путь к шаблону
     * @param array $data - массив данных для шаблона
     */
    public function render($template, $data = array())
    {
        $result = $this->_twig->loadTemplate($template);

        return $result->render($data);
    }

    /**
     * Отображает сформированный контент
     * @param string $template - путь к шаблону
     * @param array $data - массив данных для шаблона
     */
    public function view($template, $data = [])
    {
        echo $this->render($template, $data);
    }
}
