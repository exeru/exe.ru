<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28.12.15
 * Time: 16:10
 */
if (!function_exists('plural_str')) {
    function plural_str($number, $one, $two, $five) {
        $number %= 100;
        if ($number >= 5 && $number <= 20) {
            return $five;
        }
        $number %= 10;
        if ($number == 1) {
            return $one;
        }
        if ($number >= 2 && $number <= 4) {
            return $two;
        }
        return $five;
    }
}