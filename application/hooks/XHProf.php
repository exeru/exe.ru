<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 24.05.16
 * Time: 10:58
 */
class MY_XHProf
{
    private $enabled = false;
    public function start()
    {
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], 'xhprof=enable') && extension_loaded('xhprof')) {
            if (ENVIRONMENT == 'testing') {
                include_once '/home/web/xhprof-0.9.4/xhprof_lib/utils/xhprof_lib.php';
                include_once '/home/web/xhprof-0.9.4//xhprof_lib/utils/xhprof_runs.php';

                xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
                $this->enabled = true;
            } elseif($_SERVER['HTTP_HOST'] == 'exe.ru.dev') {
                include_once '/home/ruslan/xhprof/xhprof/xhprof_lib/utils/xhprof_lib.php';
                include_once '/home/ruslan/xhprof/xhprof/xhprof_lib/utils/xhprof_runs.php';

                xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
                $this->enabled = true;
            }
        }
    }

    public function end()
    {
        if ($this->enabled) {
            if (ENVIRONMENT == 'testing') {
                $profilerNamespace = 'testExeRu';
                $url = 'http://test.exe.ru/xhprof_html/?run=%s&source=%s';
            } else {
                $profilerNamespace = 'exeRuDev';
                $url = 'http://xhprof.dev/?run=%s&source=%s';
            }

            $xhprofData = xhprof_disable();
            $xhprofRuns = new XHProfRuns_Default();
            $runId = $xhprofRuns->save_run($xhprofData, $profilerNamespace);

            $profilesUrl = sprintf($url, $runId, $profilerNamespace);
            echo '<a target="_blank" href="' . $profilesUrl . '">Profiler Output</a>';
        }
    }
}