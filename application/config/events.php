<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06.10.15
 * Time: 12:45
 */
$config = [
    'events' => [
        'EVENT_USER_BLOCKED' => [ // Администратор забанил пользователя
            '\Base\Service\Api::eventManager' // Разавторизуем пользователя
        ],

        'EVENT_USER_ONLINE' => [ // Пользователь стал онлайн
            '\Base\Service\Api::eventManager' // Сообщаем друзьям
        ],

        // ИГРЫ
        'EVENT_USER_INSTALL_GAME' => [ // Пользователь установил игру
            '\Base\Service\Api::eventManager', // Информируем игровое API и нотификации
            '\Base\Service\Feeds::eventManager', // Добавляем новость
            '\Base\Service\StatsGame::eventManager' // Записываем источник установки
        ],
        'EVENT_USER_INVITE_TO_GAME' => [ // Пользователь приглашает друзей в игру
            '\Base\Service\Friends::eventManager', // Записываем
            '\Base\Service\Api::eventManager' // Сообщаем друзьям
        ],
        'EVENT_USER_INVITE_TO_FRIEND_BY_GAME' => [
            '\Base\Service\Friends::eventManager', // Записываем
            '\Base\Service\Api::eventManager' // Сообщаем друзьям
        ],

        // ДРУЖБА
        'EVENT_USER_ACCEPT_FRIEND' => [ // Пользователь подтверждает дружбу с другим пользователем
            '\Base\Service\Friends::eventManager', // Записываем
            '\Base\Service\Feeds::eventManager', // Добавляем новость
            '\Base\Service\Api::eventManager' // Уведомляем остальных пользователей о изменении количества новостей
        ],
        'EVENT_USER_APPEND_FRIEND' => [ // Пользователь отправляет заявку на дружбу
            '\Base\Service\Friends::eventManager', // Записываем
            '\Base\Service\Api::eventManager' // Информируем другого пользователя о новой заявке
        ],
        'EVENT_FRIENDS_CHANGE_COUNT'  => [ // Изменилось количество заявок в друзья
            '\Base\Service\Api::eventManager' // Сообщаем пользователям
        ],

        // ДИАЛОГИ
        'EVENT_USER_SEND_MESSAGE' => [ // Пользователь отправил сообщение другому пользователю
            '\Base\Service\Api::eventManager', // Сообщаем другу
            '\Base\Service\WebPush::eventManager', // Отправляем пуш
        ],
        'EVENT_USER_HAS_READ_MESSAGE' => [ // Пользователь прочитал сообщения от другого пользователя
            '\Base\Service\Api::eventManager' // Сообщаем автору сообщений
        ],
        'EVENT_USER_TYPING_ON'  => [ // Пользователь начал набирать сообщение другому пользователю
            '\Base\Service\Api::eventManager' // Сообщаем другу
        ],
        'EVENT_USER_TYPING_OFF'  => [ // Пользователь перестал набирать сообщение другому пользователю
            '\Base\Service\Api::eventManager' // Сообщаем другу
        ],
        'EVENT_DIALOGS_CHANGE_COUNT'  => [ // Изменилось количество непрочитанных диалогов
            '\Base\Service\Api::eventManager' // Сообщаем пользователям
        ],

        // ЧЕРНЫЙ СПИСОК
        'EVENT_USER_APPEND_TO_BLACKLIST'  => [ // Пользователь добавлен в черный список
            '\Base\Service\Api::eventManager' // Сообщаем пользователю о необходимости сбросить кеш
        ],
        'EVENT_USER_REMOVE_FROM_BLACKLIST'  => [ // Пользователь удален из черного списка
            '\Base\Service\Api::eventManager' // Сообщаем пользователю о необходимости сбросить кеш
        ],

        // ФИНАНСЫ
        'EVENT_USER_BALANCE_CHANGE' => [ // У пользователя изменился баланс
            '\Base\Service\Api::eventManager', // Информируем пользователя (изменяем баланс в браузере)
            '\Base\Service\Promo::eventManager', // Начисляем бонусы
        ],
        'EVENT_USER_BALANCE_CHANGE_BONUS' => [ // У пользователя изменился баланс (бонусное начисление)
            '\Base\Service\Api::eventManager', // Информируем пользователя (изменяем баланс в браузере)
        ],
        'EVENT_USER_PURCHASE_RESTORE' => [ // Обнаружена незавершенная покупка
            '\Base\Service\Api::eventManager', // Информируем пользователя
        ],

        // Новости
        'EVENT_NEWS_ADD' => [ // Новость была добавлена
            '\Base\Service\Api::eventManager', // Информируем пользователя
        ],
        'EVENT_NEWS_DELETE' => [ // Новость была удалена
            '\Base\Service\Api::eventManager', // Информируем пользователя
        ],
    ]
];