<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 31.07.15
 * Time: 16:39
 */

$config['template_dir'] = APPPATH.'views';
$config['cache_dir'] = APPPATH.'cache/twig';