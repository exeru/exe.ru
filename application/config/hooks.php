<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['pre_system'][] = [
    'class' => 'MY_XHProf',
    'function' => 'start',
    'filename' => 'XHProf.php',
    'filepath' => 'hooks',
    'params' => []
];
$hook['post_system'][] = [
    'class' => 'MY_XHProf',
    'function' => 'end',
    'filename' => 'XHProf.php',
    'filepath' => 'hooks',
    'params' => []
];