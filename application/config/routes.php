<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['translate_uri_dashes'] = FALSE;
$route['rules'] = 'frontend/rules';
$route['devdoc'] = 'frontend/devdoc';
$route['agreement'] = 'frontend/agreement';

$route['admin/emails_do'] = 'devAjax/emails';
$route['admin/emails_do/(:any)'] = 'devAjax/emails/$1';

$route['robots.txt'] = 'frontend/robots';

$route['auth_(:any)'] = 'frontend/auth/$1';
$route['auth_(:any)/(:any)'] = 'frontend/auth/$1/$2';

$route['default_controller'] = 'welcome';

$route['remont_all'] = 'frontend/remontAll';

if (\Base\Service\Project::isAdmin()) {
    $route['default_controller'] = 'admin';
    $route['payments'] = 'admin/payments';
    $route['userstat'] = 'admin/userstat';
    $route['marker/(:any)'] = 'admin/marker/$1';
}

if (\Base\Service\Project::isDev()) {
    $route['default_controller'] = 'dev';

    $route['doc'] = 'dev/doc/doc';
    $route['doc/(:any)'] = 'dev/doc/$1';
    $route['doc/(:any)/(:any)'] = 'dev/doc/$1/$2';
    $route['doc/(:any)/(:any)/(:any)'] = 'dev/doc/$1/$2/$3';
    $route['doc/(:any)/(:any)/(:any)/(:any)'] = 'dev/doc/$1/$2/$3/$4';

    $route['feedback_do'] = 'devActions/help/feedback';;

    $route['add'] = 'dev/index/add';
    $route['add_do'] = 'devActions/add';

    $route['games'] = 'dev/games';
    $route['control'] = 'dev/control';
    $route['control_do/(:num)'] = 'devAjax/control/$1';

    $route['contracts'] = 'dev/contracts';
    $route['contract/applications/(:num)'] = 'dev/contractApplications/$1';
    $route['contract/applications_do/(:num)'] = 'devAjax/contractApplications/$1';
    $route['contract/cashout/(:num)'] = 'dev/contractCashout/$1';
    $route['contract/cashout/(:num)/(:num)'] = 'dev/contractCashout/$1/$2';
    $route['contract/showAct/(:num)/(:num)'] = 'dev/contractShowAct/$1/$2';
    $route['contract/printAct/(:num)/(:num)'] = 'dev/contractPrintAct/$1/$2';
    $route['contract/transactions/(:num)'] = 'dev/contractTransactions/$1';
    $route['contract/edit/(:any)'] = 'dev/contractEdit/$1';
    $route['contract/edit_do/(:any)'] = 'devActions/contractEdit/$1';

    $route['info/(:num)'] = 'dev/info/$1';
    $route['info_do/(:num)'] = 'devActions/info/$1';

    $route['settings/(:num)'] = 'dev/settings/$1';
    $route['settings_do/(:num)'] = 'devActions/settings/$1';

    $route['stats/(:num)'] = 'dev/stats/$1';

    $route['changes/(:num)'] = 'dev/changes/$1';

    $route['notifications/(:num)'] = 'dev/notifications/$1';

    $route['news/(:num)'] = 'dev/news/$1';
    $route['news_do/(:num)'] = 'devActions/news/$1';
    $route['getFeed'] = 'devActions/getFeed';
    $route['deleteFeed'] = 'devActions/deleteFeed';

    $route['billing/(:num)'] = 'dev/billing/$1';
    /*$route['payments_do/(:num)'] = 'devActions/payments/$1';*/

    $route['roles/(:num)'] = 'dev/roles/$1';
    $route['roles/addTester'] = 'devAjax/rolesChange/add/tester';
    $route['roles/addAdmin'] = 'devAjax/rolesChange/add/admin';
    $route['roles/deleteTester'] = 'devAjax/rolesChange/delete/tester';
    $route['roles/deleteAdmin'] = 'devAjax/rolesChange/delete/admin';
    $route['roles/changeAccess'] = 'devAjax/rolesChange/change/tester';

    $route['status/(:num)'] = 'dev/status/$1';
    $route['status_do/(:num)'] = 'devActions/status/$1';

    $route['help/(:num)'] = 'dev/help/$1';
    $route['help_do/(:num)'] = 'devActions/help/$1';

    $route['assign'] = 'frontend/social/assign';
    $route['create'] = 'frontend/social/create';

    $route['devstats/get'] = 'devStats/get';

    $route['help'] = 'dev/doc/doc';
}

if (\Base\Service\Project::isMain()) {
    $route['default_controller'] = 'frontend';
    $route['404_override'] = '';

    $route['play'] = 'ajax/play';

    $route['acceptUser'] = 'ajax/acceptUser';
    $route['remont'] = 'frontend/remont';

    $route['bounced'] = 'frontend/bounced';

    $route['id(:num)'] = 'frontend/index';
    $route['news'] = 'frontend/index';
    $route['dialogs'] = 'frontend/index';
    $route['friends'] = 'frontend/index';
    $route['settings'] = 'frontend/index';
    $route['help'] = 'frontend/index';

    $route['games'] = 'frontend/myGames';

    $route['ajax/save_(:any)'] = 'ajax/saveUserSettings/$1';
    $route['ajax/search_users'] = 'ajax/searchUsers/users';
    $route['ajax/search_friends'] = 'ajax/searchUsers/friends';

    $route['upload'] = 'ajax/fileUpload';

    $route['app(:num)'] = 'frontend/game/$1';
    $route['assign'] = 'frontend/social/assign';
    $route['create'] = 'frontend/social/create';
    $route['frontend/change_email'] = 'frontend/changeEmail';
    $route['logout'] = 'frontend/logout';

    $route['jsapi'] = 'jsapi/index';

    $route['ajax/restoreFeed'] = 'ajax/feedAction/restore';
    $route['ajax/deleteFeed'] = 'ajax/feedAction/delete';
    $route['ajax/publishFeed'] = 'ajax/feedPublish';
    $route['ajax/getFeeds'] = 'ajax/feedsGet';

    $route['ajax/acceptRequest'] = 'ajax/friendsAction/accept';
    $route['ajax/rejectRequest'] = 'ajax/friendsAction/reject';
    $route['ajax/cancelRequest'] = 'ajax/friendsAction/cancel';
    $route['ajax/appendFriend'] = 'ajax/friendsAction/append';

    $route['ajax/inviteToGame'] = 'ajax/friendsInviteToGame';

    $route['gw/yandex'] = 'gateway/yandex';
    $route['gw/paymaster'] = 'gateway/paymaster';
    $route['gw/qiwi'] = 'gateway/qiwi';
    $route['gw/paypal'] = 'gateway/paypal';

    $route['gw/viber'] = 'gateway/viber';

    $route['blocked'] = 'frontend/blocked';

    if (ENVIRONMENT == 'development') {
        $route['myapp'] = 'jsapi/index';
    }
}


