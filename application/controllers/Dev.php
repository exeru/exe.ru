<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 28.01.16
 * Time: 15:50
 */

use Base\Service\Auth,
    Base\Service\Contract,
    Base\Service\Game,
    Base\Service\Feeds,
    Base\Service\Roles,
    Base\Service\Stats,
    Base\Service\User;

defined('BASEPATH') OR exit('No direct script access allowed');

class Dev extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ((!Auth::isAuth() || User::isTemporaryUser(Auth::getUserId())) && $this->router->method != 'index' && $this->router->method != 'doc') {
            $this->load->helper(['url']);
            redirect(base_url('/', 'http'), 'get', 302);
            exit();
        }
    }

    public function contractTransactions($cid)
    {
        $uid = Auth::getUserId();

        if (!$this->hasContractAccess($uid, $cid)) {
            Roles::forbidden();
        }

        $transactions = Contract::getTransactionsAtPeriod($cid, date('Ym'));

        if (count($transactions) > 0) {
            foreach($transactions as &$transaction) {
                $transaction['user'] = User::getUserInfo($transaction['uid']);
                $transaction['game'] = Game::getGameById($transaction['gid']);
            }
        }

        $games = Game::getGamesByContractId($cid);
        $gids = array_column($games, 'gid');

        $data = [
            'topLink' => 'contracts',
            'user' => User::getUserInfo($uid),
            'game' => [
                'contract' => $cid,
                'gid' => $gids[0]
            ],
            'contract' => Contract::getContractById($cid),
            'withdrawSum' => array_sum(array_column($transactions, 'count')),
            'transactions' => $transactions,
            'leftLink' => 'contract/transactions'
        ];

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/contractTransactions.twig', $data);
    }

    public function contractPrintAct($cid, $period = null)
    {
        $uid = Auth::getUserId();

        if (!$this->hasContractAccess($uid, $cid)) {
            Roles::forbidden();
        }

        $data = $this->contractPrepareData($cid, $period);

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/contractPrintAct.twig', $data);
    }

    public function contractShowAct($cid, $period = null)
    {
        $uid = Auth::getUserId();

        if (!$this->hasContractAccess($uid, $cid)) {
            Roles::forbidden();
        }

        $contract = Contract::getContractById($cid);

        if (empty($period)) {
            $period = new DateTime();
            $period->modify('-1 month');
            $period = $period->format('Ym');
        }

        $data = [
            'topLink' => 'contracts',
            'leftLink' => 'contract/cashout',
            'user' => User::getUserInfo($uid),
            'contract' => $contract,
            'period' => $period
        ];

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/contractShowAct.twig', $data);
    }

    public function contractCashout($cid, $period = null)
    {
        $uid = Auth::getUserId();

        if (!$this->hasContractAccess($uid, $cid)) {
            Roles::forbidden();
        }

        $data = $this->contractPrepareData($cid, $period);

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/contractCashout.twig', $data);
    }

    private function contractPrepareData($cid, $period = null)
    {
        $uid = Auth::getUserId();
        $contract = Contract::getContractById($cid);

        if (empty($period)) {
            $period = new DateTime();
            $period->modify('-1 month');
            $period = $period->format('Ym');
        }

        $year = substr($period, 0, 4);
        $month = substr($period, 4);

        $prevPeriod = new DateTime();
        $prevPeriod->setDate($year, $month, 15);
        $prevPeriod->setTime(12, 0, 0);
        $prevPeriod->modify('-1 month');

        $prevPeriod = $prevPeriod->format('Ym');

        if ($period == date('Ym')) {
            $nextPeriod = false;
        } else {
            $nextPeriod = new DateTime();
            $nextPeriod->setDate($year, $month, 15);
            $nextPeriod->setTime(12, 0, 0);
            $nextPeriod->modify('+1 month');
            $nextPeriod = $nextPeriod->format('Ym');

            if ($nextPeriod == date('Ym')) {
                $nextPeriod = false;
            }
        }

        $data['months'] = [
            '01' => 'январь',
            '02' => 'февраль',
            '03' => 'март',
            '04' => 'апрель',
            '05' => 'май',
            '06' => 'июнь',
            '07' => 'июль',
            '08' => 'август',
            '09' => 'сентябрь',
            '10' => 'октябрь',
            '11' => 'ноябрь',
            '12' => 'декабрь'
        ];

        $games = Game::getGamesByContractId($cid);
        $gids = array_column($games, 'gid');
        $gamesArray = array_combine($gids, $games);

        $withdrawByGids = Contract::getWithdrawAtPeriodByGids($gids, $period);
        foreach($withdrawByGids as $withdraw) {
            $gamesArray[$withdraw['gid']]['sum'] = $withdraw['sum'];
        }


        foreach($gamesArray as $gid => &$game) {
            if (!isset($game['sum'])) {
                $game['sum'] = 0;
            }

            $game['cashout'] = $game['sum'] - ($game['sum'] * $contract['comission']) / 100;
        }

        $data = [
            'topLink' => 'contracts',
            'leftLink' => 'contract/cashout',
            'user' => User::getUserInfo($uid),
            'game' => [
                'contract' => $cid,
                'gid' => $gids[0]
            ],
            'contract' => $contract,
            'games' => $gamesArray,
            'withdraw' => array_sum(array_column($gamesArray, 'cashout')),
            'prev' => $prevPeriod,
            'next' => $nextPeriod,
            'current' => $period,
            'currentText' => $data['months'][$month] . ' ' . $year
        ];

        if ($contract['nds'] > 0) {
            $data['nds'] = round( 100 * $data['withdraw'] * $contract['nds'] / (100 + $contract['nds']) ) / 100;
            $data['withdrawClean'] = $data['withdraw'] - $data['nds'];
        }

        return $data;
    }

    private function hasContractAccess($uid, $cid)
    {
        if (Roles::isModerator($uid)) {
            return true;
        }

        $games = Game::getGamesByContractId($cid);
        if (count($games) == 0) {
            return false;
        }

        foreach($games as $game) {
            if (Roles::hasAccess($uid, $game['gid'], Roles::ACCESS_CONTRACT)) {
                return true;
            }
        }

        return false;
    }

    public function contractApplications($cid)
    {
        $uid = Auth::getUserId();

        if (!$this->hasContractAccess($uid, $cid)) {
            Roles::forbidden();
        }

        $games = Game::getGamesByContractId($cid);
/*
        $sum = 0;
        if (count($games) > 0) {
            $sum = array_sum(array_column($games, 'balance'));
        }*/

        $gids = array_column($games, 'gid');
        $withdrawByGids = Contract::getWithdrawAtPeriodByGids($gids, date('Ym'));

        $data = [
            'topLink' => 'contracts',
            'user' => User::getUserInfo($uid),
            'game' => [
                'contract' => $cid,
                'gid' => $gids[0]
            ],
            'contract' => ['id' => $cid],
            'games' => $games,
            /*'sum' => $sum,*/
            'withdrawSum' => array_sum(array_column($withdrawByGids, 'sum')),
            'leftLink' => 'contract/applications'
        ];

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/contractApplications.twig', $data);
    }


    public function contractEdit($cid)
    {
        $uid = Auth::getUserId();

        if (!Roles::isModerator($uid)) {
            Roles::forbidden();
        }

        if (is_numeric($cid)) {
            $contract = Contract::getContractById($cid);
        }

        if (!isset($contract) || empty($contract)) {
            $contract = [
                'id' => $cid
            ];
        }

        $data = [
            'topLink' => 'contracts',
            'user' => User::getUserInfo($uid),
            'contract' => $contract,
            'leftLink' => 'contract/edit'
        ];


        $this->load->library(['Twig']);
        $this->twig->view('dev/game/contract.twig', $data);
    }

    public function contracts()
    {
        $uid = Auth::getUserId();

        if (!Roles::isModerator($uid)) {
            Roles::forbidden();
        }

        // Получаем все контракты
        $contracts = Contract::getContracts();

        if (count($contracts) > 0) {
            $temp = [];
            foreach($contracts as $row) {
                $temp[$row['id']] = $row;
                $temp[$row['id']]['number'] = trim($temp[$row['id']]['number']);
                $temp[$row['id']]['date'] = trim($temp[$row['id']]['date']);
            }
            $contracts = $temp;
            unset($temp);
        }

        $result = [];

        // Получаем игры, у которых привязаны контракты
        $games = Game::getGamesWithContracts();

        if (count($games) > 0) {
            foreach($games as $row) {
                if (isset($contracts[$row['contract']])) {
                    $contract = [
                        'game' => $row,
                        'owner' => User::getUserInfo($row['owner_uid']),
                        'number' => $contracts[$row['contract']]['number'],
                        'date' => $contracts[$row['contract']]['date']
                    ];

                    $result[] = $contract;
                }
            }
        }

        $noUsedContacts = array_diff(array_keys($contracts), array_column($games, 'contract'));

        $noUsedContactsData = [];
        if (count($noUsedContacts) > 0) {
            foreach($noUsedContacts as $id) {
                $noUsedContactsData[] = $contracts[$id];
            }
        }

        $data = [
            'topLink' => 'contracts',
            'user' => User::getUserInfo($uid),
            'contracts' => $result,
            'no_used' => $noUsedContactsData,
            'count' => count($result) + count($noUsedContactsData),
            'leftLink' => 'contract/edit'
        ];

        $data['count'] = $data['count'] < 10 ? '00' . $data['count'] : ($data['count'] < 100 ? '0' . $data['count'] : $data['count']);

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/contracts.twig', $data);
    }

    public function doc($levelOne = null, $levelTwo = null, $levelThree = null, $levelFour = null)
    {
        $data = ['topLink' => 'doc'];

        if (Auth::isAuth() && !User::isTemporaryUser(Auth::getUserId())) {
            $data ['user'] = User::getUserInfo(Auth::getUserId());
        }

        $data['leftLink'] = rtrim(implode('/', ['doc', $levelOne, $levelTwo, $levelThree, $levelFour]), '/');

        $this->load->library(['Twig']);
        $this->twig->view('dev/' . $data['leftLink'] . '.twig', $data);
    }

    public function index($page = 'index', $subPage = '')
    {
        if (Auth::isAuth() && !User::isTemporaryUser(Auth::getUserId())) {
            $data = [
                'user' => User::getUserInfo(Auth::getUserId())
            ];
        } else {
            $data = [];
        }

        $data['leftLink'] = $page;
        if ($page == 'add') {
            $data['topLink'] = 'games';
            $page = 'game/' . $page;
        } else {
            $data['topLink'] = 'index';
        }

        $this->load->library(['Twig']);
        $this->twig->view('dev/' . $page . '.twig', $data);
    }

    public function control()
    {
        $uid = Auth::getUserId();

        if (!Roles::isModerator($uid)) {
            Roles::forbidden();
        }

        $games = Game::getAllGamesSortedByStatus();

        $data = [
            'topLink' => 'control',
            'user' => User::getUserInfo($uid),
            'games' => $games,
            'count' => count($games)
        ];

        $data['count'] = $data['count'] < 10 ? '00' . $data['count'] : ($data['count'] < 100 ? '0' . $data['count'] : $data['count']);

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/control.twig', $data);
    }

    public function games()
    {
        $uid = Auth::getUserId();

        $data = [
            'topLink' => 'games',
            'user' => User::getUserInfo($uid)
        ];

        $games = Roles::getGames($uid);

        if (count($games) > 0) {
            foreach ($games as $gid) {
                if (Roles::hasAccess($uid, $gid, Roles::ACCESS_VIEW)) {
                    $game = Game::getGameDataFieldsById($gid, ['gid', 'title', 'type', 'image_810x500']);
                    $game['roleGroup'] = Roles::getUserGroup($uid, $gid);
                    $data['games'][] = $game;
                }
            }
        }

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/games.twig', $data);
    }

    public function info($gid)
    {
        $uid = Auth::getUserId();

        Roles::checkPermission($uid, $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'info',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'type', 'owner_uid', 'description', 'terms', 'privacy', 'image_256x256', 'image_64x64', 'image_16x16', 'image_150x150', 'image_810x500', 'image_80x80', 'image_guest', 'developer_info', 'contract']),
        ];

        if (Roles::isModerator($uid)) {
            $data['contracts'] = Contract::getContracts();
        }

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/info.twig', $data);
    }

    public function settings($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'settings',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'enabled', 'http', 'https', 'api_key', 'callback_url', 'height', 'image_256x256', 'repairs', 'contract']),
        ];

        if ($data['game']['repairs'] == 't') {
            $data['game']['enabled'] = 'r';
        }

        $data['game']['roleGroup'] = Roles::getUserGroup(Auth::getUserId(), $gid);

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/settings.twig', $data);
    }

    public function stats($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'stats',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'image_256x256', 'contract'])
        ];

        $stats = Stats::getStats($gid);
        $stats['visits']['days']['unique'] = implode(', ', $stats['visits']['days']['unique']);
        $stats['visits']['days']['nounique'] = implode(', ', $stats['visits']['days']['nounique']);
        $stats['visits']['months']['unique'] = implode(', ', $stats['visits']['months']['unique']);
        $stats['visits']['months']['nounique'] = implode(', ', $stats['visits']['months']['nounique']);

        // Пол/возраст в проценты
        foreach(['week', 'month'] as $period) {
            $sum = array_sum($stats['age_gender'][$period]['male']) + array_sum($stats['age_gender'][$period]['female']);
            foreach($stats['age_gender'][$period]['male'] as $group=>$value) {
                if ($value > 0 || $stats['age_gender'][$period]['female'][$group] > 0) {
                    $stats['age_gender'][$period]['male'][$group] = (int) (($stats['age_gender'][$period]['male'][$group] * 100) / $sum);
                    $stats['age_gender'][$period]['female'][$group] = (int) (($stats['age_gender'][$period]['female'][$group] * 100) / $sum);
                }
            }
            $stats['age_gender'][$period]['male'] = implode(', ', $stats['age_gender'][$period]['male']);
            $stats['age_gender'][$period]['female'] = implode(', ', $stats['age_gender'][$period]['female']);
        }

        // Гео статистика
        $max = 3;
        foreach(['day', 'week', 'month', 'all'] as $period) {
            foreach(['country', 'city'] as $type) {
                $sum = array_sum($stats['geo'][$period][$type]);
                $slice = false;
                if (count($stats['geo'][$period][$type]) > $max) {
                    $stats['geo'][$period][$type] = array_slice($stats['geo'][$period][$type], 0, $max, true);
                    $slice = true;
                }
                foreach($stats['geo'][$period][$type] as $id=>$value) {
                    $stats['geo'][$period][$type][$id] = [];
                    $stats['geo'][$period][$type][$id]['name'] = \Base\Service\Dictionary::getItem($type, $id);
                    $stats['geo'][$period][$type][$id]['value'] = (int) (100*$value/$sum);
                }

                if (count($stats['geo'][$period][$type]) == $max && $slice) {
                    $another = [];
                    $another['name'] = 'Другие';
                    $another['value'] = 100 - array_sum(array_column($stats['geo'][$period][$type], 'value'));
                    if ($another['value'] > 0) {
                        $stats['geo'][$period][$type][] = $another;
                    }
                }
            }
        }

        $data['stats'] = $stats;

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/stats.twig', $data);
    }

    public function changes($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'changes',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'image_256x256', 'contract'])
        ];

        $stats = \Base\Service\StatsGame::getSources($gid);
        foreach($stats as $period => $periodData) {
            if (count($periodData) > 0) {
                $periodData = array_column($periodData, 'count', 'group_id');
                arsort($periodData);
                $sum = array_sum($periodData);
                foreach($periodData as $groupId=>$count) {
                    $periodData[$groupId] = (int) floor(($count * 100) / $sum);
                }
                $stats[$period] = $periodData;
            }
        }

        $data['stats'] = $stats;
        $data['statsGroups'] = array_column(\Base\Service\StatsGame::STATS_INSTALL_GROUP, 'title', 'id');

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/changes.twig', $data);
    }

    public function notifications($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'notifications',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'image_256x256', 'contract'])
        ];

        $stats = \Base\Service\Notifications::getStats($gid);

        foreach(['all', 'groups'] as $type) {
            foreach(['days', 'months'] as $period) {
                foreach(['all', 'filter', 'success', 'fail'] as $dataType) {
                    $stats[$type][$period][$dataType] = implode(', ', $stats[$type][$period][$dataType]);
                }
            }
        }

        // График эффективности
        foreach($stats['types']['success'] as &$type) {
            $type = implode(',', $type);
        }

        $months = ['янв', 'фев', 'мар', 'апр', 'мая', 'июня', 'июля', 'авг', 'сен', 'окт', 'нояб', 'дек'];
        $stats['types']['categories'] = [];
        // Категории для графика
        for($i=$stats['types']['start']; $i<=$stats['types']['end']; $i+=86400)
        {
            $stats['types']['categories'][] = date('d', $i) . ' ' . $months[(int)date('m', $i) - 1] . ' ' . date('Y', $i);
        }
        $stats['types']['categories'] = "'" . implode("', '", $stats['types']['categories']) . "'";

        // Таблица эффективности
        foreach($stats['feedback']['today'] as &$today) {
            $today['filter_procent'] = $today['success_procent'] = $today['fail_procent'] = 0;
            if ($today['all'] > 0 && $today['filter'] > 0) {
                $today['filter_procent'] = floor($today['filter'] * 100 / $today['all']);
            }
            if ($today['success'] > 0) {
                $today['success_procent'] = floor($today['success'] * 100 / $today['filter']);
            }
            if ($today['fail'] > 0) {
                $today['fail_procent'] = floor($today['fail'] * 100 / $today['filter']);
            }
        }

        $data['stats'] = $stats;

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/notifications.twig', $data);
    }

    public function news($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'news',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'image_256x256', 'image_150x150', 'contract']),
            'news' => Feeds::getFeeds(NULL, [$gid], [], Feeds::FEEDS_COUNT_FOR_DEV),
            'appointed' => Feeds::getPendingFeeds($gid),
            'applink' => (ENVIRONMENT == 'development' ? 'http://exe.ru.dev' : (ENVIRONMENT == 'testing' ? 'http://test.exe.ru' : 'http://exe.ru')) . '/app' . $gid
        ];

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/news.twig', $data);
    }

    public function billing($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'billing',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'image_256x256', 'image_150x150', 'balance', 'contract']),
            'history' => \Base\Service\Account::getHistoryAsReciever($gid, 1500),
            'log' => \Base\Service\Purchases::getLog($gid, 24)
        ];

        // Агрегируем данные
        $graph = [];
        foreach($data['history'] as $purchase) {
            $time = strtotime(date('Y-m-d', $purchase['time']));
            if ($purchase['type'] == \Base\Service\Account::decrementGame) {
                continue;
            }

            if (isset($graph[$time])) {
                $graph[$time] += (int) $purchase['count'];
            } else {
                $graph[$time] = (int) $purchase['count'];
            }
        }
        // Запоминаем минимальное и максимальное время
        $end = time();
        $start = count($graph) > 0 ? min(array_keys($graph)) : $end;

        for($current = $start; $current<$end; $current+=86400) {
            if (isset($graph[$current])) {
                continue;
            }
            $graph[$current] = 0;
        }
        ksort($graph);

        $data['graph'] = [
            'start' => $start + date_offset_get(new DateTime),
            'end' => $end + date_offset_get(new DateTime),
            'data' => implode(', ', $graph)
        ];

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/billing.twig', $data);
    }

    public function roles($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $uid = Auth::getUserId();

        $users = Roles::getUsersByGroups($gid);
        $usersByGroup = [];

        if (count($users) > 0) {
            foreach($users as $uuid=>$group) {
                $usersByGroup[$group][] = User::getUserInfo($uuid);
            }
        }

        $data = [
            'topLink' => 'games',
            'leftLink' => 'roles',
            'user' => User::getUserInfo($uid),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'image_256x256', 'image_150x150', 'contract']),
            'roles' => $usersByGroup
        ];

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/roles.twig', $data);
    }

    public function status($gid)
    {
        $uid = Auth::getUserId();

        Roles::checkPermission($uid, $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $log = Game::getCheckLog($gid);

        if (count($log) > 1) {
            unset($log[0]);
        }

        $data = [
            'topLink' => 'games',
            'leftLink' => 'status',
            'user' => User::getUserInfo($uid),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'active', 'enabled', 'title', 'image_256x256', 'check_status', 'check_comment', 'dev_last_changes_checked', 'contract']),
            'log' => $log
        ];

        if (Roles::isModerator($uid)) {
            $data['editLog'] = Game::getEditLog($gid);
            if ($data['game']['dev_last_changes_checked'] == 0) {
                Game::setGameDataFieldsById($gid, ['dev_last_changes_checked' => $uid]);
            }
        }

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/status.twig', $data);
    }

    public function help($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $data = [
            'topLink' => 'games',
            'leftLink' => 'help',
            'user' => User::getUserInfo(Auth::getUserId()),
            'game' => Game::getGameDataFieldsById($gid, ['gid', 'title', 'image_256x256', 'contract']),
        ];

        $this->load->library(['Twig']);
        $this->twig->view('dev/game/help.twig', $data);

    }

    private function checkPermisson($gid) {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);
    }
}