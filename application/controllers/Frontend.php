<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.08.15
 * Time: 18:16
 */

use Base\Provider,
    Base\Service\Account,
    Base\Service\Api,
    Base\Service\Auth,
    Base\Service\AuthLog,
    Base\Service\Dialogs,
    Base\Service\Dictionary,
    Base\Service\Events,
    Base\Service\Feeds,
    Base\Service\Friends,
    Base\Service\Game,
    Base\Service\Hash,
    Base\Service\Image,
    Base\Service\Mail,
    Base\Service\Notifications,
    Base\Provider\Payment,
    Base\Service\Payments,
    Base\Service\Promo,
    Base\Service\Redis,
    Base\Service\RedisKeys,
    Base\Service\Roles,
    Base\Service\Stats,
    Base\Service\User,
    Base\Service\UserLog;

defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller
{
    private $allowedSocial = ['vk', 'fb', 'gp', 'ok', 'mm', 'lf'];

    public function bounced()
    {
        $data = file_get_contents('php://input');
        $response = json_decode($data, true);

        if ($response['Type'] != 'Notification') {
            return;
        }

        $message = json_decode($response['Message'], true);

        if ($message['notificationType'] != 'Bounce' && $message['notificationType'] != 'Complaint') {
            \Base\Service\Log::push('amazon', var_export($message, true));
            return;
        }

        if ($message['notificationType'] == 'Complaint') {
            $recipients = $message['complaint']['complainedRecipients'];

            if (count($recipients) > 0) {
                foreach($recipients as $recipient) {
                    $email = $recipient['emailAddress'];
                    \Base\Service\Log::push('amazon_complaint', 'Complaint ' . $email);

                    $uid = User::getUserIdByEmail($email);

                    if ($uid === false) {
                        \Base\Service\Log::push('amazon_complaint', 'User not found ' . $email);
                        continue;
                    }

                    User::setEmailComplaint($uid);
                }
            }

            return;
        }

        $recipients = $message['bounce']['bouncedRecipients'];

        if (count($recipients) > 0) {
            foreach($recipients as $recipient) {
                $email = $recipient['emailAddress'];
                \Base\Service\Log::push('amazon_bounce', 'Bounced ' . $email);

                $uid = User::getUserIdByEmail($email);

                if ($uid === false) {
                    \Base\Service\Log::push('amazon_bounce', 'User not found ' . $email);
                    continue;
                }

                User::setEmailBounced($uid);
            }
        }
    }

    public function robots()
    {
        $this->load->library(['Twig']);

        if (\Base\Service\Project::isDev()) {
            $this->twig->view('dev/robots.twig');
        } else {
            $this->twig->view('robots.twig');
        }
    }

    public function remont()
    {
        $this->load->library(['Twig']);
        $this->twig->view('remont.twig');
    }

    public function remontAll()
    {
        $this->load->library(['Twig']);
        $this->twig->view('remont_all.twig');
    }

    public function blocked()
    {
        if (Auth::isAuth()) {
            Auth::logout();
        }

        $this->load->library(['Twig']);
        $this->twig->view('blocked.twig');
    }

    public function rules() {
        $this->load->library(['Twig']);
        $this->twig->view('rules.twig');
    }

    public function devdoc() {
        $this->load->library(['Twig']);
        $this->twig->view('devdoc.twig');
    }

    public function agreement() {
        $this->load->library(['Twig']);
        $this->twig->view('agreement.twig');
    }

    public function changeEmail()
    {
        $oldEmail = $this->input->get('email');
        $token = $this->input->get('token');
        $code = $this->input->get('code');

        if (Auth::isTokenExists($oldEmail, $token)) {
            $redisKey = 'change_email::wait_confirm::' . $code;
            $newEmail = Redis::get($redisKey);
            Redis::del($redisKey);

            $this->load->helper('url');
            if ($newEmail !== false) {
                $userId = User::getUserIdByEmail($oldEmail);
                $anotherUserId = User::getUserIdByEmail($newEmail);

                if ($anotherUserId === false) { // Другого пользователя не существует, меняем этого
                    User::setUserEmail($userId, $newEmail);
                    Auth::loginAsUser($userId);

                    $this->load->library(['Twig']);
                    if (User::isSocialEmail($oldEmail)) {
                        $password = Hash::getRandomChars(12);
                        User::setUserPasswordByEmail($newEmail, $password);

                        $mailContent = $this->twig->render('letters/new_registration_data.twig', ['email' => $newEmail, 'password' => $password, 'site' => base_url()]);
                        Mail::send($newEmail, 'Обновленные данные Вашей учетной записи на сайте EXE.ru', $mailContent);
                    } else {
                        $mailContent = $this->twig->render('letters/new_change_email_to_old_email.twig', ['email' => $newEmail, 'site' => base_url()]);
                        Mail::send($oldEmail, 'Смена e-mail на сайте EXE.ru', $mailContent);
                    }
                } else { // Перевязываем социальный аккаунт, авторизуемся под другого пользователя
                    User::assignUidWithSocialId($anotherUserId, $oldEmail);
                    Auth::loginAsUser($anotherUserId);
                }

                User::setEmailChecked(Auth::getUserId());
            }
        }

        redirect(base_url('/', 'http'));
    }

    public function confirm()
    {
        $data = [
            'email' =>  $this->input->get('email'),
            'token' => $this->input->get('token')
        ];

        if ($data['email'] != false && $data['token'] != false
            && mb_strlen($data['email']) >= 4 && mb_strpos($data['email'], '@') !== false
            && mb_strlen($data['token']) == 32 && Auth::isTokenExists($data['email'], $data['token'])) {

            $uid = User::getUserIdByEmail($data['email']);
            User::setEmailChecked($uid);

            Auth::loginAsUser($uid);
        }

        $this->load->helper(['url']);
        redirect(base_url('/', 'http'));
    }

    public function auth($type, $popup = false)
    {
        if (!in_array($type, $this->allowedSocial)) {
            $this->load->helper('url');
            redirect(base_url('/', 'http'));
        }

        $code = $this->input->get('code');
        $error = $this->input->get('error');

        $this->load->helper('url');

        $callback = base_url('auth_' . $type . ($popup == false ? '' : '/popup'));
        $provider = Provider\Auth::get($type);

        if (!empty($error)) {
            if (!isset($this->session)) {
                $this->load->library('session', 'core');
            }
            $target = $this->session->flashdata('target_url');
            session_write_close();

            if (empty($target)) {
                $target = '/';
            }

            if ($popup) {
                $this->load->library('Twig');
                return $this->twig->view('user/js/window-close-simple.twig', ['target' => base_url($target)]);
            } else {
                redirect($target);
            }
        } elseif ($code === null) {
            $target = (string) $this->input->get('target_url');
            $destinationId = (int) $this->input->get('destId');
            $playBefore = (int) $this->input->get('play_before');

            if (preg_match('|^\/[a-z0-9\?\=]+$|', $target)) {
                if (!isset($this->session)) {
                    $this->load->library('session', 'core');
                }
                $this->session->set_flashdata('target_url', $target);
            }

            if ($playBefore > 0) {
                if (!isset($this->session)) {
                    $this->load->library('session', 'core');
                }

                $this->session->set_flashdata('play_before', $playBefore);
            }

            if ($destinationId > 0) {
                if (!isset($this->session)) {
                    $this->load->library('session', 'core');
                }

                switch(true) {
                    case $destinationId == AuthLog::DESTINATION_MAIN_PAGE_POPUP_LOGIN: {
                        $destinationId = AuthLog::DESTINATION_MAIN_PAGE_POPUP_LOGIN_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_GAME_PAGE_GREY_POPUP_LOGIN: {
                        $destinationId = AuthLog::DESTINATION_GAME_PAGE_GREY_POPUP_LOGIN_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_GAME_PAGE_TOP_MENU_POPUP_LOGIN: {
                        $destinationId = AuthLog::DESTINATION_GAME_PAGE_TOP_MENU_POPUP_LOGIN_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_GAME_PAGE_BALANCE_POPUP_LOGIN: {
                        $destinationId = AuthLog::DESTINATION_GAME_PAGE_BALANCE_POPUP_LOGIN_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_MAIN_PAGE_CATALOG_POPUP_LOGIN_POPUP: {
                        $destinationId = AuthLog::DESTINATION_MAIN_PAGE_CATALOG_POPUP_LOGIN_POPUP_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_GAME_PAGE_POPUP_LOGIN_POPUP: {
                        $destinationId = AuthLog::DESTINATION_GAME_PAGE_POPUP_LOGIN_POPUP_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_GAME_PAGE_RETENTION_POPUP_LOGIN_POPUP: {
                        $destinationId = AuthLog::DESTINATION_GAME_PAGE_RETENTION_POPUP_LOGIN_POPUP_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_GAME_PAGE_LEFT_BLOCK_LOGIN_POPUP: {
                        $destinationId = AuthLog::DESTINATION_GAME_PAGE_LEFT_BLOCK_LOGIN_POPUP_SOCIAL;
                        break;
                    }
                    case $destinationId == AuthLog::DESTINATION_GAME_PAGE_WHO_PLAY: {
                        $destinationId = AuthLog::DESTINATION_GAME_PAGE_WHO_PLAY_SOCIAL;
                        break;
                    }
                }

                $this->session->set_flashdata('destination_id', $destinationId);
            }

            if (isset($this->session)) {
                session_write_close();
            }

            $loginUrl = $provider->getLoginUrl($callback, $popup);
            redirect($loginUrl);
        } else {
            if ($provider->getAccessToken($code, $callback)) {

                $result = $provider->getUserData();

                $socialId = $result['social_id'] . '@' . $result['social_type'];

                // Пробуем найти пользователя по связи социальный идентификатор - идентификатор пользователя
                $uid = User::getUserIdBySocialId($socialId);

                if ($uid === false) {
                    /** Пробуем найти найти по социальному идентификатору в основной базе
                     * Такая ситуация возможна, если пользователь почему-то был удален в таблице с социальными идентификаторами
                     */
                    $uid = User::getUserIdByEmail($socialId);
                    if($uid !== false) { // Свяжем его.
                        User::assignUidWithSocialId($uid, $socialId);
                    }
                }

                if ($uid !== false) { // Зарегистрированный пользователь
                    if (User::getUserStatus($uid) == '0') { // Пользователь заблокирован администратором
                        if ($popup != false) {
                            $this->load->library('Twig');
                            return $this->twig->view('user/js/opener-reload.twig', ['target' => base_url('blocked')]);
                        }

                        redirect(base_url('blocked'));
                    }

                    if (Auth::isAuth() && User::isTemporaryUser(Auth::getUserId())) {
                        User::assignUidWithTempUid($uid, Auth::getUserId());
                    }

                    Auth::loginAsUser($uid);
                    Auth::remember();

                    $this->input->set_cookie('eu', base64_encode($result['social_type']), 31536000);

                    $destinationId = (int) $this->session->flashdata('destination_id');

                    if ($destinationId > 0) {
                        AuthLog::save($uid, 0, $destinationId);
                    }

                    UserLog::save($uid, null, UserLog::AUTH_SOCIAL, $result['social_type'], Auth::getUserMarker());

                    if (isset($result['mail']) && $result['mail'] != '' // Социалка отдала адрес электронной почты
                        && User::getUserEmail($uid) == $result['mail'] // И он совпадает с электронным адресом в таблице пользователей
                        && !User::isEmailChecked($uid)) { // Но не отмечен как проверенный
                        User::setEmailChecked($uid); // Отмечаем, что электронный адрес достоверный
                    }

                    $target = $this->session->flashdata('target_url');
                    session_write_close();
                    if (is_null($target)) {
                        $target = '/';
                    }

                    if ($popup != false) {
                        $this->load->library('Twig');
                        return $this->twig->view('user/js/opener-reload.twig', ['target' => $target]);
                    }

                    redirect(base_url($target));
                }

                if (isset($result['mail']) && $result['mail'] != '') { // Есть электронный адрес, поищем его у нас
                    $uid = User::getUserIdByEmail($result['mail']);
                }

                if ($uid === false) { // Не нашли. То ли у нас его не нет, то ли соц.сеть не отдала адрес электронной почты
                    if (Auth::isAuth() && User::isTemporaryUser(Auth::getUserId())) {
                        $uid = Auth::getUserId();
                        $this->registrateFromSocial($result, $uid); // Временного пользователя регистрируем под этот же идентификатор
                        $userGames = Game::getUserGameIds($uid);
                        if (count($userGames) > 0) {
                            foreach($userGames as $gid) {
                                Game::setGameSettings($uid, $gid, ['invitible' => 1]);
                            }
                        }
                    } else {
                        // Регистрируем нового пользователя
                        $uid = $this->registrateFromSocial($result);
                        // Авторизуемся
                        Auth::loginAsUser($uid);
                        Auth::remember();
                        $this->input->set_cookie('eu', base64_encode($result['social_type']), 31536000);
                    }

                    $target = $this->session->flashdata('target_url');

                    $destinationId = (int) $this->session->flashdata('destination_id');

                    $playBefore = (int) $this->session->flashdata('play_before');

                    if ($playBefore > 0) {
                        $bonus = 10;
                        Account::incrementBalance($uid, $bonus, Payment::PAYMENT_EXE_INTERNAL, $bonus);
                        $balance = User::getUserInfo($uid, true)['balance'];
                        UserLog::save($uid, null, UserLog::BONUS_TO_BALANCE, ['count' => $bonus, 'balance' => $balance]);
                        Events::rise('EVENT_USER_BALANCE_CHANGE_BONUS', [
                            'uids' => [$uid],
                            'message' => json_encode([
                                'type' => 'balance_change_bonus',
                                'message' => [
                                    'type' => 'increment',
                                    'balance' => $balance
                                ]
                            ])
                        ]);

                    }

                    if ($destinationId > 0) {
                        AuthLog::save($uid, 1, $destinationId);
                    }

                    $this->session->set_flashdata('new_user_auth_social', 1);

                    session_write_close();

                    if (is_null($target)) {
                        $target = '/';
                    }

                    if ($popup != false) {
                        $this->load->library('Twig');
                        return $this->twig->view('user/js/opener-reload.twig', ['target' => $target]);
                    }

                    redirect(base_url($target, 'http'));
                } else {
                    // Нашли пользователя по email, который отдан социальной сетью
                    // Ставим отметку, что email был проверен, если необходимо.
                    if (!User::isEmailChecked($uid)) {
                        User::setEmailChecked($uid);
                    }

                    // Связываем социальный идентификатор и идентификатор пользователя у нас
                    User::assignUidWithSocialId($uid, $socialId);

                    if (User::getUserStatus($uid) == '0') { // Пользователь заблокирован администратором
                        redirect(base_url('blocked', 'http'));
                    }

                    if (Auth::isAuth() && User::isTemporaryUser(Auth::getUserId())) {
                        User::assignUidWithTempUid($uid, Auth::getUserId());
                    }

                    Auth::loginAsUser($uid);
                    Auth::remember();

                    $this->input->set_cookie('eu', base64_encode($result['social_type']), 31536000);

                    $target = $this->session->flashdata('target_url');

                    $destinationId = (int) $this->session->flashdata('destination_id');

                    if ($destinationId > 0) {
                        AuthLog::save($uid, 0, $destinationId);
                    }

                    session_write_close();

                    if (is_null($target)) {
                        $target = '/';
                    }

                    if ($popup != false) {
                        $this->load->library('Twig');
                        return $this->twig->view('user/js/opener-reload.twig', ['target' => $target]);
                    }

                    redirect(base_url($target, 'http'));
                }
            }

            if ($popup != false) {
                $this->load->library('Twig');
                return $this->twig->view('user/js/opener-reload.twig', ['target' => '/']);
            }

            redirect(base_url('/', 'http'));
        }
    }

    public function social($type)
    {
        if (!isset($this->session)) {
            $this->load->library('session', 'core');
        }

        $social = $this->session->flashdata('social');
        if (is_null($social)) {
            $this->load->helper('url');
            redirect(base_url('/', 'http'));
        }

        $social = unserialize($social);

        if ($type == 'assign') {
            User::assignUidWithSocialId($social['uid'], $social['socialId']);
            $uid = $social['uid'];
            Auth::loginAsUser($uid);
        } elseif ($type == 'create') {
            // Очищаем email, т.к. у нас в базе точно этот адрес используется (иначе мы бы суда не попали)
            $social['result']['mail'] = '';
            $uid = $this->registrateFromSocial($social['result']);
            Auth::loginAsUser($uid);
        }

        $target = $this->session->flashdata('target_url');
        if (is_null($target)) {
            $target = '/';
        }

        session_write_close();

        $this->load->helper('url');

        redirect(base_url($target, 'http'));
    }

    private function registrateFromSocial($result, $uid = null)
    {
        $socialId = $result['social_id'] . '@' . $result['social_type'];

        // Подготавливаем данные для регистрации:
        $data = [
            'sex' => $result['sex'],
            'name' => $result['first_name'],
            'last_name' => $result['last_name']
        ];

        if (!empty($result['day']) && !empty($result['month']) && !empty($result['year'])) {
            $data['bday'] = (int) $result['month'];
            $result['day'] = (int) $result['day'];

            if ($result['day'] < 10) {
                $data['bday'] .= '0';
            }
            $data['bday'] .= $result['day'];

            $data['byear'] = $result['year'];
        }

        if ($result['country_name'] !== '') {
            // Сначала ищем по русскому полю
            $countries = Dictionary::getList('country', ['country_name_ru' => $result['country_name']]);
            if ($countries === false) {
                $countries = Dictionary::getList('country', ['country_name_en' => $result['country_name']]);
            }
            if ($countries !== false) {
                list($data['country_id'], $data['country']) = each($countries);
            }
        }

        if (isset($data['country_id']) && $result['city_name'] !== '') {
            $cities = Dictionary::getList('city', ['id_country' => $data['country_id'], 'city_name_ru' => $result['city_name']]);
            if ($cities === false) {
                $cities = Dictionary::getList('city', ['id_country' => $data['country_id'], 'city_name_en' => $result['city_name']]);
            }
            if ($cities !== false) {
                list($data['city_id'], $data['city']) = each($cities);
            }
        }

        $mail = $result['mail'] != '' ? $result['mail'] : $socialId;
        $password = Hash::getRandomChars();

        if (is_null($uid)) {
            // Регистрируем нового пользователя
            User::registrate($result['first_name'], $mail, $password);
            // Получаем идентификатор нового пользователя
            $uid = User::getUserIdByAuthData($mail, $password);
            UserLog::save($uid, null, UserLog::REGISTER_SOCIAL, $result['social_type'], Auth::getUserMarker());
        } else {
            $data['verified'] = 1;
            User::setUserEmail($uid, $mail);
            User::setUserPasswordByEmail($mail, $password);
            UserLog::save($uid, null, UserLog::REGISTER_TEMPORARY_SOCIAL, $result['social_type'], Auth::getUserMarker());
        }

        // Если социальная сеть отдала email, проставляем его проверенным
        if ($result['mail'] != '') {
            User::setEmailChecked($uid);
        }

        // Добавляем связь аккаунта в социальной сети с нашим идентификатором
        User::assignUidWithSocialId($uid, $socialId);

        $provider = Provider\Auth::get($result['social_type']);
        $pictureUrl = $provider->getUserPhoto($result);

        if ($pictureUrl != '') {
            $tempFileName = FCPATH . 'assets/uploads/' . md5($pictureUrl . microtime(true));

            $contentPhoto = file_get_contents($pictureUrl);
            file_put_contents($tempFileName, $contentPhoto);

            // Получаем информацию о размере изображения
            list($width, $height) = getimagesize($tempFileName);

            if ($height % 2 == 1) {
                $height--;
            }
            if ($width % 2 == 1) {
                $width--;
            }

            if ($width != $height) {
                if ($width > $height) {
                    $offsetLeft = ($width - $height) / 2;
                    Image::crop($tempFileName, $offsetLeft, 0, $height, $height);
                } else {
                    $offsetTop = ($height - $width) / 2;
                    Image::crop($tempFileName, 0, $offsetTop, $width, $width);
                }
            }

            User::setUserPhoto($uid, $tempFileName);
            unlink($tempFileName);
        }

        // Обновляем данные
        User::setUserInfo($uid, $data);
        User::updateRating($uid);

        return $uid;
    }

    public function myGames()
    {
        Base\Service\Log::profile(__METHOD__);
        Auth::getUserMarker();

        if (!Auth::isAuth()) {
            $this->load->helper(['url']);
            redirect(base_url('/'));
            Base\Service\Log::profile(__METHOD__);
        }

        $userId = Auth::getUserId();

        $data = $this->getUserTotal($userId);

        $data['installed'] = Game::getUserGames($userId);
        $data['catalog'] = Game::getCatalog();

        $data['friendsNotifications'] = $this->getFriendsNotifications($userId);
        $data['gameNotifications'] = $this->getGameNotifications($userId);

        $data['panel'] = $this->getPanelData($userId);
        $data['currentPage'] = 'games';

        if (false !== ($promoId = Promo::userSatisfy('games', $userId))) {
            $data['promo'] = Promo::getViewData('games', $promoId);
        }

        User::getSetOnline($userId);

        Base\Service\Log::profile(__METHOD__ . '::Twig');
        $this->load->library(['Twig']);
        $this->twig->view('user/my_games.twig', $data);
        Base\Service\Log::profile(__METHOD__ . '::Twig');

        Base\Service\Log::profile(__METHOD__);
    }

    public function index()
    {
        Base\Service\Log::profile(__METHOD__);
        Auth::getUserMarker();

        if (Auth::isAuth()) {
            Base\Service\Log::profile(__METHOD__ . '::Auth');
            $userId = Auth::getUserId();

            $data = $this->getUserTotal($userId);

            $data['catalog'] = Game::getCatalog();
            $data['carusel'] = Game::getCarusel();

            $data['panel'] = $this->getPanelData($userId);
            $data['currentPage'] = '';

            User::getSetOnline($userId);

            if (false !== ($promoId = Promo::userSatisfy('index', $userId))) {
                $data['promo'] = Promo::getViewData('index', $promoId);
            }

            Base\Service\Log::profile(__METHOD__ . '::auth::Twig');
            $this->load->library(['Twig']);
            $this->twig->view('user/main.twig', $data);
            Base\Service\Log::profile(__METHOD__ . '::auth::Twig');
            Base\Service\Log::profile(__METHOD__ . '::Auth');
        } else {
            Base\Service\Log::profile(__METHOD__ . '::Guest');

            $evenOdd = date('i') % 2;

            $redisKey = RedisKeys::gameCatalogGuest . ENVIRONMENT .  '::' . $evenOdd . '::html';

            $hash = rand(1, Game::countCachedCatalogGuestHTML);

            $html = Redis::hGet($redisKey, $hash);

            if ($html != false) {
                echo $html;
                Base\Service\Log::profile(__METHOD__ . '::Guest');
                Base\Service\Log::profile(__METHOD__);
                return;
            }

            $data = [
                'games' => Game::getCatalog(),
                'carusel' => Game::getCarusel()
            ];

            Base\Service\Log::profile(__METHOD__ . '::Twig');
            $this->load->library(['Twig']);
            $html = $this->twig->render('quest/games_list.twig', $data);
            Base\Service\Log::profile(__METHOD__ . '::Twig');

            echo $html;
            Base\Service\Log::profile(__METHOD__ . '::Guest');
        }

        Base\Service\Log::profile(__METHOD__);
    }

    private function getPanelData($uid)
    {
        Base\Service\Log::profile(__METHOD__);
        $games = Game::getUserGameIds($uid);

        $friendsByType = Friends::getFriendsByTypeArray($uid);
        $friends = array_merge(array_keys($friendsByType['both']), array_keys($friendsByType['i']));

        $result = [
            'news' => Feeds::getCountFeeds($uid, $games, $friends, Game::getActiveGamesIdWithPermissions($uid)),
            'dialogs' => Dialogs::getCountUnReadDialogs($uid),
            'friends' => Friends::getFriendsMeCount($uid)
        ];

        Base\Service\Log::profile(__METHOD__);

        return $result;
    }

    public function restore()
    {
        $this->load->helper(['url']);

        $data = [
            'email' =>  $this->input->get('email'),
            'token' => $this->input->get('token')
        ];

        if ((Base\Service\Auth::isAuth() && !User::isTemporaryUser(Auth::getUserId()))
            || $data['email'] === false || $data['token'] === false || mb_strlen($data['token']) !== 32
            || false === ($password = Auth::resetPassword($data['email'], $data['token'])) ) {
            redirect(base_url('/', 'http'));
        }

        $this->load->library(['Twig']);
        $mailContent = $this->twig->render('letters/new_restore_step_2.twig', ['password' => $password, 'site' => base_url()]);

        Mail::send($data['email'], 'Пароль на сайте EXE.ru изменен', $mailContent);

        Auth::login($data['email'], $password);

        $this->input->set_cookie('eu', base64_encode('form'), 31536000);

        User::setEmailChecked(Auth::getUserId());

        redirect(base_url('/', 'http'));
    }

    private function game_guest($gid)
    {
        Base\Service\Log::profile(__METHOD__);

        $source = $this->input->get('source');

        if (Auth::isExceededTemporary()) {
            Base\Service\Log::profile(__METHOD__ . '::captcha');
            if (!is_null($source)) {
                if (!isset($this->session)) {
                    $this->load->library('session', 'core');
                }
                $this->session->set_flashdata('source', $source);
                session_write_close();
            }
            $redisKey = RedisKeys::maxTemporaryPage . ENVIRONMENT . '::';

            $html = Redis::hGet($redisKey, $gid);

            if ($html != false) {
                echo $html;
                Base\Service\Log::profile(__METHOD__ . '::captcha');
                Base\Service\Log::profile(__METHOD__);
                return;
            }

            Base\Service\Log::profile(__METHOD__ . '::captcha::Twig');
            $this->load->library(['Twig']);
            $html = $this->twig->render('recaptcha.twig', ['gid' => $gid]);
            Base\Service\Log::profile(__METHOD__ . '::captcha::Twig');

            echo $html;

            Redis::hSet($redisKey, $gid, $html);
            Base\Service\Log::profile(__METHOD__ . '::captcha');
            Base\Service\Log::profile(__METHOD__);
            return true;
        }

        $gameInfo = Game::getGameById($gid);

        if ($gameInfo === false) {
            $this->load->helper('url');

            Base\Service\Log::profile(__METHOD__);

            redirect(base_url('/', 'http'));

            return false;
        }

        $hasAuth = $this->input->cookie('eu');

        if (!empty($hasAuth)) {
            Base\Service\Log::profile(__METHOD__ . '::retention_popup');
            $data = [];
            $hashString = $gid . '::';

            $from = base64_decode($hasAuth);
            if (!empty($from) && in_array($from, $this->allowedSocial)) {
                $data['from'] = $from;
                $hashString .= $from . '::';
            }

            $redisKey = RedisKeys::gameRetentionPage . ENVIRONMENT . '::';

            $html = Redis::hGet($redisKey, md5($hashString));

            if ($html != false) {
                echo $html;
                Base\Service\Log::profile(__METHOD__ . '::retention_popup');
                Base\Service\Log::profile(__METHOD__);
                return;
            }

            $data['game'] = $gameInfo;

            $this->load->library(['Twig']);
            $html = $this->twig->render('quest/game.twig', $data);

            echo $html;

            Redis::hSet($redisKey,  md5($hashString), $html);

            Base\Service\Log::profile(__METHOD__ . '::retention_popup');
            Base\Service\Log::profile(__METHOD__);
            return true;
        }

        User::createNewTemporary();

        Base\Service\Log::profile(__METHOD__);

        return $this->game_auth($gid);
    }

    private function game_auth($gid)
    {
        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Method game_auth (' . $gid . ')' . "\n", FILE_APPEND);
        }

        Base\Service\Log::profile(__METHOD__);

        $forbidden = false;

        switch(true) {
            // Production
            case ((ENVIRONMENT == 'production') && ($gid == 1)): {
                $forbidden = true;
                break;
            }
/*            case ((ENVIRONMENT == 'production') && $gid == 10): {
                if (!Roles::hasAccess(Auth::getUserId(), $gid, Roles::ACCESS_VIEW)) {
                    $forbidden = true;
                }
                break;
            }*/
            // Testing or Development
            case (ENVIRONMENT != 'production' && $gid == 340): {
                $forbidden = true;
                break;
            }
        }

        if ($forbidden) {
            if (defined('AUTH_LOGFILE')) {
                file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'forbidden' . "\n", FILE_APPEND);
            }

            $this->load->helper('url');
            redirect(base_url('/'));
            exit();
        }

        $userId = Auth::getUserId();

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'User uid ' . $userId . "\n", FILE_APPEND);
        }

        $gameInfo = Game::getGameById($gid);
        if ($gameInfo === false) {
            if (Roles::hasAccess($userId, $gid, Roles::ACCESS_VIEW)) {
                $gameInfo = Game::getGameById($gid, true);
            }
            if ($gameInfo === false) {
                if (defined('AUTH_LOGFILE')) {
                    file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'no permissions' . "\n", FILE_APPEND);
                }

                $this->load->helper('url');
                redirect(base_url('/', 'http'));
            }
        }

        if (!is_null($gameInfo['image_16x16'])) {
            $gameInfo['image_16x16_type'] = substr($gameInfo['image_16x16'], strrpos($gameInfo['image_16x16'], '.') + 1);
        }

        if (!is_null($gameInfo['image_64x64'])) {
            $gameInfo['image_64x64_type'] = substr($gameInfo['image_64x64'], strrpos($gameInfo['image_64x64'], '.') + 1);
        }

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Uptime game' . "\n", FILE_APPEND);
        }

        $wasInstalled = Game::uptimeGame($userId, $gid);
        if(!$wasInstalled) {
            UserLog::save($userId, $gid, UserLog::INSTALL_GAME, $gid, Auth::getUserMarker());
        }

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Stats push' . "\n", FILE_APPEND);
        }

        Stats::push($gid, $userId);

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Userlog save' . "\n", FILE_APPEND);
        }

        UserLog::save($userId, $gid, UserLog::START_GAME, $gid, Auth::getUserMarker());

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Get user total' . "\n", FILE_APPEND);
        }

        $data = $this->getUserTotal($userId);

        $gameSettings = Game::getGameSettings($userId, $gid);
        $gameSettings['authKey'] = Game::getAuthKey($userId, $gid);
        $gameSettings['developer'] = Game::getGameDataFieldsById($gid, ['description', 'developer_info']);

        $requestKey = $this->input->get('request_key');
        if (is_null($requestKey)) {
            $requestKey = $this->session->flashdata('request_key');
        }
        if (!is_null($requestKey)) {
            $gameSettings['requestKey'] = $requestKey;
        }

        $data['gameData'] = array_merge($gameInfo, $gameSettings);
        $data['gameData']['frameUrl'] = $_SERVER["SERVER_PORT"] == 443 ? $data['gameData']['https'] : $data['gameData']['http'];

        $data['userData']['gameSessionId'] = $sid = Game::getSessionId($userId, $gid);
        $data['userData']['isTemporaryUser'] = User::isTemporaryUser($userId);

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Is temp user - ' . ($data['userData']['isTemporaryUser'] ? 'true' : 'false') . "\n", FILE_APPEND);
        }

        if ($data['userData']['isTemporaryUser']) { // Смотрим номер сессии пользователя
            $marker = Auth::getUserMarker();
            $sessionNumber = User::getUserSessionNumberByMarker($marker);

            if ($sessionNumber == 1) {
                $data['userData']['retention_modals'] = true;
            }

            if ($sessionNumber == 1 || $sessionNumber % 5 == 0) {
                $data['userData']['needConnect'] = true;
            }
        }



        if (!$wasInstalled) { // Установка игры
            if (defined('AUTH_LOGFILE')) {
                file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Not installed ' . "\n", FILE_APPEND);
            }

            // Отмечаем, что это установка игры у пользователя:
            $data['install'] = 1;

            // У временного пользователя флаг "ищу друзей" снят
            if (User::isTemporaryUser($userId)) {
                Game::setGameSettings($userId, $gid, ['invitible' => 0]);
                $data['gameData']['invitible'] = 0;
            }

            $friends = [];
            // Получаем идентификаторы друзей, и тех, кто отправил нам заявки на дружбу
            if (!User::isTemporaryUser($userId)) {
                if (defined('AUTH_LOGFILE')) {
                    file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'getFriendsByTypeArray ' . "\n", FILE_APPEND);
                }

                $friendsByType = Friends::getFriendsByTypeArray($userId);
                $friends = array_merge(array_keys($friendsByType['both']), array_keys($friendsByType['me']));
            }

            $source = $this->input->get('source');
            $referer = null;

            if (!is_null($source)) {
                $this->load->library('user_agent');
                $referer = $this->agent->referrer();
            }

            if (defined('AUTH_LOGFILE')) {
                file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'EVENT_USER_INSTALL_GAME ' . "\n", FILE_APPEND);
            }

            Events::rise('EVENT_USER_INSTALL_GAME', [
                'uid' => $userId,
                'gid' => $gid,
                'source' => $source,
                'referrer' => $referer,
                'fuids' => $friends,
                'message' => json_encode([
                    'type' => 'panel',
                    'subtype' => 'news',
                    'action' => 'add'
                ])
            ]);
        }

        // Информируем о активности пользователя
        if (!User::isTemporaryUser($userId)) {
            if (defined('AUTH_LOGFILE')) {
                file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Api accept user ' . "\n", FILE_APPEND);
            }

            //Api::preloadData($userId, $sid);
            Api::acceptUser($userId, $gid, $sid);
        }

        $data['panel'] = $this->getPanelData($userId);
        $data['currentPage'] = 'app' . $gid;

        if (false !== ($promoId = Promo::userSatisfy('game', $userId))) {
            $data['promo'] = Promo::getViewData('game', $promoId);
        }

        $retentionLabel = User::getRetentionLabel(1303195, time());
        if ($retentionLabel !== false) {
            $data['retention_label'] = 'retention_' . $retentionLabel;
        }

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'SetOnline - ' . "\n", FILE_APPEND);
        }

        User::getSetOnline($userId);

        Base\Service\Log::profile(__METHOD__ . '::Twig');

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Render ' . "\n", FILE_APPEND);
        }

        $this->load->library(['Twig']);
        $this->twig->view('user/game_login_left.twig', $data);
        Base\Service\Log::profile(__METHOD__ . '::Twig');

        Base\Service\Log::profile(__METHOD__);

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Method game_auth finished' . "\n", FILE_APPEND);
        }

        return true;
    }

    public function game($gid)
    {
        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'Method game (' . $gid . ')' . "\n", FILE_APPEND);
        }

        Base\Service\Log::profile(__METHOD__);

        Auth::getUserMarker();

        if (Auth::isAuth()) {
            if (defined('AUTH_LOGFILE')) {
                file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'User is auth, uid ' . Auth::getUserId() . "\n", FILE_APPEND);
            }

            Base\Service\Log::profile(__METHOD__);
            return $this ->game_auth($gid);
        }

        if (defined('AUTH_LOGFILE')) {
            file_put_contents(AUTH_LOGFILE, date('Y-m-d H:i:s ') . microtime(true) . ': ' . 'User is guest' . "\n", FILE_APPEND);
        }

        Base\Service\Log::profile(__METHOD__);
        return $this->game_guest($gid);
    }

    public function logout()
    {
        $uid = Auth::getUserId();
        if ($uid > 0) {
            UserLog::save($uid, null, UserLog::LOGOUT, null, Auth::getUserMarker());
        }

        Auth::logout();

        $this->load->helper('url');
        redirect(base_url('/', 'http'));
    }

    private function getFriendsNotifications($userId)
    {
        $notifications = Friends::getNotifications($userId);

        // TODO Не нравится мне такой подход к формированию ссылки, переделать.
        $link = '<a href="#" class="%class%" data-gid="%gid%" data-nid="%nid%">%title%</a>';

        $activeGamesId = Game::getActiveGamesIdWithPermissions($userId);

        $friendsNotifications = [];
        foreach($notifications as $notify) {
            if (!isset($activeGamesId[$notify['gid']])) {
                continue;
            }
            $notification = [
                'user' => User::getUserInfo($notify['fid']),
                'game' => Game::getGameById($notify['gid'], true)
            ];
            $notification['game']['nid'] = $notify['nid'];

            if (!is_null($notify['params'])) {
                $notification['game']['requestKey'] = $notify['params'];
            }

            if (!is_null($notify['request_type'])) {
                $notification['request_type'] = $notify['request_type'];
            }

            $notification['link'] = str_replace([
                '%class%', '%gid%', '%nid%', '%title%'
            ], [
                'friend-request--game_link',
                $notification['game']['gid'],
                $notification['game']['nid'],
                $notification['game']['title']
            ], $link);

            $notification['text'] = $notify['content'];

            $friendsNotifications[$notify['nid']] = $notification;
        }

        // Сортируем по времени создания в обратном порядке
        krsort($friendsNotifications, SORT_NUMERIC);

        return $friendsNotifications;
    }

    private function getGameNotifications($userId) {
        $notifications = Notifications::getGameNotifications($userId);

        $activeGamesId = Game::getActiveGamesIdWithPermissions($userId);
        $gameNotifications = [];
        foreach($notifications as $notify) {
            if (!isset($activeGamesId[$notify['gid']])) {
                continue;
            }
            $game = Game::getGameById($notify['gid'], true);

            if ($game !== false) {
                $gameNotifications[$notify['nid']]['game'] = $game;
                $gameNotifications[$notify['nid']]['game']['nid'] = $notify['nid'];
                $gameNotifications[$notify['nid']]['game']['text'] = $notify['text'];
            }
        }

        // Сортируем по времени создания в обратном порядке
        krsort($gameNotifications, SORT_NUMERIC);

        return $gameNotifications;
    }

    private function getUserTotal($userId)
    {
        Base\Service\Log::profile(__METHOD__);
        $data = [];
        $data['userData'] = User::getUserInfo($userId, true);
        $data['userData']['email'] = User::getUserEmail($userId);
        $data['userData']['isSocialEmail'] = User::isSocialEmail($data['userData']['email']);
        $data['userData']['isTemporaryUser'] = User::isTemporaryUser($userId);
        $data['userData']['isEmailChecked'] = User::isTemporaryUser($userId) || User::isEmailChecked($userId) || User::isSocialEmail($data['userData']['email']);

        $data['userData']['sessionId'] = User::getSessionId($userId);

        $newUserAuthSocial = (int) $this->session->flashdata('new_user_auth_social');
        $data['userData']['newUserAuthSocial'] = $newUserAuthSocial;

        if (!is_null($data['userData']['bday'])) {
            $data['userData']['bday'] = (string) $data['userData']['bday'];
            if (strlen($data['userData']['bday']) < 4) {
                $data['userData']['bday'] = '0' . $data['userData']['bday'];
            }

            $data['userData']['bmonth'] = $data['userData']['bday'][0] . $data['userData']['bday'][1];
            $data['userData']['bday'] = $data['userData']['bday'][2] . $data['userData']['bday'][3];
        } else {
            $data['userData']['bmonth'] = '0';
            $data['userData']['bday'] = '0';
        }

        if (is_null($data['userData']['byear'])) {
            $data['userData']['byear'] = '0';
        }

        $data['months'] = [
            '01' => 'Января',
            '02' => 'Февраля',
            '03' => 'Марта',
            '04' => 'Апреля',
            '05' => 'Мая',
            '06' => 'Июня',
            '07' => 'Июля',
            '08' => 'Августа',
            '09' => 'Сентября',
            '10' => 'Октября',
            '11' => 'Ноября',
            '12' => 'Декабря'
        ];

/*        $countries = []; //Dictionary::getList('country');
        $russia = [219 => $countries[219]];
        unset($countries[219]);*/

        $data['countries'] = []; //$russia + $countries;

        if ($data['userData']['country_id'] != '') {
            $data['cities'] = []; //Dictionary::getList('city', ['id_country' => $data['userData']['country_id']]);
/*            if ($data['userData']['country_id'] == 219) {
                $msk = [17849 => $data['cities'][17849]];
                $spb = [18026 => $data['cities'][18026]];
                unset($data['cities'][17849], $data['cities'][18026]);
                $data['cities'] = $msk + $spb + $data['cities'];
            }*/
        }

        $data['payments'] = Payments::getRates();
        $data['payments_history'] = Account::getHistory($userId);

        $data['csrf'] = [
            'name' => $this->security->get_csrf_token_name(),
            'value' => $this->security->get_csrf_hash()
        ];

        Base\Service\Log::profile(__METHOD__);
        return $data;
    }
}