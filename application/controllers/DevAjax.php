<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.03.16
 * Time: 14:18
 */

use Base\Service\Auth,
    Base\Service\Contract,
    Base\Service\Feeds,
    Base\Service\Game,
    Base\Service\Roles,
    Base\Service\User;

defined('BASEPATH') OR exit('No direct script access allowed');

class DevAjax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        session_write_close();

        if (!Auth::isAuth() || User::isTemporaryUser(Auth::getUserId())) {
            Roles::forbidden();
        }
    }

    public function emails($send = null)
    {
        if (!Auth::isAuth() || !Roles::isModerator(Auth::getUserId())) {
            Roles::forbidden();
        }

        set_time_limit(0);

        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $gid = $this->input->post('gid');
        $testemail = $this->input->post('testemail');
        $subject = $this->input->post('subject');
        $template = $this->input->post('template');

        $emails = User::getVerifiedEmails($start, $end, $gid);

        if (empty($send)) {
            \Base\Service\Mail::send($testemail, $subject, $template);

            return $this->view([
                'text' => 'Количество адресов ' . count($emails). '. Тестовое письмо отправлено.'
            ]);
        }

        $data = [
            'emails' => $emails,
            'subject' => $subject,
            'template' => $template
        ];

        \Base\Service\Redis::rPush(\Base\Service\RedisKeys::mailingQueue, serialize($data));

        $this->view([
            'text' => implode(', ', $emails)
        ]);
    }

    public function contractApplications($cid)
    {
        $uid = Auth::getUserId();

        $gid = (int) $this->input->get_post('gid');
        $sum = (int) $this->input->get_post('cashout_sum');

        if (!Roles::hasAccess($uid, $gid, Roles::ACCESS_CONTRACT)) {
            Roles::forbidden();
        }

        Contract::withdraw($cid, $gid, $uid, $sum);

        return $this->view([
            'location' => '/contract/applications/' . $cid . '?rnd=' . rand()
        ]);
    }

    public function control($gid)
    {
        $action = $this->input->post('action');
        $comment = addslashes(strip_tags($this->input->post('comment')));

        $uid = Auth::getUserId();

        if (!in_array($action, ['accept', 'reject']) || !Roles::isModerator($uid)) {
            Roles::forbidden();
        }

        $gameData = Game::getGameDataFieldsById($gid, ['gid', 'active', 'enabled', 'title', 'owner_uid']);

        if ($action == 'accept') {
            Game::pushCheckLog($gid, $uid, true, '');
            Game::setGameDataFieldsById($gid, ['active' => true, 'check_status' => false]);
            $subject = 'Размещение игры «' . $gameData['title'] . '» (id ' . $gid . ') на сайте EXE.ru прошло успешно';
            $template = 'letters/new_game_accept.twig';
        } else {
            Game::pushCheckLog($gid, $uid, false,  $comment);
            Game::setGameDataFieldsById($gid, ['active' => false, 'check_status' => false, 'check_comment' => $comment]);
            $subject = 'Размещение игры «' . $gameData['title'] . '» (id ' . $gid . ') на сайте EXE.ru отклонено';
            $template = 'letters/new_game_reject.twig';
        }

        \Base\Service\Api::gamesChange($gid, (int)($gameData['active'] === 't'), (int)($gameData['enabled'] === 't'));

        $ownerEMail = \Base\Service\User::getUserEmail($gameData['owner_uid']);

        $this->load->helper(['url']);
        $this->load->library('Twig');

        $mailContent =  $this->twig->render($template, ['site' => base_url(), 'gameData' => $gameData, 'comment' => $comment]);

        \Base\Service\Mail::send($ownerEMail, $subject, $mailContent);

        return $this->view([
            'location' => '/status/' . $gid . '?rnd=' . rand()
        ]);
    }

    public function rolesChange($action, $role)
    {
        if (!in_array($action, ['add', 'delete', 'change']) || !in_array($role, ['admin', 'tester'])) {
            Roles::forbidden();
        }

        $gid = (int) $this->input->post('gid');
        $uid = (int) $this->input->post('uid');
        if ($gid < 1 || $uid < 100) {
            Roles::forbidden();
        }

       $game = \Base\Service\Game::getGameById($gid, true);
        if ($game === false) {
            Roles::forbidden();
        }

        $cuid = Auth::getUserId();
        $access = ($role==='admin' ? Roles::ACCESS_APPEND_DELETE_ADMIN : Roles::ACCESS_APPEND_DELETE_TESTER);
        if (!Roles::hasAccess($cuid, $gid, $access)) {
            Roles::forbidden();
        }

        switch($action) {
            case 'add': {
                if (Roles::hasAccess($uid, $gid, Roles::ACCESS_VIEW)) {
                    return $this->view([
                        'response'=>[
                            'errors'=>['Ошибка. Пользователь уже участвует в управлении игрой.']
                        ]
                    ]);
                }

                $group = ($role==='admin' ? Roles::USER_ADMIN : Roles::USER_TESTER);
                Roles::setAccess($uid, $gid, $group);
                Game::resetUserGamesCache($uid);
                break;
            }
            case 'delete': {
                if (!Roles::hasAccess($uid, $gid, Roles::ACCESS_VIEW)) {
                    return $this->view([
                        'response'=>[
                            'errors'=>['Ошибка. Пользователь не участвует в управлении игрой.']
                        ]
                    ]);
                }

                $group = Roles::getUserGroup($uid, $gid);
                // Просят удалить администратора, но пользователь им не является
                if (($role==='admin' && $group !== Roles::USER_ADMIN)
                    || ($role==='tester' // Просят удалить тестировщика, но пользователь не является
                        && $group !== Roles::USER_TESTER // тестировщиком
                        && $group !== Roles::USER_PAYMENT_TESTER // и не является тестировщиком платежей
                    )) {
                    Roles::forbidden();
                }

                Roles::flushAccess($uid, $gid);
                Game::resetUserGamesCache($uid);
                break;
            }
            case 'change': {
                if (!Roles::hasAccess($uid, $gid, Roles::ACCESS_VIEW)) {
                    return $this->view([
                        'response'=>[
                            'errors'=>['Ошибка. Пользователь не участвует в управлении игрой.']
                        ]
                    ]);
                }

                $checked = $this->input->post('checked') === 'true';
                // Проверим, что пользователь действительно является тестировщиком
                $oldGroup = Roles::getUserGroup($uid, $gid);
                $oldRole = (!$checked ? Roles::USER_PAYMENT_TESTER : Roles::USER_TESTER);
                if ($oldGroup !== $oldRole) {
                    // У пользователя уже установлены эти права, ничего не делаем
                    return $this->view([
                        'response'=> [
                            'location' => '/roles/' . $gid . '?rnd=' . rand()
                        ]
                    ]);
                } else {
                    $group = ($checked ? Roles::USER_PAYMENT_TESTER : Roles::USER_TESTER);
                    Roles::setAccess($uid, $gid, $group);
                    Game::resetUserGamesCache($uid);
                }
                break;

            }
        }

        return $this->view([
            'response'=> [
                'location' => '/roles/' . $gid . '?rnd=' . rand()
            ]
        ]);
    }

    public function feedsGetNext()
    {
        $gid = $this->input->get('gid');
        $offset = $this->input->get('offset');

        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $feeds = Feeds::getFeeds(NULL, [$gid], [], Feeds::FEEDS_COUNT_FOR_DEV, $offset);

        return $this->view($feeds);
    }

    public function feedsSearch()
    {
        $gid = $this->input->get('gid');
        $search = $this->input->get('search');

        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        if (mb_strlen($search) < 5) {
            return $this->view([
                'response' => []
            ]);
        }

        $feeds = Feeds::searchFeeds(NULL, $gid, $search);

        return $this->view($feeds);
    }

    private function view($data)
    {
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($data));
        return true;
    }
}