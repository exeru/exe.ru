<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 21.09.15
 * Time: 14:07
 */
class JsApi extends CI_Controller
{
    public function index()
    {
        $user_sid = $this->input->get('user_sid');
        $game_sid = $this->input->get('game_sid');

        $this->load->library('Twig');

        $this->twig->view('user_api.twig', ['user_sid'=>$user_sid, 'game_sid'=>$game_sid]);
    }
}