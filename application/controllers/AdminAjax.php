<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14.09.17
 * Time: 11:24
 */

use Base\Service\Auth,
    Base\Service\Game,
    Base\Service\Graph,
    Base\Service\Roles;

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminAjax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!Auth::isAuth() || !Roles::isModerator(Auth::getUserId())) {
            session_write_close();

            $this->load->helper('url');
            redirect('/');
        }

        session_write_close();
    }

    public function graph()
    {
        $nullGroupName = 'DirectEntry';

        $graphId = (int) $this->input->get_post('graphId');

        switch($graphId) {
            case 1: {
                $data =  Graph::getAdvertByMonth($nullGroupName);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];
                foreach($data['values'] as $groupName => $series) {
                    $group = [
                        'name' => $groupName,
                        'data' => $series,
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 2: {
                $data = Graph::getAdvertAllByMonths($nullGroupName);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['values'] as $groupName => $series) {
                    $group = [
                        'name' => $groupName
                    ];
                    $groupData = [];
                    foreach($series as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 3: {
                $data = Graph::getAdvertAllByDays($nullGroupName);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['values'] as $groupName => $series) {
                    $group = [
                        'name' => $groupName,
                        'data' => $series,
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }

            case 5: {
                $data =  Graph::getNewGamersByMonth();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 6: {
                $data = Graph::getNewGamersAllByMonths();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['values'] as $index => $series) {
                    $group = [
                        'name' => $series['name']
                    ];
                    $groupData = [];
                    foreach($series['values'] as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 7: {
                $data = Graph::getNewGamersAllByDays();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }

            case 9: {
                $data =  Graph::getUsersDurationByMonth(false);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 10: {
                $data = Graph::getUsersDurationAllByMonth(false);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name']
                    ];
                    $groupData = [];
                    foreach($series['values'] as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 11: {
                $data =  Graph::getUsersDurationAllByDays(false);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }

            case 13: {
                $data =  Graph::getPurchasesByMonth();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 14: {
                $data = Graph::getPurchasesAllByMonths();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['values'] as $index => $series) {
                    $group = [
                        'name' => $series['name']
                    ];
                    $groupData = [];
                    foreach($series['values'] as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 15: {
                $data = Graph::getPurchasesAllByDays();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }

            case 17: {
                $data =  Graph::getBalanceByMonth();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 18: {
                $data = Graph::getBalanceAllByMonths();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name']
                    ];
                    $groupData = [];
                    foreach($series['values'] as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 19: {
                $data =  Graph::getBalanceAllByDays();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }

            case 21: {
                $data =  Graph::getPaySystemsByMonth();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 22: {
                $data = Graph::getPaySystemsAllByMonths();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name']
                    ];
                    $groupData = [];
                    foreach($series['values'] as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 23: {
                $data =  Graph::getPaySystemsAllByDays();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }

            case 25: {
                $data =  Graph::getRegistrationsByMonth();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 26: {
                $data = Graph::getRegistrationsAllByMonths();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name']
                    ];
                    $groupData = [];
                    foreach($series['values'] as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 27: {
                $data =  Graph::getRegistrationsAllByDays();

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }

            case 29: {
                $data =  Graph::getUsersDurationByMonth(true);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 30: {
                $data = Graph::getUsersDurationAllByMonth(true);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end'])
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name']
                    ];
                    $groupData = [];
                    foreach($series['values'] as $yyyymmdd => $value) {
                        $groupData[] = [strtotime(substr($yyyymmdd, 0, 4) . '-' . substr($yyyymmdd, 4, 2). '-' . substr($yyyymmdd, 6, 2) . ' 03:00:00') * 1000, (int) $value];
                    }
                    $group['data'] = $groupData;
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
            case 31: {
                $data =  Graph::getUsersDurationAllByDays(true);

                $result = [
                    'start' => date('d.m.Y', $data['start']),
                    'end' => date('d.m.Y', $data['end']),
                    'xRange' => 30 * 86400 * 1000
                ];

                foreach($data['series'] as $groupName => $series) {
                    $group = [
                        'name' => $series['name'],
                        'data' => $series['values'],
                        'pointInterval' => 86400 * 1000,
                        'pointStart' => ($data['start'] + 3 * 3600) * 1000
                    ];
                    $result['series'][] = $group;
                }

                return $this->view($result);
            }
        }
    }

    public function carusel()
    {
        $gid = $this->input->post('gid');
        $set = $this->input->post('set') === 'true';

        $data = Game::getCaruselData();

        foreach($data as $index => $row) {
            if ($row['gid'] == $gid && $set != $row['enabled']) {
                $data[$index]['enabled'] = $set;
                Game::setCaruselData($data);
                break;
            }
        }

        return $this->view([
            'gid' => $gid,
            'state' => $set
        ]);
    }

    private function view($data)
    {
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($data));
        return true;
    }
}