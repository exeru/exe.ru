<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06.04.16
 * Time: 12:08
 */

use Base\Service\Auth,
    Base\Service\GReCaptcha,
    Base\Service\Mail;

defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends CI_Controller
{
    public function send()
    {
        if (!Auth::isAuth()) {
            $captcha = $this->getFromPostData(['g-recaptcha-response' => 'captcha'])['captcha'];
            if (is_null($captcha) || !GReCaptcha::check($captcha)) {
                if ($this->input->is_ajax_request()) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(['result'=>'renew']));
                    return;
                } else {
                    $this->load->helper(['url']);
                    redirect(base_url('/', 'http'), 'get', 302);
                }
            }
        }

        $uid = Auth::isAuth() ? Auth::getUserId() : 'Anonymous';

        $data = $this->getFromPostData([
            'subject' => 'subject',
            'email' => 'email',
            'name' => 'name',
            'question' => 'question',
        ]);

        $subject = $data['subject'];
        $body =
            'User ID: ' . $uid . "\n" .
            'Name: ' . $data['name'] . "\n" .
            'Email: ' . $data['email'] . "\n" .
            'Question: ' . $data['question'];

        if (in_array($subject, [
            'Ошибка в работе сайта', 'Регистрация/авторизация', 'Предложения/пожелания', 'Предложение сотрудничества', 'Другие вопросы и комментарии '
        ])) {
            Mail::simple($subject, $body);
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode(['result'=>true]));
    }

    private function getFromPostData($fields)
    {
        $data = [];

        foreach($fields as $key=>$field) {
            if (is_numeric($key)) {
                continue;
            }

            $item = $this->input->post($key);

            if (is_null($item)) {
                continue;
            }

            $data[$field] = $item;
        }

        return $data;
    }
}