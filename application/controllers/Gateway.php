<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23.12.15
 * Time: 11:36
 */

use Base\Service\Payments,
    Base\Provider\Payment\Yandex,
    Base\Provider\Payment\Paymaster,
    Base\Provider\Payment\QIWI,
    Base\Provider\Payment\Paypal,
    Base\Service\UserLog;

defined('BASEPATH') OR exit('No direct script access allowed');

class Gateway extends CI_Controller
{
    public function viber()
    {
        log_message('info', var_export($this->input->get(), true));
        log_message('info', var_export($this->input->post(), true));
        log_message('info', var_export($_SERVER, true));
        log_message('info', var_export(file_get_contents('php://input'), true));
    }

    public function paypal()
    {
        log_message('debug', file_get_contents('php://input'));
        log_message('debug', var_export($this->input->get(), true));
        log_message('debug', var_export($this->input->post(), true));
        // Сначала посмотрим PDT
        $getData = $this->input->get();
        $params = Paypal::getDataFromPDT($getData);

        if ($params === false) {
            // Смотрим, возможно это IPN
            $input = file_get_contents('php://input');
            $params = Paypal::getDataFromIPN($input);
        }

        if ($params && isset($params['txn_type']) && $params['txn_type'] == 'web_accept'
            && isset($params['payment_status']) && $params['payment_status'] == 'Completed') {

            if (Paypal::isSignValid($params)) {
                if ($params['charset'] != 'UTF-8') {
                    foreach($params as &$item) {
                        $fromCharset = ($params['charset'] == 'KOI8_R' ? 'KOI8-R' : $params['charset']);
                        $item = mb_convert_encoding($item, 'UTF-8', $fromCharset);
                    }
                }

                $paymentData = Paypal::getPaymentDataFromParams($params);
                $status = Payments::checkAndUpdatePayment($paymentData);

                if ($status === Payments::PAYMENT_STATUS_CHECKED) {
                    Payments::saveRequest($paymentData['uid'], Payments::checkPayment, $params, $paymentData['orderId'], Payments::PAYMENT_STATUS_ACCEPTED, 'Completed');
                    $status = Payments::checkAndAcceptPayment($paymentData);
                    if ($status === Payments::PAYMENT_STATUS_ACCEPTED) {
                        Payments::saveRequest($paymentData['uid'], Payments::acceptPayment, $params, $paymentData['orderId'], Payments::PAYMENT_STATUS_ACCEPTED, 'Completed');
                    }
                }
            }
        }

        $this->load->library(['Twig']);
        $this->twig->view('user/js/window-close.twig');

        return true;
    }

    public function qiwi()
    {
        log_message('info', var_export($this->input->get(), true));
        log_message('info', var_export($this->input->post(), true));
        log_message('info', var_export($_SERVER, true));

        $params = $this->input->post();

        if (!isset($params['command']) || $params['command'] != 'bill') {
            $this->load->library(['Twig']);
            $this->twig->view('user/js/window-close.twig');

            return true;
        }

        if (!QIWI::isSignValid($params)) {
            $xmlString = QIWI::getResponseText(151);
        } elseif (!isset($params['status']) || !in_array($params['status'], ['waiting', 'paid', 'rejected', 'unpaid', 'expired'])) {
            $xmlString = QIWI::getResponseText(5);
        } else {
            $paymentData = QIWI::getPaymentDataFromParams($params);
            $xmlString = QIWI::getResponseText(0);

            switch($params['status']) {
                case 'waiting': {
                    // Ничего не делаем, т.к. у qiwi нет проверки заказа. Будем маркировать как проверенный сразу при оплате
                    break;
                }
                case 'paid': {
                    $status = Payments::checkAndUpdatePayment($paymentData);

                    if ($status === false) {

                    } else {
                        $status = Payments::checkAndAcceptPayment($paymentData);
                        $uid = Payments::getPaymentsDataByIds([$paymentData['orderId']], ['uid']);
                        $paymentData['uid'] = $uid[0]['uid'];
                        Payments::saveRequest($paymentData['uid'], Payments::acceptPayment, $params, $paymentData['orderId'], Payments::PAYMENT_STATUS_ACCEPTED, $xmlString);

                        if ($status === false) {

                        } else {

                        }
                    }

                    break;
                }
                case 'rejected':
                case 'unpaid':
                case 'expired': {

                    break;
                }
            }
        }

        log_message('info', var_export($xmlString, true));

        $this->output->set_header('Content-Type: text/xml');
        $this->output->set_output($xmlString);

        return true;
    }

    public function paymaster()
    {
        log_message('info', var_export($this->input->get(), true));
        log_message('info', var_export($this->input->post(), true));

        $params = $this->input->post();

        if (isset($params['LMI_PREREQUEST']) && 1 === (int) $params['LMI_PREREQUEST']) { // Проверка заказа
            $paymentData = Paymaster::getPaymentDataFromParams($params);

            $status = Payments::checkAndUpdatePayment($paymentData);

            if ($status === false) {
                $response = 'NO';
            } else {
                $response = 'YES';
            }

            if ($status === Payments::PAYMENT_STATUS_CHECKED) {
                // Сохраняем запрос с параметрами
                Payments::saveRequest($paymentData['uid'], Payments::checkPayment, $params, $paymentData['orderId'], Payments::PAYMENT_STATUS_CHECKED, $response);
            }

        } elseif (isset($params['LMI_SYS_PAYMENT_ID']) && isset($params['LMI_HASH'])) {
            if (!Paymaster::isSignValid($params)) { // Подпись не сходится
                $response = 'NO';
            } else {
                $paymentData = Paymaster::getPaymentDataFromParams($params);

                $status = Payments::checkAndAcceptPayment($paymentData);

                if ($status === false) {
                    $response = 'NO';
                } else {
                    $response = 'YES';
                }

                if ($status === Payments::PAYMENT_STATUS_ACCEPTED) {
                    // Сохраняем запрос с параметрами
                    Payments::saveRequest($paymentData['uid'], Payments::acceptPayment, $params, $paymentData['orderId'], Payments::PAYMENT_STATUS_ACCEPTED, $response);
                }
            }
        } else {
            $data = [];
            if (isset($params['LMI_PAYMENT_NO']) && ($orderId = (int) $params['LMI_PAYMENT_NO']) > 0) {
                $paymentData = Payments::getPaymentsDataByIds([$orderId], ['uid', 'payment_system_id', 'status'])[0];
                if ($paymentData['status'] != Payments::PAYMENT_STATUS_ACCEPTED) {
                    UserLog::save($paymentData['uid'], null, UserLog::RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY, ['system' => $paymentData['payment_system_id']], \Base\Service\Auth::getUserMarker());
                }
                if (\Base\Service\Auth::isAuth()) {
                    $purchaseId = Payments::getPurchaseIdByOrderId($paymentData['uid'], $orderId);
                    $purchase = \Base\Service\Purchases::get($paymentData['uid'], $purchaseId);
                    if ($purchase !== false) {
                        $purchase = unserialize($purchase);
                        $gid = \Base\Service\Game::getGameIdBySid($purchase['sid']);
                        if ($gid > 0) {
                            $data['gid'] = $gid;
                        }
                    }
                }
            }

            $this->load->library(['Twig']);
            $this->twig->view('user/js/window-close.twig', $data);

            return true;
        }

        log_message('info', var_export($response, true));

        $this->output->set_header('Content-Type: text/plain');
        $this->output->set_output($response);

        return true;
    }

    public function yandex()
    {
        log_message('info', var_export($this->input->get(), true));
        log_message('info', var_export($this->input->post(), true));

        $data = [];
        $params = $this->input->get();
        if (isset($params['orderNumber']) && ($orderId = (int) $params['orderNumber']) > 0) {
            $paymentData = Payments::getPaymentsDataByIds([$orderId], ['uid', 'payment_system_id', 'status'])[0];
            if ($paymentData['status'] != Payments::PAYMENT_STATUS_ACCEPTED) {
                UserLog::save($paymentData['uid'], null, UserLog::RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY, ['system' => $paymentData['payment_system_id']], \Base\Service\Auth::getUserMarker());
            }
            if (\Base\Service\Auth::isAuth()) {
                $purchaseId = Payments::getPurchaseIdByOrderId($paymentData['uid'], $orderId);
                $purchase = \Base\Service\Purchases::get($paymentData['uid'], $purchaseId);
                if ($purchase !== false) {
                    $purchase = unserialize($purchase);
                    $gid = \Base\Service\Game::getGameIdBySid($purchase['sid']);
                    if ($gid > 0) {
                        $data['gid'] = $gid;
                    }
                }
            }
        }

        $params = $this->input->post();
        $data['remote_addr'] = $_SERVER['REMOTE_ADDR'];
        if (!isset($params['action'])) {
            $this->load->library(['Twig']);
            $this->twig->view('user/js/window-close.twig', $data);

            return true;
        }

        if (!in_array($params['action'], ['checkOrder', 'paymentAviso'])) { // Действие не входит в набор разрешенных

            $xmlString = Yandex::getResponseText($params, 200);

        } elseif (!Yandex::isSignValid($params)) { // Не сходится подпись

            $xmlString = Yandex::getResponseText($params, 1);

        } elseif ($params['action'] == 'checkOrder') { // Проверка заказа

            $paymentData = Yandex::getPaymentDataFromParams($params);

            $status = Payments::checkAndUpdatePayment($paymentData);

            if ($status === false) {
                $xmlString = Yandex::getResponseText($params, 100);
            } else {
                $xmlString = Yandex::getResponseText($params, 0);
            }

            if ($status === Payments::PAYMENT_STATUS_CHECKED) {
                // Сохраняем запрос с параметрами
                Payments::saveRequest($paymentData['uid'], Payments::checkPayment, $params, $paymentData['orderId'], Payments::PAYMENT_STATUS_CHECKED, $xmlString);
            }

        } elseif ($params['action'] == 'paymentAviso') { // Подтверждение оплаты

            $paymentData = Yandex::getPaymentDataFromParams($params);

            $status = Payments::checkAndAcceptPayment($paymentData);

            if ($status === false) {
                $xmlString = Yandex::getResponseText($params, 200);
            } else {
                $xmlString = Yandex::getResponseText($params, 0);
            }

            if ($status === Payments::PAYMENT_STATUS_ACCEPTED) {
                // Сохраняем запрос с параметрами
                Payments::saveRequest($paymentData['uid'], Payments::acceptPayment, $params, $paymentData['orderId'], Payments::PAYMENT_STATUS_ACCEPTED, $xmlString);
            }
        }

        log_message('info', var_export($xmlString, true));

        $this->output->set_header('Content-Type: application/xml');
        $this->output->set_output($xmlString);

        return true;
    }
}