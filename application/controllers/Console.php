<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 03.08.15
 * Time: 12:28
 */
class Console extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->input->get('pw') !== '123055' && !is_cli()) {
            show_404();
        }

        set_time_limit(0);
    }

    public function addIndexUserlog()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $mainTableName = "userlog";

        $start = strtotime('21-11-2016 00:00:00');
        $end = strtotime(date('d-m-Y 00:00:00'));

        error_reporting(E_ALL);
        ini_set('display_errors', 'on');

        while($start <= $end) {
            $tablename = $mainTableName. '_' . date('Ymd', $start);

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_marker_i"';
            $db->query($query);
            echo $query . ";\n";

            $query = 'CREATE INDEX "' . $tablename . '_marker_i" ON "' . $tablename . '" USING hash("marker")';
            $db->query($query);
            echo $query . ";\n";

            $query = 'ANALYZE VERBOSE "' . $tablename . '";';
            $db->query($query);
            echo $query . "\n";

            $query = 'REINDEX TABLE "' . $tablename . '";';
            $db->query($query);
            echo $query . "\n";

            $start += 86400;
        }

        exit();

        for($i=1; $i<=3; $i++) {
            $dayStart = strtotime(date('d-m-Y 00:00:00', $startToday + ($i * 86400)));
            $nextDayStart = strtotime(date('d-m-Y 00:00:00', $startToday + (($i + 1) * 86400)));

            $tablename = $mainTableName. '_' . date('Ymd', $dayStart);

            $query = 'CREATE TABLE IF NOT EXISTS "' . $tablename . '" () inherits ("' . $mainTableName . '")';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' .$tablename . '" DROP CONSTRAINT IF EXISTS "date_check"';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' . $tablename . '" ADD CONSTRAINT "date_check" CHECK ("time" >= \'' . $dayStart . '\' AND "time" < \'' . $nextDayStart . '\')';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_time_key"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_time_key" ON "' . $tablename . '" USING btree("time")';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_uid_action_i"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_uid_action_i" ON "' . $tablename . '" USING btree("uid", "action")';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_marker_i"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_marker_i" ON "' . $tablename . '" USING hash("marker")';
            $db->query($query);
            echo $query . "\n";
        }
    }

    public function cleanActionFromOldUserlog()
    {
        $needActions = [
            \Base\Service\UserLog::TO_BALANCE, // Пополнил баланс
            \Base\Service\UserLog::PAY_TO_GAME, // Заплатил в игре
            \Base\Service\UserLog::REGISTER, // Зарегистрировался на сайте
            \Base\Service\UserLog::REGISTER_SOCIAL, // Зарегистрировался через социалку
            \Base\Service\UserLog::REGISTER_TEMPORARY, // Временный пользователь зарегистрировался
            \Base\Service\UserLog::NEW_TEMPORARY, // Новый временный пользователь
            \Base\Service\UserLog::REGISTER_TEMPORARY_SOCIAL, // Временный пользователь зарегистрировался через социалку
            \Base\Service\UserLog::PASSWORD_RESTORE, // Пользователь запросил восстановление пароля
            \Base\Service\UserLog::INSTALL_GAME, // Установил игру
            \Base\Service\UserLog::MODERATOR_TO_BALANCE, // Модератор зачислил платеж
            \Base\Service\UserLog::BEGIN_BUY_ITEM, // Вызов окна покупки в игре
            \Base\Service\UserLog::BUY_ITEM_NO_MONEY, // У пользователя недостаточно средств для покупки
            \Base\Service\UserLog::BUY_ITEM_CANCEL, // Пользователь отказался от покупки
            \Base\Service\UserLog::GO_TO_PAYMENT_SYSTEM, // Пользователь перешел на страницу платежной системы
            \Base\Service\UserLog::BUY_ITEM_NO_MONEY_AFTER_RECHARGE, // Недостаточно средст для покупки после пополнения баланса
            \Base\Service\UserLog::RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY, // Вернулся из платежной системы не выполняя оплату
            \Base\Service\UserLog::RATING_UPDATE, // Обновлен рейтинг
            \Base\Service\UserLog::USER_AUTH_FROM_TEMPORARY, // Зарегистрированный авторизовался из временного аккаунта
            \Base\Service\UserLog::BONUS_TO_BALANCE // Пользователю начислен бонус
        ];

        $ci = & get_instance();
        $postgres = $ci->load->database('production_postgres', true);

        $query = 'DELETE FROM "userlog" WHERE "time" <= ' . (time() - 3 * 30 * 86400) . ' AND "action" NOT IN ('.implode(', ', $needActions).')';

        //var_dump($query);

        $postgres->query($query);

        var_dump($postgres->affected_rows());
    }

    public function cleanOldUserMainMarker()
    {
        $counter = 0;
        $file = fopen(FCPATH . 'mm.txt', 'r');
        while (!feof($file)) {
            $string = trim(fgets($file));
            if (strlen($string) != 32) {
                continue;
            }

            $counter++;
            echo $counter . ':' . $string . "\n";

            $ttl = \Base\Service\Redis::ttl($string, \Base\Service\Redis::__DB_MARKER_BY_MAIN_MARKER);
            if ($ttl > 0) {
                continue;
            }

            $marker = \Base\Service\User::getMarkerByMainMarker($string);
            if ($marker !== false) {
                \Base\Service\User::setMarkerByMainMarker($string, $marker);
            }
        }

        fclose($file);
    }

    public function resetHTMLCache()
    {
        $monthAgo = time() - 30 * 86400;

        $sidRedisKey = '__user::lastActive::sid::';
        $timeRedisKey = '__user::lastActive::time::';
        $timeKeys = \Base\Service\Redis::hKeys($timeRedisKey);

        if (count($timeKeys) > 0) {
            $counter = 0;
            foreach($timeKeys as $uid) {
                $time = \Base\Service\Redis::hGet($timeRedisKey, $uid);
                if ($time < $monthAgo) {
                    echo ++$counter . ": DELETE " . $uid . "\n";
                    \Base\Service\Redis::hDel($timeRedisKey, $uid);
                    \Base\Service\Redis::hDel($sidRedisKey, $uid);
                }
            }
        }
        exit();

        $redisKeys = [
            //\Base\Service\RedisKeys::gameCatalogGuest . '*',
            //\Base\Service\RedisKeys::gameRetentionPage . '*',
            //\Base\Service\RedisKeys::gameCatalogGuest . '*'
            "feedsBlackList::*"
        ];

        foreach($redisKeys as $redisKey) {
            $keys = \Base\Service\Redis::getKeys($redisKey);
            if (count($keys) > 0) {
                foreach($keys as $key) {
                    \Base\Service\Redis::del($key);
                }
            }
        }
  }

    public function decodeOpenStat()
    {
        $this->load->database();

        $db = &$this->db;

        $db->select(['id', 'point']);
        $db->from(\Base\Model\User::TABLE_STATS_REFERER_V2);
        $db->where('"adv_service" IS NULL', null, false);
        $db->where('"point" LIKE \'%_openstat=%\'', null, false);
        $db->limit(1000);

        $result = $db->get()->result_array();

        if (count($result) > 0) {
            foreach($result as $row) {
                $row['point'] = '/app4?_openstat=d2Vjd2VjO3dlY3cgd2Vjd2VjO2N3YyB3Y3djdzvRhNCy0LDQudGG0LDQuQ&lwnwlenv=welcvknwe';
                $openStat = substr($row['point'], strpos($row['point'], '_openstat=') + strlen('_openstat='));
                if (strpos($openStat, '&') !== false) {
                    $openStat = substr($openStat, 0, strpos($openStat, '&'));
                }

                $openStatString = base64_decode($openStat);

                if (preg_match('|^[a-zA-Z0-9:;._ а-яА-Яй]+$|u', $openStatString)) {
                    $parts = explode(';', $openStatString);
                }

                $openStatData = [];
                $openStatData['adv_service']  = isset($parts[0]) ? $parts[0] : null;
                $openStatData['adv_campaign'] = isset($parts[1]) ? $parts[1] : null;
                $openStatData['adv_marker']   = isset($parts[2]) ? $parts[2] : null;
                $openStatData['adv_place']    = isset($parts[3]) ? $parts[3] : null;

                if (is_null($openStatData['adv_service'])) {
                    var_dump($openStatString);
                    var_dump($openStatData);
                    continue;
                }

                $db->update(\Base\Model\User::TABLE_STATS_REFERER_V2, $openStatData, ['id' => $row['id']]);
            }
        }
    }

    public function acceptUser($gid = 15, $count = 500)
    {
        $this->load->database();

        $this->db->select(['uid']);
        $this->db->from('users_ext');
        $this->db->where(['is_bot' => true]);
        $this->db->order_by('RANDOM()', '', false);
        $this->db->limit($count);

        $result = $this->db->get()->result_array();

        $uids = array_column($result, 'uid');

        $imgFolder = FCPATH . 'assets/img/users/';
        $counter = 500;

        require_once(FCPATH . "application/controllers/Frontend.php");
        $frontendController = new Frontend();

        foreach($uids as $uid) {
            $info = Base\Service\User::getUserInfo($uid);
            if ($info['photo']['70'] == '/assets/img/users/no_photo_70.jpg') {
                do {
                    ++$counter;
                    $content = @file_get_contents('http://lostavatar.net/i/'.rand(1, 499).'.jpg');
                    if ($content) {
                        $fullFilePath = $imgFolder . md5($counter . microtime(true)) . '.jpg';
                        file_put_contents($fullFilePath, $content);
                        Base\Service\User::setUserPhoto($uid, $fullFilePath);
                        unlink($fullFilePath);
                        break;
                    }
                } while(!$content);
            }

            \Base\Service\Auth::loginAsUser($uid);

            ob_start();
            @$frontendController->game($gid);
            ob_clean();

            if (ENVIRONMENT != 'production') {
                $sid = \Base\Service\Game::getSessionId($uid, $gid);
                $apiHost = 'http://185.45.144.62';
                $defaultRequest = '/?api_id=1&format=json&rnd='.rand() . '&';

                // Метод пользователя: Users.get
                $urlUsersGet1 = $apiHost . ':80' . $defaultRequest . 'method=getUserInfo&fields=sex,bdate,photo_50&user_ids=' . $uid . '&sid=' . $sid;
                $urlUsersGet2 = $apiHost . ':81' . $defaultRequest . 'method=getUserInfo&fields=sex,bdate,photo_50&user_ids=' . $uid . '&sid=' . $sid;

                // Метод приложения: Users.isAppUser
                $urlIsAppUser1 = $apiHost . ':80' . $defaultRequest . 'method=isAppUser&user_id=' . $uid . '&sid=' . $sid;
                $urlIsAppUser2 = $apiHost . ':81' . $defaultRequest . 'method=isAppUser&user_id=' . $uid . '&sid=' . $sid;

                file_get_contents($urlUsersGet1);
                file_get_contents($urlUsersGet2);

                file_get_contents($urlIsAppUser1);
                file_get_contents($urlIsAppUser2);
            }
        }

        exit();

        // Получаем идентификаторы пользователей, у которых есть пользовательские сессии
        $this->db->select(['DISTINCT "uid"'], false);
        $this->db->from('user_sessions');
        $this->db->where_in('uid', $uids);
        $result = $this->db->get()->result_array();

        $uidsWithUserSessions = array_column($result, 'uid');

        $uidsWithoutUserSessions = array_diff($uids, $uidsWithUserSessions);

        if (count($uidsWithoutUserSessions) > 0) {
            // Формируем массив для вставки данных
            $toInsert = [];
            foreach($uidsWithoutUserSessions as $uid) {
                $toInsert[] = [
                    'sid' => md5($uid . microtime(true)),
                    'uid' => $uid,
                    'last_action' => time()
                ];
            }
            // Вносим информацию в базу
            $this->db->insert_batch('user_sessions', $toInsert);
        }
        // У всех наших пользователей теперь есть идентификаторы сессий, освобождаем память
        unset($uidsWithUserSessions, $uidsWithoutUserSessions);

        // Получаем идентификаторы игровых сессий, у кого они есть.
        $this->db->select(['DISTINCT "uid"'], false);
        $this->db->from('game_sessions');
        $this->db->where(['gid'=>$gid]);
        $this->db->where_in('uid', $uids);
        $result = $this->db->get()->result_array();

        $uidsWithGameSessions = array_column($result, 'uid');

        $uidsWithoutGameSessions = array_diff($uids, $uidsWithGameSessions);

        if (count($uidsWithoutGameSessions) > 0) {
            // Формируем массив для вставки данных
            $toInsert = [];
            foreach($uidsWithoutGameSessions as $uid) {
                $toInsert[] = [
                    'sid' => md5($uid . microtime(true)) . md5($gid . microtime(true)),
                    'uid' => $uid,
                    'gid' => $gid,
                    'time' => time()
                ];
            }
            // Вносим информацию в базу
            $this->db->insert_batch('game_sessions', $toInsert);
        }
        // Игровые сессии теперь тоже есть, освободим память
        unset($uidsWithGameSessions, $uidsWithoutGameSessions);

        $uids = array_flip($uids);

        $imgFolder = FCPATH . 'assets/img/users/';
        $counter = 0;
        foreach($uids as $uid => $empty) {
            $info = Base\Service\User::getUserInfo($uid);
            if ($info['photo']['70'] == '/assets/img/users/no_photo_70.jpg') {
                do {
                    ++$counter;
                    $content = @file_get_contents('http://lostavatar.net/i/'.$counter.'.jpg');
                    if ($content) {
                        $fullFilePath = $imgFolder . md5($counter . microtime(true)) . '.jpg';
                        file_put_contents($fullFilePath, $content);
                        Base\Service\User::setUserPhoto($uid, $fullFilePath);
                        unlink($fullFilePath);
                        break;
                    }
                } while(!$content);
            }

            $this->db->select(['sid']);
            $this->db->from('game_sessions');
            $this->db->where(['uid'=>$uid, 'gid'=>$gid]);
            $this->db->limit(1);

            $uids[$uid] = [
                'uid' => $uid,
                'gid' => $gid,
                'sid' => $this->db->get()->row()->sid
            ];
        }

        $url = 'http://test.exe.ru/acceptUser/';
        $baseParams = [
            'api_id' => 1,
            'format' => 'json'
        ];

/*        foreach($uids as $data) {
            // Проверка...
            $fullUrl = $url . '?' . http_build_query($baseParams + ['user_id'=>$data['uid'], 'change'=>5, 'method' => 'updateUser', 'rnd' => rand()], null, '&');
            $content = file_get_contents($fullUrl);
            echo $fullUrl . ": " . $content . "\n";
        }*/


/*        $counter = 0;
        while(++$counter < 1000) {*/
            foreach($uids as $data) {
                $fullUrl = $url . '?' . http_build_query($baseParams + $data + ['method' => 'acceptUser', 'rnd' => rand()], null, '&');
                $content = file_get_contents($fullUrl);
                //echo $fullUrl . ": " . $content . "\n";
/*            }*/
        }
    }


    // Наполняем таблицы сессий
    public function benchmark_prepare()
    {
        // Нам нужно 10 000 пользователей с запущенными играми и авторизованных на сайте
        $this->db->select(['uid', 'gid']);
        $this->db->from('gamers');
        $this->db->where('uid > ' . rand(10000, 400000));
        $this->db->group_by('uid');
        $this->db->order_by('uid', 'RANDOM');
        $this->db->limit(15000);
        $result = $this->db->get()->result_array();
        $userSessions = $gameSessions = [];
        foreach($result as $gamer) {
            // Генерируем данные
            $userSessId = md5($gamer['uid'] . rand());
            $gamerSessId = $userSessId . md5(rand());

            $userSessions[] = [
                'sid' => $userSessId,
                'uid' => $gamer['uid'],
                'last_action' => time()
            ];
            $gameSessions[] = [
                'sid' => $gamerSessId,
                'uid' => $gamer['uid'],
                'gid' => $gamer['gid'],
                'time' => time()
            ];
        }
        $this->db->insert_batch('user_sessions', $userSessions);
        $this->db->insert_batch('game_sessions', $gameSessions);
        print_r($userSessions);
        print_r($gameSessions);
    }

    // Подготавливаем урлы запросов, кладем в редис
    public function benchmark_prepare_requests($count = 100)
    {

        $this->db->select(['gs.uid', 'gs.gid', 'gs.sid as gsessid', 'us.sid as usessid']);
        $this->db->from('game_sessions as gs');
        $this->db->join('user_sessions as us', 'gs.uid=us.uid', 'left');
        $this->db->limit($count);
        $this->db->order_by('gs.uid', 'RANDOM');
        $result = $this->db->get()->result_array();

        // Методы игрового API
        $apiTemplates = [];
        $defaultRequest = '?api_id=1&format=json&rnd={{rand}}&';

        // Метод пользователя: Users.get
        $apiTemplates['Users.get'] = 'method=getUserInfo&fields=sex,bdate,photo_50&user_ids={{user_ids}}&sid={{usessid}}';
        // Метод пользователя: Friends.get
        $apiTemplates['Friends.get'] = 'method=getFriends&sid={{usessid}}';

        // Метод приложения: Users.isAppUser
        $apiTemplates['Users.isAppUser'] = 'method=isAppUser&user_id={{user_id}}&sid={{gsessid}}';
        // Метод приложения: Friends.getAppUsers
        $apiTemplates['Friends.getAppUsers'] = 'method=getAppUsers&sid={{gsessid}}';
        // Метод приложения: ShowRequestBox
        $apiTemplates['ShowRequestBox'] = 'method=showInvitible&sid={{gsessid}}';
        // Метод приложения: ShowPossibleFriends
        $apiTemplates['ShowPossibleFriends'] = 'method=showPossible&sid={{gsessid}}';

        $userIds = array_column($result, 'uid');

        $redis = new Redis();
        $connection = $redis->connect('localhost', 6379);

        if ($connection) {
            foreach($result as $user) {
                // Выбираем случайных пользователей
                shuffle($userIds);
                // Подготавливаем данные пользователя
                $data = [
                    '{{rand}}' => rand(1000, 9999),
                    '{{usessid}}' => $user['usessid'],
                    '{{gsessid}}' => $user['gsessid'],
                    '{{user_ids}}' => implode(',', array_slice($userIds, 1, rand(3, 10))),
                    '{{user_id}}' => $userIds[0]
                ];
                // Выбираем случайный запрос
                $key = array_rand($apiTemplates);
                $requestUrl = "http://185.45.144.62:80/" . $defaultRequest . $apiTemplates[$key];
                $requestUrl = str_replace(array_keys($data), $data, $requestUrl);
                $redis->rpush('requests', $requestUrl);
            }
            $redis->close();
        }
    }


    public function benchmark_start($count = 100)
    {
        $redis = new Redis();
        $connection = $redis->connect('localhost', 6379);

        if ($connection) {
            $curlInitedArray = [];
            $mh = curl_multi_init();
            for($i=$count; $i>0; $i--) {
                $url = $redis->lPop('requests');
                $curlInitedArray[$i] = curl_init();
                curl_setopt($curlInitedArray[$i], CURLOPT_URL, $url);
                curl_setopt($curlInitedArray[$i], CURLOPT_HEADER, 1);
                curl_setopt($curlInitedArray[$i], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curlInitedArray[$i], CURLOPT_TIMEOUT, 60);
                curl_multi_add_handle($mh, $curlInitedArray[$i]);
            }

            $active = null;
            //запускаем дескрипторы
            $timeStart = microtime(true);
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);

            while ($active && $mrc == CURLM_OK) {
                if (curl_multi_select($mh) == -1) {
                    usleep(100);
                }

                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }

            $time = microtime(true) - $timeStart;

            $ok = $errors = $database = $timeout = 0;
            foreach($curlInitedArray as $i=>$handler) {
                $info = curl_getinfo($curlInitedArray[$i]);
                if ($info['http_code'] == 200) {
                    $ok++;
                } else {
                    if ($info['http_code'] == 0) {
                        $timeout++;
                    } else {
                        $content = curl_multi_getcontent($curlInitedArray[$i]);
                        if (strpos($content, ':"database problem"}') !== false) {
                            $database++;
                        }
                    }
                    $errors++;
                }
                curl_multi_remove_handle($mh, $curlInitedArray[$i]);
                curl_close($curlInitedArray[$i]);
            }
            curl_multi_close($mh);
            var_dump($time . "\t" .$count . "\t" . $ok . "\t" . $errors . "\t" .$database . "\t" . $timeout);
        }
    }


    public function task_2477($offset = 0, $count = 250000, $onePart = 100)
    {
        $users = [];

        // Получаем количество игр и их идентификаторы
        $this->db->select('id');
        $this->db->from('games');
        $this->db->where('status', 1);
        $this->db->where('enabled', 1);
        $result = $this->db->get()->result_array();
        $allGameIdsArray = array_column($result, 'id');

        // Получаем идентификаторы пользователей, с которыми сейчас будем работать
        $this->db->select('uid');
        $this->db->from('users');
        $this->db->where('status', 1);
        $this->db->order_by('uid', 'ASC');
        $this->db->limit($count, $offset);
        $result = $this->db->get()->result_array();
        $usersForParseIdsArray = array_column($result, 'uid');

        // Получаем идентификаторы пользователей для дружбы
        $this->db->select('uid');
        $this->db->from('users');
        $this->db->where('status', 1);
        $this->db->order_by('uid', 'ASC');
        $this->db->limit(1000000, 1000000);
        $result = $this->db->get()->result_array();
        $usersForFriendshipIdsArray = array_column($result, 'uid');

        $parts = ceil($count / $onePart);
        for($i = 0; $i<$parts; $i++) {
            // Берем очередную порцию пользователей
            $currentOffset = $i * $onePart;
            $currentPart = array_slice($usersForParseIdsArray, $currentOffset, $onePart);

            foreach($currentPart as $userUid) {
                $gamesForAppend = $friendsForAppend = [];
                // Игры пользователя
                $this->db->select('gid');
                $this->db->from('gamers');
                $this->db->where('uid', $userUid);
                $result = $this->db->get()->result_array();
                $userGidsArray = array_column($result, 'gid');  // Массив с идентификаторами игр

                // Друзья пользователя
                $this->db->select('fid');
                $this->db->from('friends');
                $this->db->where('uid', $userUid);
                $result = $this->db->get()->result_array();
                $userFidsArray = array_column($result, 'fid'); // Массив с идентификаторами друзей

                // Определяем случайное количество игр и друзей
                $countGamesAndFriends = rand(100, 1000);

                // Если пользователь мало активен, сначала добавим ему игр
                if ((count($userGidsArray) + count($userFidsArray)) < $countGamesAndFriends) {
                    // Игры, в которые пользователь еще не играет
                    $newGames = array_diff($allGameIdsArray, $userGidsArray);
                    $minCountNewGames = ceil(count($newGames) / 2);
                    $countNewGames = rand($minCountNewGames, count($newGames));
                    // Перемешаем идентификаторы игр
                    shuffle($newGames);
                    // Сделаем срез с нужным нам количеством
                    $gamesForAppend = array_slice($newGames, 0, $countNewGames);
                    // Добавим эти игры в массив игр пользователя
                    $userGidsArray = array_merge($userGidsArray, $gamesForAppend);
                }

                // Если количество по прежнему недостаточное, добавим ему друзей:
                if ((count($userGidsArray) + count($userFidsArray)) < $countGamesAndFriends) {
                    // Идентификаторы будущих друзей
                    $newFriends = array_diff($usersForFriendshipIdsArray, $userFidsArray);
                    // Перемешаем
                    shuffle($newFriends);
                    // Делаем срез с нужным количеством
                    $newFriendsCount = $countGamesAndFriends - (count($userGidsArray) + count($userFidsArray));
                    $friendsForAppend = array_slice($newFriends, 0, $newFriendsCount);
                    // Добавляем этих товарищей в массив друзей пользователя
                    $userFidsArray = array_merge($userFidsArray, $friendsForAppend);
                }

                if (count($gamesForAppend) > 0) {
                    // Запрос на добавление игр пользователю
                    $rows = [];
                    foreach($gamesForAppend as $game) {
                        $row = [
                            'uid'           => $userUid,
                            'gid'           => $game,
                            'last_visit'    => 0,
                            'installed'     => rand(0, 1),
                            'notifications' => 0,
                            'invitible'     => rand(0, 1)
                        ];
                        $rows[] = $row;
                    }
                    $this->db->insert_batch('gamers', $rows);
                }

                if (count($friendsForAppend) > 0) {
                    // Запрос на добавление друзей пользователю
                    $rows = [];
                    foreach($friendsForAppend as $friend) {
                        // Определяем, будет ли это встречное добавление, или нет
                        $pair = rand(45, 100);
                        if ($pair > 50) {
                            $row = [
                                'uid' => $userUid,
                                'fid' => $friend
                            ];
                            $rows[] = $row;
                            $row = [
                                'uid' => $friend,
                                'fid' => $userUid
                            ];
                            $rows[] = $row;
                        } else {
                            $row = [
                                'uid' => $userUid,
                                'fid' => $friend
                            ];
                            $rows[] = $row;
                        }

                    }
                    $this->db->insert_batch('friends', $rows);
                }

                echo date('Y/m/d H:i:s =>') . " Uid: " . $userUid . ", appended games - " . count($gamesForAppend) . ", appended friends - " . count($friendsForAppend) . "\n";
            }
            echo date('Y/m/d H:i:s =>') . " part complite, count " . $onePart . "\n";
        }
    }

    public function fillStatsNotifications($count = 1000000, $part = 100)
    {
        $countParts = $count / $part;
        $timeCounter = 50000;
        for($i=$countParts; $i>=1; $i--) {
            $values = [];
            for($j=$part; $j>=1; $j--) {
                $timeCounter ++;
                $row = [
                    'gid' => rand(10, 200),
                    'time' => $timeCounter,
                    'type' => rand(0, 2),
                    'ntype' => rand(0, 15),
                    'allnot' => 0,
                    'filtrnot' => 0,
                    'succesnot' => 0,
                    'failurenot' => 0
                ];
                $values[] = $row;
            }
            $this->db->insert_batch('stats_notifications', $values, false);
        }
    }
}