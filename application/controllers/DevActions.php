<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 05.02.16
 * Time: 12:12
 */

use Base\Service\Auth,
    Base\Service\Contract,
    Base\Service\Feeds,
    Base\Service\File,
    Base\Service\Game,
    Base\Service\GReCaptcha,
    Base\Service\Mail,
    Base\Service\Roles,
    Base\Service\User;

defined('BASEPATH') OR exit('No direct script access allowed');

class DevActions extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        session_write_close();

        if ((!Auth::isAuth() || User::isTemporaryUser(Auth::getUserId())) && $this->router->method != 'help') {
            $this->load->helper(['url']);
            redirect(base_url('/', 'http'), 'get', 302);
            exit();
        }
    }

    public function contractEdit($cid)
    {
        $uid = Auth::getUserId();

        if (!Roles::isModerator($uid)) {
            Roles::forbidden();
        }

        $fields = [
            'number' => 'number',
            'date' => 'date',
            'comission' => 'comission',
            'nds' => 'nds',
            'org_title' => 'org_title',
            'org_address_actual' => 'org_address_actual',
            'org_address_legal' => 'org_address_legal',
            'inn' => 'inn',
            'kpp' => 'kpp',
            'bank_title' => 'bank_title',
            'bik' => 'bik',
            'ks' => 'ks',
            'rs' => 'rs',
            'resident' => 'resident'
        ];

        $data = self::getFromPostData($fields);

        if (!isset($data['resident'])) {
            $data['resident'] = 'f';
        }

        if (is_numeric($cid)) {
            $contract = Contract::getContractById($cid);
        }

        if (!isset($contract) || empty($contract)) {
            $data['id'] = Contract::add($data);

            $location = '/contract/edit/' . $data['id'];
        } else {
            $updated = array_diff_assoc($data, $contract);

            if (count($updated) > 0) {
                Contract::setContractDataById($cid, $updated);
            }

            $location = '/contract/edit/' . $cid;
        }

        $this->load->helper(['url']);
        redirect(base_url($location, 'http'), 'get', 302);
    }

    public function add()
    {   // TODO Валидация
        $action = (int)$this->input->post('action_dub');
        $title = $this->input->post('title');
        $description = $this->input->post('description');

        $gid = Game::addGame(Auth::getUserId(), $action, $title, $description, md5(microtime(true)), 500);

        if ($this->input->is_ajax_request()) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(['location' => '/info/' . $gid]));
        } else {
            $this->load->helper(['url']);
            redirect(base_url('/info/' . $gid, 'http'), 'get', 302);
        }
    }

    public function info($gid)
    {   // Валидация данных

        $uid = Auth::getUserId();

        Roles::checkPermission($uid, $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $location = '/info/' . $gid;

        $copyResult = $this->copyImageFromMain($gid);

        if ($copyResult !== false) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(['response' => $copyResult]));

            return true;
        }

        $uploadResult = $this->uploadFiles($gid);

        if ($uploadResult !== false) {
            $this->output->set_content_type('application/json');
            if ($uploadResult == 'reload') {
                $location .= '?rnd=' . rand();
                $this->output->set_output(json_encode(['location' => $location]));
            } else {
                $this->output->set_output(json_encode(['response' => $uploadResult]));
            }

            return true;
        }

        $fields = [
            'title' => 'title',
            'description' => 'description',
            'action' => 'type',
            'terms' => 'terms',
            'privacy' => 'privacy'
        ];

        if (Roles::isModerator($uid)) {
            $fields['contract'] = 'contract';
            $fields['developerInfo'] = 'developer_info';
            $fields[] = 'owner_uid';
        }

        $gameData = Game::getGameDataFieldsById($gid, array_values($fields));

        if ($gameData === false) {
            // TODO Ошибка, нет такой игры. Придумать и реализовать обработку.
            exit();
        }

        $data = self::getFromPostData($fields);

        foreach (['terms', 'privacy'] as $key) {
            if (isset($data[$key]) && in_array($data[$key], ['http://', 'https://', ''])) {
                $data[$key] = NULL;
            }
        }

        $updated = array_diff_assoc($data, $gameData);

        if (count($updated) > 0) {
            if (isset($updated['contract']) && !empty($updated['contract'])) {
                $gamesByContractId = Game::getGamesByContractId($updated['contract']);
                if (count($gamesByContractId) > 0) {
                    $ownerUids = array_unique(array_column($gamesByContractId, 'owner_uid'));
                    if ($ownerUids[0] != $gameData['owner_uid']) {
                        $this->output->set_content_type('application/json');
                        $this->output->set_output(json_encode(['errors' => ['Ошибка! К данному договору привязаны приложения с другим владельцем.']]));
                        return;
                    }
                }
            }

            Game::setGameDataFieldsById($gid, $updated);

            if (isset($updated['title'])) {
                $location .= '?rnd=' . rand();
            }
        }

        if ($this->input->is_ajax_request()) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(['location' => $location]));
        } else {
            $this->load->helper(['url']);
            redirect(base_url($location, 'http'), 'get', 302);
        }
    }

    public function settings($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $part = (int)$this->input->post('part');

        if ($part === 1) {
            if (!Roles::hasAccess(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_KEY)) {
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode([
                    'errors' => [
                        'Эти настройки может изменять только владелец приложения.'
                    ]
                ]));
                return;
            }

            $fields = [
                'api_key' => 'api_key',
                'enabled' => 'enabled',
                0 => 'active',
                1 => 'repairs'
            ];
        } elseif ($part == 2) {
            $fields = [
                'http' => 'http',
                'https' => 'https',
                'callback_url' => 'callback_url',
                'height' => 'height'
            ];
        } else {
            // TODO Ошибка, значение должно быть. Придумать и реализовать обработку.
            exit();
        }

        $gameData = Game::getGameDataFieldsById($gid, array_values($fields));

        if ($gameData === false) {
            // TODO Ошибка, нет такой игры. Придумать и реализовать обработку.
            exit();
        }

        if (isset($gameData['repairs']) && $gameData['repairs'] == 't') {
            $gameData['enabled'] = 'r';
        }

        $data = self::getFromPostData($fields); // Метод не будет получать значения для числовых индексов

        $updated = array_diff_assoc($data, $gameData);

        if (count($updated) > 0) {
            if (isset($updated['enabled'])) {
                if ($updated['enabled'] == 'r') {
                    $updated['repairs'] = 't';
                    $updated['enabled'] = 't';
                } else {
                    $updated['repairs'] = 'f';
                }
            }
            Game::setGameDataFieldsById($gid, $updated);
            if (isset($updated['enabled'])) {
                \Base\Service\Api::gamesChange($gid, (int)($gameData['active'] === 't'), (int)($updated['enabled'] === 't'));
            }
        }

        if ($this->input->is_ajax_request()) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(['location' => '/settings/' . $gid]));
        } else {
            $this->load->helper(['url']);
            redirect(base_url('/settings/' . $gid, 'http'), 'get', 302);
        }
    }

    public function news($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $location = '/news/' . $gid . '?rnd=' . rand();

        $data = [
            'fid' => NULL,
            'file_state' => NULL,
            'content' => NULL,
            'title' => NULL,
            'params' => NULL,
            'datetimepicker' => NULL
        ];

        foreach($data as $field => $value) {
            $value = trim($this->input->post($field));
            if (!is_null($value) && mb_strlen($value) > 0) {
                $data[$field] = $value;
            }
        }

        if (!is_null($data['content'])) {
            if (!is_null($data['params'])) {
                $data['params'] = rawurlencode($data['params']);
            }

            $config['upload_path']          = './assets/uploads/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png';

            $this->load->library(['upload'], $config);

            if ($this->upload->do_upload('image_url')) {
                $fileData = $this->upload->data();

                if($fileData['is_image'] !== true) {
                    unlink($fileData['full_path']);
                } else {
                    if ($fileData['image_width'] > 610) {
                        $this->resize($fileData['full_path'], 610, 0);
                    } else {
                        $this->resize($fileData['full_path'], $fileData['image_width'], 0);
                    }

                    $dir = realpath('./assets/img/') . '/feed/' . $gid . '/';
                    $filename = md5(microtime(true)) . '.' . ($fileData['image_type'] == 'jpeg' ? 'jpg' : $fileData['image_type']);
                    if (is_dir($dir) || mkdir($dir, 0755, true)) {
                        rename($fileData['full_path'], $dir . $filename);
                        File::upload($dir . $filename);
                        $data['image_url'] = '/assets/img/feed/' . $gid . '/' . $filename;
                    }
                }
            } else {
                $data['image_url'] = NULL;
            }

            if (is_null($data['fid'])) {
                if (!is_null($data['datetimepicker'])) {
                    $appointedTime = strtotime($data['datetimepicker']);
                    Feeds::addPendingFeed($appointedTime, NULL, $gid, 1, $data['content'], NULL, $data['image_url'], $data['params'], $data['title']);
                } else {
                    Feeds::addFeed(NULL, $gid, 1, $data['content'], NULL, $data['image_url'], $data['params'], $data['title']);
                }
            } else {
                $updated = [];
                if (!is_null($data['datetimepicker'])) {
                    $appointedTime = strtotime($data['datetimepicker']);
                    $feed = Feeds::getPendingFeed($data['fid'], $gid);
                    if ($appointedTime != $feed['time']) {
                        $updated['time'] = $appointedTime;
                    }
                } else {
                    $feed = Feeds::getFeed($data['fid'], $gid);
                }

                foreach(['title', 'content', 'params', 'image_url'] as $key) {
                    if (is_null($feed[$key]) && !is_null($data[$key])) { // Поле было пустым, добавили контент
                        $updated[$key] = $data[$key];
                    } elseif (!is_null($feed[$key]) && is_null($data[$key])) { // Был контент, теперь нет - стерли
                        $updated[$key] = $data[$key];
                    } elseif ($feed[$key] != $data[$key]) { // Другое значение
                        $updated[$key] = $data[$key];
                    }
                }

                if (count($updated) > 0) {
                    if (array_key_exists('image_url', $updated) && is_null($updated['image_url']) && $data['file_state'] !== 'detach') {
                        unset($updated['image_url']);
                    }
                }

                if (!is_null($data['datetimepicker'])) {
                    Feeds::updateAppointedFeed($data['fid'], $gid, $updated);
                } else {
                    Feeds::updateGameFeed($data['fid'], $gid, $updated);
                }
            }

        }

        if ($this->input->is_ajax_request()) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(['location'=>$location]));
        } else {
            $this->load->helper(['url']);
            redirect(base_url($location, 'http'), 'get', 302);
        }
    }

    public function deleteFeed()
    {
        $fid = $this->input->post('fid');
        $gid = $this->input->post('gid');
        $appointed = $this->input->post('appointed') == "true";

        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $location = '/news/' . $gid . '?rnd=' . rand();

        if ($appointed && Feeds::deleteAppointedFeed($fid, $gid)) {
            $data = [
                'response'=> [
                    'result' => [
                        'fid' => $fid,
                        'appointed' => true
                    ]
                ]
            ];
        } else if (Feeds::deleteFeed($fid, $gid)) {
            $data = [
                'response'=> [
                    'result' => [
                        'fid' => $fid
                    ]
                ]
            ];
        } else {
            $data = [
                'response' => [
                    'error' => []
                ]
            ];
        }

        if ($this->input->is_ajax_request()) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($data));
        } else {
            $this->load->helper(['url']);
            redirect(base_url($location, 'http'), 'get', 302);
        }
    }

    // TODO Вероятно, надо вынести в другой контроллер, т.к. метод не выполняет изменения данных
    public function getFeed()
    {
        $fid = $this->input->get('fid');
        $gid = $this->input->get('gid');
        $appointed = $this->input->get('appointed') == "true";

        if ($appointed) {
            $feed = Feeds::getPendingFeed($fid, $gid);
            $feed['appointed'] = date('d.m.Y H:i', $feed['time']);
        } else {
            $feed = Feeds::getFeed($fid, $gid);
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode([
            'response'=>[
                'result'=>$feed
            ]
        ]));
    }

    public function status($gid)
    {
        Roles::checkPermission(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS);

        $location = '/status/' . $gid . '?rnd=' . rand();

        Game::setGameDataFieldsById($gid, ['check_status' => TRUE]);

        Mail::simple('Заявка на размещение приложения', 'Подана заявка на размещение игры id ' . $gid);

        if ($this->input->is_ajax_request()) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(['location'=>$location]));
        } else {
            $this->load->helper(['url']);
            redirect(base_url($location, 'http'), 'get', 302);
        }
    }

    public function help($gid)
    {
        if (is_numeric($gid)) {
            $location = '/help/' . $gid;
        } else {
            $location = '/feedback/';
        }

        if (!Auth::isAuth()) {
            $captcha = $this->getFromPostData(['g-recaptcha-response' => 'captcha'])['captcha'];
            if (is_null($captcha) || !GReCaptcha::check($captcha)) {
                if ($this->input->is_ajax_request()) {
                    $this->output->set_content_type('application/json');
                    $this->output->set_output(json_encode(['result'=>'renew']));
                    return;
                } else {
                    $this->load->helper(['url']);
                    redirect(base_url($location, 'http'), 'get', 302);
                }
            }
        }

        $data = $this->getFromPostData([
            'subject' => 'subject',
            'email' => 'email',
            'name' => 'name',
            'question' => 'question',
        ]);

        $uid = Auth::isAuth() ? Auth::getUserId() : 'Anonymous';

        $subject = $data['subject'] . ' question';
        $body =
            'User ID: ' . $uid . "\n" .
            'Name: ' . $data['name'] . "\n" .
            'Email: ' . $data['email'] . "\n" .
            'Question: ' . $data['question'];

        if (in_array($subject, [
            'API question', 'Application question', 'API', 'Application'
        ])) {
            Mail::simple($subject, $body);
        }

        if ($this->input->is_ajax_request()) {
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode(['location'=>$location]));
        } else {
            $this->load->helper(['url']);
            redirect(base_url($location, 'http'), 'get', 302);
        }
    }

    private function getFromPostData($fields)
    {
        $data = [];

        foreach($fields as $key=>$field) {
            if (is_numeric($key)) {
                continue;
            }

            $item = $this->input->post($key);

            if (is_null($item)) {
                continue;
            }

            $data[$field] = $item;
        }

        return $data;
    }

    private function copyImageFromMain($gid)
    {
        $data = $this->getFromPostData(['fileIconType'=>'fileIconType', 'action'=>'action']);

        if (!isset($data['action']) || $data['action'] !== 'copy'
            || !isset($data['fileIconType']) || !in_array($data['fileIconType'], ['64x64', '16x16', '150x150', '80x80'])) {
            return false;
        }

        $mainImage = Game::getGameDataFieldsById($gid, ['image_256x256'])['image_256x256'];
        if (is_null($mainImage)) {
            return false;
        }

        $mainImagePath = rtrim(FCPATH, '/') . $mainImage;

        $mainImageType = substr($mainImagePath, strrpos($mainImagePath, '.') + 1);

        File::putFileLocally($mainImagePath);

        $result = $this->changeGameImage($gid, $data['fileIconType'], $mainImagePath, $mainImageType);

        if (ENVIRONMENT !== 'testing') {
            unlink($mainImagePath);
        }

        return $result;
    }

    private function uploadFiles($gid)
    {
        $squareIconTypes = ['256x256', '64x64', '16x16', '150x150', '80x80'];
        $rectangleIconTypes = ['810x500', 'guest'];

        $fileIconType = $this->input->post('fileIconType');

        if (is_null($fileIconType) || !(in_array($fileIconType, $squareIconTypes) || in_array($fileIconType, $rectangleIconTypes)) ) {
            return false;
        }

        $config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';

        $this->load->library(['upload'], $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            // TODO Обработать ошибки
            var_dump($error);
            exit();
        }

        $data = $this->upload->data();

        if($data['is_image'] !== true) {
            unlink($data['full_path']);
            return false;
        }

        if ($fileIconType != 'guest') {
            list($width, $height) = explode('x', $fileIconType);
        }

        switch(true) {
            case $fileIconType == 'guest': {
                if ($data['image_width'] < 1920 || $data['image_height'] < 1080) {
                    $result = ['error' => 'Слишком маленькое изображение'];
                } else {
                    $result = $this->changeGameImage($gid, $fileIconType, $data['full_path'], $data['image_type']);
                }
                break;
            }
            // Размеры совпадают с указанными
            case ($data['image_width'] < $width || $data['image_height'] < $height): {
                $result = ['error' => 'Слишком маленькое изображение'];
                break;
            }
            case (in_array($fileIconType, $squareIconTypes) && $data['image_width'] != $data['image_height']):
            case (in_array($fileIconType, $rectangleIconTypes) && (((int) ($data['image_width'] * 100 / $data['image_height'])) != 162)): {
                $result = ['error' => 'Пропорции изображения не подходят для изменения размера.'];
                break;
            }
            default: {
                $result = $this->changeGameImage($gid, $fileIconType, $data['full_path'], $data['image_type']);
                if ($result !== false && $fileIconType == '256x256') {
                    $anotherSizes = Game::getGameDataFieldsById($gid, ['image_150x150', 'image_64x64', 'image_16x16', 'image_80x80']);
                    foreach($anotherSizes as $size=>$filename) {
                        if (is_null($filename)) {
                            $this->changeGameImage($gid, substr($size, 6), $data['full_path'], $data['image_type']);
                        }
                    }
                    $result = 'reload';
                }
                break;
            }
        }

        unlink($data['full_path']);

        return $result;
    }

    private function changeGameImage($gid, $fileIconType, $fullImagePath, $imageType)
    {
        $dir = realpath('./assets/img/') . '/app/' . $gid . '/' . $fileIconType . '/';
        $filename = md5(microtime(true)) . '.' . ($imageType == 'jpeg' ? 'jpg' : $imageType);

        if (is_dir($dir) || mkdir($dir, 0755, true)) {
            copy($fullImagePath, $dir . '/' . $filename);
            if (strpos($fileIconType, 'x') !== false) {
                list($width, $height) = explode('x', $fileIconType);
                $this->resize($dir . '/' . $filename, $width, $height);
                // Копируем себе уменьшенную копию
                copy($dir . '/' . $filename, $dir . '/_' . $filename);
                if ($fileIconType != '16x16') {
                    // Уменьшаем изображение
                    $this->resize($dir . '/_' . $filename, $width / 2, $height / 2);
                }

                File::upload($dir . '/_' . $filename);
            }

            File::upload($dir . '/' . $filename);

            Game::setGameDataFieldsById($gid, ['image_' . $fileIconType => $filename]);
            return ['filename' => '/assets/img/app/' .$gid . '/' . $fileIconType . '/' . $filename];
        }

        return false;
    }

    private function resize($path, $width, $height)
    {
        return \Base\Service\Image::resize($path, $width, $height);
    }
}