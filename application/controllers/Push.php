<?php
use
    Base\Service\Auth,
    Base\Service\WebPush;
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.12.17
 * Time: 13:42
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Push extends CI_Controller
{
    public function userstate()
    {
        if (!Auth::isAuth()) {
            return;
        }

        $uid = (int) $this->input->post('uid');
        $state = $this->input->post('permission');

        if ($uid != Auth::getUserId()) {
            return;
        }

        WebPush::setSubscribeState($uid, $state);

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode([]));
        return true;
    }

    public function subscribe()
    {
        if (!Auth::isAuth()) {
            return;
        }

        $type = $this->input->post('type');
        $channel = $this->input->post('channel');
        $show = $this->input->post('show');
        $subscribe = $this->input->post('subscribe');
        $subscribeId = $this->input->post('subscribeId');

        if (!in_array($type, [
            'access_granted', // Разрешил присылать уведомления в браузере, одновременно подписался на канал,
            'access_denied', // Заблокировал все уведомления в браузере
            'subscribe', // Подписался на канал
            'unsubscribe' // Отписался от канала
        ])) {
            return false;
        }

        $uid = Auth::getUserId();

        switch($type) {
            case 'access_granted': {
                $subscribeId = md5($subscribe);

                WebPush::log($uid, $type, $channel, $show, $subscribe, $subscribeId);
                WebPush::subscribe($uid, $subscribe, $subscribeId);
                WebPush::enableChannel($uid, $channel, $subscribeId);

                $this->output->set_header('Content-Type: application/json');
                $this->output->set_output(json_encode(['subscribeId' => $subscribeId]));
                return true;
            }
            case 'unsubscribe': {
                WebPush::log($uid, $type, $channel, $show, $subscribe, $subscribeId);
                WebPush::disableChannel($uid, $channel, $subscribeId);

                $this->output->set_header('Content-Type: application/json');
                $this->output->set_output(json_encode(['subscribeId' => $subscribeId]));
                return true;
            }
            case 'subscribe': {
                WebPush::log($uid, $type, $channel, $show, $subscribe, $subscribeId);
                WebPush::enableChannel($uid, $channel, $subscribeId);

                $this->output->set_header('Content-Type: application/json');
                $this->output->set_output(json_encode(['subscribeId' => $subscribeId]));
                return true;
            }
        }
    }

    public function getMessage()
    {
        $data = [];

        /*if (Auth::isAuth()) {*/
            $uid = Auth::getUserId();
            // TODO Передаем через редис
            $pushData = [
                'title' => 'Урожай созрел!',
                'message'  => 'Приходи играть, uid ' . $uid . '!',
                'icon'  => 'https://exe.ru/assets/img/app/39/64x64/4c62fae76213c202f7a659e5e72b9810.png',
                'url'   => 'https://exe.ru/app39?source=push'
            ];

            $data = $pushData;
        /*}*/

        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($data));

        return true;
    }
}