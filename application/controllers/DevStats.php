<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.05.16
 * Time: 11:32
 */

use Base\Service\Auth,
    Base\Service\Roles,
    Base\Service\StatsGame;

class DevStats extends CI_Controller
{
    private $gid = null;

    public function get()
    {
        $type = $this->input->get_post('type');
        switch($type) {
            case 'installDelete': {
                return $this->view($this->getInstallDelete());
            }
            case 'installed': {
                return $this->view($this->getInstalled());
            }
            default: {
                show_error('Forbidden', 403);
                exit();
            }
        }
    }

    private function getInstalled()
    {
        $data = StatsGame::getCollectedInstalled($this->gid);

        $installed = [];
        if (count($data) > 0) {
            foreach($data as $row) {
                $time = $row['day_number'] * 864e5;
                $installed[] = [$time, (int) $row['count']];
            }
        }

        $currentDay = StatsGame::getDay();
        if (!isset($row) || $row['day_number'] != $currentDay) {
            $installed[] = [$currentDay * 864e5, \Base\Service\Game::getCountPlayers($this->gid)];
        }

        $startDay = $installed[0][0] / 1000;
        $endDay = $installed[count($installed) - 1][0] / 1000;

        return [$installed, [], $startDay, $endDay];
    }

    private function getInstallDelete()
    {
        $data = StatsGame::getCollectedInstallDelete($this->gid);

        $installed = $deleted = [];
        if (count($data) > 0) {
            foreach($data as $row) {
                $time = $row['day_number'] * 864e5;
                $installed[] = [$time, (int) $row['installed']];
                $deleted[] = [$time, (int) $row['deleted']];
            }
        }

        $currentDay = StatsGame::getDay();
        if (!isset($row) || $row['day_number'] != $currentDay) {
            $installed[] = [$currentDay * 864e5, 0];
            $deleted[] = [$currentDay* 864e5, 0];
        }

        $startDay = $installed[0][0] / 1000;
        $endDay = $installed[count($installed) - 1][0] / 1000;

        return [$installed, $deleted, $startDay, $endDay];
    }

    public function __construct()
    {
        parent::__construct();

        session_write_close();

        $gid = (int) $this->input->get_post('gid');

        if (!Auth::isAuth() || !Roles::hasAccess(Auth::getUserId(), $gid, Roles::ACCESS_CHANGE_SETTINGS)) {
            show_error('Forbidden', 403);
            exit();
        }

        $this->gid = $gid;
    }

    private function view($data)
    {
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($data));
        return true;
    }
}