<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.04.16
 * Time: 13:11
 */

use Base\Service\Game,
    Base\Service\LFRetention,
    Base\Service\Notifications,
    Base\Service\Redis,
    Base\Service\RedisKeys,
    Base\Service\Stats,
    Base\Service\StatsGame,
    Base\Service\User;

defined('BASEPATH') OR exit('No direct script access allowed');

class Crontask extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->input->get('pw') !== '123055' && !is_cli()) {
            show_404();
        }
        ini_set('memory_limit', '512M');
        set_time_limit(0);
    }

    public function fix_bounced()
    {
        \Base\Service\Mail::send('amazon@exe.ru', 'Fix bounced ' . date('d-m-Y H:i'), 'Fix bounced ' . date('d-m-Y H:i'));
    }

    public function mailing()
    {
        $start = microtime(true);
        $length = Redis::lLen(RedisKeys::mailingQueue);

        echo "Length: " . $length . "\n";

        if ($length > 0) {
            $data = unserialize(Redis::lPop(RedisKeys::mailingQueue));
            foreach($data['emails'] as $email) {
                \Base\Service\Mail::send($email, $data['subject'], $data['template']);
                echo "Send: " . $email . "\n";
            }

            echo "End, duration " . (microtime(true) - $start) . "\n\n";
        }
    }

    public function downloadRemoteImages()
    {
        $start = microtime(true);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            error_reporting(E_ALL);
            $ci->load->database();
        }

        $db = $ci->db;

        $query = 'SELECT DISTINCT "image_url" FROM "feed" WHERE "image_url" IS NOT NULL AND substring("image_url" from 1 for 1) != \'/\' AND "local_image_url" IS NULL LIMIT 50';
        $result = $db->query($query)->result_array();

        if (count($result) < 1) {
            echo "No images for download\n";
            return;
        }

        foreach($result as $row) {
            $image = $row['image_url'];

            list($remoteFileName,) = explode('?', $image);
            $remoteFileExt = substr($remoteFileName, strrpos($remoteFileName, '.') + 1);

            $hash = md5($image);
            $hashFilePath = rtrim(FCPATH, '/') . '/assets/img/cache/' . $hash[0] . '/' . $hash[1] . '/' .  $hash[2] . '/' . $hash . '.' . $remoteFileExt;

            if (!file_exists($hashFilePath)) {
                $hashDirName = dirname($hashFilePath);
                if (!is_dir($hashDirName)) {
                    mkdir($hashDirName, 0755, true);
                }

                file_put_contents($hashFilePath, file_get_contents($image));

                $imageSize = @getimagesize($hashFilePath);

                if (!$imageSize) {
                    echo "Path " . $image . " error!\n";
                    return;
                }

                \Base\Service\File::upload($hashFilePath);


                $shortFilePath = substr($hashFilePath, strlen(FCPATH) - 1);
                echo "Uploaded: " . $image . ", path " . $shortFilePath . "\n";

                touch($hashFilePath);
            }

            $query = 'UPDATE "feed" SET "local_image_url" = \'' . $shortFilePath . '\' WHERE "image_url" = \'' . $image . '\';';
            $db->query($query);

            echo $query . "\n";
            echo "Count: " . $db->affected_rows() . "\n";
        }

        echo "Proccessed by " . (microtime(true) - $start) . "\n\n";
    }


    public function reparseStat($offset = -1)
    {
        $postfix = date('Ymd', time() - 86400);

        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            error_reporting(E_ALL);
            ini_set('display_errors', 'on');
            $ci->load->database();
        }

        $postgres = $ci->db;

        do {
            $offset++;
            $query = 'SELECT * FROM "stats_user_referers_v2" WHERE "end" != 0 ORDER BY "id" LIMIT 1 OFFSET ' . $offset;
            $session = $postgres->query($query)->result_array();

            if (count($session) == 0) {
                break;
            }

            $markerData = $session[0];

            $sessionData = [
                'id' => $markerData['id'],
                'marker' => $markerData['marker'],
                'ip' => $markerData['ip'],
                'agent' => $markerData['agent'],
                'entry_point' => $markerData['point'],
                'referer' => $markerData['referer'],
                'adv_service' => $markerData['adv_service'],
                'adv_campaign' => $markerData['adv_campaign'],
                'adv_marker' => $markerData['adv_marker'],
                'adv_place' => $markerData['adv_place'],
                'time_start' => $markerData['start'],
                'time_end' => $markerData['start'],
                'time_duration' => $markerData['duration'] >= 0 ? $markerData['duration'] : 0,
                'uid' => null,
                'init' => false,
                'pay_count' => 0,
                'pay_sum' => 0
            ];

            $query = 'SELECT * FROM "userlog" WHERE "time">=' . $markerData['start'] . ' AND "time" <= ' . $markerData['end'] . ' AND "marker" = \'' . $markerData['marker']. '\' ORDER BY "time"';
            $log = $postgres->query($query)->result_array();

            if (count($log) == 0) {
                if (!empty($markerData['uid']) && $markerData['uid'] > 0) {
                    $sessionData['uid'] = $markerData['uid'];
                }
            } else {
                $actions = array_unique(array_column($log, 'action'));

                if (count(array_intersect($actions, [\Base\Service\UserLog::REGISTER, \Base\Service\UserLog::REGISTER_SOCIAL, \Base\Service\UserLog::NEW_TEMPORARY])) > 0
                    && count(array_intersect($actions, [\Base\Service\UserLog::TEMPORARY_AUTH_AS_USER, \Base\Service\UserLog::USER_AUTH_FROM_TEMPORARY])) == 0) {
                    $sessionData['init'] = true;
                }

                $uids = array_unique(array_column($log, 'uid'));
                if (count($uids) > 0) {
                    foreach($uids as $index => $uid) {
                        if (empty($uid)) {
                            unset($uids[$index]);
                        }
                    }
                    $uids = array_values($uids);
                }
                if (count($uids) > 0) {
                    $query = 'SELECT "uid" FROM "users_ext" WHERE "verified" = 1 AND "uid" IN (' . implode(', ', $uids) . ')';
                    $userIds =  $postgres->query($query)->result_array();
                    if (count($userIds) == 0) {
                        $sessionData['uid'] = $uids[count($uids) - 1];
                    } else {
                        $sessionData['uid'] = $userIds[0]['uid'];
                    }
                }

                if (in_array(\Base\Service\UserLog::TO_BALANCE, $actions)) {
                    foreach($log as $row) {
                        if ($row['action'] == \Base\Service\UserLog::TO_BALANCE) {
                            $sessionData['pay_count']++;
                            $sessionData['pay_sum'] += (isset($row['params']['count']) ? $row['params']['count'] : $row['params'][0]);
                        }
                    }
                }
            }

            if (empty($sessionData['uid'])) {
                 echo $markerData['marker'] . "\n";
                continue;
            }

            $query = 'SELECT * FROM "sessions_' . date('Ymd', $markerData['start']) . '" WHERE "id" = ' . $sessionData['id'] . ' LIMIT 1';
            $result = $postgres->query($query)->result_array();

            if (count($result) == 0) {
                $postgres->insert('sessions_' . date('Ymd', $markerData['start']), $sessionData);
                echo 'INSERT ' . $sessionData['id'] . "\n";
            } else {
                $equal = true;
                foreach($sessionData as $index => $value) {
                    if ($sessionData[$index] != $result[0][$index] && !($index == 'init' && $sessionData[$index] == '' && $result[0][$index] == 'f')) {
                        echo 'DIFF "' . $sessionData[$index] . '" AND "' . $result[0][$index] . '"' . "\n";

                        $equal = false;
                        break;
                    }
                }
                if ($equal) {
                    echo 'EQUAL ' . $sessionData['id'] . "\n";
                } else {
                    $postgres->update('sessions_' . date('Ymd', $markerData['start']), $sessionData, $sessionData['id']);
                    echo 'UPDATED ' . $sessionData['id'] . "\n";
                }
            }
        } while(count($session) > 0);
    }

    public function syncAdvCampaigns($date = null)
    {
        if (ENVIRONMENT !== 'production') {
            exit();
        }

        if (!empty($date)) {
            $postfix = $date;
        } else {
            $postfix = date('Ymd', time() - 86400);
        }

        set_time_limit(0);

        $ci = & get_instance();

        $postgres = $ci->load->database('production_postgres', true);
        $mysql = $ci->load->database('admin_mysql', true);

        $mysql->select(['ac_id', 'hash'])->from('adv_campaigns');
        $result = $mysql->get();

        $hashes = [];
        if ($result->num_rows() > 0) {
            $res = $result->result_array();
            $hashes = array_column($res, 'ac_id', 'hash');
        }

        $postgres->select(['adv_service', 'adv_campaign', 'adv_marker'])
            ->from('sessions_' . $postfix)
            ->group_by('adv_service')
            ->group_by('adv_campaign')
            ->group_by('adv_marker');
        $result = $postgres->get()->result_array();

        $forInsert = [];
        foreach($result as $row) {
            $title = $row['adv_service'] . ';' . $row['adv_campaign'] . ';' . $row['adv_marker'];
            $hash = md5($title);
            if (!isset($hashes[$hash])) {
                $forInsert[] = [
                    'hash' => $hash,
                    'title' => $title,
                    'adv_service' => $row['adv_service'],
                    'adv_campaign' => $row['adv_campaign'],
                    'adv_marker' => $row['adv_marker']
                ];
            }
        }

        if (count($forInsert) > 0) {
            $mysql->insert_batch('adv_campaigns', $forInsert);

            // Перестроим хеш
            $mysql->select(['ac_id', 'hash'])->from('adv_campaigns');
            $result = $mysql->get();

            $hashes = [];
            if ($result->num_rows() > 0) {
                $res = $result->result_array();
                $hashes = array_column($res, 'ac_id', 'hash');
            }
        }

        // Новые пользователи
        $postgres->select()->from('sessions_' . $postfix)->where(['init' => true]);
        $result = $postgres->get()->result_array();

        if (count($result) > 0) {
            $users = [];
            foreach($result as $row) {
                $hash = md5($row['adv_service'] . ';' . $row['adv_campaign'] . ';' . $row['adv_marker']);
                $user = [
                    'uid' => $row['uid'],
                    'date' => date('Ymd', $row['time_start']),
                    'timestamp' => $row['time_start'],
                    'bid' => $hashes[$hash],
                    'exp_init' => $row['time_duration']
                ];

                $users[$row['uid']] = $user;
            }

            $query = 'SELECT "t1"."uid", count("t2"."id") as "sess_fh", sum("t2"."time_duration") as "exp_fh" FROM (SELECT "uid", "time_start" FROM "sessions_'.$postfix.'" WHERE "init") as "t1" LEFT JOIN (SELECT "id", "uid", "time_start", "time_duration" FROM "sessions_'.$postfix.'") as "t2" ON
"t1"."uid" = "t2"."uid" AND "t2"."time_start" BETWEEN "t1"."time_start" AND "t1"."time_start" + 3600 GROUP BY "t1"."uid" ORDER BY "t1"."uid"';
            $result = $postgres->query($query)->result_array();

            $forInsert = [];
            foreach($result as $row) {
                $users[$row['uid']]['exp_fh'] = $row['exp_fh'];
                $users[$row['uid']]['sess_fh'] = $row['sess_fh'];
                $user = $users[$row['uid']];
                $forInsert[] = "('".$user['uid']."', '".$user['date']."', '".$user['timestamp']."', '".$user['bid']."', '".$user['exp_init']."', '".$user['exp_fh']."', '".$user['sess_fh']."')";
            }

            $query = 'REPLACE INTO stats__exeru_users (uid, date, timestamp, bid, exp_init, exp_fh, sess_fh) VALUES ' . implode(', ', $forInsert);
            $mysql->query($query);
        }

        // Старые пользователи:
        $query = 'SELECT DISTINCT("uid") as "uid" FROM "sessions_'.$postfix.'" WHERE init=\'f\'';
        $result = $postgres->query($query)->result_array();

        if (count($result) > 0) {
            foreach($result as $row) {
                $mysql->select()->from('stats__exeru_users')->where(['uid' => $row['uid']])->limit(1);
                $mResult = $mysql->get();

                if ($mResult->num_rows() > 0) {
                    $oldData = $mResult->row_array();
                    $newData = [];
                    foreach([2, 7, 30, 90] as $retentionDay) {
                        $retentionDayStart = $oldData['timestamp'] + ($retentionDay - 1) * 86400;
                        $retentionDayEnd = $oldData['timestamp'] + $retentionDay * 86400 - 1;
                        $retentionsDayQuery = 'SELECT count("id") as "sess' . $retentionDay . '",
                         sum("time_duration") as "exp' . $retentionDay . '" FROM "sessions"
                         WHERE "time_start" > ' . $retentionDayStart . ' AND "time_start" <= ' . $retentionDayEnd . ' AND "uid" = ' . $oldData['uid'] . '
                         GROUP BY "uid"';
                        $retentionResult = $postgres->query($retentionsDayQuery)->result_array();
                        if (count($retentionResult) > 0) {
                            $newData = array_merge($newData, $retentionResult[0]);
                            $newData['ret' . $retentionDay] = 1;
                        }

                        $payDayQuery = 'SELECT "params" FROM "userlog" WHERE "time" >= ' . $oldData['timestamp'] . ' AND "time" <= ' . $retentionDayEnd . ' AND "uid" = ' . $oldData['uid'] . ' AND "action" = 11';

                        $payResult = $postgres->query($payDayQuery)->result_array();
                        if (count($payResult) > 0) {
                            $newData['payq' . $retentionDay] = count($payResult);
                            $newData['pay' . $retentionDay] = 0;
                            foreach($payResult as $row) {
                                $data = unserialize($row['params']);
                                $sum = isset($data['count']) ? $data['count'] : $data[0];
                                $newData['pay' . $retentionDay] += $sum;
                            }
                        }
                    }

                    if (count($newData) > 0) {
                        $forUpdate = [];
                        foreach($newData as $fieldName => $fieldValue) {
                            if ($newData[$fieldName] != $oldData[$fieldName]) {
                                $forUpdate[] = $fieldName . " = '" . $fieldValue . "'";
                            }
                        }
                        if (count($forUpdate) > 0) {
                            $mysql->query($query);
                            $query = 'UPDATE stats__exeru_users SET ' . implode(", ", $forUpdate) . ' WHERE uid = ' . $oldData['uid'];
                            echo $query . "\n";
                        }
                    }
                }
            }
        }
    }

    public function parseByDays()
    {
        $start = strtotime('16-04-2018 00:00:00');
        $end = strtotime(date('d-m-Y') . ' 00:00:00') - 86400;

        while($start <= $end) {
            echo date('d-m-Y', $start) . "\n";
            $this->syncAdvCampaigns(date('Ymd', $start));
            $start += 86400;
        };
    }

    public function setMarkers()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $postgres = & $ci->db;

        $query = 'SELECT * FROM "a_userlog" WHERE "marker" IS NULL';
        $result = $postgres->query($query)->result_array();

        if (count($result) > 0) {
            foreach($result as $sess) {
                echo $sess['id'];
                $query = 'SELECT *, abs("time" - '.$sess['time'].') as "diff" FROM "userlog" WHERE "time" - 600 <= '.$sess['time'].' AND "time" + 600 >= '.$sess['time'].' AND "id" != ' . $sess['id'] . ' AND "uid" = ' . $sess['uid'] . ' AND "marker" IS NOT NULL ORDER BY "diff" LIMIT 1';
                $markerRow = $postgres->query($query)->result_array();
                if (count($markerRow) > 0) {
                    $query = 'UPDATE "a_userlog" SET "marker" =\''.$markerRow[0]['marker'].'\' WHERE "id" = ' . $sess['id'];
                    $postgres->query($query);
                    echo ':' . $markerRow[0]['marker'];
                }
                echo "\n";
            }
        }
    }

    public function cleanGamers()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $query = 'SELECT * FROM (SELECT COUNT(*) as "count", "uid", "gid" FROM "gamers" GROUP BY "uid", "gid") as "t" WHERE "count" > 1 LIMIT 100';

        $result = $db->query($query);

        var_dump($result->num_rows());

        if ($result->num_rows() > 0) {
            foreach($result->result_array() as $row) {
                $query = 'SELECT * FROM "gamers" WHERE "uid" = '. $row['uid'] .' AND "gid" = ' . $row['gid'] ;
                $result = $db->query($query);

                $data = $result->result_array();

                $record = [
                    'uid' => $row['uid'],
                    'gid' => $row['gid'],
                    'last_visit' => $data[0]['last_visit'],
                    'installed' => ((array_sum(array_column($data, 'installed')) / count($data)) >= 0.5 ? 1 : 0),
                    'notifications' => ((array_sum(array_column($data, 'notifications')) / count($data)) >= 0.5 ? 1 : 0),
                    'invitible' => ((array_sum(array_column($data, 'invitible')) / count($data)) >= 0.5 ? 1 : 0)
                ];

                $db->delete('gamers', ['uid' => $row['uid'], 'gid' => $row['gid']]);
                $db->insert('gamers', $record);

                var_dump($data);
                var_dump($record);
            }
        }
    }

    public function uploadLFRetentionBeforeAfterPixel()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $query = 'SELECT "lf_assign"."lf_uid", MAX("t1"."max") FROM (SELECT "uid", MAX("last_visit") FROM "gamers" GROUP BY "gamers"."uid") as "t1"
                  INNER JOIN "lf_assign" ON "t1"."uid" = "lf_assign"."exe_uid"
                  GROUP BY "lf_assign"."lf_uid"
                  ORDER BY "lf_assign"."lf_uid"';

        $result = $db->query($query);

        if ($result->num_rows() > 0) {
            $point = strtotime('30-11-2017 11:47:00');

            $fileBefore = FCPATH . 'assets/uploads/app_pixel_before.txt';
            if (file_exists($fileBefore)) {
                unlink($fileBefore);
            }

            $fileAfter = FCPATH . 'assets/uploads/app_pixel_after.txt';
            if (file_exists($fileAfter)) {
                unlink($fileAfter);
            }

            $data = $result->result_array();
            foreach($data as $row) {
                $file = $row['max'] > $point ? $fileAfter : $fileBefore;
                $string =  $row['lf_uid'] . ':' . md5($row['lf_uid'] . microtime(true) . $row['max']) . "\n";
                file_put_contents($file, $string, FILE_APPEND);
            }

            LFRetention::uploadFile('app_pixel_before', $fileBefore);
            LFRetention::uploadFile('app_pixel_after', $fileAfter);
        }
    }

    public function uploadLFRetentionAll()
    {
        LFRetention::uploadAll();
    }

    public function uploadLFRetention($daysForAll = 30, $daysForGamers = 7)
    {
        // При изменении входящих значений необходимо учитывать, что в таблицах сессий (пользовательских и игровых)
        // сессии за рамками этих интервалов удаляются.
        LFRetention::upload($daysForAll, $daysForGamers);
    }

    public function prepareGameCatalogCache()
    {
        $oddEven = (date('i') + 1) % 2;

        $redisKey = RedisKeys::gameCatalogGuest . ENVIRONMENT .  '::' . $oddEven . '::html';

        $this->load->library(['Twig']);

        for($i=1; $i<=Game::countCachedCatalogGuestHTML; $i++) {
            $data = [
                'games' => Game::getCatalog(), // Уже перемешанный
                'carusel' => Game::getCarusel(), // Тоже уже перемешанные
            ];

            $html = $this->twig->render('quest/games_list.twig', $data);

            Redis::hSet($redisKey, $i, $html);
        }
    }

    public function parseQueryStrings()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $db->select(['id', 'referer']);
        $db->from(\Base\Model\User::TABLE_STATS_REFERER_V2);
        $db->where(['search_string_parsed' => false]);
        $db->limit(50);

        $result = $db->get()->result_array();

        if (count($result) > 0) {
            foreach($result as $row) {
                if (strpos($row['referer'], '&q=') !== false) {
                    $q = '&q=';
                } elseif(strpos($row['referer'], '?q=') !== false) {
                    $q = '?q=';
                } else {
                    $db->update(\Base\Model\User::TABLE_STATS_REFERER_V2, ['search_string' => '', 'search_string_parsed' => true], ['id' => $row['id']]);
                    continue;
                }

                $query = substr($row['referer'], strpos($row['referer'], $q) + strlen($q));

                if (strpos($query, '&') !== false) {
                    $query = substr($query, 0, strpos($query, '&'));
                }

                $searchString = urldecode($query);
                if (!preg_match('||u', $searchString)) {
                    $searchStringCP1251 = mb_convert_encoding($searchString, 'windows-1251', 'windows-1251');
                    if (md5($searchString) == md5($searchStringCP1251)) {
                        $searchString = mb_convert_encoding($searchString, 'utf-8', 'windows-1251');
                    }
                }

                $db->update(\Base\Model\User::TABLE_STATS_REFERER_V2, ['search_string' => $searchString, 'search_string_parsed' => true], ['id' => $row['id']]);
            }
        }
    }

    public function createUserlogTables()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $mainTableName = "userlog";

        $startToday = strtotime(date('d-m-Y 00:00:00'));

        for($i=1; $i<=3; $i++) {
            $dayStart = strtotime(date('d-m-Y 00:00:00', $startToday + ($i * 86400)));
            $nextDayStart = strtotime(date('d-m-Y 00:00:00', $startToday + (($i + 1) * 86400)));

            $tablename = $mainTableName. '_' . date('Ymd', $dayStart);

            $query = 'CREATE TABLE IF NOT EXISTS "' . $tablename . '" () inherits ("' . $mainTableName . '")';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' .$tablename . '" DROP CONSTRAINT IF EXISTS "date_check"';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' . $tablename . '" ADD CONSTRAINT "date_check" CHECK ("time" >= \'' . $dayStart . '\' AND "time" < \'' . $nextDayStart . '\')';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_time_key"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_time_key" ON "' . $tablename . '" USING btree("time")';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_uid_action_i"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_uid_action_i" ON "' . $tablename . '" USING btree("uid", "action")';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_marker_i"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_marker_i" ON "' . $tablename . '" USING hash("marker")';
            $db->query($query);
            echo $query . "\n";
        }
    }

    public function createSessionTables()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $mainTableName = "sessions";

        $startToday = strtotime(date('d-m-Y 00:00:00'));

        for($i=1; $i<=3; $i++) {
            $dayStart = strtotime(date('d-m-Y 00:00:00', $startToday + ($i * 86400)));
            $nextDayStart = strtotime(date('d-m-Y 00:00:00', $startToday + (($i + 1) * 86400)));

            $tablename = $mainTableName . '_' . date('Ymd', $dayStart);

            $query = 'CREATE TABLE IF NOT EXISTS "' . $tablename . '" () inherits ("' . $mainTableName . '")';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' .$tablename . '" DROP CONSTRAINT IF EXISTS "date_check"';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' . $tablename . '" ADD CONSTRAINT "date_check" CHECK ("time_start" >= \'' . $dayStart . '\' AND "time_start" < \'' . $nextDayStart . '\')';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_time_start_key"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_time_start_key" ON "' . $tablename . '" USING btree("time_start")';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_marker_i"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_marker_i" ON "' . $tablename . '" USING hash("marker")';
            $db->query($query);
            echo $query . "\n";
        }
    }

    public function clearRedis()
    {
        $keys = Redis::getKeys(RedisKeys::userLastActive . '*');
        foreach($keys as $key) {
            Redis::del($key);
        }
    }

    public function publishAppointedFeeds()
    {
        $feeds = \Base\Service\Feeds::getAppointedFeeds(time());

        if (count($feeds) > 0) {
            $feed = $feeds[0];

            \Base\Service\Feeds::addFeed(
                $feed['uid'],
                $feed['gid'],
                $feed['type'],
                $feed['content'],
                $feed['fuid'],
                $feed['image_url'],
                $feed['params'],
                $feed['title']
            );

            \Base\Service\Feeds::deleteAppointedFeed($feed['fid'], $feed['gid']);
        }
    }

    public function updateMarkerDuration()
    {
        User::updateMarkersDuration(time() - 1800);
    }

/*    public function userCollectSessions()
    {
        $time = time();

        // Обновляем данные о уже существующих сессиях
        $redisKeySessionIds = RedisKeys::userLastActive . 'sessionIds::'; // Идентификаторы уже существующих сессий
        $uids = Redis::hKeys($redisKeySessionIds);

        if (count($uids) > 0) {
            foreach($uids as $uid) {
                usleep(1000);
                $uid = (int) $uid;
                $sessionId = (int) Redis::hGet($redisKeySessionIds, $uid);

                $redisKeySessionData = RedisKeys::userLastActive . 'sessionData::' . $uid . '::' . $sessionId . '::';
                $sessionData = Redis::hGetAll($redisKeySessionData);

                if (count($sessionData) == 0) {
                    // У нас есть только старый идентификатор, самой сессии не существует
                    continue;
                }

                if ($sessionData['last'] + 1800 <= $time) {
                    // Последняя активность была более 30 минут назад
                    $redisKeyTime = RedisKeys::userLastActive . 'time::';
                    Redis::hDel($redisKeyTime, $uid);

                    Redis::del($redisKeySessionData);

                    $sessionId++;
                    Redis::hSet($redisKeySessionIds, $uid, $sessionId);
                    continue;
                }

                if ($sessionData['db'] == $sessionData['last'] || $sessionData['after'] > $time) {
                    // Данные в базе актуальны, или обновлять ещё рано.
                    continue;
                }

                $sessionData['db'] = $sessionData['last'];
                $sessionData['after'] = $time + 300;
                Redis::hMSet($redisKeySessionData, $sessionData);

                User::uptimeSession($uid, $sessionData['last']);
                Stats::updateUserSession($uid, $sessionId, $sessionData['init'], $sessionData['last']);
            }
        }

        $redisKeyTime = RedisKeys::userLastActive . 'time::'; // Время последнего запроса
        $lastTimeData = Redis::hKeys($redisKeyTime);

        if (count($lastTimeData) > 0) {
            foreach($lastTimeData as $uid) {
                usleep(1000);
                $uid = (int) $uid;
                $lastActiveTime = (int) Redis::hGet($lastTimeData, $uid);;

                $sessionId = Redis::hGet($redisKeySessionIds, $uid);
                if ($sessionId < 1) {
                    $sessionId = 1;
                    Redis::hSet($redisKeySessionIds, $uid, $sessionId);
                }

                $redisKeySessionData = RedisKeys::userLastActive . 'sessionData::' . $uid . '::' . $sessionId . '::';
                $sessionData = Redis::hGetAll($redisKeySessionData);

                if (count($sessionData) == 0) {
                    Redis::hMSet($redisKeySessionData, [
                        'init' => $lastActiveTime,
                        'last' => $lastActiveTime,
                        'db' => $lastActiveTime,
                        'after' => $time + 300 // Обновить запись в БД не раньше, чем через 300 секунд
                    ]);

                    User::uptimeSession($uid, $lastActiveTime);
                    Stats::appendUserSession($uid, $sessionId, $lastActiveTime, $lastActiveTime);
                } else {
                    Redis::hSet($redisKeySessionData, 'last', $lastActiveTime);
                }
            }
        }
    }*/

    public function deleteOldSessions()
    {
        Game::deleteOldSessions();
        User::deleteOldSessions();
    }

    public function gameCollectStatInstalled()
    {
        return StatsGame::collectStatInstalled();
    }

    public function gameCollectStatInstallDelete($today = null)
    {
        return StatsGame::collectStatInstallDelete($today);
    }

    public function notificationsCollectStat()
    {
        return Notifications::collectStat();
    }

    public function parseStat()
    {
        return Stats::parseCrude();
    }

    public function updateMonths()
    {
        return Stats::updateMonths();
    }
}