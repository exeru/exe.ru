<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04.10.16
 * Time: 12:30
 */
use Base\Service\Auth,
    Base\Service\AuthLog,
    Base\Service\Events,
    Base\Service\Game,
    Base\Service\Graph,
    Base\Service\Payments,
    Base\Service\Project,
    Base\Service\Roles,
    Base\Service\User,
    Base\Service\UserLog;

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!Auth::isAuth() || !Roles::isModerator(Auth::getUserId())) {
            session_write_close();

            $this->load->helper('url');
            redirect(Project::getBaseURI('main'));
        }

        if (!Project::isAdmin()) {
            session_write_close();

            $this->load->helper('url');
            redirect(Project::getBaseURI('admin'));
        }

        session_write_close();

        $this->load->library('Twig');
    }

    public function other()
    {
        $do = $this->input->post('do');
        $start = $this->input->post('start');
        $end = $this->input->post('end');

        if (!empty($do) && $do == 'save_bonus_10'
            && !empty($start) && !empty($end)) {
            $startParts = explode('.', $start);
            $endParts = explode('.', $end);

            $query = 'SELECT DISTINCT "uid" as "uid"
                      FROM "balance_log"
                      WHERE "uid" IS NOT NULL
                        AND "type" = 0
                        AND "receiver" = ' . \Base\Provider\Payment::PAYMENT_EXE_INTERNAL . '
                        AND "count" = 10 
                        AND "yyyymmdd"::integer >= ' . $startParts[2] . $startParts[1] . $startParts[0] . '
                        AND "yyyymmdd"::integer <= ' . $endParts[2] . $endParts[1] . $endParts[0];

            $postgres = $this->load->database('master', true);

            $result = $postgres->query($query, false);

            if ($result->num_rows() < 1) {
                echo "Данных нет.";
                return;
            }

            $uids = array_column($result->result_array(), 'uid');

            $query = "SELECT * FROM `stats__exeru_users` WHERE `uid` IN ('".implode("', '", $uids)."') ORDER BY `uid`";

            $mysql = $this->load->database('admin_mysql', true);
            $result = $mysql->query($query);

            if ($result->num_rows() < 1) {
                echo 'В таблице ретеншн нет данных';
                return;
            }
            $content = '';
            $data = $result->result_array();
            $content .= implode(';', array_keys($data[0])) . "\n";
            foreach ($data as $row) {
                $content .= implode(';', $row) . "\n";
            }

            if (ob_get_level()) {
                ob_end_clean();
            }

            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=result.csv');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . mb_strlen($content, 'UTF-8'));
            // читаем файл и отправляем его пользователю
            echo $content;
            exit;
        }

        $result = [];
        $result['currentUrl'] = 'other';
        $result['user'] = User::getUserInfo(Auth::getUserId());

        $this->twig->view('admin/other.twig', $result);
    }

    public function carusel()
    {
        $carusel = Game::getCaruselData();

        foreach($carusel as &$row) {
            if ($game = Game::getGameById($row['gid'])) {
                $row['action'] = Game::actionTypes[$game['type']]['title'];
                $row['color'] = Game::actionTypes[$game['type']]['color'];
            }
        }

        $result = [];
        $result['currentUrl'] = 'carusel';
        $result['user'] = User::getUserInfo(Auth::getUserId());
        $result['carusel'] = $carusel;

        $this->twig->view('admin/carusel.twig', $result);
    }

    public function graph()
    {
        $nullGroupName = 'DirectEntry';

        $result = [
            'user' => User::getUserInfo(Auth::getUserId()),
            'site' => IMAGE_HOST,
            'currentUrl' => 'graph',
            'graph' => [
                'advert' => [
                    'day' => Graph::getAdvertByDay($nullGroupName)
                ],
                'newGamers' => [
                    'day' => Graph::getNewGamersByDay()
                ],
                'oldUsersDuration' => [
                    'day' => Graph::getUsersDurationByDay(false)
                ],
                'newUsersDuration' => [
                    'day' => Graph::getUsersDurationByDay(true)
                ],
                'purchases' => [
                    'day' => Graph::getPurchasesByDay()
                ],
                'balance' => [
                    'day' => Graph::getBalanceByDay()
                ],
                'paySystems' => [
                    'day' => Graph::getPaySystemsByDay()
                ],
                'registations' => [
                    'day' => Graph::getRegistrationsByDay()
                ]
            ]
        ];

        $this->twig->view('admin/graph.twig', $result);
    }

    public function marker($marker)
    {
        $refer = User::getReferByMarker($marker);

        if ($refer['end'] == 0) {
            $markerTime = \Base\Service\Redis::hGet(\Base\Service\RedisKeys::userMarkerTime, $marker);
            if ($markerTime > 0) {
                $refer['duration'] = $markerTime - $refer['start'];
            }
        }

        $log = UserLog::getLogByMarker($marker);

        $data = [];
        $info = [];

        foreach($log as $row) {
            $data['user'] = User::getUserInfo($row['uid'], true);
            $data['user']['status'] = User::getUserStatus($row['uid']);
            $data['user']['email'] = User::getUserEmail($row['uid']);
            $data['info'] = $row;
            $data['info']['action'] = self::parseAction($row);
            $info[] = $data;
        }

        $result = [
            'data' => $info,
            'refer' => $refer,
            'user' => User::getUserInfo(Auth::getUserId()),
            'site' => IMAGE_HOST,
            'currentUrl' => 'admin',
            'marker' => $marker
        ];

        $this->twig->view('admin/marker.twig', $result);
    }

    public function userstat()
    {
        $form = [
            'start' => date('d.m.Y', time() - 7 * 86400),
            'end' => date('d.m.Y'),
        ];

        $start = $this->input->get_post('start');
        $end = $this->input->get_post('end');

        if ($start != '') {
            $form['start'] = $start;
            $start = strtotime($start);
        } else {
            $start = strtotime($form['start']);
        }

        if ($end != '') {
            $form['end'] = $end;
            $end = strtotime($end) + 86400;
        } else {
            $end = strtotime($form['end'])  + 86400;
        }

        $result = [];
        $result['currentUrl'] = 'userstat';
        $result['user'] = User::getUserInfo(Auth::getUserId());
        $result['form'] = $form;
        $result['counts'] = User::getCountUsersBySocial();
        $result['counts']['form'] = User::getCountUsersNotSocial();
        $result['counts']['all'] = User::getCountUsers();
        $result['data'] = AuthLog::getLogByDestination($start, $end);
        $result['webpushes'] = \Base\Service\WebPush::getSubscribeStat();

        $this->twig->view('admin/userstat.twig', $result);
    }

    public function emails()
    {
        $template = rtrim(APPPATH, '/') . 'views/letters/template.twig';

        $form = [
            'testemail' => 'xstudent@yandex.ru'
        ];

        $result = [
            'data' => [], //$info,
            'form' => $form,
            'user' => User::getUserInfo(Auth::getUserId()),
            'site' => IMAGE_HOST,
            'currentUrl' => 'emails'
        ];

        $this->twig->view('admin/emails.twig', $result);
    }

    public function emailsList()
    {
        $result = User::getEmailsForAdvertising();
        foreach($result as $email) {
            echo $email . "<br/>";
        }
    }


    public function payments()
    {
        $form = [
            'start' => date('d.m.Y', time() - 7 * 86400),
            'end' => date('d.m.Y'),
            'byid' => '',
            'filters' => ['init' => 'Инициирован' , 'check' => 'Проверен', 'accept' => 'Зачислен', 'manual' => 'Зачислен модератором'],
            'page' => 1
        ];

        foreach($form['filters'] as $id => $text) {
            $form['filters'][$id] = ['text' => $text, 'checked' => false];
        }

        $start = $this->input->get_post('start');
        $end = $this->input->get_post('end');
        $byid = $this->input->get_post('byid');
        $filter = $this->input->get_post('filter');
        $page = $this->input->get_post('page');

        if ($start != '') {
            $form['start'] = $start;
            $start = strtotime($start);
        } else {
            $start = strtotime($form['start']);
        }

        if ($end != '') {
            $form['end'] = $end;
            $end = strtotime($end) + 86400;
        } else {
            $end = strtotime($form['end'])  + 86400;
        }

        if ($byid != '') {
            $form['byid'] = (int) $byid;
            $byid = $form['byid'];
        } else {
            $byid = NULL;
        }

        if (is_array($filter)) {
            foreach($filter as $key) {
                $form['filters'][$key]['checked'] = true;
            }
        } else {
            $filter = [];
        }

        if ($page != '') {
            $form['page'] = (int) $page;
            $offset = ($form['page'] - 1) * 20;
        } else {
            $offset = 0;
        }

        $result = Payments::getRequestsLog($start, $end, $byid, $filter, 20, $offset);

        if (count($result['result']) > 0) {
            $orderIds = array_unique(array_column($result['result'], 'orderNumber'));
            $paymentsArray = Payments::getPaymentsDataByIds($orderIds, ['payment_id', 'payment_system_id',  'sum', 'status']);
            $payments = [];
            foreach($paymentsArray as $payment)
            {
                $payments[$payment['payment_id']] = $payment;
            }
        }

        $countPages = ceil($result['count'] / 20);

        $data = [];
        $info = [];

        foreach($result['result'] as $row) {
            $data['user'] = User::getUserInfo($row['uid'], true);
            $data['user']['email'] = User::getUserEmail($row['uid']);
            $data['info'] = $row;
            $data['info']['action'] = str_replace(['init', 'check', 'accept', 'manual'], ['Инициирован', 'Проверен', 'Зачислен', 'Зачислен<br/>' . $row['request'] ], $row['type']);
            $data['info']['type'] = $row['type'];
            $data['info']['payment'] = $payments[$row['orderNumber']];
            $data['info']['paymentSystemName'] = Payments::$humansText[$data['info']['payment']['payment_system_id']];
            $info[] = $data;
        }

        $result = [
            'data' => $info,
            'form' => $form,
            'pages' => (int) $countPages,
            'user' => User::getUserInfo(Auth::getUserId()),
            'site' => IMAGE_HOST,
            'currentUrl' => 'payments',
            'graph' => Payments::getAmountByDays($start, $end)
        ];

        $this->twig->view('admin/payments.twig', $result);
    }

    public function set()
    {
        $uid = $this->input->post('uid');
        $type = $this->input->post('type');
        $set = $this->input->post('set') === 'true';
        $orderId = $this->input->post('order_id');

        if (!in_array($type, ['email_checked', 'approved', 'status', 'payment'])) {
            return $this->view([
                'uid' => $uid,
                'type' => $type,
                'state' => boolval(! $set),
                'error' => 'Неизвестное поле.'
            ]);
        }

        if ($uid < 1 && is_null($orderId)) {
            return $this->view([
                'uid' => $uid,
                'type' => $type,
                'state' => boolval(! $set),
                'error' => 'Ошибочный идентификатор пользователя.'
            ]);
        }

        if ($type == 'status' && Roles::isModerator($uid)) {
            return $this->view([
                'uid' => $uid,
                'type' => $type,
                'state' => boolval(! $set),
                'error' => 'Пользователь является модератором и не может быть заблокирован.'
            ]);
        }


        switch($type) {
            case 'email_checked': {
                if ($set === true) {
                    User::setEmailChecked($uid);
                } else {
                    User::setEmailUnChecked($uid);
                }

                return $this->view([
                    'uid' => $uid,
                    'type' => $type,
                    'state' => boolval(User::isEmailChecked($uid))
                ]);
            }
            case 'approved': {
                if ($set === true) {
                    User::setApproved($uid);
                } else {
                    User::setUnApproved($uid);
                }

                return $this->view([
                    'uid' => $uid,
                    'type' => $type,
                    'state' => User::isApproved($uid)
                ]);
            }
            case 'status': {
                if ($set === true) {
                    User::setUserStatus($uid, 0);

                    Events::rise('EVENT_USER_BLOCKED', [
                        'uids' => [$uid],
                        'message' => json_encode([
                            'type' => 'blocked'
                        ])
                    ]);
                } else {
                    User::setUserStatus($uid, 1);
                }

                return $this->view([
                    'uid' => $uid,
                    'type' => $type,
                    'state' => User::getUserStatus($uid) === '0'
                ]);
            }
            case 'payment': {
                Payments::manualAcceptPayment(Auth::getUserId(), $orderId);

                return $this->view([
                    'type' => $type,
                    'order_id' => $orderId
                ]);
            }
        }

        // Действия
        return $this->view([
            'uid' => $uid,
            'type' => $type,
            'state' => boolval($set)
        ]);
    }

    public  function index()
    {
        $form = [
            'start' => date('d.m.Y', time() - 7 * 86400),
            'end' => date('d.m.Y'),
            'byid' => '',
            'session_number' => '',
            'bygid' => '',
            'filters' => UserLog::FILTERS,
            'bots' => false,
            'temporary' => false,
            'page' => 1
        ];

        foreach($form['filters'] as $id => $text) {
            $form['filters'][$id] = ['text' => $text, 'checked' => false];
        }

        $start = $this->input->get_post('start');
        $end = $this->input->get_post('end');
        $byid = $this->input->get_post('byid');
        $sessionNumber = $this->input->get_post('session_number');
        $bygid = $this->input->get_post('bygid');
        $filter = $this->input->get_post('filter');
        $bots = $this->input->get_post('bots');
        $temporary = $this->input->get_post('temporary');
        $page = $this->input->get_post('page');

        if ($start != '') {
            $form['start'] = $start;
            $start = strtotime($start);
        } else {
            $start = strtotime($form['start']);
        }

        if ($end != '') {
            $form['end'] = $end;
            $end = strtotime($end) + 86400;
        } else {
            $end = strtotime($form['end'])  + 86400;
        }

        if ($byid != '') {
            $form['byid'] = (int) $byid;
            $byid = $form['byid'];
        } else {
            $byid = NULL;
        }

        if ($sessionNumber != '') {
            $form['session_number'] = $sessionNumber;
            if (strpos($sessionNumber, ',')) {
                $sessionNumber = explode(',', $sessionNumber);
            }

            if (!is_array($sessionNumber)) {
                $sessionNumber = [$sessionNumber];
            }

            $sessionNumberArray = [];
            foreach($sessionNumber as $number) {
                $number = trim($number);
                if (strpos($number, '-')) {
                    list($sNumber, $eNumber) = explode('-', $number);
                    $sessionNumberArray = array_merge($sessionNumberArray, range((int) $sNumber, (int) $eNumber, 1));
                } else {
                    $sessionNumberArray[] = (int) $number;
                }
            }
            $sessionNumber = $sessionNumberArray;
        } else {
            $sessionNumber = null;
        }

        if ($bygid != '') {
            $form['bygid'] = (int) $bygid;
            $bygid = $form['bygid'];
        } else {
            $bygid = NULL;
        }

        if (is_array($filter)) {
            foreach($filter as $key) {
                $form['filters'][$key]['checked'] = true;
            }
        } else {
            $filter = [];
        }

        // Разобъем фильтры по группам
        $filtersByGroup = [];
        foreach(UserLog::GROUPS as $filterId => $groupName) {
            if (isset($form['filters'][$filterId])) {
                $filtersByGroup[$groupName][$filterId] = $form['filters'][$filterId];
            }
        }

        $form['filters'] = $filtersByGroup;

        if ($bots != '') {
            $form['bots']['checked'] = true;
            $bots = true;
        } else {
            $bots = false;
        }

        if ($temporary != '') {
            $form['temporary']['checked'] = true;
            $temporary = true;
        } else {
            $temporary = false;
        }

        if ($page != '') {
            $form['page'] = (int) $page;
            $offset = ($form['page'] - 1) * 20;
        } else {
            $offset = 0;
        }

        $result = UserLog::getLog($start, $end, $byid, $sessionNumber, $bygid, $bots, $temporary, $filter, 20, $offset);

        $countPages = ceil($result['count'] / 20);

        $data = [];
        $info = [];

        foreach($result['result'] as $row) {
            $data['user'] = User::getUserInfo($row['uid'], true);
            $data['user']['status'] = User::getUserStatus($row['uid']);
            $data['user']['email'] = User::getUserEmail($row['uid']);
            $data['info'] = $row;
            $data['info']['action'] = self::parseAction($row);
            $info[] = $data;
        }

        $result = [
            'data' => $info,
            'form' => $form,
            'pages' => (int) $countPages,
            'user' => User::getUserInfo(Auth::getUserId()),
            'site' => IMAGE_HOST,
            'currentUrl' => 'admin'
        ];

        $this->twig->view('admin/admin.twig', $result);
    }

    private function parseAction($row)
    {
        switch($row['action']) {
            // Содержат ссылку на профиль другого пользователя
            case UserLog::APPEND_FRIEND:
            case UserLog::DELETE_FRIEND:
            case UserLog::TO_BLACKLIST:
            case UserLog::FROM_BLACKLIST:
            case UserLog::USER_TO_BLACKLIST:
            case UserLog::USER_FROM_BLACKLIST:
            case UserLog::USER_TO_FRIENDS:
            case UserLog::USER_FROM_FRIENDS: {
                if (isset($row['params']['fid'])) {
                    $fid = $row['params']['fid'];
                } else {
                    $fid = $row['params'][0];
                }
                return str_replace('%user%', self::getProfileLink($fid), UserLog::TEXTS[$row['action']]);
            }
            // Содержат ссылку на игру
            case UserLog::BUY_ITEM_CANCEL:
            case UserLog::UNINSTALL_GAME:
            case UserLog::INSTALL_GAME:
            case UserLog::START_GAME: {
                if (isset($row['params']['gid'])) {
                    $gid = $row['params']['gid'];
                } else {
                    $gid = $row['params'][0];
                }
                return str_replace('%game%', self::getGameLink($gid), UserLog::TEXTS[$row['action']]);
            }
            case UserLog::BUY_ITEM_NO_MONEY:
            case UserLog::BEGIN_BUY_ITEM:
            case UserLog::PAY_TO_GAME_TEST:
            case UserLog::PAY_TO_GAME: {
                if (isset($row['params']['gid'])) {
                    $gid = $row['params']['gid'];
                    $count = $row['params']['count'];
                    if (isset($row['params']['balance'])) {
                        $balance = $row['params']['balance'];
                    } else {
                        $balance = 'X';
                    }
                } else {
                    $gid = $row['params'][0];
                    $count = 'X';
                    $balance = 'X';
                }
                return str_replace(['%game%', '%count%', '%balance%'], [self::getGameLink($gid), $count, $balance], UserLog::TEXTS[$row['action']]);
            }
            case UserLog::BUY_ITEM_NO_MONEY_AFTER_RECHARGE:
            case UserLog::BONUS_TO_BALANCE:
            case UserLog::TO_BALANCE: {
                if (isset($row['params']['count'])) {
                    $count = $row['params']['count'];
                    if (isset($row['params']['balance'])) {
                        $balance = $row['params']['balance'];
                    } else {
                        $balance = 'X';
                    }
                } else {
                    $count = $row['params'][0];
                    $balance = 'X';
                }
                return str_replace(['%count%', '%balance%'], [$count, $balance], UserLog::TEXTS[$row['action']]);
            }
            case UserLog::MODERATOR_TO_BALANCE: {
                $muid = $row['params']['muid'];
                $paymentId = $row['params']['payment_id'];
                $sum = $row['params']['sum'];
                if (isset($row['params']['balance'])) {
                        $balance = $row['params']['balance'];
                } else {
                    $balance = 'X';
                }
                return str_replace(['%muid%', '%orderId%', '%sum%', '%balance%'], [$muid, $paymentId, $sum, $balance], UserLog::TEXTS[$row['action']]);
            }
            case UserLog::USER_WRITE_MESSAGE: {
                $uid = $row['params']['uid'];
                $fid = $row['params']['fid'];
                $text = $row['params']['text'];
                return str_replace(['%uid%', '%fid%', '%text%'], [self::getProfileLink($uid), self::getProfileLink($fid), $text], UserLog::TEXTS[$row['action']]);
            }
            case UserLog::GO_TO_PAYMENT_SYSTEM: {
                $system = $row['params']['system'];
                $sum = $row['params']['sum'];
                return str_replace(['%system%', '%sum%'], [Payments::$humansText[$system], $sum], UserLog::TEXTS[$row['action']]);
            }
            case UserLog::RETURN_FROM_PAYMENT_SYSTEN_WITHOUT_PAY: {
                $system = $row['params']['system'];
                return str_replace(['%system%'], [Payments::$humansText[$system]], UserLog::TEXTS[$row['action']]);
            }
            case UserLog::USER_AUTH_FROM_TEMPORARY:
            case UserLog::TEMPORARY_AUTH_AS_USER: {
                $uid = $row['params']['uid'];
                $tempuid = $row['params']['tempuid'];
                return str_replace(['%user%', '%tempuser%'], [self::getProfileLink($uid), self::getProfileLink($tempuid)], UserLog::TEXTS[$row['action']]);
            }
            default: {
                return vsprintf(UserLog::TEXTS[$row['action']], $row['params']);
            }
        }

    }

    private function getProfileLink($fid)
    {
        static $_cache = [];

        if (isset($_cache[$fid])) {
            return $_cache[$fid];
        }

        $user = User::getUserInfo($fid);
        return $_cache[$fid] = $this->twig->render('admin/userlink.twig', ['user' => $user]);
    }

    private function getGameLink($gid)
    {
        static $_cache = [];

        if (isset($_cache[$gid])) {
            return $_cache[$gid];
        }

        $game = Game::getGameById($gid, true);
        return $_cache[$gid] = $this->twig->render('admin/gamelink.twig', ['game' => $game]);
    }

    private function view($data)
    {
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($data));
        return true;
    }
}