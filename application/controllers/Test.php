<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 06.08.15
 * Time: 16:09
 */

class Test extends CI_Controller
{
    public function paypal()
    {
        // 2. Provide your Secret Key. Replace the given one with your app clientId, and Secret
        // https://developer.paypal.com/webapps/developer/applications/myapps
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'ARTRyUiVNf3tFiXDA5veJEt47IJWeLb1jaZ0u8a9j5qObQGXzed-ODuBYxmcAoYFCa2O2Ih9z6dWtTfi',     // ClientID
                'EIQntIvhF2sFGkVRrBnVoF6aNAPo0Gd0_1ELqvy564TotYAyCm2ELgFqh7wlWhk-aEP0RA5Bt01gEzVf'      // ClientSecret

//                'AYSq3RDGsmBLJE-otTkBtM-jBRd1TCQwFf9RGfwddNXWz0uFU9ztymylOhRS',     // ClientID
//                'EGnHDxD_qRPdaLdZz8iCr8N7_MzF-YHPTkjs6NKYQvQSBngp4PTTVWkPZRbL'      // ClientSecret
            )
        );

        // Step 2.1 : Between Step 2 and Step 3
        $apiContext->setConfig(
            array(
                'log.LogEnabled' => true,
                'log.FileName' => 'PayPal.log',
                'log.LogLevel' => 'DEBUG'
            )
        );


        $inputFields = new \PayPal\Api\InputFields();
        $inputFields->setNoShipping(1);

        $webProfile = new \PayPal\Api\WebProfile();
        $webProfile->setName('test' . uniqid())->setInputFields($inputFields);
        $webProfileId = $webProfile->create($apiContext)->getId();

        // 3. Lets try to create a Payment
        // https://developer.paypal.com/docs/api/payments/#payment_create
        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');


        $item1 = new \PayPal\Api\Item();
        $item1->setName('10 рублей')
            ->setCurrency('RUB')
            ->setQuantity(1)
            ->setPrice(10);

        $itemList = new \PayPal\Api\ItemList();
        $itemList->setItems(array($item1));

        $details = new \PayPal\Api\Details();
        $details->setShipping(0)
            ->setTax(0);

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal(10);
        $amount->setCurrency('RUB');
        $amount->setDetails($details);

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setItemList($itemList)
            ->setAmount($amount)
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl("https://example.com/your_redirect_url.html")
            ->setCancelUrl("https://example.com/your_cancel_url.html");

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls)
            ->setExperienceProfileId($webProfileId);

        // 4. Make a Create Call and print the values
        try {
            $payment->create($apiContext);

            $this->load->helper('url');
            redirect($payment->getApprovalLink());
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            echo $ex->getData();
        }
    }


    public function viewTime()
    {
        $date = '23-04-2018 00:00:00';
        $time = strtotime($date);

        echo date('Y/m/d', $time) . ' => ' . $time . "\n";
    }

    public function testWebPush()
    {
        $auth = array(
            'GCM' => 'AAAAs6yWuR4:APA91bEfhwY2_9AEWd0p4I7imdN00vDbvOsPLQSEOTI5uBfa9Uzq-D7C-NHgy5FMKmp1HPYxCBvXGM29AunUFMbEWdpe3-FYAYuc9bHBpbJD7Ezsm2jQfuOAIB0DCCLd2DeuVP-QohlZ', // deprecated and optional, it's here only for compatibility reasons
            'VAPID' => array(
                'subject' => 'https://test.exe.ru/', // can be a mailto: or your website address
                'publicKey' => 'BAqwfUlTj4ZvwPW8OiVJQ54VoKzJaKGQ3ty4R/1PuFjTf7iIfWlWyiVcuOpSYHT/ceZWQ24By3FU6JxugNfdQRc=', // (recommended) uncompressed public key P-256 encoded in Base64-URL
                'privateKey' => 'd+K/PlemH3sbuWohsP7C9zffjUAx6mepzTTf3kmFjc8=', // (recommended) in fact the secret multiplier of the private key encoded in Base64-URL
                //'pemFile' => 'path/to/pem', // if you have a PEM file and can link to it on your filesystem
                //'pem' => 'pemFileContent', // if you have a PEM file and want to hardcode its content
            ),
        );

        $payload = json_encode(['messageId' => 1]);

        $webPush = new \Minishlink\WebPush\WebPush($auth);

        $subscribes = \Base\Service\WebPush::getSubscribes(1303211, 'new_messages');
        var_dump($subscribes);

        if ($subscribes && count($subscribes) > 0) {
            foreach($subscribes as $subscribe) {
                $subscribe = json_decode($subscribe['subscribe'], true);
                $webPush->sendNotification($subscribe['endpoint'], $payload, $subscribe['keys']['p256dh'], $subscribe['keys']['auth']);
            }

            var_dump($webPush->flush());
        }
/*
        var_dump($endpoints); exit();

        $subscribe = json_decode('{"endpoint":"https://android.googleapis.com/gcm/send/dBRzkbJg5sU:APA91bEt68wIo60sPZIKFeGlznDlVRf1wBYeZX5hNSZO3OqiirMb71Cykz0lRkYwY_l8SVgLkCX208MEE9T3_vMeGxSM-xdWkQcMXxekEXhAHRdnsM78YqfAiwrEWadkY_Wr8NcwhJk3","expirationTime":null,"keys":{"p256dh":"BKBslRCnIt_0OqeSrIl_GQokuu11Uv8Esf1KfFrUxr7iI1jbSccXFPsO_FMYx3kqiHi3ttRRDr8hH2n3WY384pM=","auth":"_HAkq_nmFtWluAxal5TPhA=="}}', true);
        $subscribe2 = json_decode('{"endpoint":"https://updates.push.services.mozilla.com/wpush/v1/gAAAAABaVzl5GcfXDO7OTeM8V9K5cPp8JBJPXQPjxypZnlRG72hV4QyCdSIfLCVAkr3zDJAfu3IYVDNQKA0D0gzqiKLwYTGEvqC4y1oR3b5ZwVnVfYFdZ6NrXqrNOTz1MlTKO05VPSAP","keys":{"auth":"gyK1NOaEmD1Rse4OHMlLoQ","p256dh":"BOoviKUsbFO7KDMDn8cc7YADO5KJYjsWBDgWeqTzE__XpTdJMvFrncMRcwgnUENRnquzIzUblfRhmrmIVxCTC_c"}}', true);

        $subscribes = [$subscribe, $subscribe2];

        foreach($subscribes as $subscribe) {
            $webPush->sendNotification($subscribe['endpoint'], $payload, $subscribe['keys']['p256dh'], $subscribe['keys']['auth']);
        }

        var_dump($webPush->flush());*/
    }

    public function viber()
    {
        \Base\Service\Viber::send(\Base\Service\Viber::admins, 'Test');
    }

    public function rt()
    {
        var_dump(\Base\Service\Redis::getKeys('__user*'));
    }

    public function timetest()
    {
        fastcgi_finish_request();
        echo "1";
    }

    public function cleanDuplicates()
    {
        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $query = 'SELECT "id", COUNT("id") FROM "sessions" GROUP BY "id" HAVING COUNT("id") > 1 ORDER BY "id" LIMIT 5000';
        $result = $db->query($query)->result_array();

        var_dump(count($result));

        if (count($result) > 0) {
            foreach($result as $row) {
                var_dump($row);
                $query = 'SELECT * FROM "sessions" WHERE "id" = ' . $row['id'] . ' ORDER BY "time_duration" DESC LIMIT 1';
                $oneRow = $db->query($query)->result_array();

                $query = 'DELETE FROM "sessions" WHERE "id" = ' . $row['id'];
                $db->query($query);

                $db->insert('sessions_' . $oneRow[0]['yyyymmdd'], $oneRow[0]);
            }
        }
    }

    public function regsByAdvent($bid, $exp_min, $exp_max)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 'on');

        $tempTableName = 'test_' . time();

        set_time_limit(0);

        $bid = (int) $bid;
        $exp_min = (int) $exp_min;
        $exp_max = (int) $exp_max;

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $postgres = & $ci->db;

        $mysql = mysqli_connect('185.45.144.52', 'exeru', 'triwyo54a2dn2g81wnscrqidz2aop3rsf8c03l3o', 'exeru','54330')
            or die('Can`t connect to percona');

        $query = 'SELECT `uid` FROM `stats__exeru_users` WHERE `bid` = ' . $bid;

        if ($exp_min > 0) {
            $query .= ' AND `exp_init` >= ' . $exp_min;
        }

        if ($exp_max > 0) {
            $query .= ' AND `exp_init` < ' . $exp_max;
        }

        $result = mysqli_query($mysql, $query)
            or die(mysqli_error($mysql));

        $uids = [];
        if ($result->num_rows > 0) {
            if (method_exists('mysqli_result', 'fetch_all')) {
                $res = $result->fetch_all(MYSQLI_ASSOC);
            } else {
                for ($res = []; $tmp = $result->fetch_array(MYSQLI_ASSOC);) $res[] = $tmp;
            }

            $uids = array_column($res, 'uid');
        }
        echo "MySQL: количество - " . count($uids) . "\n";

        if (count($uids) > 0) {
            $query = 'CREATE TEMPORARY TABLE "'.$tempTableName.'_markers" AS SELECT DISTINCT "marker" FROM "sessions" WHERE "init" AND "uid" IN ('. implode(', ', $uids) .')';
            $postgres->query($query);
        }

        $query = 'CREATE TEMPORARY TABLE "'.$tempTableName.'_action17uids" AS SELECT DISTINCT "uid" FROM "userlog" WHERE "action" = 17 AND "marker" IN (SELECT "marker" FROM "'.$tempTableName.'_markers")';
        $postgres->query($query);

        $query = 'SELECT * FROM "'.$tempTableName.'_action17uids"';
        $result = $postgres->query($query);
        $action17 = array_column($result->result_array(), 'uid');
        echo "Postgres: количество регистраций - " . count($action17) . "\n";

        if (count($action17) > 0) {
            $query = 'SELECT SUM(`ret2`) AS `ret2`, SUM(`ret7`) AS `ret7`, SUM(`ret30`) AS `ret30` FROM `stats__exeru_users` WHERE `uid` IN ('.implode(', ', $action17).')';
            $result = mysqli_query($mysql, $query)
            or die(mysqli_error($mysql));

            if ($result->num_rows > 0) {
                if (method_exists('mysqli_result', 'fetch_all')) {
                    $res = $result->fetch_all(MYSQLI_ASSOC);
                } else {
                    for ($res = []; $tmp = $result->fetch_array(MYSQLI_ASSOC);) $res[] = $tmp;
                }
                echo "MySql: возвраты: ret2 = " . $res[0]['ret2'] . ", ret7 = " . $res[0]['ret7'] . ", ret30 = " . $res[0]['ret30'] . "\n";
            }
        }

        $query = 'CREATE TEMPORARY TABLE "'.$tempTableName.'_action19uids" AS SELECT DISTINCT "uid" FROM "userlog" WHERE "action" = 19 AND "marker" IN (SELECT "marker" FROM "'.$tempTableName.'_markers")';
        $postgres->query($query);

        $query = 'SELECT * FROM "'.$tempTableName.'_action19uids"';
        $result = $postgres->query($query);
        $action19 = array_column($result->result_array(), 'uid');
        echo "Postgres: количество регистраций через социалки - " . count($action19) . "\n";

        if (count($action19) > 0) {
            $query = 'SELECT SUM(`ret2`) AS `ret2`, SUM(`ret7`) AS `ret7`, SUM(`ret30`) AS `ret30` FROM `stats__exeru_users` WHERE `uid` IN ('.implode(', ', $action19).')';
            $result = mysqli_query($mysql, $query)
            or die(mysqli_error($mysql));

            if ($result->num_rows > 0) {
                if (method_exists('mysqli_result', 'fetch_all')) {
                    $res = $result->fetch_all(MYSQLI_ASSOC);
                } else {
                    for ($res = []; $tmp = $result->fetch_array(MYSQLI_ASSOC);) $res[] = $tmp;
                }
                echo "MySql: возвраты: ret2 = " . $res[0]['ret2'] . ", ret7 = " . $res[0]['ret7'] . ", ret30 = " . $res[0]['ret30'] . "\n";
            }
        }

        $query = 'CREATE TEMPORARY TABLE "'.$tempTableName.'_actionNReg" AS SELECT DISTINCT "uid" FROM "userlog" WHERE "marker" IN (SELECT "marker" FROM "'.$tempTableName.'_markers")
             AND "action" NOT IN (20, 21, 22, 23)
             AND "uid" NOT IN (SELECT "uid" FROM "'.$tempTableName.'_action17uids") AND "uid" NOT IN (SELECT "uid" FROM "'.$tempTableName.'_action19uids")';
        $postgres->query($query);

        $query = 'SELECT * FROM "'.$tempTableName.'_actionNReg"';
        $result = $postgres->query($query);
        $actionNReg = array_column($result->result_array(), 'uid');
        echo "Postgres: количество не зарегистрировавшихся - " . count($actionNReg) . "\n";

        if (count($actionNReg) > 0) {
            $query = 'SELECT SUM(`ret2`) AS `ret2`, SUM(`ret7`) AS `ret7`, SUM(`ret30`) AS `ret30` FROM `stats__exeru_users` WHERE `uid` IN ('.implode(', ', $actionNReg).')';
            $result = mysqli_query($mysql, $query)
            or die(mysqli_error($mysql));

            if ($result->num_rows > 0) {
                if (method_exists('mysqli_result', 'fetch_all')) {
                    $res = $result->fetch_all(MYSQLI_ASSOC);
                } else {
                    for ($res = []; $tmp = $result->fetch_array(MYSQLI_ASSOC);) $res[] = $tmp;
                }

                echo "MySql: возвраты: ret2 = " . $res[0]['ret2'] . ", ret7 = " . $res[0]['ret7'] . ", ret30 = " . $res[0]['ret30'] . "\n";
            }
        }
    }

    public function testDate()
    {
        $a = ['1', '2', '3', '4'];
        $b = ['1', '2', '3', '4'];

        var_dump(in_array($a, $b)); exit();

        $start = strtotime('2017-05-18');
        var_dump($start, date('d/m/Y H:i:s', $start));
    }


    public function testLF($marker = '9a7c14ce7020a57908895ec26ab9b541')
    {
        //var_dump(\Base\Service\User::getLFIdByMarker($marker));
        //var_dump(\Base\Service\UserLog::getLogByMarker($marker));
        \Base\Service\User::closeUserMarker($marker, time());
    }

    public function testUserlog()
    {
        set_time_limit(0);

        $ci = & get_instance();

        if (!isset($ci->db)) {
            $ci->load->database();
        }

        $db = & $ci->db;

        $mainTableName = "userlog";

        $startToday = strtotime(date('d-m-Y 00:00:00')) - 86400;

        for($i=1; $i<=3; $i++) {
            $dayStart = strtotime(date('d-m-Y 00:00:00', $startToday + ($i * 86400)));
            $nextDayStart = strtotime(date('d-m-Y 00:00:00', $startToday + (($i + 1) * 86400)));

            $tablename = 'userlog_' . date('Ymd', $dayStart);

            $query = 'CREATE TABLE IF NOT EXISTS "' . $tablename . '" () inherits ("' . $mainTableName . '")';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' .$tablename . '" DROP CONSTRAINT IF EXISTS "date_check"';
            $db->query($query);
            echo $query . "\n";

            $query = 'ALTER TABLE "' . $tablename . '" ADD CONSTRAINT "date_check" CHECK ("time" >= \'' . $dayStart . '\' AND "time" < \'' . $nextDayStart . '\')';
            $db->query($query);
            echo $query . "\n";

            $query = 'DROP INDEX IF EXISTS "' . $tablename . '_time_key"';
            $db->query($query);
            echo $query . "\n";

            $query = 'CREATE INDEX "' . $tablename . '_time_key" ON "' . $tablename . '" USING btree("time")';
            $db->query($query);
            echo $query . "\n";

            $query = 'WITH x AS (DELETE FROM ONLY "' . $mainTableName . '" WHERE "time" >= \'' . $dayStart . '\' AND "time" < \'' . $nextDayStart . '\' RETURNING *)
                        INSERT INTO "' . $tablename . '" SELECT * FROM x';

            $db->query($query);
            echo $query . "\n";
        }
    }

    public function photos()
    {
        $data = file(FCPATH . 'auth_social.log');
        $gp = '';
        foreach($data as $line) {
            $line = trim($line);

            if (strlen($line) == 0) {
                continue;
            }

            switch(true) {
                case (strpos($line, '\'access_token=') === 0): {
                    //var_dump('FB:token::' . $line);
                    break;
                }
                case (strpos($line, 'fbcdn.net') !== false): {
                    var_dump('FB:photo::' . $line);
                    break;
                }
                case (strpos($line, '\'{"access_token":"') === 0): {
                    //var_dump('VK:token::' . $line);
                    break;
                }
                case (strpos($line, 'pp.vk.me') !== false): {
                    var_dump('VK:photo::' . $line);
                    break;
                }
                case (strpos($line, '/camera_400.png') !== false): {
                    //var_dump('VK:no photo::' . $line);
                    break;
                }
                case ($line === "'{\"response\":[]}'"): {
                    //var_dump('VK:no photo::' . $line);
                    break;
                }
                case (strpos($line, '\'{"uid":"') === 0): {
                    var_dump('OK:token and photo::' . $line);
                    break;
                }
                default: {
                    $gp .= $line;
                }

            }
/*            $object = json_decode(trim($line, "'"));
            var_dump($object);*/
        }
        $gp_parts = explode("''", $gp);
        foreach ($gp_parts as $part) {
            if (strpos($part, '{"error":') === 0) {
                continue;
            }
            if (strpos($part, '{"access_token" : "') !== false) {
                continue;
            }
            var_dump($part);
        }

    }

    public function stats_test()
    {
        //$appKey = '5b53bbc365fa4e297d819e1352d49bfe'; // Румба
        //$appKey = '595609cc1cc0f2a17290fe67a134d9c8'; // Родина
        $appKey = 'cd1be061b1fd88cdd803c3c2e7045b03'; // Игруля
        $params = [
            'api_id' => 1,
            //'app_id' => 15, // Румба
            //'app_id' => 343, // Родина
            'app_id' => 340, // Игруля
            'format' => 'json',
            'message' => '3 Все жданкиscds съела)!!!!!!',
            'method' => 'sendNotification',
            'rnd' => rand(),
            'type' => 1,
            'user_ids' => '1303195,1303203',
            //'test_mode' => 1
        ];

        ksort($params);

        $paramsString = '';

        array_walk($params, function($value, $key) use (&$paramsString) {
            $paramsString .= $key . '=' .$value;
        });

        $params['sig'] = md5($paramsString . $appKey);

        $queryString = http_build_query($params);

        $result = file_get_contents('http://test.exe.ru/apiv1/?' . $queryString);

        var_dump($queryString);
        var_dump($result);
    }

    public function redis_test()
    {
        var_dump(Base\Service\Redis::getKeys('*'));
    }

    public function testMail()
    {
        \Base\Service\Mail::send('xstudent@yandex.ru', 'Тестовое письмо', 'Тело тестового письма.');
    }

    public function createSessions($uid, $gid)
    {
        // Генерируем данные
        $data = [];
        $data['uid'] = $uid;
        $data['gid'] = $uid;
        $data['u_sessid'] = md5(rand() . $uid);
        $data['g_sessid'] = $data['u_sessid'] . md5(rand() . $gid);

        // Удаляем старые идентификаторы
        $this->db->delete('user_sessions', 'uid = ' . $data['uid']);
        $this->db->delete('game_sessions', 'uid = ' . $data['uid']);

        // Добавляем новые
        $this->db->insert('user_sessions', [
            'sid' => $data['u_sessid'],
            'uid' => $data['uid'],
            'last_action' => time()
        ]);
        $this->db->insert('game_sessions', [
            'sid' => $data['g_sessid'],
            'uid' => $data['uid'],
            'gid' => $data['gid'],
            'time' => time()
        ]);

        $this->twig->view('user_api.twig', $data);
    }
}