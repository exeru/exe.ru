<?php

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.08.15
 * Time: 15:49
 */
use Base\Service\Account,
    Base\Service\Api,
    Base\Service\Auth,
    Base\Service\AuthLog,
    Base\Service\Blacklist,
    Base\Service\Dictionary,
    Base\Service\Dialogs,
    Base\Service\Events,
    Base\Service\Feeds,
    Base\Service\File,
    Base\Service\Friends,
    Base\Service\Game,
    Base\Service\GReCaptcha,
    Base\Service\Hash,
    Base\Service\Image,
    Base\Service\Mail,
    Base\Provider\Payment,
    Base\Service\Payments,
    Base\Service\Purchases,
    Base\Service\Redis,
    Base\Service\RedisKeys,
    Base\Service\Roles,
    Base\Service\User,
    Base\Service\UserLog;


defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (Auth::isAuth()) {
            User::getSetOnline(Auth::getUserId());
            if (!in_array($this->router->method, ['login', 'register', 'friendsGrade'])) {
                session_write_close();
            }
        } elseif (!in_array($this->router->method, ['login', 'register', 'restore', 'play', 'lf', 'jlf'])) {
            show_error('Access denied', 403, 'Forbidden');
        }

        Auth::getUserMarker();
    }

    public function jlf()
    {
        $data = $this->input->get_post('data');

        if (empty($data)
            || !isset($data['uid']) || !isset($data['sess'])
            || ((int) $data['uid']) < 1
            || strlen($data['sess']) !== 32
            || !self::isLFSignValid($data['uid'], $data['sess'])) {

            return $this->view([]);
        }

        User::assignMarkerWithLFId(Auth::getUserMarker(), $data['uid']);
        return $this->view([]);
    }

    public function lf()
    {
        $data = $this->input->get_post('data');

        if (empty($data)
            || !isset($data['uid']) || !isset($data['sess'])
            || ((int) $data['uid']) < 1
            || strlen($data['sess']) !== 32
            || !self::isLFSignValid($data['uid'], $data['sess'])) {

            if (!isset($this->session)) {
                $this->load->library('session', 'core');
            }
            $this->session->set_flashdata('lostFilmId', -1);

            session_write_close();
            return $this->view([]);
        }

        User::assignMarkerWithLFId(Auth::getUserMarker(), $data['uid']);

        //Auth::loginAsUser(1303195);
        //Auth::remember();

        if (!isset($this->session)) {
            $this->load->library('session', 'core');
        }
        $this->session->set_flashdata('lostFilmId', $data['uid']);

        session_write_close();
        return $this->view([]);
    }

    private function isLFSignValid($uid, $sign)
    {
        $secret_key = 'dfbnjfghDFGgj390rtfgdgsdGGGGGG';

        return $sign === md5(('::'.$uid.'::'.$secret_key.'::'));
    }

    public function acceptUser()
    {
        fastcgi_finish_request();

        Base\Service\Log::profile(__METHOD__);

        $uid = Auth::getUserId();
        $usid = trim($this->input->get_post('sid'));
        $gsid = trim($this->input->get_post('gid'));

        if (strlen($usid) == 32) {
            $redisKeyTime = RedisKeys::userLastActive . 'time::';
            $redisKeySid = RedisKeys::userLastActive . 'sid::';
            $redisKeyMarkerSidTime = RedisKeys::userMarkerUserSession . Auth::getUserMarker() . '::';

            $lastActiveTime = Redis::hGet($redisKeyTime, $uid);
            $lastActiveSid = Redis::hGet($redisKeySid, $uid);

            $time = time();

            if ( !($lastActiveTime < 1) ) {
                Redis::hSet($redisKeyTime, $uid, $time);
            }

            if ( !($lastActiveSid < 1) && $lastActiveSid == $usid ) {
                Redis::hSet($redisKeyMarkerSidTime, $lastActiveSid, $time);
            }
        }

        if (strlen($gsid) == 64) {
            $gid = Game::getGameIdBySid($gsid);

            if ($gid !== false) {
                $redisKeyMarkerGSidTime = RedisKeys::userMarkerGameSession . Auth::getUserMarker() . '::';
                Redis::hSet($redisKeyMarkerGSidTime, $gsid, time());

                Api::acceptUser($uid, $gid, $gsid);
            }
        }

        Base\Service\Log::profile(__METHOD__);
    }

    public function getCountries() {
        $countries = Dictionary::getList('country');

        $russia = [219 => $countries[219]];
        unset($countries[219]);
        $countries = $russia + $countries;

        $result = [];

        foreach($countries as $countryId => $countryTitle) {
            $result[] = [
                'id' => $countryId,
                'name' => $countryTitle
            ];
        }

        return $this->view(['result' => $result]);
    }

    public function getCities()
    {
        $countryId = (int) $this->input->get_post('country');

        if ($countryId > 0) {
            $result = Dictionary::getList('city', ['id_country' => $countryId]);
            if ($countryId == 219) {
                $msk = [17849 => $result[17849]];
                $spb = [18026 => $result[18026]];
                unset($result[17849], $result[18026]);
                $result = $msk + $spb + $result;
            }
            $forReturn = [];
            foreach($result as $id => $name) {
                $forReturn[] = [
                    'id' => $id,
                    'name' => $name
                ];
            }
            return $this->view(['result' => $forReturn]);
        }

        return $this->view(['result' => []]);
    }

    public function buyCancel()
    {
        if (1 > ($uid = Auth::getUserId())) {
            show_error('Access denied', 403, 'Forbidden');
        }

        $sid = $this->input->post('sid');

        if (is_null($sid) || strlen($sid) != 64) {
            // С главной страницы
            return;
        }

        $purchaseId = (int) $this->input->post('order_id');
        $gid = Game::getGameIdBySid($sid);

        if ($purchaseId < 1 || $gid < 1) {
            // Это отмена пополнения баланса
            return;
        }

        // Отмена пополнения баланса в процессе покупки в игре
        Payments::forget($uid, $purchaseId);
        Purchases::forget($uid, $purchaseId);

        UserLog::save($uid, $gid, UserLog::BUY_ITEM_CANCEL, ['gid' => $gid], Auth::getUserMarker());
    }

    public function buyRemember()
    {
        $data = $this->input->get('data');

        Purchases::remember(Auth::getUserId(), $data);
    }

    public function buy()
    {
        $item = $this->input->get('item_id');
        $sid = $this->input->get('sid');
        $orderId = $this->input->get('order_id');
        $test = (int) $this->input->get('test_payment');

        $uid = Auth::getUserId();
        $gid = Game::getGameIdBySid($sid);

        if (Purchases::purchaseItem($gid, $uid, $item, $orderId, $test === 1) === true) {
            Purchases::forget($uid, $orderId);
            Payments::forget($uid, $orderId);

            return $this->view(['result' => 'success', 'app_id' => (int) $gid]);
        };

        return $this->view(['result' => 'fail']);
    }

    public function buyPrepare()
    {
        $data = $this->input->get('data');
        $sid = $this->input->get('sid');

        $uid = Auth::getUserId();
        $gid = Game::getGameIdBySid($sid);

        $balance = User::getUserInfo($uid, true)['balance'];

        $result = Purchases::getItemInfo($gid, $uid, $data['item']);

        if ($result === false || $result['price'] < 1) {
            return $this->view([
                'error' => [
                    'code' => 100,
                    'text' => 'Сервер игры временно недоступен.'
                ]
            ]);
        }

        UserLog::save($uid, $gid, UserLog::BEGIN_BUY_ITEM, [
            'gid' => $gid,
            'count' => $result['price'],
            'balance' => $balance
        ], Auth::getUserMarker());

        if ($balance < $result['price']) {
            $result['error'] = [
                'code' => 200,
                'text' => 'Недостаточно средств.'
            ];

            UserLog::save($uid, $gid, UserLog::BUY_ITEM_NO_MONEY, [
                'gid' => $gid,
                'count' => $result['price'],
                'balance' => $balance
            ], Auth::getUserMarker());
        }

        $result['order_id'] = Purchases::prepare($uid, $gid, $result['price']);
        $result['balance'] = $balance;

        if (Roles::hasAccess($uid, $gid, Roles::ACCESS_PAYMENTS)) {
            $result['testing_payments'] = 'allowed';
        }

        return $this->view($result);
    }

    public function orderPrepare()
    {
        $paymentType = $this->input->get('paymentType');
        $count = $this->input->get('radiogroup');
        $phone = $this->input->get('phone');
        $purchaseId = (int) $this->input->get('purchaseId');

        if ($count === 'manual') {
            $count = $this->input->get('count');
        }

        $count = (int) $count;

        if (in_array($paymentType, ['MC', 'MTS', 'BEELINE', 'TELE2', 'MEGAFON']) && $count < 10) {
            return $this->view(['errors'=>'Минимальная сумма мобильного платежа 10 рублей.']);
        }

        if ($paymentType == 'PP' && $count < 50) {
            return $this->view(['errors'=>'Минимальная сумма платежа 50 рублей.']);
        }

        if ($count < 1 || $count >= 100000) {
            return $this->view(['errors'=>'Превышена максимальная сумма для одновременной покупки.']);
        }

        if (!in_array($paymentType, Payments::getTypes())) {
            return $this->view(['errors'=>'Ошибка при выборе платежной системы']);
        }

        $params = Payments::prepare(Auth::getUserId(), $count, $paymentType, $phone, $purchaseId);

        return $this->view($params);
    }

    public function dialogsTyping()
    {
        $fid = (int) $this->input->get_post('uid');
        $typing = $this->input->get_post('typing');

        if ($fid > 0 && in_array($typing, ['on', 'off']) && User::isOnline($fid)) {
            $uid = Auth::getUserId();

            $eventType = $typing == 'on' ? 'EVENT_USER_TYPING_ON' : 'EVENT_USER_TYPING_OFF';

            Events::rise($eventType, [
                'uids' => [$fid],
                'message' => json_encode([
                    'type' => 'user_is_typing',
                    'message' => [
                        'typing' => $typing,
                        'uid' => $uid,
                    ],
                    'uid' => $fid
                ])
            ]);

            return $this->view([
                'response' => 1
            ]);
        }

        return $this->view([
            'response' => 0
        ]);
    }

    public function dialogsMarkAsRead()
    {
        $uid = (int) Auth::getUserId();
        $dids = $this->input->get_post('dids');

        foreach($dids as $index => $value) {
            $value = (int) $value;
            if ($value > 0) {
                $dids[$index] = $value;
            } else {
                unset($dids[$index]);
            }
        }

        if (count($dids) > 0) {
            Dialogs::markAsRead($uid, $dids);

            $fid = Dialogs::getUsersByMId($dids[0])['uid']; // Кто отправлял сообщение

            Events::rise('EVENT_USER_HAS_READ_MESSAGE', [
                'uids' => [$fid],
                'message' => json_encode([
                    'type' => 'has_read_message',
                    'message' => [
                        'dids' => $dids,
                        'uid' => $uid,
                    ],
                    'uid' => $fid
                ])
            ]);

            Events::rise('EVENT_DIALOGS_CHANGE_COUNT', [
                'fids' => [$uid],
                'message' => json_encode([
                    'type' => 'panel',
                    'subtype' => 'comments',
                    'count' => Dialogs::getCountUnReadDialogs($uid)
                ])
            ]);

            Events::rise('EVENT_DIALOGS_CHANGE_COUNT', [
                'fids' => [$fid],
                'message' => json_encode([
                    'type' => 'panel',
                    'subtype' => 'comments',
                    'count' => Dialogs::getCountUnReadDialogs($fid)
                ])
            ]);
        }

        $this->view([]);
    }

    public function dialogsSendMessage()
    {
        $uid = (int) Auth::getUserId();
        $fid = (int) $this->input->post('fid');
        $text = htmlspecialchars(strip_tags(trim($this->input->post('text'))));
        $captcha = trim($this->input->post('captcha'));

        if ($uid < 1 || $fid < 1) {
            return $this->view([
                'response' => 0
            ]);
        }

        if (User::getUserInfo($uid)['is_spammer'] == 't') {
            if (is_null($captcha) || !GReCaptcha::check($captcha)) {
                return $this->view([
                    'response' => 2
                ]);
            } else {
                User::setUserInfo($uid, ['is_spammer' => false]);
                Redis::del(RedisKeys::lastMessageTime . $uid, Redis::__DB_LAST_MESSAGE_TIME);
            }
        }

        $lastSendTime = Redis::get(RedisKeys::lastMessageTime . $uid, Redis::__DB_LAST_MESSAGE_TIME);
        if ($lastSendTime > 1) {
            User::setUserInfo($uid, ['is_spammer' => true]);
            return $this->view([
                'response' => 2
            ]);
        } else {
            $delay = ENVIRONMENT == 'production' ? 1 : 10;
            Redis::set(RedisKeys::lastMessageTime . $uid, time(), $delay, Redis::__DB_LAST_MESSAGE_TIME);
        }

        if ($fid > 0 && mb_strlen($text) > 0) {
            $did = Dialogs::sendMessage($uid, $fid, $text);

            UserLog::save($uid, null, UserLog::USER_WRITE_MESSAGE, ['uid' => $uid, 'fid' => $fid, 'text' => $text], Auth::getUserMarker());

            Events::rise('EVENT_USER_SEND_MESSAGE', [
                'uids' => [$fid],
                'message' => json_encode([
                    'type' => 'new_message',
                    'message' => [
                        'did' => $did,
                        'uid' => $uid,
                        'fid' => $fid,
                        'time' => time(),
                        'text' => $text
                    ],
                    'uid' => $fid
                ])
            ]);

            Events::rise('EVENT_DIALOGS_CHANGE_COUNT', [
                'fids' => [$fid],
                'message' => json_encode([
                    'type' => 'panel',
                    'subtype' => 'comments',
                    'count' => Dialogs::getCountUnReadDialogs($fid)
                ])
            ]);

            return $this->view([
                'response' => 1,
                'message' => [
                    'did' => $did,
                    'uid' => $uid,
                    'fid' => $fid,
                    'time' => time(),
                    'text' => $text
                ],
                'uid' => $uid
            ]);
        }

        return $this->view([
            'response' => 0
        ]);
    }

    private function dialogsBlockActions($action) {
        $uid = (int) Auth::getUserId();
        $mids = $this->input->get_post('mids');

        if (is_array($mids) && count($mids) > 0) {
            foreach($mids as $index=>$value) {
                $mids[$index] = (int) $value;
                if ($mids[$index] < 1) {
                    unset($mids[$index]);
                }
            }

            if (is_array($mids) && count($mids) > 0) {
                switch($action) {
                    case 'restore': {
                        Dialogs::restoreMessages($uid, $mids);
                        break;
                    }
                    case 'delete': {
                        Dialogs::deleteMessages($uid, $mids);
                        break;
                    }
                }

                return $this->view([
                    'response' => 1,
                    'mids' => $mids,
                ]);
            }
        }

        return $this->view([
            'response' => 0
        ]);
    }

    public function dialogsRestoreBlock()
    {
        return $this->dialogsBlockActions('restore');
    }

    public function dialogsDeleteBlock()
    {
        return $this->dialogsBlockActions('delete');
    }

    public function dialogsGetOne()
    {
        $uid = (int) Auth::getUserId();
        $fid = (int) $this->input->get_post('fid');

        if ($fid > 0) {
            $messages = Dialogs::getDialog($uid, $fid);

            if (count($messages) > 0) {
                Dialogs::markAsRead($uid, array_column($messages, 'did'));
            }

            $this->view([
                'response' => 1,
                'messages' => $messages,
                'uid' => $uid,
                'fid' => $fid
            ]);
        } else {
            $this->view([
                'response' => 0
            ]);
        }
    }

    public function dialogsDelete()
    {
        $uid = Auth::getUserId();
        $fid = (int) $this->input->get_post('uid');

        if ($fid > 0) {
            Dialogs::deleteDialog($uid, $fid);

            $this->view([
                'response' => 1,
                'fid' => $fid
            ]);
        } else {
            $this->view([
                'response' => 0
            ]);
        }
    }

    public function dialogsGetList()
    {
        $uid = Auth::getUserId();
        $dialogs = Dialogs::getList($uid);
        $user = User::getUserInfo($uid);
        $this->view([
            'response' => 1,
            'dialogs' => $dialogs,
            'user' => [
                'uid' => $user['uid'],
                'name' => $user['name'],
                'last_name' => $user['last_name'],
                'photo' => $user['photo'][70]
            ]
        ]);
    }

    public function friendsAction($type)
    {
        $actions = ['accept', 'reject', 'cancel', 'append'];

        $uid = Auth::getUserId();
        $fid = (int) $this->input->get('fid');

        if ($uid > 0 && $fid > 0 && in_array($type, $actions)) {
            switch($type) {
                case 'append': { // Добавление пользователя в друзья (фактически, подписка)
                    $user = User::getUserInfo($uid);
                    Events::rise('EVENT_USER_APPEND_FRIEND', [
                        'uid' => $uid,
                        'fid' => $fid,
                        'message' => json_encode([
                            'type' => 'request_to_friends',
                            'message' => [
                                'user' => [
                                    'uid' => $user['uid'],
                                    'image' => $user['photo'][70],
                                    'name' => $user['name'],
                                    'last_name' => $user['last_name']
                                ]
                            ]
                        ])
                    ]);

                    Events::rise('EVENT_FRIENDS_CHANGE_COUNT', [
                        'fids' => [$fid],
                        'message' => json_encode([
                            'type' => 'panel',
                            'subtype' => 'friends',
                            'count' => Friends::getFriendsMeCount($fid)
                        ])
                    ]);

                    break;
                }
                case 'accept': { // Подтверждение от пользователя, что он согласен дружить, вызываем через события, т.к. надо генерировать новость
                    $uFriendsByType = Friends::getFriendsByTypeArray($uid);
                    $uFriends = array_merge(array_keys($uFriendsByType['both']), array_keys($uFriendsByType['me']));
                    $fFriendsByType = Friends::getFriendsByTypeArray($fid);
                    $fFriends = array_merge(array_keys($fFriendsByType['both']), array_keys($fFriendsByType['me']));

                    Events::rise('EVENT_USER_ACCEPT_FRIEND', [
                        'uid' => $uid,
                        'fid' => $fid,
                        'uids' => array_merge($uFriends, $fFriends, [$uid]),
                        'message' => json_encode([
                            'type' => 'panel',
                            'subtype' => 'news',
                            'action' => 'add'
                        ])
                    ]);

                    Events::rise('EVENT_FRIENDS_CHANGE_COUNT', [
                        'fids' => [$uid],
                        'message' => json_encode([
                            'type' => 'panel',
                            'subtype' => 'friends',
                            'count' => Friends::getFriendsMeCount($uid)
                        ])
                    ]);

                    break;
                }
                case 'reject': {
                    Friends::rejectFriend($uid, $fid);

                    Events::rise('EVENT_FRIENDS_CHANGE_COUNT', [
                        'fids' => [$uid],
                        'message' => json_encode([
                            'type' => 'panel',
                            'subtype' => 'friends',
                            'count' => Friends::getFriendsMeCount($uid)
                        ])
                    ]);

                    break;
                }
                case 'cancel': {
                    Friends::cancelFriend($uid, $fid);

                    Events::rise('EVENT_FRIENDS_CHANGE_COUNT', [
                        'fids' => [$fid],
                        'message' => json_encode([
                            'type' => 'panel',
                            'subtype' => 'friends',
                            'count' => Friends::getFriendsMeCount($fid)
                        ])
                    ]);

                    Events::rise('EVENT_FRIENDS_CHANGE_COUNT', [
                        'fids' => [$uid],
                        'message' => json_encode([
                            'type' => 'panel',
                            'subtype' => 'friends',
                            'count' => Friends::getFriendsMeCount($uid)
                        ])
                    ]);

                    break;
                }
            }
            $this->view([
                'response' => 1,
                'fid' => $fid
            ]);
        } else {
            $this->view([
                'response' => 0
            ]);
        }
    }

    public function feedPublish()
    {
        $uid = Auth::getUserId();
        $sid = $this->input->get('sid');
        $data = $this->input->get('data');

        $gid = Game::getGameIdBySid($sid);

        if ($gid === false) {
            return $this->view([
                'response' => 0
            ]);
        }

        Feeds::addFeed($uid, $gid, 3, $data['text'], NULL, $data['image'], rawurlencode($data['request_key']), NULL);

        return $this->view([
            'response' => 1
        ]);
    }

    public function feedsGet()
    {
        $prevFId = (int) $this->input->get_post('prev');
        $nextFId = (int) $this->input->get_post('next');
        $filter = $this->input->get_post('filter');

        $where = NULL;

        if ($prevFId > 0) {
            // Ищеем новые
            $where = ['fid > ' => $prevFId];
        } else if ($nextFId) {
            // Листаем старые
            $where = ['fid < ' => $nextFId];
        }

        $uid = Auth::getUserId();

        $games = $friends = [];

        if ($filter == 'type-all' || $filter == 'type-game') {
            $games = Game::getUserGameIds($uid);
        }

        if ($filter == 'type-all' || $filter == 'type-user') {
            $friendsByType = Friends::getFriendsByTypeArray($uid);
            $friends = array_merge(array_keys($friendsByType['both']), array_keys($friendsByType['i']));
        }

        $gamesIdWithPermissions = Game::getActiveGamesIdWithPermissions($uid);
        $feeds = Feeds::getFeeds($uid, $games, $friends, 10, NULL, $where, $gamesIdWithPermissions);

        $news = [];
        foreach($feeds as $index=>$feed) {
            $oneNews = [];

            if (isset($feed['uid']) && $feed['uid'] > 0) {
                $oneNews['user'] = \Base\Service\User::getUserInfo($feed['uid']);
            }

            if (!is_null($feed['gid'])) {
                $oneNews['game'] = \Base\Service\Game::getGameById($feed['gid'], in_array($feed['gid'], $gamesIdWithPermissions));
            }

            if (!is_null($feed['fuid'])) {
                $oneNews['friend'] = \Base\Service\User::getUserInfo($feed['fuid']);
            }

            $oneNews['title'] = $feed['title'];
            $oneNews['text'] = $feed['content'];
            $oneNews['fid'] = $feed['fid'];
            $oneNews['date'] = date('d.m.y в H:i', $feed['time']);
            $oneNews['type'] = $feed['type'];
            $oneNews['image_url'] = empty($feed['local_image_url']) ? $feed['image_url'] : $feed['local_image_url'];
            $oneNews['params'] = $feed['params'];

            $news[] = $oneNews;

        }

        $userInfo = User::getUserInfo($uid);

        if (isset($news[0]) && $userInfo['last_fid'] < $news[0]['fid']) {
            User::setUserInfo($uid, ['last_fid' => $news[0]['fid']]);
        }

        return $this->view($news);
    }

    public function feedAction($type)
    {
        $uid = Auth::getUserId();
        $fid = (int) $this->input->get('fid');

        if ($uid > 0 && $fid > 0 && in_array($type, ['restore', 'delete'])) {
            if ($type == 'restore') {
                Feeds::removeFromBlackList($uid, $fid);
            } else {
                Feeds::appendToBlackList($uid, $fid);
            }

            $this->view([
                'response' => 1,
                'fid' => $fid
            ]);
        } else {
            $this->view([
                'response' => 0
            ]);
        }
    }

    public function friendsGrade()
    {
        $fid = (int) $this->input->get('fid');
        $gid = (int) $this->input->get('gid');
        $nid = (int) $this->input->get('nid');
        $uid = Auth::getUserId();
        $type = trim($this->input->get('type'));

        if ($gid > 0 && ($nid > 0 || $fid > 0) && $uid > 0 && in_array($type, ['positive', 'negative'])) {
            if ($type == 'positive') {
                $requestKey = $this->input->get('request_key');
                if (!is_null($requestKey)) {
                    $this->session->set_flashdata('request_key', $requestKey);
                }
                $requestType = $this->input->get('request_type');
                if (!is_null($requestType)) {
                    $this->load->library('user_agent');
                    $this->session->set_flashdata('source', $requestType);
                    $this->session->set_flashdata('referrer', $this->agent->referrer());
                }
            }

            Friends::deleteNotification($uid, $fid, $gid, $nid);

            $this->view([
                'response' => 1,
                'game' => [
                    'id' => $gid,
                    'nid' => $nid
                ]
            ]);
        } else {
            $this->view([
                'response' => 0
            ]);
        }
        session_write_close();
    }


    public function notificationAppend()
    {
        $sid = $this->input->get('sid');
        $data = $this->input->get('data');

        $gid = Game::getGameIdBySid($sid);
        if ($gid === false) {
            return $this->view([
                'response' => 0
            ]);
        }

        $uid = Auth::getUserId();
        $fid = $data['uid'];
        $text = $data['text'];
        $params = rawurlencode($data['request_key']);

        if (Friends::getFriendType($uid, $fid) !== 'both' ) {
            return $this->view([
                'response' => 0
            ]);
        }

        Friends::appendNotification($fid, $uid, $gid, time(), $text, $params);

        return $this->view([
            'response' => 1
        ]);
    }

    public function notificationGrade()
    {
        $gid = (int) $this->input->get('gid');
        $nid = (int) $this->input->get('nid');
        $uid = Auth::getUserId();
        $type = trim($this->input->get('type'));

        if ($gid > 0 && $nid > 0 && $uid > 0 && in_array($type, ['positive', 'negative'])) {
            Game::notificationGrade($uid, $gid, $nid, $type);

            $this->view([
                'response' => 1,
                'game' => [
                    'id' => $gid,
                    'nid' => $nid
                ]
            ]);
        } else {
            $this->view([
                'response' => 0
            ]);
        }
    }

    public function deleteGame()
    {
        $gid = (int) $this->input->get('appId');
        $uid = Auth::getUserId();

        $timestamp = time();
        if ($uid > 0 && $gid > 0 && Game::unInstall($uid, $gid, $timestamp)) {
            // Не будем изменять настройки уведомлений от игры
            $notifications = Game::getGameSettings($uid, $gid)['notifications'];
            Api::gamersChange($uid, $gid, 0, $notifications, $timestamp); // Информируем нотификации
            Api::usersChange($uid, 4); // Информируем игровое API

            UserLog::save($uid, $gid, UserLog::UNINSTALL_GAME, $gid, Auth::getUserMarker());

            $this->view([
                'response' => 1,
                'appId' => $gid
            ]);
        } else {
            $this->view([
                'response' => 0
            ]);
        }
    }

    public function setGameSettings()
    {
        $gid = $this->input->get('gid');
        $type = $this->input->get('type');
        $set = $this->input->get('set');

        if (in_array($type, ['notifications','invitible'])
            && in_array($set, ['on','off'])
            && strlen($gid) === 64) {

            $uid = Auth::getUserId();
            // Получаем нормальный идентификатор игры
            $gid = Game::getGameIdBySid($gid);

            if ($type == 'notifications') {  // Для notifications обратная логика: значение on означает, что пользователь отключает уведомления
                $set = ($set == 'on') ? 0 : 1;
            } else {
                $set = ($set == 'on') ? 1 : 0;
            }

            $timestamp = time();
            Game::setGameSettings($uid, $gid, ['last_visit' => $timestamp, $type => $set]);

            if ($type == 'notifications') { // Уведомление модуля нотификаций о изменениях
                Api::gamersChange($uid, $gid, 1, $set, $timestamp); // Информируем нотификации
            } else { // Уведомление модуля игрового api о изменениях
                Api::usersChange($uid, 4); // Информируем игровое API
            }

            $this->view([1]);
        } else {
            $this->view([0]);
        }
    }

    public function friendsInviteToGame()
    {
        $friends = $this->input->get_post('friends');
        $requestKey = $this->input->get_post('request_key');

        if (!is_array($friends)) {
            return $this->view(['response' => 0]);
        }

        foreach($friends as $key=>$value) {
            $value = (int) $value;
            if ($value < 1) {
                unset($friends[$key]);
            } else {
                $friends[$key] = $value;
            }
        }

        if (count($friends) < 1) {
            return $this->view(['response' => 0]);
        }

        $sid = $this->input->get_post('sid');
        $gid = Game::getGameIdBySid($sid);

        if ($gid < 1) {
            return $this->view(['response' => 0]);
        }

        $uid = Auth::getUserId();

        if ($uid < 1) {
            return $this->view(['response' => 0]);
        }

        $game = Game::getGameById($gid);
        $user = User::getUserInfo($uid);

        Events::rise('EVENT_USER_INVITE_TO_GAME', [
            'uids' => $friends,
            'fid' => $uid,
            'gid' => $gid,
            'request_key' => $requestKey,
            'message' => json_encode([
                'type' => 'invite_to_game',
                'message' => [
                    'game' => [
                        'id' => $game['gid'],
                        'title' => $game['title'],
                        'request_key' => $requestKey
                    ],
                    'user' => [
                        'uid' => $user['uid'],
                        'image' => $user['photo'][70],
                        'name' => $user['name'],
                        'last_name' => $user['last_name'],
                    ]
                ]
            ])
        ]);

        return $this->view([
            'response' => 1,
            'users' => $friends
        ]);
    }


    public function fromBlackList()
    {
        $data = [];

        if (Auth::isAuth()) {
            $uid = Auth::getUserId();
            $buid = $this->input->get_post('uid');

            Friends::fromBlackList($uid, $buid);

            Events::rise('EVENT_USER_REMOVE_FROM_BLACKLIST', [
                'uids' => [$buid],
                'message' => json_encode([
                    'type' => 'from_blacklist',
                    'message' => [
                        'uid' => $uid
                    ]
                ])
            ]);

            $data['csrf'] = $this->getCSRF();
            $data['location'] = '/';
        }

        $this->view($data);
    }

    public function toBlackList()
    {
        $data = [];

        if (Auth::isAuth()) {
            $uid = Auth::getUserId();
            $buid = $this->input->get_post('uid');
            // Добавление в черный список не удаляет человека из друзей. Такая логика у контакта. (Удаление из черного списка восстанавливает в друзьях)
            Friends::toBlackList($uid, $buid);

            Events::rise('EVENT_USER_APPEND_TO_BLACKLIST', [
                'uids' => [$buid],
                'message' => json_encode([
                    'type' => 'to_blacklist',
                    'message' => [
                        'uid' => $uid
                    ]
                ])
            ]);

            $data['csrf'] = $this->getCSRF();
            $data['location'] = '/';
        }

        $this->view($data);
    }

    public function getBlackList() {
        $data = [];
        $data['result'] = [];

        if (Auth::isAuth()) {
            $uid = Auth::getUserId();
            $blacklist = Friends::getBlackList($uid);

            if (count($blacklist) > 0) {
                foreach($blacklist as $bid) {
                    $info = User::getUserInfo($bid);
                    $data['result'][] = [
                        'uid' => $info['uid'],
                        'name' => $info['name'],
                        'last_name' => $info['last_name'],
                        'photo' => $info['photo'][120],
                        'type' => 'blacklist'
                    ];
                }
            }

            $data['csrf'] = $this->getCSRF();
        }

        $this->view($data);
    }


    public function requestToFriendsByGame()
    {
        $data = [];
        $data['result'] = [];

        $fids = $this->input->get_post('friends');
        $sid = $this->input->get_post('sid');

        $gid = Game::getGameIdBySid($sid);

        if (count($fids) < 1 || $gid < 1) {
            return $this->view($data);
        }

        $uid = (int) Auth::getUserId();

        foreach($fids as $index=>$fid) {
            $fid = (int) $fid;
            if ($fid < 1) {
                unset($fids[$index]);
                continue;
            }
            // Проверяем наличие себя в черном списке пользователя, и в списке уже отклоненных заявок на дружбу
            $blacklist = Friends::getBlackList($fid);
            if (isset($blacklist[$uid]) || Friends::isRejectedFriend($fid, $uid)) {
                unset($fids[$index]);
                continue;
            }

            $fids[$index] = $fid;
        }

        if (count($fids) < 1) {
            return $this->view($data);
        }

        $game = Game::getGameById($gid);
        $user = User::getUserInfo($uid);

        Events::rise('EVENT_USER_INVITE_TO_FRIEND_BY_GAME', [
            'uid' => $uid,
            'fids' => $fids,
            'gid' => $gid,
            'message' => json_encode([
                'type' => 'invite_to_friend_by_game',
                'message' => [
                    'game' => [
                        'id' => $game['gid'],
                        'title' => $game['title']
                    ],
                    'user' => [
                        'uid' => $user['uid'],
                        'image' => $user['photo'][70],
                        'name' => $user['name'],
                        'last_name' => $user['last_name'],
                    ]
                ]
            ])
        ]);

        return $this->view($fids);

    }

    public function parseFriendsByGame()
    {
        $data = [];
        $data['result'] = $rating = [];

        $fids = $this->input->get_post('uids');

        if (count($fids) < 1) {
            return $this->view($data);
        }

        $uid = (int) Auth::getUserId();
        $blacklist = Friends::getBlackList($uid);

        foreach($fids as $index=>$fid) {
            $fid = (int) $fid;
            if ($fid < 1 || isset($blacklist[$fid])) {
                unset($fids[$index]);
                continue;
            }

            // Отбрасываем всех, с кем как-то связаны
            if (Friends::getFriendType($uid, $fid) !== '') {
                unset($fids[$index]);
                continue;
            }

            $user = User::getUserInfo($fid);

            if (!isset($user['uid']) || User::isTemporaryUser($user['uid'])) {
                unset($fids[$index]);
                continue;
            }

            $info = [
                'uid' => $user['uid'],
                'image' => $user['photo'][120],
                'name' => $user['name'],
                'last_name' => $user['last_name'],
                'online' => $user['online']
            ];

            if (!isset($rating[$user['rating']])) {
                $rating[$user['rating']] = [];
            }

            $rating[$user['rating']][] = $info;
        }

        if (count($rating) > 0) {
            krsort($rating);

            foreach($rating as $values) {
                $data['result'] = array_merge($data['result'], $values);
            }
        }

        return $this->view($data);
    }

    public function getFriendsForInvite()
    {
        $data = [];
        $data['result'] = [];

        $sid = $this->input->get_post('sid');
        $gid = Game::getGameIdBySid($sid);

        if (!Auth::isAuth() || $gid < 1) {
            return $this->view($data);
        }

        $uid = Auth::getUserId();
        $friends = Friends::getFriendsByTypeArray($uid)['both'];

        if (count($friends) < 1) {
            return $this->view($data);
        }

        $rating = [];

        foreach($friends as $fid=>$empty) {
            $user = self::prepareDataForGetFriends($fid);
            $user['invited'] = Friends::hasInviteToGame($uid, $fid, $gid);

            if (!isset($rating[$user['rating']])) {
                $rating[$user['rating']] = [];
            }

            $rating[$user['rating']][] = $user;
        }

        if (count($rating) > 0) {
            krsort($rating);

            foreach($rating as $values) {
                $data['result'] = array_merge($data['result'], $values);
            }
        }

        $data['csrf'] = $this->getCSRF();

        return $this->view($data);
    }

    public function getFriendsCount()
    {
        $data = [];
        $data['result'] = Friends::getCountFriendsByTypes(Auth::getUserId());

        $this->view($data);
    }

    public function getFriends()
    {
        $type = $this->input->get_post('type');
        $start = (int) $this->input->get_post('start');
        $count = (int) $this->input->get_post('count');

        switch($type) {
            case "both": {
                $data = Friends::getFriendsBoth(Auth::getUserId(), $start, $count);
                break;
            }
            case "i": {
                $data = Friends::getFriendsI(Auth::getUserId(), $start, $count);
                break;
            }
            case "me": {
                $data = Friends::getFriendsMe(Auth::getUserId(), $start, $count);
                break;
            }
            default: {
                $data = [];
                $data['result'] = [
                    'both' => Friends::getFriendsBoth(Auth::getUserId()),
                    'i' => Friends::getFriendsI(Auth::getUserId()),
                    'me' => Friends::getFriendsMe(Auth::getUserId())
                ];
            }
        }

/*        if (Auth::isAuth()) {
            $uid = Auth::getUserId();

            $friends = Friends::getFriendsByTypeArray($uid);

            // TODO Часто выполняется, надо бы в кеш
            foreach($friends as $type=>$friendsArray) {
                $data['result'][$type] = [];
                if (count($friendsArray) > 0) {
                    foreach($friendsArray as $uid=>$gid) {
                        $user = self::prepareDataForGetFriends($uid);
                        if ($gid > 0) {
                            $game = Game::getGameById($gid);
                            $user['game'] = [
                                'gid' => $gid,
                                'title' => $game['title'],
                                'image' => $game['image_80x80'],
                            ];

                            if (!is_null($user['game']['image'])) {
                                // Существование файла уже проверялось при получении списка игр пользователя
                                $user['game']['image'] = '/assets/img/app/' . $game['gid'] . '/80x80/' . DPRIMAGEPREFIX . $user['game']['image'];
                            } else {
                                $user['game']['image'] = '/assets/img/dev-news__item-avatar.png';
                            }
                        }
                        $data['result'][$type][] = $user;
                    }
                }
            }

            $data['csrf'] = $this->getCSRF();
        }*/

        $this->view($data);
    }

    public function getInfoAboutUser()
    {
        $uid = $this->input->get('uid');
        $data = User::getUserInfo($uid);
        $data['type'] = Friends::getFriendType(Auth::getUserId(), $uid);

        $this->view($data);
    }

    public function getFriendsTypeAndState()
    {
        $currentUserId = Auth::getUserId();

        $result = [
            'friends' => []
        ];

        $uids = $this->input->get_post('uids');

        if (!empty($uids) && count($uids) > 0) {
            $online = User::getIsOnlineByUids($uids);
            foreach($uids as $uid) {
                $result['friends'][] = [
                    'uid' => (int) $uid,
                    //'friendType' => Friends::getFriendType($currentUserId, $uid),
                    'online' => isset($online[$uid]) ? $online[$uid] : false
                ];
            }
        }

        return $this->view($result);

    }

    public function getExtendedInfoAboutUser()
    {
        \Base\Service\Log::profile(__METHOD__);

        $currentUserId = Auth::getUserId();

        $uid = $this->input->get('uid');
        $data = User::getUserInfo($uid);
        // Отношения между текущим пользователем и тем, чей профиль отображается
        $data['friendType'] = Friends::getFriendType($currentUserId, $uid);

        $rating = [];

        // Проверяем, что текущий не в черном списке другого пользователя.
        if ($data['friendType'] != 'blocked') {

            $data['friends'] = [];
            // Получаем список друзей другого пользователя
/*            $friends = Friends::getFriendsByTypeArray($uid)['both'];
            if (count($friends) > 0) {
                foreach($friends as $fid => $gid) {
                    $info = User::getUserInfo($fid);
                    if ($info == false) {
                        continue;
                    }
                    // Запоминаем отношения между текущим пользователем и одним из друзей другого пользователя
                    $info['friendType'] = Friends::getFriendType($currentUserId, $info['uid']);

                    if (!isset($rating[$info['rating']])) {
                        $rating[$info['rating']] = [];
                    }

                    $rating[$info['rating']][] = $info;
                }

                if (count($rating) > 0) {
                    krsort($rating);

                    foreach($rating as $values) {
                        $data['friends'] = array_merge($data['friends'], $values);
                    }
                }
            }*/

            $activeGamesId = Game::getActiveGamesIdWithPermissions($currentUserId);

            $data['games'] = Game::getUserGames($uid);

            if (count($data['games']) > 0) {
                $gameActionTypes = Game::actionTypes;
                foreach($data['games'] as $key => $game) {
                    if (!isset($activeGamesId[$game['gid']]) && !Roles::isModerator($currentUserId)) {
                        unset($data['games'][$key]);
                        continue;
                    }
                    $data['games'][$key]['type_color'] = $gameActionTypes[$game['type']]['color'];
                    if (!is_null($game['image_256x256'])) {
                        // Существование файла уже проверялось при получении списка игр пользователя
                        $data['games'][$key]['image'] = '/assets/img/app/' . $game['gid'] . '/256x256/' . DPRIMAGEPREFIX . $game['image_256x256'];
                    } else {
                        $data['games'][$key]['image'] = '/assets/img/dev-game__image-poster-128px.png';
                    }
                }
                $data['games'] = array_values($data['games']);
            }

            $feeds = Feeds::getFeeds(0, [], [$uid], 50, 0, NULL, Roles::isModerator($currentUserId) ? NULL : $activeGamesId);

            $news = [];
            foreach($feeds as $index=>$feed) {
                $oneNews = [];
                if (!is_null($feed['gid'])) {
                    $game = Game::getGameById($feed['gid'], Roles::hasAccess($currentUserId, $feed['gid'], Roles::ACCESS_VIEW));
                    if ($game !== false) {
                        if (!is_null($game['image_150x150'])) {
                            // Существование файла уже проверялось при получении списка игр пользователя
                            $game['image'] = '/assets/img/app/' . $game['gid'] . '/150x150/' . DPRIMAGEPREFIX . $game['image_150x150'];
                        } else {
                            $game['image'] ='/assets/img/dev-game__image-poster-75px.png';
                        }
                    }
                    $oneNews['game'] = $game;
                }

                if (!is_null($feed['fuid'])) {
                    $oneNews['friend'] = User::getUserInfo($feed['fuid']);
                }

                if ($feed['image_url']) {
                    $oneNews['image_url'] = empty($feed['local_image_url']) ? $feed['image_url'] : $feed['local_image_url'];
                }
                if (!is_null($feed['params'])) {
                    $oneNews['params'] = $feed['params'];
                }

                $oneNews['title'] = $feed['title'];
                $oneNews['text'] = $feed['content'];
                $oneNews['fid'] = $feed['fid'];
                $oneNews['date'] = date('d.m.y в H:i', $feed['time']);
                $oneNews['type'] = $feed['type'];

                $news[$feed['fid']] = $oneNews;
            }

            krsort($news, SORT_NUMERIC);

            $data['news'] = array_values($news);
        }

        $data['host'] = (ENVIRONMENT === 'development' ? 'http://exe.ru.dev' : (ENVIRONMENT === 'tesing' ? 'http://test.exe.ru' : 'http://exe.ru'));

        \Base\Service\Log::profile(__METHOD__);

        $this->view($data);
    }

    public function searchUsers($type)
    {
        $searchString = $this->input->get_post('search_string');

        $uid = Auth::getUserId();

        $data = [];
        $data['result'] = [];

        if (is_numeric($searchString)) {
            $user = User::getUserInfo($searchString);
            if ($user !== false) {
                $data['result'][] = [
                    'uid' => $user['uid'],
                    'name' => $user['name'],
                    'last_name' => $user['last_name'],
                    'photo' => $user['photo'][120],
                    'type' => Friends::getFriendType($uid, $user['uid'])
                ];
            }
        } else {
            $counter = 0;

            $blacklist = Friends::getBlackList($uid);
            // Если запрос идет из черного списка, сначала выведем пользователей из него
            if ($type != 'friends' && count($blacklist) > 0) {
                $result = User::searchUsersByName($uid, $searchString, [], $blacklist, 10, 0);
                if (count($result) > 0) {
                    array_walk($result, function(&$value) { $value['type'] = 'blacklist'; });
                    $data['result'] = array_merge($data['result'], $result);
                    $counter += count($result);
                }
            }

            $friendsByType = Friends::getFriendsByTypeArray($uid, true);

            // Получаем сначала друзей, которые попали под фильтр
            if (count($friendsByType['both']) > 0) {
                $result = User::searchUsersByName($uid, $searchString, $blacklist, $friendsByType['both'], 10, 0);
                if (count($result) > 0) {
                    array_walk($result, function(&$value) { $value['type'] = 'both'; });
                    $data['result'] = array_merge($data['result'], $result);
                    $counter += count($result);
                }
            }

            // Затем пользователей, которым я отправлял заявку
            if ($counter < 10 && count($friendsByType['i']) > 0) {
                $result = User::searchUsersByName($uid, $searchString, $blacklist, $friendsByType['i'], (10 - $counter), 0);
                if (count($result) > 0) {
                    array_walk($result, function(&$value) { $value['type'] = 'i'; });
                    $data['result'] = array_merge($data['result'], $result);
                    $counter += count($result);
                }
            }

            // Затем получаем пользователей, которые отправляли заявки нам
            if ($counter < 10 && count($friendsByType['me']) > 0) {
                $result = User::searchUsersByName($uid, $searchString, $blacklist, $friendsByType['me'], (10 - $counter), 0);
                if (count($result) > 0) {
                    array_walk($result, function(&$value, $index, $uid) {
                        if (Friends::isRejectedFriend($uid, $value['uid'])) {
                            $value['type'] = 'rejected';
                        } else {
                            $value['type'] = 'me';
                        }
                    }, $uid);
                    $data['result'] = array_merge($data['result'], $result);
                    $counter += count($result);
                }
            }

            // Затем все остальные
            if ($counter < 10) {
                // Для того, чтобы известные нам пользователи не попали в результат, отбросим их
                $blacklist = array_merge($blacklist, $friendsByType['both'], $friendsByType['i'], $friendsByType['me']);
                $result = User::searchUsersByName($uid, $searchString, $blacklist, [], (10 - $counter), 0);

                if (count($result) > 0) {
                    array_walk($result, function(&$value) { $value['type'] = ''; });
                    $data['result'] = array_merge($data['result'], $result);
                }
            }
        }


        $data['tabs'] = Friends::getCountFriendsByTypes($uid);

        $data['csrf'] = $this->getCSRF();

        $this->view($data);
    }

    public function fileUpload()
    {
        $config['upload_path'] = FCPATH . 'assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';

        $this->load->library('upload', $config);
        $this->load->library('Twig');

        if ( ! $this->upload->do_upload('photo')) {
            $error = ['error' => '\'' . addslashes($this->upload->display_errors()) . '\''];
            return $this->twig->view('user/js/error.twig', $error);
        } else {
            $fileData = $this->upload->data();

            if($fileData['is_image'] !== true) {
                unlink($fileData['full_path']);

                $error = ['error' => '\'Файл не является изображением.\''];
                return $this->twig->view('user/js/error.twig', $error);
            }

            $newFileName = md5($fileData['full_path'] . microtime(true)) . '.' . $fileData['image_type'];
            $newFullFileName = $fileData['file_path'] . $newFileName;

            rename($fileData['full_path'], $newFullFileName);
            chmod($newFullFileName, 0755);
            copy($newFullFileName, $newFullFileName . '.orig');

            $resizeWidth = $fileData['image_width'];
            $resizeHeight = $fileData['image_height'];

            if ($resizeWidth > User::MAX_PREVIEW_WIDTH || $resizeHeight > User::MAX_PREVIEW_HEIGHT) {
                if ($resizeWidth > User::MAX_PREVIEW_WIDTH && $resizeHeight > User::MAX_PREVIEW_HEIGHT) {
                    if ($resizeWidth / User::MAX_PREVIEW_WIDTH > $resizeHeight / User::MAX_PREVIEW_HEIGHT) {
                        // Шире
                        Image::resize($newFullFileName, User::MAX_PREVIEW_WIDTH, 0);
                    } else {
                        // Выше
                        Image::resize($newFullFileName, 0, User::MAX_PREVIEW_HEIGHT);
                    }
                } elseif ($resizeWidth > User::MAX_PREVIEW_WIDTH) {
                    Image::resize($newFullFileName, User::MAX_PREVIEW_WIDTH, 0);
                } elseif ($resizeHeight > User::MAX_PREVIEW_HEIGHT) {
                    Image::resize($newFullFileName, 0, User::MAX_PREVIEW_HEIGHT);
                }
            } else {
                // Ресайз в те же размеры используется для снятия анимации.
                Image::resize($newFullFileName, $resizeWidth, $resizeHeight);
            }

            File::upload($newFullFileName); // Выгрузили на удаленный сервер

            $data = [
                'image' => [
                    'path' => '/assets/uploads/' . $newFileName,
                    'width' => $resizeWidth,
                    'height' => $resizeHeight
                ],
                'csrf' => $this->getCSRF()
            ];

            $this->twig->view('user/js/change-photo.twig', ['data' => json_encode($data)]);
        }
    }

    public function saveUserSettings($type)
    {
        switch($type) {
            case 'profile_main': {
                if (count($errors = $this->_saveProfileMain()) > 0) {
                    $data['errors']['summary'] = implode(', ', $errors);
                } else {
                    $data['location'] = '/' . (empty($this->input->get_post('currentPage')) ? '' : $this->input->get_post('currentPage'));
                };
                break;
            }
            case 'change_photo': {
                if (count($errors = $this->_saveChangePhoto()) > 0) {
                    $data['errors']['summary'] = implode(', ', $errors);
                } else {
                    $data['callback'] = 'returnToEditProfile';
                    $data['data'] = User::getUserInfo(Auth::getUserId())['photo'];
                };
                break;
            }
            case 'profile_access': {
                if (!empty($errors = $this->_saveProfileAccess())) {
                    $data = $errors;
                } else {
                    $data['location'] = '/' . (empty($this->input->get_post('currentPage')) ? '' : $this->input->get_post('currentPage'));
                };
                break;
            }
        }

        $data['csrf'] = $this->getCSRF();

        $this->view($data);
    }

    private function _saveProfileAccess()
    {
        $userId = Auth::getUserId();
        $email = User::getUserEmail($userId);

        // Если идентификатор пользователя не социальный, сразу проверяем пароль
        $currentPassword = $this->input->post('current_password');
        if (! User::isSocialEmail($email)) {
            if (strlen($currentPassword) == 0) {
                return [
                    'errors'=>[
                        'summary'=>'Не указан действующий пароль'
                    ],
                    'csrf' => $this->getCSRF()
                ];
            }
            //TODO Вот такая вот странная проверка пароля, возможно стоит сделать по другому
            if ($userId !== User::getUserIdByAuthData($email, $currentPassword)) {
                return [
                    'errors'=>[
                        'summary'=>'Неверный действующий пароль'
                    ],
                    'csrf' => $this->getCSRF()
                ];
            }
            // Смена пароля доступна только пользователям с несоциальными идентификаторами
            $newPassword = $this->input->post('new_password');
            if (strlen($newPassword) > 0) {
                $newPasswordRepeat = $this->input->post('new_password_repeat');
                if ($newPassword !== $newPasswordRepeat) {
                    return [
                        'errors'=>[
                            'summary'=>'Новый пароль и повтор пароля не совпадают'
                        ],
                        'csrf' => $this->getCSRF()
                    ];
                }
                User::setUserPasswordByEmail($email, $newPassword);
                $currentPassword = $newPassword;
            }
        }

        // Смена электронного адреса
        $newEmail = $this->input->post('new_email');
        if (strlen($newEmail) > 0 && $email !== $newEmail) { // Попытка изменения email
            // Ищем существующего пользователя с новым email
            $anotherUserId = User::getUserIdByEmail($newEmail);
            if ($anotherUserId !== false && !User::isSocialEmail($email)) { // Сменить аккаунт могут только социальные пользователи
                return [
                    'errors'=>[
                        'summary'=>'Новый e-mail уже используется на другом аккаунте.'
                    ],
                    'csrf' => $this->getCSRF()
                ];
            }

            $doAssign = $this->input->post('doAssign'); // Признак, что пользователь в курсе, что есть другой аккаунт
            if (User::isSocialEmail($email) && $doAssign !=='true') { // Не в курсе
                return [
                    'callback'=>'handlerDoYouWantAssignAccount',
                    'csrf' => $this->getCSRF()
                ];
            }

            $data = [];

            $this->load->helper('url');
            $this->load->library('Twig');
            $data['link'] = base_url('frontend/change_email');

            // Ключ для хранение нового адреса в редисе, на 1 сутки
            $data['key'] = md5(microtime(true) . $newEmail);
            Redis::set('change_email::wait_confirm::' . $data['key'], $newEmail, 86400);
            $data['newEmail'] = $newEmail;
            $data['email'] = $email;
            $data['email_encode'] = rawurlencode($email);
            $data['token'] = Auth::createTokenByEmail($email);
            $data['site'] = base_url();

            $mailContent = $this->twig->render('letters/new_confirm_change_email.twig', $data);

            Mail::send($newEmail, 'Смена e-mail на сайте EXE.ru', $mailContent);

            return [
                'errors'=>[
                    'summary'=>'Инструкция с информацией о дальнейших действиях отправлена.'
                ],
                'csrf' => $this->getCSRF()
            ];
        }

        return [];
    }

    private function _saveChangePhoto()
    {
        $data = [
            'file_path' => $this->input->post('newPhotoPath')
        ];

        $x1 = (int) $this->input->post('x1');
        $x2 = (int) $this->input->post('x2');
        $y1 = (int) $this->input->post('y1');
        // Изображение у нас должно быть квадратным, поэтому вместо высоты будем использовать ширину.
        $cropWidth = $x2 - $x1;

        // Избавляемся от нечетного пикселя
        if ($cropWidth%2 == 1) {
            $cropWidth--;
        }

        $fullFilePath = FCPATH . ltrim($data['file_path'], '/');

        if (!file_exists($fullFilePath . '.orig')) {
            return ['File ' . $fullFilePath . 'not exists!'];
        }

        rename($fullFilePath . '.orig', $fullFilePath);

        Base\Service\Image::crop($fullFilePath, $x1, $y1, $cropWidth, $cropWidth);

        $uid = Auth::getUserId();

        User::setUserPhoto($uid, $fullFilePath);

        UserLog::save($uid, null, UserLog::CHANGE_PHOTO, null, Auth::getUserMarker());

        unlink($fullFilePath);

        return [];
    }

    private function _saveProfileMain()
    {
        $bday = intval($this->input->post('bday', true));
        $bmonth = intval($this->input->post('bmonth', true));
        $byear = intval($this->input->post('byear', true));

        if ($bday == 0 || $bmonth == 0) {
            $bday = NULL;
        } else {
            $bday = (int) ($bmonth . ($bday < 10 ? '0' . $bday : $bday));
        }

        if ($byear == 0) {
            $byear = NULL;
        }

        $data = [
            'name' => $this->input->post('name', true),
            'last_name' => $this->input->post('last_name', true),
            'bday' => $bday,
            'byear' => $byear,
            'country_id' => (int) $this->input->post('country_id', true),
            'city_id' => (int) $this->input->post('city_id', true),
            'sex' => (int) $this->input->post('sex', true),
            'subscribe' => $this->input->post('subscribe', true)
        ];

        if ($data['country_id'] == 0 || $data['city_id'] == 0) {
            if ($data['country_id'] == 0) {
                $data['country'] = NULL;
            }
            if ($data['city_id'] == 0) {
                $data['city'] = NULL;
            }
        }

        if ($data['country_id'] != 0) {
            // TODO Обдумать такое получение данных
            $country = Dictionary::getList('country');
            if (isset($country[$data['country_id']])) {
                $data['country'] = $country[$data['country_id']];
            }
        }

        if (isset($data['country'])) {
            $city = Dictionary::getList('city', ['id_country' => $data['country_id']]);
            if (isset($city[$data['city_id']])) {
                $data['city'] = $city[$data['city_id']];
            }

        }

        $uid = Auth::getUserId();

        User::setUserInfo($uid, $data);

        UserLog::save($uid, null, UserLog::CHANGE_INFO, null, Auth::getUserMarker());

        return [];
    }

    public function play()
    {
        if (!$this->input->is_ajax_request()) {
            $this->load->helper('url');
            redirect('/');
        }

        $response = $this->input->get_post('response');
        $target = $this->input->get_post('target_url');
        $source = $this->input->get_post('source');

        if (is_null($response) || !GReCaptcha::check($response)) {
            return $this->view([
                'result' => 0
            ]);
        }

        if (is_null($target)) {
            $target = '/';
        }

        if (is_null($source) || $source == '') {
            if (!isset($this->session)) {
                $this->load->library('session', 'core');
            }
            $source = $this->session->flashdata('source');
        }

        if (!is_null($source) && $source != '') {
            $target .= (strpos($target, '?') === false ? '?' : '&');
            $target .= 'source=' . $source;
        }

        if (Auth::isAuth()) {
            return $this->view([
                'location' => $target
            ]);
        }

        User::createNewTemporary();

        return $this->view([
            'location' => $target
        ]);
    }

    public function restore()
    {
        $data = [
            'email' =>  $this->input->post('email')
        ];

        $data['token'] = Auth::createTokenByEmail($data['email']);

        if ($data['token'] === false) {
            $this->view([
                'errors'=>[
                    'type' => 'not_found',
                    'summary'=>'Пользователь не найден.'
                ],
                'csrf' => $this->getCSRF()
            ]);
        } else {
            $this->load->helper('url');

            $data['site'] = base_url();
            $data['link'] = base_url('frontend/restore');
            $data['email_encode'] = rawurlencode($data['email']);

            $this->load->library(['Twig']);

            $mailContent = $this->twig->render('letters/new_restore_step_1.twig', $data);

            Mail::send($data['email'], 'Восстановление пароля на сайте EXE.ru', $mailContent);

            $uid = User::getUserIdByEmail($data['email']);
            UserLog::save($uid, null, UserLog::PASSWORD_RESTORE, null, Auth::getUserMarker());

            $this->view([
                'errors'=>[
                    'type' => 'sent',
                    'summary'=>'Инструкция по смене пароля отправлена.'
                ],
                'csrf' => $this->getCSRF()
            ]);
        }

    }

    public function register()
    {
        $target = (string) $this->input->post('target_url');
        $response = $this->input->get_post('g-recaptcha-response');

        if ($target == '' || !preg_match('|^\/[a-z0-9?=]+$|', $target)) {
            $target = '/';
        }

        if ( (Auth::isAuth() && !User::isTemporaryUser(Auth::getUserId()))
            || (!Auth::isAuth() && (is_null($response) || !GReCaptcha::check($response))) ) {
            return $this->view([
                'location' => $target
            ]);
        }

        $data = [
            'name' => $this->input->post('name'),
            'email' =>  $this->input->post('email'),
            'password' => $this->input->post('password'),
            'passwordRepeat' => $this->input->post('password_repeat'),
            'acceptOffer' => (int) $this->input->post('offer_agree'),
            'position' => $this->input->post('position')
        ];

        $playBefore = (int) $this->input->post('play_before');

        if (User::getUserIdByEmail($data['email']) !== false) {
            session_write_close();
            return $this->view([
                'errors'=>[
                    'summary'=>'Пользователь с таким электронным адресом уже зарегистрирован на сайте. Воспользуйтесь восстановлением пароля.',
                    'type' => 'reg'
                ],
                'csrf' => $this->getCSRF()
            ]);
        }

        //TODO Нужно сделать проверку полей

        if (($data['acceptOffer'] === 1 || $data['position'] === 'left_form') && (
            // Авторизован как временный пользователь
            (Auth::isAuth() && User::isTemporaryUser(Auth::getUserId()) && false === User::getUserIdByEmail($data['email'])) ||
            // Пользователя удалось зарегистрировать и авторизовать
            (User::registrate($data['name'], $data['email'], $data['password'])
                && Auth::login($data['email'], $data['password'])))
        ) {
            $uid = Auth::getUserId();
            if (User::isTemporaryUser($uid)) {
                User::setUserEmail($uid, $data['email']);
                User::setUserPasswordByEmail($data['email'], $data['password']);
                User::setUserInfo($uid, [
                    'name' => $data['name'],
                    'verified' => 1
                ]);
                $userGames = Game::getUserGameIds($uid);
                if (count($userGames) > 0) {
                    foreach($userGames as $gid) {
                        Game::setGameSettings($uid, $gid, ['invitible' => 1]);
                    }
                }
                Auth::remember();
                UserLog::save($uid, null, UserLog::REGISTER_TEMPORARY, null, Auth::getUserMarker());
                if ($playBefore > 0) {
                    $bonus = 10;
                    Account::incrementBalance($uid, $bonus, Payment::PAYMENT_EXE_INTERNAL, $bonus);
                    $balance = User::getUserInfo($uid, true)['balance'];
                    UserLog::save($uid, null, UserLog::BONUS_TO_BALANCE, ['count' => $bonus, 'balance' => $balance]);
                    Events::rise('EVENT_USER_BALANCE_CHANGE_BONUS', [
                        'uids' => [$uid],
                        'message' => json_encode([
                            'type' => 'balance_change_bonus',
                            'message' => [
                                'type' => 'increment',
                                'balance' => $balance
                            ]
                        ])
                    ]);

                }
            } else {
                UserLog::save($uid, null, UserLog::REGISTER, null, Auth::getUserMarker());
                $this->input->set_cookie('eu', base64_encode('form'), 31536000);
            }

            $destinationId = (int) $this->input->post('dest_id');
            if ($destinationId > 0) {
                AuthLog::save($uid, 1, $destinationId);
            }

            // Отправляем письмо с запросом на подтверждение адреса
            $this->load->helper('url');
            $this->load->library('Twig');

            session_write_close();

            $content = $this->twig->render(
                'letters/new_registration_with_confirm.twig',
                [
                    'site' => base_url(),
                    'link' => base_url('frontend/confirm'),
                    'email' => $data['email'],
                    'email_encode' => rawurlencode($data['email']),
                    'token' => Auth::createTokenByEmail($data['email'])
                ]);

            \Base\Service\Mail::send($data['email'], 'Поздравляем с регистрацией на портале EXE.ru', $content);

            $this->view([
                'location'=> $target
            ]);
        } else {
            session_write_close();

            $this->view([
                'errors'=>[
                    'summary'=>'Ошибка при регистрации пользователя'
                ],
                'csrf' => $this->getCSRF()
            ]);
        }
    }

    public function emailConfirm()
    {
        if (!Auth::isAuth()) {
            session_write_close();
            return $this->view([
                'errors'=>[
                    'summary'=>'Не удалось отправить сообщение.'
                ],
            ]);
        }

        $uid = Auth::getUserId();
        $mail = User::getUserEmail($uid);

        if (User::isTemporaryUser($uid) || User::isEmailChecked($uid) || User::isSocialEmail($mail)) {
            session_write_close();
            return $this->view([
                'errors'=>[
                    'summary'=>'Не удалось отправить сообщение.'
                ],
            ]);
        }

        $this->load->helper('url');
        $this->load->library('Twig');

        $content = $this->twig->render(
            '/letters/new_email_confirm.twig',
            [
                'site' => base_url(),
                'link' => base_url('frontend/confirm'),
                'email' => $mail,
                'email_encode' => rawurlencode($mail),
                'token' => Auth::createTokenByEmail($mail)
            ]);

        \Base\Service\Mail::send($mail, 'Подтверждение email', $content);

        session_write_close();
        return $this->view([
            'errors'=>[
                'summary'=>'Письмо отправлено. Перейдите по указанной в нем ссылке.'
            ],
        ]);
    }

    public function login()
    {
        $target = (string) $this->input->post('target_url');

        if ($target == '' || !preg_match('|^\/[a-z0-9?=]+$|', $target)) {
            $target = '/';
        }

        if (Auth::isAuth() && !User::isTemporaryUser(Auth::getUserId())) {
            return $this->view([
                'location' => $target
            ]);
        }

        // Ниже может попасть только гость или временный пользователь

        $email = trim($this->input->post('email'));
        $password = trim($this->input->post('password'));
        $noRemember = trim((string) $this->input->post('noRemember'));

        $uid = User::getUserIdByAuthData($email, $password);

        if (false === $uid) {
            session_write_close();

            return $this->view([
                'errors'=>[
                    'summary'=>'Пользователь не существует'
                ],
                'csrf' => $this->getCSRF()
            ]);
        }

        if (User::getUserStatus($uid) == '0') { // Но заблокирован
            session_write_close();
            return $this->view([
                'location' => '/blocked'
            ]);
        }

        if (Auth::isAuth() && User::isTemporaryUser(Auth::getUserId())) { // Временный пользователь
            User::assignUidWithTempUid($uid, Auth::getUserId());
        }

        Auth::loginAsUser($uid);

        $this->input->set_cookie('eu', base64_encode('form'), 31536000);

        if ($noRemember !== 'on') {
            Auth::remember();
        }

        $destinationId = (int) $this->input->post('dest_id');

        if ($destinationId > 0) {
            AuthLog::save($uid, 0, $destinationId);
        }

        UserLog::save($uid, null, UserLog::AUTH, null, Auth::getUserMarker());

        session_write_close();

        return $this->view([
            'location' => $target
        ]);
    }

    private function prepareDataForGetFriends($uid)
    {
        $info = User::getUserInfo($uid);
        return [
            'uid' => $info['uid'],
            'name' => $info['name'],
            'last_name' => $info['last_name'],
            'photo' => $info['photo'][120],
            'online' => $info['online'],
            'rating' => $info['rating']
        ];
    }

    private function getCSRF()
    {
        return [
            'name' => $this->security->get_csrf_token_name(),
            'value' => $this->security->get_csrf_hash()
        ];
    }

    private function view($data)
    {
        $this->output->set_header('Content-Type: application/json');
        $this->output->set_output(json_encode($data));
        return true;
    }
}