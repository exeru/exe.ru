'use strict';
function detectPrivateMode(cb) {
    var db,
        on = cb.bind(null, true),
        off = cb.bind(null, false);

    function tryls() {
        try {
            localStorage.length ? off() : (localStorage.x = 1, localStorage.removeItem("x"), off());
        } catch (e) {
            // Safari only enables cookie in private mode
            // if cookie is disabled then all client side storage is disabled
            // if all client side storage is disabled, then there is no point
            // in using private mode
            navigator.cookieEnabled ? on() : off();
        }
    }

    // Blink (chrome & opera)
    window.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on)
        // FF
        : "MozAppearance" in document.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off)
        // Safari
        : /constructor/i.test(window.HTMLElement) || window.safari ? tryls()
        // IE10+ & edge
        : !window.indexedDB && (window.PointerEvent || window.MSPointerEvent) ? on()
        // Rest
        : off()
}

if ('serviceWorker' in navigator) {
    document.addEventListener('DOMContentLoaded', function() {
        detectPrivateMode(function(isPrivateMode) {
            if (isPrivateMode !== false || typeof uid !== "number" || uid < 1) {
                return;
            }

            var params = {
                'uid': uid,
                'permission': Notification.permission
            };

            $.post("/push/userstate", params);

            if (Notification.permission === 'denied') {
                return;
            }

            var channels = ['new_messages'];

            for (i = 0; i < channels.length; i++) {
                var channelKey = 'webpushes::' + uid + '::' + channels[i],
                    channelState = window.localStorage.getItem(channelKey);

                if (channelState === null) {
                    var channelInfo = window.localStorage.getItem(channelKey + '::info');
                    if (channelInfo === null) {
                        channelInfo = {show:0};
                    } else {
                        channelInfo = JSON.parse(channelInfo);
                    }
                    channelInfo.show++;

                    window.localStorage.setItem(channelKey + '::info', JSON.stringify(channelInfo));
                    document.getElementById('push_' + channels[i]).style.display = 'block';

                    if (document.getElementById('emailNotChecked')) {
                        document.getElementById('emailNotChecked').style.display = 'none';
                    }

                    break;
                }
            }
        });
    });
}

function pushControl(type, channel) {
    document.getElementById('push_' + channel).style.display = 'none';

    var channelKey = 'webpushes::' + uid + '::' + channel,
        subscribeId = window.localStorage.getItem('webpushes::subscribeId'),
        info = JSON.parse(window.localStorage.getItem(channelKey + '::info'));
    if (type === 'unsubscribe') {
        // Пользователь отказался от подписки на канал
        if (subscribeId !== null) {
            $.post("/push/subscribe", {'type': 'unsubscribe', 'channel': channel, 'show': info.show, 'subscribeId': subscribeId});
        }
        window.localStorage.setItem(channelKey, 'unsubscribe');
    } else if (type === 'subscribe') {
        if (Notification.permission === 'granted') {
            // Пользователь уже разрешил присылать уведомления
            if (subscribeId !== null) {
                $.post("/push/subscribe", {'type': 'subscribe', 'channel': channel, 'show': info.show, 'subscribeId': subscribeId});
            }
            window.localStorage.setItem(channelKey, 'subscribe');
        } else if (Notification.permission === 'default') {
            navigator.serviceWorker.register('/sw.js').then(function() {
                return navigator.serviceWorker.ready;
            }).then(function(reg) {
                reg.pushManager.subscribe({userVisibleOnly: true}).then(function(sub) {
                    // Новая подписка
                    $.post("/push/subscribe", {'type': 'access_granted', 'channel': channel, 'show': info.show, 'subscribe': JSON.stringify(sub)}, function(r) {
                        window.localStorage.setItem('webpushes::subscribeId', r.subscribeId);
                    });
                    window.localStorage.setItem(channelKey, 'subscribe');
                }).catch(function(error) {
                    // Блокировка всех уведомлений
                    //$.post( "/push/subscribe", {'access': 'denied', 'channel': channel, 'show': info.show});
                    //window.localStorage.setItem('webpushes::' + channel + '::' + uid, 'denied');
                    //document.getElementById('pushSubscribe').style.display = 'none';
                });
            }).catch(function(error) {
                // Ошибка в процессе подписки
            });
        }
    }
}