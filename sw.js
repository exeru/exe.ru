'use strict';

self.addEventListener('install', function(event) {
    event.waitUntil(self.skipWaiting());
});

self.addEventListener('push', function(event) {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }

    var data = {};

    if (event.data && typeof event.data == 'object') {
        data = event.data.json();

        event.waitUntil(
            self.registration.showNotification(
                data.title, {
                    body: data.message,
                    icon: data.icon,
                    data: {
                        url: data.url
                    }
                }
            )
        );
    }
});


self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    var url = event.notification.data.url;
    event.waitUntil(
        clients.matchAll({type: 'window'}).then(function(windowClients) {
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});
